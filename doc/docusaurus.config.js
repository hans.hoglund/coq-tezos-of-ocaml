module.exports = {
  title: 'Coq Tezos of OCaml',
  tagline: 'A translation in Coq of the code of the economic protocol of Tezos',
  url: 'https://nomadic-labs.gitlab.io',
  baseUrl: '/coq-tezos-of-ocaml/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/tez.svg',
  organizationName: 'nomadiclabs', // Usually your GitHub org/user name.
  projectName: 'coq-tezos-of-ocaml', // Usually your repo name.
  themeConfig: {
    announcementBar: {
      id: 'support_us',
      content:
        'To add proofs on the protocol, see <a href="/coq-tezos-of-ocaml/docs/contribute">Contribute</a>.',
      backgroundColor: '#fafbfc',
      textColor: '#091E42',
      isCloseable: false,
    },
    colorMode: {
      // Hides the switch in the navbar
      // Useful if you want to support a single color mode
      disableSwitch: true,
    },
    prism: {
      additionalLanguages: ['ocaml'],
    },
    navbar: {
      hideOnScroll: true,
      title: 'Coq Tezos of OCaml',
      logo: {
        alt: 'Logo',
        src: 'img/tez.svg',
      },
      items: [
        {
          to: 'docs/readme/',
          activeBasePath: 'docs/readme',
          label: 'Protocol',
          position: 'left',
        },
        {
          to: 'docs/environment/readme/',
          activeBasePath: 'docs/environment/readme',
          label: 'Environment',
          position: 'left',
        },
        {
          to: 'docs/contribute/',
          activeBasePath: 'docs/contribute',
          label: 'Contribute',
          position: 'left',
        },
        {
          to: 'blog',
          label: 'Blog',
          position: 'left',
        },
        // {
        //   label: 'Securing money',
        //   className: 'slogan_src-pages-',
        //   position: 'right',
        // },
        {
          href: 'https://gitlab.com/nomadic-labs/coq-tezos-of-ocaml',
          label: 'Gitlab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Formalization',
          items: [
            {
              label: 'Protocol',
              to: 'docs/readme/',
            },
            {
              label: 'Environment',
              to: 'docs/environment/readme/',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'Tezos',
              href: 'https://tezos.com/',
            },
            {
              label: 'Nomadic Labs',
              href: 'https://www.nomadic-labs.com/',
            },
            {
              label: 'foobar.land',
              href: 'https://foobar.land/',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'GitLab',
              href: 'https://gitlab.com/nomadic-labs/coq-tezos-of-ocaml',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} <a class="footer__link-item" href="https://www.nomadic-labs.com/" target="_blank">Nomadic Labs</a>`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
