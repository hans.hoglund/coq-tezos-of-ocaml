# ꜩ 🐓 coq-tezos-of-ocaml
> A full translation of the protocol of [Tezos](https://tezos.com/) in [Coq](https://coq.inria.fr/)

**Website: https://nomadic-labs.gitlab.io/coq-tezos-of-ocaml/**

We provide a full translation of the implementation of the [protocol of Tezos](https://gitlab.com/tezos/tezos) using [coq-of-ocaml](https://github.com/clarus/coq-of-ocaml). This could be used to do formal verification of the Tezos blockchain. We translate the OCaml implementation of Tezos to Coq using an automatic method with `coq-of-ocaml` ran by the scripts in `scripts/`. This translation is a shallow embedding in Coq, hoping to have a formalization which is easy to read and manipulate.

## Install
In order to compile this translation as a Coq library, you can use [opam](https://github.com/coq/opam-coq-archive). Run from the command line:
```
opam install coq-of-ocaml
./configure.sh
make -j
```

## Content
* `src/Proto_alpha`: concerns a translation of the current development version of the protocol of Tezos. We try to keep track, as much as possible, of the current protocol version. To re-generate the Coq code, run the `import.rb` script from `scripts/alpha` using a version of the current protocol compatible with `coq-of-ocaml`;
* `src/Proto_2021_01`: concerns a translation of the development version of the protocol of Tezos dating from January 2021. To re-generate the Coq code, run the `import.rb` script from `scripts/2021_01`. We currently generate the Coq code using [this branch](https://gitlab.com/clarus1/tezos/-/tree/guillaume-claret@compiling-in-coq-rebased-with-mli-arm64-no-annots) of the Tezos code, which includes some changes to help the translation to Coq;
* `src/Proto_demo`: concerns the first protocol version for which we generate a translation for all the files. Look at the page [coq-of-ocaml examples](https://clarus.github.io/coq-of-ocaml/examples/tezos/) for more information. This corresponds to a protocol version in-between Babylon and Carthage. You can look at the changes made on the protocol to get this demo to compile on this [merge request](https://gitlab.com/clarus1/tezos/-/merge_requests/1). The scripts to translate this protocol version are on [https://github.com/clarus/coq-of-ocaml/tree/gh-pages/build-tezos](https://github.com/clarus/coq-of-ocaml/tree/gh-pages/build-tezos), using the branch `custom-tezos` of `coq-of-ocaml`.

## Website
To build the website of the project:
```
COQDOCEXTRAFLAGS="--no-lib-name --parse-comments --plain-comments -s --body-only --no-externals" make html
ruby build-doc.rb
cd doc
yarn install # install the JavaScript's dependencies
yarn start # development mode, to get a live preview
yarn build # generation of the static html files, in order to next deploy
```
We use a combination of [coqdoc](https://coq.inria.fr/refman/using/tools/coqdoc.html) and [Docusaurus](https://docusaurus.io/) bound together by the `build-doc.rb` script. Basically, all the `.v` files and `.md` files in the `src/` folder will be translated to a web page. Thus we can have a mix of documentation and proofs. This system could also be used for any Coq project.

We deploy and host the website using the GitLab CI on [https://nomadic-labs.gitlab.io/coq-tezos-of-ocaml](https://nomadic-labs.gitlab.io/coq-tezos-of-ocaml/). The configuration file is `gitlab-ci.yml`.

## Coding style
Here are some coding style rules we use for writing proofs:
* space before and after colons `:` as in OCaml:
```coq
(* right *)
Lemma force_decode_cost_is_valid {a : Set} (lexpr : Data_encoding.lazy_t a)
  : Gas_limit_repr.Cost.Valid.t (Script_repr.force_decode_cost lexpr).

(* wrong *)
Lemma force_decode_cost_is_valid {a: Set} (lexpr:Data_encoding.lazy_t a)
  : Gas_limit_repr.Cost.Valid.t (Script_repr.force_decode_cost lexpr).
```
* no `Proof` keyword to reduce verbosity:
```coq
(* right *)
Lemma refl ctxt : t ctxt ctxt [].
  reflexivity.
Qed.

(* wrong *)
Lemma refl ctxt : t ctxt ctxt [].
Proof.
  reflexivity.
Qed.
```
* no multiple empty lines:
```coq
(* right *)
Definition n := 12.

Definition m := 23.

(* wrong *)
Definition n := 12.


Definition m := 23.
```

We do not care about using generated names in proofs, but try to be reasonable. The main goal is to write as many proofs as possible.
