Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Gas_limit_repr.
Require TezosOfOCaml.Proto_alpha.Period_repr.
Require TezosOfOCaml.Proto_alpha.Tez_repr.

Definition version_number_004 : string := "\000".

Definition version_number : string := "\001".

Definition proof_of_work_nonce_size : int := 8.

Definition nonce_length : int := 32.

Definition max_anon_ops_per_block : int := 132.

Definition max_proposals_per_delegate : int := 20.

Definition max_operation_data_length : int := 32 *i 1024.

Definition max_micheline_node_count : int := 50000.

Definition max_micheline_bytes_limit : int := 50000.

Definition max_allowed_global_constant_depth : int := 10000.

Definition cache_layout : list int := [ 100000000 ].

Definition michelson_maximum_type_size : int := 2001.

Module fixed.
  Record record : Set := Build {
    proof_of_work_nonce_size : int;
    nonce_length : int;
    max_anon_ops_per_block : int;
    max_operation_data_length : int;
    max_proposals_per_delegate : int;
    max_micheline_node_count : int;
    max_micheline_bytes_limit : int;
    max_allowed_global_constant_depth : int }.
  Definition with_proof_of_work_nonce_size proof_of_work_nonce_size
    (r : record) :=
    Build proof_of_work_nonce_size r.(nonce_length) r.(max_anon_ops_per_block)
      r.(max_operation_data_length) r.(max_proposals_per_delegate)
      r.(max_micheline_node_count) r.(max_micheline_bytes_limit)
      r.(max_allowed_global_constant_depth).
  Definition with_nonce_length nonce_length (r : record) :=
    Build r.(proof_of_work_nonce_size) nonce_length r.(max_anon_ops_per_block)
      r.(max_operation_data_length) r.(max_proposals_per_delegate)
      r.(max_micheline_node_count) r.(max_micheline_bytes_limit)
      r.(max_allowed_global_constant_depth).
  Definition with_max_anon_ops_per_block max_anon_ops_per_block (r : record) :=
    Build r.(proof_of_work_nonce_size) r.(nonce_length) max_anon_ops_per_block
      r.(max_operation_data_length) r.(max_proposals_per_delegate)
      r.(max_micheline_node_count) r.(max_micheline_bytes_limit)
      r.(max_allowed_global_constant_depth).
  Definition with_max_operation_data_length max_operation_data_length
    (r : record) :=
    Build r.(proof_of_work_nonce_size) r.(nonce_length)
      r.(max_anon_ops_per_block) max_operation_data_length
      r.(max_proposals_per_delegate) r.(max_micheline_node_count)
      r.(max_micheline_bytes_limit) r.(max_allowed_global_constant_depth).
  Definition with_max_proposals_per_delegate max_proposals_per_delegate
    (r : record) :=
    Build r.(proof_of_work_nonce_size) r.(nonce_length)
      r.(max_anon_ops_per_block) r.(max_operation_data_length)
      max_proposals_per_delegate r.(max_micheline_node_count)
      r.(max_micheline_bytes_limit) r.(max_allowed_global_constant_depth).
  Definition with_max_micheline_node_count max_micheline_node_count
    (r : record) :=
    Build r.(proof_of_work_nonce_size) r.(nonce_length)
      r.(max_anon_ops_per_block) r.(max_operation_data_length)
      r.(max_proposals_per_delegate) max_micheline_node_count
      r.(max_micheline_bytes_limit) r.(max_allowed_global_constant_depth).
  Definition with_max_micheline_bytes_limit max_micheline_bytes_limit
    (r : record) :=
    Build r.(proof_of_work_nonce_size) r.(nonce_length)
      r.(max_anon_ops_per_block) r.(max_operation_data_length)
      r.(max_proposals_per_delegate) r.(max_micheline_node_count)
      max_micheline_bytes_limit r.(max_allowed_global_constant_depth).
  Definition with_max_allowed_global_constant_depth
    max_allowed_global_constant_depth (r : record) :=
    Build r.(proof_of_work_nonce_size) r.(nonce_length)
      r.(max_anon_ops_per_block) r.(max_operation_data_length)
      r.(max_proposals_per_delegate) r.(max_micheline_node_count)
      r.(max_micheline_bytes_limit) max_allowed_global_constant_depth.
End fixed.
Definition fixed := fixed.record.

Definition fixed_encoding : Data_encoding.encoding fixed :=
  Data_encoding.conv
    (fun (c : fixed) =>
      (c.(fixed.proof_of_work_nonce_size), c.(fixed.nonce_length),
        c.(fixed.max_anon_ops_per_block), c.(fixed.max_operation_data_length),
        c.(fixed.max_proposals_per_delegate),
        c.(fixed.max_micheline_node_count), c.(fixed.max_micheline_bytes_limit),
        c.(fixed.max_allowed_global_constant_depth)))
    (fun (function_parameter : int * int * int * int * int * int * int * int) =>
      let
        '(proof_of_work_nonce_size, nonce_length, max_anon_ops_per_block,
          max_operation_data_length, max_proposals_per_delegate,
          max_micheline_node_count, max_micheline_bytes_limit,
          max_allowed_global_constant_depth) := function_parameter in
      {| fixed.proof_of_work_nonce_size := proof_of_work_nonce_size;
        fixed.nonce_length := nonce_length;
        fixed.max_anon_ops_per_block := max_anon_ops_per_block;
        fixed.max_operation_data_length := max_operation_data_length;
        fixed.max_proposals_per_delegate := max_proposals_per_delegate;
        fixed.max_micheline_node_count := max_micheline_node_count;
        fixed.max_micheline_bytes_limit := max_micheline_bytes_limit;
        fixed.max_allowed_global_constant_depth :=
          max_allowed_global_constant_depth |}) None
    (Data_encoding.obj8
      (Data_encoding.req None None "proof_of_work_nonce_size"
        Data_encoding.uint8)
      (Data_encoding.req None None "nonce_length" Data_encoding.uint8)
      (Data_encoding.req None None "max_anon_ops_per_block" Data_encoding.uint8)
      (Data_encoding.req None None "max_operation_data_length"
        Data_encoding.int31)
      (Data_encoding.req None None "max_proposals_per_delegate"
        Data_encoding.uint8)
      (Data_encoding.req None None "max_micheline_node_count"
        Data_encoding.int31)
      (Data_encoding.req None None "max_micheline_bytes_limit"
        Data_encoding.int31)
      (Data_encoding.req None None "max_allowed_global_constants_depth"
        Data_encoding.int31)).

Definition fixed_value : fixed :=
  {| fixed.proof_of_work_nonce_size := proof_of_work_nonce_size;
    fixed.nonce_length := nonce_length;
    fixed.max_anon_ops_per_block := max_anon_ops_per_block;
    fixed.max_operation_data_length := max_operation_data_length;
    fixed.max_proposals_per_delegate := max_proposals_per_delegate;
    fixed.max_micheline_node_count := max_micheline_node_count;
    fixed.max_micheline_bytes_limit := max_micheline_bytes_limit;
    fixed.max_allowed_global_constant_depth := max_allowed_global_constant_depth
    |}.

Module parametric.
  Record record : Set := Build {
    preserved_cycles : int;
    blocks_per_cycle : int32;
    blocks_per_commitment : int32;
    blocks_per_roll_snapshot : int32;
    blocks_per_voting_period : int32;
    time_between_blocks : list Period_repr.t;
    minimal_block_delay : Period_repr.t;
    endorsers_per_block : int;
    hard_gas_limit_per_operation : Gas_limit_repr.Arith.integral;
    hard_gas_limit_per_block : Gas_limit_repr.Arith.integral;
    proof_of_work_threshold : int64;
    tokens_per_roll : Tez_repr.t;
    seed_nonce_revelation_tip : Tez_repr.t;
    origination_size : int;
    block_security_deposit : Tez_repr.t;
    endorsement_security_deposit : Tez_repr.t;
    baking_reward_per_endorsement : list Tez_repr.t;
    endorsement_reward : list Tez_repr.t;
    cost_per_byte : Tez_repr.t;
    hard_storage_limit_per_operation : Z.t;
    quorum_min : int32;
    quorum_max : int32;
    min_proposal_quorum : int32;
    initial_endorsers : int;
    delay_per_missing_endorsement : Period_repr.t;
    liquidity_baking_subsidy : Tez_repr.t;
    liquidity_baking_sunset_level : int32;
    liquidity_baking_escape_ema_threshold : int32 }.
  Definition with_preserved_cycles preserved_cycles (r : record) :=
    Build preserved_cycles r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(block_security_deposit) r.(endorsement_security_deposit)
      r.(baking_reward_per_endorsement) r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_blocks_per_cycle blocks_per_cycle (r : record) :=
    Build r.(preserved_cycles) blocks_per_cycle r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(block_security_deposit) r.(endorsement_security_deposit)
      r.(baking_reward_per_endorsement) r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_blocks_per_commitment blocks_per_commitment (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) blocks_per_commitment
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(block_security_deposit) r.(endorsement_security_deposit)
      r.(baking_reward_per_endorsement) r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_blocks_per_roll_snapshot blocks_per_roll_snapshot
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      blocks_per_roll_snapshot r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(block_security_deposit) r.(endorsement_security_deposit)
      r.(baking_reward_per_endorsement) r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_blocks_per_voting_period blocks_per_voting_period
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) blocks_per_voting_period
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(block_security_deposit) r.(endorsement_security_deposit)
      r.(baking_reward_per_endorsement) r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_time_between_blocks time_between_blocks (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      time_between_blocks r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(block_security_deposit) r.(endorsement_security_deposit)
      r.(baking_reward_per_endorsement) r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_minimal_block_delay minimal_block_delay (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) minimal_block_delay r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(block_security_deposit) r.(endorsement_security_deposit)
      r.(baking_reward_per_endorsement) r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_endorsers_per_block endorsers_per_block (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) endorsers_per_block
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(block_security_deposit) r.(endorsement_security_deposit)
      r.(baking_reward_per_endorsement) r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_hard_gas_limit_per_operation hard_gas_limit_per_operation
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      hard_gas_limit_per_operation r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(block_security_deposit) r.(endorsement_security_deposit)
      r.(baking_reward_per_endorsement) r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_hard_gas_limit_per_block hard_gas_limit_per_block
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) hard_gas_limit_per_block
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(block_security_deposit) r.(endorsement_security_deposit)
      r.(baking_reward_per_endorsement) r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_proof_of_work_threshold proof_of_work_threshold
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      proof_of_work_threshold r.(tokens_per_roll) r.(seed_nonce_revelation_tip)
      r.(origination_size) r.(block_security_deposit)
      r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
      r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_tokens_per_roll tokens_per_roll (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) tokens_per_roll r.(seed_nonce_revelation_tip)
      r.(origination_size) r.(block_security_deposit)
      r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
      r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_seed_nonce_revelation_tip seed_nonce_revelation_tip
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll) seed_nonce_revelation_tip
      r.(origination_size) r.(block_security_deposit)
      r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
      r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_origination_size origination_size (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) origination_size r.(block_security_deposit)
      r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
      r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_block_security_deposit block_security_deposit (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size) block_security_deposit
      r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
      r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_endorsement_security_deposit endorsement_security_deposit
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(block_security_deposit) endorsement_security_deposit
      r.(baking_reward_per_endorsement) r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_baking_reward_per_endorsement baking_reward_per_endorsement
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(block_security_deposit) r.(endorsement_security_deposit)
      baking_reward_per_endorsement r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_endorsement_reward endorsement_reward (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(block_security_deposit) r.(endorsement_security_deposit)
      r.(baking_reward_per_endorsement) endorsement_reward r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_cost_per_byte cost_per_byte (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(block_security_deposit) r.(endorsement_security_deposit)
      r.(baking_reward_per_endorsement) r.(endorsement_reward) cost_per_byte
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_hard_storage_limit_per_operation
    hard_storage_limit_per_operation (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(block_security_deposit) r.(endorsement_security_deposit)
      r.(baking_reward_per_endorsement) r.(endorsement_reward) r.(cost_per_byte)
      hard_storage_limit_per_operation r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_quorum_min quorum_min (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(block_security_deposit) r.(endorsement_security_deposit)
      r.(baking_reward_per_endorsement) r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) quorum_min r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_quorum_max quorum_max (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(block_security_deposit) r.(endorsement_security_deposit)
      r.(baking_reward_per_endorsement) r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) quorum_max
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_min_proposal_quorum min_proposal_quorum (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(block_security_deposit) r.(endorsement_security_deposit)
      r.(baking_reward_per_endorsement) r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      min_proposal_quorum r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_initial_endorsers initial_endorsers (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(block_security_deposit) r.(endorsement_security_deposit)
      r.(baking_reward_per_endorsement) r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) initial_endorsers
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_delay_per_missing_endorsement delay_per_missing_endorsement
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(block_security_deposit) r.(endorsement_security_deposit)
      r.(baking_reward_per_endorsement) r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      delay_per_missing_endorsement r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_liquidity_baking_subsidy liquidity_baking_subsidy
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(block_security_deposit) r.(endorsement_security_deposit)
      r.(baking_reward_per_endorsement) r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) liquidity_baking_subsidy
      r.(liquidity_baking_sunset_level)
      r.(liquidity_baking_escape_ema_threshold).
  Definition with_liquidity_baking_sunset_level liquidity_baking_sunset_level
    (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(block_security_deposit) r.(endorsement_security_deposit)
      r.(baking_reward_per_endorsement) r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      liquidity_baking_sunset_level r.(liquidity_baking_escape_ema_threshold).
  Definition with_liquidity_baking_escape_ema_threshold
    liquidity_baking_escape_ema_threshold (r : record) :=
    Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
      r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
      r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
      r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
      r.(proof_of_work_threshold) r.(tokens_per_roll)
      r.(seed_nonce_revelation_tip) r.(origination_size)
      r.(block_security_deposit) r.(endorsement_security_deposit)
      r.(baking_reward_per_endorsement) r.(endorsement_reward) r.(cost_per_byte)
      r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
      r.(min_proposal_quorum) r.(initial_endorsers)
      r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
      r.(liquidity_baking_sunset_level) liquidity_baking_escape_ema_threshold.
End parametric.
Definition parametric := parametric.record.

Definition parametric_encoding : Data_encoding.encoding parametric :=
  Data_encoding.conv
    (fun (c : parametric) =>
      ((c.(parametric.preserved_cycles), c.(parametric.blocks_per_cycle),
        c.(parametric.blocks_per_commitment),
        c.(parametric.blocks_per_roll_snapshot),
        c.(parametric.blocks_per_voting_period),
        c.(parametric.time_between_blocks), c.(parametric.endorsers_per_block),
        c.(parametric.hard_gas_limit_per_operation),
        c.(parametric.hard_gas_limit_per_block),
        c.(parametric.proof_of_work_threshold)),
        ((c.(parametric.tokens_per_roll),
          c.(parametric.seed_nonce_revelation_tip),
          c.(parametric.origination_size),
          c.(parametric.block_security_deposit),
          c.(parametric.endorsement_security_deposit),
          c.(parametric.baking_reward_per_endorsement),
          c.(parametric.endorsement_reward), c.(parametric.cost_per_byte),
          c.(parametric.hard_storage_limit_per_operation)),
          (c.(parametric.quorum_min), c.(parametric.quorum_max),
            c.(parametric.min_proposal_quorum),
            c.(parametric.initial_endorsers),
            c.(parametric.delay_per_missing_endorsement),
            c.(parametric.minimal_block_delay),
            c.(parametric.liquidity_baking_subsidy),
            c.(parametric.liquidity_baking_sunset_level),
            c.(parametric.liquidity_baking_escape_ema_threshold)))))
    (fun (function_parameter :
      (int * int32 * int32 * int32 * int32 * list Period_repr.t * int *
        Gas_limit_repr.Arith.integral * Gas_limit_repr.Arith.integral * int64) *
        ((Tez_repr.t * Tez_repr.t * int * Tez_repr.t * Tez_repr.t *
          list Tez_repr.t * list Tez_repr.t * Tez_repr.t * Z.t) *
          (int32 * int32 * int32 * int * Period_repr.t * Period_repr.t *
            Tez_repr.t * int32 * int32))) =>
      let
        '((preserved_cycles, blocks_per_cycle, blocks_per_commitment,
          blocks_per_roll_snapshot, blocks_per_voting_period,
          time_between_blocks, endorsers_per_block,
          hard_gas_limit_per_operation, hard_gas_limit_per_block,
          proof_of_work_threshold),
          ((tokens_per_roll, seed_nonce_revelation_tip, origination_size,
            block_security_deposit, endorsement_security_deposit,
            baking_reward_per_endorsement, endorsement_reward, cost_per_byte,
            hard_storage_limit_per_operation),
            (quorum_min, quorum_max, min_proposal_quorum, initial_endorsers,
              delay_per_missing_endorsement, minimal_block_delay,
              liquidity_baking_subsidy, liquidity_baking_sunset_level,
              liquidity_baking_escape_ema_threshold))) := function_parameter in
      {| parametric.preserved_cycles := preserved_cycles;
        parametric.blocks_per_cycle := blocks_per_cycle;
        parametric.blocks_per_commitment := blocks_per_commitment;
        parametric.blocks_per_roll_snapshot := blocks_per_roll_snapshot;
        parametric.blocks_per_voting_period := blocks_per_voting_period;
        parametric.time_between_blocks := time_between_blocks;
        parametric.minimal_block_delay := minimal_block_delay;
        parametric.endorsers_per_block := endorsers_per_block;
        parametric.hard_gas_limit_per_operation := hard_gas_limit_per_operation;
        parametric.hard_gas_limit_per_block := hard_gas_limit_per_block;
        parametric.proof_of_work_threshold := proof_of_work_threshold;
        parametric.tokens_per_roll := tokens_per_roll;
        parametric.seed_nonce_revelation_tip := seed_nonce_revelation_tip;
        parametric.origination_size := origination_size;
        parametric.block_security_deposit := block_security_deposit;
        parametric.endorsement_security_deposit := endorsement_security_deposit;
        parametric.baking_reward_per_endorsement :=
          baking_reward_per_endorsement;
        parametric.endorsement_reward := endorsement_reward;
        parametric.cost_per_byte := cost_per_byte;
        parametric.hard_storage_limit_per_operation :=
          hard_storage_limit_per_operation; parametric.quorum_min := quorum_min;
        parametric.quorum_max := quorum_max;
        parametric.min_proposal_quorum := min_proposal_quorum;
        parametric.initial_endorsers := initial_endorsers;
        parametric.delay_per_missing_endorsement :=
          delay_per_missing_endorsement;
        parametric.liquidity_baking_subsidy := liquidity_baking_subsidy;
        parametric.liquidity_baking_sunset_level :=
          liquidity_baking_sunset_level;
        parametric.liquidity_baking_escape_ema_threshold :=
          liquidity_baking_escape_ema_threshold |}) None
    (Data_encoding.merge_objs
      (Data_encoding.obj10
        (Data_encoding.req None None "preserved_cycles" Data_encoding.uint8)
        (Data_encoding.req None None "blocks_per_cycle"
          Data_encoding.int32_value)
        (Data_encoding.req None None "blocks_per_commitment"
          Data_encoding.int32_value)
        (Data_encoding.req None None "blocks_per_roll_snapshot"
          Data_encoding.int32_value)
        (Data_encoding.req None None "blocks_per_voting_period"
          Data_encoding.int32_value)
        (Data_encoding.req None None "time_between_blocks"
          (Data_encoding.list_value None Period_repr.encoding))
        (Data_encoding.req None None "endorsers_per_block" Data_encoding.uint16)
        (Data_encoding.req None None "hard_gas_limit_per_operation"
          Gas_limit_repr.Arith.z_integral_encoding)
        (Data_encoding.req None None "hard_gas_limit_per_block"
          Gas_limit_repr.Arith.z_integral_encoding)
        (Data_encoding.req None None "proof_of_work_threshold"
          Data_encoding.int64_value))
      (Data_encoding.merge_objs
        (Data_encoding.obj9
          (Data_encoding.req None None "tokens_per_roll" Tez_repr.encoding)
          (Data_encoding.req None None "seed_nonce_revelation_tip"
            Tez_repr.encoding)
          (Data_encoding.req None None "origination_size" Data_encoding.int31)
          (Data_encoding.req None None "block_security_deposit"
            Tez_repr.encoding)
          (Data_encoding.req None None "endorsement_security_deposit"
            Tez_repr.encoding)
          (Data_encoding.req None None "baking_reward_per_endorsement"
            (Data_encoding.list_value None Tez_repr.encoding))
          (Data_encoding.req None None "endorsement_reward"
            (Data_encoding.list_value None Tez_repr.encoding))
          (Data_encoding.req None None "cost_per_byte" Tez_repr.encoding)
          (Data_encoding.req None None "hard_storage_limit_per_operation"
            Data_encoding.z))
        (Data_encoding.obj9
          (Data_encoding.req None None "quorum_min" Data_encoding.int32_value)
          (Data_encoding.req None None "quorum_max" Data_encoding.int32_value)
          (Data_encoding.req None None "min_proposal_quorum"
            Data_encoding.int32_value)
          (Data_encoding.req None None "initial_endorsers" Data_encoding.uint16)
          (Data_encoding.req None None "delay_per_missing_endorsement"
            Period_repr.encoding)
          (Data_encoding.req None None "minimal_block_delay"
            Period_repr.encoding)
          (Data_encoding.req None None "liquidity_baking_subsidy"
            Tez_repr.encoding)
          (Data_encoding.req None None "liquidity_baking_sunset_level"
            Data_encoding.int32_value)
          (Data_encoding.req None None "liquidity_baking_escape_ema_threshold"
            Data_encoding.int32_value)))).

Module t.
  Record record : Set := Build {
    fixed : fixed;
    parametric : parametric }.
  Definition with_fixed fixed (r : record) :=
    Build fixed r.(parametric).
  Definition with_parametric parametric (r : record) :=
    Build r.(fixed) parametric.
End t.
Definition t := t.record.

Definition encoding : Data_encoding.encoding t :=
  Data_encoding.conv
    (fun (function_parameter : t) =>
      let '{| t.fixed := fixed_value; t.parametric := parametric_value |} :=
        function_parameter in
      (fixed_value, parametric_value))
    (fun (function_parameter : fixed * parametric) =>
      let '(fixed_value, parametric_value) := function_parameter in
      {| t.fixed := fixed_value; t.parametric := parametric_value |}) None
    (Data_encoding.merge_objs fixed_encoding parametric_encoding).

(** Init function; without side-effects in Coq *)
Definition init_module_repr : unit :=
  Error_monad.register_error_kind Error_monad.Permanent
    "constants.invalid_protocol_constants" "Invalid protocol constants"
    "The provided protocol constants are not coherent."
    (Some
      (fun (ppf : Format.formatter) =>
        fun (reason : string) =>
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal
                "Invalid protocol constants: "
                (CamlinternalFormatBasics.String
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.End_of_format))
              "Invalid protocol constants: %s") reason))
    (Data_encoding.obj1
      (Data_encoding.req None None "reason" Data_encoding.string_value))
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Invalid_protocol_constants" then
          let 'reason := cast string payload in
          Some reason
        else None
      end)
    (fun (reason : string) =>
      Build_extensible "Invalid_protocol_constants" string reason).

Definition check_constants (constants : parametric) : M? unit :=
  let min_time_between_blocks :=
    match constants.(parametric.time_between_blocks) with
    | cons first_time_between_blocks _ => first_time_between_blocks
    | [] => Period_repr.one_minute
    end in
  let? '_ :=
    Error_monad.error_unless
      ((Period_repr.to_seconds min_time_between_blocks) >=i64
      (Period_repr.to_seconds constants.(parametric.minimal_block_delay)))
      (Build_extensible "Invalid_protocol_constants" string
        (Format.asprintf
          (CamlinternalFormatBasics.Format
            (CamlinternalFormatBasics.String_literal
              "minimal_block_delay value ("
              (CamlinternalFormatBasics.Int64 CamlinternalFormatBasics.Int_d
                CamlinternalFormatBasics.No_padding
                CamlinternalFormatBasics.No_precision
                (CamlinternalFormatBasics.String_literal
                  ") should be smaller than time_between_blocks[0] value ("
                  (CamlinternalFormatBasics.Int64 CamlinternalFormatBasics.Int_d
                    CamlinternalFormatBasics.No_padding
                    CamlinternalFormatBasics.No_precision
                    (CamlinternalFormatBasics.Char_literal ")" % char
                      CamlinternalFormatBasics.End_of_format)))))
            "minimal_block_delay value (%Ld) should be smaller than time_between_blocks[0] value (%Ld)")
          (Period_repr.to_seconds constants.(parametric.minimal_block_delay))
          (Period_repr.to_seconds min_time_between_blocks))) in
  Error_monad.error_unless
    (constants.(parametric.endorsers_per_block) >=i
    constants.(parametric.initial_endorsers))
    (Build_extensible "Invalid_protocol_constants" string
      "initial_endorsers should be smaller than endorsers_per_block").

Module Proto_previous.
  Module parametric.
    Record record : Set := Build {
      preserved_cycles : int;
      blocks_per_cycle : int32;
      blocks_per_commitment : int32;
      blocks_per_roll_snapshot : int32;
      blocks_per_voting_period : int32;
      time_between_blocks : list Period_repr.t;
      minimal_block_delay : Period_repr.t;
      endorsers_per_block : int;
      hard_gas_limit_per_operation : Gas_limit_repr.Arith.integral;
      hard_gas_limit_per_block : Gas_limit_repr.Arith.integral;
      proof_of_work_threshold : int64;
      tokens_per_roll : Tez_repr.t;
      michelson_maximum_type_size : int;
      seed_nonce_revelation_tip : Tez_repr.t;
      origination_size : int;
      block_security_deposit : Tez_repr.t;
      endorsement_security_deposit : Tez_repr.t;
      baking_reward_per_endorsement : list Tez_repr.t;
      endorsement_reward : list Tez_repr.t;
      cost_per_byte : Tez_repr.t;
      hard_storage_limit_per_operation : Z.t;
      quorum_min : int32;
      quorum_max : int32;
      min_proposal_quorum : int32;
      initial_endorsers : int;
      delay_per_missing_endorsement : Period_repr.t;
      liquidity_baking_subsidy : Tez_repr.t;
      liquidity_baking_sunset_level : int32;
      liquidity_baking_escape_ema_threshold : int32 }.
    Definition with_preserved_cycles preserved_cycles (r : record) :=
      Build preserved_cycles r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_blocks_per_cycle blocks_per_cycle (r : record) :=
      Build r.(preserved_cycles) blocks_per_cycle r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_blocks_per_commitment blocks_per_commitment (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) blocks_per_commitment
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_blocks_per_roll_snapshot blocks_per_roll_snapshot
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        blocks_per_roll_snapshot r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_blocks_per_voting_period blocks_per_voting_period
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) blocks_per_voting_period
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_time_between_blocks time_between_blocks (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        time_between_blocks r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_minimal_block_delay minimal_block_delay (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) minimal_block_delay r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_endorsers_per_block endorsers_per_block (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) endorsers_per_block
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_hard_gas_limit_per_operation hard_gas_limit_per_operation
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        hard_gas_limit_per_operation r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_hard_gas_limit_per_block hard_gas_limit_per_block
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) hard_gas_limit_per_block
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_proof_of_work_threshold proof_of_work_threshold
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        proof_of_work_threshold r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_tokens_per_roll tokens_per_roll (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) tokens_per_roll
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_michelson_maximum_type_size michelson_maximum_type_size
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        michelson_maximum_type_size r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_seed_nonce_revelation_tip seed_nonce_revelation_tip
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) seed_nonce_revelation_tip
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_origination_size origination_size (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        origination_size r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_block_security_deposit block_security_deposit
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) block_security_deposit
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_endorsement_security_deposit endorsement_security_deposit
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        endorsement_security_deposit r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_baking_reward_per_endorsement baking_reward_per_endorsement
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) baking_reward_per_endorsement
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_endorsement_reward endorsement_reward (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        endorsement_reward r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_cost_per_byte cost_per_byte (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) cost_per_byte
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_hard_storage_limit_per_operation
      hard_storage_limit_per_operation (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        hard_storage_limit_per_operation r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_quorum_min quorum_min (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) quorum_min r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_quorum_max quorum_max (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) quorum_max
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_min_proposal_quorum min_proposal_quorum (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        min_proposal_quorum r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_initial_endorsers initial_endorsers (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) initial_endorsers
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_delay_per_missing_endorsement delay_per_missing_endorsement
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        delay_per_missing_endorsement r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_liquidity_baking_subsidy liquidity_baking_subsidy
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) liquidity_baking_subsidy
        r.(liquidity_baking_sunset_level)
        r.(liquidity_baking_escape_ema_threshold).
    Definition with_liquidity_baking_sunset_level liquidity_baking_sunset_level
      (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        liquidity_baking_sunset_level r.(liquidity_baking_escape_ema_threshold).
    Definition with_liquidity_baking_escape_ema_threshold
      liquidity_baking_escape_ema_threshold (r : record) :=
      Build r.(preserved_cycles) r.(blocks_per_cycle) r.(blocks_per_commitment)
        r.(blocks_per_roll_snapshot) r.(blocks_per_voting_period)
        r.(time_between_blocks) r.(minimal_block_delay) r.(endorsers_per_block)
        r.(hard_gas_limit_per_operation) r.(hard_gas_limit_per_block)
        r.(proof_of_work_threshold) r.(tokens_per_roll)
        r.(michelson_maximum_type_size) r.(seed_nonce_revelation_tip)
        r.(origination_size) r.(block_security_deposit)
        r.(endorsement_security_deposit) r.(baking_reward_per_endorsement)
        r.(endorsement_reward) r.(cost_per_byte)
        r.(hard_storage_limit_per_operation) r.(quorum_min) r.(quorum_max)
        r.(min_proposal_quorum) r.(initial_endorsers)
        r.(delay_per_missing_endorsement) r.(liquidity_baking_subsidy)
        r.(liquidity_baking_sunset_level) liquidity_baking_escape_ema_threshold.
  End parametric.
  Definition parametric := parametric.record.
  
  Definition parametric_encoding : Data_encoding.encoding parametric :=
    Data_encoding.conv
      (fun (c : parametric) =>
        ((c.(parametric.preserved_cycles), c.(parametric.blocks_per_cycle),
          c.(parametric.blocks_per_commitment),
          c.(parametric.blocks_per_roll_snapshot),
          c.(parametric.blocks_per_voting_period),
          c.(parametric.time_between_blocks),
          c.(parametric.endorsers_per_block),
          c.(parametric.hard_gas_limit_per_operation),
          c.(parametric.hard_gas_limit_per_block),
          c.(parametric.proof_of_work_threshold)),
          ((c.(parametric.tokens_per_roll),
            c.(parametric.michelson_maximum_type_size),
            c.(parametric.seed_nonce_revelation_tip),
            c.(parametric.origination_size),
            c.(parametric.block_security_deposit),
            c.(parametric.endorsement_security_deposit),
            c.(parametric.baking_reward_per_endorsement),
            c.(parametric.endorsement_reward), c.(parametric.cost_per_byte),
            c.(parametric.hard_storage_limit_per_operation)),
            (c.(parametric.quorum_min), c.(parametric.quorum_max),
              c.(parametric.min_proposal_quorum),
              c.(parametric.initial_endorsers),
              c.(parametric.delay_per_missing_endorsement),
              c.(parametric.minimal_block_delay),
              c.(parametric.liquidity_baking_subsidy),
              c.(parametric.liquidity_baking_sunset_level),
              c.(parametric.liquidity_baking_escape_ema_threshold)))))
      (fun (function_parameter :
        (int * int32 * int32 * int32 * int32 * list Period_repr.t * int *
          Gas_limit_repr.Arith.integral * Gas_limit_repr.Arith.integral * int64)
          *
          ((Tez_repr.t * int * Tez_repr.t * int * Tez_repr.t * Tez_repr.t *
            list Tez_repr.t * list Tez_repr.t * Tez_repr.t * Z.t) *
            (int32 * int32 * int32 * int * Period_repr.t * Period_repr.t *
              Tez_repr.t * int32 * int32))) =>
        let
          '((preserved_cycles, blocks_per_cycle, blocks_per_commitment,
            blocks_per_roll_snapshot, blocks_per_voting_period,
            time_between_blocks, endorsers_per_block,
            hard_gas_limit_per_operation, hard_gas_limit_per_block,
            proof_of_work_threshold),
            ((tokens_per_roll, michelson_maximum_type_size,
              seed_nonce_revelation_tip, origination_size,
              block_security_deposit, endorsement_security_deposit,
              baking_reward_per_endorsement, endorsement_reward, cost_per_byte,
              hard_storage_limit_per_operation),
              (quorum_min, quorum_max, min_proposal_quorum, initial_endorsers,
                delay_per_missing_endorsement, minimal_block_delay,
                liquidity_baking_subsidy, liquidity_baking_sunset_level,
                liquidity_baking_escape_ema_threshold))) := function_parameter
          in
        {| parametric.preserved_cycles := preserved_cycles;
          parametric.blocks_per_cycle := blocks_per_cycle;
          parametric.blocks_per_commitment := blocks_per_commitment;
          parametric.blocks_per_roll_snapshot := blocks_per_roll_snapshot;
          parametric.blocks_per_voting_period := blocks_per_voting_period;
          parametric.time_between_blocks := time_between_blocks;
          parametric.minimal_block_delay := minimal_block_delay;
          parametric.endorsers_per_block := endorsers_per_block;
          parametric.hard_gas_limit_per_operation :=
            hard_gas_limit_per_operation;
          parametric.hard_gas_limit_per_block := hard_gas_limit_per_block;
          parametric.proof_of_work_threshold := proof_of_work_threshold;
          parametric.tokens_per_roll := tokens_per_roll;
          parametric.michelson_maximum_type_size := michelson_maximum_type_size;
          parametric.seed_nonce_revelation_tip := seed_nonce_revelation_tip;
          parametric.origination_size := origination_size;
          parametric.block_security_deposit := block_security_deposit;
          parametric.endorsement_security_deposit :=
            endorsement_security_deposit;
          parametric.baking_reward_per_endorsement :=
            baking_reward_per_endorsement;
          parametric.endorsement_reward := endorsement_reward;
          parametric.cost_per_byte := cost_per_byte;
          parametric.hard_storage_limit_per_operation :=
            hard_storage_limit_per_operation;
          parametric.quorum_min := quorum_min;
          parametric.quorum_max := quorum_max;
          parametric.min_proposal_quorum := min_proposal_quorum;
          parametric.initial_endorsers := initial_endorsers;
          parametric.delay_per_missing_endorsement :=
            delay_per_missing_endorsement;
          parametric.liquidity_baking_subsidy := liquidity_baking_subsidy;
          parametric.liquidity_baking_sunset_level :=
            liquidity_baking_sunset_level;
          parametric.liquidity_baking_escape_ema_threshold :=
            liquidity_baking_escape_ema_threshold |}) None
      (Data_encoding.merge_objs
        (Data_encoding.obj10
          (Data_encoding.req None None "preserved_cycles" Data_encoding.uint8)
          (Data_encoding.req None None "blocks_per_cycle"
            Data_encoding.int32_value)
          (Data_encoding.req None None "blocks_per_commitment"
            Data_encoding.int32_value)
          (Data_encoding.req None None "blocks_per_roll_snapshot"
            Data_encoding.int32_value)
          (Data_encoding.req None None "blocks_per_voting_period"
            Data_encoding.int32_value)
          (Data_encoding.req None None "time_between_blocks"
            (Data_encoding.list_value None Period_repr.encoding))
          (Data_encoding.req None None "endorsers_per_block"
            Data_encoding.uint16)
          (Data_encoding.req None None "hard_gas_limit_per_operation"
            Gas_limit_repr.Arith.z_integral_encoding)
          (Data_encoding.req None None "hard_gas_limit_per_block"
            Gas_limit_repr.Arith.z_integral_encoding)
          (Data_encoding.req None None "proof_of_work_threshold"
            Data_encoding.int64_value))
        (Data_encoding.merge_objs
          (Data_encoding.obj10
            (Data_encoding.req None None "tokens_per_roll" Tez_repr.encoding)
            (Data_encoding.req None None "michelson_maximum_type_size"
              Data_encoding.uint16)
            (Data_encoding.req None None "seed_nonce_revelation_tip"
              Tez_repr.encoding)
            (Data_encoding.req None None "origination_size" Data_encoding.int31)
            (Data_encoding.req None None "block_security_deposit"
              Tez_repr.encoding)
            (Data_encoding.req None None "endorsement_security_deposit"
              Tez_repr.encoding)
            (Data_encoding.req None None "baking_reward_per_endorsement"
              (Data_encoding.list_value None Tez_repr.encoding))
            (Data_encoding.req None None "endorsement_reward"
              (Data_encoding.list_value None Tez_repr.encoding))
            (Data_encoding.req None None "cost_per_byte" Tez_repr.encoding)
            (Data_encoding.req None None "hard_storage_limit_per_operation"
              Data_encoding.z))
          (Data_encoding.obj9
            (Data_encoding.req None None "quorum_min" Data_encoding.int32_value)
            (Data_encoding.req None None "quorum_max" Data_encoding.int32_value)
            (Data_encoding.req None None "min_proposal_quorum"
              Data_encoding.int32_value)
            (Data_encoding.req None None "initial_endorsers"
              Data_encoding.uint16)
            (Data_encoding.req None None "delay_per_missing_endorsement"
              Period_repr.encoding)
            (Data_encoding.req None None "minimal_block_delay"
              Period_repr.encoding)
            (Data_encoding.req None None "liquidity_baking_subsidy"
              Tez_repr.encoding)
            (Data_encoding.req None None "liquidity_baking_sunset_level"
              Data_encoding.int32_value)
            (Data_encoding.req None None "liquidity_baking_escape_ema_threshold"
              Data_encoding.int32_value)))).
End Proto_previous.
