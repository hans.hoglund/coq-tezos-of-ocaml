Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Raw_context.

Definition current : Raw_context.t -> Int64.t := Raw_context.current_fitness.

Definition increase (ctxt : Raw_context.t) : Raw_context.t :=
  let fitness := current ctxt in
  Raw_context.set_current_fitness ctxt (Int64.succ fitness).
