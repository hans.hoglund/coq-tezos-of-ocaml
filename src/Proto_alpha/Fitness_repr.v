Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Constants_repr.

(** Init function; without side-effects in Coq *)
Definition init_module_repr : unit :=
  Error_monad.register_error_kind Error_monad.Permanent "invalid_fitness"
    "Invalid fitness" "Fitness representation should be exactly 8 bytes long."
    (Some
      (fun (ppf : Format.formatter) =>
        fun (function_parameter : unit) =>
          let '_ := function_parameter in
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal "Invalid fitness"
                CamlinternalFormatBasics.End_of_format) "Invalid fitness")))
    Data_encoding.empty
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Invalid_fitness" then
          Some tt
        else None
      end)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Build_extensible "Invalid_fitness" unit tt).

Definition int64_to_bytes (i : int64) : bytes :=
  let b_value := Bytes.make 8 "0" % char in
  let '_ := TzEndian.set_int64 b_value 0 i in
  b_value.

Definition int64_of_bytes (b_value : bytes) : M? int64 :=
  if (Bytes.length b_value) <>i 8 then
    Error_monad.error_value (Build_extensible "Invalid_fitness" unit tt)
  else
    return? (TzEndian.get_int64 b_value 0).

Definition from_int64 (fitness : int64) : list bytes :=
  [ Bytes.of_string Constants_repr.version_number; int64_to_bytes fitness ].

Definition to_int64 (function_parameter : list bytes) : M? int64 :=
  match
    (function_parameter,
      match function_parameter with
      | cons version (cons fitness []) =>
        Compare.String.(Compare.S.op_eq) (Bytes.to_string version)
          Constants_repr.version_number
      | _ => false
      end,
      match function_parameter with
      | cons version (cons _fitness []) =>
        Compare.String.(Compare.S.op_eq) (Bytes.to_string version)
          Constants_repr.version_number_004
      | _ => false
      end) with
  | (cons version (cons fitness []), true, _) => int64_of_bytes fitness
  | (cons version (cons _fitness []), _, true) => return? 0
  | ([], _, _) => return? 0
  | (_, _, _) =>
    Error_monad.error_value (Build_extensible "Invalid_fitness" unit tt)
  end.
