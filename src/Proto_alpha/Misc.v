Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.
Unset Positivity Checking.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Definition lazyt (a : Set) : Set := unit -> a.

Inductive lazy_list_t (a : Set) : Set :=
| LCons : a -> lazyt (M=? (lazy_list_t a)) -> lazy_list_t a.

Arguments LCons {_}.

Definition lazy_list (a : Set) : Set := M=? (lazy_list_t a).

Fixpoint op_minusminusgt (i : int) (j : int) {struct i} : list int :=
  if i >i j then
    nil
  else
    cons i (op_minusminusgt (Pervasives.succ i) j).

Fixpoint op_minusminusminusgt (i : int32) (j : int32) {struct i} : list int32 :=
  if i >i32 j then
    nil
  else
    cons i (op_minusminusminusgt (Int32.succ i) j).

Axiom split : ascii -> option int -> string -> list string.

Definition pp_print_paragraph (ppf : Format.formatter) (description : string)
  : unit :=
  Format.fprintf ppf
    (CamlinternalFormatBasics.Format
      (CamlinternalFormatBasics.Formatting_gen
        (CamlinternalFormatBasics.Open_box
          (CamlinternalFormatBasics.Format
            CamlinternalFormatBasics.End_of_format ""))
        (CamlinternalFormatBasics.Alpha
          (CamlinternalFormatBasics.Formatting_lit
            CamlinternalFormatBasics.Close_box
            CamlinternalFormatBasics.End_of_format))) "@[%a@]")
    (Format.pp_print_list (Some Format.pp_print_space) Format.pp_print_string)
    (split " " % char None description).

Definition take {A : Set} (n : int) (l_value : list A)
  : option (list A * list A) :=
  let fix loop {B : Set} (acc_value : list B) (n : int) (xs : list B)
    : option (list B * list B) :=
    if n <=i 0 then
      Some ((List.rev acc_value), xs)
    else
      match xs with
      | [] => None
      | cons x xs => loop (cons x acc_value) (n -i 1) xs
      end in
  loop nil n l_value.

Definition remove_prefix (prefix : string) (s : string) : option string :=
  let x := String.length prefix in
  let n := String.length s in
  if
    (n >=i x) && (Compare.String.(Compare.S.op_eq) (String.sub s 0 x) prefix)
  then
    Some (String.sub s x (n -i x))
  else
    None.

Fixpoint remove_elem_from_list {A : Set}
  (nb : int) (function_parameter : list A) : list A :=
  match
    (function_parameter,
      match function_parameter with
      | (cons _ _) as l_value => nb <=i 0
      | _ => false
      end) with
  | ([], _) => nil
  | ((cons _ _) as l_value, true) => l_value
  | (cons _ tl, _) => remove_elem_from_list (nb -i 1) tl
  end.
