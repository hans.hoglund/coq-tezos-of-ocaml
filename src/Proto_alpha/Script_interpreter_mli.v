Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Script_ir_translator_mli.
  Module Script_ir_translator := Script_ir_translator_mli.
Require TezosOfOCaml.Proto_alpha.Script_typed_ir.

Module execution_result.
  Record record : Set := Build {
    ctxt : Alpha_context.context;
    storage : Alpha_context.Script.expr;
    lazy_storage_diff : option Alpha_context.Lazy_storage.diffs;
    operations : list Alpha_context.packed_internal_operation }.
  Definition with_ctxt ctxt (r : record) :=
    Build ctxt r.(storage) r.(lazy_storage_diff) r.(operations).
  Definition with_storage storage (r : record) :=
    Build r.(ctxt) storage r.(lazy_storage_diff) r.(operations).
  Definition with_lazy_storage_diff lazy_storage_diff (r : record) :=
    Build r.(ctxt) r.(storage) lazy_storage_diff r.(operations).
  Definition with_operations operations (r : record) :=
    Build r.(ctxt) r.(storage) r.(lazy_storage_diff) operations.
End execution_result.
Definition execution_result := execution_result.record.

Module step_constants.
  Record record : Set := Build {
    source : Alpha_context.Contract.t;
    payer : Alpha_context.Contract.t;
    self : Alpha_context.Contract.t;
    amount : Alpha_context.Tez.t;
    chain_id : Chain_id.t }.
  Definition with_source source (r : record) :=
    Build source r.(payer) r.(self) r.(amount) r.(chain_id).
  Definition with_payer payer (r : record) :=
    Build r.(source) payer r.(self) r.(amount) r.(chain_id).
  Definition with_self self (r : record) :=
    Build r.(source) r.(payer) self r.(amount) r.(chain_id).
  Definition with_amount amount (r : record) :=
    Build r.(source) r.(payer) r.(self) amount r.(chain_id).
  Definition with_chain_id chain_id (r : record) :=
    Build r.(source) r.(payer) r.(self) r.(amount) chain_id.
End step_constants.
Definition step_constants := step_constants.record.

Parameter step : forall {a f r s : Set},
  option Script_typed_ir.logger -> Alpha_context.context -> step_constants ->
  Script_typed_ir.kdescr -> a -> s -> M=? (r * f * Alpha_context.context).

Parameter execute :
  option Script_typed_ir.logger -> Alpha_context.t ->
  option Script_ir_translator.ex_script ->
  Script_ir_translator.unparsing_mode -> step_constants ->
  Alpha_context.Script.t -> string -> Alpha_context.Script.expr -> bool ->
  M=? (execution_result * (Script_ir_translator.ex_script * int)).

Parameter kstep : forall {a f r s : Set},
  option Script_typed_ir.logger -> Alpha_context.context -> step_constants ->
  Script_typed_ir.kinstr -> a -> s -> M=? (r * f * Alpha_context.context).

Module Internals.
  Definition local_gas_counter : Set := int.
  
  Inductive outdated_context : Set :=
  | OutDatedContext : Alpha_context.context -> outdated_context.
  
  Parameter next : forall {a f r s : Set},
    option Script_typed_ir.logger -> outdated_context * step_constants ->
    local_gas_counter -> Script_typed_ir.continuation -> a -> s ->
    M=? (r * f * outdated_context * local_gas_counter).
  
  Parameter step : forall {a f r s : Set},
    outdated_context * step_constants -> local_gas_counter ->
    Script_typed_ir.kinstr -> a -> s ->
    M=? (r * f * outdated_context * local_gas_counter).
End Internals.
