Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Gas_limit_repr.
Require TezosOfOCaml.Proto_alpha.Saturation_repr.

Module S := Saturation_repr.

Definition minimal_size_of_typed_contract_in_bytes : int := 688.

Definition approximate_cardinal (bytes_value : int) : S.t :=
  S.safe_int (bytes_value /i minimal_size_of_typed_contract_in_bytes).

Definition log2 (x : S.t) : S.t := S.safe_int (1 +i (S.numbits x)).

Definition cache_update_constant : S.t := S.safe_int 600.

Definition cache_update_coeff : S.t := S.safe_int 57.

Definition cache_update (cache_size_in_bytes : int) : Gas_limit_repr.cost :=
  let approx_card := approximate_cardinal cache_size_in_bytes in
  Gas_limit_repr.atomic_step_cost
    (S.add cache_update_constant (S.mul cache_update_coeff (log2 approx_card))).

Definition cache_find : int -> Gas_limit_repr.cost := cache_update.
