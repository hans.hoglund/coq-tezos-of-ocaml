Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Inductive n : Set :=
| Natural_tag : n.

Inductive z : Set :=
| Integer_tag : z.

Definition num : Set := Z.t.

Definition compare (x : Z.t) (y : Z.t) : int := Z.compare x y.

Definition zero : Z.t := Z.zero.

Definition zero_n : Z.t := Z.zero.

Definition one_n : Z.t := Z.one.

Definition to_string (x : Z.t) : string := Z.to_string x.

Definition of_string (s : string) : option Z.t :=
  Option.catch None
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Z.of_string s).

Definition of_int32 (n : int32) : Z.t := Z.of_int64 (Int64.of_int32 n).

Definition to_int64 (x : Z.t) : option int64 :=
  Option.catch None
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Z.to_int64 x).

Definition of_int64 (n : int64) : Z.t := Z.of_int64 n.

Definition to_int (x : Z.t) : option int :=
  Option.catch None
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Z.to_int x).

Definition of_int (n : int) : Z.t := Z.of_int n.

Definition of_zint {A : Set} (x : A) : A := x.

Definition to_zint {A : Set} (x : A) : A := x.

Definition add (x : Z.t) (y : Z.t) : Z.t := x +Z y.

Definition sub (x : Z.t) (y : Z.t) : Z.t := x -Z y.

Definition mul (x : Z.t) (y : Z.t) : Z.t := x *Z y.

Definition ediv (x : Z.t) (y : Z.t) : option (Z.t * Z.t) :=
  Option.catch None
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Z.ediv_rem x y).

Definition add_n : Z.t -> Z.t -> Z.t := add.

Definition succ_n : Z.t -> Z.t := Z.succ.

Definition mul_n : Z.t -> Z.t -> Z.t := mul.

Definition ediv_n : Z.t -> Z.t -> option (Z.t * Z.t) := ediv.

Definition abs (x : Z.t) : Z.t := Z.abs x.

Definition is_nat (x : Z.t) : option Z.t :=
  if x <Z Z.zero then
    None
  else
    Some x.

Definition neg (x : Z.t) : Z.t := Z.neg x.

Definition int_value {A : Set} (x : A) : A := x.

Definition shift_left (x : Z.t) (y : Z.t) : option Z.t :=
  if (Z.compare y (Z.of_int 256)) >i 0 then
    None
  else
    let y := Z.to_int y in
    Some (Z.shift_left x y).

Definition shift_right (x : Z.t) (y : Z.t) : option Z.t :=
  if (Z.compare y (Z.of_int 256)) >i 0 then
    None
  else
    let y := Z.to_int y in
    Some (Z.shift_right x y).

Definition shift_left_n : Z.t -> Z.t -> option Z.t := shift_left.

Definition shift_right_n : Z.t -> Z.t -> option Z.t := shift_right.

Definition logor (x : Z.t) (y : Z.t) : Z.t := Z.logor x y.

Definition logxor (x : Z.t) (y : Z.t) : Z.t := Z.logxor x y.

Definition logand (x : Z.t) (y : Z.t) : Z.t := Z.logand x y.

Definition lognot (x : Z.t) : Z.t := Z.lognot x.
