Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Definition t : Set := int.

Parameter mul_safe : Set.

Parameter may_saturate : Set.

Definition may_saturate_value (x : t) : t := x.

Definition to_int {A : Set} (x : A) : A := x.

Definition op_lt : t -> t -> bool := Compare.Int.(Compare.S.op_lt).

Definition op_lteq : t -> t -> bool := Compare.Int.(Compare.S.op_lteq).

Definition op_gt : t -> t -> bool := Compare.Int.(Compare.S.op_gt).

Definition op_gteq : t -> t -> bool := Compare.Int.(Compare.S.op_gteq).

Definition op_eq : t -> t -> bool := Compare.Int.(Compare.S.op_eq).

Definition equal : t -> t -> bool := op_eq.

Definition op_ltgt : t -> t -> bool := Compare.Int.(Compare.S.op_ltgt).

Definition max (x : t) (y : t) : t :=
  if op_gteq x y then
    x
  else
    y.

Definition min (x : t) (y : t) : t :=
  if op_gteq x y then
    y
  else
    x.

Definition compare : t -> t -> t := Compare.Int.(Compare.S.compare).

Definition saturated : int := Pervasives.max_int.

Definition of_int_opt (t_value : int) : option int :=
  if (op_gteq t_value 0) && (op_lt t_value saturated) then
    Some t_value
  else
    None.

Definition of_z_opt (z : Z.t) : option int :=
  let 'int_value := Z.to_int z in
  of_int_opt int_value.

Definition to_z (x : int) : Z.t := Z.of_int x.

Definition saturate_if_undef (function_parameter : option int) : int :=
  match function_parameter with
  | None => saturated
  | Some x => x
  end.

Definition safe_int (x : int) : int := saturate_if_undef (of_int_opt x).

Definition numbits (x : int) : int :=
  let x : Pervasives.ref int :=
    Pervasives.ref_value x
  in let n : Pervasives.ref int :=
    Pervasives.ref_value 0 in
  let '_ :=
    let y := Pervasives.lsr (Pervasives.op_exclamation x) 32 in
    if op_ltgt y 0 then
      let '_ := Pervasives.op_coloneq n ((Pervasives.op_exclamation n) +i 32) in
      Pervasives.op_coloneq x y
    else
      tt in
  let '_ :=
    let y := Pervasives.lsr (Pervasives.op_exclamation x) 16 in
    if op_ltgt y 0 then
      let '_ := Pervasives.op_coloneq n ((Pervasives.op_exclamation n) +i 16) in
      Pervasives.op_coloneq x y
    else
      tt in
  let '_ :=
    let y := Pervasives.lsr (Pervasives.op_exclamation x) 8 in
    if op_ltgt y 0 then
      let '_ := Pervasives.op_coloneq n ((Pervasives.op_exclamation n) +i 8) in
      Pervasives.op_coloneq x y
    else
      tt in
  let '_ :=
    let y := Pervasives.lsr (Pervasives.op_exclamation x) 4 in
    if op_ltgt y 0 then
      let '_ := Pervasives.op_coloneq n ((Pervasives.op_exclamation n) +i 4) in
      Pervasives.op_coloneq x y
    else
      tt in
  let '_ :=
    let y := Pervasives.lsr (Pervasives.op_exclamation x) 2 in
    if op_ltgt y 0 then
      let '_ := Pervasives.op_coloneq n ((Pervasives.op_exclamation n) +i 2) in
      Pervasives.op_coloneq x y
    else
      tt in
  if op_ltgt (Pervasives.lsr (Pervasives.op_exclamation x) 1) 0 then
    (Pervasives.op_exclamation n) +i 2
  else
    (Pervasives.op_exclamation n) +i (Pervasives.op_exclamation x).

Definition zero : int := 0.

Definition one : int := 1.

Definition small_enough (z : int) : bool :=
  op_eq (Pervasives.land z (-2147483648)) 0.

Definition mul_safe_value (x : int) : option int :=
  if small_enough x then
    Some x
  else
    None.

Definition mul_safe_exn (x : int) : int :=
  if small_enough x then
    x
  else
    Pervasives.failwith
      (Format.sprintf
        (CamlinternalFormatBasics.Format
          (CamlinternalFormatBasics.String_literal "mul_safe_exn: "
            (CamlinternalFormatBasics.Int CamlinternalFormatBasics.Int_d
              CamlinternalFormatBasics.No_padding
              CamlinternalFormatBasics.No_precision
              (CamlinternalFormatBasics.String_literal
                " must be below 2147483648"
                CamlinternalFormatBasics.End_of_format)))
          "mul_safe_exn: %d must be below 2147483648") x).

Definition mul_safe_of_int_exn (x : int) : int :=
  (fun (function_parameter : option int) =>
    match function_parameter with
    | None =>
      Pervasives.failwith
        (Format.sprintf
          (CamlinternalFormatBasics.Format
            (CamlinternalFormatBasics.String_literal "mul_safe_of_int_exn: "
              (CamlinternalFormatBasics.Int CamlinternalFormatBasics.Int_d
                CamlinternalFormatBasics.No_padding
                CamlinternalFormatBasics.No_precision
                (CamlinternalFormatBasics.String_literal
                  " must be below 2147483648"
                  CamlinternalFormatBasics.End_of_format)))
            "mul_safe_of_int_exn: %d must be below 2147483648") x)
    | Some x => x
    end) (Option.bind (of_int_opt x) mul_safe_value).

Definition shift_right (x : int) (y : int) : int := Pervasives.lsr x y.

Definition shift_left (x : int) (y : int) : int :=
  if op_lt (shift_right saturated y) x then
    saturated
  else
    Pervasives.lsl x y.

Definition mul (x : int) (y : int) : int :=
  match x with
  | 0 => 0
  | x =>
    if (small_enough x) && (small_enough y) then
      x *i y
    else
      if y >i (saturated /i x) then
        saturated
      else
        x *i y
  end.

Definition mul_fast (x : int) (y : int) : int := x *i y.

Definition scale_fast (x : int) (y : int) : int :=
  if op_eq x 0 then
    0
  else
    if small_enough y then
      x *i y
    else
      if y >i (saturated /i x) then
        saturated
      else
        x *i y.

Definition add (x : int) (y : int) : int :=
  let z := x +i y in
  if z >=i 0 then
    z
  else
    saturated.

Definition succ (x : int) : int := add one x.

Definition sub (x : int) (y : int) : int :=
  Compare.Int.(Compare.S.max) (x -i y) 0.

Definition sub_opt (x : int) (y : int) : option int :=
  let s := x -i y in
  if s >=i 0 then
    Some s
  else
    None.

Definition erem (x : int) (y : int) : int := Pervasives._mod x y.

Definition ediv (x : int) (y : int) : int := x /i y.

Definition t_to_z_exn (z : Z.t) : int :=
  match of_z_opt z with
  | None =>
    (* ❌ Assert instruction is not handled. *)
    assert int false
  | Some x => x
  end.

Definition z_encoding : Data_encoding.encoding int :=
  Data_encoding.check_size 9
    (Data_encoding.conv to_z t_to_z_exn None Data_encoding.z).

Definition n_encoding : Data_encoding.encoding int :=
  Data_encoding.check_size 9
    (Data_encoding.conv to_z t_to_z_exn None Data_encoding.n).

Definition pp (fmt : Format.formatter) (x : int) : unit :=
  Format.pp_print_int fmt x.
