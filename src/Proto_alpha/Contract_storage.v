Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Contract_repr.
Require TezosOfOCaml.Proto_alpha.Delegate_storage.
Require TezosOfOCaml.Proto_alpha.Lazy_storage_diff.
Require TezosOfOCaml.Proto_alpha.Lazy_storage_kind.
Require TezosOfOCaml.Proto_alpha.Manager_repr.
Require TezosOfOCaml.Proto_alpha.Raw_context.
Require TezosOfOCaml.Proto_alpha.Roll_storage.
Require TezosOfOCaml.Proto_alpha.Script_expr_hash.
Require TezosOfOCaml.Proto_alpha.Script_repr.
Require TezosOfOCaml.Proto_alpha.Storage.
Require TezosOfOCaml.Proto_alpha.Storage_costs.
Require TezosOfOCaml.Proto_alpha.Storage_sigs.
Require TezosOfOCaml.Proto_alpha.Tez_repr.

(** Init function; without side-effects in Coq *)
Definition init_module : unit :=
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "contract.unspendable_contract" "Unspendable contract"
      "An operation tried to spend tokens from an unspendable contract"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (c : Contract_repr.contract) =>
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "The tokens of contract "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal
                      " can only be spent by its script"
                      CamlinternalFormatBasics.End_of_format)))
                "The tokens of contract %a can only be spent by its script")
              Contract_repr.pp c))
      (Data_encoding.obj1
        (Data_encoding.req None None "contract" Contract_repr.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Unspendable_contract" then
            let 'c := cast Contract_repr.contract payload in
            Some c
          else None
        end)
      (fun (c : Contract_repr.contract) =>
        Build_extensible "Unspendable_contract" Contract_repr.contract c) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "contract.balance_too_low" "Balance too low"
      "An operation tried to spend more tokens than the contract has"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter :
            Contract_repr.contract * Tez_repr.t * Tez_repr.t) =>
            let '(c, b_value, a_value) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "Balance of contract "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal " too low ("
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.String_literal ") to spend "
                          (CamlinternalFormatBasics.Alpha
                            CamlinternalFormatBasics.End_of_format))))))
                "Balance of contract %a too low (%a) to spend %a")
              Contract_repr.pp c Tez_repr.pp b_value Tez_repr.pp a_value))
      (Data_encoding.obj3
        (Data_encoding.req None None "contract" Contract_repr.encoding)
        (Data_encoding.req None None "balance" Tez_repr.encoding)
        (Data_encoding.req None None "amount" Tez_repr.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Balance_too_low" then
            let '(c, b_value, a_value) :=
              cast (Contract_repr.contract * Tez_repr.t * Tez_repr.t) payload in
            Some (c, b_value, a_value)
          else None
        end)
      (fun (function_parameter :
        Contract_repr.contract * Tez_repr.t * Tez_repr.t) =>
        let '(c, b_value, a_value) := function_parameter in
        Build_extensible "Balance_too_low"
          (Contract_repr.contract * Tez_repr.t * Tez_repr.t)
          (c, b_value, a_value)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "contract.counter_in_the_future"
      "Invalid counter (not yet reached) in a manager operation"
      "An operation assumed a contract counter in the future"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : Contract_repr.contract * Z.t * Z.t) =>
            let '(contract, exp, found) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "Counter "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal
                      " not yet reached for contract "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.String_literal " (expected "
                          (CamlinternalFormatBasics.Alpha
                            (CamlinternalFormatBasics.Char_literal ")" % char
                              CamlinternalFormatBasics.End_of_format)))))))
                "Counter %a not yet reached for contract %a (expected %a)")
              Z.pp_print found Contract_repr.pp contract Z.pp_print exp))
      (Data_encoding.obj3
        (Data_encoding.req None None "contract" Contract_repr.encoding)
        (Data_encoding.req None None "expected" Data_encoding.z)
        (Data_encoding.req None None "found" Data_encoding.z))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Counter_in_the_future" then
            let '(c, x, y) :=
              cast (Contract_repr.contract * Z.t * Z.t) payload in
            Some (c, x, y)
          else None
        end)
      (fun (function_parameter : Contract_repr.contract * Z.t * Z.t) =>
        let '(c, x, y) := function_parameter in
        Build_extensible "Counter_in_the_future"
          (Contract_repr.contract * Z.t * Z.t) (c, x, y)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Branch
      "contract.counter_in_the_past"
      "Invalid counter (already used) in a manager operation"
      "An operation assumed a contract counter in the past"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : Contract_repr.contract * Z.t * Z.t) =>
            let '(contract, exp, found) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "Counter "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal
                      " already used for contract "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.String_literal " (expected "
                          (CamlinternalFormatBasics.Alpha
                            (CamlinternalFormatBasics.Char_literal ")" % char
                              CamlinternalFormatBasics.End_of_format)))))))
                "Counter %a already used for contract %a (expected %a)")
              Z.pp_print found Contract_repr.pp contract Z.pp_print exp))
      (Data_encoding.obj3
        (Data_encoding.req None None "contract" Contract_repr.encoding)
        (Data_encoding.req None None "expected" Data_encoding.z)
        (Data_encoding.req None None "found" Data_encoding.z))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Counter_in_the_past" then
            let '(c, x, y) :=
              cast (Contract_repr.contract * Z.t * Z.t) payload in
            Some (c, x, y)
          else None
        end)
      (fun (function_parameter : Contract_repr.contract * Z.t * Z.t) =>
        let '(c, x, y) := function_parameter in
        Build_extensible "Counter_in_the_past"
          (Contract_repr.contract * Z.t * Z.t) (c, x, y)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "contract.non_existing_contract" "Non existing contract"
      "A contract handle is not present in the context (either it never was or it has been destroyed)"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (contract : Contract_repr.contract) =>
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "Contract "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal " does not exist"
                      CamlinternalFormatBasics.End_of_format)))
                "Contract %a does not exist") Contract_repr.pp contract))
      (Data_encoding.obj1
        (Data_encoding.req None None "contract" Contract_repr.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Non_existing_contract" then
            let 'c := cast Contract_repr.contract payload in
            Some c
          else None
        end)
      (fun (c : Contract_repr.contract) =>
        Build_extensible "Non_existing_contract" Contract_repr.contract c) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "contract.manager.inconsistent_hash" "Inconsistent public key hash"
      "A revealed manager public key is inconsistent with the announced hash"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter :
            Signature.public_key * Signature.public_key_hash *
              Signature.public_key_hash) =>
            let '(k, eh, ph) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "The hash of the manager public key "
                  (CamlinternalFormatBasics.String
                    CamlinternalFormatBasics.No_padding
                    (CamlinternalFormatBasics.String_literal " is not "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.String_literal
                          " as announced but "
                          (CamlinternalFormatBasics.Alpha
                            CamlinternalFormatBasics.End_of_format))))))
                "The hash of the manager public key %s is not %a as announced but %a")
              (Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.to_b58check) k)
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp) ph
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp) eh))
      (Data_encoding.obj3
        (Data_encoding.req None None "public_key"
          Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.encoding))
        (Data_encoding.req None None "expected_hash"
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding))
        (Data_encoding.req None None "provided_hash"
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Inconsistent_hash" then
            let '(k, eh, ph) :=
              cast
                (Signature.public_key * Signature.public_key_hash *
                  Signature.public_key_hash) payload in
            Some (k, eh, ph)
          else None
        end)
      (fun (function_parameter :
        Signature.public_key * Signature.public_key_hash *
          Signature.public_key_hash) =>
        let '(k, eh, ph) := function_parameter in
        Build_extensible "Inconsistent_hash"
          (Signature.public_key * Signature.public_key_hash *
            Signature.public_key_hash) (k, eh, ph)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "contract.manager.inconsistent_public_key" "Inconsistent public key"
      "A provided manager public key is different with the public key stored in the contract"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : Signature.public_key * Signature.public_key)
            =>
            let '(eh, ph) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Expected manager public key "
                  (CamlinternalFormatBasics.String
                    CamlinternalFormatBasics.No_padding
                    (CamlinternalFormatBasics.String_literal " but "
                      (CamlinternalFormatBasics.String
                        CamlinternalFormatBasics.No_padding
                        (CamlinternalFormatBasics.String_literal " was provided"
                          CamlinternalFormatBasics.End_of_format)))))
                "Expected manager public key %s but %s was provided")
              (Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.to_b58check) ph)
              (Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.to_b58check) eh)))
      (Data_encoding.obj2
        (Data_encoding.req None None "public_key"
          Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.encoding))
        (Data_encoding.req None None "expected_public_key"
          Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Inconsistent_public_key" then
            let '(eh, ph) :=
              cast (Signature.public_key * Signature.public_key) payload in
            Some (eh, ph)
          else None
        end)
      (fun (function_parameter : Signature.public_key * Signature.public_key) =>
        let '(eh, ph) := function_parameter in
        Build_extensible "Inconsistent_public_key"
          (Signature.public_key * Signature.public_key) (eh, ph)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent "contract.failure"
      "Contract storage failure" "Unexpected contract storage error"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (s : string) =>
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Contract_storage.Failure "
                  (CamlinternalFormatBasics.Caml_string
                    CamlinternalFormatBasics.No_padding
                    CamlinternalFormatBasics.End_of_format))
                "Contract_storage.Failure %S") s))
      (Data_encoding.obj1
        (Data_encoding.req None None "message" Data_encoding.string_value))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Failure" then
            let 's := cast string payload in
            Some s
          else None
        end) (fun (s : string) => Build_extensible "Failure" string s) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Branch "contract.unrevealed_key"
      "Manager operation precedes key revelation"
      "One tried to apply a manager operation without revealing the manager public key"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (s : Contract_repr.contract) =>
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Unrevealed manager key for contract "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.Char_literal "." % char
                      CamlinternalFormatBasics.End_of_format)))
                "Unrevealed manager key for contract %a.") Contract_repr.pp s))
      (Data_encoding.obj1
        (Data_encoding.req None None "contract" Contract_repr.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Unrevealed_manager_key" then
            let 's := cast Contract_repr.t payload in
            Some s
          else None
        end)
      (fun (s : Contract_repr.contract) =>
        Build_extensible "Unrevealed_manager_key" Contract_repr.contract s) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Branch
      "contract.previously_revealed_key" "Manager operation already revealed"
      "One tried to revealed twice a manager public key"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (s : Contract_repr.contract) =>
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Previously revealed manager key for contract "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.Char_literal "." % char
                      CamlinternalFormatBasics.End_of_format)))
                "Previously revealed manager key for contract %a.")
              Contract_repr.pp s))
      (Data_encoding.obj1
        (Data_encoding.req None None "contract" Contract_repr.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Previously_revealed_key" then
            let 's := cast Contract_repr.t payload in
            Some s
          else None
        end)
      (fun (s : Contract_repr.contract) =>
        Build_extensible "Previously_revealed_key" Contract_repr.contract s) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Branch
      "implicit.empty_implicit_contract" "Empty implicit contract"
      "No manager operations are allowed on an empty implicit contract."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (implicit : Signature.public_key_hash) =>
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Empty implicit contract ("
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.Char_literal ")" % char
                      CamlinternalFormatBasics.End_of_format)))
                "Empty implicit contract (%a)")
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp)
              implicit))
      (Data_encoding.obj1
        (Data_encoding.req None None "implicit"
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Empty_implicit_contract" then
            let 'c := cast Signature.public_key_hash payload in
            Some c
          else None
        end)
      (fun (c : Signature.public_key_hash) =>
        Build_extensible "Empty_implicit_contract" Signature.public_key_hash c)
    in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Branch
      "implicit.empty_implicit_delegated_contract"
      "Empty implicit delegated contract"
      "Emptying an implicit delegated account is not allowed."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (implicit : Signature.public_key_hash) =>
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Emptying implicit delegated contract ("
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.Char_literal ")" % char
                      CamlinternalFormatBasics.End_of_format)))
                "Emptying implicit delegated contract (%a)")
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp)
              implicit))
      (Data_encoding.obj1
        (Data_encoding.req None None "implicit"
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Empty_implicit_delegated_contract" then
            let 'c := cast Signature.public_key_hash payload in
            Some c
          else None
        end)
      (fun (c : Signature.public_key_hash) =>
        Build_extensible "Empty_implicit_delegated_contract"
          Signature.public_key_hash c) in
  Error_monad.register_error_kind Error_monad.Branch
    "contract.empty_transaction" "Empty transaction"
    "Forbidden to credit 0\234\156\169 to a contract without code."
    (Some
      (fun (ppf : Format.formatter) =>
        fun (contract : Contract_repr.contract) =>
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal
                "Transaction of 0\234\156\169 towards a contract without code are forbidden ("
                (CamlinternalFormatBasics.Alpha
                  (CamlinternalFormatBasics.String_literal ")."
                    CamlinternalFormatBasics.End_of_format)))
              "Transaction of 0\234\156\169 towards a contract without code are forbidden (%a).")
            Contract_repr.pp contract))
    (Data_encoding.obj1
      (Data_encoding.req None None "contract" Contract_repr.encoding))
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Empty_transaction" then
          let 'c := cast Contract_repr.t payload in
          Some c
        else None
      end)
    (fun (c : Contract_repr.contract) =>
      Build_extensible "Empty_transaction" Contract_repr.contract c).

Definition failwith {A : Set} (msg : string) : M=? A :=
  Error_monad.fail (Build_extensible "Failure" string msg).

Module Legacy_big_map_diff.
  (** Records for the constructor parameters *)
  Module ConstructorRecords_item.
    Module item.
      Module Update.
        Record record {big_map diff_key diff_key_hash diff_value : Set} : Set := Build {
          big_map : big_map;
          diff_key : diff_key;
          diff_key_hash : diff_key_hash;
          diff_value : diff_value }.
        Arguments record : clear implicits.
        Definition with_big_map
          {t_big_map t_diff_key t_diff_key_hash t_diff_value} big_map
          (r : record t_big_map t_diff_key t_diff_key_hash t_diff_value) :=
          Build t_big_map t_diff_key t_diff_key_hash t_diff_value big_map
            r.(diff_key) r.(diff_key_hash) r.(diff_value).
        Definition with_diff_key
          {t_big_map t_diff_key t_diff_key_hash t_diff_value} diff_key
          (r : record t_big_map t_diff_key t_diff_key_hash t_diff_value) :=
          Build t_big_map t_diff_key t_diff_key_hash t_diff_value r.(big_map)
            diff_key r.(diff_key_hash) r.(diff_value).
        Definition with_diff_key_hash
          {t_big_map t_diff_key t_diff_key_hash t_diff_value} diff_key_hash
          (r : record t_big_map t_diff_key t_diff_key_hash t_diff_value) :=
          Build t_big_map t_diff_key t_diff_key_hash t_diff_value r.(big_map)
            r.(diff_key) diff_key_hash r.(diff_value).
        Definition with_diff_value
          {t_big_map t_diff_key t_diff_key_hash t_diff_value} diff_value
          (r : record t_big_map t_diff_key t_diff_key_hash t_diff_value) :=
          Build t_big_map t_diff_key t_diff_key_hash t_diff_value r.(big_map)
            r.(diff_key) r.(diff_key_hash) diff_value.
      End Update.
      Definition Update_skeleton := Update.record.
      
      Module Copy.
        Record record {src dst : Set} : Set := Build {
          src : src;
          dst : dst }.
        Arguments record : clear implicits.
        Definition with_src {t_src t_dst} src (r : record t_src t_dst) :=
          Build t_src t_dst src r.(dst).
        Definition with_dst {t_src t_dst} dst (r : record t_src t_dst) :=
          Build t_src t_dst r.(src) dst.
      End Copy.
      Definition Copy_skeleton := Copy.record.
      
      Module Alloc.
        Record record {big_map key_type value_type : Set} : Set := Build {
          big_map : big_map;
          key_type : key_type;
          value_type : value_type }.
        Arguments record : clear implicits.
        Definition with_big_map {t_big_map t_key_type t_value_type} big_map
          (r : record t_big_map t_key_type t_value_type) :=
          Build t_big_map t_key_type t_value_type big_map r.(key_type)
            r.(value_type).
        Definition with_key_type {t_big_map t_key_type t_value_type} key_type
          (r : record t_big_map t_key_type t_value_type) :=
          Build t_big_map t_key_type t_value_type r.(big_map) key_type
            r.(value_type).
        Definition with_value_type {t_big_map t_key_type t_value_type}
          value_type (r : record t_big_map t_key_type t_value_type) :=
          Build t_big_map t_key_type t_value_type r.(big_map) r.(key_type)
            value_type.
      End Alloc.
      Definition Alloc_skeleton := Alloc.record.
    End item.
  End ConstructorRecords_item.
  Import ConstructorRecords_item.
  
  Reserved Notation "'item.Update".
  Reserved Notation "'item.Copy".
  Reserved Notation "'item.Alloc".
  
  Inductive item : Set :=
  | Update : 'item.Update -> item
  | Clear : Z.t -> item
  | Copy : 'item.Copy -> item
  | Alloc : 'item.Alloc -> item
  
  where "'item.Update" :=
    (item.Update_skeleton Z.t Script_repr.expr Script_expr_hash.t
      (option Script_repr.expr))
  and "'item.Copy" := (item.Copy_skeleton Z.t Z.t)
  and "'item.Alloc" :=
    (item.Alloc_skeleton Z.t Script_repr.expr Script_repr.expr).
  
  Module item.
    Include ConstructorRecords_item.item.
    Definition Update := 'item.Update.
    Definition Copy := 'item.Copy.
    Definition Alloc := 'item.Alloc.
  End item.
  
  Definition t : Set := list item.
  
  Definition item_encoding : Data_encoding.encoding item :=
    Data_encoding.union None
      [
        Data_encoding.case_value "update" None (Data_encoding.Tag 0)
          (Data_encoding.obj5
            (Data_encoding.req None None "action"
              (Data_encoding.constant "update"))
            (Data_encoding.req None None "big_map" Data_encoding.z)
            (Data_encoding.req None None "key_hash"
              Script_expr_hash.encoding)
            (Data_encoding.req None None "key" Script_repr.expr_encoding)
            (Data_encoding.opt None None "value"
              Script_repr.expr_encoding))
          (fun (function_parameter : item) =>
            match function_parameter with
            |
              Update {|
                item.Update.big_map := big_map;
                  item.Update.diff_key := diff_key;
                  item.Update.diff_key_hash := diff_key_hash;
                  item.Update.diff_value := diff_value
                  |} =>
              Some
                (tt, big_map, diff_key_hash, diff_key,
                  diff_value)
            | _ => None
            end)
          (fun (function_parameter :
            unit * Z.t * Script_expr_hash.t * Script_repr.expr *
              option Script_repr.expr) =>
            let '(_, big_map, diff_key_hash, diff_key, diff_value) :=
              function_parameter in
            Update
              {| item.Update.big_map := big_map;
                item.Update.diff_key := diff_key;
                item.Update.diff_key_hash := diff_key_hash;
                item.Update.diff_value := diff_value |});
        Data_encoding.case_value "remove" None (Data_encoding.Tag 1)
          (Data_encoding.obj2
            (Data_encoding.req None None "action"
              (Data_encoding.constant "remove"))
            (Data_encoding.req None None "big_map" Data_encoding.z))
          (fun (function_parameter : item) =>
            match function_parameter with
            | Clear big_map => Some (tt, big_map)
            | _ => None
            end)
          (fun (function_parameter : unit * Z.t) =>
            let '(_, big_map) := function_parameter in
            Clear big_map);
        Data_encoding.case_value "copy" None (Data_encoding.Tag 2)
          (Data_encoding.obj3
            (Data_encoding.req None None "action"
              (Data_encoding.constant "copy"))
            (Data_encoding.req None None "source_big_map"
              Data_encoding.z)
            (Data_encoding.req None None "destination_big_map"
              Data_encoding.z))
          (fun (function_parameter : item) =>
            match function_parameter with
            | Copy {| item.Copy.src := src; item.Copy.dst := dst |} =>
              Some (tt, src, dst)
            | _ => None
            end)
          (fun (function_parameter : unit * Z.t * Z.t) =>
            let '(_, src, dst) := function_parameter in
            Copy {| item.Copy.src := src; item.Copy.dst := dst |});
        Data_encoding.case_value "alloc" None (Data_encoding.Tag 3)
          (Data_encoding.obj4
            (Data_encoding.req None None "action"
              (Data_encoding.constant "alloc"))
            (Data_encoding.req None None "big_map" Data_encoding.z)
            (Data_encoding.req None None "key_type"
              Script_repr.expr_encoding)
            (Data_encoding.req None None "value_type"
              Script_repr.expr_encoding))
          (fun (function_parameter : item) =>
            match function_parameter with
            |
              Alloc {|
                item.Alloc.big_map := big_map;
                  item.Alloc.key_type := key_type;
                  item.Alloc.value_type := value_type
                  |} =>
              Some (tt, big_map, key_type, value_type)
            | _ => None
            end)
          (fun (function_parameter :
            unit * Z.t * Script_repr.expr * Script_repr.expr) =>
            let '(_, big_map, key_type, value_type) :=
              function_parameter in
            Alloc
              {| item.Alloc.big_map := big_map;
                item.Alloc.key_type := key_type;
                item.Alloc.value_type := value_type |})
      ].
  
  Definition encoding : Data_encoding.encoding (list item) :=
    Data_encoding.list_value None item_encoding.
  
  Definition to_lazy_storage_diff (legacy_diffs : list item)
    : list Lazy_storage_diff.diffs_item :=
    let rev_head {A B C D : Set}
      (diffs : list (A * Lazy_storage_diff.diff B C (list D)))
      : list (A * Lazy_storage_diff.diff B C (list D)) :=
      match diffs with
      | [] => nil
      | cons (_, Lazy_storage_diff.Remove) _ => diffs
      |
        cons
          (id,
            Lazy_storage_diff.Update {|
              Lazy_storage_diff.diff.Update.init := init_value;
                Lazy_storage_diff.diff.Update.updates := updates
                |}) rest =>
        cons
          (id,
            (Lazy_storage_diff.Update
              {| Lazy_storage_diff.diff.Update.init := init_value;
                Lazy_storage_diff.diff.Update.updates := List.rev updates |}))
          rest
      end in
    List.rev_map
      (fun (function_parameter :
        Z.t *
          Lazy_storage_diff.diff
            Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.ID.t)
            Lazy_storage_kind.Big_map.alloc Lazy_storage_kind.Big_map.updates)
        =>
        let '(id, diff_value) := function_parameter in
        let id :=
          Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.ID.of_legacy_USE_ONLY_IN_Legacy_big_map_diff)
            id in
        Lazy_storage_diff.make Lazy_storage_kind.Big_map id diff_value)
      (rev_head
        (List.fold_left
          (fun (new_diff :
            list
              (Z.t *
                Lazy_storage_diff.diff
                  Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.ID.t)
                  Lazy_storage_kind.Big_map.alloc
                  Lazy_storage_kind.Big_map.updates)) =>
            fun (item : item) =>
              match item with
              | Clear id =>
                cons (id, Lazy_storage_diff.Remove) (rev_head new_diff)
              | Copy {| item.Copy.src := src; item.Copy.dst := dst |} =>
                let src :=
                  Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.ID.of_legacy_USE_ONLY_IN_Legacy_big_map_diff)
                    src in
                cons
                  (dst,
                    (Lazy_storage_diff.Update
                      {|
                        Lazy_storage_diff.diff.Update.init :=
                          Lazy_storage_diff.Copy
                            {| Lazy_storage_diff.init.Copy.src := src |};
                        Lazy_storage_diff.diff.Update.updates := nil |}))
                  (rev_head new_diff)
              |
                Alloc {|
                  item.Alloc.big_map := big_map;
                    item.Alloc.key_type := key_type;
                    item.Alloc.value_type := value_type
                    |} =>
                cons
                  (big_map,
                    (Lazy_storage_diff.Update
                      {|
                        Lazy_storage_diff.diff.Update.init :=
                          Lazy_storage_diff.Alloc
                            {|
                              Lazy_storage_kind.Big_map.alloc.key_type :=
                                key_type;
                              Lazy_storage_kind.Big_map.alloc.value_type :=
                                value_type |};
                        Lazy_storage_diff.diff.Update.updates := nil |}))
                  (rev_head new_diff)
              |
                Update {|
                  item.Update.big_map := big_map;
                    item.Update.diff_key := key_value;
                    item.Update.diff_key_hash := key_hash;
                    item.Update.diff_value := value
                    |} =>
                match
                  (new_diff,
                    match new_diff with
                    | cons (id, diff_value) rest => id =Z big_map
                    | _ => false
                    end) with
                | (cons (id, diff_value) rest, true) =>
                  let diff_value :=
                    match diff_value with
                    | Lazy_storage_diff.Remove =>
                      (* ❌ Assert instruction is not handled. *)
                      assert
                        (Lazy_storage_diff.diff
                          Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.ID.t)
                          Lazy_storage_kind.Big_map.alloc
                          (list Lazy_storage_kind.Big_map.update)) false
                    |
                      Lazy_storage_diff.Update {|
                        Lazy_storage_diff.diff.Update.init := init_value;
                          Lazy_storage_diff.diff.Update.updates := updates
                          |} =>
                      let updates :=
                        cons
                          {| Lazy_storage_kind.Big_map.update.key := key_value;
                            Lazy_storage_kind.Big_map.update.key_hash :=
                              key_hash;
                            Lazy_storage_kind.Big_map.update.value := value |}
                          updates in
                      Lazy_storage_diff.Update
                        {| Lazy_storage_diff.diff.Update.init := init_value;
                          Lazy_storage_diff.diff.Update.updates := updates |}
                    end in
                  cons (id, diff_value) rest
                | (new_diff, _) =>
                  let updates :=
                    [
                      {| Lazy_storage_kind.Big_map.update.key := key_value;
                        Lazy_storage_kind.Big_map.update.key_hash
                          := key_hash;
                        Lazy_storage_kind.Big_map.update.value
                          := value |}
                    ] in
                  cons
                    (big_map,
                      (Lazy_storage_diff.Update
                        {|
                          Lazy_storage_diff.diff.Update.init :=
                            Lazy_storage_diff.Existing;
                          Lazy_storage_diff.diff.Update.updates := updates |}))
                    (rev_head new_diff)
                end
              end) nil legacy_diffs)).
  
  Axiom of_lazy_storage_diff : list Lazy_storage_diff.diffs_item -> list item.
End Legacy_big_map_diff.

Definition update_script_lazy_storage
  (c : Raw_context.t) (function_parameter : option Lazy_storage_diff.diffs)
  : M=? (Raw_context.t * Z.t) :=
  match function_parameter with
  | None => return=? (c, Z.zero)
  | Some diffs => Lazy_storage_diff.apply c diffs
  end.

Definition create_base (c : Raw_context.t) (op_staroptstar : option bool)
  : Contract_repr.contract -> Tez_repr.t -> option Signature.public_key_hash ->
  option Signature.public_key_hash ->
  option (Script_repr.t * option Lazy_storage_diff.diffs) -> unit ->
  M=? Raw_context.t :=
  let prepaid_bootstrap_storage :=
    match op_staroptstar with
    | Some op_starsthstar => op_starsthstar
    | None => false
    end in
  fun (contract : Contract_repr.contract) =>
    fun (balance : Tez_repr.t) =>
      fun (manager : option Signature.public_key_hash) =>
        fun (delegate : option Signature.public_key_hash) =>
          fun (script : option (Script_repr.t * option Lazy_storage_diff.diffs))
            =>
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              let=? c :=
                match Contract_repr.is_implicit contract with
                | None => return=? c
                | Some _ =>
                  let=? counter :=
                    Storage.Contract.Global_counter.(Storage.Simple_single_data_storage.get)
                      c in
                  Storage.Contract.Counter.(Storage_sigs.Indexed_data_storage.init_value)
                    c contract counter
                end in
              let=? c :=
                Storage.Contract.Balance.(Storage_sigs.Indexed_data_storage.init_value)
                  c contract balance in
              let=? c :=
                match manager with
                | Some manager =>
                  Storage.Contract.Manager.(Storage_sigs.Indexed_data_storage.init_value)
                    c contract (Manager_repr.Hash manager)
                | None => return=? c
                end in
              let=? c :=
                match delegate with
                | None => return=? c
                | Some delegate =>
                  Delegate_storage.init_value c contract delegate
                end in
              match script with
              |
                Some
                  ({|
                    Script_repr.t.code := code;
                      Script_repr.t.storage := storage_value
                      |}, lazy_storage_diff) =>
                let=? '(c, code_size) :=
                  Storage.Contract.Code.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.init_value)
                    c contract code in
                let=? '(c, storage_size) :=
                  Storage.Contract.Storage.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.init_value)
                    c contract storage_value in
                let=? '(c, lazy_storage_size) :=
                  update_script_lazy_storage c lazy_storage_diff in
                let total_size :=
                  ((Z.of_int code_size) +Z (Z.of_int storage_size)) +Z
                  lazy_storage_size in
                let '_ :=
                  (* ❌ Assert instruction is not handled. *)
                  assert unit (total_size >=Z Z.zero) in
                let prepaid_bootstrap_storage :=
                  if prepaid_bootstrap_storage then
                    total_size
                  else
                    Z.zero in
                let=? c :=
                  Storage.Contract.Paid_storage_space.(Storage_sigs.Indexed_data_storage.init_value)
                    c contract prepaid_bootstrap_storage in
                Storage.Contract.Used_storage_space.(Storage_sigs.Indexed_data_storage.init_value)
                  c contract total_size
              | None => return=? c
              end.

Definition raw_originate
  (c : Raw_context.t) (prepaid_bootstrap_storage : option bool)
  (contract : Contract_repr.contract) (balance : Tez_repr.t)
  (script : Script_repr.t * option Lazy_storage_diff.diffs)
  (delegate : option Signature.public_key_hash) : M=? Raw_context.t :=
  create_base c prepaid_bootstrap_storage contract balance None delegate
    (Some script) tt.

Definition create_implicit
  (c : Raw_context.t) (manager : Signature.public_key_hash)
  (balance : Tez_repr.t) : M=? Raw_context.t :=
  create_base c None (Contract_repr.implicit_contract manager) balance
    (Some manager) None None tt.

Definition delete (c : Raw_context.t) (contract : Contract_repr.contract)
  : M=? Raw_context.t :=
  match Contract_repr.is_implicit contract with
  | None => failwith "Non implicit contracts cannot be removed"
  | Some _ =>
    let=? c := Delegate_storage.remove c contract in
    let=? c :=
      Storage.Contract.Balance.(Storage_sigs.Indexed_data_storage.remove_existing)
        c contract in
    let=? c :=
      Storage.Contract.Manager.(Storage_sigs.Indexed_data_storage.remove_existing)
        c contract in
    let=? c :=
      Storage.Contract.Counter.(Storage_sigs.Indexed_data_storage.remove_existing)
        c contract in
    let=? '(c, _, _) :=
      Storage.Contract.Code.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.remove)
        c contract in
    let=? '(c, _, _) :=
      Storage.Contract.Storage.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.remove)
        c contract in
    let= c :=
      Storage.Contract.Paid_storage_space.(Storage_sigs.Indexed_data_storage.remove)
        c contract in
    Error_monad.op_gtpipeeq
      (Storage.Contract.Used_storage_space.(Storage_sigs.Indexed_data_storage.remove)
        c contract) Error_monad.ok
  end.

Definition allocated (c : Raw_context.t) (contract : Contract_repr.t)
  : M=? bool :=
  let=? function_parameter :=
    Storage.Contract.Balance.(Storage_sigs.Indexed_data_storage.find) c contract
    in
  match function_parameter with
  | None => Error_monad.return_false
  | Some _ => Error_monad.return_true
  end.

Definition _exists (c : Raw_context.t) (contract : Contract_repr.contract)
  : M=? bool :=
  match Contract_repr.is_implicit contract with
  | Some _ => Error_monad.return_true
  | None => allocated c contract
  end.

Definition must_exist (c : Raw_context.t) (contract : Contract_repr.contract)
  : M=? unit :=
  let=? function_parameter := _exists c contract in
  match function_parameter with
  | true => Error_monad.return_unit
  | false =>
    Error_monad.fail
      (Build_extensible "Non_existing_contract" Contract_repr.contract contract)
  end.

Definition must_be_allocated (c : Raw_context.t) (contract : Contract_repr.t)
  : M=? unit :=
  let=? function_parameter := allocated c contract in
  match function_parameter with
  | true => Error_monad.return_unit
  | false =>
    match Contract_repr.is_implicit contract with
    | Some pkh =>
      Error_monad.fail
        (Build_extensible "Empty_implicit_contract" Signature.public_key_hash
          pkh)
    | None =>
      Error_monad.fail
        (Build_extensible "Non_existing_contract" Contract_repr.t contract)
    end
  end.

Definition list_value (c : Raw_context.t) : M= (list Contract_repr.t) :=
  Storage.Contract.list_value c.

Definition fresh_contract_from_current_nonce (c : Raw_context.t)
  : M? (Raw_context.t * Contract_repr.contract) :=
  let? '(c, nonce_value) := Raw_context.increment_origination_nonce c in
  return? (c, (Contract_repr.originated_contract nonce_value)).

Definition originated_from_current_nonce
  (ctxt_since : Raw_context.t) (ctxt_until : Raw_context.t)
  : M=? (list Contract_repr.contract) :=
  let=? since := return= (Raw_context.get_origination_nonce ctxt_since) in
  let=? until := return= (Raw_context.get_origination_nonce ctxt_until) in
  List.filter_es
    (fun (contract : Contract_repr.contract) => _exists ctxt_until contract)
    (Contract_repr.originated_contracts since until).

Definition check_counter_increment
  (c : Raw_context.t) (manager : Signature.public_key_hash) (counter : Z.t)
  : M=? unit :=
  let contract := Contract_repr.implicit_contract manager in
  let=? contract_counter :=
    Storage.Contract.Counter.(Storage_sigs.Indexed_data_storage.get) c contract
    in
  let expected := Z.succ contract_counter in
  if expected =Z counter then
    Error_monad.return_unit
  else
    if expected >Z counter then
      Error_monad.fail
        (Build_extensible "Counter_in_the_past"
          (Contract_repr.contract * Z.t * Z.t) (contract, expected, counter))
    else
      Error_monad.fail
        (Build_extensible "Counter_in_the_future"
          (Contract_repr.contract * Z.t * Z.t) (contract, expected, counter)).

Definition increment_counter
  (c : Raw_context.t) (manager : Signature.public_key_hash)
  : M=? Raw_context.t :=
  let contract := Contract_repr.implicit_contract manager in
  let=? global_counter :=
    Storage.Contract.Global_counter.(Storage.Simple_single_data_storage.get) c
    in
  let=? c :=
    Storage.Contract.Global_counter.(Storage.Simple_single_data_storage.update)
      c (Z.succ global_counter) in
  let=? contract_counter :=
    Storage.Contract.Counter.(Storage_sigs.Indexed_data_storage.get) c contract
    in
  Storage.Contract.Counter.(Storage_sigs.Indexed_data_storage.update) c contract
    (Z.succ contract_counter).

Definition get_script_code (c : Raw_context.t) (contract : Contract_repr.t)
  : M=? (Raw_context.t * option Script_repr.lazy_expr) :=
  Storage.Contract.Code.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.find)
    c contract.

Definition get_script (c : Raw_context.t) (contract : Contract_repr.t)
  : M=? (Raw_context.t * option Script_repr.t) :=
  let=? '(c, code) :=
    Storage.Contract.Code.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.find)
      c contract in
  let=? '(c, storage_value) :=
    Storage.Contract.Storage.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.find)
      c contract in
  match (code, storage_value) with
  | (None, None) => return=? (c, None)
  | (Some code, Some storage_value) =>
    return=?
      (c,
        (Some
          {| Script_repr.t.code := code; Script_repr.t.storage := storage_value
            |}))
  | ((None, Some _) | (Some _, None)) => failwith "get_script"
  end.

Definition get_storage (ctxt : Raw_context.t) (contract : Contract_repr.t)
  : M=? (Raw_context.t * option Script_repr.expr) :=
  let=? function_parameter :=
    Storage.Contract.Storage.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.find)
      ctxt contract in
  match function_parameter with
  | (ctxt, None) => return=? (ctxt, None)
  | (ctxt, Some storage_value) =>
    let=? ctxt :=
      return=
        (Raw_context.consume_gas ctxt
          (Script_repr.force_decode_cost storage_value)) in
    let=? storage_value := return= (Script_repr.force_decode storage_value) in
    return=? (ctxt, (Some storage_value))
  end.

Definition get_counter (c : Raw_context.t) (manager : Signature.public_key_hash)
  : M=? Z.t :=
  let contract := Contract_repr.implicit_contract manager in
  let=? function_parameter :=
    Storage.Contract.Counter.(Storage_sigs.Indexed_data_storage.find) c contract
    in
  match function_parameter with
  | None =>
    match Contract_repr.is_implicit contract with
    | Some _ =>
      Storage.Contract.Global_counter.(Storage.Simple_single_data_storage.get) c
    | None => failwith "get_counter"
    end
  | Some v => return=? v
  end.

Definition get_manager_key
  (c : Raw_context.t) (manager : Signature.public_key_hash)
  : M=? Signature.public_key :=
  let contract := Contract_repr.implicit_contract manager in
  let=? function_parameter :=
    Storage.Contract.Manager.(Storage_sigs.Indexed_data_storage.find) c contract
    in
  match function_parameter with
  | None => failwith "get_manager_key"
  | Some (Manager_repr.Hash _) =>
    Error_monad.fail
      (Build_extensible "Unrevealed_manager_key" Contract_repr.contract contract)
  | Some (Manager_repr.Public_key v) => return=? v
  end.

Definition is_manager_key_revealed
  (c : Raw_context.t) (manager : Signature.public_key_hash) : M=? bool :=
  let contract := Contract_repr.implicit_contract manager in
  let=? function_parameter :=
    Storage.Contract.Manager.(Storage_sigs.Indexed_data_storage.find) c contract
    in
  match function_parameter with
  | None => Error_monad.return_false
  | Some (Manager_repr.Hash _) => Error_monad.return_false
  | Some (Manager_repr.Public_key _) => Error_monad.return_true
  end.

Definition reveal_manager_key
  (c : Raw_context.t) (manager : Signature.public_key_hash)
  (public_key : Signature.public_key) : M=? Raw_context.t :=
  let contract := Contract_repr.implicit_contract manager in
  let=? function_parameter :=
    Storage.Contract.Manager.(Storage_sigs.Indexed_data_storage.get) c contract
    in
  match function_parameter with
  | Manager_repr.Public_key _ =>
    Error_monad.fail
      (Build_extensible "Previously_revealed_key" Contract_repr.contract
        contract)
  | Manager_repr.Hash v =>
    let actual_hash :=
      Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.hash_value) public_key in
    if
      Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.equal) actual_hash
        v
    then
      let v := Manager_repr.Public_key public_key in
      Storage.Contract.Manager.(Storage_sigs.Indexed_data_storage.update) c
        contract v
    else
      Error_monad.fail
        (Build_extensible "Inconsistent_hash"
          (Signature.public_key * Signature.public_key_hash *
            Signature.public_key_hash) (public_key, v, actual_hash))
  end.

Definition get_balance (c : Raw_context.t) (contract : Contract_repr.t)
  : M=? Tez_repr.t :=
  let=? function_parameter :=
    Storage.Contract.Balance.(Storage_sigs.Indexed_data_storage.find) c contract
    in
  match function_parameter with
  | None =>
    match Contract_repr.is_implicit contract with
    | Some _ => return=? Tez_repr.zero
    | None => failwith "get_balance"
    end
  | Some v => return=? v
  end.

Definition get_balance_carbonated
  (c : Raw_context.t) (contract : Contract_repr.t)
  : M=? (Raw_context.t * Tez_repr.t) :=
  let=? c := return= (Raw_context.consume_gas c (Storage_costs.read_access 4 8))
    in
  let=? balance := get_balance c contract in
  return=? (c, balance).

Definition update_script_storage
  (c : Raw_context.t) (contract : Contract_repr.t)
  (storage_value : Script_repr.expr)
  (lazy_storage_diff : option Lazy_storage_diff.diffs) : M=? Raw_context.t :=
  let storage_value := Script_repr.lazy_expr_value storage_value in
  let=? '(c, lazy_storage_size_diff) :=
    update_script_lazy_storage c lazy_storage_diff in
  let=? '(c, size_diff) :=
    Storage.Contract.Storage.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.update)
      c contract storage_value in
  let=? previous_size :=
    Storage.Contract.Used_storage_space.(Storage_sigs.Indexed_data_storage.get)
      c contract in
  let new_size :=
    previous_size +Z (lazy_storage_size_diff +Z (Z.of_int size_diff)) in
  Storage.Contract.Used_storage_space.(Storage_sigs.Indexed_data_storage.update)
    c contract new_size.

Definition spend
  (c : Raw_context.t) (contract : Contract_repr.t) (amount : Tez_repr.t)
  : M=? Raw_context.t :=
  let=? balance :=
    Storage.Contract.Balance.(Storage_sigs.Indexed_data_storage.get) c contract
    in
  match Tez_repr.op_minusquestion balance amount with
  | Pervasives.Error _ =>
    Error_monad.fail
      (Build_extensible "Balance_too_low"
        (Contract_repr.t * Tez_repr.t * Tez_repr.t) (contract, balance, amount))
  | Pervasives.Ok new_balance =>
    let=? c :=
      Storage.Contract.Balance.(Storage_sigs.Indexed_data_storage.update) c
        contract new_balance in
    let=? c := Roll_storage.Contract.remove_amount c contract amount in
    if Tez_repr.op_gt new_balance Tez_repr.zero then
      return=? c
    else
      match Contract_repr.is_implicit contract with
      | None => return=? c
      | Some pkh =>
        let=? function_parameter := Delegate_storage.get c contract in
        match function_parameter with
        | Some pkh' =>
          if
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.equal) pkh
              pkh'
          then
            return=? c
          else
            Error_monad.fail
              (Build_extensible "Empty_implicit_delegated_contract"
                Signature.public_key_hash pkh)
        | None => delete c contract
        end
      end
  end.

Definition credit
  (c : Raw_context.t) (contract : Contract_repr.contract) (amount : Tez_repr.t)
  : M=? Raw_context.t :=
  let=? '_ :=
    if Tez_repr.op_ltgt amount Tez_repr.zero then
      Error_monad.return_unit
    else
      let=? '_ :=
        return=
          (Error_monad.error_unless
            (Option.is_some (Contract_repr.is_originated contract))
            (Build_extensible "Empty_transaction" Contract_repr.contract
              contract)) in
      must_exist c contract in
  let=? function_parameter :=
    Storage.Contract.Balance.(Storage_sigs.Indexed_data_storage.find) c contract
    in
  match function_parameter with
  | None =>
    match Contract_repr.is_implicit contract with
    | None =>
      Error_monad.fail
        (Build_extensible "Non_existing_contract" Contract_repr.contract
          contract)
    | Some manager => create_implicit c manager amount
    end
  | Some balance =>
    let=? balance := return= (Tez_repr.op_plusquestion amount balance) in
    let=? c :=
      Storage.Contract.Balance.(Storage_sigs.Indexed_data_storage.update) c
        contract balance in
    Roll_storage.Contract.add_amount c contract amount
  end.

Definition init_value (c : Raw_context.t) : M=? Raw_context.t :=
  let=? c :=
    Storage.Contract.Global_counter.(Storage.Simple_single_data_storage.init_value)
      c Z.zero in
  Lazy_storage_diff.init_value c.

Definition used_storage_space (c : Raw_context.t) (contract : Contract_repr.t)
  : M=? Z.t :=
  Error_monad.op_gtpipeeqquestion
    (Storage.Contract.Used_storage_space.(Storage_sigs.Indexed_data_storage.find)
      c contract) (fun x_1 => Option.value x_1 Z.zero).

Definition paid_storage_space (c : Raw_context.t) (contract : Contract_repr.t)
  : M=? Z.t :=
  Error_monad.op_gtpipeeqquestion
    (Storage.Contract.Paid_storage_space.(Storage_sigs.Indexed_data_storage.find)
      c contract) (fun x_1 => Option.value x_1 Z.zero).

Definition set_paid_storage_space_and_return_fees_to_pay
  (c : Raw_context.t) (contract : Contract_repr.t) (new_storage_space : Z.t)
  : M=? (Z.t * Raw_context.t) :=
  let=? already_paid_space :=
    Storage.Contract.Paid_storage_space.(Storage_sigs.Indexed_data_storage.get)
      c contract in
  if already_paid_space >=Z new_storage_space then
    return=? (Z.zero, c)
  else
    let to_pay := new_storage_space -Z already_paid_space in
    let=? c :=
      Storage.Contract.Paid_storage_space.(Storage_sigs.Indexed_data_storage.update)
        c contract new_storage_space in
    return=? (to_pay, c).
