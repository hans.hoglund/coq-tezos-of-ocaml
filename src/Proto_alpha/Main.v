Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Alpha_services.
Require TezosOfOCaml.Proto_alpha.Apply.
Require TezosOfOCaml.Proto_alpha.Apply_results.
Require TezosOfOCaml.Proto_alpha.Cache_memory_helpers.
Require TezosOfOCaml.Proto_alpha.Constants_repr.
Require TezosOfOCaml.Proto_alpha.Nonce_hash.
Require TezosOfOCaml.Proto_alpha.Script_ir_translator_mli.
  Module Script_ir_translator := Script_ir_translator_mli.
Require TezosOfOCaml.Proto_alpha.Script_typed_ir.
Require TezosOfOCaml.Proto_alpha.Services_registration.

Definition block_header_data : Set := Alpha_context.Block_header.protocol_data.

Definition block_header : Set := Alpha_context.Block_header.t.

Definition block_header_data_encoding
  : Data_encoding.encoding Alpha_context.Block_header.protocol_data :=
  Alpha_context.Block_header.protocol_data_encoding.

Definition block_header_metadata : Set := Apply_results.block_metadata.

Definition block_header_metadata_encoding
  : Data_encoding.encoding Apply_results.block_metadata :=
  Apply_results.block_metadata_encoding.

Definition operation_data : Set := Alpha_context.packed_protocol_data.

Definition operation_data_encoding
  : Data_encoding.t Alpha_context.Operation.packed_protocol_data :=
  Alpha_context.Operation.protocol_data_encoding.

Definition operation_receipt : Set := Apply_results.packed_operation_metadata.

Definition operation_receipt_encoding
  : Data_encoding.t Apply_results.packed_operation_metadata :=
  Apply_results.operation_metadata_encoding.

Definition operation_data_and_receipt_encoding
  : Data_encoding.t
    (Alpha_context.Operation.packed_protocol_data *
      Apply_results.packed_operation_metadata) :=
  Apply_results.operation_data_and_metadata_encoding.

Definition operation : Set := Alpha_context.packed_operation.

Definition acceptable_passes : Alpha_context.packed_operation -> list int :=
  Alpha_context.Operation.acceptable_passes.

Definition max_block_length : int :=
  Alpha_context.Block_header.max_header_length.

Definition max_operation_data_length : int :=
  Alpha_context.Constants.max_operation_data_length.

Definition validation_passes : list Updater.quota :=
  [
    {| Updater.quota.max_size := 2048 *i 2048; Updater.quota.max_op := Some 2048
      |};
    {| Updater.quota.max_size := 32 *i 1024; Updater.quota.max_op := None |};
    {|
      Updater.quota.max_size :=
        Alpha_context.Constants.max_anon_ops_per_block *i 1024;
      Updater.quota.max_op :=
        Some Alpha_context.Constants.max_anon_ops_per_block |};
    {| Updater.quota.max_size := 512 *i 1024; Updater.quota.max_op := None |}
  ].

Definition rpc_services : RPC_directory.directory Updater.rpc_context :=
  let '_ := Alpha_services.register tt in
  Services_registration.get_rpc_services tt.

(** Records for the constructor parameters *)
Module ConstructorRecords_validation_mode.
  Module validation_mode.
    Module Application.
      Record record {block_header baker : Set} : Set := Build {
        block_header : block_header;
        baker : baker }.
      Arguments record : clear implicits.
      Definition with_block_header {t_block_header t_baker} block_header
        (r : record t_block_header t_baker) :=
        Build t_block_header t_baker block_header r.(baker).
      Definition with_baker {t_block_header t_baker} baker
        (r : record t_block_header t_baker) :=
        Build t_block_header t_baker r.(block_header) baker.
    End Application.
    Definition Application_skeleton := Application.record.
    
    Module Partial_application.
      Record record {block_header baker : Set} : Set := Build {
        block_header : block_header;
        baker : baker }.
      Arguments record : clear implicits.
      Definition with_block_header {t_block_header t_baker} block_header
        (r : record t_block_header t_baker) :=
        Build t_block_header t_baker block_header r.(baker).
      Definition with_baker {t_block_header t_baker} baker
        (r : record t_block_header t_baker) :=
        Build t_block_header t_baker r.(block_header) baker.
    End Partial_application.
    Definition Partial_application_skeleton := Partial_application.record.
    
    Module Partial_construction.
      Record record {predecessor : Set} : Set := Build {
        predecessor : predecessor }.
      Arguments record : clear implicits.
      Definition with_predecessor {t_predecessor} predecessor
        (r : record t_predecessor) :=
        Build t_predecessor predecessor.
    End Partial_construction.
    Definition Partial_construction_skeleton := Partial_construction.record.
    
    Module Full_construction.
      Record record {predecessor protocol_data baker : Set} : Set := Build {
        predecessor : predecessor;
        protocol_data : protocol_data;
        baker : baker }.
      Arguments record : clear implicits.
      Definition with_predecessor {t_predecessor t_protocol_data t_baker}
        predecessor (r : record t_predecessor t_protocol_data t_baker) :=
        Build t_predecessor t_protocol_data t_baker predecessor
          r.(protocol_data) r.(baker).
      Definition with_protocol_data {t_predecessor t_protocol_data t_baker}
        protocol_data (r : record t_predecessor t_protocol_data t_baker) :=
        Build t_predecessor t_protocol_data t_baker r.(predecessor)
          protocol_data r.(baker).
      Definition with_baker {t_predecessor t_protocol_data t_baker} baker
        (r : record t_predecessor t_protocol_data t_baker) :=
        Build t_predecessor t_protocol_data t_baker r.(predecessor)
          r.(protocol_data) baker.
    End Full_construction.
    Definition Full_construction_skeleton := Full_construction.record.
  End validation_mode.
End ConstructorRecords_validation_mode.
Import ConstructorRecords_validation_mode.

Reserved Notation "'validation_mode.Application".
Reserved Notation "'validation_mode.Partial_application".
Reserved Notation "'validation_mode.Partial_construction".
Reserved Notation "'validation_mode.Full_construction".

Inductive validation_mode : Set :=
| Application : 'validation_mode.Application -> validation_mode
| Partial_application : 'validation_mode.Partial_application -> validation_mode
| Partial_construction :
  'validation_mode.Partial_construction -> validation_mode
| Full_construction : 'validation_mode.Full_construction -> validation_mode

where "'validation_mode.Application" :=
  (validation_mode.Application_skeleton Alpha_context.Block_header.t
    Alpha_context.public_key_hash)
and "'validation_mode.Partial_application" :=
  (validation_mode.Partial_application_skeleton Alpha_context.Block_header.t
    Alpha_context.public_key_hash)
and "'validation_mode.Partial_construction" :=
  (validation_mode.Partial_construction_skeleton Block_hash.t)
and "'validation_mode.Full_construction" :=
  (validation_mode.Full_construction_skeleton Block_hash.t
    Alpha_context.Block_header.contents Alpha_context.public_key_hash).

Module validation_mode.
  Include ConstructorRecords_validation_mode.validation_mode.
  Definition Application := 'validation_mode.Application.
  Definition Partial_application := 'validation_mode.Partial_application.
  Definition Partial_construction := 'validation_mode.Partial_construction.
  Definition Full_construction := 'validation_mode.Full_construction.
End validation_mode.

Module validation_state.
  Record record : Set := Build {
    mode : validation_mode;
    chain_id : Chain_id.t;
    ctxt : Alpha_context.t;
    op_count : int;
    migration_balance_updates : Alpha_context.Receipt.balance_updates;
    liquidity_baking_escape_ema : Int32.t;
    implicit_operations_results :
      list Apply_results.packed_successful_manager_operation_result }.
  Definition with_mode mode (r : record) :=
    Build mode r.(chain_id) r.(ctxt) r.(op_count) r.(migration_balance_updates)
      r.(liquidity_baking_escape_ema) r.(implicit_operations_results).
  Definition with_chain_id chain_id (r : record) :=
    Build r.(mode) chain_id r.(ctxt) r.(op_count) r.(migration_balance_updates)
      r.(liquidity_baking_escape_ema) r.(implicit_operations_results).
  Definition with_ctxt ctxt (r : record) :=
    Build r.(mode) r.(chain_id) ctxt r.(op_count) r.(migration_balance_updates)
      r.(liquidity_baking_escape_ema) r.(implicit_operations_results).
  Definition with_op_count op_count (r : record) :=
    Build r.(mode) r.(chain_id) r.(ctxt) op_count r.(migration_balance_updates)
      r.(liquidity_baking_escape_ema) r.(implicit_operations_results).
  Definition with_migration_balance_updates migration_balance_updates
    (r : record) :=
    Build r.(mode) r.(chain_id) r.(ctxt) r.(op_count) migration_balance_updates
      r.(liquidity_baking_escape_ema) r.(implicit_operations_results).
  Definition with_liquidity_baking_escape_ema liquidity_baking_escape_ema
    (r : record) :=
    Build r.(mode) r.(chain_id) r.(ctxt) r.(op_count)
      r.(migration_balance_updates) liquidity_baking_escape_ema
      r.(implicit_operations_results).
  Definition with_implicit_operations_results implicit_operations_results
    (r : record) :=
    Build r.(mode) r.(chain_id) r.(ctxt) r.(op_count)
      r.(migration_balance_updates) r.(liquidity_baking_escape_ema)
      implicit_operations_results.
End validation_state.
Definition validation_state := validation_state.record.

Definition cache_layout : list int := Apply.cache_layout.

Definition begin_partial_application
  (chain_id : Chain_id.t) (ctxt : Context.t) (predecessor_timestamp : Time.t)
  (predecessor_fitness : Alpha_context.Fitness.t)
  (block_header : Alpha_context.Block_header.t) : M=? validation_state :=
  let level :=
    block_header.(Alpha_context.Block_header.t.shell).(Block_header.shell_header.level)
    in
  let fitness := predecessor_fitness in
  let timestamp :=
    block_header.(Alpha_context.Block_header.t.shell).(Block_header.shell_header.timestamp)
    in
  let=? '(ctxt, migration_balance_updates, migration_operation_results) :=
    Alpha_context.prepare ctxt level predecessor_timestamp timestamp fitness in
  let=?
    '(ctxt, baker, liquidity_baking_operations_results,
      liquidity_baking_escape_ema) :=
    Apply.begin_application ctxt chain_id block_header predecessor_timestamp in
  let mode :=
    Partial_application
      {| validation_mode.Partial_application.block_header := block_header;
        validation_mode.Partial_application.baker :=
          Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.hash_value) baker |} in
  return=?
    {| validation_state.mode := mode; validation_state.chain_id := chain_id;
      validation_state.ctxt := ctxt; validation_state.op_count := 0;
      validation_state.migration_balance_updates := migration_balance_updates;
      validation_state.liquidity_baking_escape_ema :=
        liquidity_baking_escape_ema;
      validation_state.implicit_operations_results :=
        Pervasives.op_at
          (Apply_results.pack_migration_operation_results
            migration_operation_results) liquidity_baking_operations_results |}.

Definition begin_application
  (chain_id : Chain_id.t) (ctxt : Context.t) (predecessor_timestamp : Time.t)
  (predecessor_fitness : Alpha_context.Fitness.t)
  (block_header : Alpha_context.Block_header.t) : M=? validation_state :=
  let level :=
    block_header.(Alpha_context.Block_header.t.shell).(Block_header.shell_header.level)
    in
  let fitness := predecessor_fitness in
  let timestamp :=
    block_header.(Alpha_context.Block_header.t.shell).(Block_header.shell_header.timestamp)
    in
  let=? '(ctxt, migration_balance_updates, migration_operation_results) :=
    Alpha_context.prepare ctxt level predecessor_timestamp timestamp fitness in
  let=?
    '(ctxt, baker, liquidity_baking_operations_results,
      liquidity_baking_escape_ema) :=
    Apply.begin_application ctxt chain_id block_header predecessor_timestamp in
  let mode :=
    Application
      {| validation_mode.Application.block_header := block_header;
        validation_mode.Application.baker :=
          Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.hash_value) baker |} in
  return=?
    {| validation_state.mode := mode; validation_state.chain_id := chain_id;
      validation_state.ctxt := ctxt; validation_state.op_count := 0;
      validation_state.migration_balance_updates := migration_balance_updates;
      validation_state.liquidity_baking_escape_ema :=
        liquidity_baking_escape_ema;
      validation_state.implicit_operations_results :=
        Pervasives.op_at
          (Apply_results.pack_migration_operation_results
            migration_operation_results) liquidity_baking_operations_results |}.

Definition begin_construction
  (chain_id : Chain_id.t) (ctxt : Context.t) (predecessor_timestamp : Time.t)
  (pred_level : int32) (pred_fitness : Alpha_context.Fitness.t)
  (predecessor : Block_hash.t) (timestamp : Time.t)
  (protocol_data : option block_header_data) (function_parameter : unit)
  : M=? validation_state :=
  let '_ := function_parameter in
  let level := Int32.succ pred_level in
  let fitness := pred_fitness in
  let=? '(ctxt, migration_balance_updates, migration_operation_results) :=
    Alpha_context.prepare ctxt level predecessor_timestamp timestamp fitness in
  let=?
    '(mode, ctxt, liquidity_baking_operations_results,
      liquidity_baking_escape_ema) :=
    match protocol_data with
    | None =>
      let escape_vote := false in
      let=?
        '(ctxt, liquidity_baking_operations_results, liquidity_baking_escape_ema) :=
        Apply.begin_partial_construction ctxt escape_vote in
      let mode :=
        Partial_construction
          {| validation_mode.Partial_construction.predecessor := predecessor |}
        in
      return=?
        (mode, ctxt, liquidity_baking_operations_results,
          liquidity_baking_escape_ema)
    | Some proto_header =>
      let=?
        '(ctxt, protocol_data, baker, liquidity_baking_operations_results,
          liquidity_baking_escape_ema) :=
        Apply.begin_full_construction ctxt predecessor_timestamp
          proto_header.(Alpha_context.Block_header.protocol_data.contents) in
      let mode :=
        let baker :=
          Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.hash_value) baker in
        Full_construction
          {| validation_mode.Full_construction.predecessor := predecessor;
            validation_mode.Full_construction.protocol_data := protocol_data;
            validation_mode.Full_construction.baker := baker |} in
      return=?
        (mode, ctxt, liquidity_baking_operations_results,
          liquidity_baking_escape_ema)
    end in
  return=?
    {| validation_state.mode := mode; validation_state.chain_id := chain_id;
      validation_state.ctxt := ctxt; validation_state.op_count := 0;
      validation_state.migration_balance_updates := migration_balance_updates;
      validation_state.liquidity_baking_escape_ema :=
        liquidity_baking_escape_ema;
      validation_state.implicit_operations_results :=
        Pervasives.op_at
          (Apply_results.pack_migration_operation_results
            migration_operation_results) liquidity_baking_operations_results |}.

Definition apply_operation (function_parameter : validation_state)
  : Alpha_context.packed_operation -> M=? (validation_state * operation_receipt) :=
  let
    '{|
      validation_state.mode := mode;
        validation_state.chain_id := chain_id;
        validation_state.ctxt := ctxt;
        validation_state.op_count := op_count
        |} as data := function_parameter in
  fun (operation : Alpha_context.packed_operation) =>
    match
      (mode,
        match mode with
        | Partial_application _ =>
          Pervasives.not
            (List._exists (Compare.Int.(Compare.S.equal) 0)
              (Alpha_context.Operation.acceptable_passes operation))
        | _ => false
        end) with
    | (Partial_application _, true) =>
      let op_count := op_count +i 1 in
      return=?
        ((validation_state.with_op_count op_count
          (validation_state.with_ctxt ctxt data)),
          Apply_results.No_operation_metadata)
    | (_, _) =>
      let '{|
        Alpha_context.packed_operation.shell := shell;
          Alpha_context.packed_operation.protocol_data :=
            Alpha_context.Operation_data protocol_data
          |} := operation in
      let operation :=
        {| Alpha_context.operation.shell := shell;
          Alpha_context.operation.protocol_data := protocol_data |} in
      let '(predecessor, baker) :=
        match mode with
        |
          (Partial_application {|
            validation_mode.Partial_application.block_header := {|
              Alpha_context.Block_header.t.shell := {|
                Block_header.shell_header.predecessor := predecessor
                  |}
                |};
              validation_mode.Partial_application.baker := baker
              |} |
          Application {|
            validation_mode.Application.block_header := {|
              Alpha_context.Block_header.t.shell := {|
                Block_header.shell_header.predecessor := predecessor
                  |}
                |};
              validation_mode.Application.baker := baker
              |} |
          Full_construction {|
            validation_mode.Full_construction.predecessor := predecessor;
              validation_mode.Full_construction.baker := baker
              |}) => (predecessor, baker)
        |
          Partial_construction {|
            validation_mode.Partial_construction.predecessor := predecessor
              |} =>
          (predecessor,
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.zero))
        end in
      let=? '(ctxt, result_value) :=
        Apply.apply_operation ctxt chain_id Script_ir_translator.Optimized
          predecessor baker (Alpha_context.Operation.hash_value operation)
          operation in
      let op_count := op_count +i 1 in
      return=?
        ((validation_state.with_op_count op_count
          (validation_state.with_ctxt ctxt data)),
          (Apply_results.Operation_metadata result_value))
    end.

Definition cache_nonce_from_block_header
  (shell : Block_header.shell_header)
  (contents : Alpha_context.Block_header.contents) : bytes :=
  Block_hash.to_bytes
    (let shell :=
      Block_header.shell_header.with_context Context_hash.zero
        (Block_header.shell_header.with_fitness nil
          (Block_header.shell_header.with_validation_passes 0
            (Block_header.shell_header.with_timestamp (Time.of_seconds 0)
              (Block_header.shell_header.with_proto_level 0
                (Block_header.shell_header.with_level 0 shell))))) in
    let contents :=
      Alpha_context.Block_header.contents.with_proof_of_work_nonce
        (Bytes.make Constants_repr.proof_of_work_nonce_size "0" % char) contents
      in
    let protocol_data :=
      {| Alpha_context.Block_header.protocol_data.contents := contents;
        Alpha_context.Block_header.protocol_data.signature := Signature.zero |}
      in
    Alpha_context.Block_header.hash_value
      {| Alpha_context.Block_header.t.shell := shell;
        Alpha_context.Block_header.t.protocol_data := protocol_data |}).

(** Init function; without side-effects in Coq *)
Definition init_module : unit :=
  Error_monad.register_error_kind Error_monad.Permanent
    "main.missing_shell_header"
    "Missing shell_header during finalisation of a block"
    "During finalisation of a block header in Application mode or Full construction mode, a shell header should be provided so that a cache nonce can be computed."
    (Some
      (fun (ppf : Format.formatter) =>
        fun (function_parameter : unit) =>
          let '_ := function_parameter in
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal
                "No shell header provided during the finalisation of a block."
                CamlinternalFormatBasics.End_of_format)
              "No shell header provided during the finalisation of a block.")))
    Data_encoding.unit_value
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Missing_shell_header" then
          Some tt
        else None
      end)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Build_extensible "Missing_shell_header" unit tt).

Definition finalize_block (function_parameter : validation_state)
  : option Block_header.shell_header ->
  M=? (Updater.validation_result * Apply_results.block_metadata) :=
  let '{|
    validation_state.mode := mode;
      validation_state.ctxt := ctxt;
      validation_state.op_count := op_count;
      validation_state.migration_balance_updates := migration_balance_updates;
      validation_state.liquidity_baking_escape_ema :=
        liquidity_baking_escape_ema;
      validation_state.implicit_operations_results :=
        implicit_operations_results
      |} := function_parameter in
  fun (shell_header : option Block_header.shell_header) =>
    match mode with
    | Partial_construction _ =>
      let=? voting_period_info :=
        Alpha_context.Voting_period.get_rpc_current_info ctxt in
      let level_info := Alpha_context.Level.current ctxt in
      let baker := Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.zero)
        in
      let ctxt := Alpha_context.finalize None ctxt in
      return=?
        (ctxt,
          {| Apply_results.block_metadata.baker := baker;
            Apply_results.block_metadata.level_info := level_info;
            Apply_results.block_metadata.voting_period_info :=
              voting_period_info;
            Apply_results.block_metadata.nonce_hash := None;
            Apply_results.block_metadata.consumed_gas :=
              Alpha_context.Gas.Arith.zero;
            Apply_results.block_metadata.deactivated := nil;
            Apply_results.block_metadata.balance_updates :=
              migration_balance_updates;
            Apply_results.block_metadata.liquidity_baking_escape_ema :=
              liquidity_baking_escape_ema;
            Apply_results.block_metadata.implicit_operations_results :=
              implicit_operations_results |})
    |
      Partial_application {|
        validation_mode.Partial_application.block_header := block_header;
          validation_mode.Partial_application.baker := baker
          |} =>
      let included_endorsements := Alpha_context.included_endorsements ctxt in
      let=? '_ :=
        return=
          (Apply.check_minimal_valid_time ctxt
            block_header.(Alpha_context.Block_header.t.protocol_data).(Alpha_context.Block_header.protocol_data.contents).(Alpha_context.Block_header.contents.priority)
            included_endorsements) in
      let=? voting_period_info :=
        Alpha_context.Voting_period.get_rpc_current_info ctxt in
      let level_info := Alpha_context.Level.current ctxt in
      let ctxt := Alpha_context.finalize None ctxt in
      return=?
        (ctxt,
          {| Apply_results.block_metadata.baker := baker;
            Apply_results.block_metadata.level_info := level_info;
            Apply_results.block_metadata.voting_period_info :=
              voting_period_info;
            Apply_results.block_metadata.nonce_hash := None;
            Apply_results.block_metadata.consumed_gas :=
              Alpha_context.Gas.Arith.zero;
            Apply_results.block_metadata.deactivated := nil;
            Apply_results.block_metadata.balance_updates :=
              migration_balance_updates;
            Apply_results.block_metadata.liquidity_baking_escape_ema :=
              liquidity_baking_escape_ema;
            Apply_results.block_metadata.implicit_operations_results :=
              implicit_operations_results |})
    |
      (Application {|
        validation_mode.Application.block_header := {|
          Alpha_context.Block_header.t.protocol_data := {|
            Alpha_context.Block_header.protocol_data.contents := protocol_data
              |}
            |};
          validation_mode.Application.baker := baker
          |} |
      Full_construction {|
        validation_mode.Full_construction.protocol_data := protocol_data;
          validation_mode.Full_construction.baker := baker
          |}) =>
      let=? '(ctxt, receipt) :=
        Apply.finalize_application ctxt protocol_data baker
          migration_balance_updates liquidity_baking_escape_ema
          implicit_operations_results in
      let level := Alpha_context.Level.current ctxt in
      let priority :=
        protocol_data.(Alpha_context.Block_header.contents.priority) in
      let raw_level :=
        Alpha_context.Raw_level.to_int32 level.(Alpha_context.Level.t.level) in
      let fitness := Alpha_context.Fitness.current ctxt in
      let commit_message :=
        Format.asprintf
          (CamlinternalFormatBasics.Format
            (CamlinternalFormatBasics.String_literal "lvl "
              (CamlinternalFormatBasics.Int32 CamlinternalFormatBasics.Int_d
                CamlinternalFormatBasics.No_padding
                CamlinternalFormatBasics.No_precision
                (CamlinternalFormatBasics.String_literal ", fit 1:"
                  (CamlinternalFormatBasics.Int64 CamlinternalFormatBasics.Int_d
                    CamlinternalFormatBasics.No_padding
                    CamlinternalFormatBasics.No_precision
                    (CamlinternalFormatBasics.String_literal ", prio "
                      (CamlinternalFormatBasics.Int
                        CamlinternalFormatBasics.Int_d
                        CamlinternalFormatBasics.No_padding
                        CamlinternalFormatBasics.No_precision
                        (CamlinternalFormatBasics.String_literal ", "
                          (CamlinternalFormatBasics.Int
                            CamlinternalFormatBasics.Int_d
                            CamlinternalFormatBasics.No_padding
                            CamlinternalFormatBasics.No_precision
                            (CamlinternalFormatBasics.String_literal " ops"
                              CamlinternalFormatBasics.End_of_format)))))))))
            "lvl %ld, fit 1:%Ld, prio %d, %d ops") raw_level fitness priority
          op_count in
      let=? shell_header :=
        return=
          (Option.value_e shell_header
            (Error_monad.trace_of_error
              (Build_extensible "Missing_shell_header" unit tt))) in
      let cache_nonce :=
        cache_nonce_from_block_header
          (Block_header.shell_header.with_fitness
            (Alpha_context.Fitness.from_int64 fitness) shell_header)
          protocol_data in
      let= ctxt := Alpha_context.Cache.Admin.sync ctxt cache_nonce in
      let ctxt := Alpha_context.finalize (Some commit_message) ctxt in
      return=? (ctxt, receipt)
    end.

Definition relative_position_within_block
  (op1 : Alpha_context.packed_operation) (op2 : Alpha_context.packed_operation)
  : int :=
  let 'Alpha_context.Operation_data op1 :=
    op1.(Alpha_context.packed_operation.protocol_data) in
  let 'Alpha_context.Operation_data op2 :=
    op2.(Alpha_context.packed_operation.protocol_data) in
  match
    (op1.(Alpha_context.protocol_data.contents),
      op2.(Alpha_context.protocol_data.contents)) with
  |
    (Alpha_context.Single (Alpha_context.Endorsement _),
      Alpha_context.Single (Alpha_context.Endorsement _)) => 0
  | (_, Alpha_context.Single (Alpha_context.Endorsement _)) => 1
  | (Alpha_context.Single (Alpha_context.Endorsement _), _) => (-1)
  |
    (Alpha_context.Single (Alpha_context.Seed_nonce_revelation _),
      Alpha_context.Single (Alpha_context.Seed_nonce_revelation _)) => 0
  | (_, Alpha_context.Single (Alpha_context.Seed_nonce_revelation _)) => 1
  | (Alpha_context.Single (Alpha_context.Seed_nonce_revelation _), _) => (-1)
  |
    (Alpha_context.Single (Alpha_context.Double_endorsement_evidence _),
      Alpha_context.Single (Alpha_context.Double_endorsement_evidence _)) => 0
  | (_, Alpha_context.Single (Alpha_context.Double_endorsement_evidence _)) => 1
  | (Alpha_context.Single (Alpha_context.Double_endorsement_evidence _), _) =>
    (-1)
  |
    (Alpha_context.Single (Alpha_context.Endorsement_with_slot _),
      Alpha_context.Single (Alpha_context.Endorsement_with_slot _)) => 0
  | (_, Alpha_context.Single (Alpha_context.Endorsement_with_slot _)) => 1
  | (Alpha_context.Single (Alpha_context.Endorsement_with_slot _), _) => (-1)
  |
    (Alpha_context.Single (Alpha_context.Double_baking_evidence _),
      Alpha_context.Single (Alpha_context.Double_baking_evidence _)) => 0
  | (_, Alpha_context.Single (Alpha_context.Double_baking_evidence _)) => 1
  | (Alpha_context.Single (Alpha_context.Double_baking_evidence _), _) => (-1)
  |
    (Alpha_context.Single (Alpha_context.Activate_account _),
      Alpha_context.Single (Alpha_context.Activate_account _)) => 0
  | (_, Alpha_context.Single (Alpha_context.Activate_account _)) => 1
  | (Alpha_context.Single (Alpha_context.Activate_account _), _) => (-1)
  |
    (Alpha_context.Single (Alpha_context.Proposals _),
      Alpha_context.Single (Alpha_context.Proposals _)) => 0
  | (_, Alpha_context.Single (Alpha_context.Proposals _)) => 1
  | (Alpha_context.Single (Alpha_context.Proposals _), _) => (-1)
  |
    (Alpha_context.Single (Alpha_context.Ballot _),
      Alpha_context.Single (Alpha_context.Ballot _)) => 0
  | (_, Alpha_context.Single (Alpha_context.Ballot _)) => 1
  | (Alpha_context.Single (Alpha_context.Ballot _), _) => (-1)
  |
    (Alpha_context.Single (Alpha_context.Failing_noop _),
      Alpha_context.Single (Alpha_context.Failing_noop _)) => 0
  | (_, Alpha_context.Single (Alpha_context.Failing_noop _)) => 1
  | (Alpha_context.Single (Alpha_context.Failing_noop _), _) => (-1)
  |
    (Alpha_context.Single (Alpha_context.Manager_operation op1),
      Alpha_context.Single (Alpha_context.Manager_operation op2)) =>
    Z.compare op1.(Alpha_context.contents.Manager_operation.counter)
      op2.(Alpha_context.contents.Manager_operation.counter)
  |
    (Alpha_context.Cons (Alpha_context.Manager_operation op1) _,
      Alpha_context.Single (Alpha_context.Manager_operation op2)) =>
    Z.compare op1.(Alpha_context.contents.Manager_operation.counter)
      op2.(Alpha_context.contents.Manager_operation.counter)
  |
    (Alpha_context.Single (Alpha_context.Manager_operation op1),
      Alpha_context.Cons (Alpha_context.Manager_operation op2) _) =>
    Z.compare op1.(Alpha_context.contents.Manager_operation.counter)
      op2.(Alpha_context.contents.Manager_operation.counter)
  |
    (Alpha_context.Cons (Alpha_context.Manager_operation op1) _,
      Alpha_context.Cons (Alpha_context.Manager_operation op2) _) =>
    Z.compare op1.(Alpha_context.contents.Manager_operation.counter)
      op2.(Alpha_context.contents.Manager_operation.counter)
  | _ => unreachable_gadt_branch
  end.

Definition init_context (ctxt : Context.t) : M= Context.t :=
  let= ctxt := Context.Cache.(Context.CACHE.set_cache_layout) ctxt cache_layout
    in
  return= (Context.Cache.(Context.CACHE.clear) ctxt).

Definition init_value
  (ctxt : Context.t) (block_header : Block_header.shell_header)
  : M=? Updater.validation_result :=
  let level := block_header.(Block_header.shell_header.level) in
  let fitness := block_header.(Block_header.shell_header.fitness) in
  let timestamp := block_header.(Block_header.shell_header.timestamp) in
  let typecheck (ctxt : Alpha_context.context) (script : Alpha_context.Script.t)
    : M=?
      ((Alpha_context.Script.t * option Alpha_context.Lazy_storage.diffs) *
        Alpha_context.context) :=
    let allow_forged_in_storage := false in
    let=? '(Script_ir_translator.Ex_script parsed_script, ctxt) :=
      Script_ir_translator.parse_script None ctxt false allow_forged_in_storage
        script in
    let 'existT _ __Ex_script_'b [ctxt, parsed_script] as exi :=
      existT (A := Set)
        (fun __Ex_script_'b =>
          [Alpha_context.context ** Script_typed_ir.script __Ex_script_'b]) _
        [ctxt, parsed_script]
      return
        let fst := projT1 exi in
        let __Ex_script_'b := fst in
        M=?
          ((Alpha_context.Script.t * option Alpha_context.Lazy_storage.diffs) *
            Alpha_context.context) in
    let=? '(storage_value, lazy_storage_diff, ctxt) :=
      Script_ir_translator.extract_lazy_storage_diff ctxt
        Script_ir_translator.Optimized false
        Script_ir_translator.no_lazy_storage_id
        Script_ir_translator.no_lazy_storage_id
        parsed_script.(Script_typed_ir.script.storage_type)
        parsed_script.(Script_typed_ir.script.storage) in
    let=? '(storage_value, ctxt) :=
      Script_ir_translator.unparse_data ctxt Script_ir_translator.Optimized
        parsed_script.(Script_typed_ir.script.storage_type) storage_value in
    let storage_value :=
      Alpha_context.Script.lazy_expr_value
        (Micheline.strip_locations storage_value) in
    return=?
      (((Alpha_context.Script.t.with_storage storage_value script),
        lazy_storage_diff), ctxt) in
  let= ctxt := init_context ctxt in
  let=? ctxt :=
    Alpha_context.prepare_first_block ctxt typecheck level timestamp fitness in
  return=? (Alpha_context.finalize None ctxt).

Definition value_of_key {A B : Set} (function_parameter : A)
  : Context.t -> Time.t -> int32 -> Alpha_context.Fitness.t -> B -> Time.t ->
  M=? (Context.cache_key -> M=? Context.cache_value) :=
  let '_ := function_parameter in
  fun (ctxt : Context.t) =>
    fun (predecessor_timestamp : Time.t) =>
      fun (pred_level : int32) =>
        fun (pred_fitness : Alpha_context.Fitness.t) =>
          fun (function_parameter : B) =>
            let '_ := function_parameter in
            fun (timestamp : Time.t) =>
              let level := Int32.succ pred_level in
              let fitness := pred_fitness in
              let=? '(ctxt, _, _) :=
                Alpha_context.prepare ctxt level predecessor_timestamp timestamp
                  fitness in
              return=? (Apply.value_of_key ctxt).
