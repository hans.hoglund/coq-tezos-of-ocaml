Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Blinded_public_key_hash.
Require TezosOfOCaml.Proto_alpha.Commitment_repr.
Require TezosOfOCaml.Proto_alpha.Raw_context.
Require TezosOfOCaml.Proto_alpha.Storage.
Require TezosOfOCaml.Proto_alpha.Storage_sigs.
Require TezosOfOCaml.Proto_alpha.Tez_repr.

Definition find
  : Raw_context.t -> Blinded_public_key_hash.t -> M=? (option Tez_repr.t) :=
  Storage.Commitments.(Storage_sigs.Indexed_data_storage.find).

Definition remove_existing
  : Raw_context.t -> Blinded_public_key_hash.t -> M=? Raw_context.t :=
  Storage.Commitments.(Storage_sigs.Indexed_data_storage.remove_existing).

Definition init_value
  (ctxt : Raw_context.t) (commitments : list Commitment_repr.t)
  : M=? Raw_context.t :=
  let init_commitment
    (ctxt : Raw_context.t) (function_parameter : Commitment_repr.t)
    : M=? Raw_context.t :=
    let '{|
      Commitment_repr.t.blinded_public_key_hash := blinded_public_key_hash;
        Commitment_repr.t.amount := amount
        |} := function_parameter in
    Storage.Commitments.(Storage_sigs.Indexed_data_storage.init_value) ctxt
      blinded_public_key_hash amount in
  List.fold_left_es init_commitment ctxt commitments.
