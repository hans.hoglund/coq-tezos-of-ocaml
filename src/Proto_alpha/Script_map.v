Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Script_comparable.
Require TezosOfOCaml.Proto_alpha.Script_typed_ir.

Definition key_ty {a b : Set} (Box : Script_typed_ir.map a b)
  : Script_typed_ir.comparable_ty :=
  let 'existS _ _ Box := Box in
  Box.(Script_typed_ir.Boxed_map.key_ty).

Definition empty {a b : Set} (ty : Script_typed_ir.comparable_ty)
  : Script_typed_ir.map a b :=
  let OPS :=
    let functor_result :=
      Map.Make
        (let t : Set := a in
        let compare := Script_comparable.compare_comparable ty in
        {|
          Compare.COMPARABLE.compare := compare
        |}) in
    {|
      Script_typed_ir.Boxed_map_OPS.empty := functor_result.(Map.S.empty);
      Script_typed_ir.Boxed_map_OPS.add := functor_result.(Map.S.add);
      Script_typed_ir.Boxed_map_OPS.remove := functor_result.(Map.S.remove);
      Script_typed_ir.Boxed_map_OPS.find := functor_result.(Map.S.find);
      Script_typed_ir.Boxed_map_OPS.fold _ := functor_result.(Map.S.fold)
    |} in
  existS (A := Set -> Set) _ _
    (let key : Set := a in
    let value : Set := b in
    let key_ty := ty in
    let OPS :=
      let value : Set := b in
      let key := OPS.(Script_typed_ir.Boxed_map_OPS.key) in
      let t := OPS.(Script_typed_ir.Boxed_map_OPS.t) in
      let empty := OPS.(Script_typed_ir.Boxed_map_OPS.empty) in
      let add := OPS.(Script_typed_ir.Boxed_map_OPS.add) in
      let remove := OPS.(Script_typed_ir.Boxed_map_OPS.remove) in
      let find := OPS.(Script_typed_ir.Boxed_map_OPS.find) in
      let fold {a : Set} := OPS.(Script_typed_ir.Boxed_map_OPS.fold) in
      {|
        Script_typed_ir.Boxed_map_OPS.empty := empty;
        Script_typed_ir.Boxed_map_OPS.add := add;
        Script_typed_ir.Boxed_map_OPS.remove := remove;
        Script_typed_ir.Boxed_map_OPS.find := find;
        Script_typed_ir.Boxed_map_OPS.fold _ := fold
      |} in
    let boxed := (OPS.(Script_typed_ir.Boxed_map_OPS.empty), 0) in
    {|
      Script_typed_ir.Boxed_map.key_ty := key_ty;
      Script_typed_ir.Boxed_map.OPS := OPS;
      Script_typed_ir.Boxed_map.boxed := boxed
    |}).

Definition get {key value : Set} (k : key) (Box : Script_typed_ir.map key value)
  : option value :=
  let 'existS _ _ Box := Box in
  Box.(Script_typed_ir.Boxed_map.OPS).(Script_typed_ir.Boxed_map_OPS.find) k
    (Pervasives.fst Box.(Script_typed_ir.Boxed_map.boxed)).

Definition update {a b : Set}
  (k : a) (v : option b) (Box : Script_typed_ir.map a b)
  : Script_typed_ir.map a b :=
  let 'existS _ _ Box := Box in
  existS (A := Set -> Set) _ _
    (let key : Set := a in
    let value : Set := b in
    let key_ty := Box.(Script_typed_ir.Boxed_map.key_ty) in
    let OPS := Box.(Script_typed_ir.Boxed_map.OPS) in
    let boxed :=
      let '(map, size_value) := Box.(Script_typed_ir.Boxed_map.boxed) in
      let contains :=
        match
          Box.(Script_typed_ir.Boxed_map.OPS).(Script_typed_ir.Boxed_map_OPS.find)
            k map with
        | None => false
        | _ => true
        end in
      match v with
      | Some v =>
        ((Box.(Script_typed_ir.Boxed_map.OPS).(Script_typed_ir.Boxed_map_OPS.add)
          k v map),
          (size_value +i
          (if contains then
            0
          else
            1)))
      | None =>
        ((Box.(Script_typed_ir.Boxed_map.OPS).(Script_typed_ir.Boxed_map_OPS.remove)
          k map),
          (size_value -i
          (if contains then
            1
          else
            0)))
      end in
    {|
      Script_typed_ir.Boxed_map.key_ty := key_ty;
      Script_typed_ir.Boxed_map.OPS := OPS;
      Script_typed_ir.Boxed_map.boxed := boxed
    |}).

Definition mem {key value : Set} (k : key) (Box : Script_typed_ir.map key value)
  : bool :=
  let 'existS _ _ Box := Box in
  match
    Box.(Script_typed_ir.Boxed_map.OPS).(Script_typed_ir.Boxed_map_OPS.find) k
      (Pervasives.fst Box.(Script_typed_ir.Boxed_map.boxed)) with
  | None => false
  | _ => true
  end.

Definition fold {key value acc : Set}
  (f : key -> value -> acc -> acc) (Box : Script_typed_ir.map key value)
  : acc -> acc :=
  let 'existS _ _ Box := Box in
  Box.(Script_typed_ir.Boxed_map.OPS).(Script_typed_ir.Boxed_map_OPS.fold) f
    (Pervasives.fst Box.(Script_typed_ir.Boxed_map.boxed)).

Definition size_value {key value : Set} (Box : Script_typed_ir.map key value)
  : Alpha_context.Script_int.num :=
  let 'existS _ _ Box := Box in
  Alpha_context.Script_int.abs
    (Alpha_context.Script_int.of_int
      (Pervasives.snd Box.(Script_typed_ir.Boxed_map.boxed))).
