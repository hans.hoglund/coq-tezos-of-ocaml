Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Misc.
Require TezosOfOCaml.Proto_alpha.Nonce_hash.

Module Timestamp_too_early.
  Record record : Set := Build {
    minimal_time : Alpha_context.Timestamp.t;
    provided_time : Alpha_context.Timestamp.t;
    priority : int;
    endorsing_power_opt : option int }.
  Definition with_minimal_time minimal_time (r : record) :=
    Build minimal_time r.(provided_time) r.(priority) r.(endorsing_power_opt).
  Definition with_provided_time provided_time (r : record) :=
    Build r.(minimal_time) provided_time r.(priority) r.(endorsing_power_opt).
  Definition with_priority priority (r : record) :=
    Build r.(minimal_time) r.(provided_time) priority r.(endorsing_power_opt).
  Definition with_endorsing_power_opt endorsing_power_opt (r : record) :=
    Build r.(minimal_time) r.(provided_time) r.(priority) endorsing_power_opt.
End Timestamp_too_early.
Definition Timestamp_too_early := Timestamp_too_early.record.

(** Init function; without side-effects in Coq *)
Definition init_module1 : unit :=
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "baking.timestamp_too_early" "Block forged too early"
      "The block timestamp is before the minimal valid one."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : Time.t * Time.t * int * option int) =>
            let '(minimal_time, provided_time, priority, endorsing_power) :=
              function_parameter in
            let message_regarding_endorsements :=
              match endorsing_power with
              | None => ""
              | Some power =>
                Format.asprintf
                  (CamlinternalFormatBasics.Format
                    (CamlinternalFormatBasics.String_literal
                      " and endorsing power "
                      (CamlinternalFormatBasics.Int
                        CamlinternalFormatBasics.Int_d
                        CamlinternalFormatBasics.No_padding
                        CamlinternalFormatBasics.No_precision
                        CamlinternalFormatBasics.End_of_format))
                    " and endorsing power %d") power
              end in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Block forged too early: "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal
                      " is before the minimal time "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.String_literal
                          " for priority "
                          (CamlinternalFormatBasics.Int
                            CamlinternalFormatBasics.Int_d
                            CamlinternalFormatBasics.No_padding
                            CamlinternalFormatBasics.No_precision
                            (CamlinternalFormatBasics.String
                              CamlinternalFormatBasics.No_padding
                              (CamlinternalFormatBasics.Char_literal ")" % char
                                CamlinternalFormatBasics.End_of_format))))))))
                "Block forged too early: %a is before the minimal time %a for priority %d%s)")
              Time.pp_hum provided_time Time.pp_hum minimal_time priority
              message_regarding_endorsements))
      (Data_encoding.obj4
        (Data_encoding.req None None "minimal_time" Time.encoding)
        (Data_encoding.req None None "provided_time" Time.encoding)
        (Data_encoding.req None None "priority" Data_encoding.int31)
        (Data_encoding.opt None None "endorsing_power" Data_encoding.int31))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Timestamp_too_early" then
            let '{|
              Timestamp_too_early.minimal_time := minimal_time;
                Timestamp_too_early.provided_time := provided_time;
                Timestamp_too_early.priority := priority;
                Timestamp_too_early.endorsing_power_opt := endorsing_power_opt
                |} := cast Timestamp_too_early payload in
            Some (minimal_time, provided_time, priority, endorsing_power_opt)
          else None
        end)
      (fun (function_parameter : Time.t * Time.t * int * option int) =>
        let '(minimal_time, provided_time, priority, endorsing_power_opt) :=
          function_parameter in
        Build_extensible "Timestamp_too_early" Timestamp_too_early
          {| Timestamp_too_early.minimal_time := minimal_time;
            Timestamp_too_early.provided_time := provided_time;
            Timestamp_too_early.priority := priority;
            Timestamp_too_early.endorsing_power_opt := endorsing_power_opt |})
    in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "baking.invalid_fitness_gap" "Invalid fitness gap"
      "The gap of fitness is out of bounds"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : int64 * int64) =>
            let '(m, g) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "The gap of fitness "
                  (CamlinternalFormatBasics.Int64 CamlinternalFormatBasics.Int_d
                    CamlinternalFormatBasics.No_padding
                    CamlinternalFormatBasics.No_precision
                    (CamlinternalFormatBasics.String_literal
                      " is not between 0 and "
                      (CamlinternalFormatBasics.Int64
                        CamlinternalFormatBasics.Int_d
                        CamlinternalFormatBasics.No_padding
                        CamlinternalFormatBasics.No_precision
                        CamlinternalFormatBasics.End_of_format))))
                "The gap of fitness %Ld is not between 0 and %Ld") g m))
      (Data_encoding.obj2
        (Data_encoding.req None None "maximum" Data_encoding.int64_value)
        (Data_encoding.req None None "provided" Data_encoding.int64_value))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_fitness_gap" then
            let '(m, g) := cast (int64 * int64) payload in
            Some (m, g)
          else None
        end)
      (fun (function_parameter : int64 * int64) =>
        let '(m, g) := function_parameter in
        Build_extensible "Invalid_fitness_gap" (int64 * int64) (m, g)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "baking.invalid_block_signature" "Invalid block signature"
      "A block was not signed with the expected private key."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : Block_hash.t * Signature.public_key_hash) =>
            let '(block, pkh) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Invalid signature for block "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal ". Expected: "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.Char_literal "." % char
                          CamlinternalFormatBasics.End_of_format)))))
                "Invalid signature for block %a. Expected: %a.")
              Block_hash.pp_short block
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp_short)
              pkh))
      (Data_encoding.obj2
        (Data_encoding.req None None "block" Block_hash.encoding)
        (Data_encoding.req None None "expected"
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_block_signature" then
            let '(block, pkh) :=
              cast (Block_hash.t * Signature.public_key_hash) payload in
            Some (block, pkh)
          else None
        end)
      (fun (function_parameter : Block_hash.t * Signature.public_key_hash) =>
        let '(block, pkh) := function_parameter in
        Build_extensible "Invalid_block_signature"
          (Block_hash.t * Signature.public_key_hash) (block, pkh)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "baking.invalid_signature" "Invalid block signature"
      "The block's signature is invalid"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Invalid block signature"
                  CamlinternalFormatBasics.End_of_format)
                "Invalid block signature"))) Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_signature" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Invalid_signature" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "baking.insufficient_proof_of_work"
      "Insufficient block proof-of-work stamp"
      "The block's proof-of-work stamp is insufficient"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Insufficient proof-of-work stamp"
                  CamlinternalFormatBasics.End_of_format)
                "Insufficient proof-of-work stamp"))) Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_stamp" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Invalid_stamp" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "baking.unexpected_endorsement" "Endorsement from unexpected delegate"
      "The operation is signed by a delegate without endorsement rights."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "The endorsement is signed by a delegate without endorsement rights."
                  CamlinternalFormatBasics.End_of_format)
                "The endorsement is signed by a delegate without endorsement rights.")))
      Data_encoding.unit_value
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Unexpected_endorsement" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Unexpected_endorsement" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "baking.invalid_endorsement_slot" "Endorsement slot out of range"
      "The endorsement slot provided is negative or too high."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (v : int) =>
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "Endorsement slot "
                  (CamlinternalFormatBasics.Int CamlinternalFormatBasics.Int_d
                    CamlinternalFormatBasics.No_padding
                    CamlinternalFormatBasics.No_precision
                    (CamlinternalFormatBasics.String_literal
                      " provided is negative or too high."
                      CamlinternalFormatBasics.End_of_format)))
                "Endorsement slot %d provided is negative or too high.") v))
      (Data_encoding.obj1
        (Data_encoding.req None None "slot" Data_encoding.uint16))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_endorsement_slot" then
            let 'v := cast int payload in
            Some v
          else None
        end)
      (fun (v : int) => Build_extensible "Invalid_endorsement_slot" int v) in
  Error_monad.register_error_kind Error_monad.Permanent
    "baking.unexpected_endorsement_slot"
    "Endorsement slot not the smallest possible"
    "The endorsement slot provided is not the smallest possible."
    (Some
      (fun (ppf : Format.formatter) =>
        fun (v : int) =>
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal "Endorsement slot "
                (CamlinternalFormatBasics.Int CamlinternalFormatBasics.Int_d
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.No_precision
                  (CamlinternalFormatBasics.String_literal
                    " provided is not the smallest possible."
                    CamlinternalFormatBasics.End_of_format)))
              "Endorsement slot %d provided is not the smallest possible.") v))
    (Data_encoding.obj1
      (Data_encoding.req None None "slot" Data_encoding.uint16))
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Unexpected_endorsement_slot" then
          let 'v := cast int payload in
          Some v
        else None
      end)
    (fun (v : int) => Build_extensible "Unexpected_endorsement_slot" int v).

Definition minimal_time_fastpath_case
  (minimal_block_delay : Alpha_context.Period.t)
  (pred_timestamp : Alpha_context.Timestamp.time)
  : M? Alpha_context.Timestamp.time :=
  Alpha_context.Timestamp.op_plusquestion pred_timestamp minimal_block_delay.

Definition minimal_time_slowpath_case
  (time_between_blocks : list Alpha_context.Period.period) (priority : int32)
  (pred_timestamp : Alpha_context.Timestamp.time)
  : M? Alpha_context.Timestamp.time :=
  let fix cumsum_time_between_blocks
    (acc_value : Alpha_context.Timestamp.time)
    (durations : list Alpha_context.Period.period) (p_value : int32)
    {struct durations} : M? Alpha_context.Timestamp.time :=
    if p_value <=i32 0 then
      return? acc_value
    else
      match durations with
      | [] =>
        cumsum_time_between_blocks acc_value [ Alpha_context.Period.one_minute ]
          p_value
      | cons last [] =>
        let? period := Alpha_context.Period.mult p_value last in
        Alpha_context.Timestamp.op_plusquestion acc_value period
      | cons first durations =>
        let? acc_value :=
          Alpha_context.Timestamp.op_plusquestion acc_value first in
        let p_value := Int32.pred p_value in
        cumsum_time_between_blocks acc_value durations p_value
      end in
  cumsum_time_between_blocks pred_timestamp time_between_blocks
    (Int32.succ priority).

Definition minimal_time
  (constants : Alpha_context.Constants.parametric) (priority : int)
  (pred_timestamp : Alpha_context.Timestamp.time)
  : M? Alpha_context.Timestamp.time :=
  let priority := Int32.of_int priority in
  if priority =i32 0 then
    minimal_time_fastpath_case
      constants.(Alpha_context.Constants.parametric.minimal_block_delay)
      pred_timestamp
  else
    minimal_time_slowpath_case
      constants.(Alpha_context.Constants.parametric.time_between_blocks)
      priority pred_timestamp.

Definition earlier_predecessor_timestamp
  (ctxt : Alpha_context.context) (level : Alpha_context.Level.level)
  : M? Alpha_context.Timestamp.time :=
  let current := Alpha_context.Level.current ctxt in
  let current_timestamp := Alpha_context.Timestamp.current ctxt in
  let gap := Alpha_context.Level.diff_value level current in
  let step := Alpha_context.Constants.minimal_block_delay ctxt in
  if gap <i32 1 then
    Pervasives.failwith "Baking.earlier_block_timestamp: past block."
  else
    let? delay := Alpha_context.Period.mult (Int32.pred gap) step in
    Alpha_context.Timestamp.op_plusquestion current_timestamp delay.

Definition check_timestamp
  (c : Alpha_context.context) (priority : int)
  (pred_timestamp : Alpha_context.Timestamp.time) : M? unit :=
  let? minimal_time :=
    minimal_time (Alpha_context.Constants.parametric_value c) priority
      pred_timestamp in
  let timestamp := Alpha_context.Timestamp.current c in
  let? _block_delay :=
    Error_monad.record_trace
      (Build_extensible "Timestamp_too_early" Timestamp_too_early
        {| Timestamp_too_early.minimal_time := minimal_time;
          Timestamp_too_early.provided_time := timestamp;
          Timestamp_too_early.priority := priority;
          Timestamp_too_early.endorsing_power_opt := None |})
      (Alpha_context.Timestamp.op_minusquestion timestamp minimal_time) in
  return? tt.

(** Init function; without side-effects in Coq *)
Definition init_module2 : unit :=
  Error_monad.register_error_kind Error_monad.Permanent "incorrect_priority"
    "Incorrect priority" "Block priority must be non-negative."
    (Some
      (fun (ppf : Format.formatter) =>
        fun (function_parameter : unit) =>
          let '_ := function_parameter in
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal
                "The block priority must be non-negative."
                CamlinternalFormatBasics.End_of_format)
              "The block priority must be non-negative.")))
    Data_encoding.unit_value
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Incorrect_priority" then
          Some tt
        else None
      end)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Build_extensible "Incorrect_priority" unit tt).

(** Init function; without side-effects in Coq *)
Definition init_module3 : unit :=
  let description :=
    "The number of endorsements must be non-negative and at most the endorsers_per_block constant."
    in
  Error_monad.register_error_kind Error_monad.Permanent
    "incorrect_number_of_endorsements" "Incorrect number of endorsements"
    description
    (Some
      (fun (ppf : Format.formatter) =>
        fun (function_parameter : unit) =>
          let '_ := function_parameter in
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String
                CamlinternalFormatBasics.No_padding
                CamlinternalFormatBasics.End_of_format) "%s") description))
    Data_encoding.unit_value
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Incorrect_number_of_endorsements" then
          Some tt
        else None
      end)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Build_extensible "Incorrect_number_of_endorsements" unit tt).

Fixpoint reward_for_priority
  (reward_per_prio : list Alpha_context.Tez.tez) (prio : int)
  : Alpha_context.Tez.tez :=
  match reward_per_prio with
  | [] => Alpha_context.Tez.zero
  | cons last [] => last
  | cons first rest =>
    if prio <=i 0 then
      first
    else
      reward_for_priority rest (Pervasives.pred prio)
  end.

Definition baking_reward
  (ctxt : Alpha_context.context) (block_priority : int)
  (included_endorsements : int) : M? Alpha_context.Tez.tez :=
  let? '_ :=
    Error_monad.error_unless (block_priority >=i 0)
      (Build_extensible "Incorrect_priority" unit tt) in
  let? '_ :=
    Error_monad.error_unless
      ((included_endorsements >=i 0) &&
      (included_endorsements <=i
      (Alpha_context.Constants.endorsers_per_block ctxt)))
      (Build_extensible "Incorrect_number_of_endorsements" unit tt) in
  let reward_per_endorsement :=
    reward_for_priority
      (Alpha_context.Constants.baking_reward_per_endorsement ctxt)
      block_priority in
  Alpha_context.Tez.op_starquestion reward_per_endorsement
    (Int64.of_int included_endorsements).

Definition endorsing_reward
  (ctxt : Alpha_context.context) (block_priority : int) (num_slots : int)
  : M? Alpha_context.Tez.tez :=
  let? '_ :=
    Error_monad.error_unless (block_priority >=i 0)
      (Build_extensible "Incorrect_priority" unit tt) in
  let reward_per_endorsement :=
    reward_for_priority (Alpha_context.Constants.endorsement_reward ctxt)
      block_priority in
  Alpha_context.Tez.op_starquestion reward_per_endorsement
    (Int64.of_int num_slots).

Definition baking_priorities
  (c : Alpha_context.context) (level : Alpha_context.Level.t)
  : M=? (Misc.lazy_list_t Alpha_context.public_key) :=
  let fix f (priority : int)
    : M=? (Misc.lazy_list_t Alpha_context.public_key) :=
    let=? delegate := Alpha_context.Roll.baking_rights_owner c level priority in
    return=?
      (Misc.LCons delegate
        (fun (function_parameter : unit) =>
          let '_ := function_parameter in
          f (Pervasives.succ priority))) in
  f 0.

Definition endorsement_rights
  (ctxt : Alpha_context.context) (level : Alpha_context.Level.t)
  : M=?
    (Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.t)
      (Alpha_context.public_key * list int * bool)) :=
  List.fold_right_es
    (fun (slot : int) =>
      fun (acc_value :
        Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.t)
          (Alpha_context.public_key * list int * bool)) =>
        let=? pk := Alpha_context.Roll.endorsement_rights_owner ctxt level slot
          in
        let pkh := Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.hash_value) pk
          in
        let _right :=
          match
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.find)
              pkh acc_value with
          | None => (pk, [ slot ], false)
          | Some (pk, slots, used) => (pk, (cons slot slots), used)
          end in
        return=?
          (Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.add)
            pkh _right acc_value))
    (Misc.op_minusminusgt 0
      ((Alpha_context.Constants.endorsers_per_block ctxt) -i 1))
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.empty).

Axiom check_endorsement_right :
  Alpha_context.context -> Chain_id.t -> int -> Alpha_context.Operation.t ->
  M=? Signature.public_key_hash.

Definition check_endorsement_slots_at_current_level
  (ctxt : Alpha_context.context) (slot : int) (pkh : Signature.public_key_hash)
  : M=? (list int * bool) :=
  let endorsements := Alpha_context.allowed_endorsements ctxt in
  match
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.find)
      pkh endorsements with
  | None => Error_monad.fail (Build_extensible "Unexpected_endorsement" unit tt)
  | Some (_pk, (cons top_slot _) as slots, v) =>
    let=? '_ :=
      return=
        (Error_monad.error_unless (slot =i top_slot)
          (Build_extensible "Unexpected_endorsement_slot" int slot)) in
    return=? (slots, v)
  | Some (_pk, [], _) =>
    Error_monad.fail (Build_extensible "Unexpected_endorsement_slot" int slot)
  end.

Definition select_delegate
  (delegate : Signature.public_key_hash)
  (delegate_list : Misc.lazy_list_t Signature.public_key) (max_priority : int)
  : M=? (list int) :=
  let fix loop
    (acc_value : list int) (l_value : Misc.lazy_list_t Signature.public_key)
    (n : int) : M=? (list int) :=
    if n >=i max_priority then
      return=? (List.rev acc_value)
    else
      let 'Misc.LCons pk t_value := l_value in
      let acc_value :=
        if
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.equal) delegate
            (Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.hash_value) pk)
        then
          cons n acc_value
        else
          acc_value in
      let=? t_value := t_value tt in
      loop acc_value t_value (Pervasives.succ n) in
  loop nil delegate_list 0.

Definition first_baking_priorities
  (ctxt : Alpha_context.context) (op_staroptstar : option int)
  : Signature.public_key_hash -> Alpha_context.Level.t -> M=? (list int) :=
  let max_priority :=
    match op_staroptstar with
    | Some op_starsthstar => op_starsthstar
    | None => 32
    end in
  fun (delegate : Signature.public_key_hash) =>
    fun (level : Alpha_context.Level.t) =>
      let=? delegate_list := baking_priorities ctxt level in
      select_delegate delegate delegate_list max_priority.

Definition check_hash
  (hash_value : Block_hash.t) (stamp_threshold : Compare.Uint64.(Compare.S.t))
  : bool :=
  let bytes_value := Block_hash.to_bytes hash_value in
  let word := TzEndian.get_int64 bytes_value 0 in
  Compare.Uint64.(Compare.S.op_lteq) word stamp_threshold.

Definition check_header_proof_of_work_stamp
  (shell : Block_header.shell_header)
  (contents : Alpha_context.Block_header.contents)
  (stamp_threshold : Compare.Uint64.(Compare.S.t)) : bool :=
  let hash_value :=
    Alpha_context.Block_header.hash_value
      {| Alpha_context.Block_header.t.shell := shell;
        Alpha_context.Block_header.t.protocol_data :=
          {| Alpha_context.Block_header.protocol_data.contents := contents;
            Alpha_context.Block_header.protocol_data.signature := Signature.zero
            |} |} in
  check_hash hash_value stamp_threshold.

Definition check_proof_of_work_stamp
  (ctxt : Alpha_context.context) (block : Alpha_context.Block_header.t)
  : M? unit :=
  let proof_of_work_threshold :=
    Alpha_context.Constants.proof_of_work_threshold ctxt in
  if
    check_header_proof_of_work_stamp block.(Alpha_context.Block_header.t.shell)
      block.(Alpha_context.Block_header.t.protocol_data).(Alpha_context.Block_header.protocol_data.contents)
      proof_of_work_threshold
  then
    Error_monad.ok_unit
  else
    Error_monad.error_value (Build_extensible "Invalid_stamp" unit tt).

Definition check_signature
  (block : Alpha_context.Block_header.t) (chain_id : Chain_id.t)
  (key_value : Signature.public_key) : M=? unit :=
  let check_signature
    (key_value : Signature.public_key)
    (function_parameter : Alpha_context.Block_header.t) : bool :=
    let '{|
      Alpha_context.Block_header.t.shell := shell;
        Alpha_context.Block_header.t.protocol_data := {|
          Alpha_context.Block_header.protocol_data.contents := contents;
            Alpha_context.Block_header.protocol_data.signature :=
              signature
            |}
        |} := function_parameter in
    let unsigned_header :=
      Data_encoding.Binary.to_bytes_exn None
        Alpha_context.Block_header.unsigned_encoding (shell, contents) in
    Signature.check (Some (Signature.Block_header chain_id)) key_value signature
      unsigned_header in
  if check_signature key_value block then
    Error_monad.return_unit
  else
    Error_monad.fail
      (Build_extensible "Invalid_block_signature"
        (Block_hash.t * Signature.public_key_hash)
        ((Alpha_context.Block_header.hash_value block),
          (Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.hash_value) key_value))).

Definition max_fitness_gap {A : Set} (_ctxt : A) : int64 := 1.

Definition check_fitness_gap
  (ctxt : Alpha_context.context) (block : Alpha_context.Block_header.t)
  : M? unit :=
  let current_fitness := Alpha_context.Fitness.current ctxt in
  let? announced_fitness :=
    Alpha_context.Fitness.to_int64
      block.(Alpha_context.Block_header.t.shell).(Block_header.shell_header.fitness)
    in
  let gap := announced_fitness -i64 current_fitness in
  if (gap <=i64 0) || ((max_fitness_gap ctxt) <i64 gap) then
    Error_monad.error_value
      (Build_extensible "Invalid_fitness_gap" (int64 * int64)
        ((max_fitness_gap ctxt), gap))
  else
    Error_monad.ok_unit.

Definition fastpath_endorsing_power_threshold (maximal_endorsing_power : int)
  : int := (3 *i maximal_endorsing_power) /i 5.

Definition minimal_valid_time
  (constants : Alpha_context.Constants.parametric) (priority : int)
  (endorsing_power : int) (predecessor_timestamp : Alpha_context.Timestamp.time)
  : M? Alpha_context.Timestamp.time :=
  if
    (priority =i 0) &&
    (endorsing_power >=i
    (fastpath_endorsing_power_threshold
      constants.(Alpha_context.Constants.parametric.endorsers_per_block)))
  then
    minimal_time_fastpath_case
      constants.(Alpha_context.Constants.parametric.minimal_block_delay)
      predecessor_timestamp
  else
    let? minimal_time :=
      minimal_time_slowpath_case
        constants.(Alpha_context.Constants.parametric.time_between_blocks)
        (Int32.of_int priority) predecessor_timestamp in
    let delay_per_missing_endorsement :=
      constants.(Alpha_context.Constants.parametric.delay_per_missing_endorsement)
      in
    let missing_endorsements :=
      let minimal_required_endorsements :=
        constants.(Alpha_context.Constants.parametric.initial_endorsers) in
      Compare.Int.(Compare.S.max) 0
        (minimal_required_endorsements -i endorsing_power) in
    let? delay :=
      Alpha_context.Period.mult (Int32.of_int missing_endorsements)
        delay_per_missing_endorsement in
    return? (Time.add minimal_time (Alpha_context.Period.to_seconds delay)).
