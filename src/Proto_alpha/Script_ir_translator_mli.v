Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Cache_memory_helpers.
Require TezosOfOCaml.Proto_alpha.Gas_limit_repr.
Require TezosOfOCaml.Proto_alpha.Michelson_v1_primitives.
Require TezosOfOCaml.Proto_alpha.Script_expr_hash.
Require TezosOfOCaml.Proto_alpha.Script_tc_errors.
Require TezosOfOCaml.Proto_alpha.Script_typed_ir.

Inductive eq : Set :=
| Eq : eq.

Inductive ex_comparable_ty : Set :=
| Ex_comparable_ty : Script_typed_ir.comparable_ty -> ex_comparable_ty.

Inductive ex_ty : Set :=
| Ex_ty : Script_typed_ir.ty -> ex_ty.

Inductive ex_stack_ty : Set :=
| Ex_stack_ty : Script_typed_ir.stack_ty -> ex_stack_ty.

Inductive ex_script : Set :=
| Ex_script : forall {b : Set}, Script_typed_ir.script b -> ex_script.

Module toplevel.
  Record record : Set := Build {
    code_field : Alpha_context.Script.node;
    arg_type : Alpha_context.Script.node;
    storage_type : Alpha_context.Script.node;
    views : Script_typed_ir.SMap.(Map.S.t) Script_typed_ir.view;
    root_name : option Script_typed_ir.field_annot }.
  Definition with_code_field code_field (r : record) :=
    Build code_field r.(arg_type) r.(storage_type) r.(views) r.(root_name).
  Definition with_arg_type arg_type (r : record) :=
    Build r.(code_field) arg_type r.(storage_type) r.(views) r.(root_name).
  Definition with_storage_type storage_type (r : record) :=
    Build r.(code_field) r.(arg_type) storage_type r.(views) r.(root_name).
  Definition with_views views (r : record) :=
    Build r.(code_field) r.(arg_type) r.(storage_type) views r.(root_name).
  Definition with_root_name root_name (r : record) :=
    Build r.(code_field) r.(arg_type) r.(storage_type) r.(views) root_name.
End toplevel.
Definition toplevel := toplevel.record.

Module code.
  Record record : Set := Build {
    code : Script_typed_ir.lambda;
    arg_type : Script_typed_ir.ty;
    storage_type : Script_typed_ir.ty;
    views : Script_typed_ir.SMap.(Map.S.t) Script_typed_ir.view;
    root_name : option Script_typed_ir.field_annot;
    code_size : Cache_memory_helpers.sint }.
  Definition with_code code (r : record) :=
    Build code r.(arg_type) r.(storage_type) r.(views) r.(root_name)
      r.(code_size).
  Definition with_arg_type arg_type (r : record) :=
    Build r.(code) arg_type r.(storage_type) r.(views) r.(root_name)
      r.(code_size).
  Definition with_storage_type storage_type (r : record) :=
    Build r.(code) r.(arg_type) storage_type r.(views) r.(root_name)
      r.(code_size).
  Definition with_views views (r : record) :=
    Build r.(code) r.(arg_type) r.(storage_type) views r.(root_name)
      r.(code_size).
  Definition with_root_name root_name (r : record) :=
    Build r.(code) r.(arg_type) r.(storage_type) r.(views) root_name
      r.(code_size).
  Definition with_code_size code_size (r : record) :=
    Build r.(code) r.(arg_type) r.(storage_type) r.(views) r.(root_name)
      code_size.
End code.
Definition code := code.record.

Inductive ex_code : Set :=
| Ex_code : code -> ex_code.

Inductive ex_view (storage : Set) : Set :=
| Ex_view : Script_typed_ir.lambda -> ex_view storage.

Arguments Ex_view {_}.

Module cinstr.
  Record record : Set := Build {
    apply :
      Script_typed_ir.kinfo -> Script_typed_ir.kinstr -> Script_typed_ir.kinstr }.
  Definition with_apply apply (r : record) :=
    Build apply.
End cinstr.
Definition cinstr := cinstr.record.

Module descr.
  Record record : Set := Build {
    loc : Alpha_context.Script.location;
    bef : Script_typed_ir.stack_ty;
    aft : Script_typed_ir.stack_ty;
    instr : cinstr }.
  Definition with_loc loc (r : record) :=
    Build loc r.(bef) r.(aft) r.(instr).
  Definition with_bef bef (r : record) :=
    Build r.(loc) bef r.(aft) r.(instr).
  Definition with_aft aft (r : record) :=
    Build r.(loc) r.(bef) aft r.(instr).
  Definition with_instr instr (r : record) :=
    Build r.(loc) r.(bef) r.(aft) instr.
End descr.
Definition descr := descr.record.

(** Records for the constructor parameters *)
Module ConstructorRecords_tc_context.
  Module tc_context.
    Module Toplevel.
      Record record {storage_type param_type root_name
        legacy_create_contract_literal : Set} : Set := Build {
        storage_type : storage_type;
        param_type : param_type;
        root_name : root_name;
        legacy_create_contract_literal : legacy_create_contract_literal }.
      Arguments record : clear implicits.
      Definition with_storage_type
        {t_storage_type t_param_type t_root_name
          t_legacy_create_contract_literal} storage_type
        (r :
          record t_storage_type t_param_type t_root_name
            t_legacy_create_contract_literal) :=
        Build t_storage_type t_param_type t_root_name
          t_legacy_create_contract_literal storage_type r.(param_type)
          r.(root_name) r.(legacy_create_contract_literal).
      Definition with_param_type
        {t_storage_type t_param_type t_root_name
          t_legacy_create_contract_literal} param_type
        (r :
          record t_storage_type t_param_type t_root_name
            t_legacy_create_contract_literal) :=
        Build t_storage_type t_param_type t_root_name
          t_legacy_create_contract_literal r.(storage_type) param_type
          r.(root_name) r.(legacy_create_contract_literal).
      Definition with_root_name
        {t_storage_type t_param_type t_root_name
          t_legacy_create_contract_literal} root_name
        (r :
          record t_storage_type t_param_type t_root_name
            t_legacy_create_contract_literal) :=
        Build t_storage_type t_param_type t_root_name
          t_legacy_create_contract_literal r.(storage_type) r.(param_type)
          root_name r.(legacy_create_contract_literal).
      Definition with_legacy_create_contract_literal
        {t_storage_type t_param_type t_root_name
          t_legacy_create_contract_literal} legacy_create_contract_literal
        (r :
          record t_storage_type t_param_type t_root_name
            t_legacy_create_contract_literal) :=
        Build t_storage_type t_param_type t_root_name
          t_legacy_create_contract_literal r.(storage_type) r.(param_type)
          r.(root_name) legacy_create_contract_literal.
    End Toplevel.
    Definition Toplevel_skeleton := Toplevel.record.
  End tc_context.
End ConstructorRecords_tc_context.
Import ConstructorRecords_tc_context.

Reserved Notation "'tc_context.Toplevel".

Inductive tc_context : Set :=
| Lambda : tc_context
| Dip : Script_typed_ir.stack_ty -> tc_context -> tc_context
| Toplevel : 'tc_context.Toplevel -> tc_context

where "'tc_context.Toplevel" :=
  (tc_context.Toplevel_skeleton Script_typed_ir.ty Script_typed_ir.ty
    (option Script_typed_ir.field_annot) bool).

Module tc_context.
  Include ConstructorRecords_tc_context.tc_context.
  Definition Toplevel := 'tc_context.Toplevel.
End tc_context.

(** Records for the constructor parameters *)
Module ConstructorRecords_judgement.
  Module judgement.
    Module Failed.
      Record record {descr : Set} : Set := Build {
        descr : descr }.
      Arguments record : clear implicits.
      Definition with_descr {t_descr} descr (r : record t_descr) :=
        Build t_descr descr.
    End Failed.
    Definition Failed_skeleton := Failed.record.
  End judgement.
End ConstructorRecords_judgement.
Import ConstructorRecords_judgement.

Reserved Notation "'judgement.Failed".

Inductive judgement (a s : Set) : Set :=
| Typed : descr -> judgement a s
| Failed : 'judgement.Failed -> judgement a s

where "'judgement.Failed" :=
  (judgement.Failed_skeleton (Script_typed_ir.stack_ty -> descr)).

Module judgement.
  Include ConstructorRecords_judgement.judgement.
  Definition Failed := 'judgement.Failed.
End judgement.

Arguments Typed {_ _}.
Arguments Failed {_ _}.

Parameter close_descr : descr -> Script_typed_ir.kdescr.

Inductive unparsing_mode : Set :=
| Optimized : unparsing_mode
| Readable : unparsing_mode
| Optimized_legacy : unparsing_mode.

Inductive merge_type_error_flag : Set :=
| Default_merge_type_error : merge_type_error_flag
| Fast_merge_type_error : merge_type_error_flag.

Module Gas_monad.
  Parameter t : forall (a : Set), Set.
  
  Definition gas_monad (a : Set) : Set := t a.
  
  Parameter _return : forall {a : Set}, a -> t a.
  
  Parameter op_gtgtdollar : forall {a b : Set}, t a -> (a -> t b) -> t b.
  
  Parameter op_gtpipedollar : forall {a b : Set}, t a -> (a -> b) -> t b.
  
  Parameter op_gtquestiondollar : forall {a b : Set}, t a -> (a -> M? b) -> t b.
  
  Parameter op_gtquestionquestiondollar : forall {a b : Set},
    t a -> (M? a -> t b) -> t b.
  
  Parameter from_tzresult : forall {a : Set}, M? a -> t a.
  
  Parameter unsafe_embed : forall {a : Set},
    (Alpha_context.context -> M? (a * Alpha_context.context)) -> t a.
  
  Parameter gas_consume : Alpha_context.Gas.cost -> t unit.
  
  Parameter run : forall {a : Set},
    Alpha_context.context -> t a -> M? (M? a * Alpha_context.context).
  
  Parameter record_trace_eval : forall {a : Set},
    (unit -> M? Error_monad._error) -> t a -> t a.
  
  Parameter get_context : t Alpha_context.context.
End Gas_monad.

Definition type_logger : Set :=
  int -> list (Alpha_context.Script.expr * Alpha_context.Script.annot) ->
  list (Alpha_context.Script.expr * Alpha_context.Script.annot) -> unit.

Parameter empty_big_map : forall {a b : Set},
  Script_typed_ir.comparable_ty -> Script_typed_ir.ty ->
  Script_typed_ir.big_map a b.

Parameter big_map_mem : forall {key value : Set},
  Alpha_context.context -> key -> Script_typed_ir.big_map key value ->
  M=? (bool * Alpha_context.context).

Parameter big_map_get : forall {key value : Set},
  Alpha_context.context -> key -> Script_typed_ir.big_map key value ->
  M=? (option value * Alpha_context.context).

Parameter big_map_update : forall {key value : Set},
  Alpha_context.context -> key -> option value ->
  Script_typed_ir.big_map key value ->
  M=? (Script_typed_ir.big_map key value * Alpha_context.context).

Parameter big_map_get_and_update : forall {key value : Set},
  Alpha_context.context -> key -> option value ->
  Script_typed_ir.big_map key value ->
  M=?
    ((option value * Script_typed_ir.big_map key value) * Alpha_context.context).

Parameter ty_eq :
  Alpha_context.context -> Alpha_context.Script.location ->
  Script_typed_ir.ty -> Script_typed_ir.ty -> M? (eq * Alpha_context.context).

Parameter merge_types :
  bool -> merge_type_error_flag -> Alpha_context.Script.location ->
  Script_typed_ir.ty -> Script_typed_ir.ty ->
  Gas_monad.t (eq * Script_typed_ir.ty).

Parameter parse_comparable_data : forall {a : Set},
  option type_logger -> Alpha_context.context ->
  Script_typed_ir.comparable_ty -> Alpha_context.Script.node ->
  M=? (a * Alpha_context.context).

Parameter parse_data : forall {a : Set},
  option type_logger -> Alpha_context.context -> bool -> bool ->
  Script_typed_ir.ty -> Alpha_context.Script.node ->
  M=? (a * Alpha_context.context).

Parameter unparse_data : forall {a : Set},
  Alpha_context.context -> unparsing_mode -> Script_typed_ir.ty -> a ->
  M=? (Alpha_context.Script.node * Alpha_context.context).

Parameter unparse_code :
  Alpha_context.context -> unparsing_mode -> Alpha_context.Script.node ->
  M=? (Alpha_context.Script.node * Alpha_context.context).

Parameter parse_instr : forall {a s : Set},
  option type_logger -> tc_context -> Alpha_context.context -> bool ->
  Alpha_context.Script.node -> Script_typed_ir.stack_ty ->
  M=? (judgement a s * Alpha_context.context).

Parameter parse_big_map_value_ty :
  Alpha_context.context -> bool -> Alpha_context.Script.node ->
  M? (ex_ty * Alpha_context.context).

Parameter parse_packable_ty :
  Alpha_context.context -> bool -> Alpha_context.Script.node ->
  M? (ex_ty * Alpha_context.context).

Parameter parse_parameter_ty :
  Alpha_context.context -> bool -> Alpha_context.Script.node ->
  M? (ex_ty * Alpha_context.context).

Parameter parse_comparable_ty :
  Alpha_context.context -> Alpha_context.Script.node ->
  M? (ex_comparable_ty * Alpha_context.context).

Parameter parse_view_input_ty :
  Alpha_context.context -> int -> bool -> Alpha_context.Script.node ->
  M? (ex_ty * Alpha_context.context).

Parameter parse_view_output_ty :
  Alpha_context.context -> int -> bool -> Alpha_context.Script.node ->
  M? (ex_ty * Alpha_context.context).

Parameter parse_view_returning : forall {storage : Set},
  option type_logger -> Alpha_context.context -> bool -> Script_typed_ir.ty ->
  Script_typed_ir.view -> M=? (ex_view storage * Alpha_context.context).

Parameter typecheck_views :
  option type_logger -> Alpha_context.context -> bool -> Script_typed_ir.ty ->
  Script_typed_ir.SMap.(Map.S.t) Script_typed_ir.view ->
  M=? Alpha_context.context.

Parameter parse_any_ty :
  Alpha_context.context -> bool -> Alpha_context.Script.node ->
  M? (ex_ty * Alpha_context.context).

Parameter parse_ty :
  Alpha_context.context -> bool -> bool -> bool -> bool -> bool ->
  Alpha_context.Script.node -> M? (ex_ty * Alpha_context.context).

Parameter unparse_ty :
  Alpha_context.context -> Script_typed_ir.ty ->
  M? (Alpha_context.Script.node * Alpha_context.context).

Parameter ty_of_comparable_ty :
  Script_typed_ir.comparable_ty -> Script_typed_ir.ty.

Parameter parse_toplevel :
  Alpha_context.context -> bool -> Alpha_context.Script.expr ->
  M? (toplevel * Alpha_context.context).

Parameter add_field_annot :
  option Script_typed_ir.field_annot -> option Script_typed_ir.var_annot ->
  Alpha_context.Script.node -> Alpha_context.Script.node.

Parameter typecheck_code :
  bool -> Alpha_context.context -> Alpha_context.Script.expr ->
  M=? (Script_tc_errors.type_map * Alpha_context.context).

Parameter serialize_ty_for_error :
  Alpha_context.context -> Script_typed_ir.ty ->
  M? (Alpha_context.Script.expr * Alpha_context.context).

Parameter parse_code :
  option type_logger -> Alpha_context.context -> bool ->
  Alpha_context.Script.lazy_expr -> M=? (ex_code * Alpha_context.context).

Parameter parse_storage : forall {storage : Set},
  option type_logger -> Alpha_context.context -> bool -> bool ->
  Script_typed_ir.ty -> Alpha_context.Script.lazy_expr ->
  M=? (storage * Alpha_context.context).

Parameter parse_script :
  option type_logger -> Alpha_context.context -> bool -> bool ->
  Alpha_context.Script.t -> M=? (ex_script * Alpha_context.context).

Parameter unparse_script : forall {b : Set},
  Alpha_context.context -> unparsing_mode -> Script_typed_ir.script b ->
  M=? (Alpha_context.Script.t * Alpha_context.context).

Parameter parse_contract :
  bool -> Alpha_context.context -> Alpha_context.Script.location ->
  Script_typed_ir.ty -> Alpha_context.Contract.t -> string ->
  M=? (Alpha_context.context * Script_typed_ir.typed_contract).

Parameter parse_contract_for_script :
  Alpha_context.context -> Alpha_context.Script.location ->
  Script_typed_ir.ty -> Alpha_context.Contract.t -> string ->
  M=? (Alpha_context.context * option Script_typed_ir.typed_contract).

Parameter find_entrypoint :
  Script_typed_ir.ty -> option Script_typed_ir.field_annot -> string ->
  M? ((Alpha_context.Script.node -> Alpha_context.Script.node) * ex_ty).

Parameter Entrypoints_map_t : Set -> Set.

Parameter Entrypoints_map : Map.S (key := string) (t := Entrypoints_map_t).

Parameter list_entrypoints :
  Script_typed_ir.ty -> Alpha_context.context ->
  option Script_typed_ir.field_annot ->
  M?
    (list (list Michelson_v1_primitives.prim) *
      Entrypoints_map.(Map.S.t)
        (list Michelson_v1_primitives.prim * Alpha_context.Script.node)).

Parameter pack_data : forall {a : Set},
  Alpha_context.context -> Script_typed_ir.ty -> a ->
  M=? (bytes * Alpha_context.context).

Parameter hash_comparable_data : forall {a : Set},
  Alpha_context.context -> Script_typed_ir.comparable_ty -> a ->
  M=? (Script_expr_hash.t * Alpha_context.context).

Parameter hash_data : forall {a : Set},
  Alpha_context.context -> Script_typed_ir.ty -> a ->
  M=? (Script_expr_hash.t * Alpha_context.context).

Parameter lazy_storage_ids : Set.

Parameter no_lazy_storage_id : lazy_storage_ids.

Parameter collect_lazy_storage : forall {a : Set},
  Alpha_context.context -> Script_typed_ir.ty -> a ->
  M? (lazy_storage_ids * Alpha_context.context).

Parameter list_of_big_map_ids :
  lazy_storage_ids -> list Alpha_context.Big_map.Id.t.

Parameter extract_lazy_storage_diff : forall {a : Set},
  Alpha_context.context -> unparsing_mode -> bool -> lazy_storage_ids ->
  lazy_storage_ids -> Script_typed_ir.ty -> a ->
  M=? (a * option Alpha_context.Lazy_storage.diffs * Alpha_context.context).

Parameter get_single_sapling_state : forall {a : Set},
  Alpha_context.context -> Script_typed_ir.ty -> a ->
  M? (option Alpha_context.Sapling.Id.t * Alpha_context.context).

Parameter script_size : ex_script -> int * Gas_limit_repr.cost.
