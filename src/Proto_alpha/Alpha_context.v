Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Block_header_repr.
Require TezosOfOCaml.Proto_alpha.Bootstrap_storage.
Require TezosOfOCaml.Proto_alpha.Cache_costs.
Require TezosOfOCaml.Proto_alpha.Commitment_repr.
Require TezosOfOCaml.Proto_alpha.Commitment_storage.
Require TezosOfOCaml.Proto_alpha.Constants_repr.
Require TezosOfOCaml.Proto_alpha.Constants_storage.
Require TezosOfOCaml.Proto_alpha.Contract_repr.
Require TezosOfOCaml.Proto_alpha.Contract_storage.
Require TezosOfOCaml.Proto_alpha.Cycle_repr.
Require TezosOfOCaml.Proto_alpha.Delegate_storage.
Require TezosOfOCaml.Proto_alpha.Fees_storage.
Require TezosOfOCaml.Proto_alpha.Fitness_repr.
Require TezosOfOCaml.Proto_alpha.Fitness_storage.
Require TezosOfOCaml.Proto_alpha.Gas_limit_repr.
Require TezosOfOCaml.Proto_alpha.Global_constants_storage.
Require TezosOfOCaml.Proto_alpha.Init_storage.
Require TezosOfOCaml.Proto_alpha.Lazy_storage_diff.
Require TezosOfOCaml.Proto_alpha.Lazy_storage_kind.
Require TezosOfOCaml.Proto_alpha.Level_repr.
Require TezosOfOCaml.Proto_alpha.Level_storage.
Require TezosOfOCaml.Proto_alpha.Liquidity_baking_repr.
Require TezosOfOCaml.Proto_alpha.Michelson_v1_primitives.
Require TezosOfOCaml.Proto_alpha.Migration_repr.
Require TezosOfOCaml.Proto_alpha.Nonce_storage.
Require TezosOfOCaml.Proto_alpha.Operation_repr.
Require TezosOfOCaml.Proto_alpha.Parameters_repr.
Require TezosOfOCaml.Proto_alpha.Period_repr.
Require TezosOfOCaml.Proto_alpha.Raw_context.
Require TezosOfOCaml.Proto_alpha.Raw_level_repr.
Require TezosOfOCaml.Proto_alpha.Receipt_repr.
Require TezosOfOCaml.Proto_alpha.Roll_repr.
Require TezosOfOCaml.Proto_alpha.Roll_storage.
Require TezosOfOCaml.Proto_alpha.Sapling_repr.
Require TezosOfOCaml.Proto_alpha.Sapling_storage.
Require TezosOfOCaml.Proto_alpha.Sapling_validator.
Require TezosOfOCaml.Proto_alpha.Script_expr_hash.
Require TezosOfOCaml.Proto_alpha.Script_int_repr.
Require TezosOfOCaml.Proto_alpha.Script_repr.
Require TezosOfOCaml.Proto_alpha.Script_string_repr.
Require TezosOfOCaml.Proto_alpha.Script_timestamp_repr.
Require TezosOfOCaml.Proto_alpha.Seed_repr.
Require TezosOfOCaml.Proto_alpha.Seed_storage.
Require TezosOfOCaml.Proto_alpha.Storage.
Require TezosOfOCaml.Proto_alpha.Storage_description.
Require TezosOfOCaml.Proto_alpha.Storage_sigs.
Require TezosOfOCaml.Proto_alpha.Tez_repr.
Require TezosOfOCaml.Proto_alpha.Time_repr.
Require TezosOfOCaml.Proto_alpha.Vote_repr.
Require TezosOfOCaml.Proto_alpha.Vote_storage.
Require TezosOfOCaml.Proto_alpha.Voting_period_repr.
Require TezosOfOCaml.Proto_alpha.Voting_period_storage.

Definition t : Set := Raw_context.t.

Definition context : Set := t.

Module BASIC_DATA.
  Record signature {t : Set} : Set := {
    t := t;
    op_eq : t -> t -> bool;
    op_ltgt : t -> t -> bool;
    op_lt : t -> t -> bool;
    op_lteq : t -> t -> bool;
    op_gteq : t -> t -> bool;
    op_gt : t -> t -> bool;
    compare : t -> t -> int;
    equal : t -> t -> bool;
    max : t -> t -> t;
    min : t -> t -> t;
    encoding : Data_encoding.t t;
    pp : Format.formatter -> t -> unit;
  }.
End BASIC_DATA.
Definition BASIC_DATA := @BASIC_DATA.signature.
Arguments BASIC_DATA {_}.

Module Tez := Tez_repr.

Module Period := Period_repr.

Module Timestamp.
  Include Time_repr.
  
  Definition current : Raw_context.t -> Time.t := Raw_context.current_timestamp.
  
  Definition predecessor : Raw_context.t -> Time.t :=
    Raw_context.predecessor_timestamp.
End Timestamp.

Include Operation_repr.

Module Operation.
  Definition t : Set := operation.
  
  Definition packed : Set := packed_operation.
  
  Definition unsigned_encoding
    : Data_encoding.t (Operation.shell_header * packed_contents_list) :=
    unsigned_operation_encoding.
  
  Include Operation_repr.
End Operation.

Module Block_header := Block_header_repr.

Module Vote.
  Include Vote_repr.
  
  Include Vote_storage.
End Vote.

Module Raw_level := Raw_level_repr.

Module Cycle := Cycle_repr.

Module Script_string := Script_string_repr.

Module Script_int := Script_int_repr.

Module Script_timestamp.
  Include Script_timestamp_repr.
  
  Definition now (ctxt : Raw_context.t) : t :=
    let '{|
      Constants_repr.parametric.minimal_block_delay := minimal_block_delay
        |} := Raw_context.constants ctxt in
    let current_timestamp := Raw_context.predecessor_timestamp ctxt in
    of_int64
      (Timestamp.to_seconds
        (Time.add current_timestamp (Period_repr.to_seconds minimal_block_delay))).
End Script_timestamp.

Module Script.
  Include Michelson_v1_primitives.
  
  Include Script_repr.
  
  Definition force_decode_in_context
    (ctxt : Raw_context.t) (lexpr : Script_repr.lazy_expr)
    : M? (Script_repr.expr * Raw_context.t) :=
    let? ctxt :=
      Raw_context.consume_gas ctxt (Script_repr.force_decode_cost lexpr) in
    let? v := Script_repr.force_decode lexpr in
    return? (v, ctxt).
  
  Definition force_bytes_in_context
    (ctxt : Raw_context.t) (lexpr : Script_repr.lazy_expr)
    : M? (bytes * Raw_context.t) :=
    let? ctxt :=
      Raw_context.consume_gas ctxt (Script_repr.force_bytes_cost lexpr) in
    let? v := Script_repr.force_bytes lexpr in
    return? (v, ctxt).
End Script.

Module Fees := Fees_storage.

Definition public_key : Set := Signature.public_key.

Definition public_key_hash : Set := Signature.public_key_hash.

Definition signature : Set := Signature.t.

Module Constants.
  Include Constants_repr.
  
  Include Constants_storage.
End Constants.

Module Voting_period.
  Include Voting_period_repr.
  
  Include Voting_period_storage.
End Voting_period.

Module Gas.
  Include Gas_limit_repr.
  
  Definition check_limit_is_valid
    : Raw_context.t -> Gas_limit_repr.Arith.t -> M? unit :=
    Raw_context.check_gas_limit_is_valid.
  
  Definition set_limit
    : Raw_context.t -> Gas_limit_repr.Arith.t -> Raw_context.t :=
    Raw_context.set_gas_limit.
  
  Definition consume_limit_in_block
    : Raw_context.t -> Gas_limit_repr.Arith.t -> M? Raw_context.t :=
    Raw_context.consume_gas_limit_in_block.
  
  Definition set_unlimited : Raw_context.t -> Raw_context.t :=
    Raw_context.set_gas_unlimited.
  
  Definition consume
    : Raw_context.t -> Gas_limit_repr.cost -> M? Raw_context.t :=
    Raw_context.consume_gas.
  
  Definition remaining_operation_gas
    : Raw_context.t -> Gas_limit_repr.Arith.fp :=
    Raw_context.remaining_operation_gas.
  
  Definition update_remaining_operation_gas
    : Raw_context.t -> Gas_limit_repr.Arith.fp -> Raw_context.t :=
    Raw_context.update_remaining_operation_gas.
  
  Definition gas_exhausted_error {A : Set} : Raw_context.t -> M? A :=
    Raw_context.gas_exhausted_error.
  
  Definition level : Raw_context.t -> Gas_limit_repr.t := Raw_context.gas_level.
  
  Definition consumed
    : Raw_context.t -> Raw_context.t -> Gas_limit_repr.Arith.fp :=
    Raw_context.gas_consumed.
  
  Definition block_level : Raw_context.t -> Gas_limit_repr.Arith.fp :=
    Raw_context.block_gas_level.
  
  Definition cost_of_repr {A : Set} (cost : A) : A := cost.
End Gas.

Module Level.
  Include Level_repr.
  
  Include Level_storage.
End Level.

Module Lazy_storage.
  Module Kind := Lazy_storage_kind.
  
  Module IdSet := Kind.IdSet.
  
  Include Lazy_storage_diff.
  
  Definition legacy_big_map_diff_encoding
    : Data_encoding.encoding Lazy_storage_diff.diffs :=
    Data_encoding.conv Contract_storage.Legacy_big_map_diff.of_lazy_storage_diff
      Contract_storage.Legacy_big_map_diff.to_lazy_storage_diff None
      Contract_storage.Legacy_big_map_diff.encoding.
End Lazy_storage.

Module Contract.
  Include Contract_repr.
  
  Include Contract_storage.
  
  Definition originate
    (c : Raw_context.t) (contract : Contract_repr.t) (balance : Tez_repr.t)
    (script : Script_repr.t * option Lazy_storage_diff.diffs)
    (delegate : option Signature.public_key_hash) : M=? Raw_context.t :=
    raw_originate c None contract balance script delegate.
  
  Definition init_origination_nonce
    : Raw_context.t -> Operation_hash.t -> Raw_context.t :=
    Raw_context.init_origination_nonce.
  
  Definition unset_origination_nonce : Raw_context.t -> Raw_context.t :=
    Raw_context.unset_origination_nonce.
End Contract.

Module Global_constants_storage := Global_constants_storage.

Module Big_map.
  Module Big_map := Lazy_storage_kind.Big_map.
  
  Module Id.
    Definition t : Set := Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.ID.t).
    
    Definition encoding
      : Data_encoding.t Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.ID.t) :=
      Big_map.Id.(Lazy_storage_kind.ID.encoding).
    
    Definition rpc_arg
      : RPC_arg.arg Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.ID.t) :=
      Big_map.Id.(Lazy_storage_kind.ID.rpc_arg).
    
    Definition parse_z
      : Z.t -> Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.ID.t) :=
      Big_map.Id.(Lazy_storage_kind.ID.parse_z).
    
    Definition unparse_to_z
      : Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.ID.t) -> Z.t :=
      Big_map.Id.(Lazy_storage_kind.ID.unparse_to_z).
  End Id.
  
  Definition fresh (temporary : bool) (c : Raw_context.t)
    : M=?
      (Raw_context.t * Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.ID.t)) :=
    Lazy_storage.fresh Lazy_storage_kind.Big_map temporary c.
  
  Definition mem
    (c : Raw_context.t) (m : Storage.Big_map.id) (k : Script_expr_hash.t)
    : M=? (Raw_context.t * bool) :=
    Storage.Big_map.Contents.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.mem)
      (c, m) k.
  
  Definition get_opt
    (c : Raw_context.t) (m : Storage.Big_map.id) (k : Script_expr_hash.t)
    : M=? (Raw_context.t * option Script_repr.expr) :=
    Storage.Big_map.Contents.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.find)
      (c, m) k.
  
  Definition list_values
    (offset : option int) (length : option int) (c : Raw_context.t)
    (m : Storage.Big_map.id) : M=? (Raw_context.t * list Script_repr.expr) :=
    Storage.Big_map.Contents.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.list_values)
      offset length (c, m).
  
  Definition _exists
    (c : Raw_context.t)
    (id : Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.ID.t))
    : M=? (Raw_context.t * option (Script_repr.expr * Script_repr.expr)) :=
    let=? c :=
      return= (Raw_context.consume_gas c (Gas_limit_repr.read_bytes_cost 0)) in
    let=? kt :=
      Storage.Big_map.Key_type.(Storage_sigs.Indexed_data_storage.find) c id in
    match kt with
    | None => return=? (c, None)
    | Some kt =>
      let=? kv :=
        Storage.Big_map.Value_type.(Storage_sigs.Indexed_data_storage.get) c id
        in
      return=? (c, (Some (kt, kv)))
    end.
  
  Definition update : Set := Big_map.update.
  
  Definition updates : Set := Big_map.updates.
  
  Definition alloc : Set := Big_map.alloc.
End Big_map.

Module Sapling.
  Module Sapling_state := Lazy_storage_kind.Sapling_state.
  
  Module Id.
    Definition t : Set :=
      Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.ID.t).
    
    Definition encoding
      : Data_encoding.t
        Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.ID.t) :=
      Sapling_state.Id.(Lazy_storage_kind.ID.encoding).
    
    Definition rpc_arg
      : RPC_arg.arg Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.ID.t) :=
      Sapling_state.Id.(Lazy_storage_kind.ID.rpc_arg).
    
    Definition parse_z
      : Z.t -> Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.ID.t) :=
      Sapling_state.Id.(Lazy_storage_kind.ID.parse_z).
    
    Definition unparse_to_z
      : Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.ID.t) -> Z.t :=
      Sapling_state.Id.(Lazy_storage_kind.ID.unparse_to_z).
  End Id.
  
  Include Sapling_repr.
  
  Include Sapling_storage.
  
  Include Sapling_validator.
  
  Definition fresh (temporary : bool) (c : Raw_context.t)
    : M=?
      (Raw_context.t *
        Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.ID.t)) :=
    Lazy_storage.fresh Lazy_storage_kind.Sapling_state temporary c.
  
  Definition updates : Set := Sapling_state.updates.
  
  Definition alloc : Set := Sapling_state.alloc.
End Sapling.

Module Receipt := Receipt_repr.

Module Delegate := Delegate_storage.

Module Roll.
  Include Roll_repr.
  
  Include Roll_storage.
End Roll.

Module Nonce := Nonce_storage.

Module Seed.
  Include Seed_repr.
  
  Include Seed_storage.
End Seed.

Module Fitness.
  Include Fitness_repr.
  
  Include Environment.Fitness.
  
  Definition fitness : Set := t.
  
  Include Fitness_storage.
End Fitness.

Module Bootstrap := Bootstrap_storage.

Module Commitment.
  Include Commitment_repr.
  
  Include Commitment_storage.
End Commitment.

Module Global.
  Definition get_block_priority : Raw_context.t -> M=? int :=
    Storage.Block_priority.(Storage.Simple_single_data_storage.get).
  
  Definition set_block_priority : Raw_context.t -> int -> M=? Raw_context.t :=
    Storage.Block_priority.(Storage.Simple_single_data_storage.update).
End Global.

Module Migration := Migration_repr.

Definition prepare_first_block
  : Context.t ->
  (Raw_context.t -> Script_repr.t ->
  M=? ((Script_repr.t * option Lazy_storage_diff.diffs) * Raw_context.t)) ->
  int32 -> Time.t -> Fitness.t -> M=? Raw_context.t :=
  Init_storage.prepare_first_block.

Definition prepare
  : Context.t -> Int32.t -> Time.t -> Time.t -> Fitness.t ->
  M=?
    (Raw_context.t * Receipt_repr.balance_updates *
      list Migration_repr.origination_result) := Init_storage.prepare.

Definition max_operations_ttl : int := 120.

Definition finalize (message : option string) (c : Raw_context.t)
  : Updater.validation_result :=
  let fitness := Fitness.from_int64 (Fitness.current c) in
  let context_value := Raw_context.recover c in
  {| Updater.validation_result.context := context_value;
    Updater.validation_result.fitness := fitness;
    Updater.validation_result.message := message;
    Updater.validation_result.max_operations_ttl := max_operations_ttl;
    Updater.validation_result.last_allowed_fork_level :=
      Raw_level.to_int32 (Level.last_allowed_fork_level c) |}.

Definition activate : Raw_context.t -> Protocol_hash.t -> M= Raw_context.t :=
  Raw_context.activate.

Definition record_endorsement
  : Raw_context.t -> Signature.public_key_hash -> Raw_context.t :=
  Raw_context.record_endorsement.

Definition allowed_endorsements
  : Raw_context.t ->
  Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.t)
    (Signature.public_key * list int * bool) :=
  Raw_context.allowed_endorsements.

Definition init_endorsements
  : Raw_context.t ->
  Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.t)
    (Signature.public_key * list int * bool) -> Raw_context.t :=
  Raw_context.init_endorsements.

Definition included_endorsements : Raw_context.t -> int :=
  Raw_context.included_endorsements.

Definition reset_internal_nonce : Raw_context.t -> Raw_context.t :=
  Raw_context.reset_internal_nonce.

Definition fresh_internal_nonce : Raw_context.t -> M? (Raw_context.t * int) :=
  Raw_context.fresh_internal_nonce.

Definition record_internal_nonce : Raw_context.t -> int -> Raw_context.t :=
  Raw_context.record_internal_nonce.

Definition internal_nonce_already_recorded : Raw_context.t -> int -> bool :=
  Raw_context.internal_nonce_already_recorded.

Definition add_fees : Raw_context.t -> Tez_repr.t -> M? Raw_context.t :=
  Raw_context.add_fees.

Definition add_rewards : Raw_context.t -> Tez_repr.t -> M? Raw_context.t :=
  Raw_context.add_rewards.

Definition get_fees : Raw_context.t -> Tez_repr.t := Raw_context.get_fees.

Definition get_rewards : Raw_context.t -> Tez_repr.t := Raw_context.get_rewards.

Definition description : Storage_description.t Raw_context.t :=
  Raw_context.description.

Module Parameters := Parameters_repr.

Module Liquidity_baking := Liquidity_baking_repr.

Module Cache.
  Definition index : Set := int.
  
  Definition size : Set := int.
  
  Definition identifier : Set := string.
  
  Definition namespace : Set := string.
  
  Definition compare_namespace : string -> string -> int :=
    Compare.String.(Compare.S.compare).
  
  Module internal_identifier.
    Record record : Set := Build {
      namespace : namespace;
      id : identifier }.
    Definition with_namespace namespace (r : record) :=
      Build namespace r.(id).
    Definition with_id id (r : record) :=
      Build r.(namespace) id.
  End internal_identifier.
  Definition internal_identifier := internal_identifier.record.
  
  Definition separator : ascii := "@" % char.
  
  Definition sanitize (namespace_value : string) : string :=
    if String.contains namespace_value separator then
      Pervasives.invalid_arg
        (Format.asprintf
          (CamlinternalFormatBasics.Format
            (CamlinternalFormatBasics.String_literal
              "Invalid cache namespace: '"
              (CamlinternalFormatBasics.String
                CamlinternalFormatBasics.No_padding
                (CamlinternalFormatBasics.String_literal "'. Character "
                  (CamlinternalFormatBasics.Char
                    (CamlinternalFormatBasics.String_literal " is forbidden."
                      CamlinternalFormatBasics.End_of_format)))))
            "Invalid cache namespace: '%s'. Character %c is forbidden.")
          namespace_value separator)
    else
      namespace_value.
  
  Definition string_of_internal_identifier
    (function_parameter : internal_identifier) : string :=
    let '{|
      internal_identifier.namespace := namespace_value;
        internal_identifier.id := id
        |} := function_parameter in
    Pervasives.op_caret namespace_value
      (Pervasives.op_caret (String.make 1 separator) id).
  
  Definition internal_identifier_of_string (raw_value : string)
    : internal_identifier :=
    match String.split_on_char separator raw_value with
    | [] =>
      (* ❌ Assert instruction is not handled. *)
      assert internal_identifier false
    | cons namespace_value id =>
      {| internal_identifier.namespace := sanitize namespace_value;
        internal_identifier.id := String.concat "" id |}
    end.
  
  Definition internal_identifier_of_key (key_value : Context.cache_key)
    : internal_identifier :=
    let raw_value :=
      Raw_context.Cache.(Context.CACHE.identifier_of_key) key_value in
    internal_identifier_of_string raw_value.
  
  Definition key_of_internal_identifier
    (cache_index : int) (identifier : internal_identifier)
    : Context.cache_key :=
    let raw_value := string_of_internal_identifier identifier in
    Raw_context.Cache.(Context.CACHE.key_of_identifier) cache_index raw_value.
  
  Definition make_key : int -> String.t -> identifier -> Context.cache_key :=
    let namespaces := Pervasives.ref_value nil in
    fun (cache_index : int) =>
      fun (namespace_value : String.t) =>
        let namespace_value := sanitize namespace_value in
        if
          List.mem String.equal namespace_value
            (Pervasives.op_exclamation namespaces)
        then
          Pervasives.invalid_arg
            (Format.sprintf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "Cache key namespace "
                  (CamlinternalFormatBasics.String
                    CamlinternalFormatBasics.No_padding
                    (CamlinternalFormatBasics.String_literal " already exist."
                      CamlinternalFormatBasics.End_of_format)))
                "Cache key namespace %s already exist.") namespace_value)
        else
          let '_ :=
            Pervasives.op_coloneq namespaces
              (cons namespace_value (Pervasives.op_exclamation namespaces)) in
          fun (id : identifier) =>
            let identifier :=
              {| internal_identifier.namespace := namespace_value;
                internal_identifier.id := id |} in
            key_of_internal_identifier cache_index identifier.
  
  Definition NamespaceMap :=
    Map.Make
      (let t : Set := namespace in
      let compare := compare_namespace in
      {|
        Compare.COMPARABLE.compare := compare
      |}).
  
  Definition partial_key_handler : Set :=
    t -> string -> M=? Context.cache_value.
  
  Definition value_of_key_handlers
    : Pervasives.ref (NamespaceMap.(Map.S.t) partial_key_handler) :=
    Pervasives.ref_value NamespaceMap.(Map.S.empty).
  
  Module Admin.
    (** Inclusion of the module [Raw_context.Cache] *)
    Definition key := Raw_context.Cache.(Context.CACHE.key).
    
    Definition value := Raw_context.Cache.(Context.CACHE.value).
    
    Definition key_of_identifier :=
      Raw_context.Cache.(Context.CACHE.key_of_identifier).
    
    Definition identifier_of_key :=
      Raw_context.Cache.(Context.CACHE.identifier_of_key).
    
    Definition pp := Raw_context.Cache.(Context.CACHE.pp).
    
    Definition find := Raw_context.Cache.(Context.CACHE.find).
    
    Definition set_cache_layout :=
      Raw_context.Cache.(Context.CACHE.set_cache_layout).
    
    Definition update := Raw_context.Cache.(Context.CACHE.update).
    
    Definition sync := Raw_context.Cache.(Context.CACHE.sync).
    
    Definition clear := Raw_context.Cache.(Context.CACHE.clear).
    
    
    Definition future_cache_expectation :=
      Raw_context.Cache.(Context.CACHE.future_cache_expectation).
    
    Definition cache_size := Raw_context.Cache.(Context.CACHE.cache_size).
    
    Definition cache_size_limit :=
      Raw_context.Cache.(Context.CACHE.cache_size_limit).
    
    Definition list_keys (context_value : Raw_context.t) (cache_index : int)
      : option (list (Context.cache_key * int)) :=
      Raw_context.Cache.(Context.CACHE.list_keys) context_value cache_index.
    
    Definition key_rank
      (context_value : Raw_context.t) (key_value : Context.cache_key)
      : option int :=
      Raw_context.Cache.(Context.CACHE.key_rank) context_value key_value.
    
    Definition value_of_key
      (ctxt : Raw_context.t) (key_value : Context.cache_key)
      : M=? Context.cache_value :=
      let ctxt := Gas.set_unlimited ctxt in
      let '{|
        internal_identifier.namespace := namespace_value;
          internal_identifier.id := id
          |} := internal_identifier_of_key key_value in
      match
        NamespaceMap.(Map.S.find) namespace_value
          (Pervasives.op_exclamation value_of_key_handlers) with
      | Some value_of_key => value_of_key ctxt id
      | None =>
        Pervasives.failwith
          (Format.sprintf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal "No handler for key `"
                (CamlinternalFormatBasics.String
                  CamlinternalFormatBasics.No_padding
                  (CamlinternalFormatBasics.Char
                    (CamlinternalFormatBasics.String
                      CamlinternalFormatBasics.No_padding
                      (CamlinternalFormatBasics.Char_literal "'" % char
                        CamlinternalFormatBasics.End_of_format)))))
              "No handler for key `%s%c%s'") namespace_value separator id)
      end.
  End Admin.
  
  Module CLIENT.
    Record signature {cached_value : Set} : Set := {
      cache_index : int;
      namespace_value : namespace;
      cached_value := cached_value;
      value_of_identifier : t -> identifier -> M=? cached_value;
    }.
  End CLIENT.
  Definition CLIENT := @CLIENT.signature.
  Arguments CLIENT {_}.
  
  Module INTERFACE.
    Record signature {cached_value : Set} : Set := {
      cached_value := cached_value;
      update : t -> identifier -> option (cached_value * int) -> M? t;
      find : t -> identifier -> M=? (option cached_value);
      list_identifiers : t -> list (identifier * int);
      identifier_rank : t -> identifier -> option int;
      size_value : context -> size;
      size_limit : context -> size;
    }.
  End INTERFACE.
  Definition INTERFACE := @INTERFACE.signature.
  Arguments INTERFACE {_}.
  
  Definition register_exn {cvalue : Set}
    (C : {_ : unit @ CLIENT (cached_value := cvalue)})
    : {_ : unit @ INTERFACE (cached_value := cvalue)} :=
    let 'existS _ _ C := C in
    let '_ :=
      if
        (C.(CLIENT.cache_index) <i 0) ||
        (C.(CLIENT.cache_index) >=i (List.length Constants_repr.cache_layout))
      then
        Pervasives.invalid_arg "Cache index is invalid"
      else
        tt in
    let mk := make_key C.(CLIENT.cache_index) C.(CLIENT.namespace_value) in
    existS (A := unit) (fun _ => _) tt
      (let cached_value : Set := cvalue in
      (* ❌ top_level_evaluation *)
      let size_value (ctxt : Raw_context.t) : int :=
        (fun x_1 =>
          Option.value x_1 Pervasives.max_int)
            (Admin.cache_size ctxt C.(CLIENT.cache_index)) in
      let size_limit (ctxt : Raw_context.t) : int :=
        (fun x_1 =>
          Option.value x_1 Pervasives.max_int)
            (Admin.cache_size_limit ctxt C.(CLIENT.cache_index)) in
      let update
        (ctxt : Raw_context.t) (id : identifier)
        (v : option (cached_value * int)) : M? Raw_context.t :=
        let cache_size_in_bytes := size_value ctxt in
        let? ctxt :=
          Raw_context.consume_gas ctxt
            (Cache_costs.cache_update cache_size_in_bytes) in
        let v :=
          Option.map
            (fun (function_parameter : cached_value * int) =>
              let '(v, size_value) := function_parameter in
              ((Build_extensible "K" cached_value v), size_value)) v in
        return? (Admin.update ctxt (mk id) v) in
      let find (ctxt : Raw_context.t) (id : identifier)
        : M=? (option cached_value) :=
        let cache_size_in_bytes := size_value ctxt in
        let=? ctxt :=
          return=
            (Raw_context.consume_gas ctxt
              (Cache_costs.cache_update cache_size_in_bytes)) in
        let= function_parameter := Admin.find ctxt (mk id) in
        match function_parameter with
        | None => return=? None
        | Some value =>
          match value with
          | Build_extensible tag _ payload =>
            if String.eqb tag "K" then
              let 'v := cast cached_value payload in
              return=? (Some v)
            else
              (* ❌ Assert instruction is not handled. *)
              assert (M=? (option cached_value)) false
          end
        end in
      let list_identifiers (ctxt : Raw_context.t) : list (identifier * int) :=
        (fun (function_parameter : option (list (Context.cache_key * int))) =>
          match function_parameter with
          | None =>
            (* ❌ Assert instruction is not handled. *)
            assert (list (identifier * int)) false
          | Some list_value =>
            List.filter_map
              (fun (function_parameter : Context.cache_key * int) =>
                let '(key_value, age) := function_parameter in
                let '{|
                  internal_identifier.namespace := namespace_value;
                    internal_identifier.id := id
                    |} := internal_identifier_of_key key_value in
                if
                  String.equal namespace_value C.(CLIENT.namespace_value)
                then
                  Some (id, age)
                else
                  None) list_value
          end) (Admin.list_keys ctxt C.(CLIENT.cache_index)) in
      let identifier_rank (ctxt : Raw_context.t) (id : identifier)
        : option int :=
        Admin.key_rank ctxt (mk id) in
      {|
        INTERFACE.update := update;
        INTERFACE.find := find;
        INTERFACE.list_identifiers := list_identifiers;
        INTERFACE.identifier_rank := identifier_rank;
        INTERFACE.size_value := size_value;
        INTERFACE.size_limit := size_limit
      |}).
End Cache.
