Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Michelson_v1_primitives.
Require TezosOfOCaml.Proto_alpha.Script_tc_errors.

Definition type_map_enc
  : Data_encoding.encoding
    (list
      (Alpha_context.Script.location *
        (list (Alpha_context.Script.expr * list string) *
          list (Alpha_context.Script.expr * list string)))) :=
  let stack_enc :=
    Data_encoding.list_value None
      (Data_encoding.tup2 Alpha_context.Script.expr_encoding
        (Data_encoding.list_value None Data_encoding.string_value)) in
  Data_encoding.list_value None
    (Data_encoding.conv
      (fun (function_parameter :
        Alpha_context.Script.location *
          (list (Alpha_context.Script.expr * list string) *
            list (Alpha_context.Script.expr * list string))) =>
        let '(loc, (bef, aft)) := function_parameter in
        (loc, bef, aft))
      (fun (function_parameter :
        Alpha_context.Script.location *
          list (Alpha_context.Script.expr * list string) *
          list (Alpha_context.Script.expr * list string)) =>
        let '(loc, bef, aft) := function_parameter in
        (loc, (bef, aft))) None
      (Data_encoding.obj3
        (Data_encoding.req None None "location"
          Alpha_context.Script.location_encoding)
        (Data_encoding.req None None "stack_before" stack_enc)
        (Data_encoding.req None None "stack_after" stack_enc))).

Definition stack_ty_enc
  : Data_encoding.encoding (list (Alpha_context.Script.expr * list string)) :=
  Data_encoding.list_value None
    (Data_encoding.obj2
      (Data_encoding.req None None "type" Alpha_context.Script.expr_encoding)
      (Data_encoding.dft None None "annots"
        (Data_encoding.list_value None Data_encoding.string_value) nil)).

(** Init function; without side-effects in Coq *)
Definition init_module : unit :=
  let located {A : Set} (enc : Data_encoding.encoding A)
    : Data_encoding.encoding (Alpha_context.Script.location * A) :=
    Data_encoding.merge_objs
      (Data_encoding.obj1
        (Data_encoding.req None None "location"
          Alpha_context.Script.location_encoding)) enc in
  let arity_enc := Data_encoding.int8 in
  let namespace_enc :=
    Data_encoding.def "primitiveNamespace" (Some "Primitive namespace")
      (Some
        "One of the five possible namespaces of primitive (data constructor, type name, instruction, keyword, or constant hash).")
      (Data_encoding.string_enum
        [
          ("type", Michelson_v1_primitives.Type_namespace);
          ("constant", Michelson_v1_primitives.Constant_namespace);
          ("instruction", Michelson_v1_primitives.Instr_namespace);
          ("keyword", Michelson_v1_primitives.Keyword_namespace);
          ("constant_hash", Michelson_v1_primitives.Constant_hash_namespace)
        ]) in
  let kind_enc :=
    Data_encoding.def "expressionKind" (Some "Expression kind")
      (Some
        "One of the four possible kinds of expression (integer, string, primitive application or sequence).")
      (Data_encoding.string_enum
        [
          ("integer", Script_tc_errors.Int_kind);
          ("string", Script_tc_errors.String_kind);
          ("bytes", Script_tc_errors.Bytes_kind);
          ("primitiveApplication", Script_tc_errors.Prim_kind);
          ("sequence", Script_tc_errors.Seq_kind)
        ]) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.invalid_arity" "Invalid arity"
      "In a script or data expression, a primitive was applied to an unsupported number of arguments."
      None
      (located
        (Data_encoding.obj3
          (Data_encoding.req None None "primitive_name"
            Alpha_context.Script.prim_encoding)
          (Data_encoding.req None None "expected_arity" arity_enc)
          (Data_encoding.req None None "wrong_arity" arity_enc)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_arity" then
            let '(loc, name, exp, got) :=
              cast
                (Alpha_context.Script.location * Alpha_context.Script.prim * int
                  * int) payload in
            Some (loc, (name, exp, got))
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location * (Alpha_context.Script.prim * int * int))
        =>
        let '(loc, (name, exp, got)) := function_parameter in
        Build_extensible "Invalid_arity"
          (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
          (loc, name, exp, got)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.invalid_seq_arity" "Invalid sequence arity"
      "In a script or data expression, a sequence was used with a number of elements too small."
      None
      (located
        (Data_encoding.obj2
          (Data_encoding.req None None "minimal_expected_arity" arity_enc)
          (Data_encoding.req None None "wrong_arity" arity_enc)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_seq_arity" then
            let '(loc, exp, got) :=
              cast (Alpha_context.Script.location * int * int) payload in
            Some (loc, (exp, got))
          else None
        end)
      (fun (function_parameter : Alpha_context.Script.location * (int * int)) =>
        let '(loc, (exp, got)) := function_parameter in
        Build_extensible "Invalid_seq_arity"
          (Alpha_context.Script.location * int * int) (loc, exp, got)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.missing_script_field"
      "Script is missing a field (parse error)"
      "When parsing script, a field was expected, but not provided" None
      (Data_encoding.obj1
        (Data_encoding.req None None "prim" Alpha_context.Script.prim_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Missing_field" then
            let 'prim := cast Alpha_context.Script.prim payload in
            Some prim
          else None
        end)
      (fun (prim : Alpha_context.Script.prim) =>
        Build_extensible "Missing_field" Alpha_context.Script.prim prim) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.invalid_primitive" "Invalid primitive"
      "In a script or data expression, a primitive was unknown." None
      (located
        (Data_encoding.obj2
          (Data_encoding.dft None None "expected_primitive_names"
            (Data_encoding.list_value None Alpha_context.Script.prim_encoding)
            nil)
          (Data_encoding.req None None "wrong_primitive_name"
            Alpha_context.Script.prim_encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_primitive" then
            let '(loc, exp, got) :=
              cast
                (Alpha_context.Script.location * list Alpha_context.Script.prim
                  * Alpha_context.Script.prim) payload in
            Some (loc, (exp, got))
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location *
          (list Alpha_context.Script.prim * Alpha_context.Script.prim)) =>
        let '(loc, (exp, got)) := function_parameter in
        Build_extensible "Invalid_primitive"
          (Alpha_context.Script.location * list Alpha_context.Script.prim *
            Alpha_context.Script.prim) (loc, exp, got)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.invalid_expression_kind" "Invalid expression kind"
      "In a script or data expression, an expression was of the wrong kind (for instance a string where only a primitive applications can appear)."
      None
      (located
        (Data_encoding.obj2
          (Data_encoding.req None None "expected_kinds"
            (Data_encoding.list_value None kind_enc))
          (Data_encoding.req None None "wrong_kind" kind_enc)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_kind" then
            let '(loc, exp, got) :=
              cast
                (Alpha_context.Script.location * list Script_tc_errors.kind *
                  Script_tc_errors.kind) payload in
            Some (loc, (exp, got))
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location *
          (list Script_tc_errors.kind * Script_tc_errors.kind)) =>
        let '(loc, (exp, got)) := function_parameter in
        Build_extensible "Invalid_kind"
          (Alpha_context.Script.location * list Script_tc_errors.kind *
            Script_tc_errors.kind) (loc, exp, got)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.invalid_primitive_namespace" "Invalid primitive namespace"
      "In a script or data expression, a primitive was of the wrong namespace."
      None
      (located
        (Data_encoding.obj3
          (Data_encoding.req None None "primitive_name"
            Alpha_context.Script.prim_encoding)
          (Data_encoding.req None None "expected_namespace" namespace_enc)
          (Data_encoding.req None None "wrong_namespace" namespace_enc)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_namespace" then
            let '(loc, name, exp, got) :=
              cast
                (Alpha_context.Script.location * Alpha_context.Script.prim *
                  Michelson_v1_primitives.namespace *
                  Michelson_v1_primitives.namespace) payload in
            Some (loc, (name, exp, got))
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location *
          (Alpha_context.Script.prim * Michelson_v1_primitives.namespace *
            Michelson_v1_primitives.namespace)) =>
        let '(loc, (name, exp, got)) := function_parameter in
        Build_extensible "Invalid_namespace"
          (Alpha_context.Script.location * Alpha_context.Script.prim *
            Michelson_v1_primitives.namespace *
            Michelson_v1_primitives.namespace) (loc, name, exp, got)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.invalid_never_expr" "Invalid expression for type never"
      "In a script or data expression, an expression was provided but a value of type never was expected. No expression can have type never."
      None (located Data_encoding.unit_value)
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_never_expr" then
            let 'loc := cast Alpha_context.Script.location payload in
            Some (loc, tt)
          else None
        end)
      (fun (function_parameter : Alpha_context.Script.location * unit) =>
        let '(loc, _) := function_parameter in
        Build_extensible "Invalid_never_expr" Alpha_context.Script.location loc)
    in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.duplicate_script_field"
      "Script has a duplicated field (parse error)"
      "When parsing script, a field was found more than once" None
      (Data_encoding.obj2
        (Data_encoding.req None None "loc"
          Alpha_context.Script.location_encoding)
        (Data_encoding.req None None "prim" Alpha_context.Script.prim_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Duplicate_field" then
            let '(loc, prim) :=
              cast (Alpha_context.Script.location * Alpha_context.Script.prim)
                payload in
            Some (loc, prim)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location * Alpha_context.Script.prim) =>
        let '(loc, prim) := function_parameter in
        Build_extensible "Duplicate_field"
          (Alpha_context.Script.location * Alpha_context.Script.prim)
          (loc, prim)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.unexpected_lazy_storage"
      "Lazy storage in unauthorized position (type error)"
      "When parsing script, a big_map or sapling_state type was found in a position where it could end up stored inside a big_map, which is forbidden for now."
      None
      (Data_encoding.obj1
        (Data_encoding.req None None "loc"
          Alpha_context.Script.location_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Unexpected_lazy_storage" then
            let 'loc := cast Alpha_context.Script.location payload in
            Some loc
          else None
        end)
      (fun (loc : Alpha_context.Script.location) =>
        Build_extensible "Unexpected_lazy_storage" Alpha_context.Script.location
          loc) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.unexpected_operation"
      "Operation in unauthorized position (type error)"
      "When parsing script, an operation type was found in the storage or parameter field."
      None
      (Data_encoding.obj1
        (Data_encoding.req None None "loc"
          Alpha_context.Script.location_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Unexpected_operation" then
            let 'loc := cast Alpha_context.Script.location payload in
            Some loc
          else None
        end)
      (fun (loc : Alpha_context.Script.location) =>
        Build_extensible "Unexpected_operation" Alpha_context.Script.location
          loc) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.no_such_entrypoint" "No such entrypoint (type error)"
      "An entrypoint was not found when calling a contract." None
      (Data_encoding.obj1
        (Data_encoding.req None None "entrypoint" Data_encoding.string_value))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "No_such_entrypoint" then
            let 'entrypoint := cast string payload in
            Some entrypoint
          else None
        end)
      (fun (entrypoint : string) =>
        Build_extensible "No_such_entrypoint" string entrypoint) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.unreachable_entrypoint"
      "Unreachable entrypoint (type error)"
      "An entrypoint in the contract is not reachable." None
      (Data_encoding.obj1
        (Data_encoding.req None None "path"
          (Data_encoding.list_value None Alpha_context.Script.prim_encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Unreachable_entrypoint" then
            let 'path := cast (list Alpha_context.Script.prim) payload in
            Some path
          else None
        end)
      (fun (path : list Alpha_context.Script.prim) =>
        Build_extensible "Unreachable_entrypoint"
          (list Alpha_context.Script.prim) path) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.duplicate_entrypoint" "Duplicate entrypoint (type error)"
      "Two entrypoints have the same name." None
      (Data_encoding.obj1
        (Data_encoding.req None None "path" Data_encoding.string_value))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Duplicate_entrypoint" then
            let 'entrypoint := cast string payload in
            Some entrypoint
          else None
        end)
      (fun (entrypoint : string) =>
        Build_extensible "Duplicate_entrypoint" string entrypoint) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.entrypoint_name_too_long"
      "Entrypoint name too long (type error)"
      "An entrypoint name exceeds the maximum length of 31 characters." None
      (Data_encoding.obj1
        (Data_encoding.req None None "name" Data_encoding.string_value))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Entrypoint_name_too_long" then
            let 'entrypoint := cast string payload in
            Some entrypoint
          else None
        end)
      (fun (entrypoint : string) =>
        Build_extensible "Entrypoint_name_too_long" string entrypoint) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.unexpected_contract"
      "Contract in unauthorized position (type error)"
      "When parsing script, a contract type was found in the storage or parameter field."
      None
      (Data_encoding.obj1
        (Data_encoding.req None None "loc"
          Alpha_context.Script.location_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Unexpected_contract" then
            let 'loc := cast Alpha_context.Script.location payload in
            Some loc
          else None
        end)
      (fun (loc : Alpha_context.Script.location) =>
        Build_extensible "Unexpected_contract" Alpha_context.Script.location loc)
    in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.unordered_map_literal" "Invalid map key order"
      "Map keys must be in strictly increasing order" None
      (Data_encoding.obj2
        (Data_encoding.req None None "location"
          Alpha_context.Script.location_encoding)
        (Data_encoding.req None None "item" Alpha_context.Script.expr_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Unordered_map_keys" then
            let '(loc, expr) :=
              cast (Alpha_context.Script.location * Alpha_context.Script.expr)
                payload in
            Some (loc, expr)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location * Alpha_context.Script.expr) =>
        let '(loc, expr) := function_parameter in
        Build_extensible "Unordered_map_keys"
          (Alpha_context.Script.location * Alpha_context.Script.expr)
          (loc, expr)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.duplicate_map_keys" "Duplicate map keys"
      "Map literals cannot contain duplicated keys" None
      (Data_encoding.obj2
        (Data_encoding.req None None "location"
          Alpha_context.Script.location_encoding)
        (Data_encoding.req None None "item" Alpha_context.Script.expr_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Duplicate_map_keys" then
            let '(loc, expr) :=
              cast (Alpha_context.Script.location * Alpha_context.Script.expr)
                payload in
            Some (loc, expr)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location * Alpha_context.Script.expr) =>
        let '(loc, expr) := function_parameter in
        Build_extensible "Duplicate_map_keys"
          (Alpha_context.Script.location * Alpha_context.Script.expr)
          (loc, expr)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.unordered_set_literal" "Invalid set value order"
      "Set values must be in strictly increasing order" None
      (Data_encoding.obj2
        (Data_encoding.req None None "location"
          Alpha_context.Script.location_encoding)
        (Data_encoding.req None None "value" Alpha_context.Script.expr_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Unordered_set_values" then
            let '(loc, expr) :=
              cast (Alpha_context.Script.location * Alpha_context.Script.expr)
                payload in
            Some (loc, expr)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location * Alpha_context.Script.expr) =>
        let '(loc, expr) := function_parameter in
        Build_extensible "Unordered_set_values"
          (Alpha_context.Script.location * Alpha_context.Script.expr)
          (loc, expr)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.duplicate_set_values_in_literal"
      "Sets literals cannot contain duplicate elements"
      "Set literals cannot contain duplicate elements, but a duplicate was found while parsing."
      None
      (Data_encoding.obj2
        (Data_encoding.req None None "location"
          Alpha_context.Script.location_encoding)
        (Data_encoding.req None None "value" Alpha_context.Script.expr_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Duplicate_set_values" then
            let '(loc, expr) :=
              cast (Alpha_context.Script.location * Alpha_context.Script.expr)
                payload in
            Some (loc, expr)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location * Alpha_context.Script.expr) =>
        let '(loc, expr) := function_parameter in
        Build_extensible "Duplicate_set_values"
          (Alpha_context.Script.location * Alpha_context.Script.expr)
          (loc, expr)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.fail_not_in_tail_position" "FAIL not in tail position"
      "There is non trivial garbage code after a FAIL instruction." None
      (located Data_encoding.empty)
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Fail_not_in_tail_position" then
            let 'loc := cast Alpha_context.Script.location payload in
            Some (loc, tt)
          else None
        end)
      (fun (function_parameter : Alpha_context.Script.location * unit) =>
        let '(loc, _) := function_parameter in
        Build_extensible "Fail_not_in_tail_position"
          Alpha_context.Script.location loc) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.undefined_binop" "Undefined binop"
      "A binary operation is called on operands of types over which it is not defined."
      None
      (located
        (Data_encoding.obj3
          (Data_encoding.req None None "operator_name"
            Alpha_context.Script.prim_encoding)
          (Data_encoding.req None None "wrong_left_operand_type"
            Alpha_context.Script.expr_encoding)
          (Data_encoding.req None None "wrong_right_operand_type"
            Alpha_context.Script.expr_encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Undefined_binop" then
            let '(loc, n, tyl, tyr) :=
              cast
                (Alpha_context.Script.location * Alpha_context.Script.prim *
                  Alpha_context.Script.expr * Alpha_context.Script.expr) payload
                in
            Some (loc, (n, tyl, tyr))
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location *
          (Alpha_context.Script.prim * Alpha_context.Script.expr *
            Alpha_context.Script.expr)) =>
        let '(loc, (n, tyl, tyr)) := function_parameter in
        Build_extensible "Undefined_binop"
          (Alpha_context.Script.location * Alpha_context.Script.prim *
            Alpha_context.Script.expr * Alpha_context.Script.expr)
          (loc, n, tyl, tyr)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.undefined_unop" "Undefined unop"
      "A unary operation is called on an operand of type over which it is not defined."
      None
      (located
        (Data_encoding.obj2
          (Data_encoding.req None None "operator_name"
            Alpha_context.Script.prim_encoding)
          (Data_encoding.req None None "wrong_operand_type"
            Alpha_context.Script.expr_encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Undefined_unop" then
            let '(loc, n, ty) :=
              cast
                (Alpha_context.Script.location * Alpha_context.Script.prim *
                  Alpha_context.Script.expr) payload in
            Some (loc, (n, ty))
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location *
          (Alpha_context.Script.prim * Alpha_context.Script.expr)) =>
        let '(loc, (n, ty)) := function_parameter in
        Build_extensible "Undefined_unop"
          (Alpha_context.Script.location * Alpha_context.Script.prim *
            Alpha_context.Script.expr) (loc, n, ty)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.bad_return" "Bad return"
      "Unexpected stack at the end of a lambda or script." None
      (located
        (Data_encoding.obj2
          (Data_encoding.req None None "expected_return_type"
            Alpha_context.Script.expr_encoding)
          (Data_encoding.req None None "wrong_stack_type" stack_ty_enc)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Bad_return" then
            let '(loc, sty, ty) :=
              cast
                (Alpha_context.Script.location *
                  Script_tc_errors.unparsed_stack_ty * Alpha_context.Script.expr)
                payload in
            Some (loc, (ty, sty))
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location *
          (Alpha_context.Script.expr * Script_tc_errors.unparsed_stack_ty)) =>
        let '(loc, (ty, sty)) := function_parameter in
        Build_extensible "Bad_return"
          (Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty *
            Alpha_context.Script.expr) (loc, sty, ty)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.bad_stack" "Bad stack"
      "The stack has an unexpected length or contents." None
      (located
        (Data_encoding.obj3
          (Data_encoding.req None None "primitive_name"
            Alpha_context.Script.prim_encoding)
          (Data_encoding.req None None "relevant_stack_portion"
            Data_encoding.int16)
          (Data_encoding.req None None "wrong_stack_type" stack_ty_enc)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Bad_stack" then
            let '(loc, name, s, sty) :=
              cast
                (Alpha_context.Script.location * Alpha_context.Script.prim * int
                  * Script_tc_errors.unparsed_stack_ty) payload in
            Some (loc, (name, s, sty))
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location *
          (Alpha_context.Script.prim * int * Script_tc_errors.unparsed_stack_ty))
        =>
        let '(loc, (name, s, sty)) := function_parameter in
        Build_extensible "Bad_stack"
          (Alpha_context.Script.location * Alpha_context.Script.prim * int *
            Script_tc_errors.unparsed_stack_ty) (loc, name, s, sty)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.inconsistent_annotations"
      "Annotations inconsistent between branches"
      "The annotations on two types could not be merged" None
      (Data_encoding.obj2
        (Data_encoding.req None None "annot1" Data_encoding.string_value)
        (Data_encoding.req None None "annot2" Data_encoding.string_value))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Inconsistent_annotations" then
            let '(annot1, annot2) := cast (string * string) payload in
            Some (annot1, annot2)
          else None
        end)
      (fun (function_parameter : string * string) =>
        let '(annot1, annot2) := function_parameter in
        Build_extensible "Inconsistent_annotations" (string * string)
          (annot1, annot2)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.inconsistent_field_annotations"
      "Annotations for field accesses is inconsistent"
      "The specified field does not match the field annotation in the type" None
      (Data_encoding.obj2
        (Data_encoding.req None None "annot1" Data_encoding.string_value)
        (Data_encoding.req None None "annot2" Data_encoding.string_value))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Inconsistent_field_annotations" then
            let '(annot1, annot2) := cast (string * string) payload in
            Some (annot1, annot2)
          else None
        end)
      (fun (function_parameter : string * string) =>
        let '(annot1, annot2) := function_parameter in
        Build_extensible "Inconsistent_field_annotations" (string * string)
          (annot1, annot2)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.inconsistent_type_annotations"
      "Types contain inconsistent annotations"
      "The two types contain annotations that do not match" None
      (located
        (Data_encoding.obj2
          (Data_encoding.req None None "type1"
            Alpha_context.Script.expr_encoding)
          (Data_encoding.req None None "type2"
            Alpha_context.Script.expr_encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Inconsistent_type_annotations" then
            let '(loc, ty1, ty2) :=
              cast
                (Alpha_context.Script.location * Alpha_context.Script.expr *
                  Alpha_context.Script.expr) payload in
            Some (loc, (ty1, ty2))
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location *
          (Alpha_context.Script.expr * Alpha_context.Script.expr)) =>
        let '(loc, (ty1, ty2)) := function_parameter in
        Build_extensible "Inconsistent_type_annotations"
          (Alpha_context.Script.location * Alpha_context.Script.expr *
            Alpha_context.Script.expr) (loc, ty1, ty2)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.unexpected_annotation"
      "An annotation was encountered where no annotation is expected"
      "A node in the syntax tree was improperly annotated" None
      (located Data_encoding.empty)
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Unexpected_annotation" then
            let 'loc := cast Alpha_context.Script.location payload in
            Some (loc, tt)
          else None
        end)
      (fun (function_parameter : Alpha_context.Script.location * unit) =>
        let '(loc, _) := function_parameter in
        Build_extensible "Unexpected_annotation" Alpha_context.Script.location
          loc) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.ungrouped_annotations"
      "Annotations of the same kind were found spread apart"
      "Annotations of the same kind must be grouped" None
      (located Data_encoding.empty)
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Ungrouped_annotations" then
            let 'loc := cast Alpha_context.Script.location payload in
            Some (loc, tt)
          else None
        end)
      (fun (function_parameter : Alpha_context.Script.location * unit) =>
        let '(loc, _) := function_parameter in
        Build_extensible "Ungrouped_annotations" Alpha_context.Script.location
          loc) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.unmatched_branches" "Unmatched branches"
      "At the join point at the end of two code branches the stacks have inconsistent lengths or contents."
      None
      (located
        (Data_encoding.obj2
          (Data_encoding.req None None "first_stack_type" stack_ty_enc)
          (Data_encoding.req None None "other_stack_type" stack_ty_enc)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Unmatched_branches" then
            let '(loc, stya, styb) :=
              cast
                (Alpha_context.Script.location *
                  Script_tc_errors.unparsed_stack_ty *
                  Script_tc_errors.unparsed_stack_ty) payload in
            Some (loc, (stya, styb))
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location *
          (Script_tc_errors.unparsed_stack_ty *
            Script_tc_errors.unparsed_stack_ty)) =>
        let '(loc, (stya, styb)) := function_parameter in
        Build_extensible "Unmatched_branches"
          (Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty *
            Script_tc_errors.unparsed_stack_ty) (loc, stya, styb)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.bad_stack_item" "Bad stack item"
      "The type of a stack item is unexpected (this error is always accompanied by a more precise one)."
      None
      (Data_encoding.obj1
        (Data_encoding.req None None "item_level" Data_encoding.int16))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Bad_stack_item" then
            let 'n := cast int payload in
            Some n
          else None
        end) (fun (n : int) => Build_extensible "Bad_stack_item" int n) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.self_in_lambda" "SELF instruction in lambda"
      "A SELF instruction was encountered in a lambda expression." None
      (located Data_encoding.empty)
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Self_in_lambda" then
            let 'loc := cast Alpha_context.Script.location payload in
            Some (loc, tt)
          else None
        end)
      (fun (function_parameter : Alpha_context.Script.location * unit) =>
        let '(loc, _) := function_parameter in
        Build_extensible "Self_in_lambda" Alpha_context.Script.location loc) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.inconsistent_stack_lengths" "Inconsistent stack lengths"
      "A stack was of an unexpected length (this error is always in the context of a located error)."
      None Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Bad_stack_length" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Bad_stack_length" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.invalid_constant" "Invalid constant"
      "A data expression was invalid for its expected type." None
      (located
        (Data_encoding.obj2
          (Data_encoding.req None None "expected_type"
            Alpha_context.Script.expr_encoding)
          (Data_encoding.req None None "wrong_expression"
            Alpha_context.Script.expr_encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_constant" then
            let '(loc, expr, ty) :=
              cast
                (Alpha_context.Script.location * Alpha_context.Script.expr *
                  Alpha_context.Script.expr) payload in
            Some (loc, (ty, expr))
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location *
          (Alpha_context.Script.expr * Alpha_context.Script.expr)) =>
        let '(loc, (ty, expr)) := function_parameter in
        Build_extensible "Invalid_constant"
          (Alpha_context.Script.location * Alpha_context.Script.expr *
            Alpha_context.Script.expr) (loc, expr, ty)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.view_name_too_long" "View name too long (type error)"
      "A view name exceeds the maximum length of 31 characters." None
      (Data_encoding.obj1
        (Data_encoding.req None None "name" Data_encoding.string_value))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "View_name_too_long" then
            let 'name := cast string payload in
            Some name
          else None
        end)
      (fun (name : string) => Build_extensible "View_name_too_long" string name)
    in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.duplicated_view_name" "Duplicated view name"
      "The name of view in toplevel should be unique." None
      (Data_encoding.obj1
        (Data_encoding.req None None "location"
          Alpha_context.Script.location_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Duplicated_view_name" then
            let 'loc := cast Alpha_context.Script.location payload in
            Some loc
          else None
        end)
      (fun (loc : Alpha_context.Script.location) =>
        Build_extensible "Duplicated_view_name" Alpha_context.Script.location
          loc) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.invalid_syntactic_constant" "Invalid constant (parse error)"
      "A compile-time constant was invalid for its expected form." None
      (located
        (Data_encoding.obj2
          (Data_encoding.req None None "expected_form"
            Data_encoding.string_value)
          (Data_encoding.req None None "wrong_expression"
            Alpha_context.Script.expr_encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_syntactic_constant" then
            let '(loc, expr, expected) :=
              cast
                (Alpha_context.Script.location * Alpha_context.Script.expr *
                  string) payload in
            Some (loc, (expected, expr))
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location * (string * Alpha_context.Script.expr)) =>
        let '(loc, (expected, expr)) := function_parameter in
        Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location * Alpha_context.Script.expr * string)
          (loc, expr, expected)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.invalid_contract" "Invalid contract"
      "A script or data expression references a contract that does not exist or assumes a wrong type for an existing contract."
      None
      (located
        (Data_encoding.obj1
          (Data_encoding.req None None "contract"
            Alpha_context.Contract.encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_contract" then
            let '(loc, c) :=
              cast (Alpha_context.Script.location * Alpha_context.Contract.t)
                payload in
            Some (loc, c)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location * Alpha_context.Contract.t) =>
        let '(loc, c) := function_parameter in
        Build_extensible "Invalid_contract"
          (Alpha_context.Script.location * Alpha_context.Contract.t) (loc, c))
    in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.invalid_big_map" "Invalid big_map"
      "A script or data expression references a big_map that does not exist or assumes a wrong type for an existing big_map."
      None
      (located
        (Data_encoding.obj1
          (Data_encoding.req None None "big_map"
            Alpha_context.Big_map.Id.encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_big_map" then
            let '(loc, c) :=
              cast (Alpha_context.Script.location * Alpha_context.Big_map.Id.t)
                payload in
            Some (loc, c)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location * Alpha_context.Big_map.Id.t) =>
        let '(loc, c) := function_parameter in
        Build_extensible "Invalid_big_map"
          (Alpha_context.Script.location * Alpha_context.Big_map.Id.t) (loc, c))
    in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.comparable_type_expected" "Comparable type expected"
      "A non comparable type was used in a place where only comparable types are accepted."
      None
      (located
        (Data_encoding.obj1
          (Data_encoding.req None None "wrong_type"
            Alpha_context.Script.expr_encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Comparable_type_expected" then
            let '(loc, ty) :=
              cast (Alpha_context.Script.location * Alpha_context.Script.expr)
                payload in
            Some (loc, ty)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location * Alpha_context.Script.expr) =>
        let '(loc, ty) := function_parameter in
        Build_extensible "Comparable_type_expected"
          (Alpha_context.Script.location * Alpha_context.Script.expr) (loc, ty))
    in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.inconsistent_type_sizes" "Inconsistent type sizes"
      "Two types were expected to be equal but they have different sizes." None
      (Data_encoding.obj2
        (Data_encoding.req None None "first_type_size" Data_encoding.int31)
        (Data_encoding.req None None "other_type_size" Data_encoding.int31))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Inconsistent_type_sizes" then
            let '(tya, tyb) := cast (int * int) payload in
            Some (tya, tyb)
          else None
        end)
      (fun (function_parameter : int * int) =>
        let '(tya, tyb) := function_parameter in
        Build_extensible "Inconsistent_type_sizes" (int * int) (tya, tyb)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.inconsistent_types" "Inconsistent types"
      "This is the basic type clash error, that appears in several places where the equality of two types have to be proven, it is always accompanied with another error that provides more context."
      None
      (Data_encoding.obj3
        (Data_encoding.opt None None "loc"
          Alpha_context.Script.location_encoding)
        (Data_encoding.req None None "first_type"
          Alpha_context.Script.expr_encoding)
        (Data_encoding.req None None "other_type"
          Alpha_context.Script.expr_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Inconsistent_types" then
            let '(loc, tya, tyb) :=
              cast
                (option Alpha_context.Script.location *
                  Alpha_context.Script.expr * Alpha_context.Script.expr) payload
                in
            Some (loc, tya, tyb)
          else None
        end)
      (fun (function_parameter :
        option Alpha_context.Script.location * Alpha_context.Script.expr *
          Alpha_context.Script.expr) =>
        let '(loc, tya, tyb) := function_parameter in
        Build_extensible "Inconsistent_types"
          (option Alpha_context.Script.location * Alpha_context.Script.expr *
            Alpha_context.Script.expr) (loc, tya, tyb)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.inconsistent_memo_sizes" "Inconsistent memo sizes"
      "Memo sizes of two sapling states or transactions do not match" None
      (Data_encoding.obj2
        (Data_encoding.req None None "first_memo_size"
          Alpha_context.Sapling.Memo_size.encoding)
        (Data_encoding.req None None "other_memo_size"
          Alpha_context.Sapling.Memo_size.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Inconsistent_memo_sizes" then
            let '(msa, msb) :=
              cast
                (Alpha_context.Sapling.Memo_size.t *
                  Alpha_context.Sapling.Memo_size.t) payload in
            Some (msa, msb)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Sapling.Memo_size.t * Alpha_context.Sapling.Memo_size.t)
        =>
        let '(msa, msb) := function_parameter in
        Build_extensible "Inconsistent_memo_sizes"
          (Alpha_context.Sapling.Memo_size.t * Alpha_context.Sapling.Memo_size.t)
          (msa, msb)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.bad_view_name" "Bad view name"
      "In a view declaration, the view name must be a string" None
      (Data_encoding.obj1
        (Data_encoding.req None None "loc"
          Alpha_context.Script.location_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Bad_view_name" then
            let 'loc := cast Alpha_context.Script.location payload in
            Some loc
          else None
        end)
      (fun (loc : Alpha_context.Script.location) =>
        Build_extensible "Bad_view_name" Alpha_context.Script.location loc) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.ill_typed_view" "Ill typed view"
      "The return of a view block did not match the expected type" None
      (Data_encoding.obj3
        (Data_encoding.req None None "loc"
          Alpha_context.Script.location_encoding)
        (Data_encoding.req None None "resulted_view_stack" stack_ty_enc)
        (Data_encoding.req None None "expected_view_stack" stack_ty_enc))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Ill_typed_view" then
            let '{|
              Script_tc_errors.Ill_typed_view.loc := loc;
                Script_tc_errors.Ill_typed_view.actual := actual;
                Script_tc_errors.Ill_typed_view.expected := expected
                |} := cast Script_tc_errors.Ill_typed_view payload in
            Some (loc, actual, expected)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty *
          Script_tc_errors.unparsed_stack_ty) =>
        let '(loc, actual, expected) := function_parameter in
        Build_extensible "Ill_typed_view" Script_tc_errors.Ill_typed_view
          {| Script_tc_errors.Ill_typed_view.loc := loc;
            Script_tc_errors.Ill_typed_view.actual := actual;
            Script_tc_errors.Ill_typed_view.expected := expected |}) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.invalid_map_body" "Invalid map body"
      "The body of a map block did not match the expected type" None
      (Data_encoding.obj2
        (Data_encoding.req None None "loc"
          Alpha_context.Script.location_encoding)
        (Data_encoding.req None None "body_type" stack_ty_enc))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_map_body" then
            let '(loc, stack_value) :=
              cast
                (Alpha_context.Script.location *
                  Script_tc_errors.unparsed_stack_ty) payload in
            Some (loc, stack_value)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty) =>
        let '(loc, stack_value) := function_parameter in
        Build_extensible "Invalid_map_body"
          (Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty)
          (loc, stack_value)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.invalid_map_block_fail"
      "FAIL instruction occurred as body of map block"
      "FAIL cannot be the only instruction in the body. The proper type of the return list cannot be inferred."
      None
      (Data_encoding.obj1
        (Data_encoding.req None None "loc"
          Alpha_context.Script.location_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_map_block_fail" then
            let 'loc := cast Alpha_context.Script.location payload in
            Some loc
          else None
        end)
      (fun (loc : Alpha_context.Script.location) =>
        Build_extensible "Invalid_map_block_fail" Alpha_context.Script.location
          loc) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.invalid_iter_body" "ITER body returned wrong stack type"
      "The body of an ITER instruction must result in the same stack type as before the ITER."
      None
      (Data_encoding.obj3
        (Data_encoding.req None None "loc"
          Alpha_context.Script.location_encoding)
        (Data_encoding.req None None "bef_stack" stack_ty_enc)
        (Data_encoding.req None None "aft_stack" stack_ty_enc))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_iter_body" then
            let '(loc, bef, aft) :=
              cast
                (Alpha_context.Script.location *
                  Script_tc_errors.unparsed_stack_ty *
                  Script_tc_errors.unparsed_stack_ty) payload in
            Some (loc, bef, aft)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty *
          Script_tc_errors.unparsed_stack_ty) =>
        let '(loc, bef, aft) := function_parameter in
        Build_extensible "Invalid_iter_body"
          (Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty *
            Script_tc_errors.unparsed_stack_ty) (loc, bef, aft)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.type_too_large" "Stack item type too large"
      "An instruction generated a type larger than the limit." None
      (Data_encoding.obj2
        (Data_encoding.req None None "loc"
          Alpha_context.Script.location_encoding)
        (Data_encoding.req None None "maximum_type_size" Data_encoding.uint16))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Type_too_large" then
            let '(loc, maxts) :=
              cast (Alpha_context.Script.location * int) payload in
            Some (loc, maxts)
          else None
        end)
      (fun (function_parameter : Alpha_context.Script.location * int) =>
        let '(loc, maxts) := function_parameter in
        Build_extensible "Type_too_large" (Alpha_context.Script.location * int)
          (loc, maxts)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.bad_pair_argument" "0 or 1 passed to PAIR"
      "PAIR expects an argument of at least 2" None
      (Data_encoding.obj1
        (Data_encoding.req None None "loc"
          Alpha_context.Script.location_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Pair_bad_argument" then
            let 'loc := cast Alpha_context.Script.location payload in
            Some loc
          else None
        end)
      (fun (loc : Alpha_context.Script.location) =>
        Build_extensible "Pair_bad_argument" Alpha_context.Script.location loc)
    in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.bad_unpair_argument" "0 or 1 passed to UNPAIR"
      "UNPAIR expects an argument of at least 2" None
      (Data_encoding.obj1
        (Data_encoding.req None None "loc"
          Alpha_context.Script.location_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Unpair_bad_argument" then
            let 'loc := cast Alpha_context.Script.location payload in
            Some loc
          else None
        end)
      (fun (loc : Alpha_context.Script.location) =>
        Build_extensible "Unpair_bad_argument" Alpha_context.Script.location loc)
    in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.bad_dupn_argument" "0 passed to DUP n"
      "DUP expects an argument of at least 1 (passed 0)" None
      (Data_encoding.obj1
        (Data_encoding.req None None "loc"
          Alpha_context.Script.location_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Dup_n_bad_argument" then
            let 'loc := cast Alpha_context.Script.location payload in
            Some loc
          else None
        end)
      (fun (loc : Alpha_context.Script.location) =>
        Build_extensible "Dup_n_bad_argument" Alpha_context.Script.location loc)
    in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.bad_dupn_stack" "Stack too short when typing DUP n"
      "Stack present when typing DUP n was too short" None
      (Data_encoding.obj1
        (Data_encoding.req None None "loc"
          Alpha_context.Script.location_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Dup_n_bad_stack" then
            let 'x := cast Alpha_context.Script.location payload in
            Some x
          else None
        end)
      (fun (x : Alpha_context.Script.location) =>
        Build_extensible "Dup_n_bad_stack" Alpha_context.Script.location x) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.ill_typed_data" "Ill typed data"
      "The toplevel error thrown when trying to typecheck a data expression against a given type (always followed by more precise errors)."
      None
      (Data_encoding.obj3
        (Data_encoding.opt None None "identifier" Data_encoding.string_value)
        (Data_encoding.req None None "expected_type"
          Alpha_context.Script.expr_encoding)
        (Data_encoding.req None None "ill_typed_expression"
          Alpha_context.Script.expr_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Ill_typed_data" then
            let '(name, expr, ty) :=
              cast
                (option string * Alpha_context.Script.expr *
                  Alpha_context.Script.expr) payload in
            Some (name, ty, expr)
          else None
        end)
      (fun (function_parameter :
        option string * Alpha_context.Script.expr * Alpha_context.Script.expr)
        =>
        let '(name, ty, expr) := function_parameter in
        Build_extensible "Ill_typed_data"
          (option string * Alpha_context.Script.expr * Alpha_context.Script.expr)
          (name, expr, ty)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.ill_formed_type" "Ill formed type"
      "The toplevel error thrown when trying to parse a type expression (always followed by more precise errors)."
      None
      (Data_encoding.obj3
        (Data_encoding.opt None None "identifier" Data_encoding.string_value)
        (Data_encoding.req None None "ill_formed_expression"
          Alpha_context.Script.expr_encoding)
        (Data_encoding.req None None "location"
          Alpha_context.Script.location_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Ill_formed_type" then
            let '(name, expr, loc) :=
              cast
                (option string * Alpha_context.Script.expr *
                  Alpha_context.Script.location) payload in
            Some (name, expr, loc)
          else None
        end)
      (fun (function_parameter :
        option string * Alpha_context.Script.expr *
          Alpha_context.Script.location) =>
        let '(name, expr, loc) := function_parameter in
        Build_extensible "Ill_formed_type"
          (option string * Alpha_context.Script.expr *
            Alpha_context.Script.location) (name, expr, loc)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.ill_typed_contract" "Ill typed contract"
      "The toplevel error thrown when trying to typecheck a contract code against given input, output and storage types (always followed by more precise errors)."
      None
      (Data_encoding.obj2
        (Data_encoding.req None None "ill_typed_code"
          Alpha_context.Script.expr_encoding)
        (Data_encoding.req None None "type_map" type_map_enc))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Ill_typed_contract" then
            let '(expr, type_map) :=
              cast (Alpha_context.Script.expr * Script_tc_errors.type_map)
                payload in
            Some (expr, type_map)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.expr * Script_tc_errors.type_map) =>
        let '(expr, type_map) := function_parameter in
        Build_extensible "Ill_typed_contract"
          (Alpha_context.Script.expr * Script_tc_errors.type_map)
          (expr, type_map)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "michelson_v1.cannot_serialize_error" "Not enough gas to serialize error"
      "The error was too big to be serialized with the provided gas" None
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Cannot_serialize_error" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Cannot_serialize_error" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.deprecated_instruction"
      "Script is using a deprecated instruction"
      "A deprecated instruction usage is disallowed in newly created contracts"
      None
      (Data_encoding.obj1
        (Data_encoding.req None None "prim" Alpha_context.Script.prim_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Deprecated_instruction" then
            let 'prim := cast Alpha_context.Script.prim payload in
            Some prim
          else None
        end)
      (fun (prim : Alpha_context.Script.prim) =>
        Build_extensible "Deprecated_instruction" Alpha_context.Script.prim prim)
    in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "michelson_v1.typechecking_too_many_recursive_calls"
      "Too many recursive calls during typechecking"
      "Too many recursive calls were needed for typechecking" None
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Typechecking_too_many_recursive_calls" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Typechecking_too_many_recursive_calls" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "michelson_v1.unparsing_stack_overflow"
      "Too many recursive calls during unparsing"
      "Too many recursive calls were needed for unparsing" None
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Unparsing_too_many_recursive_calls" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Unparsing_too_many_recursive_calls" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.unexpected_forged_value" "Unexpected forged value"
      "A forged value was encountered but disallowed for that position." None
      (Data_encoding.obj1
        (Data_encoding.req None None "location"
          Alpha_context.Script.location_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Unexpected_forged_value" then
            let 'loc := cast Alpha_context.Script.location payload in
            Some loc
          else None
        end)
      (fun (loc : Alpha_context.Script.location) =>
        Build_extensible "Unexpected_forged_value" Alpha_context.Script.location
          loc) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.unexpected_ticket"
      "Ticket in unauthorized position (type error)"
      "A ticket type has been found" None
      (Data_encoding.obj1
        (Data_encoding.req None None "loc"
          Alpha_context.Script.location_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Unexpected_ticket" then
            let 'loc := cast Alpha_context.Script.location payload in
            Some loc
          else None
        end)
      (fun (loc : Alpha_context.Script.location) =>
        Build_extensible "Unexpected_ticket" Alpha_context.Script.location loc)
    in
  Error_monad.register_error_kind Error_monad.Permanent
    "michelson_v1.non_dupable_type" "Non-dupable type duplication attempt"
    "DUP was used on a non-dupable type (e.g. tickets)." None
    (Data_encoding.obj2
      (Data_encoding.req None None "loc" Alpha_context.Script.location_encoding)
      (Data_encoding.req None None "type" Alpha_context.Script.expr_encoding))
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Non_dupable_type" then
          let '(loc, ty) :=
            cast (Alpha_context.Script.location * Alpha_context.Script.expr)
              payload in
          Some (loc, ty)
        else None
      end)
    (fun (function_parameter :
      Alpha_context.Script.location * Alpha_context.Script.expr) =>
      let '(loc, ty) := function_parameter in
      Build_extensible "Non_dupable_type"
        (Alpha_context.Script.location * Alpha_context.Script.expr) (loc, ty)).
