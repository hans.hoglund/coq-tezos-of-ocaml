Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Cache_memory_helpers.
Require TezosOfOCaml.Proto_alpha.Gas_limit_repr.
Require TezosOfOCaml.Proto_alpha.Michelson_v1_gas.
Require TezosOfOCaml.Proto_alpha.Michelson_v1_primitives.
Require TezosOfOCaml.Proto_alpha.Saturation_repr.
Require TezosOfOCaml.Proto_alpha.Script_expr_hash.
Require TezosOfOCaml.Proto_alpha.Script_ir_annot.
Require TezosOfOCaml.Proto_alpha.Script_repr.
Require TezosOfOCaml.Proto_alpha.Script_string_repr.
Require TezosOfOCaml.Proto_alpha.Script_tc_errors.
Require TezosOfOCaml.Proto_alpha.Script_typed_ir.
Require TezosOfOCaml.Proto_alpha.Script_typed_ir_size.
Require TezosOfOCaml.Proto_alpha.Script_typed_ir_size_costs.

Module Typecheck_costs := Michelson_v1_gas.Cost_of.Typechecking.

Module Unparse_costs := Michelson_v1_gas.Cost_of.Unparsing.

Inductive ex_stack_ty : Set :=
| Ex_stack_ty : Script_typed_ir.stack_ty -> ex_stack_ty.

Module cinstr.
  Record record : Set := Build {
    apply :
      Script_typed_ir.kinfo -> Script_typed_ir.kinstr -> Script_typed_ir.kinstr }.
  Definition with_apply apply (r : record) :=
    Build apply.
End cinstr.
Definition cinstr := cinstr.record.

Module descr.
  Record record : Set := Build {
    loc : Alpha_context.Script.location;
    bef : Script_typed_ir.stack_ty;
    aft : Script_typed_ir.stack_ty;
    instr : cinstr }.
  Definition with_loc loc (r : record) :=
    Build loc r.(bef) r.(aft) r.(instr).
  Definition with_bef bef (r : record) :=
    Build r.(loc) bef r.(aft) r.(instr).
  Definition with_aft aft (r : record) :=
    Build r.(loc) r.(bef) aft r.(instr).
  Definition with_instr instr (r : record) :=
    Build r.(loc) r.(bef) r.(aft) instr.
End descr.
Definition descr := descr.record.

Definition close_descr (function_parameter : descr) : Script_typed_ir.kdescr :=
  let '{|
    descr.loc := loc;
      descr.bef := bef;
      descr.aft := aft;
      descr.instr := instr
      |} := function_parameter in
  let kinfo_value :=
    {| Script_typed_ir.kinfo.iloc := loc; Script_typed_ir.kinfo.kstack_ty := aft
      |} in
  let kinfo' :=
    {| Script_typed_ir.kinfo.iloc := loc; Script_typed_ir.kinfo.kstack_ty := bef
      |} in
  let kinstr := instr.(cinstr.apply) kinfo' (Script_typed_ir.IHalt kinfo_value)
    in
  {| Script_typed_ir.kdescr.kloc := loc; Script_typed_ir.kdescr.kbef := bef;
    Script_typed_ir.kdescr.kaft := aft; Script_typed_ir.kdescr.kinstr := kinstr
    |}.

Definition kinfo_of_descr (function_parameter : descr)
  : Script_typed_ir.kinfo :=
  let '{| descr.loc := loc; descr.bef := bef |} := function_parameter in
  {| Script_typed_ir.kinfo.iloc := loc; Script_typed_ir.kinfo.kstack_ty := bef
    |}.

Definition compose_descr
  (loc : Alpha_context.Script.location) (d1 : descr) (d2 : descr) : descr :=
  {| descr.loc := loc; descr.bef := d1.(descr.bef); descr.aft := d2.(descr.aft);
    descr.instr :=
      {|
        cinstr.apply :=
          fun (function_parameter : Script_typed_ir.kinfo) =>
            let '_ := function_parameter in
            fun (k : Script_typed_ir.kinstr) =>
              d1.(descr.instr).(cinstr.apply) (kinfo_of_descr d1)
                (d2.(descr.instr).(cinstr.apply) (kinfo_of_descr d2) k) |} |}.

(** Records for the constructor parameters *)
Module ConstructorRecords_tc_context.
  Module tc_context.
    Module Toplevel.
      Record record {storage_type param_type root_name
        legacy_create_contract_literal : Set} : Set := Build {
        storage_type : storage_type;
        param_type : param_type;
        root_name : root_name;
        legacy_create_contract_literal : legacy_create_contract_literal }.
      Arguments record : clear implicits.
      Definition with_storage_type
        {t_storage_type t_param_type t_root_name
          t_legacy_create_contract_literal} storage_type
        (r :
          record t_storage_type t_param_type t_root_name
            t_legacy_create_contract_literal) :=
        Build t_storage_type t_param_type t_root_name
          t_legacy_create_contract_literal storage_type r.(param_type)
          r.(root_name) r.(legacy_create_contract_literal).
      Definition with_param_type
        {t_storage_type t_param_type t_root_name
          t_legacy_create_contract_literal} param_type
        (r :
          record t_storage_type t_param_type t_root_name
            t_legacy_create_contract_literal) :=
        Build t_storage_type t_param_type t_root_name
          t_legacy_create_contract_literal r.(storage_type) param_type
          r.(root_name) r.(legacy_create_contract_literal).
      Definition with_root_name
        {t_storage_type t_param_type t_root_name
          t_legacy_create_contract_literal} root_name
        (r :
          record t_storage_type t_param_type t_root_name
            t_legacy_create_contract_literal) :=
        Build t_storage_type t_param_type t_root_name
          t_legacy_create_contract_literal r.(storage_type) r.(param_type)
          root_name r.(legacy_create_contract_literal).
      Definition with_legacy_create_contract_literal
        {t_storage_type t_param_type t_root_name
          t_legacy_create_contract_literal} legacy_create_contract_literal
        (r :
          record t_storage_type t_param_type t_root_name
            t_legacy_create_contract_literal) :=
        Build t_storage_type t_param_type t_root_name
          t_legacy_create_contract_literal r.(storage_type) r.(param_type)
          r.(root_name) legacy_create_contract_literal.
    End Toplevel.
    Definition Toplevel_skeleton := Toplevel.record.
  End tc_context.
End ConstructorRecords_tc_context.
Import ConstructorRecords_tc_context.

Reserved Notation "'tc_context.Toplevel".

Inductive tc_context : Set :=
| Lambda : tc_context
| Dip : Script_typed_ir.stack_ty -> tc_context -> tc_context
| Toplevel : 'tc_context.Toplevel -> tc_context

where "'tc_context.Toplevel" :=
  (tc_context.Toplevel_skeleton Script_typed_ir.ty Script_typed_ir.ty
    (option Script_typed_ir.field_annot) bool).

Module tc_context.
  Include ConstructorRecords_tc_context.tc_context.
  Definition Toplevel := 'tc_context.Toplevel.
End tc_context.

Inductive unparsing_mode : Set :=
| Optimized : unparsing_mode
| Readable : unparsing_mode
| Optimized_legacy : unparsing_mode.

Definition type_logger : Set :=
  int -> list (Alpha_context.Script.expr * Alpha_context.Script.annot) ->
  list (Alpha_context.Script.expr * Alpha_context.Script.annot) -> unit.

Definition add_dip
  (ty : Script_typed_ir.ty) (annot : option Script_typed_ir.var_annot)
  (prev : tc_context) : tc_context :=
  match prev with
  | (Lambda | Toplevel _) =>
    Dip
      (Script_typed_ir.Item_t ty
        (Script_typed_ir.Item_t (Script_typed_ir.unit_t None)
          Script_typed_ir.Bot_t None) annot) prev
  | Dip stack_value _ => Dip (Script_typed_ir.Item_t ty stack_value annot) prev
  end.

Definition location {A B : Set} (function_parameter : Micheline.node A B) : A :=
  match function_parameter with
  |
    (Micheline.Prim loc _ _ _ | Micheline.Int loc _ | Micheline.String loc _ |
    Micheline.Bytes loc _ | Micheline.Seq loc _) => loc
  end.

Definition kind_equal
  (a_value : Script_tc_errors.kind) (b_value : Script_tc_errors.kind) : bool :=
  match (a_value, b_value) with
  |
    ((Script_tc_errors.Int_kind, Script_tc_errors.Int_kind) |
    (Script_tc_errors.String_kind, Script_tc_errors.String_kind) |
    (Script_tc_errors.Bytes_kind, Script_tc_errors.Bytes_kind) |
    (Script_tc_errors.Prim_kind, Script_tc_errors.Prim_kind) |
    (Script_tc_errors.Seq_kind, Script_tc_errors.Seq_kind)) => true
  | _ => false
  end.

Definition kind_value {A B : Set} (function_parameter : Micheline.node A B)
  : Script_tc_errors.kind :=
  match function_parameter with
  | Micheline.Int _ _ => Script_tc_errors.Int_kind
  | Micheline.String _ _ => Script_tc_errors.String_kind
  | Micheline.Bytes _ _ => Script_tc_errors.Bytes_kind
  | Micheline.Prim _ _ _ _ => Script_tc_errors.Prim_kind
  | Micheline.Seq _ _ => Script_tc_errors.Seq_kind
  end.

Definition unexpected
  (expr :
    Micheline.node Alpha_context.Script.location Michelson_v1_primitives.prim)
  (exp_kinds : list Script_tc_errors.kind)
  (exp_ns : Michelson_v1_primitives.namespace)
  (exp_prims : list Alpha_context.Script.prim) : Error_monad._error :=
  match expr with
  | Micheline.Int loc _ =>
    Build_extensible "Invalid_kind"
      (Alpha_context.Script.location * list Script_tc_errors.kind *
        Script_tc_errors.kind)
      (loc, (cons Script_tc_errors.Prim_kind exp_kinds),
        Script_tc_errors.Int_kind)
  | Micheline.String loc _ =>
    Build_extensible "Invalid_kind"
      (Alpha_context.Script.location * list Script_tc_errors.kind *
        Script_tc_errors.kind)
      (loc, (cons Script_tc_errors.Prim_kind exp_kinds),
        Script_tc_errors.String_kind)
  | Micheline.Bytes loc _ =>
    Build_extensible "Invalid_kind"
      (Alpha_context.Script.location * list Script_tc_errors.kind *
        Script_tc_errors.kind)
      (loc, (cons Script_tc_errors.Prim_kind exp_kinds),
        Script_tc_errors.Bytes_kind)
  | Micheline.Seq loc _ =>
    Build_extensible "Invalid_kind"
      (Alpha_context.Script.location * list Script_tc_errors.kind *
        Script_tc_errors.kind)
      (loc, (cons Script_tc_errors.Prim_kind exp_kinds),
        Script_tc_errors.Seq_kind)
  | Micheline.Prim loc name _ _ =>
    match ((Michelson_v1_primitives.namespace_value name), exp_ns) with
    |
      ((Michelson_v1_primitives.Type_namespace,
        Michelson_v1_primitives.Type_namespace) |
      (Michelson_v1_primitives.Instr_namespace,
        Michelson_v1_primitives.Instr_namespace) |
      (Michelson_v1_primitives.Constant_namespace,
        Michelson_v1_primitives.Constant_namespace)) =>
      Build_extensible "Invalid_primitive"
        (Alpha_context.Script.location * list Alpha_context.Script.prim *
          Michelson_v1_primitives.prim) (loc, exp_prims, name)
    | (ns, _) =>
      Build_extensible "Invalid_namespace"
        (Alpha_context.Script.location * Michelson_v1_primitives.prim *
          Michelson_v1_primitives.namespace * Michelson_v1_primitives.namespace)
        (loc, name, exp_ns, ns)
    end
  end.

Definition check_kind {A : Set}
  (kinds : list Script_tc_errors.kind)
  (expr : Micheline.node Alpha_context.Script.location A) : M? unit :=
  let kind_value := kind_value expr in
  if List._exists (kind_equal kind_value) kinds then
    Error_monad.ok_unit
  else
    let loc := location expr in
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind) (loc, kinds, kind_value)).

Fixpoint ty_of_comparable_ty
  (function_parameter : Script_typed_ir.comparable_ty) : Script_typed_ir.ty :=
  match function_parameter with
  | Script_typed_ir.Unit_key tname => Script_typed_ir.Unit_t tname
  | Script_typed_ir.Never_key tname => Script_typed_ir.Never_t tname
  | Script_typed_ir.Int_key tname => Script_typed_ir.Int_t tname
  | Script_typed_ir.Nat_key tname => Script_typed_ir.Nat_t tname
  | Script_typed_ir.Signature_key tname => Script_typed_ir.Signature_t tname
  | Script_typed_ir.String_key tname => Script_typed_ir.String_t tname
  | Script_typed_ir.Bytes_key tname => Script_typed_ir.Bytes_t tname
  | Script_typed_ir.Mutez_key tname => Script_typed_ir.Mutez_t tname
  | Script_typed_ir.Bool_key tname => Script_typed_ir.Bool_t tname
  | Script_typed_ir.Key_hash_key tname => Script_typed_ir.Key_hash_t tname
  | Script_typed_ir.Key_key tname => Script_typed_ir.Key_t tname
  | Script_typed_ir.Timestamp_key tname => Script_typed_ir.Timestamp_t tname
  | Script_typed_ir.Address_key tname => Script_typed_ir.Address_t tname
  | Script_typed_ir.Chain_id_key tname => Script_typed_ir.Chain_id_t tname
  | Script_typed_ir.Pair_key (l_value, al) (r_value, ar) tname =>
    Script_typed_ir.Pair_t ((ty_of_comparable_ty l_value), al, None)
      ((ty_of_comparable_ty r_value), ar, None) tname
  | Script_typed_ir.Union_key (l_value, al) (r_value, ar) tname =>
    Script_typed_ir.Union_t ((ty_of_comparable_ty l_value), al)
      ((ty_of_comparable_ty r_value), ar) tname
  | Script_typed_ir.Option_key t_value tname =>
    Script_typed_ir.Option_t (ty_of_comparable_ty t_value) tname
  end.

Definition add_field_annot {A B : Set}
  (a_value : option Script_typed_ir.field_annot)
  (var : option Script_typed_ir.var_annot)
  (function_parameter : Micheline.node A B) : Micheline.node A B :=
  match function_parameter with
  | Micheline.Prim loc prim args annots =>
    Micheline.Prim loc prim args
      (Pervasives.op_at annots
        (Pervasives.op_at (Script_ir_annot.unparse_field_annot a_value)
          (Script_ir_annot.unparse_var_annot var)))
  | expr => expr
  end.

Fixpoint unparse_comparable_ty
  (function_parameter : Script_typed_ir.comparable_ty)
  : Alpha_context.Script.node :=
  match function_parameter with
  | Script_typed_ir.Unit_key meta =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_unit nil
      (Script_ir_annot.unparse_type_annot
        meta.(Script_typed_ir.ty_metadata.annot))
  | Script_typed_ir.Never_key meta =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_never nil
      (Script_ir_annot.unparse_type_annot
        meta.(Script_typed_ir.ty_metadata.annot))
  | Script_typed_ir.Int_key meta =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_int nil
      (Script_ir_annot.unparse_type_annot
        meta.(Script_typed_ir.ty_metadata.annot))
  | Script_typed_ir.Nat_key meta =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_nat nil
      (Script_ir_annot.unparse_type_annot
        meta.(Script_typed_ir.ty_metadata.annot))
  | Script_typed_ir.Signature_key meta =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_signature nil
      (Script_ir_annot.unparse_type_annot
        meta.(Script_typed_ir.ty_metadata.annot))
  | Script_typed_ir.String_key meta =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_string nil
      (Script_ir_annot.unparse_type_annot
        meta.(Script_typed_ir.ty_metadata.annot))
  | Script_typed_ir.Bytes_key meta =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_bytes nil
      (Script_ir_annot.unparse_type_annot
        meta.(Script_typed_ir.ty_metadata.annot))
  | Script_typed_ir.Mutez_key meta =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_mutez nil
      (Script_ir_annot.unparse_type_annot
        meta.(Script_typed_ir.ty_metadata.annot))
  | Script_typed_ir.Bool_key meta =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_bool nil
      (Script_ir_annot.unparse_type_annot
        meta.(Script_typed_ir.ty_metadata.annot))
  | Script_typed_ir.Key_hash_key meta =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_key_hash nil
      (Script_ir_annot.unparse_type_annot
        meta.(Script_typed_ir.ty_metadata.annot))
  | Script_typed_ir.Key_key meta =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_key nil
      (Script_ir_annot.unparse_type_annot
        meta.(Script_typed_ir.ty_metadata.annot))
  | Script_typed_ir.Timestamp_key meta =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_timestamp nil
      (Script_ir_annot.unparse_type_annot
        meta.(Script_typed_ir.ty_metadata.annot))
  | Script_typed_ir.Address_key meta =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_address nil
      (Script_ir_annot.unparse_type_annot
        meta.(Script_typed_ir.ty_metadata.annot))
  | Script_typed_ir.Chain_id_key meta =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_chain_id nil
      (Script_ir_annot.unparse_type_annot
        meta.(Script_typed_ir.ty_metadata.annot))
  | Script_typed_ir.Pair_key (l_value, al) (r_value, ar) meta =>
    let tl := add_field_annot al None (unparse_comparable_ty l_value) in
    let tr := add_field_annot ar None (unparse_comparable_ty r_value) in
    match tr with
    | Micheline.Prim _ Michelson_v1_primitives.T_pair ts [] =>
      Micheline.Prim (-1) Michelson_v1_primitives.T_pair (cons tl ts)
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot))
    | _ =>
      Micheline.Prim (-1) Michelson_v1_primitives.T_pair [ tl; tr ]
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot))
    end
  | Script_typed_ir.Union_key (l_value, al) (r_value, ar) meta =>
    let tl := add_field_annot al None (unparse_comparable_ty l_value) in
    let tr := add_field_annot ar None (unparse_comparable_ty r_value) in
    Micheline.Prim (-1) Michelson_v1_primitives.T_or [ tl; tr ]
      (Script_ir_annot.unparse_type_annot
        meta.(Script_typed_ir.ty_metadata.annot))
  | Script_typed_ir.Option_key t_value meta =>
    Micheline.Prim (-1) Michelson_v1_primitives.T_option
      [ unparse_comparable_ty t_value ]
      (Script_ir_annot.unparse_type_annot
        meta.(Script_typed_ir.ty_metadata.annot))
  end.

Definition unparse_memo_size {A : Set}
  (memo_size : Alpha_context.Sapling.Memo_size.t) : Micheline.node int A :=
  let z := Alpha_context.Sapling.Memo_size.unparse_to_z memo_size in
  Micheline.Int (-1) z.

Fixpoint unparse_ty (ctxt : Alpha_context.context) (ty : Script_typed_ir.ty)
  : M? (Alpha_context.Script.node * Alpha_context.context) :=
  let? ctxt := Alpha_context.Gas.consume ctxt Unparse_costs.unparse_type_cycle
    in
  let _return {A B C : Set}
    (ctxt : A)
    (function_parameter : B * list (Micheline.node int B) * Micheline.annot)
    : Pervasives.result (Micheline.node int B * A) C :=
    let '(name, args, annot) := function_parameter in
    let result_value := Micheline.Prim (-1) name args annot in
    return? (result_value, ctxt) in
  match ty with
  | Script_typed_ir.Unit_t meta =>
    _return ctxt
      (Michelson_v1_primitives.T_unit, nil,
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Int_t meta =>
    _return ctxt
      (Michelson_v1_primitives.T_int, nil,
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Nat_t meta =>
    _return ctxt
      (Michelson_v1_primitives.T_nat, nil,
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Signature_t meta =>
    _return ctxt
      (Michelson_v1_primitives.T_signature, nil,
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.String_t meta =>
    _return ctxt
      (Michelson_v1_primitives.T_string, nil,
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Bytes_t meta =>
    _return ctxt
      (Michelson_v1_primitives.T_bytes, nil,
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Mutez_t meta =>
    _return ctxt
      (Michelson_v1_primitives.T_mutez, nil,
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Bool_t meta =>
    _return ctxt
      (Michelson_v1_primitives.T_bool, nil,
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Key_hash_t meta =>
    _return ctxt
      (Michelson_v1_primitives.T_key_hash, nil,
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Key_t meta =>
    _return ctxt
      (Michelson_v1_primitives.T_key, nil,
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Timestamp_t meta =>
    _return ctxt
      (Michelson_v1_primitives.T_timestamp, nil,
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Address_t meta =>
    _return ctxt
      (Michelson_v1_primitives.T_address, nil,
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Operation_t meta =>
    _return ctxt
      (Michelson_v1_primitives.T_operation, nil,
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Chain_id_t meta =>
    _return ctxt
      (Michelson_v1_primitives.T_chain_id, nil,
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Never_t meta =>
    _return ctxt
      (Michelson_v1_primitives.T_never, nil,
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Bls12_381_g1_t meta =>
    _return ctxt
      (Michelson_v1_primitives.T_bls12_381_g1, nil,
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Bls12_381_g2_t meta =>
    _return ctxt
      (Michelson_v1_primitives.T_bls12_381_g2, nil,
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Bls12_381_fr_t meta =>
    _return ctxt
      (Michelson_v1_primitives.T_bls12_381_fr, nil,
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Contract_t ut meta =>
    let? '(t_value, ctxt) := unparse_ty ctxt ut in
    _return ctxt
      (Michelson_v1_primitives.T_contract, [ t_value ],
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Pair_t (utl, l_field, l_var) (utr, r_field, r_var) meta =>
    let annot :=
      Script_ir_annot.unparse_type_annot
        meta.(Script_typed_ir.ty_metadata.annot) in
    let? '(utl, ctxt) := unparse_ty ctxt utl in
    let tl := add_field_annot l_field l_var utl in
    let? '(utr, ctxt) := unparse_ty ctxt utr in
    let tr := add_field_annot r_field r_var utr in
    _return ctxt
      match tr with
      | Micheline.Prim _ Michelson_v1_primitives.T_pair ts [] =>
        (Michelson_v1_primitives.T_pair, (cons tl ts), annot)
      | _ => (Michelson_v1_primitives.T_pair, [ tl; tr ], annot)
      end
  | Script_typed_ir.Union_t (utl, l_field) (utr, r_field) meta =>
    let annot :=
      Script_ir_annot.unparse_type_annot
        meta.(Script_typed_ir.ty_metadata.annot) in
    let? '(utl, ctxt) := unparse_ty ctxt utl in
    let tl := add_field_annot l_field None utl in
    let? '(utr, ctxt) := unparse_ty ctxt utr in
    let tr := add_field_annot r_field None utr in
    _return ctxt (Michelson_v1_primitives.T_or, [ tl; tr ], annot)
  | Script_typed_ir.Lambda_t uta utr meta =>
    let? '(ta, ctxt) := unparse_ty ctxt uta in
    let? '(tr, ctxt) := unparse_ty ctxt utr in
    _return ctxt
      (Michelson_v1_primitives.T_lambda, [ ta; tr ],
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Option_t ut meta =>
    let annot :=
      Script_ir_annot.unparse_type_annot
        meta.(Script_typed_ir.ty_metadata.annot) in
    let? '(ut, ctxt) := unparse_ty ctxt ut in
    _return ctxt (Michelson_v1_primitives.T_option, [ ut ], annot)
  | Script_typed_ir.List_t ut meta =>
    let? '(t_value, ctxt) := unparse_ty ctxt ut in
    _return ctxt
      (Michelson_v1_primitives.T_list, [ t_value ],
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Ticket_t ut meta =>
    let t_value := unparse_comparable_ty ut in
    _return ctxt
      (Michelson_v1_primitives.T_ticket, [ t_value ],
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Set_t ut meta =>
    let t_value := unparse_comparable_ty ut in
    _return ctxt
      (Michelson_v1_primitives.T_set, [ t_value ],
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Map_t uta utr meta =>
    let ta := unparse_comparable_ty uta in
    let? '(tr, ctxt) := unparse_ty ctxt utr in
    _return ctxt
      (Michelson_v1_primitives.T_map, [ ta; tr ],
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Big_map_t uta utr meta =>
    let ta := unparse_comparable_ty uta in
    let? '(tr, ctxt) := unparse_ty ctxt utr in
    _return ctxt
      (Michelson_v1_primitives.T_big_map, [ ta; tr ],
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Sapling_transaction_t memo_size meta =>
    _return ctxt
      (Michelson_v1_primitives.T_sapling_transaction,
        [ unparse_memo_size memo_size ],
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Sapling_state_t memo_size meta =>
    _return ctxt
      (Michelson_v1_primitives.T_sapling_state, [ unparse_memo_size memo_size ],
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Chest_key_t meta =>
    _return ctxt
      (Michelson_v1_primitives.T_chest_key, nil,
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  | Script_typed_ir.Chest_t meta =>
    _return ctxt
      (Michelson_v1_primitives.T_chest, nil,
        (Script_ir_annot.unparse_type_annot
          meta.(Script_typed_ir.ty_metadata.annot)))
  end.

Fixpoint strip_var_annots {A B : Set} (function_parameter : Micheline.node A B)
  {struct function_parameter} : Micheline.node A B :=
  match function_parameter with
  | (Micheline.Int _ _ | Micheline.String _ _ | Micheline.Bytes _ _) as atom =>
    atom
  | Micheline.Seq loc args => Micheline.Seq loc (List.map strip_var_annots args)
  | Micheline.Prim loc name args annots =>
    let not_var_annot (s : string) : bool :=
      Compare.Char.(Compare.S.op_ltgt) (String.get s 0) "@" % char in
    let annots := List.filter not_var_annot annots in
    Micheline.Prim loc name (List.map strip_var_annots args) annots
  end.

Definition serialize_ty_for_error
  (ctxt : Alpha_context.context) (ty : Script_typed_ir.ty)
  : M? (Micheline.canonical Alpha_context.Script.prim * Alpha_context.context) :=
  Error_monad.record_trace (Build_extensible "Cannot_serialize_error" unit tt)
    (let? '(ty, ctxt) := unparse_ty ctxt ty in
    let? ctxt :=
      Alpha_context.Gas.consume ctxt
        (Alpha_context.Script.strip_locations_cost ty) in
    return? ((Micheline.strip_locations (strip_var_annots ty)), ctxt)).

Axiom comparable_ty_of_ty :
  Alpha_context.context -> Alpha_context.Script.location ->
  Script_typed_ir.ty ->
  M? (Script_typed_ir.comparable_ty * Alpha_context.context).

Fixpoint unparse_stack
  (ctxt : Alpha_context.context) (function_parameter : Script_typed_ir.stack_ty)
  : M?
    (list (Alpha_context.Script.expr * Alpha_context.Script.annot) *
      Alpha_context.context) :=
  match function_parameter with
  | Script_typed_ir.Bot_t => return? (nil, ctxt)
  | Script_typed_ir.Item_t ty rest annot =>
    let? '(uty, ctxt) := unparse_ty ctxt ty in
    let? '(urest, ctxt) := unparse_stack ctxt rest in
    return?
      ((cons
        ((Micheline.strip_locations uty),
          (Script_ir_annot.unparse_var_annot annot)) urest), ctxt)
  end.

Definition serialize_stack_for_error
  (ctxt : Alpha_context.context) (stack_ty : Script_typed_ir.stack_ty)
  : M?
    (list (Alpha_context.Script.expr * Alpha_context.Script.annot) *
      Alpha_context.context) :=
  Error_monad.record_trace (Build_extensible "Cannot_serialize_error" unit tt)
    (unparse_stack ctxt stack_ty).

Definition name_of_ty (function_parameter : Script_typed_ir.ty)
  : option Script_typed_ir.type_annot :=
  match function_parameter with
  | Script_typed_ir.Unit_t meta => meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Int_t meta => meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Nat_t meta => meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.String_t meta => meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Bytes_t meta => meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Mutez_t meta => meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Bool_t meta => meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Key_hash_t meta => meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Key_t meta => meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Timestamp_t meta => meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Address_t meta => meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Signature_t meta => meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Operation_t meta => meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Chain_id_t meta => meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Never_t meta => meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Contract_t _ meta =>
    meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Pair_t _ _ meta => meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Union_t _ _ meta => meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Lambda_t _ _ meta =>
    meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Option_t _ meta => meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.List_t _ meta => meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Ticket_t _ meta => meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Set_t _ meta => meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Map_t _ _ meta => meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Big_map_t _ _ meta =>
    meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Bls12_381_g1_t meta =>
    meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Bls12_381_g2_t meta =>
    meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Bls12_381_fr_t meta =>
    meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Sapling_state_t _ meta =>
    meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Sapling_transaction_t _ meta =>
    meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Chest_key_t meta => meta.(Script_typed_ir.ty_metadata.annot)
  | Script_typed_ir.Chest_t meta => meta.(Script_typed_ir.ty_metadata.annot)
  end.

Definition unparse_unit {A B : Set} (ctxt : A) (function_parameter : unit)
  : Pervasives.result (Micheline.node int Alpha_context.Script.prim * A) B :=
  let '_ := function_parameter in
  return? ((Micheline.Prim (-1) Michelson_v1_primitives.D_Unit nil nil), ctxt).

Definition unparse_int {A B C : Set}
  (ctxt : A) (v : Alpha_context.Script_int.num)
  : Pervasives.result (Micheline.node int B * A) C :=
  return? ((Micheline.Int (-1) (Alpha_context.Script_int.to_zint v)), ctxt).

Definition unparse_nat {A B C : Set}
  (ctxt : A) (v : Alpha_context.Script_int.num)
  : Pervasives.result (Micheline.node int B * A) C :=
  return? ((Micheline.Int (-1) (Alpha_context.Script_int.to_zint v)), ctxt).

Definition unparse_string {A B C : Set}
  (ctxt : A) (s : Alpha_context.Script_string.t)
  : Pervasives.result (Micheline.node int B * A) C :=
  return?
    ((Micheline.String (-1) (Alpha_context.Script_string.to_string s)), ctxt).

Definition unparse_bytes {A B C : Set} (ctxt : A) (s : bytes)
  : Pervasives.result (Micheline.node int B * A) C :=
  return? ((Micheline.Bytes (-1) s), ctxt).

Definition unparse_bool {A B : Set} (ctxt : A) (b_value : bool)
  : Pervasives.result (Micheline.node int Alpha_context.Script.prim * A) B :=
  return?
    ((Micheline.Prim (-1)
      (if b_value then
        Michelson_v1_primitives.D_True
      else
        Michelson_v1_primitives.D_False) nil nil), ctxt).

Definition unparse_timestamp {A : Set}
  (ctxt : Alpha_context.context) (mode : unparsing_mode)
  (t_value : Alpha_context.Script_timestamp.t)
  : M? (Micheline.node int A * Alpha_context.context) :=
  match mode with
  | (Optimized | Optimized_legacy) =>
    return?
      ((Micheline.Int (-1) (Alpha_context.Script_timestamp.to_zint t_value)),
        ctxt)
  | Readable =>
    let? ctxt := Alpha_context.Gas.consume ctxt Unparse_costs.timestamp_readable
      in
    match Alpha_context.Script_timestamp.to_notation t_value with
    | None =>
      return?
        ((Micheline.Int (-1) (Alpha_context.Script_timestamp.to_zint t_value)),
          ctxt)
    | Some s => return? ((Micheline.String (-1) s), ctxt)
    end
  end.

Definition unparse_address {A : Set}
  (ctxt : Alpha_context.context) (mode : unparsing_mode)
  (function_parameter : Alpha_context.Contract.t * string)
  : M? (Micheline.node int A * Alpha_context.context) :=
  let '(c, entrypoint) := function_parameter in
  let? ctxt := Alpha_context.Gas.consume ctxt Unparse_costs.contract in
  let? '_ :=
    match entrypoint with
    | "" =>
      Error_monad.error_value
        (Build_extensible "Unparsing_invariant_violated" unit tt)
    | _ => return? tt
    end in
  match mode with
  | (Optimized | Optimized_legacy) =>
    let entrypoint :=
      match entrypoint with
      | "default" => ""
      | name => name
      end in
    let bytes_value :=
      Data_encoding.Binary.to_bytes_exn None
        (Data_encoding.tup2 Alpha_context.Contract.encoding
          Data_encoding._Variable.string_value) (c, entrypoint) in
    return? ((Micheline.Bytes (-1) bytes_value), ctxt)
  | Readable =>
    let notation :=
      match entrypoint with
      | "default" => Alpha_context.Contract.to_b58check c
      | entrypoint =>
        Pervasives.op_caret (Alpha_context.Contract.to_b58check c)
          (Pervasives.op_caret "%" entrypoint)
      end in
    return? ((Micheline.String (-1) notation), ctxt)
  end.

Definition unparse_contract {A B : Set}
  (ctxt : Alpha_context.context) (mode : unparsing_mode)
  (function_parameter : A * (Alpha_context.Contract.t * string))
  : M? (Micheline.node int B * Alpha_context.context) :=
  let '(_, address) := function_parameter in
  unparse_address ctxt mode address.

Definition unparse_signature {A : Set}
  (ctxt : Alpha_context.context) (mode : unparsing_mode) (s : Signature.t)
  : M? (Micheline.node int A * Alpha_context.context) :=
  match mode with
  | (Optimized | Optimized_legacy) =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Unparse_costs.signature_optimized in
    let bytes_value :=
      Data_encoding.Binary.to_bytes_exn None Signature.encoding s in
    return? ((Micheline.Bytes (-1) bytes_value), ctxt)
  | Readable =>
    let? ctxt := Alpha_context.Gas.consume ctxt Unparse_costs.signature_readable
      in
    return? ((Micheline.String (-1) (Signature.to_b58check s)), ctxt)
  end.

Definition unparse_mutez {A B C : Set} (ctxt : A) (v : Alpha_context.Tez.tez)
  : Pervasives.result (Micheline.node int B * A) C :=
  return?
    ((Micheline.Int (-1) (Z.of_int64 (Alpha_context.Tez.to_mutez v))), ctxt).

Definition unparse_key {A : Set}
  (ctxt : Alpha_context.context) (mode : unparsing_mode)
  (k : Signature.public_key)
  : M? (Micheline.node int A * Alpha_context.context) :=
  match mode with
  | (Optimized | Optimized_legacy) =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Unparse_costs.public_key_optimized in
    let bytes_value :=
      Data_encoding.Binary.to_bytes_exn None
        Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.encoding) k in
    return? ((Micheline.Bytes (-1) bytes_value), ctxt)
  | Readable =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Unparse_costs.public_key_readable in
    return?
      ((Micheline.String (-1)
        (Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.to_b58check) k)), ctxt)
  end.

Definition unparse_key_hash {A : Set}
  (ctxt : Alpha_context.context) (mode : unparsing_mode)
  (k : Signature.public_key_hash)
  : M? (Micheline.node int A * Alpha_context.context) :=
  match mode with
  | (Optimized | Optimized_legacy) =>
    let? ctxt := Alpha_context.Gas.consume ctxt Unparse_costs.key_hash_optimized
      in
    let bytes_value :=
      Data_encoding.Binary.to_bytes_exn None
        Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding) k in
    return? ((Micheline.Bytes (-1) bytes_value), ctxt)
  | Readable =>
    let? ctxt := Alpha_context.Gas.consume ctxt Unparse_costs.key_hash_readable
      in
    return?
      ((Micheline.String (-1)
        (Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.to_b58check) k)),
        ctxt)
  end.

Definition unparse_operation {A B : Set}
  (ctxt : Alpha_context.context)
  (function_parameter : Alpha_context.packed_internal_operation * A)
  : M? (Micheline.node int B * Alpha_context.context) :=
  let '(op, _big_map_diff) := function_parameter in
  let bytes_value :=
    Data_encoding.Binary.to_bytes_exn None
      Alpha_context.Operation.internal_operation_encoding op in
  let? ctxt :=
    Alpha_context.Gas.consume ctxt (Unparse_costs.operation bytes_value) in
  return? ((Micheline.Bytes (-1) bytes_value), ctxt).

Definition unparse_chain_id {A : Set}
  (ctxt : Alpha_context.context) (mode : unparsing_mode) (chain_id : Chain_id.t)
  : M? (Micheline.node int A * Alpha_context.context) :=
  match mode with
  | (Optimized | Optimized_legacy) =>
    let? ctxt := Alpha_context.Gas.consume ctxt Unparse_costs.chain_id_optimized
      in
    let bytes_value :=
      Data_encoding.Binary.to_bytes_exn None Chain_id.encoding chain_id in
    return? ((Micheline.Bytes (-1) bytes_value), ctxt)
  | Readable =>
    let? ctxt := Alpha_context.Gas.consume ctxt Unparse_costs.chain_id_readable
      in
    return? ((Micheline.String (-1) (Chain_id.to_b58check chain_id)), ctxt)
  end.

Definition unparse_bls12_381_g1 {A : Set}
  (ctxt : Alpha_context.context) (x : Bls12_381.G1.(S.CURVE.t))
  : M? (Micheline.node int A * Alpha_context.context) :=
  let? ctxt := Alpha_context.Gas.consume ctxt Unparse_costs.bls12_381_g1 in
  let bytes_value := Bls12_381.G1.(S.CURVE.to_bytes) x in
  return? ((Micheline.Bytes (-1) bytes_value), ctxt).

Definition unparse_bls12_381_g2 {A : Set}
  (ctxt : Alpha_context.context) (x : Bls12_381.G2.(S.CURVE.t))
  : M? (Micheline.node int A * Alpha_context.context) :=
  let? ctxt := Alpha_context.Gas.consume ctxt Unparse_costs.bls12_381_g2 in
  let bytes_value := Bls12_381.G2.(S.CURVE.to_bytes) x in
  return? ((Micheline.Bytes (-1) bytes_value), ctxt).

Definition unparse_bls12_381_fr {A : Set}
  (ctxt : Alpha_context.context) (x : Bls12_381.Fr.(S.PRIME_FIELD.t))
  : M? (Micheline.node int A * Alpha_context.context) :=
  let? ctxt := Alpha_context.Gas.consume ctxt Unparse_costs.bls12_381_fr in
  let bytes_value := Bls12_381.Fr.(S.PRIME_FIELD.to_bytes) x in
  return? ((Micheline.Bytes (-1) bytes_value), ctxt).

Definition unparse_with_data_encoding {A B : Set}
  (ctxt : Alpha_context.context) (s : A) (unparse_cost : Alpha_context.Gas.cost)
  (encoding : Data_encoding.encoding A)
  : M=? (Micheline.node int B * Alpha_context.context) :=
  return=
    (let? ctxt := Alpha_context.Gas.consume ctxt unparse_cost in
    let bytes_value := Data_encoding.Binary.to_bytes_exn None encoding s in
    return? ((Micheline.Bytes (-1) bytes_value), ctxt)).

Inductive comb_witness : Set :=
| Comb_Pair : comb_witness -> comb_witness
| Comb_Any : comb_witness.

Definition unparse_pair {A B C D r F : Set}
  (unparse_l :
    A -> B ->
    M= (Pervasives.result (Micheline.node int Alpha_context.Script.prim * C) D))
  (unparse_r :
    C -> r ->
    M= (Pervasives.result (Micheline.node int Alpha_context.Script.prim * F) D))
  (ctxt : A) (mode : unparsing_mode) (r_comb_witness : comb_witness)
  (function_parameter : B * r)
  : M= (Pervasives.result (Micheline.node int Alpha_context.Script.prim * F) D) :=
  let '(l_value, r_value) := function_parameter in
  let=? '(l_value, ctxt) := unparse_l ctxt l_value in
  let=? '(r_value, ctxt) := unparse_r ctxt r_value in
  let res :=
    match (mode, r_comb_witness, r_value) with
    | (Optimized, Comb_Pair _, Micheline.Seq _ r_value) =>
      Micheline.Seq (-1) (cons l_value r_value)
    |
      (Optimized, Comb_Pair (Comb_Pair _),
        Micheline.Prim _ Michelson_v1_primitives.D_Pair
          (cons x2
            (cons
              (Micheline.Prim _ Michelson_v1_primitives.D_Pair
                (cons x3 (cons x4 [])) []) [])) []) =>
      Micheline.Seq (-1) [ l_value; x2; x3; x4 ]
    |
      (Readable, Comb_Pair _,
        Micheline.Prim _ Michelson_v1_primitives.D_Pair xs []) =>
      Micheline.Prim (-1) Michelson_v1_primitives.D_Pair (cons l_value xs) nil
    | _ =>
      Micheline.Prim (-1) Michelson_v1_primitives.D_Pair [ l_value; r_value ]
        nil
    end in
  return=? (res, ctxt).

Definition unparse_union {A B C D E : Set}
  (unparse_l :
    A -> B ->
    M= (Pervasives.result (Micheline.node int Alpha_context.Script.prim * C) D))
  (unparse_r :
    A -> E ->
    M= (Pervasives.result (Micheline.node int Alpha_context.Script.prim * C) D))
  (ctxt : A) (function_parameter : Script_typed_ir.union B E)
  : M= (Pervasives.result (Micheline.node int Alpha_context.Script.prim * C) D) :=
  match function_parameter with
  | Script_typed_ir.L l_value =>
    let=? '(l_value, ctxt) := unparse_l ctxt l_value in
    return=?
      ((Micheline.Prim (-1) Michelson_v1_primitives.D_Left [ l_value ] nil),
        ctxt)
  | Script_typed_ir.R r_value =>
    let=? '(r_value, ctxt) := unparse_r ctxt r_value in
    return=?
      ((Micheline.Prim (-1) Michelson_v1_primitives.D_Right [ r_value ] nil),
        ctxt)
  end.

Definition unparse_option {A B C : Set}
  (unparse_v :
    A -> B ->
    M= (Pervasives.result (Micheline.node int Alpha_context.Script.prim * A) C))
  (ctxt : A) (function_parameter : option B)
  : M= (Pervasives.result (Micheline.node int Alpha_context.Script.prim * A) C) :=
  match function_parameter with
  | Some v =>
    let=? '(v, ctxt) := unparse_v ctxt v in
    return=?
      ((Micheline.Prim (-1) Michelson_v1_primitives.D_Some [ v ] nil), ctxt)
  | None =>
    return=?
      ((Micheline.Prim (-1) Michelson_v1_primitives.D_None nil nil), ctxt)
  end.

Definition comparable_comb_witness2
  (function_parameter : Script_typed_ir.comparable_ty) : comb_witness :=
  match function_parameter with
  | Script_typed_ir.Pair_key _ (Script_typed_ir.Pair_key _ _ _, _) _ =>
    Comb_Pair (Comb_Pair Comb_Any)
  | Script_typed_ir.Pair_key _ _ _ => Comb_Pair Comb_Any
  | _ => Comb_Any
  end.

Axiom unparse_comparable_data : forall {a : Set},
  Alpha_context.context -> unparsing_mode -> Script_typed_ir.comparable_ty ->
  a -> M=? (Alpha_context.Script.node * Alpha_context.context).

Definition pack_node
  (unparsed : Alpha_context.Script.node) (ctxt : Alpha_context.context)
  : M? (bytes * Alpha_context.context) :=
  let? ctxt :=
    Alpha_context.Gas.consume ctxt
      (Alpha_context.Script.strip_locations_cost unparsed) in
  let bytes_value :=
    Data_encoding.Binary.to_bytes_exn None Alpha_context.Script.expr_encoding
      (Micheline.strip_locations unparsed) in
  let? ctxt :=
    Alpha_context.Gas.consume ctxt
      (Alpha_context.Script.serialized_cost bytes_value) in
  let bytes_value := Bytes.cat (Bytes.of_string "\005") bytes_value in
  let? ctxt :=
    Alpha_context.Gas.consume ctxt
      (Alpha_context.Script.serialized_cost bytes_value) in
  return? (bytes_value, ctxt).

Definition pack_comparable_data {A : Set}
  (ctxt : Alpha_context.context) (typ : Script_typed_ir.comparable_ty)
  (data : A) (mode : unparsing_mode) : M=? (bytes * Alpha_context.context) :=
  let=? '(unparsed, ctxt) := unparse_comparable_data ctxt mode typ data in
  return= (pack_node unparsed ctxt).

Definition hash_bytes (ctxt : Alpha_context.context) (bytes_value : bytes)
  : M? (Script_expr_hash.t * Alpha_context.context) :=
  let? ctxt :=
    Alpha_context.Gas.consume ctxt
      (Michelson_v1_gas.Cost_of.Interpreter.blake2b bytes_value) in
  return? ((Script_expr_hash.hash_bytes None [ bytes_value ]), ctxt).

Definition hash_comparable_data {A : Set}
  (ctxt : Alpha_context.context) (typ : Script_typed_ir.comparable_ty)
  (data : A) : M=? (Script_expr_hash.t * Alpha_context.context) :=
  let=? '(bytes_value, ctxt) :=
    pack_comparable_data ctxt typ data Optimized_legacy in
  return= (hash_bytes ctxt bytes_value).

Definition check_dupable_comparable_ty
  (function_parameter : Script_typed_ir.comparable_ty) : unit :=
  match function_parameter with
  |
    (Script_typed_ir.Unit_key _ | Script_typed_ir.Never_key _ |
    Script_typed_ir.Int_key _ | Script_typed_ir.Nat_key _ |
    Script_typed_ir.Signature_key _ | Script_typed_ir.String_key _ |
    Script_typed_ir.Bytes_key _ | Script_typed_ir.Mutez_key _ |
    Script_typed_ir.Bool_key _ | Script_typed_ir.Key_hash_key _ |
    Script_typed_ir.Key_key _ | Script_typed_ir.Timestamp_key _ |
    Script_typed_ir.Chain_id_key _ | Script_typed_ir.Address_key _ |
    Script_typed_ir.Pair_key _ _ _ | Script_typed_ir.Union_key _ _ _ |
    Script_typed_ir.Option_key _ _) => tt
  end.

Fixpoint check_dupable_ty
  (ctxt : Alpha_context.context) (loc : Alpha_context.Script.location)
  (ty : Script_typed_ir.ty) : M? Alpha_context.context :=
  let? ctxt :=
    Alpha_context.Gas.consume ctxt Typecheck_costs.check_dupable_cycle in
  match ty with
  | Script_typed_ir.Unit_t _ => return? ctxt
  | Script_typed_ir.Int_t _ => return? ctxt
  | Script_typed_ir.Nat_t _ => return? ctxt
  | Script_typed_ir.Signature_t _ => return? ctxt
  | Script_typed_ir.String_t _ => return? ctxt
  | Script_typed_ir.Bytes_t _ => return? ctxt
  | Script_typed_ir.Mutez_t _ => return? ctxt
  | Script_typed_ir.Key_hash_t _ => return? ctxt
  | Script_typed_ir.Key_t _ => return? ctxt
  | Script_typed_ir.Timestamp_t _ => return? ctxt
  | Script_typed_ir.Address_t _ => return? ctxt
  | Script_typed_ir.Bool_t _ => return? ctxt
  | Script_typed_ir.Contract_t _ _ => return? ctxt
  | Script_typed_ir.Operation_t _ => return? ctxt
  | Script_typed_ir.Chain_id_t _ => return? ctxt
  | Script_typed_ir.Never_t _ => return? ctxt
  | Script_typed_ir.Bls12_381_g1_t _ => return? ctxt
  | Script_typed_ir.Bls12_381_g2_t _ => return? ctxt
  | Script_typed_ir.Bls12_381_fr_t _ => return? ctxt
  | Script_typed_ir.Sapling_state_t _ _ => return? ctxt
  | Script_typed_ir.Sapling_transaction_t _ _ => return? ctxt
  | Script_typed_ir.Chest_t _ => return? ctxt
  | Script_typed_ir.Chest_key_t _ => return? ctxt
  | Script_typed_ir.Ticket_t _ _ =>
    Error_monad.error_value
      (Build_extensible "Unexpected_ticket" Alpha_context.Script.location loc)
  | Script_typed_ir.Pair_t (ty_a, _, _) (ty_b, _, _) _ =>
    let? ctxt := check_dupable_ty ctxt loc ty_a in
    check_dupable_ty ctxt loc ty_b
  | Script_typed_ir.Union_t (ty_a, _) (ty_b, _) _ =>
    let? ctxt := check_dupable_ty ctxt loc ty_a in
    check_dupable_ty ctxt loc ty_b
  | Script_typed_ir.Lambda_t _ _ _ => return? ctxt
  | Script_typed_ir.Option_t ty _ => check_dupable_ty ctxt loc ty
  | Script_typed_ir.List_t ty _ => check_dupable_ty ctxt loc ty
  | Script_typed_ir.Set_t key_ty _ =>
    let '_ := check_dupable_comparable_ty key_ty in
    return? ctxt
  | Script_typed_ir.Map_t key_ty val_ty _ =>
    let '_ := check_dupable_comparable_ty key_ty in
    check_dupable_ty ctxt loc val_ty
  | Script_typed_ir.Big_map_t key_ty val_ty _ =>
    let '_ := check_dupable_comparable_ty key_ty in
    check_dupable_ty ctxt loc val_ty
  end.

Inductive eq : Set :=
| Eq : eq.

Definition record_inconsistent_types {A : Set}
  (ctxt : Alpha_context.context) (loc : Alpha_context.Script.location)
  (ta : Script_typed_ir.ty) (tb : Script_typed_ir.ty) : M? A -> M? A :=
  Error_monad.record_trace_eval
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      let? '(ta, ctxt) := serialize_ty_for_error ctxt ta in
      let? '(tb, _ctxt) := serialize_ty_for_error ctxt tb in
      return?
        (Build_extensible "Inconsistent_types"
          (option Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim *
            Micheline.canonical Alpha_context.Script.prim) ((Some loc), ta, tb))).

Module GAS_MONAD.
  Record signature {t : Set -> Set} : Set := {
    t := t;
    gas_monad := forall {a : Set}, t a;
    _return : forall {a : Set}, a -> t a;
    op_gtgtdollar : forall {a b : Set}, t a -> (a -> t b) -> t b;
    op_gtpipedollar : forall {a b : Set}, t a -> (a -> b) -> t b;
    op_gtquestiondollar : forall {a b : Set}, t a -> (a -> M? b) -> t b;
    op_gtquestionquestiondollar :
      forall {a b : Set}, t a -> (M? a -> t b) -> t b;
    from_tzresult : forall {a : Set}, M? a -> t a;
    unsafe_embed :
      forall {a : Set},
      (Alpha_context.context -> M? (a * Alpha_context.context)) -> t a;
    gas_consume : Alpha_context.Gas.cost -> t unit;
    run :
      forall {a : Set},
      Alpha_context.context -> t a -> M? (M? a * Alpha_context.context);
    record_trace_eval :
      forall {a : Set}, (unit -> M? Error_monad._error) -> t a -> t a;
    get_context : t Alpha_context.context;
  }.
End GAS_MONAD.
Definition GAS_MONAD := @GAS_MONAD.signature.
Arguments GAS_MONAD {_}.

Module Gas_monad.
  Definition t (a : Set) : Set :=
    Alpha_context.context -> M? (M? a * Alpha_context.context).
  
  Definition gas_monad (a : Set) : Set := t a.
  
  Definition from_tzresult {A B C : Set} (x : A) (ctxt : B)
    : Pervasives.result (A * B) C := return? (x, ctxt).
  
  Definition _return {A B C D : Set} (x : A)
    : B -> Pervasives.result (Pervasives.result A C * B) D :=
    from_tzresult (return? x).
  
  Definition op_gtgtdollar {A B C D E F : Set}
    (m : A -> Pervasives.result (Pervasives.result B C * D) E)
    (f : B -> D -> Pervasives.result (Pervasives.result F C * D) E) (ctxt : A)
    : Pervasives.result (Pervasives.result F C * D) E :=
    let? '(x, ctxt) := m ctxt in
    match x with
    | Pervasives.Ok y => f y ctxt
    | (Pervasives.Error _) as err => from_tzresult err ctxt
    end.
  
  Definition op_gtpipedollar {A B C D E F : Set}
    (m : A -> Pervasives.result (Pervasives.result B C * D) E) (f : B -> F)
    (ctxt : A) : Pervasives.result (Pervasives.result F C * D) E :=
    let? '(x, ctxt) := m ctxt in
    from_tzresult (Error_monad.op_gtpipequestion x f) ctxt.
  
  Definition op_gtquestiondollar {A B C D E F : Set}
    (m : A -> Pervasives.result (Pervasives.result B C * D) E)
    (f : B -> Pervasives.result F C)
    : A -> Pervasives.result (Pervasives.result F C * D) E :=
    op_gtgtdollar m (fun (x : B) => from_tzresult (f x)).
  
  Definition op_gtquestionquestiondollar {A B C D E : Set}
    (m : A -> Pervasives.result (B * C) D) (f : B -> C -> Pervasives.result E D)
    (ctxt : A) : Pervasives.result E D :=
    let? '(x, ctxt) := m ctxt in
    f x ctxt.
  
  Definition unsafe_embed {A B C D E : Set}
    (f : A -> Pervasives.result (B * C) D) (ctxt : A)
    : Pervasives.result (Pervasives.result B E * C) D :=
    let? '(x, ctxt) := f ctxt in
    _return x ctxt.
  
  Definition gas_consume {A : Set}
    (cost : Alpha_context.Gas.cost) (ctxt : Alpha_context.context)
    : M? (Pervasives.result unit A * Alpha_context.context) :=
    Error_monad.op_gtgtquestion (Alpha_context.Gas.consume ctxt cost)
      (_return tt).
  
  Definition run {A B : Set} (ctxt : A) (x : A -> B) : B := x ctxt.
  
  Definition get_context {A B C : Set} (ctxt : A)
    : Pervasives.result (Pervasives.result A B * A) C := _return ctxt ctxt.
  
  Definition record_trace_eval {A B C : Set}
    (f : unit -> M? A) (x : B -> M? C) (ctxt : B) : M? C :=
    Error_monad.record_trace_eval f (x ctxt).
  
  Definition module :=
    {|
      GAS_MONAD._return _ := _return;
      GAS_MONAD.op_gtgtdollar _ _ := op_gtgtdollar;
      GAS_MONAD.op_gtpipedollar _ _ := op_gtpipedollar;
      GAS_MONAD.op_gtquestiondollar _ _ := op_gtquestiondollar;
      GAS_MONAD.op_gtquestionquestiondollar _ _ := op_gtquestionquestiondollar;
      GAS_MONAD.from_tzresult _ := from_tzresult;
      GAS_MONAD.unsafe_embed _ := unsafe_embed;
      GAS_MONAD.gas_consume := gas_consume;
      GAS_MONAD.run _ := run;
      GAS_MONAD.record_trace_eval _ := record_trace_eval;
      GAS_MONAD.get_context := get_context
    |}.
End Gas_monad.
Definition Gas_monad : GAS_MONAD (t := _) := Gas_monad.module.

Definition serialize_ty_for_error_carbonated (t_value : Script_typed_ir.ty)
  : Gas_monad.(GAS_MONAD.t) (Micheline.canonical Alpha_context.Script.prim) :=
  Gas_monad.(GAS_MONAD.unsafe_embed)
    (fun (ctxt : Alpha_context.context) => serialize_ty_for_error ctxt t_value).

Definition merge_type_metadata
  (legacy : bool) (function_parameter : Script_typed_ir.ty_metadata)
  : Script_typed_ir.ty_metadata -> M? Script_typed_ir.ty_metadata :=
  let '{|
    Script_typed_ir.ty_metadata.annot := annot_a;
      Script_typed_ir.ty_metadata.size := size_a
      |} := function_parameter in
  fun (function_parameter : Script_typed_ir.ty_metadata) =>
    let '{|
      Script_typed_ir.ty_metadata.annot := annot_b;
        Script_typed_ir.ty_metadata.size := size_b
        |} := function_parameter in
    let? size_value := Script_typed_ir.Type_size.merge size_a size_b in
    let? annot := Script_ir_annot.merge_type_annot legacy annot_a annot_b in
    return?
      {| Script_typed_ir.ty_metadata.annot := annot;
        Script_typed_ir.ty_metadata.size := size_value |}.

Fixpoint merge_comparable_types
  (legacy : bool) (ta : Script_typed_ir.comparable_ty)
  (tb : Script_typed_ir.comparable_ty)
  : Gas_monad.(GAS_MONAD.t) (eq * Script_typed_ir.comparable_ty) :=
  Gas_monad.(GAS_MONAD.op_gtgtdollar)
    (Gas_monad.(GAS_MONAD.gas_consume) Typecheck_costs.merge_cycle)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      let merge_type_metadata
        (legacy : bool) (meta_a : Script_typed_ir.ty_metadata)
        (meta_b : Script_typed_ir.ty_metadata)
        : Gas_monad.(GAS_MONAD.t) Script_typed_ir.ty_metadata :=
        Gas_monad.(GAS_MONAD.from_tzresult)
          (merge_type_metadata legacy meta_a meta_b) in
      let merge_field_annot
        (legacy : bool) (annot_a : option Script_typed_ir.field_annot)
        (annot_b : option Script_typed_ir.field_annot)
        : Gas_monad.(GAS_MONAD.t) (option Script_typed_ir.field_annot) :=
        Gas_monad.(GAS_MONAD.from_tzresult)
          (Script_ir_annot.merge_field_annot legacy annot_a annot_b) in
      let _return
        (f : Script_typed_ir.ty_metadata -> Script_typed_ir.comparable_ty)
        (eq_value : eq) (annot_a : Script_typed_ir.ty_metadata)
        (annot_b : Script_typed_ir.ty_metadata)
        : Gas_monad.(GAS_MONAD.t) (eq * Script_typed_ir.comparable_ty) :=
        Gas_monad.(GAS_MONAD.op_gtgtdollar)
          (merge_type_metadata legacy annot_a annot_b)
          (fun (annot : Script_typed_ir.ty_metadata) =>
            Gas_monad.(GAS_MONAD._return) (eq_value, (f annot))) in
      match (ta, tb) with
      | (Script_typed_ir.Unit_key annot_a, Script_typed_ir.Unit_key annot_b) =>
        _return
          (fun (annot : Script_typed_ir.ty_metadata) =>
            Script_typed_ir.Unit_key annot) Eq annot_a annot_b
      | (Script_typed_ir.Never_key annot_a, Script_typed_ir.Never_key annot_b)
        =>
        _return
          (fun (annot : Script_typed_ir.ty_metadata) =>
            Script_typed_ir.Never_key annot) Eq annot_a annot_b
      | (Script_typed_ir.Int_key annot_a, Script_typed_ir.Int_key annot_b) =>
        _return
          (fun (annot : Script_typed_ir.ty_metadata) =>
            Script_typed_ir.Int_key annot) Eq annot_a annot_b
      | (Script_typed_ir.Nat_key annot_a, Script_typed_ir.Nat_key annot_b) =>
        _return
          (fun (annot : Script_typed_ir.ty_metadata) =>
            Script_typed_ir.Nat_key annot) Eq annot_a annot_b
      |
        (Script_typed_ir.Signature_key annot_a,
          Script_typed_ir.Signature_key annot_b) =>
        _return
          (fun (annot : Script_typed_ir.ty_metadata) =>
            Script_typed_ir.Signature_key annot) Eq annot_a annot_b
      | (Script_typed_ir.String_key annot_a, Script_typed_ir.String_key annot_b)
        =>
        _return
          (fun (annot : Script_typed_ir.ty_metadata) =>
            Script_typed_ir.String_key annot) Eq annot_a annot_b
      | (Script_typed_ir.Bytes_key annot_a, Script_typed_ir.Bytes_key annot_b)
        =>
        _return
          (fun (annot : Script_typed_ir.ty_metadata) =>
            Script_typed_ir.Bytes_key annot) Eq annot_a annot_b
      | (Script_typed_ir.Mutez_key annot_a, Script_typed_ir.Mutez_key annot_b)
        =>
        _return
          (fun (annot : Script_typed_ir.ty_metadata) =>
            Script_typed_ir.Mutez_key annot) Eq annot_a annot_b
      | (Script_typed_ir.Bool_key annot_a, Script_typed_ir.Bool_key annot_b) =>
        _return
          (fun (annot : Script_typed_ir.ty_metadata) =>
            Script_typed_ir.Bool_key annot) Eq annot_a annot_b
      |
        (Script_typed_ir.Key_hash_key annot_a,
          Script_typed_ir.Key_hash_key annot_b) =>
        _return
          (fun (annot : Script_typed_ir.ty_metadata) =>
            Script_typed_ir.Key_hash_key annot) Eq annot_a annot_b
      | (Script_typed_ir.Key_key annot_a, Script_typed_ir.Key_key annot_b) =>
        _return
          (fun (annot : Script_typed_ir.ty_metadata) =>
            Script_typed_ir.Key_key annot) Eq annot_a annot_b
      |
        (Script_typed_ir.Timestamp_key annot_a,
          Script_typed_ir.Timestamp_key annot_b) =>
        _return
          (fun (annot : Script_typed_ir.ty_metadata) =>
            Script_typed_ir.Timestamp_key annot) Eq annot_a annot_b
      |
        (Script_typed_ir.Chain_id_key annot_a,
          Script_typed_ir.Chain_id_key annot_b) =>
        _return
          (fun (annot : Script_typed_ir.ty_metadata) =>
            Script_typed_ir.Chain_id_key annot) Eq annot_a annot_b
      |
        (Script_typed_ir.Address_key annot_a,
          Script_typed_ir.Address_key annot_b) =>
        _return
          (fun (annot : Script_typed_ir.ty_metadata) =>
            Script_typed_ir.Address_key annot) Eq annot_a annot_b
      |
        (Script_typed_ir.Pair_key (left_a, annot_left_a)
          (right_a, annot_right_a) annot_a,
          Script_typed_ir.Pair_key (left_b, annot_left_b)
            (right_b, annot_right_b) annot_b) =>
        Gas_monad.(GAS_MONAD.op_gtgtdollar)
          (merge_type_metadata legacy annot_a annot_b)
          (fun (annot : Script_typed_ir.ty_metadata) =>
            Gas_monad.(GAS_MONAD.op_gtgtdollar)
              (merge_field_annot legacy annot_left_a annot_left_b)
              (fun (annot_left : option Script_typed_ir.field_annot) =>
                Gas_monad.(GAS_MONAD.op_gtgtdollar)
                  (merge_field_annot legacy annot_right_a annot_right_b)
                  (fun (annot_right : option Script_typed_ir.field_annot) =>
                    Gas_monad.(GAS_MONAD.op_gtgtdollar)
                      (merge_comparable_types legacy left_a left_b)
                      (fun (function_parameter :
                        eq * Script_typed_ir.comparable_ty) =>
                        let '(Eq, _left) := function_parameter in
                        Gas_monad.(GAS_MONAD.op_gtpipedollar)
                          (merge_comparable_types legacy right_a right_b)
                          (fun (function_parameter :
                            eq * Script_typed_ir.comparable_ty) =>
                            let '(Eq, _right) := function_parameter in
                            (Eq,
                              (Script_typed_ir.Pair_key (_left, annot_left)
                                (_right, annot_right) annot)))))))
      |
        (Script_typed_ir.Union_key (left_a, annot_left_a)
          (right_a, annot_right_a) annot_a,
          Script_typed_ir.Union_key (left_b, annot_left_b)
            (right_b, annot_right_b) annot_b) =>
        Gas_monad.(GAS_MONAD.op_gtgtdollar)
          (merge_type_metadata legacy annot_a annot_b)
          (fun (annot : Script_typed_ir.ty_metadata) =>
            Gas_monad.(GAS_MONAD.op_gtgtdollar)
              (merge_field_annot legacy annot_left_a annot_left_b)
              (fun (annot_left : option Script_typed_ir.field_annot) =>
                Gas_monad.(GAS_MONAD.op_gtgtdollar)
                  (merge_field_annot legacy annot_right_a annot_right_b)
                  (fun (annot_right : option Script_typed_ir.field_annot) =>
                    Gas_monad.(GAS_MONAD.op_gtgtdollar)
                      (merge_comparable_types legacy left_a left_b)
                      (fun (function_parameter :
                        eq * Script_typed_ir.comparable_ty) =>
                        let '(Eq, _left) := function_parameter in
                        Gas_monad.(GAS_MONAD.op_gtpipedollar)
                          (merge_comparable_types legacy right_a right_b)
                          (fun (function_parameter :
                            eq * Script_typed_ir.comparable_ty) =>
                            let '(Eq, _right) := function_parameter in
                            (Eq,
                              (Script_typed_ir.Union_key (_left, annot_left)
                                (_right, annot_right) annot)))))))
      |
        (Script_typed_ir.Option_key ta annot_a,
          Script_typed_ir.Option_key tb annot_b) =>
        Gas_monad.(GAS_MONAD.op_gtgtdollar)
          (merge_type_metadata legacy annot_a annot_b)
          (fun (annot : Script_typed_ir.ty_metadata) =>
            Gas_monad.(GAS_MONAD.op_gtpipedollar)
              (merge_comparable_types legacy ta tb)
              (fun (function_parameter : eq * Script_typed_ir.comparable_ty) =>
                let '(Eq, t_value) := function_parameter in
                (Eq, (Script_typed_ir.Option_key t_value annot))))
      | (_, _) =>
        Gas_monad.(GAS_MONAD.op_gtgtdollar)
          (serialize_ty_for_error_carbonated (ty_of_comparable_ty ta))
          (fun (ta : Micheline.canonical Alpha_context.Script.prim) =>
            Gas_monad.(GAS_MONAD.op_gtquestiondollar)
              (serialize_ty_for_error_carbonated (ty_of_comparable_ty tb))
              (fun (tb : Micheline.canonical Alpha_context.Script.prim) =>
                Error_monad.error_value
                  (Build_extensible "Inconsistent_types"
                    (option Alpha_context.Script.location *
                      Micheline.canonical Alpha_context.Script.prim *
                      Micheline.canonical Alpha_context.Script.prim)
                    (None, ta, tb))))
      end).

Definition comparable_ty_eq
  (ctxt : Alpha_context.context) (ta : Script_typed_ir.comparable_ty)
  (tb : Script_typed_ir.comparable_ty) : M? (eq * Alpha_context.context) :=
  let? '(eq_ty, ctxt) :=
    Gas_monad.(GAS_MONAD.run) ctxt (merge_comparable_types true ta tb) in
  let? '(eq_value, _ty) := eq_ty in
  return? (eq_value, ctxt).

Definition merge_memo_sizes
  (ms1 : Alpha_context.Sapling.Memo_size.t)
  (ms2 : Alpha_context.Sapling.Memo_size.t)
  : M? Alpha_context.Sapling.Memo_size.t :=
  if Alpha_context.Sapling.Memo_size.equal ms1 ms2 then
    return? ms1
  else
    Error_monad.error_value
      (Build_extensible "Inconsistent_memo_sizes"
        (Alpha_context.Sapling.Memo_size.t * Alpha_context.Sapling.Memo_size.t)
        (ms1, ms2)).

Inductive merge_type_error_flag : Set :=
| Default_merge_type_error : merge_type_error_flag
| Fast_merge_type_error : merge_type_error_flag.

Definition default_merge_type_error
  (ty1 : Script_typed_ir.ty) (ty2 : Script_typed_ir.ty)
  : Gas_monad.(GAS_MONAD.t) Error_monad._error :=
  Gas_monad.(GAS_MONAD.op_gtgtdollar) (serialize_ty_for_error_carbonated ty1)
    (fun (ty1 : Micheline.canonical Alpha_context.Script.prim) =>
      Gas_monad.(GAS_MONAD.op_gtquestiondollar)
        (serialize_ty_for_error_carbonated ty2)
        (fun (ty2 : Micheline.canonical Alpha_context.Script.prim) =>
          return?
            (Build_extensible "Inconsistent_types"
              (option Alpha_context.Script.location *
                Micheline.canonical Alpha_context.Script.prim *
                Micheline.canonical Alpha_context.Script.prim) (None, ty1, ty2)))).

Definition fast_merge_type_error {A B : Set} (_ty1 : A) (_ty2 : B)
  : Gas_monad.(GAS_MONAD.t) Error_monad._error :=
  Gas_monad.(GAS_MONAD._return)
    (Build_extensible "Inconsistent_types_fast" unit tt).

Definition merge_type_error (merge_type_error_flag : merge_type_error_flag)
  : Script_typed_ir.ty -> Script_typed_ir.ty ->
  Gas_monad.(GAS_MONAD.t) Error_monad._error :=
  match merge_type_error_flag with
  | Default_merge_type_error => default_merge_type_error
  | Fast_merge_type_error => fast_merge_type_error
  end.

Definition record_inconsistent_carbonated {A : Set}
  (ctxt : Alpha_context.context) (ta : Script_typed_ir.ty)
  (tb : Script_typed_ir.ty)
  : Gas_monad.(GAS_MONAD.t) A -> Gas_monad.(GAS_MONAD.t) A :=
  Gas_monad.(GAS_MONAD.record_trace_eval)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      let? '(ta, ctxt) := serialize_ty_for_error ctxt ta in
      let? '(tb, _ctxt) := serialize_ty_for_error ctxt tb in
      return?
        (Build_extensible "Inconsistent_types"
          (option Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim *
            Micheline.canonical Alpha_context.Script.prim) (None, ta, tb))).

Axiom merge_types :
  bool -> merge_type_error_flag -> Alpha_context.Script.location ->
  Script_typed_ir.ty -> Script_typed_ir.ty ->
  Gas_monad.(GAS_MONAD.t) (eq * Script_typed_ir.ty).

Definition ty_eq
  (legacy : bool) (ctxt : Alpha_context.context)
  (loc : Alpha_context.Script.location) (ta : Script_typed_ir.ty)
  (tb : Script_typed_ir.ty) : M? (eq * Alpha_context.context) :=
  let? '(eq_ty, ctxt) :=
    Gas_monad.(GAS_MONAD.run) ctxt
      (merge_types legacy Default_merge_type_error loc ta tb) in
  let? '(eq_value, _ty) := eq_ty in
  return? (eq_value, ctxt).

Definition merge_stacks (legacy : bool) (loc : Alpha_context.Script.location)
  : Alpha_context.context -> int -> Script_typed_ir.stack_ty ->
  Script_typed_ir.stack_ty ->
  M? (eq * Script_typed_ir.stack_ty * Alpha_context.context) :=
  let fix help
    (ctxt : Alpha_context.context) (lvl : int)
    (stack1 : Script_typed_ir.stack_ty) (stack2 : Script_typed_ir.stack_ty)
    : M? (eq * Script_typed_ir.stack_ty * Alpha_context.context) :=
    match (stack1, stack2) with
    | (Script_typed_ir.Bot_t, Script_typed_ir.Bot_t) =>
      return? (Eq, Script_typed_ir.Bot_t, ctxt)
    |
      (Script_typed_ir.Item_t ty1 rest1 annot1,
        Script_typed_ir.Item_t ty2 rest2 annot2) =>
      let? '(eq_ty, ctxt) :=
        Error_monad.record_trace (Build_extensible "Bad_stack_item" int lvl)
          (Gas_monad.(GAS_MONAD.run) ctxt
            (merge_types legacy Default_merge_type_error loc ty1 ty2)) in
      let? '(Eq, ty) := eq_ty in
      let? '(Eq, rest, ctxt) := help ctxt (lvl +i 1) rest1 rest2 in
      let annot := Script_ir_annot.merge_var_annot annot1 annot2 in
      return? (Eq, (Script_typed_ir.Item_t ty rest annot), ctxt)
    | (_, _) =>
      Error_monad.error_value (Build_extensible "Bad_stack_length" unit tt)
    end in
  help.

(** Records for the constructor parameters *)
Module ConstructorRecords_judgement.
  Module judgement.
    Module Failed.
      Record record {descr : Set} : Set := Build {
        descr : descr }.
      Arguments record : clear implicits.
      Definition with_descr {t_descr} descr (r : record t_descr) :=
        Build t_descr descr.
    End Failed.
    Definition Failed_skeleton := Failed.record.
  End judgement.
End ConstructorRecords_judgement.
Import ConstructorRecords_judgement.

Reserved Notation "'judgement.Failed".

Inductive judgement (a s : Set) : Set :=
| Typed : descr -> judgement a s
| Failed : 'judgement.Failed -> judgement a s

where "'judgement.Failed" :=
  (judgement.Failed_skeleton (Script_typed_ir.stack_ty -> descr)).

Module judgement.
  Include ConstructorRecords_judgement.judgement.
  Definition Failed := 'judgement.Failed.
End judgement.

Arguments Typed {_ _}.
Arguments Failed {_ _}.

Module branch.
  Record record : Set := Build {
    branch : descr -> descr -> descr }.
  Definition with_branch branch (r : record) :=
    Build branch.
End branch.
Definition branch := branch.record.

Definition merge_branches {a s b u c v : Set}
  (legacy : bool) (ctxt : Alpha_context.context) (loc : int)
  (btr : judgement a s) (bfr : judgement b u) (function_parameter : branch)
  : M? (judgement c v * Alpha_context.context) :=
  let '{| branch.branch := branch |} := function_parameter in
  match (btr, bfr) with
  |
    (Typed ({| descr.aft := aftbt |} as dbt),
      Typed ({| descr.aft := aftbf |} as dbf)) =>
    let unmatched_branches (function_parameter : unit)
      : M? Error_monad._error :=
      let '_ := function_parameter in
      let? '(aftbt, ctxt) := serialize_stack_for_error ctxt aftbt in
      let? '(aftbf, _ctxt) := serialize_stack_for_error ctxt aftbf in
      return?
        (Build_extensible "Unmatched_branches"
          (Alpha_context.Script.location * Script_tc_errors.unparsed_stack_ty *
            Script_tc_errors.unparsed_stack_ty) (loc, aftbt, aftbf)) in
    Error_monad.record_trace_eval unmatched_branches
      (let? '(Eq, merged_stack, ctxt) :=
        merge_stacks legacy loc ctxt 1 aftbt aftbf in
      return?
        ((Typed
          (branch (descr.with_aft merged_stack dbt)
            (descr.with_aft merged_stack dbf))), ctxt))
  |
    (Failed {| judgement.Failed.descr := descrt |},
      Failed {| judgement.Failed.descr := descrf |}) =>
    let descr_value (ret : Script_typed_ir.stack_ty) : descr :=
      branch (descrt ret) (descrf ret) in
    return? ((Failed {| judgement.Failed.descr := descr_value |}), ctxt)
  | (Typed dbt, Failed {| judgement.Failed.descr := descrf |}) =>
    return? ((Typed (branch dbt (descrf dbt.(descr.aft)))), ctxt)
  | (Failed {| judgement.Failed.descr := descrt |}, Typed dbf) =>
    return? ((Typed (branch (descrt dbf.(descr.aft)) dbf)), ctxt)
  end.

Definition parse_memo_size
  (n : Micheline.node Alpha_context.Script.location Alpha_context.Script.prim)
  : M? Alpha_context.Sapling.Memo_size.t :=
  match n with
  | Micheline.Int _ z =>
    match Alpha_context.Sapling.Memo_size.parse_z z with
    | (Pervasives.Ok _) as ok_memo_size =>
      cast (M? Alpha_context.Sapling.Memo_size.t) ok_memo_size
    | Pervasives.Error msg =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          ((location n), (Micheline.strip_locations n), msg))
    end
  | _ =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location n), [ Script_tc_errors.Int_kind ], (kind_value n)))
  end.

Inductive ex_comparable_ty : Set :=
| Ex_comparable_ty : Script_typed_ir.comparable_ty -> ex_comparable_ty.

Fixpoint parse_comparable_ty
  (stack_depth : int) (ctxt : Alpha_context.context)
  (ty : Alpha_context.Script.node) {struct ty}
  : M? (ex_comparable_ty * Alpha_context.context) :=
  let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.parse_type_cycle
    in
  if stack_depth >i 10000 then
    Error_monad.error_value
      (Build_extensible "Typechecking_too_many_recursive_calls" unit tt)
  else
    match ty with
    | Micheline.Prim loc Michelson_v1_primitives.T_unit [] annot =>
      let? annot := Script_ir_annot.parse_type_annot loc annot in
      return? ((Ex_comparable_ty (Script_typed_ir.unit_key annot)), ctxt)
    | Micheline.Prim loc Michelson_v1_primitives.T_never [] annot =>
      let? annot := Script_ir_annot.parse_type_annot loc annot in
      return? ((Ex_comparable_ty (Script_typed_ir.never_key annot)), ctxt)
    | Micheline.Prim loc Michelson_v1_primitives.T_int [] annot =>
      let? annot := Script_ir_annot.parse_type_annot loc annot in
      return? ((Ex_comparable_ty (Script_typed_ir.int_key annot)), ctxt)
    | Micheline.Prim loc Michelson_v1_primitives.T_nat [] annot =>
      let? annot := Script_ir_annot.parse_type_annot loc annot in
      return? ((Ex_comparable_ty (Script_typed_ir.nat_key annot)), ctxt)
    | Micheline.Prim loc Michelson_v1_primitives.T_signature [] annot =>
      let? annot := Script_ir_annot.parse_type_annot loc annot in
      return? ((Ex_comparable_ty (Script_typed_ir.signature_key annot)), ctxt)
    | Micheline.Prim loc Michelson_v1_primitives.T_string [] annot =>
      let? annot := Script_ir_annot.parse_type_annot loc annot in
      return? ((Ex_comparable_ty (Script_typed_ir.string_key annot)), ctxt)
    | Micheline.Prim loc Michelson_v1_primitives.T_bytes [] annot =>
      let? annot := Script_ir_annot.parse_type_annot loc annot in
      return? ((Ex_comparable_ty (Script_typed_ir.bytes_key annot)), ctxt)
    | Micheline.Prim loc Michelson_v1_primitives.T_mutez [] annot =>
      let? annot := Script_ir_annot.parse_type_annot loc annot in
      return? ((Ex_comparable_ty (Script_typed_ir.mutez_key annot)), ctxt)
    | Micheline.Prim loc Michelson_v1_primitives.T_bool [] annot =>
      let? annot := Script_ir_annot.parse_type_annot loc annot in
      return? ((Ex_comparable_ty (Script_typed_ir.bool_key annot)), ctxt)
    | Micheline.Prim loc Michelson_v1_primitives.T_key_hash [] annot =>
      let? annot := Script_ir_annot.parse_type_annot loc annot in
      return? ((Ex_comparable_ty (Script_typed_ir.key_hash_key annot)), ctxt)
    | Micheline.Prim loc Michelson_v1_primitives.T_key [] annot =>
      let? annot := Script_ir_annot.parse_type_annot loc annot in
      return? ((Ex_comparable_ty (Script_typed_ir.key_key annot)), ctxt)
    | Micheline.Prim loc Michelson_v1_primitives.T_timestamp [] annot =>
      let? annot := Script_ir_annot.parse_type_annot loc annot in
      return? ((Ex_comparable_ty (Script_typed_ir.timestamp_key annot)), ctxt)
    | Micheline.Prim loc Michelson_v1_primitives.T_chain_id [] annot =>
      let? annot := Script_ir_annot.parse_type_annot loc annot in
      return? ((Ex_comparable_ty (Script_typed_ir.chain_id_key annot)), ctxt)
    | Micheline.Prim loc Michelson_v1_primitives.T_address [] annot =>
      let? annot := Script_ir_annot.parse_type_annot loc annot in
      return? ((Ex_comparable_ty (Script_typed_ir.address_key annot)), ctxt)
    |
      Micheline.Prim loc
        ((Michelson_v1_primitives.T_unit | Michelson_v1_primitives.T_never |
        Michelson_v1_primitives.T_int | Michelson_v1_primitives.T_nat |
        Michelson_v1_primitives.T_string | Michelson_v1_primitives.T_bytes |
        Michelson_v1_primitives.T_mutez | Michelson_v1_primitives.T_bool |
        Michelson_v1_primitives.T_key_hash | Michelson_v1_primitives.T_timestamp
        | Michelson_v1_primitives.T_address | Michelson_v1_primitives.T_chain_id
        | Michelson_v1_primitives.T_signature | Michelson_v1_primitives.T_key)
          as prim) l_value _ =>
      Error_monad.error_value
        (Build_extensible "Invalid_arity"
          (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
          (loc, prim, 0, (List.length l_value)))
    |
      Micheline.Prim loc Michelson_v1_primitives.T_pair (cons _left _right)
        annot =>
      let? annot := Script_ir_annot.parse_type_annot loc annot in
      let? '(_left, left_annot) := Script_ir_annot.extract_field_annot _left in
      let? '(_right, right_annot) :=
        match _right with
        | cons _right [] => Script_ir_annot.extract_field_annot _right
        | _right =>
          return?
            ((Micheline.Prim loc Michelson_v1_primitives.T_pair _right nil),
              None)
        end in
      let? '(Ex_comparable_ty _right, ctxt) :=
        parse_comparable_ty (stack_depth +i 1) ctxt _right in
      let? '(Ex_comparable_ty _left, ctxt) :=
        parse_comparable_ty (stack_depth +i 1) ctxt _left in
      let? ty :=
        Script_typed_ir.pair_key loc (_left, left_annot) (_right, right_annot)
          annot in
      return? ((Ex_comparable_ty ty), ctxt)
    |
      Micheline.Prim loc Michelson_v1_primitives.T_or
        (cons _left (cons _right [])) annot =>
      let? annot := Script_ir_annot.parse_type_annot loc annot in
      let? '(_left, left_annot) := Script_ir_annot.extract_field_annot _left in
      let? '(_right, right_annot) := Script_ir_annot.extract_field_annot _right
        in
      let? '(Ex_comparable_ty _right, ctxt) :=
        parse_comparable_ty (stack_depth +i 1) ctxt _right in
      let? '(Ex_comparable_ty _left, ctxt) :=
        parse_comparable_ty (stack_depth +i 1) ctxt _left in
      let? ty :=
        Script_typed_ir.union_key loc (_left, left_annot) (_right, right_annot)
          annot in
      return? ((Ex_comparable_ty ty), ctxt)
    |
      Micheline.Prim loc
        ((Michelson_v1_primitives.T_pair | Michelson_v1_primitives.T_or) as prim)
        l_value _ =>
      Error_monad.error_value
        (Build_extensible "Invalid_arity"
          (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
          (loc, prim, 2, (List.length l_value)))
    |
      Micheline.Prim loc Michelson_v1_primitives.T_option (cons t_value [])
        annot =>
      let? annot := Script_ir_annot.parse_type_annot loc annot in
      let? '(Ex_comparable_ty t_value, ctxt) :=
        parse_comparable_ty (stack_depth +i 1) ctxt t_value in
      let? ty := Script_typed_ir.option_key loc t_value annot in
      return? ((Ex_comparable_ty ty), ctxt)
    | Micheline.Prim loc Michelson_v1_primitives.T_option l_value _ =>
      Error_monad.error_value
        (Build_extensible "Invalid_arity"
          (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
          (loc, Michelson_v1_primitives.T_option, 1, (List.length l_value)))
    |
      Micheline.Prim loc
        (Michelson_v1_primitives.T_set | Michelson_v1_primitives.T_map |
        Michelson_v1_primitives.T_list | Michelson_v1_primitives.T_lambda |
        Michelson_v1_primitives.T_contract | Michelson_v1_primitives.T_operation)
        _ _ =>
      Error_monad.error_value
        (Build_extensible "Comparable_type_expected"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim)
          (loc, (Micheline.strip_locations ty)))
    | expr =>
      Error_monad.error_value
        (unexpected expr nil Michelson_v1_primitives.Type_namespace
          [
            Michelson_v1_primitives.T_unit;
            Michelson_v1_primitives.T_never;
            Michelson_v1_primitives.T_int;
            Michelson_v1_primitives.T_nat;
            Michelson_v1_primitives.T_string;
            Michelson_v1_primitives.T_bytes;
            Michelson_v1_primitives.T_mutez;
            Michelson_v1_primitives.T_bool;
            Michelson_v1_primitives.T_key_hash;
            Michelson_v1_primitives.T_timestamp;
            Michelson_v1_primitives.T_address;
            Michelson_v1_primitives.T_pair;
            Michelson_v1_primitives.T_or;
            Michelson_v1_primitives.T_option;
            Michelson_v1_primitives.T_chain_id;
            Michelson_v1_primitives.T_signature;
            Michelson_v1_primitives.T_key
          ])
    end.

Inductive ex_ty : Set :=
| Ex_ty : Script_typed_ir.ty -> ex_ty.

Axiom parse_packable_ty :
  Alpha_context.context -> int -> bool -> Alpha_context.Script.node ->
  M? (ex_ty * Alpha_context.context).

Axiom parse_parameter_ty :
  Alpha_context.context -> int -> bool -> Alpha_context.Script.node ->
  M? (ex_ty * Alpha_context.context).

Axiom parse_normal_storage_ty :
  Alpha_context.context -> int -> bool -> Alpha_context.Script.node ->
  M? (ex_ty * Alpha_context.context).

Axiom parse_any_ty :
  Alpha_context.context -> int -> bool -> Alpha_context.Script.node ->
  M? (ex_ty * Alpha_context.context).

Axiom parse_ty :
  Alpha_context.context -> int -> bool -> bool -> bool -> bool -> bool ->
  Alpha_context.Script.node -> M? (ex_ty * Alpha_context.context).

Axiom parse_big_map_ty :
  Alpha_context.context -> int -> bool -> Alpha_context.Script.location ->
  list (Micheline.node Alpha_context.Script.location Alpha_context.Script.prim)
  -> Micheline.annot -> M? (ex_ty * Alpha_context.context).

Axiom parse_big_map_value_ty :
  Alpha_context.context -> int -> bool ->
  Micheline.node Alpha_context.Script.location Alpha_context.Script.prim ->
  M? (ex_ty * Alpha_context.context).

Fixpoint parse_view_input_ty
  (ctxt : Alpha_context.context) (stack_depth : int) (legacy : bool)
  : Alpha_context.Script.node -> M? (ex_ty * Alpha_context.context) :=
  parse_ty ctxt stack_depth legacy false false true false

with parse_view_output_ty
  (ctxt : Alpha_context.context) (stack_depth : int) (legacy : bool)
  : Alpha_context.Script.node -> M? (ex_ty * Alpha_context.context) :=
  parse_ty ctxt stack_depth legacy false false true false.

Definition parse_storage_ty
  (ctxt : Alpha_context.context) (stack_depth : int) (legacy : bool)
  (node_value : Alpha_context.Script.node)
  : M? (ex_ty * Alpha_context.context) :=
  match
    (node_value,
      match node_value with
      |
        Micheline.Prim loc Michelson_v1_primitives.T_pair
          (cons
            (Micheline.Prim big_map_loc Michelson_v1_primitives.T_big_map args
              map_annot) (cons remaining_storage [])) storage_annot => legacy
      | _ => false
      end) with
  |
    (Micheline.Prim loc Michelson_v1_primitives.T_pair
      (cons
        (Micheline.Prim big_map_loc Michelson_v1_primitives.T_big_map args
          map_annot) (cons remaining_storage [])) storage_annot, true) =>
    match
      (storage_annot,
        match storage_annot with
        | cons single [] =>
          ((String.length single) >i 0) &&
          (Compare.Char.(Compare.S.op_eq) (String.get single 0) "%" % char)
        | _ => false
        end) with
    | ([], _) => parse_normal_storage_ty ctxt stack_depth legacy node_value
    | (cons single [], true) =>
      parse_normal_storage_ty ctxt stack_depth legacy node_value
    | (_, _) =>
      let? ctxt :=
        Alpha_context.Gas.consume ctxt Typecheck_costs.parse_type_cycle in
      let? '(Ex_ty big_map_ty, ctxt) :=
        parse_big_map_ty ctxt (stack_depth +i 1) legacy big_map_loc args
          map_annot in
      let? '(Ex_ty remaining_storage, ctxt) :=
        parse_normal_storage_ty ctxt (stack_depth +i 1) legacy remaining_storage
        in
      let? '(annot, map_field, storage_field) :=
        Script_ir_annot.parse_composed_type_annot loc storage_annot in
      let? ty :=
        Script_typed_ir.pair_t loc (big_map_ty, map_field, None)
          (remaining_storage, storage_field, None) annot in
      return? ((Ex_ty ty), ctxt)
    end
  | (_, _) => parse_normal_storage_ty ctxt stack_depth legacy node_value
  end.

Definition check_packable
  (legacy : bool) (loc : Alpha_context.Script.location)
  (root : Script_typed_ir.ty) : M? unit :=
  let fix check (function_parameter : Script_typed_ir.ty) : M? unit :=
    match
      (function_parameter,
        match function_parameter with
        | Script_typed_ir.Contract_t _ _ => legacy
        | _ => false
        end) with
    | (Script_typed_ir.Big_map_t _ _ _, _) =>
      Error_monad.error_value
        (Build_extensible "Unexpected_lazy_storage"
          Alpha_context.Script.location loc)
    | (Script_typed_ir.Sapling_state_t _ _, _) =>
      Error_monad.error_value
        (Build_extensible "Unexpected_lazy_storage"
          Alpha_context.Script.location loc)
    | (Script_typed_ir.Operation_t _, _) =>
      Error_monad.error_value
        (Build_extensible "Unexpected_operation" Alpha_context.Script.location
          loc)
    | (Script_typed_ir.Unit_t _, _) => Error_monad.ok_unit
    | (Script_typed_ir.Int_t _, _) => Error_monad.ok_unit
    | (Script_typed_ir.Nat_t _, _) => Error_monad.ok_unit
    | (Script_typed_ir.Signature_t _, _) => Error_monad.ok_unit
    | (Script_typed_ir.String_t _, _) => Error_monad.ok_unit
    | (Script_typed_ir.Bytes_t _, _) => Error_monad.ok_unit
    | (Script_typed_ir.Mutez_t _, _) => Error_monad.ok_unit
    | (Script_typed_ir.Key_hash_t _, _) => Error_monad.ok_unit
    | (Script_typed_ir.Key_t _, _) => Error_monad.ok_unit
    | (Script_typed_ir.Timestamp_t _, _) => Error_monad.ok_unit
    | (Script_typed_ir.Address_t _, _) => Error_monad.ok_unit
    | (Script_typed_ir.Bool_t _, _) => Error_monad.ok_unit
    | (Script_typed_ir.Chain_id_t _, _) => Error_monad.ok_unit
    | (Script_typed_ir.Never_t _, _) => Error_monad.ok_unit
    | (Script_typed_ir.Set_t _ _, _) => Error_monad.ok_unit
    | (Script_typed_ir.Ticket_t _ _, _) =>
      Error_monad.error_value
        (Build_extensible "Unexpected_ticket" Alpha_context.Script.location loc)
    | (Script_typed_ir.Lambda_t _ _ _, _) => Error_monad.ok_unit
    | (Script_typed_ir.Bls12_381_g1_t _, _) => Error_monad.ok_unit
    | (Script_typed_ir.Bls12_381_g2_t _, _) => Error_monad.ok_unit
    | (Script_typed_ir.Bls12_381_fr_t _, _) => Error_monad.ok_unit
    | (Script_typed_ir.Pair_t (l_ty, _, _) (r_ty, _, _) _, _) =>
      let? '_ := check l_ty in
      check r_ty
    | (Script_typed_ir.Union_t (l_ty, _) (r_ty, _) _, _) =>
      let? '_ := check l_ty in
      check r_ty
    | (Script_typed_ir.Option_t v_ty _, _) => check v_ty
    | (Script_typed_ir.List_t elt_ty _, _) => check elt_ty
    | (Script_typed_ir.Map_t _ elt_ty _, _) => check elt_ty
    | (Script_typed_ir.Contract_t _ _, true) => Error_monad.ok_unit
    | (Script_typed_ir.Contract_t _ _, _) =>
      Error_monad.error_value
        (Build_extensible "Unexpected_contract" Alpha_context.Script.location
          loc)
    | (Script_typed_ir.Sapling_transaction_t _ _, _) => return? tt
    | (Script_typed_ir.Chest_key_t _, _) => Error_monad.ok_unit
    | (Script_typed_ir.Chest_t _, _) => Error_monad.ok_unit
    end in
  check root.

Module toplevel.
  Record record : Set := Build {
    code_field : Alpha_context.Script.node;
    arg_type : Alpha_context.Script.node;
    storage_type : Alpha_context.Script.node;
    views : Script_typed_ir.SMap.(Map.S.t) Script_typed_ir.view;
    root_name : option Script_typed_ir.field_annot }.
  Definition with_code_field code_field (r : record) :=
    Build code_field r.(arg_type) r.(storage_type) r.(views) r.(root_name).
  Definition with_arg_type arg_type (r : record) :=
    Build r.(code_field) arg_type r.(storage_type) r.(views) r.(root_name).
  Definition with_storage_type storage_type (r : record) :=
    Build r.(code_field) r.(arg_type) storage_type r.(views) r.(root_name).
  Definition with_views views (r : record) :=
    Build r.(code_field) r.(arg_type) r.(storage_type) views r.(root_name).
  Definition with_root_name root_name (r : record) :=
    Build r.(code_field) r.(arg_type) r.(storage_type) r.(views) root_name.
End toplevel.
Definition toplevel := toplevel.record.

Module code.
  Record record : Set := Build {
    code : Script_typed_ir.lambda;
    arg_type : Script_typed_ir.ty;
    storage_type : Script_typed_ir.ty;
    views : Script_typed_ir.SMap.(Map.S.t) Script_typed_ir.view;
    root_name : option Script_typed_ir.field_annot;
    code_size : Cache_memory_helpers.sint }.
  Definition with_code code (r : record) :=
    Build code r.(arg_type) r.(storage_type) r.(views) r.(root_name)
      r.(code_size).
  Definition with_arg_type arg_type (r : record) :=
    Build r.(code) arg_type r.(storage_type) r.(views) r.(root_name)
      r.(code_size).
  Definition with_storage_type storage_type (r : record) :=
    Build r.(code) r.(arg_type) storage_type r.(views) r.(root_name)
      r.(code_size).
  Definition with_views views (r : record) :=
    Build r.(code) r.(arg_type) r.(storage_type) views r.(root_name)
      r.(code_size).
  Definition with_root_name root_name (r : record) :=
    Build r.(code) r.(arg_type) r.(storage_type) r.(views) root_name
      r.(code_size).
  Definition with_code_size code_size (r : record) :=
    Build r.(code) r.(arg_type) r.(storage_type) r.(views) r.(root_name)
      code_size.
End code.
Definition code := code.record.

Inductive ex_script : Set :=
| Ex_script : forall {c : Set}, Script_typed_ir.script c -> ex_script.

Inductive ex_code : Set :=
| Ex_code : code -> ex_code.

Inductive ex_view (storage : Set) : Set :=
| Ex_view : Script_typed_ir.lambda -> ex_view storage.

Arguments Ex_view {_}.

Inductive dig_proof_argument : Set :=
| Dig_proof_argument :
  Script_typed_ir.stack_prefix_preservation_witness -> Script_typed_ir.ty ->
  option Script_typed_ir.var_annot -> Script_typed_ir.stack_ty ->
  dig_proof_argument.

Inductive dug_proof_argument : Set :=
| Dug_proof_argument :
  Script_typed_ir.stack_prefix_preservation_witness * unit *
    Script_typed_ir.stack_ty -> dug_proof_argument.

Inductive dipn_proof_argument : Set :=
| Dipn_proof_argument :
  Script_typed_ir.stack_prefix_preservation_witness -> Alpha_context.context ->
  descr -> Script_typed_ir.stack_ty -> dipn_proof_argument.

Inductive dropn_proof_argument : Set :=
| Dropn_proof_argument :
  Script_typed_ir.stack_prefix_preservation_witness ->
  Script_typed_ir.stack_ty -> dropn_proof_argument.

Inductive comb_proof_argument : Set :=
| Comb_proof_argument :
  Script_typed_ir.comb_gadt_witness -> Script_typed_ir.stack_ty ->
  comb_proof_argument.

Inductive uncomb_proof_argument : Set :=
| Uncomb_proof_argument :
  Script_typed_ir.uncomb_gadt_witness -> Script_typed_ir.stack_ty ->
  uncomb_proof_argument.

Inductive comb_get_proof_argument (before : Set) : Set :=
| Comb_get_proof_argument :
  Script_typed_ir.comb_get_gadt_witness -> Script_typed_ir.ty ->
  comb_get_proof_argument before.

Arguments Comb_get_proof_argument {_}.

Inductive comb_set_proof_argument (rest before : Set) : Set :=
| Comb_set_proof_argument :
  Script_typed_ir.comb_set_gadt_witness -> Script_typed_ir.ty ->
  comb_set_proof_argument rest before.

Arguments Comb_set_proof_argument {_ _}.

Inductive dup_n_proof_argument (before : Set) : Set :=
| Dup_n_proof_argument :
  Script_typed_ir.dup_n_gadt_witness -> Script_typed_ir.ty ->
  dup_n_proof_argument before.

Arguments Dup_n_proof_argument {_}.

Definition find_entrypoint
  (full : Script_typed_ir.ty) (root_name : option Script_typed_ir.field_annot)
  (entrypoint : string)
  : M? ((Alpha_context.Script.node -> Alpha_context.Script.node) * ex_ty) :=
  let annot_is_entrypoint
    (entrypoint : string)
    (function_parameter : option Script_typed_ir.field_annot) : bool :=
    match function_parameter with
    | None => false
    | Some (Script_typed_ir.Field_annot l_value) =>
      Compare.String.(Compare.S.op_eq) l_value entrypoint
    end in
  let fix find_entrypoint (t_value : Script_typed_ir.ty) (entrypoint : string)
    : option ((Alpha_context.Script.node -> Alpha_context.Script.node) * ex_ty) :=
    match t_value with
    | Script_typed_ir.Union_t (tl, al) (tr, ar) _ =>
      if annot_is_entrypoint entrypoint al then
        Some
          ((fun (e : Alpha_context.Script.node) =>
            Micheline.Prim 0 Michelson_v1_primitives.D_Left [ e ] nil),
            (Ex_ty tl))
      else
        if annot_is_entrypoint entrypoint ar then
          Some
            ((fun (e : Alpha_context.Script.node) =>
              Micheline.Prim 0 Michelson_v1_primitives.D_Right [ e ] nil),
              (Ex_ty tr))
        else
          match find_entrypoint tl entrypoint with
          | Some (f, t_value) =>
            Some
              ((fun (e : Alpha_context.Script.node) =>
                Micheline.Prim 0 Michelson_v1_primitives.D_Left [ f e ] nil),
                t_value)
          | None =>
            match find_entrypoint tr entrypoint with
            | Some (f, t_value) =>
              Some
                ((fun (e : Alpha_context.Script.node) =>
                  Micheline.Prim 0 Michelson_v1_primitives.D_Right [ f e ] nil),
                  t_value)
            | None => None
            end
          end
    | _ => None
    end in
  let entrypoint :=
    if Compare.String.(Compare.S.op_eq) entrypoint "" then
      "default"
    else
      entrypoint in
  if (String.length entrypoint) >i 31 then
    Error_monad.error_value
      (Build_extensible "Entrypoint_name_too_long" string entrypoint)
  else
    match
      (root_name,
        match root_name with
        | Some (Script_typed_ir.Field_annot root_name) =>
          Compare.String.(Compare.S.op_eq) entrypoint root_name
        | _ => false
        end) with
    | (Some (Script_typed_ir.Field_annot root_name), true) =>
      return? ((fun (e : Alpha_context.Script.node) => e), (Ex_ty full))
    | (_, _) =>
      match find_entrypoint full entrypoint with
      | Some result_value => return? result_value
      | None =>
        match entrypoint with
        | "default" =>
          return? ((fun (e : Alpha_context.Script.node) => e), (Ex_ty full))
        | _ =>
          Error_monad.error_value
            (Build_extensible "No_such_entrypoint" string entrypoint)
        end
      end
    end.

Definition find_entrypoint_for_type
  (legacy : bool) (merge_type_error_flag : merge_type_error_flag)
  (full : Script_typed_ir.ty) (expected : Script_typed_ir.ty)
  (root_name : option Script_typed_ir.field_annot) (entrypoint : string)
  (loc : Alpha_context.Script.location)
  : Gas_monad.(GAS_MONAD.t) (string * Script_typed_ir.ty) :=
  match find_entrypoint full root_name entrypoint with
  | (Pervasives.Error _) as err => Gas_monad.(GAS_MONAD.from_tzresult) err
  | Pervasives.Ok (_, Ex_ty ty) =>
    Gas_monad.(GAS_MONAD.op_gtquestionquestiondollar)
      (merge_types legacy merge_type_error_flag loc ty expected)
      (fun (eq_ty : M? (eq * Script_typed_ir.ty)) =>
        match (entrypoint, root_name) with
        | ("default", Some (Script_typed_ir.Field_annot "root")) =>
          match eq_ty with
          | Pervasives.Ok (Eq, ty) =>
            Gas_monad.(GAS_MONAD._return) ("default", ty)
          | Pervasives.Error _ =>
            Gas_monad.(GAS_MONAD.op_gtquestiondollar)
              (merge_types legacy merge_type_error_flag loc full expected)
              (fun (function_parameter : eq * Script_typed_ir.ty) =>
                let '(Eq, full) := function_parameter in
                return? ("root", full))
          end
        | _ =>
          Gas_monad.(GAS_MONAD.from_tzresult)
            (let? '(Eq, ty) := eq_ty in
            return? (entrypoint, ty))
        end)
  end.

Definition Entrypoints :=
  _Set.Make
    {|
      Compare.COMPARABLE.compare := String.compare
    |}.

Axiom well_formed_entrypoints :
  Script_typed_ir.ty -> option Script_typed_ir.field_annot -> M? unit.

Definition parse_uint (nb_bits : int)
  : Micheline.node Alpha_context.Script.location Alpha_context.Script.prim ->
  M? int :=
  let '_ :=
    (* ❌ Assert instruction is not handled. *)
    assert unit ((nb_bits >=i 0) && (nb_bits <=i 30)) in
  let max_int := (Pervasives.lsl 1 nb_bits) -i 1 in
  let max_z := Z.of_int max_int in
  fun (function_parameter :
    Micheline.node Alpha_context.Script.location Alpha_context.Script.prim) =>
    match
      (function_parameter,
        match function_parameter with
        | Micheline.Int _ n => (Z.zero <=Z n) && (n <=Z max_z)
        | _ => false
        end) with
    | (Micheline.Int _ n, true) => return? (Z.to_int n)
    | (node_value, _) =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          ((location node_value), (Micheline.strip_locations node_value),
            (Pervasives.op_caret "a positive "
              (Pervasives.op_caret (Pervasives.string_of_int nb_bits)
                (Pervasives.op_caret "-bit integer (between 0 and "
                  (Pervasives.op_caret (Pervasives.string_of_int max_int) ")"))))))
    end.

Definition parse_uint10
  : Micheline.node Alpha_context.Script.location Alpha_context.Script.prim ->
  M? int := parse_uint 10.

Definition parse_uint11
  : Micheline.node Alpha_context.Script.location Alpha_context.Script.prim ->
  M? int := parse_uint 11.

Definition opened_ticket_type
  (loc : Alpha_context.Script.location) (ty : Script_typed_ir.comparable_ty)
  : M? Script_typed_ir.comparable_ty :=
  Script_typed_ir.pair_3_key loc ((Script_typed_ir.address_key None), None)
    (ty, None) ((Script_typed_ir.nat_key None), None).

Definition parse_unit
  (ctxt : Alpha_context.context) (legacy : bool)
  (function_parameter :
    Micheline.node Alpha_context.Script.location Alpha_context.Script.prim)
  : M? (unit * Alpha_context.context) :=
  match function_parameter with
  | Micheline.Prim loc Michelson_v1_primitives.D_Unit [] annot =>
    let? '_ :=
      if legacy then
        Error_monad.ok_unit
      else
        Script_ir_annot.error_unexpected_annot loc annot in
    let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.unit_value in
    return? (tt, ctxt)
  | Micheline.Prim loc Michelson_v1_primitives.D_Unit l_value _ =>
    Error_monad.error_value
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, Michelson_v1_primitives.D_Unit, 0, (List.length l_value)))
  | expr =>
    Error_monad.error_value
      (unexpected expr nil Michelson_v1_primitives.Constant_namespace
        [ Michelson_v1_primitives.D_Unit ])
  end.

Definition parse_bool
  (ctxt : Alpha_context.context) (legacy : bool)
  (function_parameter :
    Micheline.node Alpha_context.Script.location Alpha_context.Script.prim)
  : M? (bool * Alpha_context.context) :=
  match function_parameter with
  | Micheline.Prim loc Michelson_v1_primitives.D_True [] annot =>
    let? '_ :=
      if legacy then
        Error_monad.ok_unit
      else
        Script_ir_annot.error_unexpected_annot loc annot in
    let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.bool_value in
    return? (true, ctxt)
  | Micheline.Prim loc Michelson_v1_primitives.D_False [] annot =>
    let? '_ :=
      if legacy then
        Error_monad.ok_unit
      else
        Script_ir_annot.error_unexpected_annot loc annot in
    let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.bool_value in
    return? (false, ctxt)
  |
    Micheline.Prim loc
      ((Michelson_v1_primitives.D_True | Michelson_v1_primitives.D_False) as c)
      l_value _ =>
    Error_monad.error_value
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, c, 0, (List.length l_value)))
  | expr =>
    Error_monad.error_value
      (unexpected expr nil Michelson_v1_primitives.Constant_namespace
        [ Michelson_v1_primitives.D_True; Michelson_v1_primitives.D_False ])
  end.

Definition parse_string
  (ctxt : Alpha_context.context)
  (function_parameter : Alpha_context.Script.node)
  : M? (Alpha_context.Script_string.t * Alpha_context.context) :=
  match function_parameter with
  | (Micheline.String loc v) as expr =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt (Typecheck_costs.check_printable v) in
    Error_monad.record_trace
      (Build_extensible "Invalid_syntactic_constant"
        (Alpha_context.Script.location *
          Micheline.canonical Alpha_context.Script.prim * string)
        (loc, (Micheline.strip_locations expr), "a printable ascii string"))
      (let? s := Alpha_context.Script_string.of_string v in
      return? (s, ctxt))
  | expr =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr), [ Script_tc_errors.String_kind ], (kind_value expr)))
  end.

Definition parse_bytes {A B : Set}
  (ctxt : A)
  (function_parameter : Micheline.node Alpha_context.Script.location B)
  : M? (bytes * A) :=
  match function_parameter with
  | Micheline.Bytes _ v => return? (v, ctxt)
  | expr =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr), [ Script_tc_errors.Bytes_kind ], (kind_value expr)))
  end.

Definition parse_int {A B : Set}
  (ctxt : A)
  (function_parameter : Micheline.node Alpha_context.Script.location B)
  : M? (Alpha_context.Script_int.num * A) :=
  match function_parameter with
  | Micheline.Int _ v => return? ((Alpha_context.Script_int.of_zint v), ctxt)
  | expr =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr), [ Script_tc_errors.Int_kind ], (kind_value expr)))
  end.

Definition parse_nat
  (ctxt : Alpha_context.context)
  (function_parameter : Alpha_context.Script.node)
  : M? (Alpha_context.Script_int.num * Alpha_context.context) :=
  match function_parameter with
  | (Micheline.Int loc v) as expr =>
    let v := Alpha_context.Script_int.of_zint v in
    match Alpha_context.Script_int.is_nat v with
    | Some nat => return? (nat, ctxt)
    | None =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a non-negative integer"))
    end
  | expr =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr), [ Script_tc_errors.Int_kind ], (kind_value expr)))
  end.

Definition parse_mutez
  (ctxt : Alpha_context.context)
  (function_parameter : Alpha_context.Script.node)
  : M? (Alpha_context.Tez.t * Alpha_context.context) :=
  match function_parameter with
  | (Micheline.Int loc v) as expr =>
    match
      Option.bind
        (Option.catch None
          (fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Z.to_int64 v)) Alpha_context.Tez.of_mutez with
    | Some tez => Pervasives.Ok (tez, ctxt)
    | None =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a valid mutez amount"))
    end
  | expr =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr), [ Script_tc_errors.Int_kind ], (kind_value expr)))
  end.

Definition parse_timestamp
  (ctxt : Alpha_context.context)
  (function_parameter : Alpha_context.Script.node)
  : M? (Alpha_context.Script_timestamp.t * Alpha_context.context) :=
  match function_parameter with
  | Micheline.Int _ v =>
    return? ((Alpha_context.Script_timestamp.of_zint v), ctxt)
  | (Micheline.String loc s) as expr =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Typecheck_costs.timestamp_readable in
    match Alpha_context.Script_timestamp.of_string s with
    | Some v => return? (v, ctxt)
    | None =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a valid timestamp"))
    end
  | expr =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr),
          [ Script_tc_errors.String_kind; Script_tc_errors.Int_kind ],
          (kind_value expr)))
  end.

Definition parse_key
  (ctxt : Alpha_context.context)
  (function_parameter : Alpha_context.Script.node)
  : M? (Alpha_context.public_key * Alpha_context.context) :=
  match function_parameter with
  | (Micheline.Bytes loc bytes_value) as expr =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Typecheck_costs.public_key_optimized in
    match
      Data_encoding.Binary.of_bytes_opt
        Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.encoding) bytes_value with
    | Some k => return? (k, ctxt)
    | None =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a valid public key"))
    end
  | (Micheline.String loc s) as expr =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Typecheck_costs.public_key_readable in
    match Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.of_b58check_opt) s with
    | Some k => return? (k, ctxt)
    | None =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a valid public key"))
    end
  | expr =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr),
          [ Script_tc_errors.String_kind; Script_tc_errors.Bytes_kind ],
          (kind_value expr)))
  end.

Definition parse_key_hash
  (ctxt : Alpha_context.context)
  (function_parameter : Alpha_context.Script.node)
  : M? (Alpha_context.public_key_hash * Alpha_context.context) :=
  match function_parameter with
  | (Micheline.Bytes loc bytes_value) as expr =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Typecheck_costs.key_hash_optimized in
    match
      Data_encoding.Binary.of_bytes_opt
        Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)
        bytes_value with
    | Some k => return? (k, ctxt)
    | None =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a valid key hash"))
    end
  | (Micheline.String loc s) as expr =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Typecheck_costs.key_hash_readable in
    match
      Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.of_b58check_opt) s
      with
    | Some k => return? (k, ctxt)
    | None =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a valid key hash"))
    end
  | expr =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr),
          [ Script_tc_errors.String_kind; Script_tc_errors.Bytes_kind ],
          (kind_value expr)))
  end.

Definition parse_signature
  (ctxt : Alpha_context.context)
  (function_parameter : Alpha_context.Script.node)
  : M? (Alpha_context.signature * Alpha_context.context) :=
  match function_parameter with
  | (Micheline.Bytes loc bytes_value) as expr =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Typecheck_costs.signature_optimized in
    match Data_encoding.Binary.of_bytes_opt Signature.encoding bytes_value with
    | Some k => return? (k, ctxt)
    | None =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a valid signature"))
    end
  | (Micheline.String loc s) as expr =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Typecheck_costs.signature_readable in
    match Signature.of_b58check_opt s with
    | Some s => return? (s, ctxt)
    | None =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a valid signature"))
    end
  | expr =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr),
          [ Script_tc_errors.String_kind; Script_tc_errors.Bytes_kind ],
          (kind_value expr)))
  end.

Definition parse_chain_id
  (ctxt : Alpha_context.context)
  (function_parameter : Alpha_context.Script.node)
  : M? (Chain_id.t * Alpha_context.context) :=
  match function_parameter with
  | (Micheline.Bytes loc bytes_value) as expr =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Typecheck_costs.chain_id_optimized in
    match Data_encoding.Binary.of_bytes_opt Chain_id.encoding bytes_value with
    | Some k => return? (k, ctxt)
    | None =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a valid chain id"))
    end
  | (Micheline.String loc s) as expr =>
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Typecheck_costs.chain_id_readable in
    match Chain_id.of_b58check_opt s with
    | Some s => return? (s, ctxt)
    | None =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a valid chain id"))
    end
  | expr =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr),
          [ Script_tc_errors.String_kind; Script_tc_errors.Bytes_kind ],
          (kind_value expr)))
  end.

Definition parse_address
  (ctxt : Alpha_context.context)
  (function_parameter : Alpha_context.Script.node)
  : M? (Script_typed_ir.address * Alpha_context.context) :=
  match function_parameter with
  | (Micheline.Bytes loc bytes_value) as expr =>
    let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.contract in
    match
      Data_encoding.Binary.of_bytes_opt
        (Data_encoding.tup2 Alpha_context.Contract.encoding
          Data_encoding._Variable.string_value) bytes_value with
    | Some (c, entrypoint) =>
      if (String.length entrypoint) >i 31 then
        Error_monad.error_value
          (Build_extensible "Entrypoint_name_too_long" string entrypoint)
      else
        match entrypoint with
        | "" => return? ((c, "default"), ctxt)
        | "default" =>
          Error_monad.error_value
            (Build_extensible "Unexpected_annotation"
              Alpha_context.Script.location loc)
        | name => return? ((c, name), ctxt)
        end
    | None =>
      Error_monad.error_value
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr), "a valid address"))
    end
  | Micheline.String loc s =>
    let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.contract in
    let? '(addr, entrypoint) :=
      match String.index_opt s "%" % char with
      | None => return? (s, "default")
      | Some pos =>
        let len := ((String.length s) -i pos) -i 1 in
        let name := String.sub s (pos +i 1) len in
        if len >i 31 then
          Error_monad.error_value
            (Build_extensible "Entrypoint_name_too_long" string name)
        else
          match ((String.sub s 0 pos), name) with
          | (addr, "") => return? (addr, "default")
          | (_, "default") =>
            Error_monad.error_value
              (Build_extensible "Unexpected_annotation"
                Alpha_context.Script.location loc)
          | addr_and_name => return? addr_and_name
          end
      end in
    let? c := Alpha_context.Contract.of_b58check addr in
    return? ((c, entrypoint), ctxt)
  | expr =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr),
          [ Script_tc_errors.String_kind; Script_tc_errors.Bytes_kind ],
          (kind_value expr)))
  end.

Definition parse_never {A : Set}
  (expr : Micheline.node Alpha_context.Script.location A)
  : M? (Script_typed_ir.never * Alpha_context.context) :=
  Error_monad.error_value
    (Build_extensible "Invalid_never_expr" Alpha_context.Script.location
      (location expr)).

Definition parse_pair {A B C D E : Set}
  (parse_l :
    A ->
    Micheline.node Alpha_context.Script.location Alpha_context.Script.prim ->
    M=? (B * C))
  (parse_r :
    C ->
    Micheline.node Alpha_context.Script.location Alpha_context.Script.prim ->
    M=? (D * E)) (ctxt : A) (legacy : bool) (r_comb_witness : comb_witness)
  (expr : Micheline.node Alpha_context.Script.location Alpha_context.Script.prim)
  : M=? ((B * D) * E) :=
  let parse_comb
    (loc : Alpha_context.Script.location)
    (l_value :
      Micheline.node Alpha_context.Script.location Alpha_context.Script.prim)
    (rs :
      list
        (Micheline.node Alpha_context.Script.location Alpha_context.Script.prim))
    : M=? ((B * D) * E) :=
    let=? '(l_value, ctxt) := parse_l ctxt l_value in
    let=? r_value :=
      match (rs, r_comb_witness) with
      | (cons r_value [], _) => return=? r_value
      | ([], _) =>
        return=
          (Error_monad.error_value
            (Build_extensible "Invalid_arity"
              (Alpha_context.Script.location * Alpha_context.Script.prim * int *
                int) (loc, Michelson_v1_primitives.D_Pair, 2, 1)))
      | (cons _ _, Comb_Pair _) =>
        return=? (Micheline.Prim loc Michelson_v1_primitives.D_Pair rs nil)
      | _ =>
        return=
          (Error_monad.error_value
            (Build_extensible "Invalid_arity"
              (Alpha_context.Script.location * Alpha_context.Script.prim * int *
                int)
              (loc, Michelson_v1_primitives.D_Pair, 2, (1 +i (List.length rs)))))
      end in
    let=? '(r_value, ctxt) := parse_r ctxt r_value in
    return=? ((l_value, r_value), ctxt) in
  match expr with
  | Micheline.Prim loc Michelson_v1_primitives.D_Pair (cons l_value rs) annot =>
    let=? '_ :=
      return=
        (if legacy then
          Error_monad.ok_unit
        else
          Script_ir_annot.error_unexpected_annot loc annot) in
    parse_comb loc l_value rs
  | Micheline.Prim loc Michelson_v1_primitives.D_Pair l_value _ =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, Michelson_v1_primitives.D_Pair, 2, (List.length l_value)))
  | Micheline.Seq loc (cons l_value ((cons _ _) as rs)) =>
    parse_comb loc l_value rs
  | Micheline.Seq loc l_value =>
    Error_monad.fail
      (Build_extensible "Invalid_seq_arity"
        (Alpha_context.Script.location * int * int)
        (loc, 2, (List.length l_value)))
  | expr =>
    Error_monad.fail
      (unexpected expr nil Michelson_v1_primitives.Constant_namespace
        [ Michelson_v1_primitives.D_Pair ])
  end.

Definition parse_union {A B C D : Set}
  (parse_l :
    A ->
    Micheline.node Alpha_context.Script.location Alpha_context.Script.prim ->
    M=? (B * C))
  (parse_r :
    A ->
    Micheline.node Alpha_context.Script.location Alpha_context.Script.prim ->
    M=? (D * C)) (ctxt : A) (legacy : bool)
  (function_parameter :
    Micheline.node Alpha_context.Script.location Alpha_context.Script.prim)
  : M=? (Script_typed_ir.union B D * C) :=
  match function_parameter with
  | Micheline.Prim loc Michelson_v1_primitives.D_Left (cons v []) annot =>
    let=? '_ :=
      return=
        (if legacy then
          Error_monad.ok_unit
        else
          Script_ir_annot.error_unexpected_annot loc annot) in
    let=? '(v, ctxt) := parse_l ctxt v in
    return=? ((Script_typed_ir.L v), ctxt)
  | Micheline.Prim loc Michelson_v1_primitives.D_Left l_value _ =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, Michelson_v1_primitives.D_Left, 1, (List.length l_value)))
  | Micheline.Prim loc Michelson_v1_primitives.D_Right (cons v []) annot =>
    let=? '_ :=
      return=
        (if legacy then
          Error_monad.ok_unit
        else
          Script_ir_annot.error_unexpected_annot loc annot) in
    let=? '(v, ctxt) := parse_r ctxt v in
    return=? ((Script_typed_ir.R v), ctxt)
  | Micheline.Prim loc Michelson_v1_primitives.D_Right l_value _ =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, Michelson_v1_primitives.D_Right, 1, (List.length l_value)))
  | expr =>
    Error_monad.fail
      (unexpected expr nil Michelson_v1_primitives.Constant_namespace
        [ Michelson_v1_primitives.D_Left; Michelson_v1_primitives.D_Right ])
  end.

Definition parse_option {A B : Set}
  (parse_v :
    A ->
    Micheline.node Alpha_context.Script.location Alpha_context.Script.prim ->
    M=? (B * A)) (ctxt : A) (legacy : bool)
  (function_parameter :
    Micheline.node Alpha_context.Script.location Alpha_context.Script.prim)
  : M=? (option B * A) :=
  match function_parameter with
  | Micheline.Prim loc Michelson_v1_primitives.D_Some (cons v []) annot =>
    let=? '_ :=
      return=
        (if legacy then
          Error_monad.ok_unit
        else
          Script_ir_annot.error_unexpected_annot loc annot) in
    let=? '(v, ctxt) := parse_v ctxt v in
    return=? ((Some v), ctxt)
  | Micheline.Prim loc Michelson_v1_primitives.D_Some l_value _ =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, Michelson_v1_primitives.D_Some, 1, (List.length l_value)))
  | Micheline.Prim loc Michelson_v1_primitives.D_None [] annot =>
    return=
      (let? '_ :=
        if legacy then
          Error_monad.ok_unit
        else
          Script_ir_annot.error_unexpected_annot loc annot in
      return? (None, ctxt))
  | Micheline.Prim loc Michelson_v1_primitives.D_None l_value _ =>
    Error_monad.fail
      (Build_extensible "Invalid_arity"
        (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
        (loc, Michelson_v1_primitives.D_None, 0, (List.length l_value)))
  | expr =>
    Error_monad.fail
      (unexpected expr nil Michelson_v1_primitives.Constant_namespace
        [ Michelson_v1_primitives.D_Some; Michelson_v1_primitives.D_None ])
  end.

Definition comparable_comb_witness1
  (function_parameter : Script_typed_ir.comparable_ty) : comb_witness :=
  match function_parameter with
  | Script_typed_ir.Pair_key _ _ _ => Comb_Pair Comb_Any
  | _ => Comb_Any
  end.

Axiom parse_comparable_data : forall {a : Set},
  option type_logger -> Alpha_context.context ->
  Script_typed_ir.comparable_ty -> Alpha_context.Script.node ->
  M=? (a * Alpha_context.context).

Definition comb_witness1 (function_parameter : Script_typed_ir.ty)
  : comb_witness :=
  match function_parameter with
  | Script_typed_ir.Pair_t _ _ _ => Comb_Pair Comb_Any
  | _ => Comb_Any
  end.

Axiom parse_data : forall {a : Set},
  option type_logger -> int -> Alpha_context.context -> bool -> bool ->
  Script_typed_ir.ty -> Alpha_context.Script.node ->
  M=? (a * Alpha_context.context).

Axiom parse_returning :
  option type_logger -> int -> tc_context -> Alpha_context.context -> bool ->
  Script_typed_ir.ty * option Script_typed_ir.var_annot -> Script_typed_ir.ty ->
  Alpha_context.Script.node ->
  M=? (Script_typed_ir.lambda * Alpha_context.context).

Axiom parse_instr : forall {a s : Set},
  option type_logger -> int -> tc_context -> Alpha_context.context -> bool ->
  Alpha_context.Script.node -> Script_typed_ir.stack_ty ->
  M=? (judgement a s * Alpha_context.context).

Axiom parse_contract :
  int -> bool -> Alpha_context.context -> Alpha_context.Script.location ->
  Script_typed_ir.ty -> Alpha_context.Contract.t -> string ->
  M=? (Alpha_context.context * Script_typed_ir.typed_contract).

Fixpoint parse_view_returning {storage : Set}
  (type_logger_value : option type_logger) (ctxt : Alpha_context.context)
  (legacy : bool) (storage_type : Script_typed_ir.ty)
  (function_parameter : Script_typed_ir.view)
  : M=? (ex_view storage * Alpha_context.context) :=
  let '{|
    Script_typed_ir.view.input_ty := input_ty;
      Script_typed_ir.view.output_ty := output_ty;
      Script_typed_ir.view.view_code := view_code
      |} := function_parameter in
  let input_ty_loc := location input_ty in
  let=? '(Ex_ty input_ty', ctxt) :=
    return=
      (Error_monad.record_trace_eval
        (fun (function_parameter : unit) =>
          let '_ := function_parameter in
          return?
            (Build_extensible "Ill_formed_type"
              (option string * Micheline.canonical Alpha_context.Script.prim *
                Alpha_context.Script.location)
              ((Some "arg of view"), (Micheline.strip_locations input_ty),
                input_ty_loc))) (parse_view_input_ty ctxt 0 legacy input_ty)) in
  let output_ty_loc := location output_ty in
  let=? '(Ex_ty output_ty', ctxt) :=
    return=
      (Error_monad.record_trace_eval
        (fun (function_parameter : unit) =>
          let '_ := function_parameter in
          return?
            (Build_extensible "Ill_formed_type"
              (option string * Micheline.canonical Alpha_context.Script.prim *
                Alpha_context.Script.location)
              ((Some "return of view"), (Micheline.strip_locations output_ty),
                output_ty_loc))) (parse_view_output_ty ctxt 0 legacy output_ty))
    in
  let=? pair_ty :=
    return=
      (Script_typed_ir.pair_t input_ty_loc (input_ty', None, None)
        (storage_type, None, None) None) in
  let=? '(judgement_value, ctxt) :=
    parse_instr type_logger_value 0 Lambda ctxt legacy view_code
      (Script_typed_ir.Item_t pair_ty Script_typed_ir.Bot_t None) in
  match judgement_value with
  | Failed {| judgement.Failed.descr := descr_value |} =>
    let cur_view' :=
      Ex_view
        (Script_typed_ir.Lam
          (close_descr
            (descr_value
              (Script_typed_ir.Item_t output_ty' Script_typed_ir.Bot_t None)))
          view_code) in
    return=? (cur_view', ctxt)
  | Typed ({| descr.loc := loc; descr.aft := aft |} as descr_value) =>
    return=
      (let ill_type_view {B : Set}
        (loc : Alpha_context.Script.location)
        (stack_ty : Script_typed_ir.stack_ty) : M? B :=
        let? '(actual, ctxt) := serialize_stack_for_error ctxt stack_ty in
        let expected_stack :=
          Script_typed_ir.Item_t output_ty' Script_typed_ir.Bot_t None in
        let? '(expected, _ctxt) := serialize_stack_for_error ctxt expected_stack
          in
        Error_monad.error_value
          (Build_extensible "Ill_typed_view" Script_tc_errors.Ill_typed_view
            {| Script_tc_errors.Ill_typed_view.loc := loc;
              Script_tc_errors.Ill_typed_view.actual := actual;
              Script_tc_errors.Ill_typed_view.expected := expected |}) in
      match aft with
      | Script_typed_ir.Item_t ty Script_typed_ir.Bot_t _ =>
        Error_monad.record_trace_eval
          (fun (function_parameter : unit) =>
            let '_ := function_parameter in
            ill_type_view loc aft)
          (let? '(Eq, ctxt) := ty_eq legacy ctxt loc ty output_ty' in
          let view' :=
            Ex_view (Script_typed_ir.Lam (close_descr descr_value) view_code) in
          return? (view', ctxt))
      | _ => ill_type_view loc aft
      end)
  end

with typecheck_views
  (type_logger_value : option type_logger) (ctxt : Alpha_context.context)
  (legacy : bool) (storage_type : Script_typed_ir.ty)
  (views : Script_typed_ir.SMap.(Map.S.t) Script_typed_ir.view)
  : M=? Alpha_context.context :=
  let aux {A : Set}
    (_name : A) (cur_view : Script_typed_ir.view) (ctxt : Alpha_context.context)
    : M=? Alpha_context.context :=
    let=? '(_parsed_view, ctxt) :=
      parse_view_returning type_logger_value ctxt legacy storage_type cur_view
      in
    return=? ctxt in
  Script_typed_ir.SMap.(Map.S.fold_es) aux views ctxt

with parse_view_name
  (ctxt : Alpha_context.context)
  (function_parameter :
    Micheline.node Alpha_context.Script.location Alpha_context.Script.prim)
  : M? (Alpha_context.Script_string.t * Alpha_context.context) :=
  match function_parameter with
  | (Micheline.String loc v) as expr =>
    if (String.length v) >i 31 then
      Error_monad.error_value (Build_extensible "View_name_too_long" string v)
    else
      let fix check_char (i : int) : M? string :=
        if i <i 0 then
          return? v
        else
          if Script_ir_annot.is_allowed_char (String.get v i) then
            check_char (i -i 1)
          else
            Error_monad.error_value
              (Build_extensible "Bad_view_name" Alpha_context.Script.location
                loc) in
      let? ctxt :=
        Alpha_context.Gas.consume ctxt (Typecheck_costs.check_printable v) in
      Error_monad.record_trace
        (Build_extensible "Invalid_syntactic_constant"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim * string)
          (loc, (Micheline.strip_locations expr),
            "string [a-zA-Z0-9_.%@] and the maximum string length of 31 characters"))
        (let? v := check_char ((String.length v) -i 1) in
        let? s := Alpha_context.Script_string.of_string v in
        return? (s, ctxt))
  | expr =>
    Error_monad.error_value
      (Build_extensible "Invalid_kind"
        (Alpha_context.Script.location * list Script_tc_errors.kind *
          Script_tc_errors.kind)
        ((location expr), [ Script_tc_errors.String_kind ], (kind_value expr)))
  end

with parse_toplevel
  (ctxt : Alpha_context.context) (legacy : bool)
  (toplevel_value : Alpha_context.Script.expr)
  : M? (toplevel * Alpha_context.context) :=
  Error_monad.record_trace
    (Build_extensible "Ill_typed_contract"
      (Alpha_context.Script.expr * Script_tc_errors.type_map)
      (toplevel_value, nil))
    match Micheline.root toplevel_value with
    | Micheline.Int loc _ =>
      Error_monad.error_value
        (Build_extensible "Invalid_kind"
          (Micheline.canonical_location * list Script_tc_errors.kind *
            Script_tc_errors.kind)
          (loc, [ Script_tc_errors.Seq_kind ], Script_tc_errors.Int_kind))
    | Micheline.String loc _ =>
      Error_monad.error_value
        (Build_extensible "Invalid_kind"
          (Micheline.canonical_location * list Script_tc_errors.kind *
            Script_tc_errors.kind)
          (loc, [ Script_tc_errors.Seq_kind ], Script_tc_errors.String_kind))
    | Micheline.Bytes loc _ =>
      Error_monad.error_value
        (Build_extensible "Invalid_kind"
          (Micheline.canonical_location * list Script_tc_errors.kind *
            Script_tc_errors.kind)
          (loc, [ Script_tc_errors.Seq_kind ], Script_tc_errors.Bytes_kind))
    | Micheline.Prim loc _ _ _ =>
      Error_monad.error_value
        (Build_extensible "Invalid_kind"
          (Micheline.canonical_location * list Script_tc_errors.kind *
            Script_tc_errors.kind)
          (loc, [ Script_tc_errors.Seq_kind ], Script_tc_errors.Prim_kind))
    | Micheline.Seq _ fields =>
      let fix find_fields
        (ctxt : Alpha_context.context)
        (p_value :
          option
            (Micheline.node Alpha_context.Script.location
              Alpha_context.Script.prim * Alpha_context.Script.location *
              Micheline.annot))
        (s :
          option
            (Micheline.node Alpha_context.Script.location
              Alpha_context.Script.prim * Alpha_context.Script.location *
              Micheline.annot))
        (c :
          option
            (Micheline.node Alpha_context.Script.location
              Alpha_context.Script.prim * Alpha_context.Script.location *
              Micheline.annot))
        (views : Script_typed_ir.SMap.(Map.S.t) Script_typed_ir.view)
        (fields :
          list
            (Micheline.node Alpha_context.Script.location
              Alpha_context.Script.prim))
        : M?
          (Alpha_context.context *
            (option
              (Micheline.node Alpha_context.Script.location
                Alpha_context.Script.prim * Alpha_context.Script.location *
                Micheline.annot) *
              option
                (Micheline.node Alpha_context.Script.location
                  Alpha_context.Script.prim * Alpha_context.Script.location *
                  Micheline.annot) *
              option
                (Micheline.node Alpha_context.Script.location
                  Alpha_context.Script.prim * Alpha_context.Script.location *
                  Micheline.annot) *
              Script_typed_ir.SMap.(Map.S.t) Script_typed_ir.view)) :=
        match fields with
        | [] => return? (ctxt, (p_value, s, c, views))
        | cons (Micheline.Int loc _) _ =>
          Error_monad.error_value
            (Build_extensible "Invalid_kind"
              (Alpha_context.Script.location * list Script_tc_errors.kind *
                Script_tc_errors.kind)
              (loc, [ Script_tc_errors.Prim_kind ], Script_tc_errors.Int_kind))
        | cons (Micheline.String loc _) _ =>
          Error_monad.error_value
            (Build_extensible "Invalid_kind"
              (Alpha_context.Script.location * list Script_tc_errors.kind *
                Script_tc_errors.kind)
              (loc, [ Script_tc_errors.Prim_kind ], Script_tc_errors.String_kind))
        | cons (Micheline.Bytes loc _) _ =>
          Error_monad.error_value
            (Build_extensible "Invalid_kind"
              (Alpha_context.Script.location * list Script_tc_errors.kind *
                Script_tc_errors.kind)
              (loc, [ Script_tc_errors.Prim_kind ], Script_tc_errors.Bytes_kind))
        | cons (Micheline.Seq loc _) _ =>
          Error_monad.error_value
            (Build_extensible "Invalid_kind"
              (Alpha_context.Script.location * list Script_tc_errors.kind *
                Script_tc_errors.kind)
              (loc, [ Script_tc_errors.Prim_kind ], Script_tc_errors.Seq_kind))
        |
          cons
            (Micheline.Prim loc Michelson_v1_primitives.K_parameter
              (cons arg []) annot) rest =>
          match p_value with
          | None => find_fields ctxt (Some (arg, loc, annot)) s c views rest
          | Some _ =>
            Error_monad.error_value
              (Build_extensible "Duplicate_field"
                (Alpha_context.Script.location * Alpha_context.Script.prim)
                (loc, Michelson_v1_primitives.K_parameter))
          end
        |
          cons
            (Micheline.Prim loc Michelson_v1_primitives.K_storage (cons arg [])
              annot) rest =>
          match s with
          | None =>
            find_fields ctxt p_value (Some (arg, loc, annot)) c views rest
          | Some _ =>
            Error_monad.error_value
              (Build_extensible "Duplicate_field"
                (Alpha_context.Script.location * Alpha_context.Script.prim)
                (loc, Michelson_v1_primitives.K_storage))
          end
        |
          cons
            (Micheline.Prim loc Michelson_v1_primitives.K_code (cons arg [])
              annot) rest =>
          match c with
          | None =>
            find_fields ctxt p_value s (Some (arg, loc, annot)) views rest
          | Some _ =>
            Error_monad.error_value
              (Build_extensible "Duplicate_field"
                (Alpha_context.Script.location * Alpha_context.Script.prim)
                (loc, Michelson_v1_primitives.K_code))
          end
        |
          cons
            (Micheline.Prim loc
              ((Michelson_v1_primitives.K_parameter |
              Michelson_v1_primitives.K_storage | Michelson_v1_primitives.K_code)
                as name) args _) _ =>
          Error_monad.error_value
            (Build_extensible "Invalid_arity"
              (Alpha_context.Script.location * Alpha_context.Script.prim * int *
                int) (loc, name, 1, (List.length args)))
        |
          cons
            (Micheline.Prim loc Michelson_v1_primitives.K_view
              (cons name (cons input_ty (cons output_ty (cons view_code [])))) _)
            rest =>
          let? '(str, ctxt) := parse_view_name ctxt name in
          let? ctxt :=
            Alpha_context.Gas.consume ctxt
              (Michelson_v1_gas.Cost_of.Interpreter.view_update str views) in
          if Script_typed_ir.SMap.(Map.S.mem) str views then
            Error_monad.error_value
              (Build_extensible "Duplicated_view_name"
                Alpha_context.Script.location loc)
          else
            let views' :=
              Script_typed_ir.SMap.(Map.S.add) str
                {| Script_typed_ir.view.input_ty := input_ty;
                  Script_typed_ir.view.output_ty := output_ty;
                  Script_typed_ir.view.view_code := view_code |} views in
            find_fields ctxt p_value s c views' rest
        | cons (Micheline.Prim loc Michelson_v1_primitives.K_view args _) _ =>
          Error_monad.error_value
            (Build_extensible "Invalid_arity"
              (Alpha_context.Script.location * Alpha_context.Script.prim * int *
                int)
              (loc, Michelson_v1_primitives.K_view, 4, (List.length args)))
        | cons (Micheline.Prim loc name _ _) _ =>
          let allowed :=
            [
              Michelson_v1_primitives.K_parameter;
              Michelson_v1_primitives.K_storage;
              Michelson_v1_primitives.K_code;
              Michelson_v1_primitives.K_view
            ] in
          Error_monad.error_value
            (Build_extensible "Invalid_primitive"
              (Alpha_context.Script.location * list Alpha_context.Script.prim *
                Alpha_context.Script.prim) (loc, allowed, name))
        end in
      let? '(ctxt, toplevel_value) :=
        find_fields ctxt None None None Script_typed_ir.SMap.(Map.S.empty)
          fields in
      match toplevel_value with
      | (None, _, _, _) =>
        Error_monad.error_value
          (Build_extensible "Missing_field" Alpha_context.Script.prim
            Michelson_v1_primitives.K_parameter)
      | (Some _, None, _, _) =>
        Error_monad.error_value
          (Build_extensible "Missing_field" Alpha_context.Script.prim
            Michelson_v1_primitives.K_storage)
      | (Some _, Some _, None, _) =>
        Error_monad.error_value
          (Build_extensible "Missing_field" Alpha_context.Script.prim
            Michelson_v1_primitives.K_code)
      |
        (Some (p_value, ploc, pannot), Some (s, sloc, sannot),
          Some (c, cloc, carrot), views) =>
        let maybe_root_name :=
          let? '(p_value, root_name) :=
            Script_ir_annot.extract_field_annot p_value in
          match root_name with
          | Some _ => return? (p_value, pannot, root_name)
          | None =>
            match
              (pannot,
                match pannot with
                | cons single [] =>
                  ((String.length single) >i 0) &&
                  (Compare.Char.(Compare.S.op_eq) (String.get single 0)
                    "%" % char)
                | _ => false
                end) with
            | (cons single [], true) =>
              let? pannot := Script_ir_annot.parse_field_annot ploc [ single ]
                in
              return? (p_value, nil, pannot)
            | (_, _) => return? (p_value, pannot, None)
            end
          end in
        let? '(arg_type, root_name) :=
          if legacy then
            match maybe_root_name with
            | Pervasives.Ok (p_value, _, root_name) =>
              return? (p_value, root_name)
            | Pervasives.Error _ => return? (p_value, None)
            end
          else
            let? '(p_value, pannot, root_name) := maybe_root_name in
            let? '_ := Script_ir_annot.error_unexpected_annot ploc pannot in
            let? '_ := Script_ir_annot.error_unexpected_annot cloc carrot in
            let? '_ := Script_ir_annot.error_unexpected_annot sloc sannot in
            return? (p_value, root_name) in
        return?
          ({| toplevel.code_field := c; toplevel.arg_type := arg_type;
            toplevel.storage_type := s; toplevel.views := views;
            toplevel.root_name := root_name |}, ctxt)
      end
    end.

Definition parse_contract_for_script
  (ctxt : Alpha_context.context) (loc : Alpha_context.Script.location)
  (arg : Script_typed_ir.ty) (contract : Alpha_context.Contract.t)
  (entrypoint : string)
  : M=? (Alpha_context.context * option Script_typed_ir.typed_contract) :=
  match Alpha_context.Contract.is_implicit contract with
  | Some _ =>
    match entrypoint with
    | "default" =>
      return=
        (let? '(eq_ty, ctxt) :=
          Gas_monad.(GAS_MONAD.run) ctxt
            (merge_types true Fast_merge_type_error loc arg
              (Script_typed_ir.unit_t None)) in
        match eq_ty with
        | Pervasives.Ok (Eq, _ty) =>
          let contract := (arg, (contract, entrypoint)) in
          return? (ctxt, (Some contract))
        | Pervasives.Error _ => return? (ctxt, None)
        end)
    | _ =>
      return=
        (let? ctxt :=
          Alpha_context.Gas.consume ctxt Typecheck_costs.parse_instr_cycle in
        return? (ctxt, None))
    end
  | None =>
    let=? '(ctxt, code) :=
      Error_monad.trace_value
        (Build_extensible "Invalid_contract"
          (Alpha_context.Script.location * Alpha_context.Contract.t)
          (loc, contract))
        (Alpha_context.Contract.get_script_code ctxt contract) in
    match code with
    | None => return=? (ctxt, None)
    | Some code =>
      return=
        (let? '(code, ctxt) :=
          Alpha_context.Script.force_decode_in_context ctxt code in
        match parse_toplevel ctxt true code with
        | Pervasives.Error _ =>
          Error_monad.error_value
            (Build_extensible "Invalid_contract"
              (Alpha_context.Script.location * Alpha_context.Contract.t)
              (loc, contract))
        |
          Pervasives.Ok
            ({|
              toplevel.arg_type := arg_type;
                toplevel.root_name := root_name
                |}, ctxt) =>
          match parse_parameter_ty ctxt 0 true arg_type with
          | Pervasives.Error _ =>
            Error_monad.error_value
              (Build_extensible "Invalid_contract"
                (Alpha_context.Script.location * Alpha_context.Contract.t)
                (loc, contract))
          | Pervasives.Ok (Ex_ty targ, ctxt) =>
            let? '(entrypoint_arg, ctxt) :=
              Gas_monad.(GAS_MONAD.run) ctxt
                (find_entrypoint_for_type false Fast_merge_type_error targ arg
                  root_name entrypoint loc) in
            match entrypoint_arg with
            | Pervasives.Ok (entrypoint, arg) =>
              let contract := (arg, (contract, entrypoint)) in
              return? (ctxt, (Some contract))
            | Pervasives.Error _ => return? (ctxt, None)
            end
          end
        end)
    end
  end.

Definition parse_code
  (type_logger_value : option type_logger) (ctxt : Alpha_context.context)
  (legacy : bool) (code : Alpha_context.Script.lazy_expr)
  : M=? (ex_code * Alpha_context.context) :=
  let=? '(code, ctxt) :=
    return= (Alpha_context.Script.force_decode_in_context ctxt code) in
  let=? '(ctxt, code) := Alpha_context.Global_constants_storage.expand ctxt code
    in
  let=?
    '({|
      toplevel.code_field := code_field;
        toplevel.arg_type := arg_type;
        toplevel.storage_type := storage_type;
        toplevel.views := views;
        toplevel.root_name := root_name
        |}, ctxt) := return= (parse_toplevel ctxt legacy code) in
  let arg_type_loc := location arg_type in
  let=? '(Ex_ty arg_type, ctxt) :=
    return=
      (Error_monad.record_trace
        (Build_extensible "Ill_formed_type"
          (option string * Alpha_context.Script.expr *
            Alpha_context.Script.location)
          ((Some "parameter"), code, arg_type_loc))
        (parse_parameter_ty ctxt 0 legacy arg_type)) in
  let=? '_ :=
    return=
      (if legacy then
        Error_monad.ok_unit
      else
        well_formed_entrypoints arg_type root_name) in
  let storage_type_loc := location storage_type in
  let=? '(Ex_ty storage_type, ctxt) :=
    return=
      (Error_monad.record_trace
        (Build_extensible "Ill_formed_type"
          (option string * Alpha_context.Script.expr *
            Alpha_context.Script.location)
          ((Some "storage"), code, storage_type_loc))
        (parse_storage_ty ctxt 0 legacy storage_type)) in
  let arg_annot :=
    Script_ir_annot.default_annot Script_ir_annot.default_param_annot
      (Script_ir_annot.type_to_var_annot (name_of_ty arg_type)) in
  let storage_annot :=
    Script_ir_annot.default_annot Script_ir_annot.default_storage_annot
      (Script_ir_annot.type_to_var_annot (name_of_ty storage_type)) in
  let=? arg_type_full :=
    return=
      (Script_typed_ir.pair_t storage_type_loc (arg_type, None, arg_annot)
        (storage_type, None, storage_annot) None) in
  let=? ret_type_full :=
    return=
      (Script_typed_ir.pair_t storage_type_loc
        (Script_typed_ir.list_operation_t, None, None)
        (storage_type, None, None) None) in
  let=? '(code, ctxt) :=
    Error_monad.trace_value
      (Build_extensible "Ill_typed_contract"
        (Alpha_context.Script.expr * Script_tc_errors.type_map) (code, nil))
      (parse_returning type_logger_value 0
        (Toplevel
          {| tc_context.Toplevel.storage_type := storage_type;
            tc_context.Toplevel.param_type := arg_type;
            tc_context.Toplevel.root_name := root_name;
            tc_context.Toplevel.legacy_create_contract_literal := false |}) ctxt
        legacy (arg_type_full, None) ret_type_full code_field) in
  return=
    (let view_size (view : Script_typed_ir.view)
      : Cache_memory_helpers.nodes_and_size :=
      Script_typed_ir_size.op_plusplus
        (Script_typed_ir_size.op_plusplus
          (Script_typed_ir_size.node_size view.(Script_typed_ir.view.view_code))
          (Script_typed_ir_size.node_size view.(Script_typed_ir.view.input_ty)))
        (Script_typed_ir_size.node_size view.(Script_typed_ir.view.output_ty))
      in
    let views_size :=
      Script_typed_ir.SMap.(Map.S.fold)
        (fun (function_parameter : Script_string_repr.t) =>
          let '_ := function_parameter in
          fun (v : Script_typed_ir.view) =>
            fun (s : Cache_memory_helpers.nodes_and_size) =>
              Script_typed_ir_size.op_plusplus (view_size v) s) views
        Script_typed_ir_size.zero in
    let ir_size := Script_typed_ir_size.lambda_size code in
    let '(nodes, code_size) :=
      Script_typed_ir_size.op_plusplus views_size ir_size in
    let? ctxt :=
      Alpha_context.Gas.consume ctxt
        (Script_typed_ir_size_costs.nodes_cost nodes) in
    return?
      ((Ex_code
        {| code.code := code; code.arg_type := arg_type;
          code.storage_type := storage_type; code.views := views;
          code.root_name := root_name; code.code_size := code_size |}), ctxt)).

Definition parse_storage {storage : Set}
  (type_logger_value : option type_logger) (ctxt : Alpha_context.context)
  (legacy : bool) (allow_forged : bool) (storage_type : Script_typed_ir.ty)
  (storage_value : Alpha_context.Script.lazy_expr)
  : M=? (storage * Alpha_context.context) :=
  let=? '(storage_value, ctxt) :=
    return= (Alpha_context.Script.force_decode_in_context ctxt storage_value) in
  Error_monad.trace_eval
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      return=
        (let? '(storage_type, _ctxt) := serialize_ty_for_error ctxt storage_type
          in
        return?
          (Build_extensible "Ill_typed_data"
            (option string * Alpha_context.Script.expr *
              Micheline.canonical Alpha_context.Script.prim)
            (None, storage_value, storage_type))))
    (parse_data type_logger_value 0 ctxt legacy allow_forged storage_type
      (Micheline.root storage_value)).

Axiom parse_script :
  option type_logger -> Alpha_context.context -> bool -> bool ->
  Alpha_context.Script.t -> M=? (ex_script * Alpha_context.context).

Definition typecheck_code
  (legacy : bool) (ctxt : Alpha_context.context)
  (code : Alpha_context.Script.expr)
  : M=? (Script_tc_errors.type_map * Alpha_context.context) :=
  let=? '(ctxt, code) := Alpha_context.Global_constants_storage.expand ctxt code
    in
  let=?
    '({|
      toplevel.code_field := code_field;
        toplevel.arg_type := arg_type;
        toplevel.storage_type := storage_type;
        toplevel.views := views;
        toplevel.root_name := root_name
        |}, ctxt) := return= (parse_toplevel ctxt legacy code) in
  let type_map := Pervasives.ref_value nil in
  let arg_type_loc := location arg_type in
  let=? '(Ex_ty arg_type, ctxt) :=
    return=
      (Error_monad.record_trace
        (Build_extensible "Ill_formed_type"
          (option string * Alpha_context.Script.expr *
            Alpha_context.Script.location)
          ((Some "parameter"), code, arg_type_loc))
        (parse_parameter_ty ctxt 0 legacy arg_type)) in
  let=? '_ :=
    return=
      (if legacy then
        Error_monad.ok_unit
      else
        well_formed_entrypoints arg_type root_name) in
  let storage_type_loc := location storage_type in
  let=? '(Ex_ty storage_type, ctxt) :=
    return=
      (Error_monad.record_trace
        (Build_extensible "Ill_formed_type"
          (option string * Alpha_context.Script.expr *
            Alpha_context.Script.location)
          ((Some "storage"), code, storage_type_loc))
        (parse_storage_ty ctxt 0 legacy storage_type)) in
  let arg_annot :=
    Script_ir_annot.default_annot Script_ir_annot.default_param_annot
      (Script_ir_annot.type_to_var_annot (name_of_ty arg_type)) in
  let storage_annot :=
    Script_ir_annot.default_annot Script_ir_annot.default_storage_annot
      (Script_ir_annot.type_to_var_annot (name_of_ty storage_type)) in
  let=? arg_type_full :=
    return=
      (Script_typed_ir.pair_t storage_type_loc (arg_type, None, arg_annot)
        (storage_type, None, storage_annot) None) in
  let=? ret_type_full :=
    return=
      (Script_typed_ir.pair_t storage_type_loc
        (Script_typed_ir.list_operation_t, None, None)
        (storage_type, None, None) None) in
  let result_value :=
    parse_returning
      (Some
        (fun (loc : int) =>
          fun (bef : Script_tc_errors.unparsed_stack_ty) =>
            fun (aft : Script_tc_errors.unparsed_stack_ty) =>
              Pervasives.op_coloneq type_map
                (cons (loc, (bef, aft)) (Pervasives.op_exclamation type_map))))
      0
      (Toplevel
        {| tc_context.Toplevel.storage_type := storage_type;
          tc_context.Toplevel.param_type := arg_type;
          tc_context.Toplevel.root_name := root_name;
          tc_context.Toplevel.legacy_create_contract_literal := false |}) ctxt
      legacy (arg_type_full, None) ret_type_full code_field in
  let=? '(Script_typed_ir.Lam _ _, ctxt) :=
    Error_monad.trace_value
      (Build_extensible "Ill_typed_contract"
        (Alpha_context.Script.expr * Script_tc_errors.type_map)
        (code, (Pervasives.op_exclamation type_map))) result_value in
  let views_result :=
    typecheck_views
      (Some
        (fun (loc : int) =>
          fun (bef : Script_tc_errors.unparsed_stack_ty) =>
            fun (aft : Script_tc_errors.unparsed_stack_ty) =>
              Pervasives.op_coloneq type_map
                (cons (loc, (bef, aft)) (Pervasives.op_exclamation type_map))))
      ctxt legacy storage_type views in
  let=? ctxt :=
    Error_monad.trace_value
      (Build_extensible "Ill_typed_contract"
        (Alpha_context.Script.expr * Script_tc_errors.type_map)
        (code, (Pervasives.op_exclamation type_map))) views_result in
  return=? ((Pervasives.op_exclamation type_map), ctxt).

Definition Entrypoints_map :=
  Map.Make
    {|
      Compare.COMPARABLE.compare := String.compare
    |}.

Axiom list_entrypoints :
  Script_typed_ir.ty -> Alpha_context.context ->
  option Script_typed_ir.field_annot ->
  M?
    (list (list Alpha_context.Script.prim) *
      Entrypoints_map.(Map.S.t)
        (list Alpha_context.Script.prim * Alpha_context.Script.node)).

Definition comb_witness2 (function_parameter : Script_typed_ir.ty)
  : comb_witness :=
  match function_parameter with
  | Script_typed_ir.Pair_t _ (Script_typed_ir.Pair_t _ _ _, _, _) _ =>
    Comb_Pair (Comb_Pair Comb_Any)
  | Script_typed_ir.Pair_t _ _ _ => Comb_Pair Comb_Any
  | _ => Comb_Any
  end.

Axiom unparse_data : forall {a : Set},
  Alpha_context.context -> int -> unparsing_mode -> Script_typed_ir.ty -> a ->
  M=? (Alpha_context.Script.node * Alpha_context.context).

Axiom unparse_code :
  Alpha_context.context -> int -> unparsing_mode -> Alpha_context.Script.node ->
  M=?
    (Micheline.node Alpha_context.Script.location Alpha_context.Script.prim *
      Alpha_context.context).

Fixpoint unparse_items {k v : Set}
  (ctxt : Alpha_context.context) (stack_depth : int) (mode : unparsing_mode)
  (kt : Script_typed_ir.comparable_ty) (vt : Script_typed_ir.ty)
  (items : list (k * v))
  : M=? (list Alpha_context.Script.node * Alpha_context.context) :=
  List.fold_left_es
    (fun (function_parameter :
      list Alpha_context.Script.node * Alpha_context.context) =>
      let '(l_value, ctxt) := function_parameter in
      fun (function_parameter : k * v) =>
        let '(k, v) := function_parameter in
        let=? '(key_value, ctxt) := unparse_comparable_data ctxt mode kt k in
        let=? '(value, ctxt) := unparse_data ctxt (stack_depth +i 1) mode vt v
          in
        return=?
          ((cons
            (Micheline.Prim (-1) Michelson_v1_primitives.D_Elt
              [ key_value; value ] nil) l_value), ctxt)) (nil, ctxt) items.

Definition unparse_script {A : Set}
  (ctxt : Alpha_context.context) (mode : unparsing_mode)
  (function_parameter : Script_typed_ir.script A)
  : M=? (Alpha_context.Script.t * Alpha_context.context) :=
  let '{|
    Script_typed_ir.script.code := code;
      Script_typed_ir.script.arg_type := arg_type;
      Script_typed_ir.script.storage := storage_value;
      Script_typed_ir.script.storage_type := storage_type;
      Script_typed_ir.script.views := views;
      Script_typed_ir.script.root_name := root_name
      |} := function_parameter in
  let 'Script_typed_ir.Lam _ original_code := code in
  let=? '(code, ctxt) := unparse_code ctxt 0 mode original_code in
  let=? '(storage_value, ctxt) :=
    unparse_data ctxt 0 mode storage_type storage_value in
  return=
    (let? '(arg_type, ctxt) := unparse_ty ctxt arg_type in
    let? '(storage_type, ctxt) := unparse_ty ctxt storage_type in
    let arg_type := add_field_annot root_name None arg_type in
    let view
      (name : Alpha_context.Script_string.t)
      (function_parameter : Script_typed_ir.view)
      : list
        (Micheline.node Alpha_context.Script.location Alpha_context.Script.prim)
      ->
      list
        (Micheline.node Alpha_context.Script.location Alpha_context.Script.prim) :=
      let '{|
        Script_typed_ir.view.input_ty := input_ty;
          Script_typed_ir.view.output_ty := output_ty;
          Script_typed_ir.view.view_code := view_code
          |} := function_parameter in
      fun (views :
        list
          (Micheline.node Alpha_context.Script.location
            Alpha_context.Script.prim)) =>
        cons
          (Micheline.Prim (-1) Michelson_v1_primitives.K_view
            [
              Micheline.String (-1) (Alpha_context.Script_string.to_string name);
              input_ty;
              output_ty;
              view_code
            ] nil) views in
    let views := List.rev (Script_typed_ir.SMap.(Map.S.fold) view views nil) in
    let code :=
      Micheline.Seq (-1)
        (Pervasives.op_at
          [
            Micheline.Prim (-1) Michelson_v1_primitives.K_parameter [ arg_type ]
              nil;
            Micheline.Prim (-1) Michelson_v1_primitives.K_storage
              [ storage_type ] nil;
            Micheline.Prim (-1) Michelson_v1_primitives.K_code [ code ] nil
          ] views) in
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Unparse_costs.unparse_instr_cycle in
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Unparse_costs.unparse_instr_cycle in
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Unparse_costs.unparse_instr_cycle in
    let? ctxt :=
      Alpha_context.Gas.consume ctxt Unparse_costs.unparse_instr_cycle in
    let? ctxt :=
      Alpha_context.Gas.consume ctxt
        (Alpha_context.Script.strip_locations_cost code) in
    let? ctxt :=
      Alpha_context.Gas.consume ctxt
        (Alpha_context.Script.strip_locations_cost storage_value) in
    return?
      ({|
        Alpha_context.Script.t.code :=
          Alpha_context.Script.lazy_expr_value (Micheline.strip_locations code);
        Alpha_context.Script.t.storage :=
          Alpha_context.Script.lazy_expr_value
            (Micheline.strip_locations storage_value) |}, ctxt)).

Definition pack_data_with_mode {A : Set}
  (ctxt : Alpha_context.context) (typ : Script_typed_ir.ty) (data : A)
  (mode : unparsing_mode) : M=? (bytes * Alpha_context.context) :=
  let=? '(unparsed, ctxt) := unparse_data ctxt 0 mode typ data in
  return= (pack_node unparsed ctxt).

Definition hash_data {A : Set}
  (ctxt : Alpha_context.context) (typ : Script_typed_ir.ty) (data : A)
  : M=? (Script_expr_hash.t * Alpha_context.context) :=
  let=? '(bytes_value, ctxt) :=
    pack_data_with_mode ctxt typ data Optimized_legacy in
  return= (hash_bytes ctxt bytes_value).

Definition pack_data {A : Set}
  (ctxt : Alpha_context.context) (typ : Script_typed_ir.ty) (data : A)
  : M=? (bytes * Alpha_context.context) :=
  pack_data_with_mode ctxt typ data Optimized_legacy.

Definition empty_big_map {A B : Set}
  (key_type : Script_typed_ir.comparable_ty) (value_type : Script_typed_ir.ty)
  : Script_typed_ir.big_map A B :=
  {| Script_typed_ir.big_map.id := None;
    Script_typed_ir.big_map.diff :=
      {|
        Script_typed_ir.big_map_overlay.map :=
          Script_typed_ir.Big_map_overlay.(Map.S.empty);
        Script_typed_ir.big_map_overlay.size := 0 |};
    Script_typed_ir.big_map.key_type := key_type;
    Script_typed_ir.big_map.value_type := value_type |}.

Definition big_map_mem {A B : Set}
  (ctxt : Alpha_context.context) (key_value : A)
  (function_parameter : Script_typed_ir.big_map A B)
  : M=? (bool * Alpha_context.context) :=
  let '{|
    Script_typed_ir.big_map.id := id;
      Script_typed_ir.big_map.diff := diff_value;
      Script_typed_ir.big_map.key_type := key_type
      |} := function_parameter in
  let=? '(key_value, ctxt) := hash_comparable_data ctxt key_type key_value in
  match
    ((Script_typed_ir.Big_map_overlay.(Map.S.find) key_value
      diff_value.(Script_typed_ir.big_map_overlay.map)), id) with
  | (None, None) => return=? (false, ctxt)
  | (None, Some id) =>
    let=? '(ctxt, res) := Alpha_context.Big_map.mem ctxt id key_value in
    return=? (res, ctxt)
  | (Some (_, None), _) => return=? (false, ctxt)
  | (Some (_, Some _), _) => return=? (true, ctxt)
  end.

Definition big_map_get_by_hash {A B : Set}
  (ctxt : Alpha_context.context) (key_value : Script_expr_hash.t)
  (function_parameter : Script_typed_ir.big_map A B)
  : M=? (option B * Alpha_context.context) :=
  let '{|
    Script_typed_ir.big_map.id := id;
      Script_typed_ir.big_map.diff := diff_value;
      Script_typed_ir.big_map.value_type := value_type
      |} := function_parameter in
  match
    ((Script_typed_ir.Big_map_overlay.(Map.S.find) key_value
      diff_value.(Script_typed_ir.big_map_overlay.map)), id) with
  | (Some (_, x), _) => return=? (x, ctxt)
  | (None, None) => return=? (None, ctxt)
  | (None, Some id) =>
    let=? function_parameter := Alpha_context.Big_map.get_opt ctxt id key_value
      in
    match function_parameter with
    | (ctxt, None) => return=? (None, ctxt)
    | (ctxt, Some value) =>
      let=? '(x, ctxt) :=
        parse_data None 0 ctxt true true value_type (Micheline.root value) in
      return=? ((Some x), ctxt)
    end
  end.

Definition big_map_get {A B : Set}
  (ctxt : Alpha_context.context) (key_value : A)
  (map : Script_typed_ir.big_map A B)
  : M=? (option B * Alpha_context.context) :=
  let=? '(key_hash, ctxt) :=
    hash_comparable_data ctxt map.(Script_typed_ir.big_map.key_type) key_value
    in
  big_map_get_by_hash ctxt key_hash map.

Definition big_map_update_by_hash {A B C D : Set}
  (ctxt : A) (key_hash : Script_expr_hash.t) (key_value : B) (value : option C)
  (map : Script_typed_ir.big_map B C)
  : M= (Pervasives.result (Script_typed_ir.big_map B C * A) D) :=
  let contains :=
    Script_typed_ir.Big_map_overlay.(Map.S.mem) key_hash
      map.(Script_typed_ir.big_map.diff).(Script_typed_ir.big_map_overlay.map)
    in
  return=?
    ((Script_typed_ir.big_map.with_diff
      {|
        Script_typed_ir.big_map_overlay.map :=
          Script_typed_ir.Big_map_overlay.(Map.S.add) key_hash
            (key_value, value)
            map.(Script_typed_ir.big_map.diff).(Script_typed_ir.big_map_overlay.map);
        Script_typed_ir.big_map_overlay.size :=
          if contains then
            map.(Script_typed_ir.big_map.diff).(Script_typed_ir.big_map_overlay.size)
          else
            map.(Script_typed_ir.big_map.diff).(Script_typed_ir.big_map_overlay.size)
            +i 1 |} map), ctxt).

Definition big_map_update {A B : Set}
  (ctxt : Alpha_context.context) (key_value : A) (value : option B)
  (map : Script_typed_ir.big_map A B)
  : M=? (Script_typed_ir.big_map A B * Alpha_context.context) :=
  let=? '(key_hash, ctxt) :=
    hash_comparable_data ctxt map.(Script_typed_ir.big_map.key_type) key_value
    in
  big_map_update_by_hash ctxt key_hash key_value value map.

Definition big_map_get_and_update {A B : Set}
  (ctxt : Alpha_context.context) (key_value : A) (value : option B)
  (map : Script_typed_ir.big_map A B)
  : M=? ((option B * Script_typed_ir.big_map A B) * Alpha_context.context) :=
  let=? '(key_hash, ctxt) :=
    hash_comparable_data ctxt map.(Script_typed_ir.big_map.key_type) key_value
    in
  let=? '(map', ctxt) :=
    big_map_update_by_hash ctxt key_hash key_value value map in
  let=? '(old_value, ctxt) := big_map_get_by_hash ctxt key_hash map in
  return=? ((old_value, map'), ctxt).

Definition lazy_storage_ids : Set := Alpha_context.Lazy_storage.IdSet.t.

Definition no_lazy_storage_id : Alpha_context.Lazy_storage.IdSet.t :=
  Alpha_context.Lazy_storage.IdSet.empty.

Definition diff_of_big_map {A B : Set}
  (ctxt : Alpha_context.context) (mode : unparsing_mode) (temporary : bool)
  (ids_to_copy : Alpha_context.Lazy_storage.IdSet.t)
  (function_parameter : Script_typed_ir.big_map A B)
  : M=?
    (Alpha_context.Lazy_storage.diff Alpha_context.Big_map.Id.t
      Alpha_context.Big_map.alloc (list Alpha_context.Big_map.update) *
      Alpha_context.Big_map.Id.t * Alpha_context.context) :=
  let '{|
    Script_typed_ir.big_map.id := id;
      Script_typed_ir.big_map.diff := diff_value;
      Script_typed_ir.big_map.key_type := key_type;
      Script_typed_ir.big_map.value_type := value_type
      |} := function_parameter in
  let=? '(ctxt, init_value, id) :=
    match id with
    | Some id =>
      if
        Alpha_context.Lazy_storage.IdSet.mem
          Alpha_context.Lazy_storage.Kind.Big_map id ids_to_copy
      then
        let=? '(ctxt, duplicate) := Alpha_context.Big_map.fresh temporary ctxt
          in
        return=?
          (ctxt,
            (Alpha_context.Lazy_storage.Copy
              {| Alpha_context.Lazy_storage.init.Copy.src := id |}), duplicate)
      else
        return=? (ctxt, Alpha_context.Lazy_storage.Existing, id)
    | None =>
      let=? '(ctxt, id) := Alpha_context.Big_map.fresh temporary ctxt in
      let kt := unparse_comparable_ty key_type in
      return=
        (let? ctxt :=
          Alpha_context.Gas.consume ctxt
            (Alpha_context.Script.strip_locations_cost kt) in
        let? '(kv, ctxt) := unparse_ty ctxt value_type in
        let? ctxt :=
          Alpha_context.Gas.consume ctxt
            (Alpha_context.Script.strip_locations_cost kv) in
        let key_type := Micheline.strip_locations kt in
        let value_type := Micheline.strip_locations kv in
        return?
          (ctxt,
            (Alpha_context.Lazy_storage.Alloc
              {| Alpha_context.Big_map.alloc.key_type := key_type;
                Alpha_context.Big_map.alloc.value_type := value_type |}), id))
    end in
  let pairs :=
    Script_typed_ir.Big_map_overlay.(Map.S.fold)
      (fun (key_hash : Script_expr_hash.t) =>
        fun (function_parameter : A * option B) =>
          let '(key_value, value) := function_parameter in
          fun (acc_value : list (Script_expr_hash.t * A * option B)) =>
            cons (key_hash, key_value, value) acc_value)
      diff_value.(Script_typed_ir.big_map_overlay.map) nil in
  let=? '(updates, ctxt) :=
    List.fold_left_es
      (fun (function_parameter :
        list Alpha_context.Big_map.update * Alpha_context.context) =>
        let '(acc_value, ctxt) := function_parameter in
        fun (function_parameter : Script_expr_hash.t * A * option B) =>
          let '(key_hash, key_value, value) := function_parameter in
          let=? ctxt :=
            return=
              (Alpha_context.Gas.consume ctxt Typecheck_costs.parse_instr_cycle)
            in
          let=? '(key_node, ctxt) :=
            unparse_comparable_data ctxt mode key_type key_value in
          let=? ctxt :=
            return=
              (Alpha_context.Gas.consume ctxt
                (Alpha_context.Script.strip_locations_cost key_node)) in
          let key_value := Micheline.strip_locations key_node in
          let=? '(value, ctxt) :=
            match value with
            | None => return=? (None, ctxt)
            | Some x =>
              let=? '(node_value, ctxt) := unparse_data ctxt 0 mode value_type x
                in
              return=
                (let? ctxt :=
                  Alpha_context.Gas.consume ctxt
                    (Alpha_context.Script.strip_locations_cost node_value) in
                return? ((Some (Micheline.strip_locations node_value)), ctxt))
            end in
          let diff_item :=
            {| Alpha_context.Big_map.update.key := key_value;
              Alpha_context.Big_map.update.key_hash := key_hash;
              Alpha_context.Big_map.update.value := value |} in
          return=? ((cons diff_item acc_value), ctxt)) (nil, ctxt)
      (List.rev pairs) in
  return=?
    ((Alpha_context.Lazy_storage.Update
      {| Alpha_context.Lazy_storage.diff.Update.init := init_value;
        Alpha_context.Lazy_storage.diff.Update.updates := updates |}), id, ctxt).

Definition diff_of_sapling_state
  (ctxt : Alpha_context.context) (temporary : bool)
  (ids_to_copy : Alpha_context.Lazy_storage.IdSet.t)
  (function_parameter : Alpha_context.Sapling.state)
  : M=?
    (Alpha_context.Lazy_storage.diff Alpha_context.Sapling.Id.t
      Alpha_context.Sapling.alloc Alpha_context.Sapling.diff *
      Alpha_context.Sapling.Id.t * Alpha_context.context) :=
  let '{|
    Alpha_context.Sapling.state.id := id;
      Alpha_context.Sapling.state.diff := diff_value;
      Alpha_context.Sapling.state.memo_size := memo_size
      |} := function_parameter in
  let=? '(ctxt, init_value, id) :=
    match id with
    | Some id =>
      if
        Alpha_context.Lazy_storage.IdSet.mem
          Alpha_context.Lazy_storage.Kind.Sapling_state id ids_to_copy
      then
        let=? '(ctxt, duplicate) := Alpha_context.Sapling.fresh temporary ctxt
          in
        return=?
          (ctxt,
            (Alpha_context.Lazy_storage.Copy
              {| Alpha_context.Lazy_storage.init.Copy.src := id |}), duplicate)
      else
        return=? (ctxt, Alpha_context.Lazy_storage.Existing, id)
    | None =>
      let=? '(ctxt, id) := Alpha_context.Sapling.fresh temporary ctxt in
      return=?
        (ctxt,
          (Alpha_context.Lazy_storage.Alloc
            {| Alpha_context.Sapling.alloc.memo_size := memo_size |}), id)
    end in
  return=?
    ((Alpha_context.Lazy_storage.Update
      {| Alpha_context.Lazy_storage.diff.Update.init := init_value;
        Alpha_context.Lazy_storage.diff.Update.updates := diff_value |}), id,
      ctxt).

(** Witness flag for whether a type can be populated by a value containing a
    lazy storage.
    [False_f] must be used only when a value of the type cannot contain a lazy
    storage.

    This flag is built in [has_lazy_storage] and used only in
    [extract_lazy_storage_updates] and [collect_lazy_storage].

    This flag is necessary to avoid these two functions to have a quadratic
    complexity in the size of the type.

    Add new lazy storage kinds here.

    Please keep the usage of this GADT local. *)
Inductive has_lazy_storage : Set :=
| True_f : has_lazy_storage
| False_f : has_lazy_storage
| Pair_f : has_lazy_storage -> has_lazy_storage -> has_lazy_storage
| Union_f : has_lazy_storage -> has_lazy_storage -> has_lazy_storage
| Option_f : has_lazy_storage -> has_lazy_storage
| List_f : has_lazy_storage -> has_lazy_storage
| Map_f : has_lazy_storage -> has_lazy_storage.

(** This function is called only on storage and parameter types of contracts,
    once per typechecked contract. It has a complexity linear in the size of
    the types, which happen to be literally written types, so the gas for them
    has already been paid. *)
Fixpoint has_lazy_storage_value (ty : Script_typed_ir.ty) : has_lazy_storage :=
  let aux1
    (cons_value : has_lazy_storage -> has_lazy_storage)
    (t_value : Script_typed_ir.ty) : has_lazy_storage :=
    match has_lazy_storage_value t_value with
    | False_f => False_f
    | h => cons_value h
    end in
  let aux2
    (cons_value : has_lazy_storage -> has_lazy_storage -> has_lazy_storage)
    (t1 : Script_typed_ir.ty) (t2 : Script_typed_ir.ty) : has_lazy_storage :=
    match ((has_lazy_storage_value t1), (has_lazy_storage_value t2)) with
    | (False_f, False_f) => False_f
    | (h1, h2) => cons_value h1 h2
    end in
  match ty with
  | Script_typed_ir.Big_map_t _ _ _ => True_f
  | Script_typed_ir.Sapling_state_t _ _ => True_f
  | Script_typed_ir.Unit_t _ => False_f
  | Script_typed_ir.Int_t _ => False_f
  | Script_typed_ir.Nat_t _ => False_f
  | Script_typed_ir.Signature_t _ => False_f
  | Script_typed_ir.String_t _ => False_f
  | Script_typed_ir.Bytes_t _ => False_f
  | Script_typed_ir.Mutez_t _ => False_f
  | Script_typed_ir.Key_hash_t _ => False_f
  | Script_typed_ir.Key_t _ => False_f
  | Script_typed_ir.Timestamp_t _ => False_f
  | Script_typed_ir.Address_t _ => False_f
  | Script_typed_ir.Bool_t _ => False_f
  | Script_typed_ir.Lambda_t _ _ _ => False_f
  | Script_typed_ir.Set_t _ _ => False_f
  | Script_typed_ir.Contract_t _ _ => False_f
  | Script_typed_ir.Operation_t _ => False_f
  | Script_typed_ir.Chain_id_t _ => False_f
  | Script_typed_ir.Never_t _ => False_f
  | Script_typed_ir.Bls12_381_g1_t _ => False_f
  | Script_typed_ir.Bls12_381_g2_t _ => False_f
  | Script_typed_ir.Bls12_381_fr_t _ => False_f
  | Script_typed_ir.Sapling_transaction_t _ _ => False_f
  | Script_typed_ir.Ticket_t _ _ => False_f
  | Script_typed_ir.Chest_key_t _ => False_f
  | Script_typed_ir.Chest_t _ => False_f
  | Script_typed_ir.Pair_t (l_value, _, _) (r_value, _, _) _ =>
    aux2
      (fun (l_value : has_lazy_storage) =>
        fun (r_value : has_lazy_storage) => Pair_f l_value r_value) l_value
      r_value
  | Script_typed_ir.Union_t (l_value, _) (r_value, _) _ =>
    aux2
      (fun (l_value : has_lazy_storage) =>
        fun (r_value : has_lazy_storage) => Union_f l_value r_value) l_value
      r_value
  | Script_typed_ir.Option_t t_value _ =>
    aux1 (fun (h : has_lazy_storage) => Option_f h) t_value
  | Script_typed_ir.List_t t_value _ =>
    aux1 (fun (h : has_lazy_storage) => List_f h) t_value
  | Script_typed_ir.Map_t _ t_value _ =>
    aux1 (fun (h : has_lazy_storage) => Map_f h) t_value
  end.

(** Transforms a value potentially containing lazy storage in an intermediary
  state to a value containing lazy storage only represented by identifiers.

  Returns the updated value, the updated set of ids to copy, and the lazy
  storage diff to show on the receipt and apply on the storage. *)
Axiom extract_lazy_storage_updates : forall {A : Set},
  Alpha_context.context -> unparsing_mode -> bool ->
  Alpha_context.Lazy_storage.IdSet.t -> Alpha_context.Lazy_storage.diffs ->
  Script_typed_ir.ty -> A ->
  M=?
    (Alpha_context.context * A * Alpha_context.Lazy_storage.IdSet.t *
      Alpha_context.Lazy_storage.diffs).

(** We namespace an error type for [fold_lazy_storage]. The error case is only
    available when the ['error] parameter is equal to unit. *)
Module Fold_lazy_storage.
  Inductive result (acc : Set) : Set :=
  | Ok : acc -> result acc
  | Error : result acc.
  
  Arguments Ok {_}.
  Arguments Error {_}.
End Fold_lazy_storage.

(** Prematurely abort if [f] generates an error. Use this function without the
    [unit] type for [error] if you are in a case where errors are impossible. *)
Axiom fold_lazy_storage : forall {acc a : Set},
  Alpha_context.Lazy_storage.IdSet.fold_f (Fold_lazy_storage.result acc) ->
  acc -> Alpha_context.context -> Script_typed_ir.ty -> a -> has_lazy_storage ->
  M? (Fold_lazy_storage.result acc * Alpha_context.context).

Axiom collect_lazy_storage : forall {A : Set},
  Alpha_context.context -> Script_typed_ir.ty -> A ->
  M? (Alpha_context.Lazy_storage.IdSet.t * Alpha_context.context).

Axiom extract_lazy_storage_diff : forall {A : Set},
  Alpha_context.context -> unparsing_mode -> bool ->
  Alpha_context.Lazy_storage.IdSet.t -> Alpha_context.Lazy_storage.IdSet.t ->
  Script_typed_ir.ty -> A ->
  M=? (A * option Alpha_context.Lazy_storage.diffs * Alpha_context.context).

Definition list_of_big_map_ids (ids : Alpha_context.Lazy_storage.IdSet.t)
  : list Alpha_context.Big_map.Id.t :=
  Alpha_context.Lazy_storage.IdSet.fold Alpha_context.Lazy_storage.Kind.Big_map
    (fun (id : Alpha_context.Big_map.Id.t) =>
      fun (acc_value : list Alpha_context.Big_map.Id.t) => cons id acc_value)
    ids nil.

Definition parse_data {A : Set}
  : option type_logger -> Alpha_context.context -> bool -> bool ->
  Script_typed_ir.ty -> Alpha_context.Script.node ->
  M=? (A * Alpha_context.context) := fun x_1 => parse_data x_1 0.

Definition parse_instr {a s : Set}
  (type_logger_value : option type_logger) (tc_context_value : tc_context)
  (ctxt : Alpha_context.context) (legacy : bool)
  (script_instr : Alpha_context.Script.node)
  (stack_ty : Script_typed_ir.stack_ty)
  : M=? (judgement a s * Alpha_context.context) :=
  parse_instr type_logger_value 0 tc_context_value ctxt legacy script_instr
    stack_ty.

Definition unparse_data {A : Set}
  : Alpha_context.context -> unparsing_mode -> Script_typed_ir.ty -> A ->
  M=? (Alpha_context.Script.node * Alpha_context.context) :=
  fun x_1 => unparse_data x_1 0.

Definition unparse_code {A : Set}
  (ctxt : Alpha_context.t) (mode : unparsing_mode)
  (code : Micheline.node A Alpha_context.Script.prim)
  : M=?
    (Micheline.node Alpha_context.Script.location Alpha_context.Script.prim *
      Alpha_context.context) :=
  let=? '(ctxt, code) :=
    Alpha_context.Global_constants_storage.expand ctxt
      (Micheline.strip_locations code) in
  unparse_code ctxt 0 mode (Micheline.root code).

Definition parse_contract
  (legacy : bool) (context_value : Alpha_context.context)
  (loc : Alpha_context.Script.location) (arg_ty : Script_typed_ir.ty)
  (contract : Alpha_context.Contract.t) (entrypoint : string)
  : M=? (Alpha_context.context * Script_typed_ir.typed_contract) :=
  parse_contract 0 legacy context_value loc arg_ty contract entrypoint.

Definition parse_comparable_ty
  : Alpha_context.context -> Alpha_context.Script.node ->
  M? (ex_comparable_ty * Alpha_context.context) := parse_comparable_ty 0.

Definition parse_big_map_value_ty
  : Alpha_context.context -> bool ->
  Micheline.node Alpha_context.Script.location Alpha_context.Script.prim ->
  M? (ex_ty * Alpha_context.context) := fun x_1 => parse_big_map_value_ty x_1 0.

Definition parse_packable_ty
  : Alpha_context.context -> bool -> Alpha_context.Script.node ->
  M? (ex_ty * Alpha_context.context) := fun x_1 => parse_packable_ty x_1 0.

Definition parse_parameter_ty
  : Alpha_context.context -> bool -> Alpha_context.Script.node ->
  M? (ex_ty * Alpha_context.context) := fun x_1 => parse_parameter_ty x_1 0.

Definition parse_any_ty
  : Alpha_context.context -> bool -> Alpha_context.Script.node ->
  M? (ex_ty * Alpha_context.context) := fun x_1 => parse_any_ty x_1 0.

Definition parse_ty
  : Alpha_context.context -> bool -> bool -> bool -> bool -> bool ->
  Alpha_context.Script.node -> M? (ex_ty * Alpha_context.context) :=
  fun x_1 => parse_ty x_1 0.

Definition ty_eq (ctxt : Alpha_context.context)
  : Alpha_context.Script.location -> Script_typed_ir.ty -> Script_typed_ir.ty ->
  M? (eq * Alpha_context.context) := ty_eq true ctxt.

Axiom get_single_sapling_state : forall {A : Set},
  Alpha_context.context -> Script_typed_ir.ty -> A ->
  M? (option Alpha_context.Sapling.Id.t * Alpha_context.context).

Definition script_size (function_parameter : ex_script)
  : int * Gas_limit_repr.cost :=
  let
    'Ex_script {|
      Script_typed_ir.script.code := _;
        Script_typed_ir.script.arg_type := _;
        Script_typed_ir.script.storage := storage_value;
        Script_typed_ir.script.storage_type := storage_type;
        Script_typed_ir.script.views := _;
        Script_typed_ir.script.root_name := _;
        Script_typed_ir.script.code_size := code_size
        |} := function_parameter in
  let 'existT _ __Ex_script_'c [code_size, storage_type, storage_value] as exi
    :=
    existT (A := Set)
      (fun __Ex_script_'c =>
        [Cache_memory_helpers.sint ** Script_typed_ir.ty ** __Ex_script_'c]) _
      [code_size, storage_type, storage_value]
    return
      let fst := projT1 exi in
      let __Ex_script_'c := fst in
      int * Gas_limit_repr.cost in
  let '(nodes, storage_size) :=
    Script_typed_ir_size.value_size storage_type storage_value in
  let cost := Script_typed_ir_size_costs.nodes_cost nodes in
  ((Saturation_repr.to_int (Saturation_repr.add code_size storage_size)), cost).
