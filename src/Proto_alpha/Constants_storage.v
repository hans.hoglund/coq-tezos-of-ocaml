Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Constants_repr.
Require TezosOfOCaml.Proto_alpha.Gas_limit_repr.
Require TezosOfOCaml.Proto_alpha.Period_repr.
Require TezosOfOCaml.Proto_alpha.Raw_context.
Require TezosOfOCaml.Proto_alpha.Tez_repr.

Definition preserved_cycles (c : Raw_context.t) : int :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.preserved_cycles).

Definition blocks_per_cycle (c : Raw_context.t) : int32 :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.blocks_per_cycle).

Definition blocks_per_commitment (c : Raw_context.t) : int32 :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.blocks_per_commitment).

Definition blocks_per_roll_snapshot (c : Raw_context.t) : int32 :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.blocks_per_roll_snapshot).

Definition blocks_per_voting_period (c : Raw_context.t) : int32 :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.blocks_per_voting_period).

Definition time_between_blocks (c : Raw_context.t) : list Period_repr.t :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.time_between_blocks).

Definition minimal_block_delay (c : Raw_context.t) : Period_repr.t :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.minimal_block_delay).

Definition endorsers_per_block (c : Raw_context.t) : int :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.endorsers_per_block).

Definition initial_endorsers (c : Raw_context.t) : int :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.initial_endorsers).

Definition delay_per_missing_endorsement (c : Raw_context.t) : Period_repr.t :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.delay_per_missing_endorsement).

Definition hard_gas_limit_per_operation (c : Raw_context.t)
  : Gas_limit_repr.Arith.integral :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.hard_gas_limit_per_operation).

Definition hard_gas_limit_per_block (c : Raw_context.t)
  : Gas_limit_repr.Arith.integral :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.hard_gas_limit_per_block).

Definition cost_per_byte (c : Raw_context.t) : Tez_repr.t :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.cost_per_byte).

Definition hard_storage_limit_per_operation (c : Raw_context.t) : Z.t :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.hard_storage_limit_per_operation).

Definition proof_of_work_threshold (c : Raw_context.t) : int64 :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.proof_of_work_threshold).

Definition tokens_per_roll (c : Raw_context.t) : Tez_repr.t :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.tokens_per_roll).

Definition seed_nonce_revelation_tip (c : Raw_context.t) : Tez_repr.t :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.seed_nonce_revelation_tip).

Definition origination_size (c : Raw_context.t) : int :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.origination_size).

Definition block_security_deposit (c : Raw_context.t) : Tez_repr.t :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.block_security_deposit).

Definition endorsement_security_deposit (c : Raw_context.t) : Tez_repr.t :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.endorsement_security_deposit).

Definition baking_reward_per_endorsement (c : Raw_context.t)
  : list Tez_repr.t :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.baking_reward_per_endorsement).

Definition endorsement_reward (c : Raw_context.t) : list Tez_repr.t :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.endorsement_reward).

Definition quorum_min (c : Raw_context.t) : int32 :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.quorum_min).

Definition quorum_max (c : Raw_context.t) : int32 :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.quorum_max).

Definition min_proposal_quorum (c : Raw_context.t) : int32 :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.min_proposal_quorum).

Definition liquidity_baking_subsidy (c : Raw_context.t) : Tez_repr.t :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.liquidity_baking_subsidy).

Definition liquidity_baking_sunset_level (c : Raw_context.t) : int32 :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.liquidity_baking_sunset_level).

Definition liquidity_baking_escape_ema_threshold (c : Raw_context.t) : int32 :=
  let constants := Raw_context.constants c in
  constants.(Constants_repr.parametric.liquidity_baking_escape_ema_threshold).

Definition parametric_value (c : Raw_context.t) : Constants_repr.parametric :=
  Raw_context.constants c.
