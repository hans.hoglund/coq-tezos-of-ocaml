Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Blinded_public_key_hash.
Require TezosOfOCaml.Proto_alpha.Bootstrap_storage.
Require TezosOfOCaml.Proto_alpha.Commitment_repr.
Require TezosOfOCaml.Proto_alpha.Commitment_storage.
Require TezosOfOCaml.Proto_alpha.Constants_repr.
Require TezosOfOCaml.Proto_alpha.Contract_repr.
Require TezosOfOCaml.Proto_alpha.Contract_storage.
Require TezosOfOCaml.Proto_alpha.Gas_limit_repr.
Require TezosOfOCaml.Proto_alpha.Lazy_storage_diff.
Require TezosOfOCaml.Proto_alpha.Level_repr.
Require TezosOfOCaml.Proto_alpha.Level_storage.
Require TezosOfOCaml.Proto_alpha.Liquidity_baking_migration.
Require TezosOfOCaml.Proto_alpha.Migration_repr.
Require TezosOfOCaml.Proto_alpha.Parameters_repr.
Require TezosOfOCaml.Proto_alpha.Period_repr.
Require TezosOfOCaml.Proto_alpha.Raw_context.
Require TezosOfOCaml.Proto_alpha.Raw_context_intf.
Require TezosOfOCaml.Proto_alpha.Receipt_repr.
Require TezosOfOCaml.Proto_alpha.Roll_storage.
Require TezosOfOCaml.Proto_alpha.Script_repr.
Require TezosOfOCaml.Proto_alpha.Seed_storage.
Require TezosOfOCaml.Proto_alpha.Storage.
Require TezosOfOCaml.Proto_alpha.Storage_sigs.
Require TezosOfOCaml.Proto_alpha.Tez_repr.
Require TezosOfOCaml.Proto_alpha.Vote_storage.

Definition log {A : Set} (fmt : Pervasives.format4 A Format.formatter unit unit)
  : A := Logging.log Logging.Notice fmt.

Module Flatten_storage_for_H.
  Definition flatten
    (tree : Raw_context.tree) (key_value : Raw_context.key) (depth : int)
    (rename : Raw_context.key -> Raw_context.key)
    (init_value : Raw_context.tree) : M= Raw_context.tree :=
    let= dst_tree :=
      Raw_context.Tree.(Raw_context_intf.TREE.fold) (Some (Context.Eq depth))
        tree key_value init_value
        (fun (old_key : Raw_context.key) =>
          fun (tree : Raw_context.tree) =>
            fun (dst_tree : Raw_context.tree) =>
              let new_key := rename old_key in
              Raw_context.Tree.(Raw_context_intf.TREE.add_tree) dst_tree new_key
                tree) in
    Raw_context.Tree.(Raw_context_intf.TREE.add_tree) tree key_value dst_tree.
  
  Definition fold_flatten
    (ctxt : Raw_context.t) (abs_key : Raw_context.key) (depth' : int)
    (mid_key : Raw_context.key) (depth : int)
    (rename : Raw_context.key -> Raw_context.key) : M= Raw_context.t :=
    Raw_context.fold (Some (Context.Eq depth')) ctxt abs_key ctxt
      (fun (key_value : Raw_context.key) =>
        fun (tree : Raw_context.tree) =>
          fun (ctxt : Raw_context.t) =>
            let= tree :=
              flatten tree mid_key depth rename
                (Raw_context.Tree.(Raw_context_intf.TREE.empty) ctxt) in
            Raw_context.add_tree ctxt (Pervasives.op_at abs_key key_value) tree).
  
  Definition flatten_storage (ctxt : Raw_context.t) : M= Raw_context.t :=
    let '_ :=
      log
        (CamlinternalFormatBasics.Format
          (CamlinternalFormatBasics.String_literal
            "flattening the context storage: this operation may take several minutes"
            CamlinternalFormatBasics.End_of_format)
          "flattening the context storage: this operation may take several minutes")
      in
    let fix drop {A : Set} (n : int) (xs : list A) : list A :=
      match (n, xs) with
      | (0, _) => xs
      | (_, []) =>
        (* ❌ Assert instruction is not handled. *)
        assert (list _) false
      | (_, cons _ xs) => drop (n -i 1) xs
      end in
    let rename_blake2b (function_parameter : list string) : list string :=
      match function_parameter with
      | cons n1 (cons n2 (cons n3 (cons n4 (cons n5 (cons n6 rest))))) =>
        cons (String.concat "" [ n1; n2; n3; n4; n5; n6 ]) rest
      | _ =>
        (* ❌ Assert instruction is not handled. *)
        assert (list string) false
      end in
    let rename_public_key_hash (function_parameter : list string)
      : list string :=
      match function_parameter with
      |
        cons (("ed25519" | "secp256k1" | "p256") as k)
          (cons n1 (cons n2 (cons n3 (cons n4 (cons n5 (cons n6 rest)))))) =>
        cons k (cons (String.concat "" [ n1; n2; n3; n4; n5; n6 ]) rest)
      | _ =>
        (* ❌ Assert instruction is not handled. *)
        assert (list string) false
      end in
    let= ctxt := fold_flatten ctxt [ "contracts"; "index" ] 0 nil 7 (drop 6) in
    let= ctxt :=
      fold_flatten ctxt [ "contracts"; "index" ] 1 [ "delegated" ] 7 (drop 6) in
    let= ctxt := fold_flatten ctxt [ "big_maps"; "index" ] 0 nil 7 (drop 6) in
    let= ctxt :=
      fold_flatten ctxt [ "big_maps"; "index" ] 1 [ "contents" ] 6
        rename_blake2b in
    let= ctxt := fold_flatten ctxt [ "rolls"; "index" ] 0 nil 3 (drop 2) in
    let= ctxt :=
      fold_flatten ctxt [ "rolls"; "owner"; "current" ] 0 nil 3 (drop 2) in
    let= ctxt :=
      fold_flatten ctxt [ "rolls"; "owner"; "snapshot" ] 2 nil 3 (drop 2) in
    let= ctxt := fold_flatten ctxt [ "commitments" ] 0 nil 6 rename_blake2b in
    let= ctxt :=
      fold_flatten ctxt [ "votes"; "listings" ] 0 nil 7 rename_public_key_hash
      in
    let= ctxt :=
      fold_flatten ctxt [ "votes"; "ballots" ] 0 nil 7 rename_public_key_hash in
    let= ctxt :=
      fold_flatten ctxt [ "votes"; "proposals_count" ] 0 nil 7
        rename_public_key_hash in
    let= ctxt :=
      fold_flatten ctxt [ "votes"; "proposals" ] 0 nil 6 rename_blake2b in
    let= ctxt :=
      fold_flatten ctxt [ "votes"; "proposals" ] 1 nil 7 rename_public_key_hash
      in
    let= ctxt :=
      fold_flatten ctxt [ "delegates" ] 0 nil 7 rename_public_key_hash in
    let= ctxt :=
      fold_flatten ctxt [ "active_delegates_with_rolls" ] 0 nil 7
        rename_public_key_hash in
    let= ctxt :=
      fold_flatten ctxt [ "delegates_with_frozen_balance" ] 1 nil 7
        rename_public_key_hash in
    let '_ :=
      log
        (CamlinternalFormatBasics.Format
          (CamlinternalFormatBasics.String_literal
            "context storage flattening completed"
            CamlinternalFormatBasics.End_of_format)
          "context storage flattening completed") in
    return= ctxt.
End Flatten_storage_for_H.

Definition prepare_first_block
  (ctxt : Context.t)
  (typecheck :
    Raw_context.t -> Script_repr.t ->
    M=? ((Script_repr.t * option Lazy_storage_diff.diffs) * Raw_context.t))
  (level : int32) (timestamp : Time.t) (fitness : Fitness.t)
  : M=? Raw_context.t :=
  let=? '(previous_protocol, ctxt) :=
    Raw_context.prepare_first_block level timestamp fitness ctxt in
  match previous_protocol with
  | Raw_context.Genesis param =>
    let=? ctxt :=
      Commitment_storage.init_value ctxt param.(Parameters_repr.t.commitments)
      in
    let=? ctxt := Roll_storage.init_value ctxt in
    let=? ctxt := Seed_storage.init_value ctxt in
    let=? ctxt := Contract_storage.init_value ctxt in
    let=? ctxt :=
      Bootstrap_storage.init_value ctxt typecheck
        param.(Parameters_repr.t.security_deposit_ramp_up_cycles)
        param.(Parameters_repr.t.no_reward_cycles)
        param.(Parameters_repr.t.bootstrap_accounts)
        param.(Parameters_repr.t.bootstrap_contracts) in
    let=? ctxt := Roll_storage.init_first_cycles ctxt in
    let=? ctxt :=
      Vote_storage.init_value ctxt
        (Level_storage.current ctxt).(Level_repr.t.level_position) in
    let=? ctxt :=
      Storage.Block_priority.(Storage.Simple_single_data_storage.init_value)
        ctxt 0 in
    let=? ctxt := Vote_storage.update_listings ctxt in
    let=? '(ctxt, operation_results) :=
      Liquidity_baking_migration.init_value ctxt typecheck in
    Storage.Pending_migration.Operation_results.(Storage_sigs.Single_data_storage.init_value)
      ctxt operation_results
  | Raw_context.Granada_010 =>
    Error_monad.op_gtgteq (Flatten_storage_for_H.flatten_storage ctxt)
      Error_monad._return
  end.

Definition prepare
  (ctxt : Context.t) (level : Int32.t) (predecessor_timestamp : Time.t)
  (timestamp : Time.t) (fitness : Fitness.t)
  : M=?
    (Raw_context.t * Receipt_repr.balance_updates *
      list Migration_repr.origination_result) :=
  let=? ctxt :=
    Raw_context.prepare level predecessor_timestamp timestamp fitness ctxt in
  Storage.Pending_migration.remove ctxt.
