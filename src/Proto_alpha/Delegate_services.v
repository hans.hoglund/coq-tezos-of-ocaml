Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Baking.
Require TezosOfOCaml.Proto_alpha.Services_registration.

(** Init function; without side-effects in Coq *)
Definition init_module : unit :=
  Error_monad.register_error_kind Error_monad.Temporary
    "delegate_service.balance_rpc_on_non_delegate"
    "Balance request for an unregistered delegate"
    "The account whose balance was requested is not a delegate."
    (Some
      (fun (ppf : Format.formatter) =>
        fun (pkh : Signature.public_key_hash) =>
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal "The implicit account ("
                (CamlinternalFormatBasics.Alpha
                  (CamlinternalFormatBasics.String_literal
                    ") whose balance was requested is not a registered delegate. To get the balance of this account you can use the ../context/contracts/"
                    (CamlinternalFormatBasics.Alpha
                      (CamlinternalFormatBasics.String_literal "/balance RPC."
                        CamlinternalFormatBasics.End_of_format)))))
              "The implicit account (%a) whose balance was requested is not a registered delegate. To get the balance of this account you can use the ../context/contracts/%a/balance RPC.")
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp) pkh
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp) pkh))
    (Data_encoding.obj1
      (Data_encoding.req None None "pkh"
        Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)))
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Balance_rpc_non_delegate" then
          let 'pkh := cast Alpha_context.public_key_hash payload in
          Some pkh
        else None
      end)
    (fun (pkh : Signature.public_key_hash) =>
      Build_extensible "Balance_rpc_non_delegate" Signature.public_key_hash pkh).

Module info.
  Record record : Set := Build {
    balance : Alpha_context.Tez.t;
    frozen_balance : Alpha_context.Tez.t;
    frozen_balance_by_cycle :
      Alpha_context.Cycle.Map.(Map.S.t) Alpha_context.Delegate.frozen_balance;
    staking_balance : Alpha_context.Tez.t;
    delegated_contracts : list Alpha_context.Contract.t;
    delegated_balance : Alpha_context.Tez.t;
    deactivated : bool;
    grace_period : Alpha_context.Cycle.t;
    voting_power : int32 }.
  Definition with_balance balance (r : record) :=
    Build balance r.(frozen_balance) r.(frozen_balance_by_cycle)
      r.(staking_balance) r.(delegated_contracts) r.(delegated_balance)
      r.(deactivated) r.(grace_period) r.(voting_power).
  Definition with_frozen_balance frozen_balance (r : record) :=
    Build r.(balance) frozen_balance r.(frozen_balance_by_cycle)
      r.(staking_balance) r.(delegated_contracts) r.(delegated_balance)
      r.(deactivated) r.(grace_period) r.(voting_power).
  Definition with_frozen_balance_by_cycle frozen_balance_by_cycle
    (r : record) :=
    Build r.(balance) r.(frozen_balance) frozen_balance_by_cycle
      r.(staking_balance) r.(delegated_contracts) r.(delegated_balance)
      r.(deactivated) r.(grace_period) r.(voting_power).
  Definition with_staking_balance staking_balance (r : record) :=
    Build r.(balance) r.(frozen_balance) r.(frozen_balance_by_cycle)
      staking_balance r.(delegated_contracts) r.(delegated_balance)
      r.(deactivated) r.(grace_period) r.(voting_power).
  Definition with_delegated_contracts delegated_contracts (r : record) :=
    Build r.(balance) r.(frozen_balance) r.(frozen_balance_by_cycle)
      r.(staking_balance) delegated_contracts r.(delegated_balance)
      r.(deactivated) r.(grace_period) r.(voting_power).
  Definition with_delegated_balance delegated_balance (r : record) :=
    Build r.(balance) r.(frozen_balance) r.(frozen_balance_by_cycle)
      r.(staking_balance) r.(delegated_contracts) delegated_balance
      r.(deactivated) r.(grace_period) r.(voting_power).
  Definition with_deactivated deactivated (r : record) :=
    Build r.(balance) r.(frozen_balance) r.(frozen_balance_by_cycle)
      r.(staking_balance) r.(delegated_contracts) r.(delegated_balance)
      deactivated r.(grace_period) r.(voting_power).
  Definition with_grace_period grace_period (r : record) :=
    Build r.(balance) r.(frozen_balance) r.(frozen_balance_by_cycle)
      r.(staking_balance) r.(delegated_contracts) r.(delegated_balance)
      r.(deactivated) grace_period r.(voting_power).
  Definition with_voting_power voting_power (r : record) :=
    Build r.(balance) r.(frozen_balance) r.(frozen_balance_by_cycle)
      r.(staking_balance) r.(delegated_contracts) r.(delegated_balance)
      r.(deactivated) r.(grace_period) voting_power.
End info.
Definition info := info.record.

Definition info_encoding : Data_encoding.encoding info :=
  Data_encoding.conv
    (fun (function_parameter : info) =>
      let '{|
        info.balance := balance;
          info.frozen_balance := frozen_balance_value;
          info.frozen_balance_by_cycle := frozen_balance_by_cycle;
          info.staking_balance := staking_balance;
          info.delegated_contracts := delegated_contracts;
          info.delegated_balance := delegated_balance;
          info.deactivated := deactivated;
          info.grace_period := grace_period;
          info.voting_power := voting_power
          |} := function_parameter in
      (balance, frozen_balance_value, frozen_balance_by_cycle, staking_balance,
        delegated_contracts, delegated_balance, deactivated, grace_period,
        voting_power))
    (fun (function_parameter :
      Alpha_context.Tez.t * Alpha_context.Tez.t *
        Alpha_context.Cycle.Map.(Map.S.t) Alpha_context.Delegate.frozen_balance
        * Alpha_context.Tez.t * list Alpha_context.Contract.t *
        Alpha_context.Tez.t * bool * Alpha_context.Cycle.t * int32) =>
      let
        '(balance, frozen_balance_value, frozen_balance_by_cycle,
          staking_balance, delegated_contracts, delegated_balance, deactivated,
          grace_period, voting_power) := function_parameter in
      {| info.balance := balance; info.frozen_balance := frozen_balance_value;
        info.frozen_balance_by_cycle := frozen_balance_by_cycle;
        info.staking_balance := staking_balance;
        info.delegated_contracts := delegated_contracts;
        info.delegated_balance := delegated_balance;
        info.deactivated := deactivated; info.grace_period := grace_period;
        info.voting_power := voting_power |}) None
    (Data_encoding.obj9
      (Data_encoding.req None None "balance" Alpha_context.Tez.encoding)
      (Data_encoding.req None None "frozen_balance" Alpha_context.Tez.encoding)
      (Data_encoding.req None None "frozen_balance_by_cycle"
        Alpha_context.Delegate.frozen_balance_by_cycle_encoding)
      (Data_encoding.req None None "staking_balance" Alpha_context.Tez.encoding)
      (Data_encoding.req None None "delegated_contracts"
        (Data_encoding.list_value None Alpha_context.Contract.encoding))
      (Data_encoding.req None None "delegated_balance"
        Alpha_context.Tez.encoding)
      (Data_encoding.req None None "deactivated" Data_encoding.bool_value)
      (Data_encoding.req None None "grace_period" Alpha_context.Cycle.encoding)
      (Data_encoding.req None None "voting_power" Data_encoding.int32_value)).

Module S.
  Definition raw_path : RPC_path.path Updater.rpc_context Updater.rpc_context :=
    RPC_path.op_div (RPC_path.op_div RPC_path.open_root "context") "delegates".
  
  Module list_query.
    Record record : Set := Build {
      active : bool;
      inactive : bool }.
    Definition with_active active (r : record) :=
      Build active r.(inactive).
    Definition with_inactive inactive (r : record) :=
      Build r.(active) inactive.
  End list_query.
  Definition list_query := list_query.record.
  
  Definition list_query_value : RPC_query.t list_query :=
    RPC_query.seal
      (RPC_query.op_pipeplus
        (RPC_query.op_pipeplus
          (RPC_query.query_value
            (fun (active : bool) =>
              fun (inactive : bool) =>
                {| list_query.active := active; list_query.inactive := inactive
                  |}))
          (RPC_query.flag None "active"
            (fun (t_value : list_query) => t_value.(list_query.active))))
        (RPC_query.flag None "inactive"
          (fun (t_value : list_query) => t_value.(list_query.inactive)))).
  
  Definition list_delegate
    : RPC_service.service Updater.rpc_context Updater.rpc_context list_query
      unit (list Signature.public_key_hash) :=
    RPC_service.get_service (Some "Lists all registered delegates.")
      list_query_value
      (Data_encoding.list_value None
        Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding))
      raw_path.
  
  Definition path
    : RPC_path.path Updater.rpc_context
      (Updater.rpc_context * Signature.public_key_hash) :=
    RPC_path.op_divcolon raw_path
      Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.rpc_arg).
  
  Definition info_value
    : RPC_service.service Updater.rpc_context
      (Updater.rpc_context * Signature.public_key_hash) unit unit info :=
    RPC_service.get_service (Some "Everything about a delegate.")
      RPC_query.empty info_encoding path.
  
  Definition balance
    : RPC_service.service Updater.rpc_context
      (Updater.rpc_context * Signature.public_key_hash) unit unit
      Alpha_context.Tez.t :=
    RPC_service.get_service
      (Some
        "Returns the full balance of a given delegate, including the frozen balances.")
      RPC_query.empty Alpha_context.Tez.encoding
      (RPC_path.op_div path "balance").
  
  Definition frozen_balance_value
    : RPC_service.service Updater.rpc_context
      (Updater.rpc_context * Signature.public_key_hash) unit unit
      Alpha_context.Tez.t :=
    RPC_service.get_service
      (Some
        "Returns the total frozen balances of a given delegate, this includes the frozen deposits, rewards and fees.")
      RPC_query.empty Alpha_context.Tez.encoding
      (RPC_path.op_div path "frozen_balance").
  
  Definition frozen_balance_by_cycle
    : RPC_service.service Updater.rpc_context
      (Updater.rpc_context * Signature.public_key_hash) unit unit
      (Alpha_context.Cycle.Map.(Map.S.t) Alpha_context.Delegate.frozen_balance) :=
    RPC_service.get_service
      (Some
        "Returns the frozen balances of a given delegate, indexed by the cycle by which it will be unfrozen")
      RPC_query.empty Alpha_context.Delegate.frozen_balance_by_cycle_encoding
      (RPC_path.op_div path "frozen_balance_by_cycle").
  
  Definition staking_balance
    : RPC_service.service Updater.rpc_context
      (Updater.rpc_context * Signature.public_key_hash) unit unit
      Alpha_context.Tez.t :=
    RPC_service.get_service
      (Some
        "Returns the total amount of tokens delegated to a given delegate. This includes the balances of all the contracts that delegate to it, but also the balance of the delegate itself and its frozen fees and deposits. The rewards do not count in the delegated balance until they are unfrozen.")
      RPC_query.empty Alpha_context.Tez.encoding
      (RPC_path.op_div path "staking_balance").
  
  Definition delegated_contracts
    : RPC_service.service Updater.rpc_context
      (Updater.rpc_context * Signature.public_key_hash) unit unit
      (list Alpha_context.Contract.t) :=
    RPC_service.get_service
      (Some "Returns the list of contracts that delegate to a given delegate.")
      RPC_query.empty
      (Data_encoding.list_value None Alpha_context.Contract.encoding)
      (RPC_path.op_div path "delegated_contracts").
  
  Definition delegated_balance
    : RPC_service.service Updater.rpc_context
      (Updater.rpc_context * Signature.public_key_hash) unit unit
      Alpha_context.Tez.t :=
    RPC_service.get_service
      (Some
        "Returns the balances of all the contracts that delegate to a given delegate. This excludes the delegate's own balance and its frozen balances.")
      RPC_query.empty Alpha_context.Tez.encoding
      (RPC_path.op_div path "delegated_balance").
  
  Definition deactivated
    : RPC_service.service Updater.rpc_context
      (Updater.rpc_context * Signature.public_key_hash) unit unit bool :=
    RPC_service.get_service
      (Some
        "Tells whether the delegate is currently tagged as deactivated or not.")
      RPC_query.empty Data_encoding.bool_value
      (RPC_path.op_div path "deactivated").
  
  Definition grace_period
    : RPC_service.service Updater.rpc_context
      (Updater.rpc_context * Signature.public_key_hash) unit unit
      Alpha_context.Cycle.t :=
    RPC_service.get_service
      (Some
        "Returns the cycle by the end of which the delegate might be deactivated if she fails to execute any delegate action. A deactivated delegate might be reactivated (without loosing any rolls) by simply re-registering as a delegate. For deactivated delegates, this value contains the cycle by which they were deactivated.")
      RPC_query.empty Alpha_context.Cycle.encoding
      (RPC_path.op_div path "grace_period").
  
  Definition voting_power
    : RPC_service.service Updater.rpc_context
      (Updater.rpc_context * Signature.public_key_hash) unit unit int32 :=
    RPC_service.get_service
      (Some "The number of rolls in the vote listings for a given delegate")
      RPC_query.empty Data_encoding.int32_value
      (RPC_path.op_div path "voting_power").
End S.

Definition delegate_register (function_parameter : unit) : unit :=
  let '_ := function_parameter in
  let '_ :=
    Services_registration.register0 true S.list_delegate
      (fun (ctxt : Alpha_context.t) =>
        fun (q : S.list_query) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            let= delegates := Alpha_context.Delegate.list_value ctxt in
            match q with
            | {| S.list_query.active := true; S.list_query.inactive := false |}
              =>
              List.filter_es
                (fun (pkh : Signature.public_key_hash) =>
                  Error_monad.op_gtpipeeqquestion
                    (Alpha_context.Delegate.deactivated ctxt pkh) Pervasives.not)
                delegates
            | {| S.list_query.active := false; S.list_query.inactive := true |}
              =>
              List.filter_es
                (fun (pkh : Signature.public_key_hash) =>
                  Alpha_context.Delegate.deactivated ctxt pkh) delegates
            | _ => return=? delegates
            end) in
  let '_ :=
    Services_registration.register1 false S.info_value
      (fun (ctxt : Alpha_context.t) =>
        fun (pkh : Signature.public_key_hash) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              let=? '_ := Alpha_context.Delegate.check_delegate ctxt pkh in
              let=? balance := Alpha_context.Delegate.full_balance ctxt pkh in
              let=? frozen_balance_value :=
                Alpha_context.Delegate.frozen_balance_value ctxt pkh in
              let= frozen_balance_by_cycle :=
                Alpha_context.Delegate.frozen_balance_by_cycle ctxt pkh in
              let=? staking_balance :=
                Alpha_context.Delegate.staking_balance ctxt pkh in
              let= delegated_contracts :=
                Alpha_context.Delegate.delegated_contracts ctxt pkh in
              let=? delegated_balance :=
                Alpha_context.Delegate.delegated_balance ctxt pkh in
              let=? deactivated := Alpha_context.Delegate.deactivated ctxt pkh
                in
              let=? grace_period := Alpha_context.Delegate.grace_period ctxt pkh
                in
              let=? voting_power :=
                Alpha_context.Vote.get_voting_power_free ctxt pkh in
              return=?
                {| info.balance := balance;
                  info.frozen_balance := frozen_balance_value;
                  info.frozen_balance_by_cycle := frozen_balance_by_cycle;
                  info.staking_balance := staking_balance;
                  info.delegated_contracts := delegated_contracts;
                  info.delegated_balance := delegated_balance;
                  info.deactivated := deactivated;
                  info.grace_period := grace_period;
                  info.voting_power := voting_power |}) in
  let '_ :=
    Services_registration.register1 false S.balance
      (fun (ctxt : Alpha_context.t) =>
        fun (pkh : Signature.public_key_hash) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              let=? '_ :=
                Error_monad.trace_value
                  (Build_extensible "Balance_rpc_non_delegate"
                    Signature.public_key_hash pkh)
                  (Alpha_context.Delegate.check_delegate ctxt pkh) in
              Alpha_context.Delegate.full_balance ctxt pkh) in
  let '_ :=
    Services_registration.register1 false S.frozen_balance_value
      (fun (ctxt : Alpha_context.t) =>
        fun (pkh : Signature.public_key_hash) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              let=? '_ := Alpha_context.Delegate.check_delegate ctxt pkh in
              Alpha_context.Delegate.frozen_balance_value ctxt pkh) in
  let '_ :=
    Services_registration.register1 true S.frozen_balance_by_cycle
      (fun (ctxt : Alpha_context.t) =>
        fun (pkh : Signature.public_key_hash) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              let=? '_ := Alpha_context.Delegate.check_delegate ctxt pkh in
              Error_monad.op_gtpipeeq
                (Alpha_context.Delegate.frozen_balance_by_cycle ctxt pkh)
                Error_monad.ok) in
  let '_ :=
    Services_registration.register1 false S.staking_balance
      (fun (ctxt : Alpha_context.t) =>
        fun (pkh : Signature.public_key_hash) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              let=? '_ := Alpha_context.Delegate.check_delegate ctxt pkh in
              Alpha_context.Delegate.staking_balance ctxt pkh) in
  let '_ :=
    Services_registration.register1 true S.delegated_contracts
      (fun (ctxt : Alpha_context.t) =>
        fun (pkh : Signature.public_key_hash) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              let=? '_ := Alpha_context.Delegate.check_delegate ctxt pkh in
              Error_monad.op_gtpipeeq
                (Alpha_context.Delegate.delegated_contracts ctxt pkh)
                Error_monad.ok) in
  let '_ :=
    Services_registration.register1 false S.delegated_balance
      (fun (ctxt : Alpha_context.t) =>
        fun (pkh : Signature.public_key_hash) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              let=? '_ := Alpha_context.Delegate.check_delegate ctxt pkh in
              Alpha_context.Delegate.delegated_balance ctxt pkh) in
  let '_ :=
    Services_registration.register1 false S.deactivated
      (fun (ctxt : Alpha_context.t) =>
        fun (pkh : Signature.public_key_hash) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              let=? '_ := Alpha_context.Delegate.check_delegate ctxt pkh in
              Alpha_context.Delegate.deactivated ctxt pkh) in
  let '_ :=
    Services_registration.register1 false S.grace_period
      (fun (ctxt : Alpha_context.t) =>
        fun (pkh : Signature.public_key_hash) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              let=? '_ := Alpha_context.Delegate.check_delegate ctxt pkh in
              Alpha_context.Delegate.grace_period ctxt pkh) in
  Services_registration.register1 false S.voting_power
    (fun (ctxt : Alpha_context.t) =>
      fun (pkh : Signature.public_key_hash) =>
        fun (function_parameter : unit) =>
          let '_ := function_parameter in
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            let=? '_ := Alpha_context.Delegate.check_delegate ctxt pkh in
            Alpha_context.Vote.get_voting_power_free ctxt pkh).

Definition list_value {A : Set}
  (ctxt : RPC_context.simple A) (block : A) (op_staroptstar : option bool)
  : option bool -> unit ->
  M= (Error_monad.shell_tzresult (list Signature.public_key_hash)) :=
  let active :=
    match op_staroptstar with
    | Some op_starsthstar => op_starsthstar
    | None => true
    end in
  fun (op_staroptstar : option bool) =>
    let inactive :=
      match op_staroptstar with
      | Some op_starsthstar => op_starsthstar
      | None => false
      end in
    fun (function_parameter : unit) =>
      let '_ := function_parameter in
      RPC_context.make_call0 S.list_delegate ctxt block
        {| S.list_query.active := active; S.list_query.inactive := inactive |}
        tt.

Definition info_value {A : Set}
  (ctxt : RPC_context.simple A) (block : A) (pkh : Signature.public_key_hash)
  : M= (Error_monad.shell_tzresult info) :=
  RPC_context.make_call1 S.info_value ctxt block pkh tt tt.

Definition balance {A : Set}
  (ctxt : RPC_context.simple A) (block : A) (pkh : Signature.public_key_hash)
  : M= (Error_monad.shell_tzresult Alpha_context.Tez.t) :=
  RPC_context.make_call1 S.balance ctxt block pkh tt tt.

Definition frozen_balance_value {A : Set}
  (ctxt : RPC_context.simple A) (block : A) (pkh : Signature.public_key_hash)
  : M= (Error_monad.shell_tzresult Alpha_context.Tez.t) :=
  RPC_context.make_call1 S.frozen_balance_value ctxt block pkh tt tt.

Definition frozen_balance_by_cycle {A : Set}
  (ctxt : RPC_context.simple A) (block : A) (pkh : Signature.public_key_hash)
  : M=
    (Error_monad.shell_tzresult
      (Alpha_context.Cycle.Map.(Map.S.t) Alpha_context.Delegate.frozen_balance)) :=
  RPC_context.make_call1 S.frozen_balance_by_cycle ctxt block pkh tt tt.

Definition staking_balance {A : Set}
  (ctxt : RPC_context.simple A) (block : A) (pkh : Signature.public_key_hash)
  : M= (Error_monad.shell_tzresult Alpha_context.Tez.t) :=
  RPC_context.make_call1 S.staking_balance ctxt block pkh tt tt.

Definition delegated_contracts {A : Set}
  (ctxt : RPC_context.simple A) (block : A) (pkh : Signature.public_key_hash)
  : M= (Error_monad.shell_tzresult (list Alpha_context.Contract.t)) :=
  RPC_context.make_call1 S.delegated_contracts ctxt block pkh tt tt.

Definition delegated_balance {A : Set}
  (ctxt : RPC_context.simple A) (block : A) (pkh : Signature.public_key_hash)
  : M= (Error_monad.shell_tzresult Alpha_context.Tez.t) :=
  RPC_context.make_call1 S.delegated_balance ctxt block pkh tt tt.

Definition deactivated {A : Set}
  (ctxt : RPC_context.simple A) (block : A) (pkh : Signature.public_key_hash)
  : M= (Error_monad.shell_tzresult bool) :=
  RPC_context.make_call1 S.deactivated ctxt block pkh tt tt.

Definition grace_period {A : Set}
  (ctxt : RPC_context.simple A) (block : A) (pkh : Signature.public_key_hash)
  : M= (Error_monad.shell_tzresult Alpha_context.Cycle.t) :=
  RPC_context.make_call1 S.grace_period ctxt block pkh tt tt.

Definition voting_power {A : Set}
  (ctxt : RPC_context.simple A) (block : A) (pkh : Signature.public_key_hash)
  : M= (Error_monad.shell_tzresult int32) :=
  RPC_context.make_call1 S.voting_power ctxt block pkh tt tt.

Module Minimal_valid_time.
  Definition minimal_valid_time
    (ctxt : Alpha_context.context) (priority : int) (endorsing_power : int)
    (predecessor_timestamp : Time.t) : M? Time.t :=
    Baking.minimal_valid_time (Alpha_context.Constants.parametric_value ctxt)
      priority endorsing_power predecessor_timestamp.
  
  Module S.
    Module t.
      Record record : Set := Build {
        priority : int;
        endorsing_power : int }.
      Definition with_priority priority (r : record) :=
        Build priority r.(endorsing_power).
      Definition with_endorsing_power endorsing_power (r : record) :=
        Build r.(priority) endorsing_power.
    End t.
    Definition t := t.record.
    
    Definition minimal_valid_time_query : RPC_query.t t :=
      RPC_query.seal
        (RPC_query.op_pipeplus
          (RPC_query.op_pipeplus
            (RPC_query.query_value
              (fun (priority : int) =>
                fun (endorsing_power : int) =>
                  {| t.priority := priority;
                    t.endorsing_power := endorsing_power |}))
            (RPC_query.field_value None "priority" RPC_arg.int_value 0
              (fun (t_value : t) => t_value.(t.priority))))
          (RPC_query.field_value None "endorsing_power" RPC_arg.int_value 0
            (fun (t_value : t) => t_value.(t.endorsing_power)))).
    
    Definition minimal_valid_time
      : RPC_service.service Updater.rpc_context Updater.rpc_context t unit
        Time.t :=
      RPC_service.get_service
        (Some
          "Minimal valid time for a block given a priority and an endorsing power.")
        minimal_valid_time_query Time.encoding
        (RPC_path.op_div RPC_path.open_root "minimal_valid_time").
  End S.
  
  Definition register (function_parameter : unit) : unit :=
    let '_ := function_parameter in
    Services_registration.register0 false S.minimal_valid_time
      (fun (ctxt : Alpha_context.t) =>
        fun (function_parameter : S.t) =>
          let '{|
            S.t.priority := priority;
              S.t.endorsing_power := endorsing_power
              |} := function_parameter in
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            let predecessor_timestamp :=
              Alpha_context.Timestamp.predecessor ctxt in
            return=
              (minimal_valid_time ctxt priority endorsing_power
                predecessor_timestamp)).
  
  Definition get {A : Set}
    (ctxt : RPC_context.simple A) (block : A) (priority : int)
    (endorsing_power : int) : M= (Error_monad.shell_tzresult Time.t) :=
    RPC_context.make_call0 S.minimal_valid_time ctxt block
      {| S.t.priority := priority; S.t.endorsing_power := endorsing_power |} tt.
End Minimal_valid_time.

Definition register (function_parameter : unit) : unit :=
  let '_ := function_parameter in
  let '_ := delegate_register tt in
  Minimal_valid_time.register tt.

Definition minimal_valid_time
  (ctxt : Alpha_context.context) (priority : int) (endorsing_power : int)
  (predecessor_timestamp : Time.t) : M? Time.t :=
  Minimal_valid_time.minimal_valid_time ctxt priority endorsing_power
    predecessor_timestamp.
