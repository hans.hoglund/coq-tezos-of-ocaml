Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Script_typed_ir.

Definition compare_address
  (function_parameter : Alpha_context.Contract.t * string)
  : Alpha_context.Contract.t * string -> int :=
  let '(x, ex) := function_parameter in
  fun (function_parameter : Alpha_context.Contract.t * string) =>
    let '(y, ey) := function_parameter in
    let lres := Alpha_context.Contract.compare x y in
    if lres =i 0 then
      Compare.String.(Compare.S.compare) ex ey
    else
      lres.

Inductive compare_comparable_cont : Set :=
| Compare_comparable : forall {a : Set},
  Script_typed_ir.comparable_ty -> a -> a -> compare_comparable_cont ->
  compare_comparable_cont
| Compare_comparable_return : compare_comparable_cont.

Module Compare_comparable.
  Reserved Notation "'apply".
  
  Fixpoint compare_comparable {a : Set}
    (kind_value : Script_typed_ir.comparable_ty) (k : compare_comparable_cont)
    (x : a) (y : a) {struct kind_value} : int :=
    let apply := 'apply in
    match (kind_value, x, y) with
    | (Script_typed_ir.Unit_key _, _, _) => apply 0 k
    
    | (Script_typed_ir.Signature_key _, x, y) =>
      let '[y, x] :=
        cast [Alpha_context.signature ** Alpha_context.signature] [y, x] in
      apply (Signature.compare x y) k
    
    | (Script_typed_ir.String_key _, x, y) =>
      let '[y, x] :=
        cast [Alpha_context.Script_string.t ** Alpha_context.Script_string.t]
          [y, x] in
      apply (Alpha_context.Script_string.compare x y) k
    
    | (Script_typed_ir.Bool_key _, x, y) =>
      let '[y, x] := cast [bool ** bool] [y, x] in
      apply (Compare.Bool.(Compare.S.compare) x y) k
    
    | (Script_typed_ir.Mutez_key _, x, y) =>
      let '[y, x] := cast [Alpha_context.Tez.t ** Alpha_context.Tez.t] [y, x] in
      apply (Alpha_context.Tez.compare x y) k
    
    | (Script_typed_ir.Key_hash_key _, x, y) =>
      let '[y, x] :=
        cast [Alpha_context.public_key_hash ** Alpha_context.public_key_hash]
          [y, x] in
      apply
        (Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.compare) x y) k
    
    | (Script_typed_ir.Key_key _, x, y) =>
      let '[y, x] :=
        cast [Alpha_context.public_key ** Alpha_context.public_key] [y, x] in
      apply (Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.compare) x y) k
    
    | (Script_typed_ir.Int_key _, x, y) =>
      let '[y, x] :=
        cast [Alpha_context.Script_int.num ** Alpha_context.Script_int.num]
          [y, x] in
      apply (Alpha_context.Script_int.compare x y) k
    
    | (Script_typed_ir.Nat_key _, x, y) =>
      let '[y, x] :=
        cast [Alpha_context.Script_int.num ** Alpha_context.Script_int.num]
          [y, x] in
      apply (Alpha_context.Script_int.compare x y) k
    
    | (Script_typed_ir.Timestamp_key _, x, y) =>
      let '[y, x] :=
        cast
          [Alpha_context.Script_timestamp.t ** Alpha_context.Script_timestamp.t]
          [y, x] in
      apply (Alpha_context.Script_timestamp.compare x y) k
    
    | (Script_typed_ir.Address_key _, x, y) =>
      let '[y, x] :=
        cast [Script_typed_ir.address ** Script_typed_ir.address] [y, x] in
      apply (compare_address x y) k
    
    | (Script_typed_ir.Bytes_key _, x, y) =>
      let '[y, x] := cast [bytes ** bytes] [y, x] in
      apply (Compare.Bytes.(Compare.S.compare) x y) k
    
    | (Script_typed_ir.Chain_id_key _, x, y) =>
      let '[y, x] := cast [Chain_id.t ** Chain_id.t] [y, x] in
      apply (Chain_id.compare x y) k
    
    | (Script_typed_ir.Pair_key (tl, _) (tr, _) _, x, y) =>
      let 'existT _ [__0, __1] [y, x, tr, tl] as exi :=
        cast_exists (Es := [Set ** Set])
          (fun '[__0, __1] =>
            [__0 * __1 ** __0 * __1 ** Script_typed_ir.comparable_ty **
              Script_typed_ir.comparable_ty]) [y, x, tr, tl]
        return
          let fst := projT1 exi in
          let __1 := Primitive.snd fst in
          let __0 := Primitive.fst fst in
          int in
      let '(lx, rx) := x in
      let '(ly, ry) := y in
      compare_comparable tl (Compare_comparable tr rx ry k) lx ly
    
    | (Script_typed_ir.Union_key (tl, _) (tr, _) _, x, y) =>
      let 'existT _ [__2, __3] [y, x, tr, tl] as exi :=
        cast_exists (Es := [Set ** Set])
          (fun '[__2, __3] =>
            [Script_typed_ir.union __2 __3 ** Script_typed_ir.union __2 __3 **
              Script_typed_ir.comparable_ty ** Script_typed_ir.comparable_ty])
          [y, x, tr, tl]
        return
          let fst := projT1 exi in
          let __3 := Primitive.snd fst in
          let __2 := Primitive.fst fst in
          int in
      match (x, y) with
      | (Script_typed_ir.L x, Script_typed_ir.L y) =>
        compare_comparable tl k x y
      | (Script_typed_ir.L _, Script_typed_ir.R _) => (-1)
      | (Script_typed_ir.R _, Script_typed_ir.L _) => 1
      | (Script_typed_ir.R x, Script_typed_ir.R y) =>
        compare_comparable tr k x y
      end
    
    | (Script_typed_ir.Option_key t_value _, x, y) =>
      let 'existT _ __4 [y, x, t_value] as exi :=
        cast_exists (Es := Set)
          (fun __4 =>
            [option __4 ** option __4 ** Script_typed_ir.comparable_ty])
          [y, x, t_value]
        return
          let fst := projT1 exi in
          let __4 := fst in
          int in
      match (x, y) with
      | (None, None) => apply 0 k
      | (None, Some _) => (-1)
      | (Some _, None) => 1
      | (Some x, Some y) => compare_comparable t_value k x y
      end
    | _ => unreachable_gadt_branch
    end
  
  where "'apply" :=
    (fun (ret : int) (k : compare_comparable_cont) =>
      match (ret, k) with
      | (0, Compare_comparable ty x y k) =>
        let 'existT _ __Compare_comparable_'a [k, y, x, ty] as exi :=
          existT (A := Set)
            (fun __Compare_comparable_'a =>
              [compare_comparable_cont ** __Compare_comparable_'a **
                __Compare_comparable_'a ** Script_typed_ir.comparable_ty]) _
            [k, y, x, ty]
          return
            let fst := projT1 exi in
            let __Compare_comparable_'a := fst in
            int in
        compare_comparable ty k x y
      | (0, Compare_comparable_return) => 0
      | (ret, _) =>
        if ret >i 0 then
          1
        else
          (-1)
      end).
  
  Definition apply := 'apply.
End Compare_comparable.

Definition compare_comparable {a : Set}
  (t_value : Script_typed_ir.comparable_ty) : a -> a -> int :=
  Compare_comparable.compare_comparable t_value Compare_comparable_return.
