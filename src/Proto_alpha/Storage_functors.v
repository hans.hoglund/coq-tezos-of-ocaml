Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Gas_limit_repr.
Require TezosOfOCaml.Proto_alpha.Misc.
Require TezosOfOCaml.Proto_alpha.Raw_context.
Require TezosOfOCaml.Proto_alpha.Raw_context_intf.
Require TezosOfOCaml.Proto_alpha.Storage_costs.
Require TezosOfOCaml.Proto_alpha.Storage_description.
Require TezosOfOCaml.Proto_alpha.Storage_sigs.

Module Registered.
  Definition ghost : bool := false.
  
  Definition module :=
    {|
      Storage_sigs.REGISTER.ghost := ghost
    |}.
End Registered.
Definition Registered := Registered.module.

Module Ghost.
  Definition ghost : bool := true.
  
  Definition module :=
    {|
      Storage_sigs.REGISTER.ghost := ghost
    |}.
End Ghost.
Definition Ghost := Ghost.module.

Module ENCODER.
  Record signature {t : Set} : Set := {
    t := t;
    of_bytes : (unit -> list string) -> bytes -> M? t;
    to_bytes : t -> bytes;
  }.
End ENCODER.
Definition ENCODER := @ENCODER.signature.
Arguments ENCODER {_}.

Module Make_encoder.
  Class FArgs {V_t : Set} := {
    V : Storage_sigs.VALUE (t := V_t);
  }.
  Arguments Build_FArgs {_}.
  
  Definition of_bytes `{FArgs}
    (key_value : unit -> list string) (b_value : bytes)
    : M? V.(Storage_sigs.VALUE.t) :=
    match
      Data_encoding.Binary.of_bytes_opt V.(Storage_sigs.VALUE.encoding) b_value
      with
    | None =>
      Error_monad.error_value
        (Build_extensible "Storage_error" Raw_context.storage_error
          (Raw_context.Corrupted_data (key_value tt)))
    | Some v => Pervasives.Ok v
    end.
  
  Definition to_bytes `{FArgs} (v : V.(Storage_sigs.VALUE.t)) : bytes :=
    match
      Data_encoding.Binary.to_bytes_opt None V.(Storage_sigs.VALUE.encoding) v
      with
    | Some b_value => b_value
    | None => Bytes.empty
    end.
  
  Definition functor `{FArgs} :=
    {|
      ENCODER.of_bytes := of_bytes;
      ENCODER.to_bytes := to_bytes
    |}.
End Make_encoder.
Definition Make_encoder {V_t : Set} (V : Storage_sigs.VALUE (t := V_t))
  : ENCODER (t := V.(Storage_sigs.VALUE.t)) :=
  let '_ := Make_encoder.Build_FArgs V in
  Make_encoder.functor.

Definition len_name : string := "len".

Definition data_name : string := "data".

Definition encode_len_value (bytes_value : bytes) : bytes :=
  let length := Bytes.length bytes_value in
  Data_encoding.Binary.to_bytes_exn None Data_encoding.int31 length.

Definition decode_len_value (key_value : list string) (len : bytes) : M? int :=
  match Data_encoding.Binary.of_bytes_opt Data_encoding.int31 len with
  | None =>
    Error_monad.error_value
      (Build_extensible "Storage_error" Raw_context.storage_error
        (Raw_context.Corrupted_data key_value))
  | Some len => return? len
  end.

Module Make_subcontext.
  Class FArgs {C_t : Set} := {
    R : Storage_sigs.REGISTER;
    C : Raw_context.T (t := C_t);
    N : Storage_sigs.NAME;
  }.
  Arguments Build_FArgs {_}.
  
  Definition t `{FArgs} : Set := C.(Raw_context_intf.T.t).
  
  Definition to_key `{FArgs} (k : list string) : list string :=
    Pervasives.op_at N.(Storage_sigs.NAME.name) k.
  
  Definition mem `{FArgs} (t_value : C.(Raw_context_intf.T.t)) (k : list string)
    : M= bool := C.(Raw_context_intf.T.mem) t_value (to_key k).
  
  Definition mem_tree `{FArgs}
    (t_value : C.(Raw_context_intf.T.t)) (k : list string) : M= bool :=
    C.(Raw_context_intf.T.mem_tree) t_value (to_key k).
  
  Definition get `{FArgs} (t_value : C.(Raw_context_intf.T.t)) (k : list string)
    : M=? Raw_context.value := C.(Raw_context_intf.T.get) t_value (to_key k).
  
  Definition get_tree `{FArgs}
    (t_value : C.(Raw_context_intf.T.t)) (k : list string)
    : M=? Raw_context.tree :=
    C.(Raw_context_intf.T.get_tree) t_value (to_key k).
  
  Definition find `{FArgs}
    (t_value : C.(Raw_context_intf.T.t)) (k : list string)
    : M= (option Raw_context.value) :=
    C.(Raw_context_intf.T.find) t_value (to_key k).
  
  Definition find_tree `{FArgs}
    (t_value : C.(Raw_context_intf.T.t)) (k : list string)
    : M= (option Raw_context.tree) :=
    C.(Raw_context_intf.T.find_tree) t_value (to_key k).
  
  Definition add `{FArgs}
    (t_value : C.(Raw_context_intf.T.t)) (k : list string)
    (v : Raw_context.value) : M= C.(Raw_context_intf.T.t) :=
    C.(Raw_context_intf.T.add) t_value (to_key k) v.
  
  Definition add_tree `{FArgs}
    (t_value : C.(Raw_context_intf.T.t)) (k : list string)
    (v : Raw_context.tree) : M= C.(Raw_context_intf.T.t) :=
    C.(Raw_context_intf.T.add_tree) t_value (to_key k) v.
  
  Definition init_value `{FArgs}
    (t_value : C.(Raw_context_intf.T.t)) (k : list string)
    (v : Raw_context.value) : M=? C.(Raw_context_intf.T.t) :=
    C.(Raw_context_intf.T.init_value) t_value (to_key k) v.
  
  Definition init_tree `{FArgs}
    (t_value : C.(Raw_context_intf.T.t)) (k : list string)
    (v : Raw_context.tree) : M=? C.(Raw_context_intf.T.t) :=
    C.(Raw_context_intf.T.init_tree) t_value (to_key k) v.
  
  Definition update `{FArgs}
    (t_value : C.(Raw_context_intf.T.t)) (k : list string)
    (v : Raw_context.value) : M=? C.(Raw_context_intf.T.t) :=
    C.(Raw_context_intf.T.update) t_value (to_key k) v.
  
  Definition update_tree `{FArgs}
    (t_value : C.(Raw_context_intf.T.t)) (k : list string)
    (v : Raw_context.tree) : M=? C.(Raw_context_intf.T.t) :=
    C.(Raw_context_intf.T.update_tree) t_value (to_key k) v.
  
  Definition add_or_remove `{FArgs}
    (t_value : C.(Raw_context_intf.T.t)) (k : list string)
    (v : option Raw_context.value) : M= C.(Raw_context_intf.T.t) :=
    C.(Raw_context_intf.T.add_or_remove) t_value (to_key k) v.
  
  Definition add_or_remove_tree `{FArgs}
    (t_value : C.(Raw_context_intf.T.t)) (k : list string)
    (v : option Raw_context.tree) : M= C.(Raw_context_intf.T.t) :=
    C.(Raw_context_intf.T.add_or_remove_tree) t_value (to_key k) v.
  
  Definition remove_existing `{FArgs}
    (t_value : C.(Raw_context_intf.T.t)) (k : list string)
    : M=? C.(Raw_context_intf.T.t) :=
    C.(Raw_context_intf.T.remove_existing) t_value (to_key k).
  
  Definition remove_existing_tree `{FArgs}
    (t_value : C.(Raw_context_intf.T.t)) (k : list string)
    : M=? C.(Raw_context_intf.T.t) :=
    C.(Raw_context_intf.T.remove_existing_tree) t_value (to_key k).
  
  Definition remove `{FArgs}
    (t_value : C.(Raw_context_intf.T.t)) (k : list string)
    : M= C.(Raw_context_intf.T.t) :=
    C.(Raw_context_intf.T.remove) t_value (to_key k).
  
  Definition list_value `{FArgs}
    (t_value : C.(Raw_context_intf.T.t)) (offset : option int)
    (length : option int) (k : list string)
    : M= (list (string * Raw_context.tree)) :=
    C.(Raw_context_intf.T.list_value) t_value offset length (to_key k).
  
  Definition fold `{FArgs} {A : Set}
    (depth : option Context.depth) (t_value : C.(Raw_context_intf.T.t))
    (k : list string) (init_value : A)
    (f : Raw_context.key -> Raw_context.tree -> A -> M= A) : M= A :=
    C.(Raw_context_intf.T.fold) depth t_value (to_key k) init_value f.
  
  Definition Tree `{FArgs} := C.(Raw_context_intf.T.Tree).
  
  Definition project `{FArgs} : C.(Raw_context_intf.T.t) -> Raw_context.root :=
    C.(Raw_context_intf.T.project).
  
  Definition absolute_key `{FArgs}
    (c : C.(Raw_context_intf.T.t)) (k : list string) : Raw_context.key :=
    C.(Raw_context_intf.T.absolute_key) c (to_key k).
  
  Definition consume_gas `{FArgs}
    : C.(Raw_context_intf.T.t) -> Gas_limit_repr.cost ->
    M? C.(Raw_context_intf.T.t) := C.(Raw_context_intf.T.consume_gas).
  
  Definition check_enough_gas `{FArgs}
    : C.(Raw_context_intf.T.t) -> Gas_limit_repr.cost -> M? unit :=
    C.(Raw_context_intf.T.check_enough_gas).
  
  Definition description `{FArgs}
    : Storage_description.t C.(Raw_context_intf.T.t) :=
    let description :=
      if R.(Storage_sigs.REGISTER.ghost) then
        Storage_description.create tt
      else
        C.(Raw_context_intf.T.description) in
    Storage_description.register_named_subcontext description
      N.(Storage_sigs.NAME.name).
  
  Definition functor `{FArgs} :=
    {|
      Raw_context_intf.T.mem := mem;
      Raw_context_intf.T.mem_tree := mem_tree;
      Raw_context_intf.T.get := get;
      Raw_context_intf.T.get_tree := get_tree;
      Raw_context_intf.T.find := find;
      Raw_context_intf.T.find_tree := find_tree;
      Raw_context_intf.T.list_value := list_value;
      Raw_context_intf.T.init_value := init_value;
      Raw_context_intf.T.init_tree := init_tree;
      Raw_context_intf.T.update := update;
      Raw_context_intf.T.update_tree := update_tree;
      Raw_context_intf.T.add := add;
      Raw_context_intf.T.add_tree := add_tree;
      Raw_context_intf.T.remove := remove;
      Raw_context_intf.T.remove_existing := remove_existing;
      Raw_context_intf.T.remove_existing_tree := remove_existing_tree;
      Raw_context_intf.T.add_or_remove := add_or_remove;
      Raw_context_intf.T.add_or_remove_tree := add_or_remove_tree;
      Raw_context_intf.T.fold _ := fold;
      Raw_context_intf.T.Tree := Tree;
      Raw_context_intf.T.project := project;
      Raw_context_intf.T.absolute_key := absolute_key;
      Raw_context_intf.T.consume_gas := consume_gas;
      Raw_context_intf.T.check_enough_gas := check_enough_gas;
      Raw_context_intf.T.description := description
    |}.
End Make_subcontext.
Definition Make_subcontext {C_t : Set}
  (R : Storage_sigs.REGISTER) (C : Raw_context.T (t := C_t))
  (N : Storage_sigs.NAME) : Raw_context.T (t := C.(Raw_context_intf.T.t)) :=
  let '_ := Make_subcontext.Build_FArgs R C N in
  Make_subcontext.functor.

Module Make_single_data_storage.
  Class FArgs {C_t V_t : Set} := {
    R : Storage_sigs.REGISTER;
    C : Raw_context.T (t := C_t);
    N : Storage_sigs.NAME;
    V : Storage_sigs.VALUE (t := V_t);
  }.
  Arguments Build_FArgs {_ _}.
  
  Definition t `{FArgs} : Set := C.(Raw_context_intf.T.t).
  
  Definition context `{FArgs} : Set := t.
  
  Definition value `{FArgs} : Set := V.(Storage_sigs.VALUE.t).
  
  Definition mem `{FArgs} (t_value : C.(Raw_context_intf.T.t)) : M= bool :=
    C.(Raw_context_intf.T.mem) t_value N.(Storage_sigs.NAME.name).
  
  Definition Make_encoder_include `{FArgs} := Make_encoder V.
  
  (** Inclusion of the module [Make_encoder_include] *)
  Definition of_bytes `{FArgs} := Make_encoder_include.(ENCODER.of_bytes).
  
  Definition to_bytes `{FArgs} := Make_encoder_include.(ENCODER.to_bytes).
  
  Definition get `{FArgs} (t_value : C.(Raw_context_intf.T.t))
    : M=? V.(Storage_sigs.VALUE.t) :=
    let=? b_value :=
      C.(Raw_context_intf.T.get) t_value N.(Storage_sigs.NAME.name) in
    let key_value (function_parameter : unit) : Raw_context.key :=
      let '_ := function_parameter in
      C.(Raw_context_intf.T.absolute_key) t_value N.(Storage_sigs.NAME.name) in
    return= (of_bytes key_value b_value).
  
  Definition find `{FArgs} (t_value : C.(Raw_context_intf.T.t))
    : M=? (option V.(Storage_sigs.VALUE.t)) :=
    let= function_parameter :=
      C.(Raw_context_intf.T.find) t_value N.(Storage_sigs.NAME.name) in
    match function_parameter with
    | None => return= Error_monad.ok_none
    | Some b_value =>
      return=
        (let key_value (function_parameter : unit) : Raw_context.key :=
          let '_ := function_parameter in
          C.(Raw_context_intf.T.absolute_key) t_value N.(Storage_sigs.NAME.name)
          in
        let? v := of_bytes key_value b_value in
        return? (Some v))
    end.
  
  Definition init_value `{FArgs}
    (t_value : C.(Raw_context_intf.T.t)) (v : V.(Storage_sigs.VALUE.t))
    : M=? Raw_context.root :=
    let=? t_value :=
      C.(Raw_context_intf.T.init_value) t_value N.(Storage_sigs.NAME.name)
        (to_bytes v) in
    return=? (C.(Raw_context_intf.T.project) t_value).
  
  Definition update `{FArgs}
    (t_value : C.(Raw_context_intf.T.t)) (v : V.(Storage_sigs.VALUE.t))
    : M=? Raw_context.root :=
    let=? t_value :=
      C.(Raw_context_intf.T.update) t_value N.(Storage_sigs.NAME.name)
        (to_bytes v) in
    return=? (C.(Raw_context_intf.T.project) t_value).
  
  Definition add `{FArgs}
    (t_value : C.(Raw_context_intf.T.t)) (v : V.(Storage_sigs.VALUE.t))
    : M= Raw_context.root :=
    let= t_value :=
      C.(Raw_context_intf.T.add) t_value N.(Storage_sigs.NAME.name) (to_bytes v)
      in
    return= (C.(Raw_context_intf.T.project) t_value).
  
  Definition add_or_remove `{FArgs}
    (t_value : C.(Raw_context_intf.T.t)) (v : option V.(Storage_sigs.VALUE.t))
    : M= Raw_context.root :=
    let= t_value :=
      C.(Raw_context_intf.T.add_or_remove) t_value N.(Storage_sigs.NAME.name)
        (Option.map to_bytes v) in
    return= (C.(Raw_context_intf.T.project) t_value).
  
  Definition remove `{FArgs} (t_value : C.(Raw_context_intf.T.t))
    : M= Raw_context.root :=
    let= t_value :=
      C.(Raw_context_intf.T.remove) t_value N.(Storage_sigs.NAME.name) in
    return= (C.(Raw_context_intf.T.project) t_value).
  
  Definition remove_existing `{FArgs} (t_value : C.(Raw_context_intf.T.t))
    : M=? Raw_context.root :=
    let=? t_value :=
      C.(Raw_context_intf.T.remove_existing) t_value N.(Storage_sigs.NAME.name)
      in
    return=? (C.(Raw_context_intf.T.project) t_value).
  
  Definition functor `{FArgs} :=
    {|
      Storage_sigs.Single_data_storage.mem := mem;
      Storage_sigs.Single_data_storage.get := get;
      Storage_sigs.Single_data_storage.find := find;
      Storage_sigs.Single_data_storage.init_value := init_value;
      Storage_sigs.Single_data_storage.update := update;
      Storage_sigs.Single_data_storage.add := add;
      Storage_sigs.Single_data_storage.add_or_remove := add_or_remove;
      Storage_sigs.Single_data_storage.remove_existing := remove_existing;
      Storage_sigs.Single_data_storage.remove := remove
    |}.
End Make_single_data_storage.
Definition Make_single_data_storage {C_t V_t : Set}
  (R : Storage_sigs.REGISTER) (C : Raw_context.T (t := C_t))
  (N : Storage_sigs.NAME) (V : Storage_sigs.VALUE (t := V_t))
  : Storage_sigs.Single_data_storage (t := C.(Raw_context_intf.T.t))
    (value := V.(Storage_sigs.VALUE.t)) :=
  let '_ := Make_single_data_storage.Build_FArgs R C N V in
  Make_single_data_storage.functor.

Module INDEX.
  Record signature {t : Set} {ipath : Set -> Set} : Set := {
    t := t;
    to_path : t -> list string -> list string;
    of_path : list string -> option t;
    path_length : int;
    ipath := ipath;
    args : Storage_description.args;
  }.
End INDEX.
Definition INDEX := @INDEX.signature.
Arguments INDEX {_ _}.

Module Pair.
  Class FArgs {I1_t : Set} {I1_ipath : Set -> Set} {I2_t : Set}
    {I2_ipath : Set -> Set} := {
    I1 : INDEX (t := I1_t) (ipath := I1_ipath);
    I2 : INDEX (t := I2_t) (ipath := I2_ipath);
  }.
  Arguments Build_FArgs {_ _ _ _}.
  
  Definition t `{FArgs} : Set := I1.(INDEX.t) * I2.(INDEX.t).
  
  Definition path_length `{FArgs} : int :=
    I1.(INDEX.path_length) +i I2.(INDEX.path_length).
  
  Definition to_path `{FArgs} (function_parameter : I1.(INDEX.t) * I2.(INDEX.t))
    : list string -> list string :=
    let '(x, y) := function_parameter in
    fun (l_value : list string) =>
      I1.(INDEX.to_path) x (I2.(INDEX.to_path) y l_value).
  
  Definition of_path `{FArgs} (l_value : list string)
    : option (I1.(INDEX.t) * I2.(INDEX.t)) :=
    match Misc.take I1.(INDEX.path_length) l_value with
    | None => None
    | Some (l1, l2) =>
      match ((I1.(INDEX.of_path) l1), (I2.(INDEX.of_path) l2)) with
      | (Some x, Some y) => Some (x, y)
      | _ => None
      end
    end.
  
  Definition ipath `{FArgs} (a : Set) : Set :=
    I2.(INDEX.ipath) (I1.(INDEX.ipath) a).
  
  Definition args `{FArgs} : Storage_description.args :=
    Storage_description.Pair I1.(INDEX.args) I2.(INDEX.args).
  
  Definition functor `{FArgs} : INDEX.signature (ipath := ipath) :=
    {|
      INDEX.to_path := to_path;
      INDEX.of_path := of_path;
      INDEX.path_length := path_length;
      INDEX.args := args
    |}.
End Pair.
Definition Pair
  {I1_t : Set} {I1_ipath : Set -> Set} {I2_t : Set} {I2_ipath : Set -> Set}
  (I1 : INDEX (t := I1_t) (ipath := I1_ipath))
  (I2 : INDEX (t := I2_t) (ipath := I2_ipath))
  : INDEX (t := I1.(INDEX.t) * I2.(INDEX.t)) (ipath := _) :=
  let '_ := Pair.Build_FArgs I1 I2 in
  Pair.functor.

Module Make_data_set_storage.
  Class FArgs {C_t I_t : Set} {I_ipath : Set -> Set} := {
    C : Raw_context.T (t := C_t);
    I : INDEX (t := I_t) (ipath := I_ipath);
  }.
  Arguments Build_FArgs {_ _ _}.
  
  Definition t `{FArgs} : Set := C.(Raw_context_intf.T.t).
  
  Definition context `{FArgs} : Set := t.
  
  Definition elt `{FArgs} : Set := I.(INDEX.t).
  
  Definition inited `{FArgs} : bytes := Bytes.of_string "inited".
  
  Definition mem `{FArgs} (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    : M= bool := C.(Raw_context_intf.T.mem) s (I.(INDEX.to_path) i nil).
  
  Definition add `{FArgs} (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    : M= Raw_context.root :=
    let= t_value :=
      C.(Raw_context_intf.T.add) s (I.(INDEX.to_path) i nil) inited in
    return= (C.(Raw_context_intf.T.project) t_value).
  
  Definition remove `{FArgs} (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    : M= Raw_context.root :=
    let= t_value := C.(Raw_context_intf.T.remove) s (I.(INDEX.to_path) i nil) in
    return= (C.(Raw_context_intf.T.project) t_value).
  
  Definition clear `{FArgs} (s : C.(Raw_context_intf.T.t))
    : M= Raw_context.root :=
    let= t_value := C.(Raw_context_intf.T.remove) s nil in
    return= (C.(Raw_context_intf.T.project) t_value).
  
  Definition fold `{FArgs} {A : Set}
    (s : C.(Raw_context_intf.T.t)) (init_value : A)
    (f : I.(INDEX.t) -> A -> M= A) : M= A :=
    C.(Raw_context_intf.T.fold) (Some (Context.Eq I.(INDEX.path_length))) s nil
      init_value
      (fun (file : Raw_context.key) =>
        fun (tree : Raw_context.tree) =>
          fun (acc_value : A) =>
            match
              C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.kind_value)
                tree with
            | Context.Kind.Value =>
              match I.(INDEX.of_path) file with
              | None =>
                (* ❌ Assert instruction is not handled. *)
                assert (M= _) false
              | Some p_value => f p_value acc_value
              end
            | Context.Kind.Tree => return= acc_value
            end).
  
  Definition elements `{FArgs} (s : C.(Raw_context_intf.T.t))
    : M= (list I.(INDEX.t)) :=
    fold s nil
      (fun (p_value : I.(INDEX.t)) =>
        fun (acc_value : list I.(INDEX.t)) => return= (cons p_value acc_value)).
  
  Definition functor `{FArgs} :=
    {|
      Storage_sigs.Data_set_storage.mem := mem;
      Storage_sigs.Data_set_storage.add := add;
      Storage_sigs.Data_set_storage.remove := remove;
      Storage_sigs.Data_set_storage.elements := elements;
      Storage_sigs.Data_set_storage.fold _ := fold;
      Storage_sigs.Data_set_storage.clear := clear
    |}.
End Make_data_set_storage.
Definition Make_data_set_storage {C_t I_t : Set} {I_ipath : Set -> Set}
  (C : Raw_context.T (t := C_t)) (I : INDEX (t := I_t) (ipath := I_ipath))
  : Storage_sigs.Data_set_storage (t := C.(Raw_context_intf.T.t))
    (elt := I.(INDEX.t)) :=
  let '_ := Make_data_set_storage.Build_FArgs C I in
  Make_data_set_storage.functor.

Module Make_indexed_data_storage.
  Class FArgs {C_t I_t : Set} {I_ipath : Set -> Set} {V_t : Set} := {
    C : Raw_context.T (t := C_t);
    I : INDEX (t := I_t) (ipath := I_ipath);
    V : Storage_sigs.VALUE (t := V_t);
  }.
  Arguments Build_FArgs {_ _ _ _}.
  
  Definition t `{FArgs} : Set := C.(Raw_context_intf.T.t).
  
  Definition context `{FArgs} : Set := t.
  
  Definition key `{FArgs} : Set := I.(INDEX.t).
  
  Definition value `{FArgs} : Set := V.(Storage_sigs.VALUE.t).
  
  Definition Make_encoder_include `{FArgs} := Make_encoder V.
  
  (** Inclusion of the module [Make_encoder_include] *)
  Definition of_bytes `{FArgs} := Make_encoder_include.(ENCODER.of_bytes).
  
  Definition to_bytes `{FArgs} := Make_encoder_include.(ENCODER.to_bytes).
  
  Definition mem `{FArgs} (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    : M= bool := C.(Raw_context_intf.T.mem) s (I.(INDEX.to_path) i nil).
  
  Definition get `{FArgs} (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    : M=? V.(Storage_sigs.VALUE.t) :=
    let=? b_value := C.(Raw_context_intf.T.get) s (I.(INDEX.to_path) i nil) in
    let key_value (function_parameter : unit) : Raw_context.key :=
      let '_ := function_parameter in
      C.(Raw_context_intf.T.absolute_key) s (I.(INDEX.to_path) i nil) in
    return= (of_bytes key_value b_value).
  
  Definition find `{FArgs} (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    : M=? (option V.(Storage_sigs.VALUE.t)) :=
    let= function_parameter :=
      C.(Raw_context_intf.T.find) s (I.(INDEX.to_path) i nil) in
    match function_parameter with
    | None => return= Error_monad.ok_none
    | Some b_value =>
      return=
        (let key_value (function_parameter : unit) : Raw_context.key :=
          let '_ := function_parameter in
          C.(Raw_context_intf.T.absolute_key) s (I.(INDEX.to_path) i nil) in
        let? v := of_bytes key_value b_value in
        return? (Some v))
    end.
  
  Definition update `{FArgs}
    (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    (v : V.(Storage_sigs.VALUE.t)) : M=? Raw_context.root :=
    let=? t_value :=
      C.(Raw_context_intf.T.update) s (I.(INDEX.to_path) i nil) (to_bytes v) in
    return=? (C.(Raw_context_intf.T.project) t_value).
  
  Definition init_value `{FArgs}
    (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    (v : V.(Storage_sigs.VALUE.t)) : M=? Raw_context.root :=
    let=? t_value :=
      C.(Raw_context_intf.T.init_value) s (I.(INDEX.to_path) i nil) (to_bytes v)
      in
    return=? (C.(Raw_context_intf.T.project) t_value).
  
  Definition add `{FArgs}
    (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    (v : V.(Storage_sigs.VALUE.t)) : M= Raw_context.root :=
    let= t_value :=
      C.(Raw_context_intf.T.add) s (I.(INDEX.to_path) i nil) (to_bytes v) in
    return= (C.(Raw_context_intf.T.project) t_value).
  
  Definition add_or_remove `{FArgs}
    (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    (v : option V.(Storage_sigs.VALUE.t)) : M= Raw_context.root :=
    let= t_value :=
      C.(Raw_context_intf.T.add_or_remove) s (I.(INDEX.to_path) i nil)
        (Option.map to_bytes v) in
    return= (C.(Raw_context_intf.T.project) t_value).
  
  Definition remove `{FArgs} (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    : M= Raw_context.root :=
    let= t_value := C.(Raw_context_intf.T.remove) s (I.(INDEX.to_path) i nil) in
    return= (C.(Raw_context_intf.T.project) t_value).
  
  Definition remove_existing `{FArgs}
    (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t)) : M=? Raw_context.root :=
    let=? t_value :=
      C.(Raw_context_intf.T.remove_existing) s (I.(INDEX.to_path) i nil) in
    return=? (C.(Raw_context_intf.T.project) t_value).
  
  Definition clear `{FArgs} (s : C.(Raw_context_intf.T.t))
    : M= Raw_context.root :=
    let= t_value := C.(Raw_context_intf.T.remove) s nil in
    return= (C.(Raw_context_intf.T.project) t_value).
  
  Definition fold `{FArgs} {A : Set}
    (s : C.(Raw_context_intf.T.t)) (init_value : A)
    (f : I.(INDEX.t) -> V.(Storage_sigs.VALUE.t) -> A -> M= A) : M= A :=
    C.(Raw_context_intf.T.fold) (Some (Context.Eq I.(INDEX.path_length))) s nil
      init_value
      (fun (file : Raw_context.key) =>
        fun (tree : Raw_context.tree) =>
          fun (acc_value : A) =>
            let= function_parameter :=
              C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.to_value) tree
              in
            match function_parameter with
            | Some v =>
              match I.(INDEX.of_path) file with
              | None =>
                (* ❌ Assert instruction is not handled. *)
                assert (M= _) false
              | Some path =>
                let key_value (function_parameter : unit) : Raw_context.key :=
                  let '_ := function_parameter in
                  C.(Raw_context_intf.T.absolute_key) s file in
                match of_bytes key_value v with
                | Pervasives.Ok v => f path v acc_value
                | Pervasives.Error _ => return= acc_value
                end
              end
            | None => return= acc_value
            end).
  
  Definition fold_keys `{FArgs} {A : Set}
    (s : C.(Raw_context_intf.T.t)) (init_value : A)
    (f : I.(INDEX.t) -> A -> M= A) : M= A :=
    fold s init_value
      (fun (k : I.(INDEX.t)) =>
        fun (function_parameter : V.(Storage_sigs.VALUE.t)) =>
          let '_ := function_parameter in
          fun (acc_value : A) => f k acc_value).
  
  Definition bindings `{FArgs} (s : C.(Raw_context_intf.T.t))
    : M= (list (I.(INDEX.t) * V.(Storage_sigs.VALUE.t))) :=
    fold s nil
      (fun (p_value : I.(INDEX.t)) =>
        fun (v : V.(Storage_sigs.VALUE.t)) =>
          fun (acc_value : list (I.(INDEX.t) * V.(Storage_sigs.VALUE.t))) =>
            return= (cons (p_value, v) acc_value)).
  
  Definition keys `{FArgs} (s : C.(Raw_context_intf.T.t))
    : M= (list I.(INDEX.t)) :=
    fold_keys s nil
      (fun (p_value : I.(INDEX.t)) =>
        fun (acc_value : list I.(INDEX.t)) => return= (cons p_value acc_value)).
  
  Definition functor `{FArgs} :=
    {|
      Storage_sigs.Indexed_data_storage.mem := mem;
      Storage_sigs.Indexed_data_storage.get := get;
      Storage_sigs.Indexed_data_storage.find := find;
      Storage_sigs.Indexed_data_storage.update := update;
      Storage_sigs.Indexed_data_storage.init_value := init_value;
      Storage_sigs.Indexed_data_storage.add := add;
      Storage_sigs.Indexed_data_storage.add_or_remove := add_or_remove;
      Storage_sigs.Indexed_data_storage.remove_existing := remove_existing;
      Storage_sigs.Indexed_data_storage.remove := remove;
      Storage_sigs.Indexed_data_storage.clear := clear;
      Storage_sigs.Indexed_data_storage.keys := keys;
      Storage_sigs.Indexed_data_storage.bindings := bindings;
      Storage_sigs.Indexed_data_storage.fold _ := fold;
      Storage_sigs.Indexed_data_storage.fold_keys _ := fold_keys
    |}.
End Make_indexed_data_storage.
Definition Make_indexed_data_storage
  {C_t I_t : Set} {I_ipath : Set -> Set} {V_t : Set}
  (C : Raw_context.T (t := C_t)) (I : INDEX (t := I_t) (ipath := I_ipath))
  (V : Storage_sigs.VALUE (t := V_t))
  : Storage_sigs.Indexed_data_storage (t := C.(Raw_context_intf.T.t))
    (key := I.(INDEX.t)) (value := V.(Storage_sigs.VALUE.t)) :=
  let '_ := Make_indexed_data_storage.Build_FArgs C I V in
  Make_indexed_data_storage.functor.

Module Make_indexed_carbonated_data_storage_INTERNAL.
  Class FArgs {C_t I_t : Set} {I_ipath : Set -> Set} {V_t : Set} := {
    C : Raw_context.T (t := C_t);
    I : INDEX (t := I_t) (ipath := I_ipath);
    V : Storage_sigs.VALUE (t := V_t);
  }.
  Arguments Build_FArgs {_ _ _ _}.
  
  Definition t `{FArgs} : Set := C.(Raw_context_intf.T.t).
  
  Definition context `{FArgs} : Set := t.
  
  Definition key `{FArgs} : Set := I.(INDEX.t).
  
  Definition value `{FArgs} : Set := V.(Storage_sigs.VALUE.t).
  
  Definition Make_encoder_include `{FArgs} := Make_encoder V.
  
  (** Inclusion of the module [Make_encoder_include] *)
  Definition of_bytes `{FArgs} := Make_encoder_include.(ENCODER.of_bytes).
  
  Definition to_bytes `{FArgs} := Make_encoder_include.(ENCODER.to_bytes).
  
  Definition data_key `{FArgs} (i : I.(INDEX.t)) : list string :=
    I.(INDEX.to_path) i [ data_name ].
  
  Definition len_key `{FArgs} (i : I.(INDEX.t)) : list string :=
    I.(INDEX.to_path) i [ len_name ].
  
  Definition consume_mem_gas `{FArgs} {A : Set}
    (c : C.(Raw_context_intf.T.t)) (key_value : list A)
    : M? C.(Raw_context_intf.T.t) :=
    C.(Raw_context_intf.T.consume_gas) c
      (Storage_costs.read_access (List.length key_value) 0).
  
  Definition existing_size `{FArgs}
    (c : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t)) : M=? (int * bool) :=
    let= function_parameter := C.(Raw_context_intf.T.find) c (len_key i) in
    match function_parameter with
    | None => return=? (0, false)
    | Some len =>
      return=
        (let? len := decode_len_value (len_key i) len in
        return? (len, true))
    end.
  
  Definition consume_read_gas `{FArgs}
    (get : C.(Raw_context_intf.T.t) -> list string -> M=? bytes)
    (c : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    : M=? C.(Raw_context_intf.T.t) :=
    let len_key := len_key i in
    let=? len := get c len_key in
    return=
      (let? read_bytes := decode_len_value len_key len in
      let cost := Storage_costs.read_access (List.length len_key) read_bytes in
      C.(Raw_context_intf.T.consume_gas) c cost).
  
  Definition consume_serialize_write_gas `{FArgs} {A : Set}
    (set : C.(Raw_context_intf.T.t) -> list string -> bytes -> M=? A)
    (c : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    (v : V.(Storage_sigs.VALUE.t)) : M=? (A * bytes) :=
    let bytes_value := to_bytes v in
    let len := Bytes.length bytes_value in
    let=? c :=
      return=
        (C.(Raw_context_intf.T.consume_gas) c
          (Gas_limit_repr.alloc_mbytes_cost len)) in
    let cost := Storage_costs.write_access len in
    let=? c := return= (C.(Raw_context_intf.T.consume_gas) c cost) in
    let=? c := set c (len_key i) (encode_len_value bytes_value) in
    return=? (c, bytes_value).
  
  Definition consume_remove_gas `{FArgs} {A : Set}
    (del : C.(Raw_context_intf.T.t) -> list string -> M=? A)
    (c : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t)) : M=? A :=
    let=? c :=
      return=
        (C.(Raw_context_intf.T.consume_gas) c (Storage_costs.write_access 0)) in
    del c (len_key i).
  
  Definition mem `{FArgs} (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    : M=? (Raw_context.root * bool) :=
    let key_value := data_key i in
    let=? s := return= (consume_mem_gas s key_value) in
    let= _exists := C.(Raw_context_intf.T.mem) s key_value in
    return=? ((C.(Raw_context_intf.T.project) s), _exists).
  
  Definition get_unprojected `{FArgs}
    (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    : M=? (C.(Raw_context_intf.T.t) * V.(Storage_sigs.VALUE.t)) :=
    let=? s := consume_read_gas C.(Raw_context_intf.T.get) s i in
    let=? b_value := C.(Raw_context_intf.T.get) s (data_key i) in
    let key_value (function_parameter : unit) : Raw_context.key :=
      let '_ := function_parameter in
      C.(Raw_context_intf.T.absolute_key) s (data_key i) in
    return=
      (let? v := of_bytes key_value b_value in
      return? (s, v)).
  
  Definition get `{FArgs} (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    : M=? (Raw_context.root * V.(Storage_sigs.VALUE.t)) :=
    let=? '(s, v) := get_unprojected s i in
    return=? ((C.(Raw_context_intf.T.project) s), v).
  
  Definition find `{FArgs} (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    : M=? (Raw_context.root * option V.(Storage_sigs.VALUE.t)) :=
    let key_value := data_key i in
    let=? s := return= (consume_mem_gas s key_value) in
    let= _exists := C.(Raw_context_intf.T.mem) s key_value in
    if _exists then
      let=? '(s, v) := get s i in
      return=? (s, (Some v))
    else
      return=? ((C.(Raw_context_intf.T.project) s), None).
  
  Definition update `{FArgs}
    (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    (v : V.(Storage_sigs.VALUE.t)) : M=? (Raw_context.root * int) :=
    let=? '(prev_size, _) := existing_size s i in
    let=? '(s, bytes_value) :=
      consume_serialize_write_gas C.(Raw_context_intf.T.update) s i v in
    let=? t_value := C.(Raw_context_intf.T.update) s (data_key i) bytes_value in
    let size_diff := (Bytes.length bytes_value) -i prev_size in
    return=? ((C.(Raw_context_intf.T.project) t_value), size_diff).
  
  Definition init_value `{FArgs}
    (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    (v : V.(Storage_sigs.VALUE.t)) : M=? (Raw_context.root * int) :=
    let=? '(s, bytes_value) :=
      consume_serialize_write_gas C.(Raw_context_intf.T.init_value) s i v in
    let=? t_value :=
      C.(Raw_context_intf.T.init_value) s (data_key i) bytes_value in
    let size_value := Bytes.length bytes_value in
    return=? ((C.(Raw_context_intf.T.project) t_value), size_value).
  
  Definition add `{FArgs}
    (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    (v : V.(Storage_sigs.VALUE.t)) : M=? (Raw_context.root * int * bool) :=
    let add {A : Set}
      (s : C.(Raw_context_intf.T.t)) (i : Raw_context.key)
      (v : Raw_context.value)
      : M= (Pervasives.result C.(Raw_context_intf.T.t) A) :=
      Error_monad.op_gtpipeeq (C.(Raw_context_intf.T.add) s i v) Error_monad.ok
      in
    let=? '(prev_size, existed) := existing_size s i in
    let=? '(s, bytes_value) := consume_serialize_write_gas add s i v in
    let=? t_value := add s (data_key i) bytes_value in
    let size_diff := (Bytes.length bytes_value) -i prev_size in
    return=? ((C.(Raw_context_intf.T.project) t_value), size_diff, existed).
  
  Definition remove `{FArgs} (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    : M=? (Raw_context.root * int * bool) :=
    let remove {A : Set} (s : C.(Raw_context_intf.T.t)) (i : Raw_context.key)
      : M= (Pervasives.result C.(Raw_context_intf.T.t) A) :=
      Error_monad.op_gtpipeeq (C.(Raw_context_intf.T.remove) s i) Error_monad.ok
      in
    let=? '(prev_size, existed) := existing_size s i in
    let=? s := consume_remove_gas remove s i in
    let=? t_value := remove s (data_key i) in
    return=? ((C.(Raw_context_intf.T.project) t_value), prev_size, existed).
  
  Definition remove_existing `{FArgs}
    (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    : M=? (Raw_context.root * int) :=
    let=? '(prev_size, _) := existing_size s i in
    let=? s := consume_remove_gas C.(Raw_context_intf.T.remove_existing) s i in
    let=? t_value := C.(Raw_context_intf.T.remove_existing) s (data_key i) in
    return=? ((C.(Raw_context_intf.T.project) t_value), prev_size).
  
  Definition add_or_remove `{FArgs}
    (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    (v : option V.(Storage_sigs.VALUE.t))
    : M=? (Raw_context.root * int * bool) :=
    match v with
    | None => remove s i
    | Some v => add s i v
    end.
  
  (** Because big map values are not stored under some common key,
      we have no choice but to fold over all nodes with a path of length
      [I.path_length] to retrieve actual keys and then paginate.

      While this is inefficient and will traverse the whole tree ([O(n)]), there
      currently isn't a better decent alternative.

      Once https://gitlab.com/tezos/tezos/-/merge_requests/2771 which flattens paths is done,
      {!C.list} could be used instead here. *)
  Definition list_values `{FArgs} (op_staroptstar : option int)
    : option int -> C.(Raw_context_intf.T.t) ->
    M=? (Raw_context.root * list V.(Storage_sigs.VALUE.t)) :=
    let offset :=
      match op_staroptstar with
      | Some op_starsthstar => op_starsthstar
      | None => 0
      end in
    fun (op_staroptstar : option int) =>
      let length :=
        match op_staroptstar with
        | Some op_starsthstar => op_starsthstar
        | None => Pervasives.max_int
        end in
      fun (s : C.(Raw_context_intf.T.t)) =>
        let root {A : Set} : list A :=
          nil in
        let depth := Context.Eq I.(INDEX.path_length) in
        let=? '(s, rev_values, _offset, _length) :=
          C.(Raw_context_intf.T.fold) (Some depth) s root
            (return? (s, nil, offset, length))
            (fun (file : Raw_context.key) =>
              fun (tree : Raw_context.tree) =>
                fun (acc_value :
                  M?
                    (C.(Raw_context_intf.T.t) * list V.(Storage_sigs.VALUE.t) *
                      int * int)) =>
                  match
                    ((C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.kind_value)
                      tree), acc_value) with
                  |
                    (Context.Kind.Tree,
                      Pervasives.Ok (s, rev_values, offset, length)) =>
                    if length <=i 0 then
                      return= acc_value
                    else
                      if offset >i 0 then
                        let offset := Pervasives.pred offset in
                        return= (Pervasives.Ok (s, rev_values, offset, length))
                      else
                        match I.(INDEX.of_path) file with
                        | None =>
                          (* ❌ Assert instruction is not handled. *)
                          assert
                            (M=?
                              (C.(Raw_context_intf.T.t) *
                                list V.(Storage_sigs.VALUE.t) * int * int))
                            false
                        | Some key_value =>
                          let=? '(s, value) := get_unprojected s key_value in
                          return=?
                            (s, (cons value rev_values), 0,
                              (Pervasives.pred length))
                        end
                  | _ => return= acc_value
                  end) in
        return=? ((C.(Raw_context_intf.T.project) s), (List.rev rev_values)).
  
  Definition fold_keys_unaccounted `{FArgs} {A : Set}
    (s : C.(Raw_context_intf.T.t)) (init_value : A)
    (f : I.(INDEX.t) -> A -> M= A) : M= A :=
    C.(Raw_context_intf.T.fold) (Some (Context.Eq I.(INDEX.path_length))) s nil
      init_value
      (fun (file : Raw_context.key) =>
        fun (tree : Raw_context.tree) =>
          fun (acc_value : A) =>
            match
              C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.kind_value)
                tree with
            | Context.Kind.Value =>
              match
                ((List.rev file),
                  match List.rev file with
                  | cons last _ =>
                    Compare.String.(Compare.S.op_eq) last len_name
                  | _ => false
                  end,
                  match List.rev file with
                  | cons last rest =>
                    Compare.String.(Compare.S.op_eq) last data_name
                  | _ => false
                  end) with
              | (cons last _, true, _) => return= acc_value
              | (cons last rest, _, true) =>
                let file := List.rev rest in
                match I.(INDEX.of_path) file with
                | None =>
                  (* ❌ Assert instruction is not handled. *)
                  assert (M= _) false
                | Some path => f path acc_value
                end
              | (_, _, _) =>
                (* ❌ Assert instruction is not handled. *)
                assert (M= _) false
              end
            | Context.Kind.Tree => return= acc_value
            end).
  
  Definition keys_unaccounted `{FArgs} (s : C.(Raw_context_intf.T.t))
    : M= (list I.(INDEX.t)) :=
    fold_keys_unaccounted s nil
      (fun (p_value : I.(INDEX.t)) =>
        fun (acc_value : list I.(INDEX.t)) => return= (cons p_value acc_value)).
  
  Definition functor `{FArgs} :=
    {|
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.mem :=
        mem;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.get :=
        get;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.find :=
        find;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.update :=
        update;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.init_value :=
        init_value;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.add :=
        add;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.add_or_remove :=
        add_or_remove;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.remove_existing :=
        remove_existing;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.remove :=
        remove;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.list_values :=
        list_values;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.fold_keys_unaccounted
        _ := fold_keys_unaccounted
    |}.
End Make_indexed_carbonated_data_storage_INTERNAL.
Definition Make_indexed_carbonated_data_storage_INTERNAL
  {C_t I_t : Set} {I_ipath : Set -> Set} {V_t : Set}
  (C : Raw_context.T (t := C_t)) (I : INDEX (t := I_t) (ipath := I_ipath))
  (V : Storage_sigs.VALUE (t := V_t))
  : Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL
    (t := C.(Raw_context_intf.T.t)) (key := I.(INDEX.t))
    (value := V.(Storage_sigs.VALUE.t)) :=
  let '_ := Make_indexed_carbonated_data_storage_INTERNAL.Build_FArgs C I V in
  Make_indexed_carbonated_data_storage_INTERNAL.functor.

Definition Make_indexed_carbonated_data_storage
  {C_t I_t : Set} {I_ipath : Set -> Set} {V_t : Set}
  (C : Raw_context.T (t := C_t)) (I : INDEX (t := I_t) (ipath := I_ipath))
  (V : Storage_sigs.VALUE (t := V_t))
  : Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values
    (t := C.(Raw_context_intf.T.t)) (key := I.(INDEX.t))
    (value := V.(Storage_sigs.VALUE.t)) :=
  let functor_result := Make_indexed_carbonated_data_storage_INTERNAL C I V in
  {|
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.mem :=
      functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.mem);
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.get :=
      functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.get);
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.find :=
      functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.find);
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.update :=
      functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.update);
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.init_value :=
      functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.init_value);
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.add :=
      functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.add);
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.add_or_remove :=
      functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.add_or_remove);
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.remove_existing :=
      functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.remove_existing);
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.remove :=
      functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.remove);
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.list_values :=
      functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.list_values)
  |}.

Module Make_carbonated_data_set_storage.
  Class FArgs {C_t I_t : Set} {I_ipath : Set -> Set} := {
    C : Raw_context.T (t := C_t);
    I : INDEX (t := I_t) (ipath := I_ipath);
  }.
  Arguments Build_FArgs {_ _ _}.
  
  Module V.
    Definition t `{FArgs} : Set := unit.
    
    Definition encoding `{FArgs} : Data_encoding.encoding unit :=
      Data_encoding.unit_value.
    
    Definition module `{FArgs} :=
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}.
  End V.
  Definition V `{FArgs} := V.module.
  
  Definition M `{FArgs} := Make_indexed_carbonated_data_storage_INTERNAL C I V.
  
  Definition t `{FArgs} : Set := C.(Raw_context_intf.T.t).
  
  Definition context `{FArgs} : Set := t.
  
  Definition elt `{FArgs} : Set := I.(INDEX.t).
  
  Definition mem `{FArgs}
    : C.(Raw_context_intf.T.t) -> I.(INDEX.t) -> M=? (Raw_context.t * bool) :=
    M.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.mem).
  
  Definition init_value `{FArgs}
    (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    : M=? (Raw_context.t * int) :=
    M.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.init_value)
      s i tt.
  
  Definition remove `{FArgs} (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
    : M=? (Raw_context.t * int * bool) :=
    M.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.remove)
      s i.
  
  Definition fold_keys_unaccounted `{FArgs} {A : Set}
    : C.(Raw_context_intf.T.t) -> A -> (I.(INDEX.t) -> A -> M= A) -> M= A :=
    M.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.fold_keys_unaccounted).
  
  Definition functor `{FArgs} :=
    {|
      Storage_sigs.Carbonated_data_set_storage.mem := mem;
      Storage_sigs.Carbonated_data_set_storage.init_value := init_value;
      Storage_sigs.Carbonated_data_set_storage.remove := remove;
      Storage_sigs.Carbonated_data_set_storage.fold_keys_unaccounted _ :=
        fold_keys_unaccounted
    |}.
End Make_carbonated_data_set_storage.
Definition Make_carbonated_data_set_storage
  {C_t I_t : Set} {I_ipath : Set -> Set}
  (C : Raw_context.T (t := C_t)) (I : INDEX (t := I_t) (ipath := I_ipath))
  : Storage_sigs.Carbonated_data_set_storage (t := C.(Raw_context_intf.T.t))
    (elt := I.(INDEX.t)) :=
  let '_ := Make_carbonated_data_set_storage.Build_FArgs C I in
  Make_carbonated_data_set_storage.functor.

Module Make_indexed_data_snapshotable_storage.
  Class FArgs {C_t Snapshot_index_t : Set} {Snapshot_index_ipath : Set -> Set}
    {I_t : Set} {I_ipath : Set -> Set} {V_t : Set} := {
    C : Raw_context.T (t := C_t);
    Snapshot_index :
      INDEX (t := Snapshot_index_t) (ipath := Snapshot_index_ipath);
    I : INDEX (t := I_t) (ipath := I_ipath);
    V : Storage_sigs.VALUE (t := V_t);
  }.
  Arguments Build_FArgs {_ _ _ _ _ _}.
  
  Definition snapshot `{FArgs} : Set := Snapshot_index.(INDEX.t).
  
  Definition data_name `{FArgs} : list string := [ "current" ].
  
  Definition snapshot_name `{FArgs} : list string := [ "snapshot" ].
  
  Definition C_data `{FArgs} :=
    Make_subcontext Registered C
      (let name := data_name in
      {|
        Storage_sigs.NAME.name := name
      |}).
  
  Definition C_snapshot `{FArgs} :=
    Make_subcontext Registered C
      (let name := snapshot_name in
      {|
        Storage_sigs.NAME.name := name
      |}).
  
  Definition Make_indexed_data_storage_include `{FArgs} :=
    Make_indexed_data_storage C_data I V.
  
  (** Inclusion of the module [Make_indexed_data_storage_include] *)
  Definition t `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.t).
  
  Definition context `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.context).
  
  Definition key `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.key).
  
  Definition value `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.value).
  
  Definition mem `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.mem).
  
  Definition get `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.get).
  
  Definition find `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.find).
  
  Definition update `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.update).
  
  Definition init_value `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.init_value).
  
  Definition add `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.add).
  
  Definition add_or_remove `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.add_or_remove).
  
  Definition remove_existing `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.remove_existing).
  
  Definition remove `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.remove).
  
  Definition clear `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.clear).
  
  Definition keys `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.keys).
  
  Definition bindings `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.bindings).
  
  Definition fold `{FArgs} {a : Set} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.fold)
      (a := a).
  
  Definition fold_keys `{FArgs} {a : Set} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.fold_keys)
      (a := a).
  
  Definition Snapshot `{FArgs} :=
    Make_indexed_data_storage C_snapshot (Pair Snapshot_index I) V.
  
  Definition snapshot_path `{FArgs} (id : Snapshot_index.(INDEX.t))
    : list string :=
    Pervasives.op_at snapshot_name (Snapshot_index.(INDEX.to_path) id nil).
  
  Definition snapshot_exists `{FArgs}
    (s : C.(Raw_context_intf.T.t)) (id : Snapshot_index.(INDEX.t)) : M= bool :=
    C.(Raw_context_intf.T.mem_tree) s (snapshot_path id).
  
  Definition err_missing_key `{FArgs} {A : Set} (key_value : list string)
    : M? A :=
    Raw_context.storage_error_value
      (Raw_context.Missing_key key_value Raw_context.Copy).
  
  Definition snapshot_value `{FArgs}
    (s : C.(Raw_context_intf.T.t)) (id : Snapshot_index.(INDEX.t))
    : M=? Raw_context.root :=
    let= function_parameter := C.(Raw_context_intf.T.find_tree) s data_name in
    match function_parameter with
    | None => return= (err_missing_key data_name)
    | Some tree =>
      Error_monad.op_gtpipeeq
        (let= t_value :=
          C.(Raw_context_intf.T.add_tree) s (snapshot_path id) tree in
        return= (C.(Raw_context_intf.T.project) t_value)) Error_monad.ok
    end.
  
  Definition delete_snapshot `{FArgs}
    (s : C.(Raw_context_intf.T.t)) (id : Snapshot_index.(INDEX.t))
    : M= Raw_context.root :=
    let= t_value := C.(Raw_context_intf.T.remove) s (snapshot_path id) in
    return= (C.(Raw_context_intf.T.project) t_value).
  
  Definition functor `{FArgs} :=
    {|
      Storage_sigs.Indexed_data_snapshotable_storage.mem := mem;
      Storage_sigs.Indexed_data_snapshotable_storage.get := get;
      Storage_sigs.Indexed_data_snapshotable_storage.find := find;
      Storage_sigs.Indexed_data_snapshotable_storage.update := update;
      Storage_sigs.Indexed_data_snapshotable_storage.init_value := init_value;
      Storage_sigs.Indexed_data_snapshotable_storage.add := add;
      Storage_sigs.Indexed_data_snapshotable_storage.add_or_remove :=
        add_or_remove;
      Storage_sigs.Indexed_data_snapshotable_storage.remove_existing :=
        remove_existing;
      Storage_sigs.Indexed_data_snapshotable_storage.remove := remove;
      Storage_sigs.Indexed_data_snapshotable_storage.clear := clear;
      Storage_sigs.Indexed_data_snapshotable_storage.keys := keys;
      Storage_sigs.Indexed_data_snapshotable_storage.bindings := bindings;
      Storage_sigs.Indexed_data_snapshotable_storage.fold _ := fold;
      Storage_sigs.Indexed_data_snapshotable_storage.fold_keys _ := fold_keys;
      Storage_sigs.Indexed_data_snapshotable_storage.Snapshot := Snapshot;
      Storage_sigs.Indexed_data_snapshotable_storage.snapshot_exists :=
        snapshot_exists;
      Storage_sigs.Indexed_data_snapshotable_storage.snapshot_value :=
        snapshot_value;
      Storage_sigs.Indexed_data_snapshotable_storage.delete_snapshot :=
        delete_snapshot
    |}.
End Make_indexed_data_snapshotable_storage.
Definition Make_indexed_data_snapshotable_storage
  {C_t Snapshot_index_t : Set} {Snapshot_index_ipath : Set -> Set} {I_t : Set}
  {I_ipath : Set -> Set} {V_t : Set}
  (C : Raw_context.T (t := C_t))
  (Snapshot_index :
    INDEX (t := Snapshot_index_t) (ipath := Snapshot_index_ipath))
  (I : INDEX (t := I_t) (ipath := I_ipath)) (V : Storage_sigs.VALUE (t := V_t))
  : Storage_sigs.Indexed_data_snapshotable_storage
    (snapshot := Snapshot_index.(INDEX.t)) (key := I.(INDEX.t))
    (t := C.(Raw_context_intf.T.t)) (value := V.(Storage_sigs.VALUE.t)) :=
  let '_ :=
    Make_indexed_data_snapshotable_storage.Build_FArgs C Snapshot_index I V in
  Make_indexed_data_snapshotable_storage.functor.

Module Make_indexed_subcontext.
  Class FArgs {C_t I_t : Set} {I_ipath : Set -> Set} := {
    C : Raw_context.T (t := C_t);
    I : INDEX (t := I_t) (ipath := I_ipath);
  }.
  Arguments Build_FArgs {_ _ _}.
  
  Definition t `{FArgs} : Set := C.(Raw_context_intf.T.t).
  
  Definition context `{FArgs} : Set := t.
  
  Definition key `{FArgs} : Set := I.(INDEX.t).
  
  Definition ipath `{FArgs} (a : Set) : Set := I.(INDEX.ipath) a.
  
  Definition clear `{FArgs} (t_value : C.(Raw_context_intf.T.t))
    : M= Raw_context.root :=
    let= t_value := C.(Raw_context_intf.T.remove) t_value nil in
    return= (C.(Raw_context_intf.T.project) t_value).
  
  Definition fold_keys `{FArgs} {A : Set}
    (t_value : C.(Raw_context_intf.T.t)) (init_value : A)
    (f : I.(INDEX.t) -> A -> M= A) : M= A :=
    C.(Raw_context_intf.T.fold) (Some (Context.Eq I.(INDEX.path_length)))
      t_value nil init_value
      (fun (path : Raw_context.key) =>
        fun (tree : Raw_context.tree) =>
          fun (acc_value : A) =>
            match
              C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.kind_value)
                tree with
            | Context.Kind.Tree =>
              match I.(INDEX.of_path) path with
              | None =>
                (* ❌ Assert instruction is not handled. *)
                assert (M= _) false
              | Some path => f path acc_value
              end
            | Context.Kind.Value => return= acc_value
            end).
  
  Definition keys `{FArgs} (t_value : C.(Raw_context_intf.T.t))
    : M= (list I.(INDEX.t)) :=
    fold_keys t_value nil
      (fun (i : I.(INDEX.t)) =>
        fun (acc_value : list I.(INDEX.t)) => return= (cons i acc_value)).
  
  Definition err_missing_key `{FArgs} {A : Set} (key_value : list string)
    : M? A :=
    Raw_context.storage_error_value
      (Raw_context.Missing_key key_value Raw_context.Copy).
  
  Definition copy `{FArgs}
    (t_value : C.(Raw_context_intf.T.t)) (from : I.(INDEX.t))
    (to_ : I.(INDEX.t)) : M=? C.(Raw_context_intf.T.t) :=
    let from := I.(INDEX.to_path) from nil in
    let to_ := I.(INDEX.to_path) to_ nil in
    let= function_parameter := C.(Raw_context_intf.T.find_tree) t_value from in
    match function_parameter with
    | None => return= (err_missing_key from)
    | Some tree =>
      Error_monad.op_gtpipeeq (C.(Raw_context_intf.T.add_tree) t_value to_ tree)
        Error_monad.ok
    end.
  
  Definition remove `{FArgs}
    (t_value : C.(Raw_context_intf.T.t)) (k : I.(INDEX.t))
    : M= C.(Raw_context_intf.T.t) :=
    C.(Raw_context_intf.T.remove) t_value (I.(INDEX.to_path) k nil).
  
  Definition description `{FArgs}
    : Storage_description.t (I.(INDEX.ipath) C.(Raw_context_intf.T.t)) :=
    Storage_description.register_indexed_subcontext
      C.(Raw_context_intf.T.description)
      (fun (c : C.(Raw_context_intf.T.t)) =>
        Error_monad.op_gtpipeeq (keys c) Error_monad.ok) I.(INDEX.args).
  
  Definition unpack `{FArgs}
    : I.(INDEX.ipath) C.(Raw_context_intf.T.t) ->
    C.(Raw_context_intf.T.t) * I.(INDEX.t) :=
    Storage_description.unpack I.(INDEX.args).
  
  Definition _pack `{FArgs}
    : C.(Raw_context_intf.T.t) -> I.(INDEX.t) ->
    I.(INDEX.ipath) C.(Raw_context_intf.T.t) :=
    Storage_description._pack I.(INDEX.args).
  
  Module Raw_context.
    Definition t `{FArgs} : Set := I.(INDEX.ipath) C.(Raw_context_intf.T.t).
    
    Definition to_key `{FArgs} (i : I.(INDEX.t)) (k : list string)
      : list string := I.(INDEX.to_path) i k.
    
    Definition mem `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context_intf.T.t)) (k : list string)
      : M= bool :=
      let '(t_value, i) := unpack c in
      C.(Raw_context_intf.T.mem) t_value (to_key i k).
    
    Definition mem_tree `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context_intf.T.t)) (k : list string)
      : M= bool :=
      let '(t_value, i) := unpack c in
      C.(Raw_context_intf.T.mem_tree) t_value (to_key i k).
    
    Definition get `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context_intf.T.t)) (k : list string)
      : M=? Raw_context.value :=
      let '(t_value, i) := unpack c in
      C.(Raw_context_intf.T.get) t_value (to_key i k).
    
    Definition get_tree `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context_intf.T.t)) (k : list string)
      : M=? Raw_context.tree :=
      let '(t_value, i) := unpack c in
      C.(Raw_context_intf.T.get_tree) t_value (to_key i k).
    
    Definition find `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context_intf.T.t)) (k : list string)
      : M= (option Raw_context.value) :=
      let '(t_value, i) := unpack c in
      C.(Raw_context_intf.T.find) t_value (to_key i k).
    
    Definition find_tree `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context_intf.T.t)) (k : list string)
      : M= (option Raw_context.tree) :=
      let '(t_value, i) := unpack c in
      C.(Raw_context_intf.T.find_tree) t_value (to_key i k).
    
    Definition list_value `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context_intf.T.t)) (offset : option int)
      (length : option int) (k : list string)
      : M= (list (string * Raw_context.tree)) :=
      let '(t_value, i) := unpack c in
      C.(Raw_context_intf.T.list_value) t_value offset length (to_key i k).
    
    Definition init_value `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context_intf.T.t)) (k : list string)
      (v : Raw_context.value)
      : M=? (I.(INDEX.ipath) C.(Raw_context_intf.T.t)) :=
      let '(t_value, i) := unpack c in
      let=? t_value := C.(Raw_context_intf.T.init_value) t_value (to_key i k) v
        in
      return=? (_pack t_value i).
    
    Definition init_tree `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context_intf.T.t)) (k : list string)
      (v : Raw_context.tree) : M=? (I.(INDEX.ipath) C.(Raw_context_intf.T.t)) :=
      let '(t_value, i) := unpack c in
      let=? t_value := C.(Raw_context_intf.T.init_tree) t_value (to_key i k) v
        in
      return=? (_pack t_value i).
    
    Definition update `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context_intf.T.t)) (k : list string)
      (v : Raw_context.value)
      : M=? (I.(INDEX.ipath) C.(Raw_context_intf.T.t)) :=
      let '(t_value, i) := unpack c in
      let=? t_value := C.(Raw_context_intf.T.update) t_value (to_key i k) v in
      return=? (_pack t_value i).
    
    Definition update_tree `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context_intf.T.t)) (k : list string)
      (v : Raw_context.tree) : M=? (I.(INDEX.ipath) C.(Raw_context_intf.T.t)) :=
      let '(t_value, i) := unpack c in
      let=? t_value := C.(Raw_context_intf.T.update_tree) t_value (to_key i k) v
        in
      return=? (_pack t_value i).
    
    Definition add `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context_intf.T.t)) (k : list string)
      (v : Raw_context.value) : M= (I.(INDEX.ipath) C.(Raw_context_intf.T.t)) :=
      let '(t_value, i) := unpack c in
      let= t_value := C.(Raw_context_intf.T.add) t_value (to_key i k) v in
      return= (_pack t_value i).
    
    Definition add_tree `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context_intf.T.t)) (k : list string)
      (v : Raw_context.tree) : M= (I.(INDEX.ipath) C.(Raw_context_intf.T.t)) :=
      let '(t_value, i) := unpack c in
      let= t_value := C.(Raw_context_intf.T.add_tree) t_value (to_key i k) v in
      return= (_pack t_value i).
    
    Definition add_or_remove `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context_intf.T.t)) (k : list string)
      (v : option Raw_context.value)
      : M= (I.(INDEX.ipath) C.(Raw_context_intf.T.t)) :=
      let '(t_value, i) := unpack c in
      let= t_value :=
        C.(Raw_context_intf.T.add_or_remove) t_value (to_key i k) v in
      return= (_pack t_value i).
    
    Definition add_or_remove_tree `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context_intf.T.t)) (k : list string)
      (v : option Raw_context.tree)
      : M= (I.(INDEX.ipath) C.(Raw_context_intf.T.t)) :=
      let '(t_value, i) := unpack c in
      let= t_value :=
        C.(Raw_context_intf.T.add_or_remove_tree) t_value (to_key i k) v in
      return= (_pack t_value i).
    
    Definition remove_existing `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context_intf.T.t)) (k : list string)
      : M=? (I.(INDEX.ipath) C.(Raw_context_intf.T.t)) :=
      let '(t_value, i) := unpack c in
      let=? t_value :=
        C.(Raw_context_intf.T.remove_existing) t_value (to_key i k) in
      return=? (_pack t_value i).
    
    Definition remove_existing_tree `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context_intf.T.t)) (k : list string)
      : M=? (I.(INDEX.ipath) C.(Raw_context_intf.T.t)) :=
      let '(t_value, i) := unpack c in
      let=? t_value :=
        C.(Raw_context_intf.T.remove_existing_tree) t_value (to_key i k) in
      return=? (_pack t_value i).
    
    Definition remove `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context_intf.T.t)) (k : list string)
      : M= (I.(INDEX.ipath) C.(Raw_context_intf.T.t)) :=
      let '(t_value, i) := unpack c in
      let= t_value := C.(Raw_context_intf.T.remove) t_value (to_key i k) in
      return= (_pack t_value i).
    
    Definition fold `{FArgs} {A : Set}
      (depth : option Context.depth)
      (c : I.(INDEX.ipath) C.(Raw_context_intf.T.t)) (k : list string)
      (init_value : A) (f : Raw_context.key -> Raw_context.tree -> A -> M= A)
      : M= A :=
      let '(t_value, i) := unpack c in
      C.(Raw_context_intf.T.fold) depth t_value (to_key i k) init_value f.
    
    Module Tree.
      (** Inclusion of the module [C.Tree] *)
      Definition mem `{FArgs} := C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.mem).
      
      Definition mem_tree `{FArgs} := C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.mem_tree).
      
      Definition get `{FArgs} := C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.get).
      
      Definition get_tree `{FArgs} := C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.get_tree).
      
      Definition find `{FArgs} := C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.find).
      
      Definition find_tree `{FArgs} := C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.find_tree).
      
      Definition list_value `{FArgs} :=
        C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.list_value).
      
      Definition init_value `{FArgs} :=
        C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.init_value).
      
      Definition init_tree `{FArgs} := C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.init_tree).
      
      Definition update `{FArgs} := C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.update).
      
      Definition update_tree `{FArgs} :=
        C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.update_tree).
      
      Definition add `{FArgs} := C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.add).
      
      Definition add_tree `{FArgs} := C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.add_tree).
      
      Definition remove `{FArgs} := C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.remove).
      
      Definition remove_existing `{FArgs} :=
        C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.remove_existing).
      
      Definition remove_existing_tree `{FArgs} :=
        C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.remove_existing_tree).
      
      Definition add_or_remove `{FArgs} :=
        C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.add_or_remove).
      
      Definition add_or_remove_tree `{FArgs} :=
        C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.add_or_remove_tree).
      
      Definition fold `{FArgs} {a : Set} :=
        C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.fold) (a := a).
      
      (* Definition empty `{FArgs} := C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.empty). *)
      
      Definition is_empty `{FArgs} := C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.is_empty).
      
      Definition kind_value `{FArgs} :=
        C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.kind_value).
      
      Definition to_value `{FArgs} := C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.to_value).
      
      Definition hash_value `{FArgs} :=
        C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.hash_value).
      
      Definition equal `{FArgs} := C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.equal).
      
      Definition clear `{FArgs} := C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.clear).
      
      Definition empty `{FArgs} (c : I.(INDEX.ipath) C.(Raw_context_intf.T.t))
        : Raw_context.tree :=
        let '(t_value, _) := unpack c in
        C.(Raw_context_intf.T.Tree).(Raw_context_intf.TREE.empty) t_value.
      
      Definition module `{FArgs} :=
        {|
          Raw_context_intf.TREE.mem := mem;
          Raw_context_intf.TREE.mem_tree := mem_tree;
          Raw_context_intf.TREE.get := get;
          Raw_context_intf.TREE.get_tree := get_tree;
          Raw_context_intf.TREE.find := find;
          Raw_context_intf.TREE.find_tree := find_tree;
          Raw_context_intf.TREE.list_value := list_value;
          Raw_context_intf.TREE.init_value := init_value;
          Raw_context_intf.TREE.init_tree := init_tree;
          Raw_context_intf.TREE.update := update;
          Raw_context_intf.TREE.update_tree := update_tree;
          Raw_context_intf.TREE.add := add;
          Raw_context_intf.TREE.add_tree := add_tree;
          Raw_context_intf.TREE.remove := remove;
          Raw_context_intf.TREE.remove_existing := remove_existing;
          Raw_context_intf.TREE.remove_existing_tree := remove_existing_tree;
          Raw_context_intf.TREE.add_or_remove := add_or_remove;
          Raw_context_intf.TREE.add_or_remove_tree := add_or_remove_tree;
          Raw_context_intf.TREE.fold _ := fold;
          Raw_context_intf.TREE.is_empty := is_empty;
          Raw_context_intf.TREE.kind_value := kind_value;
          Raw_context_intf.TREE.to_value := to_value;
          Raw_context_intf.TREE.hash_value := hash_value;
          Raw_context_intf.TREE.equal := equal;
          Raw_context_intf.TREE.clear := clear;
          Raw_context_intf.TREE.empty := empty
        |}.
    End Tree.
    Definition Tree `{FArgs} := Tree.module.
    
    Definition project `{FArgs} (c : I.(INDEX.ipath) C.(Raw_context_intf.T.t))
      : Raw_context.root :=
      let '(t_value, _) := unpack c in
      C.(Raw_context_intf.T.project) t_value.
    
    Definition absolute_key `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context_intf.T.t)) (k : list string)
      : Raw_context.key :=
      let '(t_value, i) := unpack c in
      C.(Raw_context_intf.T.absolute_key) t_value (to_key i k).
    
    Definition consume_gas `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context_intf.T.t)) (g : Gas_limit_repr.cost)
      : M? (I.(INDEX.ipath) C.(Raw_context_intf.T.t)) :=
      let '(t_value, i) := unpack c in
      let? t_value := C.(Raw_context_intf.T.consume_gas) t_value g in
      return? (_pack t_value i).
    
    Definition check_enough_gas `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context_intf.T.t)) (g : Gas_limit_repr.cost)
      : M? unit :=
      let '(t_value, _i) := unpack c in
      C.(Raw_context_intf.T.check_enough_gas) t_value g.
    
    Definition description `{FArgs}
      : Storage_description.t (I.(INDEX.ipath) C.(Raw_context_intf.T.t)) :=
      description.
    
    Definition module `{FArgs} :=
      {|
        Raw_context_intf.T.mem := mem;
        Raw_context_intf.T.mem_tree := mem_tree;
        Raw_context_intf.T.get := get;
        Raw_context_intf.T.get_tree := get_tree;
        Raw_context_intf.T.find := find;
        Raw_context_intf.T.find_tree := find_tree;
        Raw_context_intf.T.list_value := list_value;
        Raw_context_intf.T.init_value := init_value;
        Raw_context_intf.T.init_tree := init_tree;
        Raw_context_intf.T.update := update;
        Raw_context_intf.T.update_tree := update_tree;
        Raw_context_intf.T.add := add;
        Raw_context_intf.T.add_tree := add_tree;
        Raw_context_intf.T.remove := remove;
        Raw_context_intf.T.remove_existing := remove_existing;
        Raw_context_intf.T.remove_existing_tree := remove_existing_tree;
        Raw_context_intf.T.add_or_remove := add_or_remove;
        Raw_context_intf.T.add_or_remove_tree := add_or_remove_tree;
        Raw_context_intf.T.fold _ := fold;
        Raw_context_intf.T.Tree := Tree;
        Raw_context_intf.T.project := project;
        Raw_context_intf.T.absolute_key := absolute_key;
        Raw_context_intf.T.consume_gas := consume_gas;
        Raw_context_intf.T.check_enough_gas := check_enough_gas;
        Raw_context_intf.T.description := description
      |}.
  End Raw_context.
  Definition Raw_context `{FArgs}
    : Raw_context.T (t := I.(INDEX.ipath) C.(Raw_context_intf.T.t)) :=
    Raw_context.module.
  
  Module Make_set.
    Class FArgs `{FArgs} := {
      R : Storage_sigs.REGISTER;
      N : Storage_sigs.NAME;
    }.
    Arguments Build_FArgs {_ _ _ _}.
    
    Definition t `{FArgs} : Set := C.(Raw_context_intf.T.t).
    
    Definition context `{FArgs} : Set := t.
    
    Definition elt `{FArgs} : Set := I.(INDEX.t).
    
    Definition inited `{FArgs} : bytes := Bytes.of_string "inited".
    
    Definition mem `{FArgs} (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
      : M= bool :=
      Raw_context.(Raw_context_intf.T.mem) (_pack s i)
        N.(Storage_sigs.NAME.name).
    
    Definition add `{FArgs} (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
      : M= Raw_context.root :=
      let= c :=
        Raw_context.(Raw_context_intf.T.add) (_pack s i)
          N.(Storage_sigs.NAME.name) inited in
      let '(s, _) := unpack c in
      return= (C.(Raw_context_intf.T.project) s).
    
    Definition remove `{FArgs} (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
      : M= Raw_context.root :=
      let= c :=
        Raw_context.(Raw_context_intf.T.remove) (_pack s i)
          N.(Storage_sigs.NAME.name) in
      let '(s, _) := unpack c in
      return= (C.(Raw_context_intf.T.project) s).
    
    Definition clear `{FArgs} (s : C.(Raw_context_intf.T.t))
      : M= Raw_context.root :=
      let= t_value :=
        fold_keys s s
          (fun (i : I.(INDEX.t)) =>
            fun (s : C.(Raw_context_intf.T.t)) =>
              let= c :=
                Raw_context.(Raw_context_intf.T.remove) (_pack s i)
                  N.(Storage_sigs.NAME.name) in
              let '(s, _) := unpack c in
              return= s) in
      return= (C.(Raw_context_intf.T.project) t_value).
    
    Definition fold `{FArgs} {A : Set}
      (s : C.(Raw_context_intf.T.t)) (init_value : A)
      (f : I.(INDEX.t) -> A -> M= A) : M= A :=
      fold_keys s init_value
        (fun (i : I.(INDEX.t)) =>
          fun (acc_value : A) =>
            let= function_parameter := mem s i in
            match function_parameter with
            | true => f i acc_value
            | false => return= acc_value
            end).
    
    Definition elements `{FArgs} (s : C.(Raw_context_intf.T.t))
      : M= (list I.(INDEX.t)) :=
      fold s nil
        (fun (p_value : I.(INDEX.t)) =>
          fun (acc_value : list I.(INDEX.t)) => return= (cons p_value acc_value)).
    
    Definition functor `{FArgs} :=
      {|
        Storage_sigs.Data_set_storage.mem := mem;
        Storage_sigs.Data_set_storage.add := add;
        Storage_sigs.Data_set_storage.remove := remove;
        Storage_sigs.Data_set_storage.elements := elements;
        Storage_sigs.Data_set_storage.fold _ := fold;
        Storage_sigs.Data_set_storage.clear := clear
      |}.
  End Make_set.
  Definition Make_set `{FArgs}
    (R : Storage_sigs.REGISTER) (N : Storage_sigs.NAME)
    : Storage_sigs.Data_set_storage (t := t) (elt := key) :=
    let '_ := Make_set.Build_FArgs R N in
    Make_set.functor.
  
  Module Make_map.
    Class FArgs `{FArgs} {V_t : Set} := {
      N : Storage_sigs.NAME;
      V : Storage_sigs.VALUE (t := V_t);
    }.
    Arguments Build_FArgs {_ _ _ _ _}.
    
    Definition t `{FArgs} : Set := C.(Raw_context_intf.T.t).
    
    Definition context `{FArgs} : Set := t.
    
    Definition key `{FArgs} : Set := I.(INDEX.t).
    
    Definition value `{FArgs} : Set := V.(Storage_sigs.VALUE.t).
    
    Definition Make_encoder_include `{FArgs} := Make_encoder V.
    
    (** Inclusion of the module [Make_encoder_include] *)
    Definition of_bytes `{FArgs} := Make_encoder_include.(ENCODER.of_bytes).
    
    Definition to_bytes `{FArgs} := Make_encoder_include.(ENCODER.to_bytes).
    
    Definition mem `{FArgs} (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
      : M= bool :=
      Raw_context.(Raw_context_intf.T.mem) (_pack s i)
        N.(Storage_sigs.NAME.name).
    
    Definition get `{FArgs} (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
      : M=? V.(Storage_sigs.VALUE.t) :=
      let=? b_value :=
        Raw_context.(Raw_context_intf.T.get) (_pack s i)
          N.(Storage_sigs.NAME.name) in
      let key_value (function_parameter : unit) : Raw_context.key :=
        let '_ := function_parameter in
        Raw_context.(Raw_context_intf.T.absolute_key) (_pack s i)
          N.(Storage_sigs.NAME.name) in
      return= (of_bytes key_value b_value).
    
    Definition find `{FArgs} (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
      : M=? (option V.(Storage_sigs.VALUE.t)) :=
      let= function_parameter :=
        Raw_context.(Raw_context_intf.T.find) (_pack s i)
          N.(Storage_sigs.NAME.name) in
      match function_parameter with
      | None => return= Error_monad.ok_none
      | Some b_value =>
        return=
          (let key_value (function_parameter : unit) : Raw_context.key :=
            let '_ := function_parameter in
            Raw_context.(Raw_context_intf.T.absolute_key) (_pack s i)
              N.(Storage_sigs.NAME.name) in
          let? v := of_bytes key_value b_value in
          return? (Some v))
      end.
    
    Definition update `{FArgs}
      (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
      (v : V.(Storage_sigs.VALUE.t)) : M=? Raw_context.root :=
      let=? c :=
        Raw_context.(Raw_context_intf.T.update) (_pack s i)
          N.(Storage_sigs.NAME.name) (to_bytes v) in
      let '(s, _) := unpack c in
      return=? (C.(Raw_context_intf.T.project) s).
    
    Definition init_value `{FArgs}
      (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
      (v : V.(Storage_sigs.VALUE.t)) : M=? Raw_context.root :=
      let=? c :=
        Raw_context.(Raw_context_intf.T.init_value) (_pack s i)
          N.(Storage_sigs.NAME.name) (to_bytes v) in
      let '(s, _) := unpack c in
      return=? (C.(Raw_context_intf.T.project) s).
    
    Definition add `{FArgs}
      (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
      (v : V.(Storage_sigs.VALUE.t)) : M= Raw_context.root :=
      let= c :=
        Raw_context.(Raw_context_intf.T.add) (_pack s i)
          N.(Storage_sigs.NAME.name) (to_bytes v) in
      let '(s, _) := unpack c in
      return= (C.(Raw_context_intf.T.project) s).
    
    Definition add_or_remove `{FArgs}
      (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
      (v : option V.(Storage_sigs.VALUE.t)) : M= Raw_context.root :=
      let= c :=
        Raw_context.(Raw_context_intf.T.add_or_remove) (_pack s i)
          N.(Storage_sigs.NAME.name) (Option.map to_bytes v) in
      let '(s, _) := unpack c in
      return= (C.(Raw_context_intf.T.project) s).
    
    Definition remove `{FArgs} (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
      : M= Raw_context.root :=
      let= c :=
        Raw_context.(Raw_context_intf.T.remove) (_pack s i)
          N.(Storage_sigs.NAME.name) in
      let '(s, _) := unpack c in
      return= (C.(Raw_context_intf.T.project) s).
    
    Definition remove_existing `{FArgs}
      (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t)) : M=? Raw_context.root :=
      let=? c :=
        Raw_context.(Raw_context_intf.T.remove_existing) (_pack s i)
          N.(Storage_sigs.NAME.name) in
      let '(s, _) := unpack c in
      return=? (C.(Raw_context_intf.T.project) s).
    
    Definition clear `{FArgs} (s : C.(Raw_context_intf.T.t))
      : M= Raw_context.root :=
      let= t_value :=
        fold_keys s s
          (fun (i : I.(INDEX.t)) =>
            fun (s : C.(Raw_context_intf.T.t)) =>
              let= c :=
                Raw_context.(Raw_context_intf.T.remove) (_pack s i)
                  N.(Storage_sigs.NAME.name) in
              let '(s, _) := unpack c in
              return= s) in
      return= (C.(Raw_context_intf.T.project) t_value).
    
    Definition fold `{FArgs} {A : Set}
      (s : C.(Raw_context_intf.T.t)) (init_value : A)
      (f : I.(INDEX.t) -> V.(Storage_sigs.VALUE.t) -> A -> M= A) : M= A :=
      fold_keys s init_value
        (fun (i : I.(INDEX.t)) =>
          fun (acc_value : A) =>
            let= function_parameter := get s i in
            match function_parameter with
            | Pervasives.Error _ => return= acc_value
            | Pervasives.Ok v => f i v acc_value
            end).
    
    Definition bindings `{FArgs} (s : C.(Raw_context_intf.T.t))
      : M= (list (I.(INDEX.t) * V.(Storage_sigs.VALUE.t))) :=
      fold s nil
        (fun (p_value : I.(INDEX.t)) =>
          fun (v : V.(Storage_sigs.VALUE.t)) =>
            fun (acc_value : list (I.(INDEX.t) * V.(Storage_sigs.VALUE.t))) =>
              return= (cons (p_value, v) acc_value)).
    
    Definition fold_keys `{FArgs} {A : Set}
      (s : C.(Raw_context_intf.T.t)) (init_value : A)
      (f : I.(INDEX.t) -> A -> M= A) : M= A :=
      fold_keys s init_value
        (fun (i : I.(INDEX.t)) =>
          fun (acc_value : A) =>
            let= function_parameter := mem s i in
            match function_parameter with
            | false => return= acc_value
            | true => f i acc_value
            end).
    
    Definition keys `{FArgs} (s : C.(Raw_context_intf.T.t))
      : M= (list I.(INDEX.t)) :=
      fold_keys s nil
        (fun (p_value : I.(INDEX.t)) =>
          fun (acc_value : list I.(INDEX.t)) => return= (cons p_value acc_value)).
    
    Definition functor `{FArgs} :=
      {|
        Storage_sigs.Indexed_data_storage.mem := mem;
        Storage_sigs.Indexed_data_storage.get := get;
        Storage_sigs.Indexed_data_storage.find := find;
        Storage_sigs.Indexed_data_storage.update := update;
        Storage_sigs.Indexed_data_storage.init_value := init_value;
        Storage_sigs.Indexed_data_storage.add := add;
        Storage_sigs.Indexed_data_storage.add_or_remove := add_or_remove;
        Storage_sigs.Indexed_data_storage.remove_existing := remove_existing;
        Storage_sigs.Indexed_data_storage.remove := remove;
        Storage_sigs.Indexed_data_storage.clear := clear;
        Storage_sigs.Indexed_data_storage.keys := keys;
        Storage_sigs.Indexed_data_storage.bindings := bindings;
        Storage_sigs.Indexed_data_storage.fold _ := fold;
        Storage_sigs.Indexed_data_storage.fold_keys _ := fold_keys
      |}.
  End Make_map.
  Definition Make_map `{FArgs} {V_t : Set}
    (N : Storage_sigs.NAME) (V : Storage_sigs.VALUE (t := V_t))
    : Storage_sigs.Indexed_data_storage (t := t) (key := key)
      (value := V.(Storage_sigs.VALUE.t)) :=
    let '_ := Make_map.Build_FArgs N V in
    Make_map.functor.
  
  Module Make_carbonated_map.
    Class FArgs `{FArgs} {V_t : Set} := {
      N : Storage_sigs.NAME;
      V : Storage_sigs.VALUE (t := V_t);
    }.
    Arguments Build_FArgs {_ _ _ _ _}.
    
    Definition t `{FArgs} : Set := C.(Raw_context_intf.T.t).
    
    Definition context `{FArgs} : Set := t.
    
    Definition key `{FArgs} : Set := I.(INDEX.t).
    
    Definition value `{FArgs} : Set := V.(Storage_sigs.VALUE.t).
    
    Definition Make_encoder_include `{FArgs} := Make_encoder V.
    
    (** Inclusion of the module [Make_encoder_include] *)
    Definition of_bytes `{FArgs} := Make_encoder_include.(ENCODER.of_bytes).
    
    Definition to_bytes `{FArgs} := Make_encoder_include.(ENCODER.to_bytes).
    
    Definition len_name `{FArgs} : list string :=
      cons len_name N.(Storage_sigs.NAME.name).
    
    Definition data_name `{FArgs} : list string :=
      cons data_name N.(Storage_sigs.NAME.name).
    
    Definition path_length `{FArgs} : int :=
      (List.length N.(Storage_sigs.NAME.name)) +i 1.
    
    Definition consume_mem_gas `{FArgs} (c : Raw_context.(Raw_context_intf.T.t))
      : M? Raw_context.(Raw_context_intf.T.t) :=
      Raw_context.(Raw_context_intf.T.consume_gas) c
        (Storage_costs.read_access path_length 0).
    
    Definition existing_size `{FArgs} (c : Raw_context.(Raw_context_intf.T.t))
      : M=? (int * bool) :=
      let= function_parameter :=
        Raw_context.(Raw_context_intf.T.find) c len_name in
      match function_parameter with
      | None => return=? (0, false)
      | Some len =>
        return=
          (let? len := decode_len_value len_name len in
          return? (len, true))
      end.
    
    Definition consume_read_gas `{FArgs}
      (get : Raw_context.(Raw_context_intf.T.t) -> list string -> M=? bytes)
      (c : Raw_context.(Raw_context_intf.T.t))
      : M=? Raw_context.(Raw_context_intf.T.t) :=
      let=? len := get c len_name in
      return=
        (let? read_bytes := decode_len_value len_name len in
        Raw_context.(Raw_context_intf.T.consume_gas) c
          (Storage_costs.read_access path_length read_bytes)).
    
    Definition consume_write_gas `{FArgs} {A : Set}
      (set : Raw_context.(Raw_context_intf.T.t) -> list string -> bytes -> M=? A)
      (c : Raw_context.(Raw_context_intf.T.t)) (v : V.(Storage_sigs.VALUE.t))
      : M=? (A * bytes) :=
      let bytes_value := to_bytes v in
      let len := Bytes.length bytes_value in
      let=? c :=
        return=
          (Raw_context.(Raw_context_intf.T.consume_gas) c
            (Storage_costs.write_access len)) in
      let=? c := set c len_name (encode_len_value bytes_value) in
      return=? (c, bytes_value).
    
    Definition consume_remove_gas `{FArgs} {A : Set}
      (del : Raw_context.(Raw_context_intf.T.t) -> list string -> M=? A)
      (c : Raw_context.(Raw_context_intf.T.t)) : M=? A :=
      let=? c :=
        return=
          (Raw_context.(Raw_context_intf.T.consume_gas) c
            (Storage_costs.write_access 0)) in
      del c len_name.
    
    Definition mem `{FArgs} (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
      : M=? (Raw_context.root * bool) :=
      let=? c := return= (consume_mem_gas (_pack s i)) in
      let= res := Raw_context.(Raw_context_intf.T.mem) c data_name in
      return=? ((Raw_context.(Raw_context_intf.T.project) c), res).
    
    Definition get `{FArgs} (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
      : M=? (Raw_context.root * V.(Storage_sigs.VALUE.t)) :=
      let=? c :=
        consume_read_gas Raw_context.(Raw_context_intf.T.get) (_pack s i) in
      let=? b_value := Raw_context.(Raw_context_intf.T.get) c data_name in
      let key_value (function_parameter : unit) : Raw_context.key :=
        let '_ := function_parameter in
        Raw_context.(Raw_context_intf.T.absolute_key) c data_name in
      return=
        (let? v := of_bytes key_value b_value in
        return? ((Raw_context.(Raw_context_intf.T.project) c), v)).
    
    Definition find `{FArgs} (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
      : M=? (Raw_context.root * option V.(Storage_sigs.VALUE.t)) :=
      let=? c := return= (consume_mem_gas (_pack s i)) in
      let '(s, _) := unpack c in
      let= _exists := Raw_context.(Raw_context_intf.T.mem) (_pack s i) data_name
        in
      if _exists then
        let=? '(s, v) := get s i in
        return=? (s, (Some v))
      else
        return=? ((C.(Raw_context_intf.T.project) s), None).
    
    Definition update `{FArgs}
      (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
      (v : V.(Storage_sigs.VALUE.t)) : M=? (Raw_context.root * int) :=
      let=? '(prev_size, _) := existing_size (_pack s i) in
      let=? '(c, bytes_value) :=
        consume_write_gas Raw_context.(Raw_context_intf.T.update) (_pack s i) v
        in
      let=? c := Raw_context.(Raw_context_intf.T.update) c data_name bytes_value
        in
      let size_diff := (Bytes.length bytes_value) -i prev_size in
      return=? ((Raw_context.(Raw_context_intf.T.project) c), size_diff).
    
    Definition init_value `{FArgs}
      (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
      (v : V.(Storage_sigs.VALUE.t)) : M=? (Raw_context.root * int) :=
      let=? '(c, bytes_value) :=
        consume_write_gas Raw_context.(Raw_context_intf.T.init_value)
          (_pack s i) v in
      let=? c :=
        Raw_context.(Raw_context_intf.T.init_value) c data_name bytes_value in
      let size_value := Bytes.length bytes_value in
      return=? ((Raw_context.(Raw_context_intf.T.project) c), size_value).
    
    Definition add `{FArgs}
      (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
      (v : V.(Storage_sigs.VALUE.t)) : M=? (Raw_context.root * int * bool) :=
      let add {A : Set}
        (c : Raw_context.(Raw_context_intf.T.t)) (k : Raw_context.key)
        (v : Raw_context.value)
        : M= (Pervasives.result Raw_context.(Raw_context_intf.T.t) A) :=
        Error_monad.op_gtpipeeq (Raw_context.(Raw_context_intf.T.add) c k v)
          Error_monad.ok in
      let=? '(prev_size, existed) := existing_size (_pack s i) in
      let=? '(c, bytes_value) := consume_write_gas add (_pack s i) v in
      let=? c := add c data_name bytes_value in
      let size_diff := (Bytes.length bytes_value) -i prev_size in
      return=?
        ((Raw_context.(Raw_context_intf.T.project) c), size_diff, existed).
    
    Definition remove `{FArgs} (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
      : M=? (Raw_context.root * int * bool) :=
      let remove {A : Set}
        (c : Raw_context.(Raw_context_intf.T.t)) (k : Raw_context.key)
        : M= (Pervasives.result Raw_context.(Raw_context_intf.T.t) A) :=
        Error_monad.op_gtpipeeq (Raw_context.(Raw_context_intf.T.remove) c k)
          Error_monad.ok in
      let=? '(prev_size, existed) := existing_size (_pack s i) in
      let=? c := consume_remove_gas remove (_pack s i) in
      let=? c := remove c data_name in
      return=?
        ((Raw_context.(Raw_context_intf.T.project) c), prev_size, existed).
    
    Definition remove_existing `{FArgs}
      (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
      : M=? (Raw_context.root * int) :=
      let=? '(prev_size, _) := existing_size (_pack s i) in
      let=? c :=
        consume_remove_gas Raw_context.(Raw_context_intf.T.remove_existing)
          (_pack s i) in
      let=? c := Raw_context.(Raw_context_intf.T.remove_existing) c data_name in
      return=? ((Raw_context.(Raw_context_intf.T.project) c), prev_size).
    
    Definition add_or_remove `{FArgs}
      (s : C.(Raw_context_intf.T.t)) (i : I.(INDEX.t))
      (v : option V.(Storage_sigs.VALUE.t))
      : M=? (Raw_context.root * int * bool) :=
      match v with
      | None => remove s i
      | Some v => add s i v
      end.
    
    Definition functor `{FArgs} :=
      {|
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.mem := mem;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.get := get;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.find := find;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.update :=
          update;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.init_value :=
          init_value;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.add := add;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.add_or_remove :=
          add_or_remove;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.remove_existing :=
          remove_existing;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.remove :=
          remove
      |}.
  End Make_carbonated_map.
  Definition Make_carbonated_map `{FArgs} {V_t : Set}
    (N : Storage_sigs.NAME) (V : Storage_sigs.VALUE (t := V_t))
    : Storage_sigs.Non_iterable_indexed_carbonated_data_storage (t := t)
      (key := key) (value := V.(Storage_sigs.VALUE.t)) :=
    let '_ := Make_carbonated_map.Build_FArgs N V in
    Make_carbonated_map.functor.
  
  Definition functor `{FArgs} :=
    {|
      Storage_sigs.Indexed_raw_context.clear := clear;
      Storage_sigs.Indexed_raw_context.fold_keys _ := fold_keys;
      Storage_sigs.Indexed_raw_context.keys := keys;
      Storage_sigs.Indexed_raw_context.remove := remove;
      Storage_sigs.Indexed_raw_context.copy := copy;
      Storage_sigs.Indexed_raw_context.Make_set := Make_set;
      Storage_sigs.Indexed_raw_context.Make_map _ := Make_map;
      Storage_sigs.Indexed_raw_context.Make_carbonated_map _ :=
        Make_carbonated_map;
      Storage_sigs.Indexed_raw_context.Raw_context := Raw_context
    |}.
End Make_indexed_subcontext.
Definition Make_indexed_subcontext {C_t I_t : Set} {I_ipath : Set -> Set}
  (C : Raw_context.T (t := C_t)) (I : INDEX (t := I_t) (ipath := I_ipath))
  : Storage_sigs.Indexed_raw_context (t := C.(Raw_context_intf.T.t))
    (key := I.(INDEX.t)) (ipath := fun (a : Set) => I.(INDEX.ipath) a) :=
  let '_ := Make_indexed_subcontext.Build_FArgs C I in
  Make_indexed_subcontext.functor.

Module WRAPPER.
  Record signature {t key : Set} : Set := {
    t := t;
    key := key;
    wrap : t -> key;
    unwrap : key -> option t;
  }.
End WRAPPER.
Definition WRAPPER := @WRAPPER.signature.
Arguments WRAPPER {_ _}.

Module Wrap_indexed_data_storage.
  Class FArgs {C_t C_key C_value K_t : Set} := {
    C :
      Storage_sigs.Indexed_data_storage (t := C_t) (key := C_key)
        (value := C_value);
    K : WRAPPER (t := K_t) (key := C.(Storage_sigs.Indexed_data_storage.key));
  }.
  Arguments Build_FArgs {_ _ _ _}.
  
  Definition t `{FArgs} : Set := C.(Storage_sigs.Indexed_data_storage.t).
  
  Definition context `{FArgs} : Set := C.(Storage_sigs.Indexed_data_storage.t).
  
  Definition key `{FArgs} : Set := K.(WRAPPER.t).
  
  Definition value `{FArgs} : Set :=
    C.(Storage_sigs.Indexed_data_storage.value).
  
  Definition mem `{FArgs}
    (ctxt : C.(Storage_sigs.Indexed_data_storage.t)) (k : K.(WRAPPER.t))
    : M= bool :=
    C.(Storage_sigs.Indexed_data_storage.mem) ctxt (K.(WRAPPER.wrap) k).
  
  Definition get `{FArgs}
    (ctxt : C.(Storage_sigs.Indexed_data_storage.t)) (k : K.(WRAPPER.t))
    : M=? C.(Storage_sigs.Indexed_data_storage.value) :=
    C.(Storage_sigs.Indexed_data_storage.get) ctxt (K.(WRAPPER.wrap) k).
  
  Definition find `{FArgs}
    (ctxt : C.(Storage_sigs.Indexed_data_storage.t)) (k : K.(WRAPPER.t))
    : M=? (option C.(Storage_sigs.Indexed_data_storage.value)) :=
    C.(Storage_sigs.Indexed_data_storage.find) ctxt (K.(WRAPPER.wrap) k).
  
  Definition update `{FArgs}
    (ctxt : C.(Storage_sigs.Indexed_data_storage.t)) (k : K.(WRAPPER.t))
    (v : C.(Storage_sigs.Indexed_data_storage.value)) : M=? Raw_context.t :=
    C.(Storage_sigs.Indexed_data_storage.update) ctxt (K.(WRAPPER.wrap) k) v.
  
  Definition init_value `{FArgs}
    (ctxt : C.(Storage_sigs.Indexed_data_storage.t)) (k : K.(WRAPPER.t))
    (v : C.(Storage_sigs.Indexed_data_storage.value)) : M=? Raw_context.t :=
    C.(Storage_sigs.Indexed_data_storage.init_value) ctxt (K.(WRAPPER.wrap) k) v.
  
  Definition add `{FArgs}
    (ctxt : C.(Storage_sigs.Indexed_data_storage.t)) (k : K.(WRAPPER.t))
    (v : C.(Storage_sigs.Indexed_data_storage.value)) : M= Raw_context.t :=
    C.(Storage_sigs.Indexed_data_storage.add) ctxt (K.(WRAPPER.wrap) k) v.
  
  Definition add_or_remove `{FArgs}
    (ctxt : C.(Storage_sigs.Indexed_data_storage.t)) (k : K.(WRAPPER.t))
    (v : option C.(Storage_sigs.Indexed_data_storage.value))
    : M= Raw_context.t :=
    C.(Storage_sigs.Indexed_data_storage.add_or_remove) ctxt
      (K.(WRAPPER.wrap) k) v.
  
  Definition remove_existing `{FArgs}
    (ctxt : C.(Storage_sigs.Indexed_data_storage.t)) (k : K.(WRAPPER.t))
    : M=? Raw_context.t :=
    C.(Storage_sigs.Indexed_data_storage.remove_existing) ctxt
      (K.(WRAPPER.wrap) k).
  
  Definition remove `{FArgs}
    (ctxt : C.(Storage_sigs.Indexed_data_storage.t)) (k : K.(WRAPPER.t))
    : M= Raw_context.t :=
    C.(Storage_sigs.Indexed_data_storage.remove) ctxt (K.(WRAPPER.wrap) k).
  
  Definition clear `{FArgs} (ctxt : C.(Storage_sigs.Indexed_data_storage.t))
    : M= Raw_context.t := C.(Storage_sigs.Indexed_data_storage.clear) ctxt.
  
  Definition fold `{FArgs} {A : Set}
    (ctxt : C.(Storage_sigs.Indexed_data_storage.t)) (init_value : A)
    (f :
      K.(WRAPPER.t) -> C.(Storage_sigs.Indexed_data_storage.value) -> A -> M= A)
    : M= A :=
    C.(Storage_sigs.Indexed_data_storage.fold) ctxt init_value
      (fun (k : C.(Storage_sigs.Indexed_data_storage.key)) =>
        fun (v : C.(Storage_sigs.Indexed_data_storage.value)) =>
          fun (acc_value : A) =>
            match K.(WRAPPER.unwrap) k with
            | None => return= acc_value
            | Some k => f k v acc_value
            end).
  
  Definition bindings `{FArgs} (s : C.(Storage_sigs.Indexed_data_storage.t))
    : M= (list (K.(WRAPPER.t) * C.(Storage_sigs.Indexed_data_storage.value))) :=
    fold s nil
      (fun (p_value : K.(WRAPPER.t)) =>
        fun (v : C.(Storage_sigs.Indexed_data_storage.value)) =>
          fun (acc_value :
            list (K.(WRAPPER.t) * C.(Storage_sigs.Indexed_data_storage.value)))
            => return= (cons (p_value, v) acc_value)).
  
  Definition fold_keys `{FArgs} {A : Set}
    (s : C.(Storage_sigs.Indexed_data_storage.t)) (init_value : A)
    (f : K.(WRAPPER.t) -> A -> M= A) : M= A :=
    C.(Storage_sigs.Indexed_data_storage.fold_keys) s init_value
      (fun (k : C.(Storage_sigs.Indexed_data_storage.key)) =>
        fun (acc_value : A) =>
          match K.(WRAPPER.unwrap) k with
          | None => return= acc_value
          | Some k => f k acc_value
          end).
  
  Definition keys `{FArgs} (s : C.(Storage_sigs.Indexed_data_storage.t))
    : M= (list K.(WRAPPER.t)) :=
    fold_keys s nil
      (fun (p_value : K.(WRAPPER.t)) =>
        fun (acc_value : list K.(WRAPPER.t)) => return= (cons p_value acc_value)).
  
  Definition functor `{FArgs} :=
    {|
      Storage_sigs.Indexed_data_storage.mem := mem;
      Storage_sigs.Indexed_data_storage.get := get;
      Storage_sigs.Indexed_data_storage.find := find;
      Storage_sigs.Indexed_data_storage.update := update;
      Storage_sigs.Indexed_data_storage.init_value := init_value;
      Storage_sigs.Indexed_data_storage.add := add;
      Storage_sigs.Indexed_data_storage.add_or_remove := add_or_remove;
      Storage_sigs.Indexed_data_storage.remove_existing := remove_existing;
      Storage_sigs.Indexed_data_storage.remove := remove;
      Storage_sigs.Indexed_data_storage.clear := clear;
      Storage_sigs.Indexed_data_storage.keys := keys;
      Storage_sigs.Indexed_data_storage.bindings := bindings;
      Storage_sigs.Indexed_data_storage.fold _ := fold;
      Storage_sigs.Indexed_data_storage.fold_keys _ := fold_keys
    |}.
End Wrap_indexed_data_storage.
Definition Wrap_indexed_data_storage {C_t C_key C_value K_t : Set}
  (C :
    Storage_sigs.Indexed_data_storage (t := C_t) (key := C_key)
      (value := C_value))
  (K : WRAPPER (t := K_t) (key := C.(Storage_sigs.Indexed_data_storage.key)))
  : Storage_sigs.Indexed_data_storage
    (t := C.(Storage_sigs.Indexed_data_storage.t)) (key := K.(WRAPPER.t))
    (value := C.(Storage_sigs.Indexed_data_storage.value)) :=
  let '_ := Wrap_indexed_data_storage.Build_FArgs C K in
  Wrap_indexed_data_storage.functor.
