Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require TezosOfOCaml.Proto_alpha.Alpha_context.

Inductive kind : Set :=
| Int_kind : kind
| String_kind : kind
| Bytes_kind : kind
| Prim_kind : kind
| Seq_kind : kind.

Definition unparsed_stack_ty : Set :=
  list (Alpha_context.Script.expr * Alpha_context.Script.annot).

Definition type_map : Set :=
  list (int * (unparsed_stack_ty * unparsed_stack_ty)).

Module Ill_typed_view.
  Record record : Set := Build {
    loc : Alpha_context.Script.location;
    actual : unparsed_stack_ty;
    expected : unparsed_stack_ty }.
  Definition with_loc loc (r : record) :=
    Build loc r.(actual) r.(expected).
  Definition with_actual actual (r : record) :=
    Build r.(loc) actual r.(expected).
  Definition with_expected expected (r : record) :=
    Build r.(loc) r.(actual) expected.
End Ill_typed_view.
Definition Ill_typed_view := Ill_typed_view.record.
