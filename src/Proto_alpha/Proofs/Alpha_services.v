Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_services.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Proofs.Nonce_hash.
Require TezosOfOCaml.Proto_alpha.Proofs.Seed_repr.

Module Nonce.
  Module Info.
    Module Valid.
      Definition t (info : Alpha_services.Nonce.info) : Prop :=
        match info with
        | Alpha_services.Nonce.Revealed nonce => Seed_repr.Nonce.Valid.t nonce
        | _ => True
        end.
    End Valid.

    Lemma encoding_is_valid
      : Data_encoding.Valid_on.t Valid.t Alpha_services.Nonce.info_encoding.
      eapply Data_encoding.Valid_on.implies.
      eapply Data_encoding.Valid_on.union.
      (repeat econstructor; simpl; try intuition congruence);
        try (intro x; destruct x; reflexivity);
        try apply Data_encoding.Valid_on.obj1.
      apply Data_encoding.Valid.empty.
      apply Nonce_hash.encoding_is_valid.
      apply Seed_repr.Nonce.encoding_is_valid.
      intros x H_x.
      destruct x; tauto.
    Qed.
  End Info.
End Nonce.
