Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Require Import Lia.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Vote_storage.

Require TezosOfOCaml.Proto_alpha.Proofs.Storage.
Require Import TezosOfOCaml.Proto_alpha.Proofs.Storage_sigs.

Definition simple_recorded_proposal_count_for_delegate ctxt proposer : int :=
  let count :=
    Indexed_data_storage.Op.find
      (Storage.Eq.Votes.Proposals_count.parse ctxt) proposer in
  Option.value count 0.

Lemma simple_recorded_proposal_count_for_delegate_eq ctxt proposer
  : Vote_storage.recorded_proposal_count_for_delegate ctxt proposer =
    (return=? simple_recorded_proposal_count_for_delegate ctxt proposer).
  unfold simple_recorded_proposal_count_for_delegate,
    Vote_storage.recorded_proposal_count_for_delegate.
  now rewrite Storage.Eq.Votes.Proposals_count.eq.(
    Indexed_data_storage.Eq.find).
Qed.

Definition simple_record_proposal ctxt proposal proposer : Raw_context.t :=
  let count := simple_recorded_proposal_count_for_delegate ctxt proposer in
  let ctxt :=
    Storage.Eq.Votes.Proposals_count.apply ctxt
      (Indexed_data_storage.Op.add proposer (count +i 1)) in
  Storage.Eq.Votes.Proposals.apply ctxt
    (Data_set_storage.Op.add (proposal, proposer)).

Lemma simple_record_proposal_eq ctxt proposal proposer
  : Vote_storage.record_proposal ctxt proposal proposer =
    return=? (simple_record_proposal ctxt proposal proposer).
  unfold simple_record_proposal, Vote_storage.record_proposal.
  rewrite simple_recorded_proposal_count_for_delegate_eq; simpl.
  rewrite Storage.Eq.Votes.Proposals_count.eq.(
    Indexed_data_storage.Eq.add); simpl.
  now rewrite Storage.Eq.Votes.Proposals.eq.(
    Data_set_storage.Eq.add).
Qed.

(** We write this proof as an example of the technique of using simulations to
    represent the storage. We apply some lemmas to reduce the problem to
    reasonings over maps. *)
Lemma record_proposal_not_idempotent ctxt proposal proposer
  : let ctxt' := simple_record_proposal ctxt proposal proposer in
    let ctxt'' := simple_record_proposal ctxt' proposal proposer in
    ctxt <> ctxt''.
  intros ctxt' ctxt'' H.
  assert (H_proposals_count :
    Indexed_data_storage.Op.find
      (Storage.Eq.Votes.Proposals_count.parse ctxt)
      proposer <>
    Indexed_data_storage.Op.find
      (Storage.Eq.Votes.Proposals_count.parse ctxt'')
      proposer
  ).
  { unfold ctxt'', ctxt', simple_record_proposal,
      Storage.Eq.Votes.Proposals_count.apply,
      Storage.Eq.Votes.Proposals_count.parse,
      Storage.Eq.Votes.Proposals.apply,
      simple_recorded_proposal_count_for_delegate.
    repeat (rewrite Storage.parse_apply; simpl).
    unfold Storage.Eq.Votes.Proposals_count.parse.
    repeat (rewrite Storage.parse_apply; simpl).
    set (proposals_count := _.(Storage.Votes.proposals_count)).
    unfold Indexed_data_storage.Op.find, Indexed_data_storage.State.Map; simpl.
    do 2 rewrite Map.find_add; simpl.
    destruct (Make.find _ _); simpl; try congruence.
    assert (i <> i +i 1 +i 1) by (autounfold with tezos_z in *; lia).
    congruence.
  }
  { congruence. }
Qed.
