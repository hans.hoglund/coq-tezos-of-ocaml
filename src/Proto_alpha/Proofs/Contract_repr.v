Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Contract_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Compare.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Signature.
Require TezosOfOCaml.Proto_alpha.Proofs.Contract_hash.

Lemma compare_is_valid
  : Compare.Valid.t (Compare.wrap_compare Contract_repr.compare).
  constructor; unfold Compare.wrap_compare, Contract_repr.compare; simpl;
    unfold id; change (_.(Compare.COMPARABLE.t)) with Contract_repr.t;
    intros;
    try match goal with
    | [H : _ = _ |- _] => now rewrite H
    end;
    destruct_all Contract_repr.t; simpl in *; try easy;
    try f_equal;
    try apply Signature.Public_key_hash_compare_is_valid;
    try apply Contract_hash.compare_is_valid;
    try easy;
    try eapply
      (Signature.Public_key_hash_compare_is_valid.(Compare.Pre_valid.trans));
    try eapply Contract_hash.compare_is_valid;
    match goal with
    | [H : _ = 1 |- _] => exact H
    end.
Qed.
