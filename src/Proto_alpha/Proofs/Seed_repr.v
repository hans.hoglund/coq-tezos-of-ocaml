Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Seed_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Module Nonce.
  Module Valid.
    Definition t (nonce : Seed_repr.nonce) : Prop :=
      Bytes.length nonce = Constants_repr.nonce_length.
  End Valid.

  Lemma encoding_is_valid
    : Data_encoding.Valid_on.t Valid.t Seed_repr.nonce_encoding.
    apply Data_encoding.Valid_on.Fixed.bytes_value.
  Qed.
End Nonce.
