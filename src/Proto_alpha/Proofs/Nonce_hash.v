Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Nonce_hash.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Axiom encoding_is_valid : Data_encoding.Valid.t Nonce_hash.encoding.
