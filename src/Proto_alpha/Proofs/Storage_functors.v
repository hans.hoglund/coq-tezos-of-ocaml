Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require Import TezosOfOCaml.Proto_alpha.Storage_functors.
Require Import TezosOfOCaml.Proto_alpha.Storage_sigs.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Context.
Require TezosOfOCaml.Proto_alpha.Proofs.Raw_context.
Require Import TezosOfOCaml.Proto_alpha.Proofs.Storage_sigs.

Module ENCODER.
  Module Valid.
    Record property {t : Set} {E : ENCODER (t := t)} : Prop := {
      of_bytes_to_bytes path value :
        E.(ENCODER.of_bytes) path (E.(ENCODER.to_bytes) value) = return? value;
    }.
  End Valid.
End ENCODER.

Module Make_single_data_storage.
  Lemma of_nil_sub_context_eq {value : Set}
    (R : REGISTER)
    (N : NAME)
    (V : VALUE (t := value))
    : let N_nil := {| Storage_sigs.NAME.name := [] |} in
      let C := Storage_functors.Make_subcontext R Raw_context.Root.M N_nil in
      Make_single_data_storage R C N V =
      Make_single_data_storage R Raw_context.Root.M N V.
    reflexivity.
  Qed.

  Lemma eq {value : Set}
    (R : REGISTER)
    (N1 N2 : NAME)
    (V : VALUE (t := value))
    : let path := op_at N1.(NAME.name) N2.(NAME.name) in
      Single_data_storage.Eq.t
        (let C := Storage_functors.Make_subcontext R Raw_context.Root.M N1 in
        Make_single_data_storage R C N2 V)
        (Single_data_storage.Make
          (fun ctxt =>
            let value := Raw.find (Raw_context.context_value ctxt) path in
            Single_data_storage.State.parse V.(VALUE.encoding) value
          )
          axiom
          path
        ).
    constructor; intros; simpl;
      unfold
        Make_single_data_storage.mem, Single_data_storage.Op.mem,
        Make_single_data_storage.get, Single_data_storage.Op.get,
        Make_single_data_storage.find, Single_data_storage.Op.find;
      simpl;
      unfold
        Make_subcontext.mem,
        Make_subcontext.get,
        Make_subcontext.find,
        Make_subcontext.to_key;
      simpl.
    { unfold Raw_context.mem, Context.mem.
      rewrite Context.Raw.mem_find; simpl.
      destruct (Context.Raw.find _ _); trivial; simpl.
      now destruct (Binary.of_bytes_opt _ _).
    }
    { unfold Raw_context.get, Context.find.
      destruct (Context.Raw.find _ _); trivial; simpl.
      unfold Make_single_data_storage.of_bytes; simpl.
      unfold Make_encoder.of_bytes; simpl.
      now destruct (Binary.of_bytes_opt _ _).
    }
    { destruct (Context.Raw.find _ _); trivial; simpl.
      unfold Make_single_data_storage.of_bytes; simpl.
      unfold Make_encoder.of_bytes; simpl.
      now destruct (Binary.of_bytes_opt _ _).
    }
    { apply axiom. }
    { apply axiom. }
    { apply axiom. }
    { apply axiom. }
    { apply axiom. }
    { apply axiom. }
  Qed.
End Make_single_data_storage.
