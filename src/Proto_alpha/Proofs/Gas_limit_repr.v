Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Require Import Lia.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Gas_limit_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Option.
Require TezosOfOCaml.Proto_alpha.Proofs.Saturation_repr.

Module Cost.
  Module Valid.
    Definition t (x : Gas_limit_repr.cost) : Prop :=
      Saturation_repr.Valid.t x.
  End Valid.
End Cost.

Lemma raw_consume_is_valid gas_counter cost
  : Cost.Valid.t cost ->
    Option.post_when_success (Gas_limit_repr.raw_consume gas_counter cost)
      (fun gas_counter' =>
        Gas_limit_repr.Arith.op_lteq gas_counter' gas_counter = true).
  unfold Option.post_when_success, Gas_limit_repr.raw_consume,
    Gas_limit_repr.Arith.sub_opt,
    Gas_limit_repr.cost_to_milligas, Gas_limit_repr.S.sub_opt,
    Cost.Valid.t, Saturation_repr.Valid.t, Saturation_repr.Valid.check.
  cbn; autounfold with tezos_z.
  match goal with
  | [|- context[if ?e then _ else _]] => destruct e eqn:H_eq; trivial
  end.
  lia.
Qed.
