Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Require Import Lia.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Raw_context.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Error_monad.
Require TezosOfOCaml.Proto_alpha.Proofs.Gas_limit_repr.

(** A record version of the root context, corresponding to the `Raw_context.v`
    file. *)
Module Root.
  Definition M : Raw_context.T (t := Raw_context.t) :=
    {|
      Raw_context_intf.T.mem := Raw_context.mem;
      Raw_context_intf.T.mem_tree := Raw_context.mem_tree;
      Raw_context_intf.T.get := Raw_context.get;
      Raw_context_intf.T.get_tree := Raw_context.get_tree;
      Raw_context_intf.T.find := Raw_context.find;
      Raw_context_intf.T.find_tree := Raw_context.find_tree;
      Raw_context_intf.T.list_value := Raw_context.list_value;
      Raw_context_intf.T.init_value := Raw_context.init_value;
      Raw_context_intf.T.init_tree := Raw_context.init_tree;
      Raw_context_intf.T.update := Raw_context.update;
      Raw_context_intf.T.update_tree := Raw_context.update_tree;
      Raw_context_intf.T.add := Raw_context.add;
      Raw_context_intf.T.add_tree := Raw_context.add_tree;
      Raw_context_intf.T.remove := Raw_context.remove;
      Raw_context_intf.T.remove_existing := Raw_context.remove_existing;
      Raw_context_intf.T.remove_existing_tree := Raw_context.remove_existing_tree;
      Raw_context_intf.T.add_or_remove := Raw_context.add_or_remove;
      Raw_context_intf.T.add_or_remove_tree := Raw_context.add_or_remove_tree;
      Raw_context_intf.T.fold _ := Raw_context.fold;
      Raw_context_intf.T.Tree := Raw_context.Tree;
      Raw_context_intf.T.project := Raw_context.project;
      Raw_context_intf.T.absolute_key := Raw_context.absolute_key;
      Raw_context_intf.T.consume_gas := Raw_context.consume_gas;
      Raw_context_intf.T.check_enough_gas := Raw_context.check_enough_gas;
      Raw_context_intf.T.description := Raw_context.description;
    |}.
End Root.

Lemma set_gas_limit_is_valid ctxt remaining
  : let ctxt := Raw_context.set_gas_limit ctxt remaining in
    Raw_context.unlimited_operation_gas ctxt = false /\
    Raw_context.remaining_operation_gas ctxt = remaining.
  split; reflexivity.
Qed.

Lemma set_gas_unlimited_is_valid ctxt
  : let ctxt := Raw_context.set_gas_unlimited ctxt in
    Raw_context.unlimited_operation_gas ctxt = true.
  reflexivity.
Qed.

Module Consume_gas.
  (** Apply a list of gas consume operations, until it fails. In case of
      failure, return [None]. *)
  Fixpoint apply_costs ctxt costs : option Raw_context.t :=
    match costs with
    | [] => Some ctxt
    | cost :: costs =>
      match Raw_context.consume_gas ctxt cost with
      | Pervasives.Ok ctxt => apply_costs ctxt costs
      | Pervasives.Error _ => None
      end
    end.

  (** Project the fields of the context corresponding to the gas status. *)
  Definition extract_gas_status ctxt : Gas_limit_repr.Arith.fp * bool :=
    (Raw_context.remaining_operation_gas ctxt,
      Raw_context.unlimited_operation_gas ctxt).

  (** Equality check between two contexts, to compare the gas part. *)
  Definition eq_on_gas_status (ctxt1 ctxt2 : Raw_context.t) : Prop :=
    extract_gas_status ctxt1 = extract_gas_status ctxt2.

  Definition eq_on_gas_status_option (ctxt1 ctxt2 : option Raw_context.t)
    : Prop :=
    Option.map extract_gas_status ctxt1 = Option.map extract_gas_status ctxt2.

  Fixpoint apply_costs_eq {ctxt1 ctxt2 costs}
    : eq_on_gas_status ctxt1 ctxt2 ->
      eq_on_gas_status_option
        (apply_costs ctxt1 costs) (apply_costs ctxt2 costs).
    destruct costs; simpl; cbv - [apply_costs Gas_limit_repr.Arith.sub_opt];
      try congruence.
    intro H; inversion H.
    destruct (Gas_limit_repr.Arith.sub_opt _ _).
    { apply apply_costs_eq.
      cbv; congruence.
    }
    { match goal with
      | [|- context[if ?e then _ else _]] =>
        destruct e eqn:H_eq; rewrite <- H_eq in *; clear H_eq
      end; trivial.
      apply apply_costs_eq; cbv; congruence.
    }
  Qed.

  Module Valid.
    (** We consider a gas consumption to be valid when it can be deduced from
        the application of a list of gas consumptions. *)
    Definition t initial_ctxt final_ctxt costs : Prop :=
      eq_on_gas_status_option
        (apply_costs initial_ctxt costs) (Some final_ctxt).

    (** Not changing the context is a valid gas consumption. *)
    Lemma refl ctxt : t ctxt ctxt [].
      reflexivity.
    Qed.

    (** Transitivity rule. *)
    Fixpoint trans ctxt1 ctxt2 ctxt3 costs12 costs23
      : t ctxt1 ctxt2 costs12 -> t ctxt2 ctxt3 costs23 ->
        t ctxt1 ctxt3 (List.app costs12 costs23).
      cbv - [apply_costs extract_gas_status List.app].
      destruct costs12; simpl; intros H12 H23.
      { etransitivity;
          try apply (apply_costs_eq (ctxt2 := ctxt2)); try congruence;
          trivial.
      }
      { destruct (Raw_context.consume_gas _ _) as [ctxt1'|]; try congruence.
        now apply trans with (ctxt2 := ctxt2).
      }
    Qed.

    (** Calling the consume gas operation with a valid cost does a valid gas
        consumption. *)
    Lemma consume_gas ctxt cost
      : Error_monad.post_when_success
          (Raw_context.consume_gas ctxt cost)
          (fun ctxt' => t ctxt ctxt' [cost]).
      destruct (Raw_context.consume_gas _ _) eqn:H_eq; unfold t; simpl; trivial.
      now rewrite H_eq.
    Qed.
  End Valid.
End Consume_gas.
