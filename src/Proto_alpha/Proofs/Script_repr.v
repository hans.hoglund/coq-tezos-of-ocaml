Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Require Import Lia.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Script_repr.

Require TezosOfOCaml.Proto_alpha.Proofs.Gas_limit_repr.

Lemma deserialization_cost_estimated_from_bytes_is_valid length
  : Gas_limit_repr.Cost.Valid.t
    (Script_repr.deserialization_cost_estimated_from_bytes length).
  unfold Script_repr.deserialization_cost_estimated_from_bytes.
  apply Saturation_repr.mul_is_valid.
  reflexivity.
  apply Saturation_repr.safe_int_is_valid.
Qed.

Lemma force_decode_cost_is_valid {a : Set} (lexpr : Data_encoding.lazy_t a)
  : Gas_limit_repr.Cost.Valid.t (Script_repr.force_decode_cost lexpr).
  unfold Script_repr.force_decode_cost, Data_encoding.apply_lazy.
  destruct lexpr; try reflexivity.
  apply deserialization_cost_estimated_from_bytes_is_valid.
Qed.

(** A simpler implementation of [Script_repr.micheline_nodes]. *)
Module Reference_micheline_nodes.
  Reserved Notation "'micheline_nodes_list".

  Fixpoint micheline_nodes (node : Script_repr.node) : int :=
    let micheline_nodes_list := 'micheline_nodes_list in
    match node with
    | Micheline.Int _ _ | Micheline.String _ _ | Micheline.Bytes _ _ => 1
    | Micheline.Prim _ _ subterms _ | Micheline.Seq _ subterms =>
      micheline_nodes_list subterms +i 1
    end

  where "'micheline_nodes_list" :=
    (fix micheline_nodes_list
      (subterms : list Script_repr.node) {struct subterms} : int :=
      match subterms with
      | [] => 0
      | cons n nodes => micheline_nodes n +i micheline_nodes_list nodes
      end).

  Definition micheline_nodes_list := 'micheline_nodes_list.
End Reference_micheline_nodes.

(** How to unfold the continuation of [Script_repr.micheline_fold_aux]. *)
Fixpoint micheline_fold_aux_continuation {A : Set}
  (node : Script_repr.node) (f : A -> Script_repr.node -> A) {struct node}
  : forall (acc_value : A) {B : Set} (k : A -> B),
    Script_repr.micheline_fold_aux node f acc_value k =
    k (Script_repr.micheline_fold_aux node f acc_value id).
  assert (micheline_fold_nodes_continuation :
    forall (subterms : list Script_repr.node) (acc_value : A)
      {B : Set} (k : A -> B),
    Script_repr.micheline_fold_nodes subterms f acc_value k =
    k (Script_repr.micheline_fold_nodes subterms f acc_value id)
  ).
  { induction subterms; intros; try reflexivity; simpl.
    match goal with
    | [H : _ |- _] => rewrite H
    end.
    rewrite micheline_fold_aux_continuation.
    f_equal.
    match goal with
    | [H : _ |- _ = Script_repr.micheline_fold_nodes _ _ _ ?k] =>
      now rewrite (H _ _ k)
    end.
  }
  { intros.
    destruct node; try reflexivity.
    all:
      simpl;
      fold (@Script_repr.micheline_fold_nodes A B);
      now rewrite micheline_fold_nodes_continuation.
  }
Qed.

(** How to unfold the continuation of [Script_repr.micheline_fold_nodes]. *)
Fixpoint micheline_fold_nodes_continuation {A B : Set}
  (nodes : list Script_repr.node) (f : A -> Script_repr.node -> A)
  (acc_value : A) (k : A -> B) {struct nodes}
  : Script_repr.micheline_fold_nodes nodes f acc_value k =
    k (Script_repr.micheline_fold_nodes nodes f acc_value id).
  destruct nodes; try reflexivity; simpl.
  rewrite micheline_fold_nodes_continuation.
  rewrite micheline_fold_aux_continuation.
  f_equal.
  match goal with
  | [|- _ = Script_repr.micheline_fold_nodes _ _ _ ?k'] =>
    now rewrite micheline_fold_nodes_continuation with (k := k')
  end.
Qed.

Fixpoint micheline_nodes_like_reference_aux (node : Script_repr.node) acc
  : Script_repr.micheline_fold_aux node (fun acc '_ => acc +i 1) acc id =
    Reference_micheline_nodes.micheline_nodes node +i acc.
  assert (micheline_nodes_list_like_reference :
    forall (subterms : list Script_repr.node) acc,
      Script_repr.micheline_fold_nodes subterms (fun n '_ => n +i 1) acc id =
      Reference_micheline_nodes.micheline_nodes_list subterms +i acc
  ).
  { induction subterms; try reflexivity; simpl; intros.
    rewrite micheline_fold_nodes_continuation.
    rewrite micheline_fold_aux_continuation.
    unfold id; simpl.
    match goal with
    | [H : _ |- _] => rewrite H
    end.
    rewrite micheline_nodes_like_reference_aux.
    autounfold with tezos_z; lia.
  }
  { destruct node;
      try (cbv - ["+i"]; autounfold with tezos_z; lia).
    all:
      simpl;
      rewrite micheline_nodes_list_like_reference;
      cbv - ["+i"]; autounfold with tezos_z; lia.
  }
Qed.

(** [Script_repr.micheline_nodes] behaves like its reference implementation. *)
Lemma micheline_nodes_like_reference (node : Script_repr.node)
  : Script_repr.micheline_nodes node =
    Reference_micheline_nodes.micheline_nodes node.
  unfold Script_repr.micheline_nodes, Script_repr.fold.
  rewrite micheline_nodes_like_reference_aux.
  autounfold with tezos_z; lia.
Qed.
