Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Vote_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Lemma ballot_encoding_is_valid
  : Data_encoding.Valid.t Vote_repr.ballot_encoding.
  apply Data_encoding.Valid_on.splitted.
  { eapply Data_encoding.Valid_on.implies.
    { apply Data_encoding.Valid_on.string_enum. }
    { intro x; destruct x; simpl; intuition congruence. }
  }
  { eapply Data_encoding.Valid_on.implies.
    eapply Data_encoding.Valid_on.conv_with_guard.
    { intro v_a; now destruct v_a. }
    { apply Data_encoding.Valid.int8. }
    { trivial. }
  }
Qed.
