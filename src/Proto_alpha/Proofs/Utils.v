Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

(** Document the proof state by checking that a certain expression is there,
    either in the goal or the hypothesis. *)
Ltac step e :=
  match goal with
  | |- context [e] => idtac
  | _ : context [e] |- _ => idtac
  | _ => fail "expression not found in the current goal"
  end.
