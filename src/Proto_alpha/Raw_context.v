Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Blinded_public_key_hash.
Require TezosOfOCaml.Proto_alpha.Commitment_repr.
Require TezosOfOCaml.Proto_alpha.Constants_repr.
Require TezosOfOCaml.Proto_alpha.Contract_repr.
Require TezosOfOCaml.Proto_alpha.Cycle_repr.
Require TezosOfOCaml.Proto_alpha.Fitness_repr.
Require TezosOfOCaml.Proto_alpha.Gas_limit_repr.
Require TezosOfOCaml.Proto_alpha.Lazy_storage_kind.
Require TezosOfOCaml.Proto_alpha.Level_repr.
Require TezosOfOCaml.Proto_alpha.Parameters_repr.
Require TezosOfOCaml.Proto_alpha.Period_repr.
Require TezosOfOCaml.Proto_alpha.Raw_context_intf.
Require TezosOfOCaml.Proto_alpha.Raw_level_repr.
Require TezosOfOCaml.Proto_alpha.Saturation_repr.
Require TezosOfOCaml.Proto_alpha.Script_repr.
Require TezosOfOCaml.Proto_alpha.Storage_description.
Require TezosOfOCaml.Proto_alpha.Tez_repr.

Definition Int_set :=
  _Set.Make
    {|
      Compare.COMPARABLE.compare := Compare.Int.(Compare.S.compare)
    |}.

Module back.
  Record record : Set := Build {
    context : Context.t;
    constants : Constants_repr.parametric;
    cycle_eras : Level_repr.cycle_eras;
    level : Level_repr.t;
    predecessor_timestamp : Time.t;
    timestamp : Time.t;
    fitness : Int64.t;
    included_endorsements : int;
    allowed_endorsements :
      Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.t)
        (Signature.public_key * list int * bool);
    fees : Tez_repr.t;
    rewards : Tez_repr.t;
    storage_space_to_pay : option Z.t;
    allocated_contracts : option int;
    origination_nonce : option Contract_repr.origination_nonce;
    temporary_lazy_storage_ids : Lazy_storage_kind.Temp_ids.t;
    internal_nonce : int;
    internal_nonces_used : Int_set.(_Set.S.t);
    remaining_block_gas : Gas_limit_repr.Arith.fp;
    unlimited_operation_gas : bool }.
  Definition with_context context (r : record) :=
    Build context r.(constants) r.(cycle_eras) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(remaining_block_gas) r.(unlimited_operation_gas).
  Definition with_constants constants (r : record) :=
    Build r.(context) constants r.(cycle_eras) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(remaining_block_gas) r.(unlimited_operation_gas).
  Definition with_cycle_eras cycle_eras (r : record) :=
    Build r.(context) r.(constants) cycle_eras r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(remaining_block_gas) r.(unlimited_operation_gas).
  Definition with_level level (r : record) :=
    Build r.(context) r.(constants) r.(cycle_eras) level
      r.(predecessor_timestamp) r.(timestamp) r.(fitness)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(remaining_block_gas) r.(unlimited_operation_gas).
  Definition with_predecessor_timestamp predecessor_timestamp (r : record) :=
    Build r.(context) r.(constants) r.(cycle_eras) r.(level)
      predecessor_timestamp r.(timestamp) r.(fitness) r.(included_endorsements)
      r.(allowed_endorsements) r.(fees) r.(rewards) r.(storage_space_to_pay)
      r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(remaining_block_gas) r.(unlimited_operation_gas).
  Definition with_timestamp timestamp (r : record) :=
    Build r.(context) r.(constants) r.(cycle_eras) r.(level)
      r.(predecessor_timestamp) timestamp r.(fitness) r.(included_endorsements)
      r.(allowed_endorsements) r.(fees) r.(rewards) r.(storage_space_to_pay)
      r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(remaining_block_gas) r.(unlimited_operation_gas).
  Definition with_fitness fitness (r : record) :=
    Build r.(context) r.(constants) r.(cycle_eras) r.(level)
      r.(predecessor_timestamp) r.(timestamp) fitness r.(included_endorsements)
      r.(allowed_endorsements) r.(fees) r.(rewards) r.(storage_space_to_pay)
      r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(remaining_block_gas) r.(unlimited_operation_gas).
  Definition with_included_endorsements included_endorsements (r : record) :=
    Build r.(context) r.(constants) r.(cycle_eras) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness) included_endorsements
      r.(allowed_endorsements) r.(fees) r.(rewards) r.(storage_space_to_pay)
      r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(remaining_block_gas) r.(unlimited_operation_gas).
  Definition with_allowed_endorsements allowed_endorsements (r : record) :=
    Build r.(context) r.(constants) r.(cycle_eras) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness)
      r.(included_endorsements) allowed_endorsements r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(remaining_block_gas) r.(unlimited_operation_gas).
  Definition with_fees fees (r : record) :=
    Build r.(context) r.(constants) r.(cycle_eras) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness)
      r.(included_endorsements) r.(allowed_endorsements) fees r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(remaining_block_gas) r.(unlimited_operation_gas).
  Definition with_rewards rewards (r : record) :=
    Build r.(context) r.(constants) r.(cycle_eras) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) rewards
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(remaining_block_gas) r.(unlimited_operation_gas).
  Definition with_storage_space_to_pay storage_space_to_pay (r : record) :=
    Build r.(context) r.(constants) r.(cycle_eras) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      storage_space_to_pay r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(remaining_block_gas) r.(unlimited_operation_gas).
  Definition with_allocated_contracts allocated_contracts (r : record) :=
    Build r.(context) r.(constants) r.(cycle_eras) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) allocated_contracts r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(remaining_block_gas) r.(unlimited_operation_gas).
  Definition with_origination_nonce origination_nonce (r : record) :=
    Build r.(context) r.(constants) r.(cycle_eras) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) origination_nonce
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(remaining_block_gas) r.(unlimited_operation_gas).
  Definition with_temporary_lazy_storage_ids temporary_lazy_storage_ids
    (r : record) :=
    Build r.(context) r.(constants) r.(cycle_eras) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      temporary_lazy_storage_ids r.(internal_nonce) r.(internal_nonces_used)
      r.(remaining_block_gas) r.(unlimited_operation_gas).
  Definition with_internal_nonce internal_nonce (r : record) :=
    Build r.(context) r.(constants) r.(cycle_eras) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) internal_nonce r.(internal_nonces_used)
      r.(remaining_block_gas) r.(unlimited_operation_gas).
  Definition with_internal_nonces_used internal_nonces_used (r : record) :=
    Build r.(context) r.(constants) r.(cycle_eras) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) internal_nonces_used
      r.(remaining_block_gas) r.(unlimited_operation_gas).
  Definition with_remaining_block_gas remaining_block_gas (r : record) :=
    Build r.(context) r.(constants) r.(cycle_eras) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      remaining_block_gas r.(unlimited_operation_gas).
  Definition with_unlimited_operation_gas unlimited_operation_gas
    (r : record) :=
    Build r.(context) r.(constants) r.(cycle_eras) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(remaining_block_gas) unlimited_operation_gas.
End back.
Definition back := back.record.

Module t.
  Record record : Set := Build {
    remaining_operation_gas : Gas_limit_repr.Arith.fp;
    back : back }.
  Definition with_remaining_operation_gas remaining_operation_gas
    (r : record) :=
    Build remaining_operation_gas r.(back).
  Definition with_back back (r : record) :=
    Build r.(remaining_operation_gas) back.
End t.
Definition t := t.record.

Definition root : Set := t.

Definition context_value (ctxt : t) : Context.t := ctxt.(t.back).(back.context).

Definition current_level (ctxt : t) : Level_repr.t :=
  ctxt.(t.back).(back.level).

Definition storage_space_to_pay (ctxt : t) : option Z.t :=
  ctxt.(t.back).(back.storage_space_to_pay).

Definition predecessor_timestamp (ctxt : t) : Time.t :=
  ctxt.(t.back).(back.predecessor_timestamp).

Definition current_timestamp (ctxt : t) : Time.t :=
  ctxt.(t.back).(back.timestamp).

Definition current_fitness (ctxt : t) : Int64.t := ctxt.(t.back).(back.fitness).

Definition cycle_eras (ctxt : t) : Level_repr.cycle_eras :=
  ctxt.(t.back).(back.cycle_eras).

Definition constants (ctxt : t) : Constants_repr.parametric :=
  ctxt.(t.back).(back.constants).

Definition recover (ctxt : t) : Context.t := ctxt.(t.back).(back.context).

Definition fees (ctxt : t) : Tez_repr.t := ctxt.(t.back).(back.fees).

Definition origination_nonce (ctxt : t)
  : option Contract_repr.origination_nonce :=
  ctxt.(t.back).(back.origination_nonce).

Definition allowed_endorsements (ctxt : t)
  : Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.t)
    (Signature.public_key * list int * bool) :=
  ctxt.(t.back).(back.allowed_endorsements).

Definition included_endorsements (ctxt : t) : int :=
  ctxt.(t.back).(back.included_endorsements).

Definition internal_nonce (ctxt : t) : int :=
  ctxt.(t.back).(back.internal_nonce).

Definition internal_nonces_used (ctxt : t) : Int_set.(_Set.S.t) :=
  ctxt.(t.back).(back.internal_nonces_used).

Definition remaining_block_gas (ctxt : t) : Gas_limit_repr.Arith.fp :=
  ctxt.(t.back).(back.remaining_block_gas).

Definition unlimited_operation_gas (ctxt : t) : bool :=
  ctxt.(t.back).(back.unlimited_operation_gas).

Definition rewards (ctxt : t) : Tez_repr.t := ctxt.(t.back).(back.rewards).

Definition allocated_contracts (ctxt : t) : option int :=
  ctxt.(t.back).(back.allocated_contracts).

Definition temporary_lazy_storage_ids (ctxt : t)
  : Lazy_storage_kind.Temp_ids.t :=
  ctxt.(t.back).(back.temporary_lazy_storage_ids).

Definition remaining_operation_gas (ctxt : t) : Gas_limit_repr.Arith.fp :=
  ctxt.(t.remaining_operation_gas).

Definition update_remaining_operation_gas
  (ctxt : t) (remaining_operation_gas : Gas_limit_repr.Arith.fp) : t :=
  t.with_remaining_operation_gas remaining_operation_gas ctxt.

Definition update_back (ctxt : t) (back : back) : t := t.with_back back ctxt.

Definition update_remaining_block_gas
  (ctxt : t) (remaining_block_gas : Gas_limit_repr.Arith.fp) : t :=
  update_back ctxt
    (back.with_remaining_block_gas remaining_block_gas ctxt.(t.back)).

Definition update_unlimited_operation_gas
  (ctxt : t) (unlimited_operation_gas : bool) : t :=
  update_back ctxt
    (back.with_unlimited_operation_gas unlimited_operation_gas ctxt.(t.back)).

Definition update_context (ctxt : t) (context_value : Context.t) : t :=
  update_back ctxt (back.with_context context_value ctxt.(t.back)).

Definition update_constants (ctxt : t) (constants : Constants_repr.parametric)
  : t := update_back ctxt (back.with_constants constants ctxt.(t.back)).

Definition update_fitness (ctxt : t) (fitness : Int64.t) : t :=
  update_back ctxt (back.with_fitness fitness ctxt.(t.back)).

Definition update_allowed_endorsements
  (ctxt : t)
  (allowed_endorsements :
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.t)
      (Signature.public_key * list int * bool)) : t :=
  update_back ctxt
    (back.with_allowed_endorsements allowed_endorsements ctxt.(t.back)).

Definition update_rewards (ctxt : t) (rewards : Tez_repr.t) : t :=
  update_back ctxt (back.with_rewards rewards ctxt.(t.back)).

Definition raw_update_storage_space_to_pay
  (ctxt : t) (storage_space_to_pay : option Z.t) : t :=
  update_back ctxt
    (back.with_storage_space_to_pay storage_space_to_pay ctxt.(t.back)).

Definition update_allocated_contracts
  (ctxt : t) (allocated_contracts : option int) : t :=
  update_back ctxt
    (back.with_allocated_contracts allocated_contracts ctxt.(t.back)).

Definition update_origination_nonce
  (ctxt : t) (origination_nonce : option Contract_repr.origination_nonce) : t :=
  update_back ctxt (back.with_origination_nonce origination_nonce ctxt.(t.back)).

Definition update_internal_nonce (ctxt : t) (internal_nonce : int) : t :=
  update_back ctxt (back.with_internal_nonce internal_nonce ctxt.(t.back)).

Definition update_internal_nonces_used
  (ctxt : t) (internal_nonces_used : Int_set.(_Set.S.t)) : t :=
  update_back ctxt
    (back.with_internal_nonces_used internal_nonces_used ctxt.(t.back)).

Definition update_included_endorsements (ctxt : t) (included_endorsements : int)
  : t :=
  update_back ctxt
    (back.with_included_endorsements included_endorsements ctxt.(t.back)).

Definition update_fees (ctxt : t) (fees : Tez_repr.t) : t :=
  update_back ctxt (back.with_fees fees ctxt.(t.back)).

Definition update_temporary_lazy_storage_ids
  (ctxt : t) (temporary_lazy_storage_ids : Lazy_storage_kind.Temp_ids.t) : t :=
  update_back ctxt
    (back.with_temporary_lazy_storage_ids temporary_lazy_storage_ids
      ctxt.(t.back)).

Definition record_endorsement (ctxt : t) (k : Signature.public_key_hash) : t :=
  match
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.find)
      k (allowed_endorsements ctxt) with
  | None =>
    (* ❌ Assert instruction is not handled. *)
    assert t false
  | Some (_, _, true) =>
    (* ❌ Assert instruction is not handled. *)
    assert t false
  | Some (d, s, false) =>
    let ctxt :=
      update_included_endorsements ctxt
        ((included_endorsements ctxt) +i (List.length s)) in
    update_allowed_endorsements ctxt
      (Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.add)
        k (d, s, true) (allowed_endorsements ctxt))
  end.

Definition init_endorsements
  (ctxt : t)
  (allowed_endorsements' :
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.t)
      (Signature.public_key * list int * bool)) : t :=
  if
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.is_empty)
      allowed_endorsements'
  then
    (* ❌ Assert instruction is not handled. *)
    assert t false
  else
    if
      Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.is_empty)
        (allowed_endorsements ctxt)
    then
      update_allowed_endorsements ctxt allowed_endorsements'
    else
      (* ❌ Assert instruction is not handled. *)
      assert t false.

(** Init function; without side-effects in Coq *)
Definition init_module1 : unit :=
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "too_many_internal_operations" "Too many internal operations"
      "A transaction exceeded the hard limit of internal operations it can emit"
      None Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Too_many_internal_operations" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Too_many_internal_operations" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "gas_exhausted.operation" "Gas quota exceeded for the operation"
      "A script or one of its callee took more time than the operation said it would"
      None Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Operation_quota_exceeded" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Operation_quota_exceeded" unit tt) in
  Error_monad.register_error_kind Error_monad.Temporary "gas_exhausted.block"
    "Gas quota exceeded for the block"
    "The sum of gas consumed by all the operations in the block exceeds the hard gas limit per block"
    None Data_encoding.empty
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Block_quota_exceeded" then
          Some tt
        else None
      end)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Build_extensible "Block_quota_exceeded" unit tt).

Definition fresh_internal_nonce (ctxt : t) : M? (t * int) :=
  if (internal_nonce ctxt) >=i 65535 then
    Error_monad.error_value
      (Build_extensible "Too_many_internal_operations" unit tt)
  else
    return?
      ((update_internal_nonce ctxt ((internal_nonce ctxt) +i 1)),
        (internal_nonce ctxt)).

Definition reset_internal_nonce (ctxt : t) : t :=
  let ctxt := update_internal_nonce ctxt 0 in
  update_internal_nonces_used ctxt Int_set.(_Set.S.empty).

Definition record_internal_nonce (ctxt : t) (k : int) : t :=
  update_internal_nonces_used ctxt
    (Int_set.(_Set.S.add) k (internal_nonces_used ctxt)).

Definition internal_nonce_already_recorded (ctxt : t) (k : int) : bool :=
  Int_set.(_Set.S.mem) k (internal_nonces_used ctxt).

Definition set_current_fitness (ctxt : t) (fitness : Int64.t) : t :=
  update_fitness ctxt fitness.

Definition add_fees (ctxt : t) (fees' : Tez_repr.t) : M? t :=
  Error_monad.op_gtpipequestion (Tez_repr.op_plusquestion (fees ctxt) fees')
    (update_fees ctxt).

Definition add_rewards (ctxt : t) (rewards' : Tez_repr.t) : M? t :=
  Error_monad.op_gtpipequestion
    (Tez_repr.op_plusquestion (rewards ctxt) rewards') (update_rewards ctxt).

Definition get_rewards : t -> Tez_repr.t := rewards.

Definition get_fees : t -> Tez_repr.t := fees.

(** Init function; without side-effects in Coq *)
Definition init_module2 : unit :=
  Error_monad.register_error_kind Error_monad.Permanent
    "undefined_operation_nonce" "Ill timed access to the origination nonce"
    "An origination was attempted out of the scope of a manager operation" None
    Data_encoding.empty
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Undefined_operation_nonce" then
          Some tt
        else None
      end)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Build_extensible "Undefined_operation_nonce" unit tt).

Definition init_origination_nonce (ctxt : t) (operation_hash : Operation_hash.t)
  : t :=
  let origination_nonce :=
    Some (Contract_repr.initial_origination_nonce operation_hash) in
  update_origination_nonce ctxt origination_nonce.

Definition increment_origination_nonce (ctxt : t)
  : M? (t * Contract_repr.origination_nonce) :=
  match origination_nonce ctxt with
  | None =>
    Error_monad.error_value
      (Build_extensible "Undefined_operation_nonce" unit tt)
  | Some cur_origination_nonce =>
    let origination_nonce :=
      Some (Contract_repr.incr_origination_nonce cur_origination_nonce) in
    let ctxt := update_origination_nonce ctxt origination_nonce in
    return? (ctxt, cur_origination_nonce)
  end.

Definition get_origination_nonce (ctxt : t)
  : M? Contract_repr.origination_nonce :=
  match origination_nonce ctxt with
  | None =>
    Error_monad.error_value
      (Build_extensible "Undefined_operation_nonce" unit tt)
  | Some origination_nonce => return? origination_nonce
  end.

Definition unset_origination_nonce (ctxt : t) : t :=
  update_origination_nonce ctxt None.

(** Init function; without side-effects in Coq *)
Definition init_module3 : unit :=
  Error_monad.register_error_kind Error_monad.Permanent "gas_limit_too_high"
    "Gas limit out of protocol hard bounds"
    "A transaction tried to exceed the hard limit on gas" None
    Data_encoding.empty
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Gas_limit_too_high" then
          Some tt
        else None
      end)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Build_extensible "Gas_limit_too_high" unit tt).

Definition gas_level (ctxt : t) : Gas_limit_repr.t :=
  if unlimited_operation_gas ctxt then
    Gas_limit_repr.Unaccounted
  else
    Gas_limit_repr.Limited
      {| Gas_limit_repr.t.Limited.remaining := remaining_operation_gas ctxt |}.

Definition block_gas_level : t -> Gas_limit_repr.Arith.fp :=
  remaining_block_gas.

Definition check_gas_limit_is_valid
  (ctxt : t) (remaining : Gas_limit_repr.Arith.t) : M? unit :=
  if
    (Gas_limit_repr.Arith.op_gt remaining
      (constants ctxt).(Constants_repr.parametric.hard_gas_limit_per_operation))
    || (Gas_limit_repr.Arith.op_lt remaining Gas_limit_repr.Arith.zero)
  then
    Error_monad.error_value (Build_extensible "Gas_limit_too_high" unit tt)
  else
    Error_monad.ok_unit.

Definition consume_gas_limit_in_block
  (ctxt : t) (limit : Gas_limit_repr.Arith.t) : M? t :=
  let? '_ := check_gas_limit_is_valid ctxt limit in
  let block_gas := block_gas_level ctxt in
  let limit := Gas_limit_repr.Arith.fp_value limit in
  if Gas_limit_repr.Arith.op_gt limit block_gas then
    Error_monad.error_value (Build_extensible "Block_quota_exceeded" unit tt)
  else
    let level := Gas_limit_repr.Arith.sub (block_gas_level ctxt) limit in
    let ctxt := update_remaining_block_gas ctxt level in
    Pervasives.Ok ctxt.

Definition set_gas_limit (ctxt : t) (remaining : Gas_limit_repr.Arith.t) : t :=
  let remaining_operation_gas := Gas_limit_repr.Arith.fp_value remaining in
  let ctxt := update_unlimited_operation_gas ctxt false in
  t.with_remaining_operation_gas remaining_operation_gas ctxt.

Definition set_gas_unlimited (ctxt : t) : t :=
  update_unlimited_operation_gas ctxt true.

Definition gas_exhausted_error {A B : Set} (_ctxt : A) : M? B :=
  Error_monad.error_value (Build_extensible "Operation_quota_exceeded" unit tt).

Definition consume_gas (ctxt : t) (cost : Gas_limit_repr.cost) : M? t :=
  match Gas_limit_repr.raw_consume (remaining_operation_gas ctxt) cost with
  | Some gas_counter =>
    Pervasives.Ok (update_remaining_operation_gas ctxt gas_counter)
  | None =>
    if unlimited_operation_gas ctxt then
      return? ctxt
    else
      Error_monad.error_value
        (Build_extensible "Operation_quota_exceeded" unit tt)
  end.

Definition check_enough_gas (ctxt : t) (cost : Gas_limit_repr.cost) : M? unit :=
  let? function_parameter := consume_gas ctxt cost in
  let '_ := function_parameter in
  Error_monad.ok_unit.

Definition gas_consumed (since : t) (until : t) : Saturation_repr.t :=
  match ((gas_level since), (gas_level until)) with
  |
    (Gas_limit_repr.Limited {| Gas_limit_repr.t.Limited.remaining := before |},
      Gas_limit_repr.Limited {| Gas_limit_repr.t.Limited.remaining := after |})
    => Gas_limit_repr.Arith.sub before after
  | (_, _) => Gas_limit_repr.Arith.zero
  end.

Definition init_storage_space_to_pay (ctxt : t) : t :=
  match storage_space_to_pay ctxt with
  | Some _ =>
    (* ❌ Assert instruction is not handled. *)
    assert t false
  | None =>
    let ctxt := raw_update_storage_space_to_pay ctxt (Some Z.zero) in
    update_allocated_contracts ctxt (Some 0)
  end.

Definition clear_storage_space_to_pay (ctxt : t) : t * Z.t * int :=
  match ((storage_space_to_pay ctxt), (allocated_contracts ctxt)) with
  | ((None, _) | (_, None)) =>
    (* ❌ Assert instruction is not handled. *)
    assert (t * Z.t * int) false
  | (Some storage_space_to_pay, Some allocated_contracts) =>
    let ctxt := raw_update_storage_space_to_pay ctxt None in
    let ctxt := update_allocated_contracts ctxt None in
    (ctxt, storage_space_to_pay, allocated_contracts)
  end.

Definition update_storage_space_to_pay (ctxt : t) (n : Z.t) : t :=
  match storage_space_to_pay ctxt with
  | None =>
    (* ❌ Assert instruction is not handled. *)
    assert t false
  | Some storage_space_to_pay =>
    raw_update_storage_space_to_pay ctxt (Some (n +Z storage_space_to_pay))
  end.

Definition update_allocated_contracts_count (ctxt : t) : t :=
  match allocated_contracts ctxt with
  | None =>
    (* ❌ Assert instruction is not handled. *)
    assert t false
  | Some allocated_contracts =>
    update_allocated_contracts ctxt (Some (Pervasives.succ allocated_contracts))
  end.

Inductive missing_key_kind : Set :=
| Get : missing_key_kind
| _Set : missing_key_kind
| Del : missing_key_kind
| Copy : missing_key_kind.

Inductive storage_error : Set :=
| Incompatible_protocol_version : string -> storage_error
| Missing_key : list string -> missing_key_kind -> storage_error
| Existing_key : list string -> storage_error
| Corrupted_data : list string -> storage_error.

Definition storage_error_encoding : Data_encoding.encoding storage_error :=
  Data_encoding.union None
    [
      Data_encoding.case_value "Incompatible_protocol_version" None
        (Data_encoding.Tag 0)
        (Data_encoding.obj1
          (Data_encoding.req None None "incompatible_protocol_version"
            Data_encoding.string_value))
        (fun (function_parameter : storage_error) =>
          match function_parameter with
          | Incompatible_protocol_version arg => Some arg
          | _ => None
          end) (fun (arg : string) => Incompatible_protocol_version arg);
      Data_encoding.case_value "Missing_key" None (Data_encoding.Tag 1)
        (Data_encoding.obj2
          (Data_encoding.req None None "missing_key"
            (Data_encoding.list_value None Data_encoding.string_value))
          (Data_encoding.req None None "function"
            (Data_encoding.string_enum
              [
                ("get", Get);
                ("set", _Set);
                ("del", Del);
                ("copy", Copy)
              ])))
        (fun (function_parameter : storage_error) =>
          match function_parameter with
          | Missing_key key_value f => Some (key_value, f)
          | _ => None
          end)
        (fun (function_parameter : list string * missing_key_kind) =>
          let '(key_value, f) := function_parameter in
          Missing_key key_value f);
      Data_encoding.case_value "Existing_key" None (Data_encoding.Tag 2)
        (Data_encoding.obj1
          (Data_encoding.req None None "existing_key"
            (Data_encoding.list_value None Data_encoding.string_value)))
        (fun (function_parameter : storage_error) =>
          match function_parameter with
          | Existing_key key_value => Some key_value
          | _ => None
          end) (fun (key_value : list string) => Existing_key key_value);
      Data_encoding.case_value "Corrupted_data" None (Data_encoding.Tag 3)
        (Data_encoding.obj1
          (Data_encoding.req None None "corrupted_data"
            (Data_encoding.list_value None Data_encoding.string_value)))
        (fun (function_parameter : storage_error) =>
          match function_parameter with
          | Corrupted_data key_value => Some key_value
          | _ => None
          end) (fun (key_value : list string) => Corrupted_data key_value)
    ].

Definition pp_storage_error
  (ppf : Format.formatter) (function_parameter : storage_error) : unit :=
  match function_parameter with
  | Incompatible_protocol_version version =>
    Format.fprintf ppf
      (CamlinternalFormatBasics.Format
        (CamlinternalFormatBasics.String_literal
          "Found a context with an unexpected version '"
          (CamlinternalFormatBasics.String CamlinternalFormatBasics.No_padding
            (CamlinternalFormatBasics.String_literal "'."
              CamlinternalFormatBasics.End_of_format)))
        "Found a context with an unexpected version '%s'.") version
  | Missing_key key_value Get =>
    Format.fprintf ppf
      (CamlinternalFormatBasics.Format
        (CamlinternalFormatBasics.String_literal "Missing key '"
          (CamlinternalFormatBasics.String CamlinternalFormatBasics.No_padding
            (CamlinternalFormatBasics.String_literal "'."
              CamlinternalFormatBasics.End_of_format))) "Missing key '%s'.")
      (String.concat "/" key_value)
  | Missing_key key_value _Set =>
    Format.fprintf ppf
      (CamlinternalFormatBasics.Format
        (CamlinternalFormatBasics.String_literal "Cannot set undefined key '"
          (CamlinternalFormatBasics.String CamlinternalFormatBasics.No_padding
            (CamlinternalFormatBasics.String_literal "'."
              CamlinternalFormatBasics.End_of_format)))
        "Cannot set undefined key '%s'.") (String.concat "/" key_value)
  | Missing_key key_value Del =>
    Format.fprintf ppf
      (CamlinternalFormatBasics.Format
        (CamlinternalFormatBasics.String_literal "Cannot delete undefined key '"
          (CamlinternalFormatBasics.String CamlinternalFormatBasics.No_padding
            (CamlinternalFormatBasics.String_literal "'."
              CamlinternalFormatBasics.End_of_format)))
        "Cannot delete undefined key '%s'.") (String.concat "/" key_value)
  | Missing_key key_value Copy =>
    Format.fprintf ppf
      (CamlinternalFormatBasics.Format
        (CamlinternalFormatBasics.String_literal "Cannot copy undefined key '"
          (CamlinternalFormatBasics.String CamlinternalFormatBasics.No_padding
            (CamlinternalFormatBasics.String_literal "'."
              CamlinternalFormatBasics.End_of_format)))
        "Cannot copy undefined key '%s'.") (String.concat "/" key_value)
  | Existing_key key_value =>
    Format.fprintf ppf
      (CamlinternalFormatBasics.Format
        (CamlinternalFormatBasics.String_literal
          "Cannot initialize defined key '"
          (CamlinternalFormatBasics.String CamlinternalFormatBasics.No_padding
            (CamlinternalFormatBasics.String_literal "'."
              CamlinternalFormatBasics.End_of_format)))
        "Cannot initialize defined key '%s'.") (String.concat "/" key_value)
  | Corrupted_data key_value =>
    Format.fprintf ppf
      (CamlinternalFormatBasics.Format
        (CamlinternalFormatBasics.String_literal "Failed to parse the data at '"
          (CamlinternalFormatBasics.String CamlinternalFormatBasics.No_padding
            (CamlinternalFormatBasics.String_literal "'."
              CamlinternalFormatBasics.End_of_format)))
        "Failed to parse the data at '%s'.") (String.concat "/" key_value)
  end.

(** Init function; without side-effects in Coq *)
Definition init_module4 : unit :=
  Error_monad.register_error_kind Error_monad.Permanent "context.storage_error"
    "Storage error (fatal internal error)"
    "An error that should never happen unless something has been deleted or corrupted in the database."
    (Some
      (fun (ppf : Format.formatter) =>
        fun (err : storage_error) =>
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.Formatting_gen
                (CamlinternalFormatBasics.Open_box
                  (CamlinternalFormatBasics.Format
                    (CamlinternalFormatBasics.String_literal "<v 2>"
                      CamlinternalFormatBasics.End_of_format) "<v 2>"))
                (CamlinternalFormatBasics.String_literal "Storage error:"
                  (CamlinternalFormatBasics.Formatting_lit
                    (CamlinternalFormatBasics.Break "@ " 1 0)
                    (CamlinternalFormatBasics.Alpha
                      (CamlinternalFormatBasics.Formatting_lit
                        CamlinternalFormatBasics.Close_box
                        CamlinternalFormatBasics.End_of_format)))))
              "@[<v 2>Storage error:@ %a@]") pp_storage_error err))
    storage_error_encoding
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Storage_error" then
          let 'err := cast storage_error payload in
          Some err
        else None
      end)
    (fun (err : storage_error) =>
      Build_extensible "Storage_error" storage_error err).

Definition storage_error_value {A : Set} (err : storage_error) : M? A :=
  Error_monad.error_value (Build_extensible "Storage_error" storage_error err).

Definition version_key : list string := [ "version" ].

Definition version_value : string := "alpha_current".

Definition version : string := "v1".

Definition cycle_eras_key : list string := [ version; "cycle_eras" ].

Definition constants_key : list string := [ version; "constants" ].

Definition protocol_param_key : list string := [ "protocol_parameters" ].

Definition get_cycle_eras (ctxt : Context.t) : M=? Level_repr.cycle_eras :=
  let= function_parameter := Context.find ctxt cycle_eras_key in
  match function_parameter with
  | None => return= (storage_error_value (Missing_key cycle_eras_key Get))
  | Some bytes_value =>
    match
      Data_encoding.Binary.of_bytes_opt Level_repr.cycle_eras_encoding
        bytes_value with
    | None => return= (storage_error_value (Corrupted_data cycle_eras_key))
    | Some cycle_eras => return=? cycle_eras
    end
  end.

Definition set_cycle_eras {A : Set}
  (ctxt : Context.t) (cycle_eras : Level_repr.cycle_eras)
  : M= (Pervasives.result Context.t A) :=
  let bytes_value :=
    Data_encoding.Binary.to_bytes_exn None Level_repr.cycle_eras_encoding
      cycle_eras in
  Error_monad.op_gtpipeeq (Context.add ctxt cycle_eras_key bytes_value)
    Error_monad.ok.

(** Init function; without side-effects in Coq *)
Definition init_module5 : unit :=
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "context.failed_to_parse_parameter" "Failed to parse parameter"
      "The protocol parameters are not valid JSON."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (bytes_value : bytes) =>
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.Formatting_gen
                  (CamlinternalFormatBasics.Open_box
                    (CamlinternalFormatBasics.Format
                      (CamlinternalFormatBasics.String_literal "<v 2>"
                        CamlinternalFormatBasics.End_of_format) "<v 2>"))
                  (CamlinternalFormatBasics.String_literal
                    "Cannot parse the protocol parameter:"
                    (CamlinternalFormatBasics.Formatting_lit
                      (CamlinternalFormatBasics.Break "@ " 1 0)
                      (CamlinternalFormatBasics.String
                        CamlinternalFormatBasics.No_padding
                        (CamlinternalFormatBasics.Formatting_lit
                          CamlinternalFormatBasics.Close_box
                          CamlinternalFormatBasics.End_of_format)))))
                "@[<v 2>Cannot parse the protocol parameter:@ %s@]")
              (Bytes.to_string bytes_value)))
      (Data_encoding.obj1
        (Data_encoding.req None None "contents" Data_encoding.bytes_value))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Failed_to_parse_parameter" then
            let 'data := cast bytes payload in
            Some data
          else None
        end)
      (fun (data : bytes) =>
        Build_extensible "Failed_to_parse_parameter" bytes data) in
  Error_monad.register_error_kind Error_monad.Temporary
    "context.failed_to_decode_parameter" "Failed to decode parameter"
    "Unexpected JSON object."
    (Some
      (fun (ppf : Format.formatter) =>
        fun (function_parameter : Data_encoding.json * string) =>
          let '(json_value, msg) := function_parameter in
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.Formatting_gen
                (CamlinternalFormatBasics.Open_box
                  (CamlinternalFormatBasics.Format
                    (CamlinternalFormatBasics.String_literal "<v 2>"
                      CamlinternalFormatBasics.End_of_format) "<v 2>"))
                (CamlinternalFormatBasics.String_literal
                  "Cannot decode the protocol parameter:"
                  (CamlinternalFormatBasics.Formatting_lit
                    (CamlinternalFormatBasics.Break "@ " 1 0)
                    (CamlinternalFormatBasics.String
                      CamlinternalFormatBasics.No_padding
                      (CamlinternalFormatBasics.Formatting_lit
                        (CamlinternalFormatBasics.Break "@ " 1 0)
                        (CamlinternalFormatBasics.Alpha
                          (CamlinternalFormatBasics.Formatting_lit
                            CamlinternalFormatBasics.Close_box
                            CamlinternalFormatBasics.End_of_format)))))))
              "@[<v 2>Cannot decode the protocol parameter:@ %s@ %a@]") msg
            Data_encoding.Json.pp json_value))
    (Data_encoding.obj2
      (Data_encoding.req None None "contents" Data_encoding.json_value)
      (Data_encoding.req None None "error" Data_encoding.string_value))
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Failed_to_decode_parameter" then
          let '(json_value, msg) :=
            cast (Data_encoding.json * string) payload in
          Some (json_value, msg)
        else None
      end)
    (fun (function_parameter : Data_encoding.json * string) =>
      let '(json_value, msg) := function_parameter in
      Build_extensible "Failed_to_decode_parameter"
        (Data_encoding.json * string) (json_value, msg)).

Definition get_proto_param (ctxt : Context.t)
  : M=? (Parameters_repr.t * Context.t) :=
  let= function_parameter := Context.find ctxt protocol_param_key in
  match function_parameter with
  | None => Pervasives.failwith "Missing protocol parameters."
  | Some bytes_value =>
    match Data_encoding.Binary.of_bytes_opt Data_encoding.json_value bytes_value
      with
    | None =>
      Error_monad.fail
        (Build_extensible "Failed_to_parse_parameter" Context.value bytes_value)
    | Some json_value =>
      let= ctxt := Context.remove ctxt protocol_param_key in
      let 'param :=
        Data_encoding.Json.destruct Parameters_repr.encoding json_value in
      return=
        (let? '_ := Parameters_repr.check_params param in
        return? (param, ctxt))
    end
  end.

Definition add_constants
  (ctxt : Context.t) (constants : Constants_repr.parametric) : M= Context.t :=
  let bytes_value :=
    Data_encoding.Binary.to_bytes_exn None Constants_repr.parametric_encoding
      constants in
  Context.add ctxt constants_key bytes_value.

Definition get_constants {A : Set} (ctxt : Context.t)
  : M= (Pervasives.result Constants_repr.parametric A) :=
  let= function_parameter := Context.find ctxt constants_key in
  match function_parameter with
  | None =>
    return=
      (Pervasives.failwith "Internal error: cannot read constants in context.")
  | Some bytes_value =>
    match
      Data_encoding.Binary.of_bytes_opt Constants_repr.parametric_encoding
        bytes_value with
    | None =>
      return=
        (Pervasives.failwith
          "Internal error: cannot parse constants in context.")
    | Some constants => return=? constants
    end
  end.

Definition patch_constants
  (ctxt : t) (f : Constants_repr.parametric -> Constants_repr.parametric)
  : M= t :=
  let constants := f (constants ctxt) in
  let= context_value := add_constants (context_value ctxt) constants in
  let ctxt := update_context ctxt context_value in
  return= (update_constants ctxt constants).

Definition check_inited (ctxt : Context.t) : M=? unit :=
  let= function_parameter := Context.find ctxt version_key in
  match function_parameter with
  | None =>
    return= (Pervasives.failwith "Internal error: un-initialized context.")
  | Some bytes_value =>
    let s := Bytes.to_string bytes_value in
    return=
      (if Compare.String.(Compare.S.op_eq) s version_value then
        Error_monad.ok_unit
      else
        storage_error_value (Incompatible_protocol_version s))
  end.

Definition check_cycle_eras
  (cycle_eras : Level_repr.cycle_eras) (constants : Constants_repr.parametric)
  : unit :=
  let current_era := Level_repr.current_era cycle_eras in
  let '_ :=
    (* ❌ Assert instruction is not handled. *)
    assert unit
      (current_era.(Level_repr.cycle_era.blocks_per_cycle) =i32
      constants.(Constants_repr.parametric.blocks_per_cycle)) in
  (* ❌ Assert instruction is not handled. *)
  assert unit
    (current_era.(Level_repr.cycle_era.blocks_per_commitment) =i32
    constants.(Constants_repr.parametric.blocks_per_commitment)).

Definition prepare
  (level : int32) (predecessor_timestamp : Time.t) (timestamp : Time.t)
  (fitness : list bytes) (ctxt : Context.t) : M=? t :=
  let=? level := return= (Raw_level_repr.of_int32 level) in
  let=? fitness := return= (Fitness_repr.to_int64 fitness) in
  let=? '_ := check_inited ctxt in
  let=? constants := get_constants ctxt in
  let=? cycle_eras := get_cycle_eras ctxt in
  let '_ := check_cycle_eras cycle_eras constants in
  let level := Level_repr.level_from_raw cycle_eras level in
  return=?
    {| t.remaining_operation_gas := Gas_limit_repr.Arith.zero;
      t.back :=
        {| back.context := ctxt; back.constants := constants;
          back.cycle_eras := cycle_eras; back.level := level;
          back.predecessor_timestamp := predecessor_timestamp;
          back.timestamp := timestamp; back.fitness := fitness;
          back.included_endorsements := 0;
          back.allowed_endorsements :=
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.empty);
          back.fees := Tez_repr.zero; back.rewards := Tez_repr.zero;
          back.storage_space_to_pay := None; back.allocated_contracts := None;
          back.origination_nonce := None;
          back.temporary_lazy_storage_ids :=
            Lazy_storage_kind.Temp_ids.init_value; back.internal_nonce := 0;
          back.internal_nonces_used := Int_set.(_Set.S.empty);
          back.remaining_block_gas :=
            Gas_limit_repr.Arith.fp_value
              constants.(Constants_repr.parametric.hard_gas_limit_per_block);
          back.unlimited_operation_gas := true |} |}.

Inductive previous_protocol : Set :=
| Genesis : Parameters_repr.t -> previous_protocol
| Granada_010 : previous_protocol.

Definition check_and_update_protocol_version (ctxt : Context.t)
  : M=? (previous_protocol * Context.t) :=
  let=? '(previous_proto, ctxt) :=
    let= function_parameter := Context.find ctxt version_key in
    match function_parameter with
    | None =>
      Pervasives.failwith
        "Internal error: un-initialized context in check_first_block."
    | Some bytes_value =>
      let s := Bytes.to_string bytes_value in
      if Compare.String.(Compare.S.op_eq) s version_value then
        Pervasives.failwith "Internal error: previously initialized context."
      else
        if Compare.String.(Compare.S.op_eq) s "genesis" then
          let=? '(param, ctxt) := get_proto_param ctxt in
          return=? ((Genesis param), ctxt)
        else
          if Compare.String.(Compare.S.op_eq) s "granada_010" then
            return=? (Granada_010, ctxt)
          else
            return= (storage_error_value (Incompatible_protocol_version s))
    end in
  let= ctxt := Context.add ctxt version_key (Bytes.of_string version_value) in
  return=? (previous_proto, ctxt).

Definition get_previous_protocol_constants (ctxt : Context.t)
  : M= Constants_repr.Proto_previous.parametric :=
  let= function_parameter := Context.find ctxt constants_key in
  match function_parameter with
  | None =>
    Pervasives.failwith
      "Internal error: cannot read previous protocol constants in context."
  | Some bytes_value =>
    match
      Data_encoding.Binary.of_bytes_opt
        Constants_repr.Proto_previous.parametric_encoding bytes_value with
    | None =>
      Pervasives.failwith
        "Internal error: cannot parse previous protocol constants in context."
    | Some constants => return= constants
    end
  end.

Definition prepare_first_block
  (level : int32) (timestamp : Time.t) (fitness : list bytes) (ctxt : Context.t)
  : M=? (previous_protocol * t) :=
  let=? '(previous_proto, ctxt) := check_and_update_protocol_version ctxt in
  let=? ctxt :=
    match previous_proto with
    | Genesis param =>
      let=? first_level := return= (Raw_level_repr.of_int32 level) in
      let cycle_era :=
        {| Level_repr.cycle_era.first_level := first_level;
          Level_repr.cycle_era.first_cycle := Cycle_repr.root;
          Level_repr.cycle_era.blocks_per_cycle :=
            param.(Parameters_repr.t.constants).(Constants_repr.parametric.blocks_per_cycle);
          Level_repr.cycle_era.blocks_per_commitment :=
            param.(Parameters_repr.t.constants).(Constants_repr.parametric.blocks_per_commitment)
          |} in
      let=? cycle_eras := return= (Level_repr.create_cycle_eras [ cycle_era ])
        in
      let=? ctxt := set_cycle_eras ctxt cycle_eras in
      Error_monad.op_gtpipeeq
        (add_constants ctxt param.(Parameters_repr.t.constants)) Error_monad.ok
    | Granada_010 =>
      let= c := get_previous_protocol_constants ctxt in
      let constants :=
        {|
          Constants_repr.parametric.preserved_cycles :=
            c.(Constants_repr.Proto_previous.parametric.preserved_cycles);
          Constants_repr.parametric.blocks_per_cycle :=
            c.(Constants_repr.Proto_previous.parametric.blocks_per_cycle);
          Constants_repr.parametric.blocks_per_commitment :=
            c.(Constants_repr.Proto_previous.parametric.blocks_per_commitment);
          Constants_repr.parametric.blocks_per_roll_snapshot :=
            c.(Constants_repr.Proto_previous.parametric.blocks_per_roll_snapshot);
          Constants_repr.parametric.blocks_per_voting_period :=
            c.(Constants_repr.Proto_previous.parametric.blocks_per_voting_period);
          Constants_repr.parametric.time_between_blocks :=
            c.(Constants_repr.Proto_previous.parametric.time_between_blocks);
          Constants_repr.parametric.minimal_block_delay :=
            c.(Constants_repr.Proto_previous.parametric.minimal_block_delay);
          Constants_repr.parametric.endorsers_per_block :=
            c.(Constants_repr.Proto_previous.parametric.endorsers_per_block);
          Constants_repr.parametric.hard_gas_limit_per_operation :=
            c.(Constants_repr.Proto_previous.parametric.hard_gas_limit_per_operation);
          Constants_repr.parametric.hard_gas_limit_per_block :=
            c.(Constants_repr.Proto_previous.parametric.hard_gas_limit_per_block);
          Constants_repr.parametric.proof_of_work_threshold :=
            c.(Constants_repr.Proto_previous.parametric.proof_of_work_threshold);
          Constants_repr.parametric.tokens_per_roll :=
            c.(Constants_repr.Proto_previous.parametric.tokens_per_roll);
          Constants_repr.parametric.seed_nonce_revelation_tip :=
            c.(Constants_repr.Proto_previous.parametric.seed_nonce_revelation_tip);
          Constants_repr.parametric.origination_size :=
            c.(Constants_repr.Proto_previous.parametric.origination_size);
          Constants_repr.parametric.block_security_deposit :=
            c.(Constants_repr.Proto_previous.parametric.block_security_deposit);
          Constants_repr.parametric.endorsement_security_deposit :=
            c.(Constants_repr.Proto_previous.parametric.endorsement_security_deposit);
          Constants_repr.parametric.baking_reward_per_endorsement :=
            c.(Constants_repr.Proto_previous.parametric.baking_reward_per_endorsement);
          Constants_repr.parametric.endorsement_reward :=
            c.(Constants_repr.Proto_previous.parametric.endorsement_reward);
          Constants_repr.parametric.cost_per_byte :=
            c.(Constants_repr.Proto_previous.parametric.cost_per_byte);
          Constants_repr.parametric.hard_storage_limit_per_operation :=
            c.(Constants_repr.Proto_previous.parametric.hard_storage_limit_per_operation);
          Constants_repr.parametric.quorum_min :=
            c.(Constants_repr.Proto_previous.parametric.quorum_min);
          Constants_repr.parametric.quorum_max :=
            c.(Constants_repr.Proto_previous.parametric.quorum_max);
          Constants_repr.parametric.min_proposal_quorum :=
            c.(Constants_repr.Proto_previous.parametric.min_proposal_quorum);
          Constants_repr.parametric.initial_endorsers :=
            c.(Constants_repr.Proto_previous.parametric.initial_endorsers);
          Constants_repr.parametric.delay_per_missing_endorsement :=
            c.(Constants_repr.Proto_previous.parametric.delay_per_missing_endorsement);
          Constants_repr.parametric.liquidity_baking_subsidy :=
            c.(Constants_repr.Proto_previous.parametric.liquidity_baking_subsidy);
          Constants_repr.parametric.liquidity_baking_sunset_level :=
            if
              c.(Constants_repr.Proto_previous.parametric.liquidity_baking_sunset_level)
              =i32 2032928
            then
              2244609
            else
              c.(Constants_repr.Proto_previous.parametric.liquidity_baking_sunset_level);
          Constants_repr.parametric.liquidity_baking_escape_ema_threshold :=
            c.(Constants_repr.Proto_previous.parametric.liquidity_baking_escape_ema_threshold)
          |} in
      let= ctxt := add_constants ctxt constants in
      return=? ctxt
    end in
  let=? ctxt := prepare level timestamp timestamp fitness ctxt in
  return=? (previous_proto, ctxt).

Definition activate (ctxt : t) (h : Protocol_hash.t) : M= t :=
  Error_monad.op_gtpipeeq (Updater.activate (context_value ctxt) h)
    (update_context ctxt).

Definition key : Set := list string.

Definition value : Set := bytes.

Definition tree : Set := Context.tree.

Definition T {t : Set} : Set :=
  Raw_context_intf.T (root := root) (t := t) (tree := tree).

Definition mem (ctxt : t) (k : Context.key) : M= bool :=
  Context.mem (context_value ctxt) k.

Definition mem_tree (ctxt : t) (k : Context.key) : M= bool :=
  Context.mem_tree (context_value ctxt) k.

Definition get (ctxt : t) (k : Context.key) : M=? Context.value :=
  let= function_parameter := Context.find (context_value ctxt) k in
  match function_parameter with
  | None => return= (storage_error_value (Missing_key k Get))
  | Some v => return=? v
  end.

Definition get_tree (ctxt : t) (k : Context.key) : M=? Context.tree :=
  let= function_parameter := Context.find_tree (context_value ctxt) k in
  match function_parameter with
  | None => return= (storage_error_value (Missing_key k Get))
  | Some v => return=? v
  end.

Definition find (ctxt : t) (k : Context.key) : M= (option Context.value) :=
  Context.find (context_value ctxt) k.

Definition find_tree (ctxt : t) (k : Context.key) : M= (option Context.tree) :=
  Context.find_tree (context_value ctxt) k.

Definition add (ctxt : t) (k : Context.key) (v : Context.value) : M= t :=
  Error_monad.op_gtpipeeq (Context.add (context_value ctxt) k v)
    (update_context ctxt).

Definition add_tree (ctxt : t) (k : Context.key) (v : Context.tree) : M= t :=
  Error_monad.op_gtpipeeq (Context.add_tree (context_value ctxt) k v)
    (update_context ctxt).

Definition init_value (ctxt : t) (k : Context.key) (v : Context.value)
  : M=? t :=
  let= function_parameter := Context.mem (context_value ctxt) k in
  match function_parameter with
  | true => return= (storage_error_value (Existing_key k))
  | _ =>
    let= context_value := Context.add (context_value ctxt) k v in
    return=? (update_context ctxt context_value)
  end.

Definition init_tree (ctxt : t) (k : Context.key) (v : Context.tree) : M=? t :=
  let= function_parameter := Context.mem_tree (context_value ctxt) k in
  match function_parameter with
  | true => return= (storage_error_value (Existing_key k))
  | _ =>
    let= context_value := Context.add_tree (context_value ctxt) k v in
    return=? (update_context ctxt context_value)
  end.

Definition update (ctxt : t) (k : Context.key) (v : Context.value) : M=? t :=
  let= function_parameter := Context.mem (context_value ctxt) k in
  match function_parameter with
  | false => return= (storage_error_value (Missing_key k _Set))
  | _ =>
    let= context_value := Context.add (context_value ctxt) k v in
    return=? (update_context ctxt context_value)
  end.

Definition update_tree (ctxt : t) (k : Context.key) (v : Context.tree)
  : M=? t :=
  let= function_parameter := Context.mem_tree (context_value ctxt) k in
  match function_parameter with
  | false => return= (storage_error_value (Missing_key k _Set))
  | _ =>
    let= context_value := Context.add_tree (context_value ctxt) k v in
    return=? (update_context ctxt context_value)
  end.

Definition remove_existing (ctxt : t) (k : Context.key) : M=? t :=
  let= function_parameter := Context.mem (context_value ctxt) k in
  match function_parameter with
  | false => return= (storage_error_value (Missing_key k Del))
  | _ =>
    let= context_value := Context.remove (context_value ctxt) k in
    return=? (update_context ctxt context_value)
  end.

Definition remove_existing_tree (ctxt : t) (k : Context.key) : M=? t :=
  let= function_parameter := Context.mem_tree (context_value ctxt) k in
  match function_parameter with
  | false => return= (storage_error_value (Missing_key k Del))
  | _ =>
    let= context_value := Context.remove (context_value ctxt) k in
    return=? (update_context ctxt context_value)
  end.

Definition remove (ctxt : t) (k : Context.key) : M= t :=
  Error_monad.op_gtpipeeq (Context.remove (context_value ctxt) k)
    (update_context ctxt).

Definition add_or_remove
  (ctxt : t) (k : Context.key) (function_parameter : option Context.value)
  : M= t :=
  match function_parameter with
  | None => remove ctxt k
  | Some v => add ctxt k v
  end.

Definition add_or_remove_tree
  (ctxt : t) (k : Context.key) (function_parameter : option Context.tree)
  : M= t :=
  match function_parameter with
  | None => remove ctxt k
  | Some v => add_tree ctxt k v
  end.

Definition list_value
  (ctxt : t) (offset : option int) (length : option int) (k : Context.key)
  : M= (list (string * Context.tree)) :=
  Context.list_value (context_value ctxt) offset length k.

Definition fold {A : Set}
  (depth : option Context.depth) (ctxt : t) (k : Context.key) (init_value : A)
  (f : Context.key -> Context.tree -> A -> M= A) : M= A :=
  Context.fold depth (context_value ctxt) k init_value f.

Module Tree.
  (** Inclusion of the module [Context.Tree] *)
  Definition mem := Context.Tree.(Context.TREE.mem).
  
  Definition mem_tree := Context.Tree.(Context.TREE.mem_tree).
  
  Definition find := Context.Tree.(Context.TREE.find).
  
  Definition find_tree := Context.Tree.(Context.TREE.find_tree).
  
  Definition list_value := Context.Tree.(Context.TREE.list_value).
  
  Definition add := Context.Tree.(Context.TREE.add).
  
  Definition add_tree := Context.Tree.(Context.TREE.add_tree).
  
  Definition remove := Context.Tree.(Context.TREE.remove).
  
  Definition fold {a : Set} := Context.Tree.(Context.TREE.fold) (a := a).
  
  (* Definition empty := Context.Tree.(Context.TREE.empty). *)
  
  Definition is_empty := Context.Tree.(Context.TREE.is_empty).
  
  Definition kind_value := Context.Tree.(Context.TREE.kind_value).
  
  Definition to_value := Context.Tree.(Context.TREE.to_value).
  
  Definition of_value := Context.Tree.(Context.TREE.of_value).
  
  Definition hash_value := Context.Tree.(Context.TREE.hash_value).
  
  Definition equal := Context.Tree.(Context.TREE.equal).
  
  Definition clear := Context.Tree.(Context.TREE.clear).
  
  Definition empty (ctxt : t) : Context.tree :=
    Context.Tree.(Context.TREE.empty) (context_value ctxt).
  
  Definition get (t_value : Context.tree) (k : Context.key)
    : M=? Context.value :=
    let= function_parameter := find t_value k in
    match function_parameter with
    | None => return= (storage_error_value (Missing_key k Get))
    | Some v => return=? v
    end.
  
  Definition get_tree (t_value : Context.tree) (k : Context.key)
    : M=? Context.tree :=
    let= function_parameter := find_tree t_value k in
    match function_parameter with
    | None => return= (storage_error_value (Missing_key k Get))
    | Some v => return=? v
    end.
  
  Definition init_value
    (t_value : Context.tree) (k : Context.key) (v : Context.value)
    : M=? Context.tree :=
    let= function_parameter := mem t_value k in
    match function_parameter with
    | true => return= (storage_error_value (Existing_key k))
    | _ => Error_monad.op_gtpipeeq (add t_value k v) Error_monad.ok
    end.
  
  Definition init_tree
    (t_value : Context.tree) (k : Context.key) (v : Context.tree)
    : M=? Context.tree :=
    let= function_parameter := mem_tree t_value k in
    match function_parameter with
    | true => return= (storage_error_value (Existing_key k))
    | _ => Error_monad.op_gtpipeeq (add_tree t_value k v) Error_monad.ok
    end.
  
  Definition update
    (t_value : Context.tree) (k : Context.key) (v : Context.value)
    : M=? Context.tree :=
    let= function_parameter := mem t_value k in
    match function_parameter with
    | false => return= (storage_error_value (Missing_key k _Set))
    | _ => Error_monad.op_gtpipeeq (add t_value k v) Error_monad.ok
    end.
  
  Definition update_tree
    (t_value : Context.tree) (k : Context.key) (v : Context.tree)
    : M=? Context.tree :=
    let= function_parameter := mem_tree t_value k in
    match function_parameter with
    | false => return= (storage_error_value (Missing_key k _Set))
    | _ => Error_monad.op_gtpipeeq (add_tree t_value k v) Error_monad.ok
    end.
  
  Definition remove_existing (t_value : Context.tree) (k : Context.key)
    : M=? Context.tree :=
    let= function_parameter := mem t_value k in
    match function_parameter with
    | false => return= (storage_error_value (Missing_key k Del))
    | _ => Error_monad.op_gtpipeeq (remove t_value k) Error_monad.ok
    end.
  
  Definition remove_existing_tree (t_value : Context.tree) (k : Context.key)
    : M=? Context.tree :=
    let= function_parameter := mem_tree t_value k in
    match function_parameter with
    | false => return= (storage_error_value (Missing_key k Del))
    | _ => Error_monad.op_gtpipeeq (remove t_value k) Error_monad.ok
    end.
  
  Definition add_or_remove
    (t_value : Context.tree) (k : Context.key)
    (function_parameter : option Context.value) : M= Context.tree :=
    match function_parameter with
    | None => remove t_value k
    | Some v => add t_value k v
    end.
  
  Definition add_or_remove_tree
    (t_value : Context.tree) (k : Context.key)
    (function_parameter : option Context.tree) : M= Context.tree :=
    match function_parameter with
    | None => remove t_value k
    | Some v => add_tree t_value k v
    end.
  
  Definition module :=
    {|
      Raw_context_intf.TREE.mem := mem;
      Raw_context_intf.TREE.mem_tree := mem_tree;
      Raw_context_intf.TREE.get := get;
      Raw_context_intf.TREE.get_tree := get_tree;
      Raw_context_intf.TREE.find := find;
      Raw_context_intf.TREE.find_tree := find_tree;
      Raw_context_intf.TREE.list_value := list_value;
      Raw_context_intf.TREE.init_value := init_value;
      Raw_context_intf.TREE.init_tree := init_tree;
      Raw_context_intf.TREE.update := update;
      Raw_context_intf.TREE.update_tree := update_tree;
      Raw_context_intf.TREE.add := add;
      Raw_context_intf.TREE.add_tree := add_tree;
      Raw_context_intf.TREE.remove := remove;
      Raw_context_intf.TREE.remove_existing := remove_existing;
      Raw_context_intf.TREE.remove_existing_tree := remove_existing_tree;
      Raw_context_intf.TREE.add_or_remove := add_or_remove;
      Raw_context_intf.TREE.add_or_remove_tree := add_or_remove_tree;
      Raw_context_intf.TREE.fold _ := fold;
      Raw_context_intf.TREE.empty := empty;
      Raw_context_intf.TREE.is_empty := is_empty;
      Raw_context_intf.TREE.kind_value := kind_value;
      Raw_context_intf.TREE.to_value := to_value;
      Raw_context_intf.TREE.hash_value := hash_value;
      Raw_context_intf.TREE.equal := equal;
      Raw_context_intf.TREE.clear := clear
    |}.
End Tree.
Definition Tree : Raw_context_intf.TREE (t := t) (tree := tree) := Tree.module.

Definition project {A : Set} (x : A) : A := x.

Definition absolute_key {A B : Set} (function_parameter : A) : B -> B :=
  let '_ := function_parameter in
  fun (k : B) => k.

Definition description {A : Set} : Storage_description.t A :=
  Storage_description.create tt.

Definition fold_map_temporary_lazy_storage_ids {A : Set}
  (ctxt : t)
  (f : Lazy_storage_kind.Temp_ids.t -> Lazy_storage_kind.Temp_ids.t * A)
  : t * A :=
  (fun (function_parameter : Lazy_storage_kind.Temp_ids.t * A) =>
    let '(temporary_lazy_storage_ids, x) := function_parameter in
    ((update_temporary_lazy_storage_ids ctxt temporary_lazy_storage_ids), x))
    (f (temporary_lazy_storage_ids ctxt)).

Definition map_temporary_lazy_storage_ids_s
  (ctxt : t)
  (f : Lazy_storage_kind.Temp_ids.t -> M= (t * Lazy_storage_kind.Temp_ids.t))
  : M= t :=
  let= '(ctxt, temporary_lazy_storage_ids) :=
    f (temporary_lazy_storage_ids ctxt) in
  return= (update_temporary_lazy_storage_ids ctxt temporary_lazy_storage_ids).

Module Cache.
  Definition key : Set := Context.cache_key.
  
  Definition value : Set := Context.cache_value.
  
  Definition key_of_identifier : int -> string -> Context.cache_key :=
    Context.Cache.(Context.CACHE.key_of_identifier).
  
  Definition identifier_of_key : Context.cache_key -> string :=
    Context.Cache.(Context.CACHE.identifier_of_key).
  
  Definition pp (fmt : Format.formatter) (ctxt : t) : unit :=
    Context.Cache.(Context.CACHE.pp) fmt (context_value ctxt).
  
  Definition find (c : t) (k : Context.cache_key)
    : M= (option Context.cache_value) :=
    Context.Cache.(Context.CACHE.find) (context_value c) k.
  
  Definition set_cache_layout (c : t) (layout : list int) : M= t :=
    let= ctxt :=
      Context.Cache.(Context.CACHE.set_cache_layout) (context_value c) layout in
    return= (update_context c ctxt).
  
  Definition update
    (c : t) (k : Context.cache_key) (v : option (Context.cache_value * int))
    : t :=
    update_context c
      (Context.Cache.(Context.CACHE.update) (context_value c) k v).
  
  Definition sync (c : t) (cache_nonce : Bytes.t) : M= t :=
    let= ctxt :=
      Context.Cache.(Context.CACHE.sync) (context_value c) cache_nonce in
    return= (update_context c ctxt).
  
  Definition clear (c : t) : t :=
    update_context c (Context.Cache.(Context.CACHE.clear) (context_value c)).
  
  Definition list_keys (c : t) (cache_index : int)
    : option (list (Context.cache_key * int)) :=
    Context.Cache.(Context.CACHE.list_keys) (context_value c) cache_index.
  
  Definition key_rank (c : t) (key_value : Context.cache_key) : option int :=
    Context.Cache.(Context.CACHE.key_rank) (context_value c) key_value.
  
  Definition cache_size_limit (c : t) (cache_index : int) : option int :=
    Context.Cache.(Context.CACHE.cache_size_limit) (context_value c) cache_index.
  
  Definition cache_size (c : t) (cache_index : int) : option int :=
    Context.Cache.(Context.CACHE.cache_size) (context_value c) cache_index.
  
  Definition future_cache_expectation (c : t) (time_in_blocks : int) : t :=
    update_context c
      (Context.Cache.(Context.CACHE.future_cache_expectation) (context_value c)
        time_in_blocks).
  
  Definition module :=
    {|
      Context.CACHE.key_of_identifier := key_of_identifier;
      Context.CACHE.identifier_of_key := identifier_of_key;
      Context.CACHE.pp := pp;
      Context.CACHE.find := find;
      Context.CACHE.set_cache_layout := set_cache_layout;
      Context.CACHE.update := update;
      Context.CACHE.sync := sync;
      Context.CACHE.clear := clear;
      Context.CACHE.list_keys := list_keys;
      Context.CACHE.key_rank := key_rank;
      Context.CACHE.cache_size_limit := cache_size_limit;
      Context.CACHE.cache_size := cache_size;
      Context.CACHE.future_cache_expectation := future_cache_expectation
    |}.
End Cache.
Definition Cache := Cache.module.
