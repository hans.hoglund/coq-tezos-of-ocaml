Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Michelson_v1_gas.
Require TezosOfOCaml.Proto_alpha.Michelson_v1_primitives.
Require TezosOfOCaml.Proto_alpha.Saturation_repr.
Require TezosOfOCaml.Proto_alpha.Script_ir_translator_mli.
  Module Script_ir_translator := Script_ir_translator_mli.
Require TezosOfOCaml.Proto_alpha.Script_typed_ir.

Module Interp_costs := Michelson_v1_gas.Cost_of.Interpreter.

Axiom cost_of_instr : forall {a s : Set},
  Script_typed_ir.kinstr -> a -> s -> Alpha_context.Gas.cost.

Definition cost_of_control (ks : Script_typed_ir.continuation)
  : Alpha_context.Gas.cost :=
  match ks with
  | Script_typed_ir.KLog _ _ => Alpha_context.Gas.free
  
  | Script_typed_ir.KNil => Interp_costs.Control.nil
  
  | Script_typed_ir.KCons _ _ => Interp_costs.Control.cons_value
  
  | Script_typed_ir.KReturn _ _ => Interp_costs.Control._return
  
  | Script_typed_ir.KUndip _ _ => Interp_costs.Control.undip
  
  | Script_typed_ir.KLoop_in _ _ => Interp_costs.Control.loop_in
  
  | Script_typed_ir.KLoop_in_left _ _ => Interp_costs.Control.loop_in_left
  
  | Script_typed_ir.KIter _ _ _ => Interp_costs.Control.iter
  
  | Script_typed_ir.KList_enter_body _ xs _ len _ =>
    let 'existT _ __KList_enter_body_'a [len, xs] as exi :=
      existT (A := Set)
        (fun __KList_enter_body_'a => [int ** list __KList_enter_body_'a]) _
        [len, xs]
      return
        let fst := projT1 exi in
        let __KList_enter_body_'a := fst in
        Alpha_context.Gas.cost in
    Interp_costs.Control.list_enter_body xs len
  
  | Script_typed_ir.KList_exit_body _ _ _ _ _ =>
    Interp_costs.Control.list_exit_body
  
  | Script_typed_ir.KMap_enter_body _ _ _ _ =>
    Interp_costs.Control.map_enter_body
  
  | Script_typed_ir.KMap_exit_body _ _ map key_value _ =>
    let 'existT _ __KMap_exit_body_'a [key_value, map] as exi :=
      existT (A := Set)
        (fun __KMap_exit_body_'a =>
          [__KMap_exit_body_'a ** Script_typed_ir.map __KMap_exit_body_'a a]) _
        [key_value, map]
      return
        let fst := projT1 exi in
        let __KMap_exit_body_'a := fst in
        Alpha_context.Gas.cost in
    Interp_costs.Control.map_exit_body key_value map
  end.

Definition local_gas_counter : Set := int.

Inductive outdated_context : Set :=
| OutDatedContext : Alpha_context.context -> outdated_context.

Definition update_context
  (local_gas_counter : int) (function_parameter : outdated_context)
  : Alpha_context.context :=
  let 'OutDatedContext ctxt := function_parameter in
  Alpha_context.Gas.update_remaining_operation_gas ctxt
    (Saturation_repr.safe_int local_gas_counter).

Definition update_local_gas_counter (ctxt : Alpha_context.context) : int :=
  Alpha_context.Gas.remaining_operation_gas ctxt.

Definition outdated (ctxt : Alpha_context.context) : outdated_context :=
  OutDatedContext ctxt.

Definition context_from_outdated_context (function_parameter : outdated_context)
  : Alpha_context.context :=
  let 'OutDatedContext ctxt := function_parameter in
  ctxt.

Definition use_gas_counter_in_ctxt {A B : Set}
  (ctxt : outdated_context) (local_gas_counter : int)
  (f :
    Alpha_context.context ->
    M= (Pervasives.result (A * Alpha_context.context) B))
  : M= (Pervasives.result (A * outdated_context * int) B) :=
  let ctxt := update_context local_gas_counter ctxt in
  let=? '(y, ctxt) := f ctxt in
  return=? (y, (outdated ctxt), (update_local_gas_counter ctxt)).

Definition update_and_check (gas_counter : int) (cost : Alpha_context.Gas.cost)
  : option int :=
  let gas_counter := gas_counter -i cost in
  if gas_counter <i 0 then
    None
  else
    Some gas_counter.

Definition consume {A B : Set}
  (local_gas_counter : int) (k : Script_typed_ir.kinstr) (accu_value : A)
  (stack_value : B) : option int :=
  let cost := cost_of_instr k accu_value stack_value in
  update_and_check local_gas_counter cost.

Definition consume'
  (ctxt : outdated_context) (local_gas_counter : int)
  (cost : Alpha_context.Gas.cost) : M? int :=
  match update_and_check local_gas_counter cost with
  | None =>
    Alpha_context.Gas.gas_exhausted_error
      (update_context local_gas_counter ctxt)
  | Some local_gas_counter => Pervasives.Ok local_gas_counter
  end.

Definition consume_control
  (local_gas_counter : int) (ks : Script_typed_ir.continuation) : option int :=
  let cost := cost_of_control ks in
  update_and_check local_gas_counter cost.

Definition log_entry {A B : Set}
  (logger : Script_typed_ir.logger) (ctxt : outdated_context) (gas : int)
  (k : Script_typed_ir.kinstr) (accu_value : A) (stack_value : B) : unit :=
  let kinfo_value := Script_typed_ir.kinfo_of_kinstr k in
  let ctxt := update_context gas ctxt in
  logger.(Script_typed_ir.logger.log_entry) k ctxt
    kinfo_value.(Script_typed_ir.kinfo.iloc)
    kinfo_value.(Script_typed_ir.kinfo.kstack_ty) (accu_value, stack_value).

Definition log_exit {A B : Set}
  (logger : Script_typed_ir.logger) (ctxt : outdated_context) (gas : int)
  (kinfo_prev : Script_typed_ir.kinfo) (k : Script_typed_ir.kinstr)
  (accu_value : A) (stack_value : B) : unit :=
  let kinfo_value := Script_typed_ir.kinfo_of_kinstr k in
  let ctxt := update_context gas ctxt in
  logger.(Script_typed_ir.logger.log_exit) k ctxt
    kinfo_prev.(Script_typed_ir.kinfo.iloc)
    kinfo_value.(Script_typed_ir.kinfo.kstack_ty) (accu_value, stack_value).

Definition log_control
  (logger : Script_typed_ir.logger) (ks : Script_typed_ir.continuation)
  : unit := logger.(Script_typed_ir.logger.log_control) ks.

Definition get_log (function_parameter : option Script_typed_ir.logger)
  : M=? (option Script_typed_ir.execution_trace) :=
  match function_parameter with
  | None => return= (Pervasives.Ok None)
  | Some logger => logger.(Script_typed_ir.logger.get_log) tt
  end.

Definition log_kinstr
  (logger : Script_typed_ir.logger) (i : Script_typed_ir.kinstr)
  : Script_typed_ir.kinstr :=
  Script_typed_ir.ILog (Script_typed_ir.kinfo_of_kinstr i)
    Script_typed_ir.LogEntry logger i.

Definition log_next_kinstr
  (logger : Script_typed_ir.logger) (i : Script_typed_ir.kinstr)
  : Script_typed_ir.kinstr :=
  let apply (k : Script_typed_ir.kinstr) : Script_typed_ir.kinstr :=
    Script_typed_ir.ILog (Script_typed_ir.kinfo_of_kinstr k)
      (Script_typed_ir.LogExit (Script_typed_ir.kinfo_of_kinstr i)) logger
      (log_kinstr logger k) in
  Script_typed_ir.kinstr_rewritek_value i
    {| Script_typed_ir.kinstr_rewritek.apply := apply |}.

Definition id {A : Set} (x : A) : A := x.

Module step_constants.
  Record record : Set := Build {
    source : Alpha_context.Contract.t;
    payer : Alpha_context.Contract.t;
    self : Alpha_context.Contract.t;
    amount : Alpha_context.Tez.t;
    chain_id : Chain_id.t }.
  Definition with_source source (r : record) :=
    Build source r.(payer) r.(self) r.(amount) r.(chain_id).
  Definition with_payer payer (r : record) :=
    Build r.(source) payer r.(self) r.(amount) r.(chain_id).
  Definition with_self self (r : record) :=
    Build r.(source) r.(payer) self r.(amount) r.(chain_id).
  Definition with_amount amount (r : record) :=
    Build r.(source) r.(payer) r.(self) amount r.(chain_id).
  Definition with_chain_id chain_id (r : record) :=
    Build r.(source) r.(payer) r.(self) r.(amount) chain_id.
End step_constants.
Definition step_constants := step_constants.record.

Fixpoint kundip {c u a s : Set}
  (w : Script_typed_ir.stack_prefix_preservation_witness) (accu_value : c)
  (stack_value : u) (k : Script_typed_ir.kinstr)
  : a * s * Script_typed_ir.kinstr :=
  match w with
  | Script_typed_ir.KPrefix kinfo_value w =>
    let k := Script_typed_ir.IConst kinfo_value accu_value k in
    let '(accu_value, stack_value) := stack_value in
    kundip w accu_value stack_value k
  | Script_typed_ir.KRest => (accu_value, stack_value, k)
  end.

Definition apply {A : Set}
  (ctxt : outdated_context) (gas : int) (capture_ty : Script_typed_ir.ty)
  (capture : A) (lam : Script_typed_ir.lambda)
  : M=? (Script_typed_ir.lambda * outdated_context * int) :=
  let 'Script_typed_ir.Lam descr_value expr := lam in
  let 'Script_typed_ir.Item_t full_arg_ty _ _ :=
    descr_value.(Script_typed_ir.kdescr.kbef) in
  let ctxt := update_context gas ctxt in
  let=? '(const_expr, ctxt) :=
    Script_ir_translator.unparse_data ctxt Script_ir_translator.Optimized
      capture_ty capture in
  let=? '(ty_expr, ctxt) :=
    return= (Script_ir_translator.unparse_ty ctxt capture_ty) in
  match full_arg_ty with
  | Script_typed_ir.Pair_t (capture_ty, _, _) (arg_ty, _, _) _ =>
    let arg_stack_ty := Script_typed_ir.Item_t arg_ty Script_typed_ir.Bot_t None
      in
    let full_descr :=
      {|
        Script_typed_ir.kdescr.kloc := descr_value.(Script_typed_ir.kdescr.kloc);
        Script_typed_ir.kdescr.kbef := arg_stack_ty;
        Script_typed_ir.kdescr.kaft := descr_value.(Script_typed_ir.kdescr.kaft);
        Script_typed_ir.kdescr.kinstr :=
          let kinfo_const :=
            {|
              Script_typed_ir.kinfo.iloc :=
                descr_value.(Script_typed_ir.kdescr.kloc);
              Script_typed_ir.kinfo.kstack_ty := arg_stack_ty |} in
          let kinfo_pair :=
            {|
              Script_typed_ir.kinfo.iloc :=
                descr_value.(Script_typed_ir.kdescr.kloc);
              Script_typed_ir.kinfo.kstack_ty :=
                Script_typed_ir.Item_t capture_ty arg_stack_ty None |} in
          Script_typed_ir.IConst kinfo_const capture
            (Script_typed_ir.ICons_pair kinfo_pair
              descr_value.(Script_typed_ir.kdescr.kinstr)) |} in
    let full_expr :=
      Micheline.Seq 0
        [
          Micheline.Prim 0 Michelson_v1_primitives.I_PUSH
            [ ty_expr; const_expr ] nil;
          Micheline.Prim 0 Michelson_v1_primitives.I_PAIR nil nil;
          expr
        ] in
    let lam' := Script_typed_ir.Lam full_descr full_expr in
    let gas := update_local_gas_counter ctxt in
    return=? (lam', (outdated ctxt), gas)
  | _ =>
    (* ❌ Assert instruction is not handled. *)
    assert (M=? (Script_typed_ir.lambda * outdated_context * int)) false
  end.

Definition transfer {A : Set}
  (function_parameter : outdated_context * step_constants)
  : int -> Alpha_context.Tez.tez -> Script_typed_ir.ty -> A ->
  Alpha_context.Contract.contract -> string ->
  M=?
    ((Alpha_context.packed_internal_operation *
      option Alpha_context.Lazy_storage.diffs) * outdated_context * int) :=
  let '(ctxt, sc) := function_parameter in
  fun (gas : int) =>
    fun (amount : Alpha_context.Tez.tez) =>
      fun (tp : Script_typed_ir.ty) =>
        fun (p_value : A) =>
          fun (destination : Alpha_context.Contract.contract) =>
            fun (entrypoint : string) =>
              let ctxt := update_context gas ctxt in
              let=? '(to_duplicate, ctxt) :=
                return=
                  (Script_ir_translator.collect_lazy_storage ctxt tp p_value) in
              let to_update := Script_ir_translator.no_lazy_storage_id in
              let=? '(p_value, lazy_storage_diff, ctxt) :=
                Script_ir_translator.extract_lazy_storage_diff ctxt
                  Script_ir_translator.Optimized true to_duplicate to_update tp
                  p_value in
              let=? '(p_value, ctxt) :=
                Script_ir_translator.unparse_data ctxt
                  Script_ir_translator.Optimized tp p_value in
              let=? ctxt :=
                return=
                  (Alpha_context.Gas.consume ctxt
                    (Alpha_context.Script.strip_locations_cost p_value)) in
              let operation :=
                Alpha_context.Transaction
                  {|
                    Alpha_context.manager_operation.Transaction.amount := amount;
                    Alpha_context.manager_operation.Transaction.parameters :=
                      Alpha_context.Script.lazy_expr_value
                        (Micheline.strip_locations p_value);
                    Alpha_context.manager_operation.Transaction.entrypoint :=
                      entrypoint;
                    Alpha_context.manager_operation.Transaction.destination :=
                      destination |} in
              let=? '(ctxt, nonce_value) :=
                return= (Alpha_context.fresh_internal_nonce ctxt) in
              let iop :=
                {|
                  Alpha_context.internal_operation.source :=
                    sc.(step_constants.self);
                  Alpha_context.internal_operation.operation := operation;
                  Alpha_context.internal_operation.nonce := nonce_value |} in
              let res :=
                ((Alpha_context.Internal_operation iop), lazy_storage_diff) in
              let gas := update_local_gas_counter ctxt in
              let ctxt := outdated ctxt in
              return=? (res, ctxt, gas).

Definition create_contract {A : Set}
  (function_parameter : outdated_context * step_constants)
  : int -> Script_typed_ir.ty -> Script_typed_ir.ty ->
  Micheline.node Alpha_context.Script.location Alpha_context.Script.prim ->
  Script_typed_ir.SMap.(Map.S.t) Script_typed_ir.view ->
  option Script_typed_ir.field_annot -> option Signature.public_key_hash ->
  Alpha_context.Tez.tez -> A ->
  M=?
    ((Alpha_context.packed_internal_operation *
      option Alpha_context.Lazy_storage.diffs) * Alpha_context.Contract.t *
      outdated_context * int) :=
  let '(ctxt, sc) := function_parameter in
  fun (gas : int) =>
    fun (storage_type : Script_typed_ir.ty) =>
      fun (param_type : Script_typed_ir.ty) =>
        fun (code :
          Micheline.node Alpha_context.Script.location Alpha_context.Script.prim)
          =>
          fun (views : Script_typed_ir.SMap.(Map.S.t) Script_typed_ir.view) =>
            fun (root_name : option Script_typed_ir.field_annot) =>
              fun (delegate : option Signature.public_key_hash) =>
                fun (credit : Alpha_context.Tez.tez) =>
                  fun (init_value : A) =>
                    let ctxt := update_context gas ctxt in
                    let=? '(unparsed_param_type, ctxt) :=
                      return= (Script_ir_translator.unparse_ty ctxt param_type)
                      in
                    let unparsed_param_type :=
                      Script_ir_translator.add_field_annot root_name None
                        unparsed_param_type in
                    let=? '(unparsed_storage_type, ctxt) :=
                      return=
                        (Script_ir_translator.unparse_ty ctxt storage_type) in
                    let view
                      (name : Alpha_context.Script_string.t)
                      (function_parameter : Script_typed_ir.view)
                      : list
                        (Micheline.node Alpha_context.Script.location
                          Alpha_context.Script.prim) ->
                      list
                        (Micheline.node Alpha_context.Script.location
                          Alpha_context.Script.prim) :=
                      let '{|
                        Script_typed_ir.view.input_ty := input_ty;
                          Script_typed_ir.view.output_ty := output_ty;
                          Script_typed_ir.view.view_code := view_code
                          |} := function_parameter in
                      fun (views :
                        list
                          (Micheline.node Alpha_context.Script.location
                            Alpha_context.Script.prim)) =>
                        cons
                          (Micheline.Prim 0 Michelson_v1_primitives.K_view
                            [
                              Micheline.String 0
                                (Alpha_context.Script_string.to_string
                                  name);
                              input_ty;
                              output_ty;
                              view_code
                            ] nil) views in
                    let views :=
                      List.rev
                        (Script_typed_ir.SMap.(Map.S.fold) view views nil) in
                    let code :=
                      Micheline.strip_locations
                        (Micheline.Seq 0
                          (Pervasives.op_at
                            [
                              Micheline.Prim 0
                                Michelson_v1_primitives.K_parameter
                                [ unparsed_param_type ]
                                nil;
                              Micheline.Prim 0 Michelson_v1_primitives.K_storage
                                [
                                  unparsed_storage_type
                                ] nil;
                              Micheline.Prim 0 Michelson_v1_primitives.K_code
                                [ code ] nil
                            ] views)) in
                    let=? '(to_duplicate, ctxt) :=
                      return=
                        (Script_ir_translator.collect_lazy_storage ctxt
                          storage_type init_value) in
                    let to_update := Script_ir_translator.no_lazy_storage_id in
                    let=? '(init_value, lazy_storage_diff, ctxt) :=
                      Script_ir_translator.extract_lazy_storage_diff ctxt
                        Script_ir_translator.Optimized true to_duplicate
                        to_update storage_type init_value in
                    let=? '(storage_value, ctxt) :=
                      Script_ir_translator.unparse_data ctxt
                        Script_ir_translator.Optimized storage_type init_value
                      in
                    let=? ctxt :=
                      return=
                        (Alpha_context.Gas.consume ctxt
                          (Alpha_context.Script.strip_locations_cost
                            storage_value)) in
                    let storage_value := Micheline.strip_locations storage_value
                      in
                    let=? '(ctxt, contract) :=
                      return=
                        (Alpha_context.Contract.fresh_contract_from_current_nonce
                          ctxt) in
                    let operation :=
                      Alpha_context.Origination
                        {|
                          Alpha_context.manager_operation.Origination.delegate
                            := delegate;
                          Alpha_context.manager_operation.Origination.script :=
                            {|
                              Alpha_context.Script.t.code :=
                                Alpha_context.Script.lazy_expr_value code;
                              Alpha_context.Script.t.storage :=
                                Alpha_context.Script.lazy_expr_value
                                  storage_value |};
                          Alpha_context.manager_operation.Origination.credit :=
                            credit;
                          Alpha_context.manager_operation.Origination.preorigination
                            := Some contract |} in
                    let=? '(ctxt, nonce_value) :=
                      return= (Alpha_context.fresh_internal_nonce ctxt) in
                    let res :=
                      ((Alpha_context.Internal_operation
                        {|
                          Alpha_context.internal_operation.source :=
                            sc.(step_constants.self);
                          Alpha_context.internal_operation.operation :=
                            operation;
                          Alpha_context.internal_operation.nonce := nonce_value
                          |}), lazy_storage_diff) in
                    let gas := update_local_gas_counter ctxt in
                    let ctxt := outdated ctxt in
                    return=? (res, contract, ctxt, gas).

Definition unpack {A : Set}
  (ctxt : Alpha_context.context) (ty : Script_typed_ir.ty) (bytes_value : bytes)
  : M=? (option A * Alpha_context.context) :=
  let=? ctxt :=
    return=
      (Alpha_context.Gas.consume ctxt
        (Alpha_context.Script.deserialization_cost_estimated_from_bytes
          (Bytes.length bytes_value))) in
  if
    ((Bytes.length bytes_value) >=i 1) &&
    ((TzEndian.get_uint8 bytes_value 0) =i 5)
  then
    let bytes_value := Bytes.sub bytes_value 1 ((Bytes.length bytes_value) -i 1)
      in
    match
      Data_encoding.Binary.of_bytes_opt Alpha_context.Script.expr_encoding
        bytes_value with
    | None =>
      return=
        (let? ctxt :=
          Alpha_context.Gas.consume ctxt
            (Interp_costs.unpack_failed bytes_value) in
        return? (None, ctxt))
    | Some expr =>
      let= function_parameter :=
        Script_ir_translator.parse_data None ctxt false false ty
          (Micheline.root expr) in
      match function_parameter with
      | Pervasives.Ok (value, ctxt) => return=? ((Some value), ctxt)
      | Pervasives.Error _ignored =>
        return=
          (let? ctxt :=
            Alpha_context.Gas.consume ctxt
              (Interp_costs.unpack_failed bytes_value) in
          return? (None, ctxt))
      end
    end
  else
    return=? (None, ctxt).

Fixpoint interp_stack_prefix_preserving_operation {a s b t result c u d w : Set}
  (f : a -> s -> (b * t) * result)
  (n : Script_typed_ir.stack_prefix_preservation_witness) (accu_value : c)
  (stk : u) : (d * w) * result :=
  match (n, stk) with
  | (Script_typed_ir.KPrefix _ n, rest) =>
    (fun (function_parameter : (__2 * __3) * result) =>
      let '((v, rest'), result_value) := function_parameter in
      ((accu_value, (v, rest')), result_value))
      (interp_stack_prefix_preserving_operation f n (Pervasives.fst rest)
        (Pervasives.snd rest))
  | (Script_typed_ir.KRest, v) => f accu_value v
  end.

Definition step_type (a s r f : Set) : Set :=
  outdated_context * step_constants -> local_gas_counter ->
  Script_typed_ir.kinstr -> Script_typed_ir.continuation -> a -> s ->
  M=? (r * f * outdated_context * local_gas_counter).

Definition kmap_exit_type (a b g h m n o : Set) : Set :=
  (Script_typed_ir.continuation -> Script_typed_ir.continuation) ->
  outdated_context * step_constants -> local_gas_counter ->
  Script_typed_ir.kinstr * list (m * n) * Script_typed_ir.map m o * m ->
  Script_typed_ir.continuation -> o -> a * b ->
  M=? (g * h * outdated_context * local_gas_counter).

Definition kmap_enter_type (a b c d e j k : Set) : Set :=
  (Script_typed_ir.continuation -> Script_typed_ir.continuation) ->
  outdated_context * step_constants -> local_gas_counter ->
  Script_typed_ir.kinstr * list (j * k) * Script_typed_ir.map j a ->
  Script_typed_ir.continuation -> b -> c ->
  M=? (d * e * outdated_context * local_gas_counter).

Definition klist_exit_type (a b c d i j : Set) : Set :=
  (Script_typed_ir.continuation -> Script_typed_ir.continuation) ->
  outdated_context * step_constants -> local_gas_counter ->
  Script_typed_ir.kinstr * list i * list j * local_gas_counter ->
  Script_typed_ir.continuation -> j -> a * b ->
  M=? (c * d * outdated_context * local_gas_counter).

Definition klist_enter_type (a b c d e j : Set) : Set :=
  (Script_typed_ir.continuation -> Script_typed_ir.continuation) ->
  outdated_context * step_constants -> local_gas_counter ->
  Script_typed_ir.kinstr * list j * list b * local_gas_counter ->
  Script_typed_ir.continuation -> a -> c ->
  M=? (d * e * outdated_context * local_gas_counter).

Definition kloop_in_left_type (a b e f g : Set) : Set :=
  outdated_context * step_constants -> local_gas_counter ->
  Script_typed_ir.continuation -> Script_typed_ir.kinstr ->
  Script_typed_ir.continuation -> Script_typed_ir.union a b -> g ->
  M=? (e * f * outdated_context * local_gas_counter).

Definition kloop_in_type (a r f s : Set) : Set :=
  outdated_context * step_constants -> local_gas_counter ->
  Script_typed_ir.continuation -> Script_typed_ir.kinstr ->
  Script_typed_ir.continuation -> bool -> a * s ->
  M=? (r * f * outdated_context * local_gas_counter).

Definition kiter_type (a b s r f : Set) : Set :=
  (Script_typed_ir.continuation -> Script_typed_ir.continuation) ->
  outdated_context * step_constants -> local_gas_counter ->
  Script_typed_ir.kinstr * list b -> Script_typed_ir.continuation -> a -> s ->
  M=? (r * f * outdated_context * local_gas_counter).

Definition ilist_map_type (a b c d e : Set) : Set :=
  (Script_typed_ir.continuation -> Script_typed_ir.continuation) ->
  outdated_context * step_constants -> local_gas_counter ->
  Script_typed_ir.kinstr * Script_typed_ir.kinstr ->
  Script_typed_ir.continuation -> Script_typed_ir.boxed_list e -> a * b ->
  M=? (c * d * outdated_context * local_gas_counter).

Definition ilist_iter_type (a b c d e : Set) : Set :=
  (Script_typed_ir.continuation -> Script_typed_ir.continuation) ->
  outdated_context * step_constants -> local_gas_counter ->
  Script_typed_ir.kinstr * Script_typed_ir.kinstr ->
  Script_typed_ir.continuation -> Script_typed_ir.boxed_list e -> a * b ->
  M=? (c * d * outdated_context * local_gas_counter).

Definition iset_iter_type (a b c d e : Set) : Set :=
  (Script_typed_ir.continuation -> Script_typed_ir.continuation) ->
  outdated_context * step_constants -> local_gas_counter ->
  Script_typed_ir.kinstr * Script_typed_ir.kinstr ->
  Script_typed_ir.continuation -> Script_typed_ir.set e -> a * b ->
  M=? (c * d * outdated_context * local_gas_counter).

Definition imap_map_type (a b c d e f : Set) : Set :=
  (Script_typed_ir.continuation -> Script_typed_ir.continuation) ->
  outdated_context * step_constants -> local_gas_counter ->
  Script_typed_ir.kinstr * Script_typed_ir.kinstr ->
  Script_typed_ir.continuation -> Script_typed_ir.map e f -> a * b ->
  M=? (c * d * outdated_context * local_gas_counter).

Definition imap_iter_type (a b c d e f : Set) : Set :=
  (Script_typed_ir.continuation -> Script_typed_ir.continuation) ->
  outdated_context * step_constants -> local_gas_counter ->
  Script_typed_ir.kinstr * Script_typed_ir.kinstr ->
  Script_typed_ir.continuation -> Script_typed_ir.map e f -> a * b ->
  M=? (c * d * outdated_context * local_gas_counter).

Definition imul_teznat_type (b e f : Set) : Set :=
  option Script_typed_ir.logger -> outdated_context * step_constants ->
  local_gas_counter -> Script_typed_ir.kinfo * Script_typed_ir.kinstr ->
  Script_typed_ir.continuation -> Alpha_context.Tez.t ->
  Alpha_context.Script_int.num * b ->
  M=? (e * f * outdated_context * local_gas_counter).

Definition imul_nattez_type (b e f : Set) : Set :=
  option Script_typed_ir.logger -> outdated_context * step_constants ->
  local_gas_counter -> Script_typed_ir.kinfo * Script_typed_ir.kinstr ->
  Script_typed_ir.continuation -> Alpha_context.Script_int.num ->
  Alpha_context.Tez.t * b -> M=? (e * f * outdated_context * local_gas_counter).

Definition ilsl_nat_type (b e f : Set) : Set :=
  option Script_typed_ir.logger -> outdated_context * step_constants ->
  local_gas_counter -> Script_typed_ir.kinfo * Script_typed_ir.kinstr ->
  Script_typed_ir.continuation -> Alpha_context.Script_int.num ->
  Alpha_context.Script_int.num * b ->
  M=? (e * f * outdated_context * local_gas_counter).

Definition ilsr_nat_type (b e f : Set) : Set :=
  option Script_typed_ir.logger -> outdated_context * step_constants ->
  local_gas_counter -> Script_typed_ir.kinfo * Script_typed_ir.kinstr ->
  Script_typed_ir.continuation -> Alpha_context.Script_int.num ->
  Alpha_context.Script_int.num * b ->
  M=? (e * f * outdated_context * local_gas_counter).

Definition ifailwith_type (a b : Set) : Set :=
  option Script_typed_ir.logger -> outdated_context * step_constants ->
  local_gas_counter -> int -> Script_typed_ir.ty -> a -> M=? b.

Definition iexec_type (b e f g : Set) : Set :=
  option Script_typed_ir.logger -> outdated_context * step_constants ->
  local_gas_counter -> Script_typed_ir.kinstr -> Script_typed_ir.continuation ->
  g -> Script_typed_ir.lambda * b ->
  M=? (e * f * outdated_context * local_gas_counter).
