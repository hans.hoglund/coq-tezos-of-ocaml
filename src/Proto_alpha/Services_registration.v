Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Storage_description.

Module rpc_context.
  Record record : Set := Build {
    block_hash : Block_hash.t;
    block_header : Alpha_context.Block_header.shell_header;
    context : Alpha_context.t }.
  Definition with_block_hash block_hash (r : record) :=
    Build block_hash r.(block_header) r.(context).
  Definition with_block_header block_header (r : record) :=
    Build r.(block_hash) block_header r.(context).
  Definition with_context context (r : record) :=
    Build r.(block_hash) r.(block_header) context.
End rpc_context.
Definition rpc_context := rpc_context.record.

Definition rpc_init (function_parameter : Updater.rpc_context)
  : M=? rpc_context :=
  let '{|
    Updater.rpc_context.block_hash := block_hash;
      Updater.rpc_context.block_header := block_header;
      Updater.rpc_context.context := context_value
      |} := function_parameter in
  let level := block_header.(Block_header.shell_header.level) in
  let timestamp := block_header.(Block_header.shell_header.timestamp) in
  let fitness := block_header.(Block_header.shell_header.fitness) in
  let=? '(context_value, _, _) :=
    Alpha_context.prepare context_value level timestamp timestamp fitness in
  return=?
    {| rpc_context.block_hash := block_hash;
      rpc_context.block_header := block_header;
      rpc_context.context := context_value |}.

Definition rpc_services
  : Pervasives.ref (RPC_directory.t Updater.rpc_context) :=
  Pervasives.ref_value RPC_directory.empty.

Definition register0_fullctxt {A B C : Set}
  (chunked : bool)
  (s : RPC_service.t Updater.rpc_context Updater.rpc_context A B C)
  (f : rpc_context -> A -> B -> M=? C) : unit :=
  Pervasives.op_coloneq rpc_services
    (RPC_directory.register chunked (Pervasives.op_exclamation rpc_services) s
      (fun (ctxt : Updater.rpc_context) =>
        fun (q : A) =>
          fun (i : B) =>
            let=? ctxt := rpc_init ctxt in
            f ctxt q i)).

Definition register0 {A B C : Set}
  (chunked : bool)
  (s : RPC_service.t Updater.rpc_context Updater.rpc_context A B C)
  (f : Alpha_context.t -> A -> B -> M=? C) : unit :=
  register0_fullctxt chunked s
    (fun (function_parameter : rpc_context) =>
      let '{| rpc_context.context := context_value |} := function_parameter in
      f context_value).

Definition register0_noctxt {A B C D : Set}
  (chunked : bool) (s : RPC_service.t Updater.rpc_context A B C D)
  (f : B -> C -> M=? D) : unit :=
  Pervasives.op_coloneq rpc_services
    (RPC_directory.register chunked (Pervasives.op_exclamation rpc_services) s
      (fun (function_parameter : A) =>
        let '_ := function_parameter in
        fun (q : B) => fun (i : C) => f q i)).

Definition register1_fullctxt {A B C D : Set}
  (chunked : bool)
  (s : RPC_service.t Updater.rpc_context (Updater.rpc_context * A) B C D)
  (f : rpc_context -> A -> B -> C -> M=? D) : unit :=
  Pervasives.op_coloneq rpc_services
    (RPC_directory.register chunked (Pervasives.op_exclamation rpc_services) s
      (fun (function_parameter : Updater.rpc_context * A) =>
        let '(ctxt, arg) := function_parameter in
        fun (q : B) =>
          fun (i : C) =>
            let=? ctxt := rpc_init ctxt in
            f ctxt arg q i)).

Definition register1 {A B C D : Set}
  (chunked : bool)
  (s : RPC_service.t Updater.rpc_context (Updater.rpc_context * A) B C D)
  (f : Alpha_context.t -> A -> B -> C -> M=? D) : unit :=
  register1_fullctxt chunked s
    (fun (function_parameter : rpc_context) =>
      let '{| rpc_context.context := context_value |} := function_parameter in
      fun (x : A) => f context_value x).

Definition register2_fullctxt {A B C D E : Set}
  (chunked : bool)
  (s : RPC_service.t Updater.rpc_context ((Updater.rpc_context * A) * B) C D E)
  (f : rpc_context -> A -> B -> C -> D -> M=? E) : unit :=
  Pervasives.op_coloneq rpc_services
    (RPC_directory.register chunked (Pervasives.op_exclamation rpc_services) s
      (fun (function_parameter : (Updater.rpc_context * A) * B) =>
        let '((ctxt, arg1), arg2) := function_parameter in
        fun (q : C) =>
          fun (i : D) =>
            let=? ctxt := rpc_init ctxt in
            f ctxt arg1 arg2 q i)).

Definition register2 {A B C D E : Set}
  (chunked : bool)
  (s : RPC_service.t Updater.rpc_context ((Updater.rpc_context * A) * B) C D E)
  (f : Alpha_context.t -> A -> B -> C -> D -> M=? E) : unit :=
  register2_fullctxt chunked s
    (fun (function_parameter : rpc_context) =>
      let '{| rpc_context.context := context_value |} := function_parameter in
      fun (a1 : A) =>
        fun (a2 : B) => fun (q : C) => fun (i : D) => f context_value a1 a2 q i).

Definition opt_register0_fullctxt {A B C : Set}
  (chunked : bool)
  (s : RPC_service.t Updater.rpc_context Updater.rpc_context A B C)
  (f : rpc_context -> A -> B -> M=? (option C)) : unit :=
  Pervasives.op_coloneq rpc_services
    (RPC_directory.opt_register chunked (Pervasives.op_exclamation rpc_services)
      s
      (fun (ctxt : Updater.rpc_context) =>
        fun (q : A) =>
          fun (i : B) =>
            let=? ctxt := rpc_init ctxt in
            f ctxt q i)).

Definition opt_register0 {A B C : Set}
  (chunked : bool)
  (s : RPC_service.t Updater.rpc_context Updater.rpc_context A B C)
  (f : Alpha_context.t -> A -> B -> M=? (option C)) : unit :=
  opt_register0_fullctxt chunked s
    (fun (function_parameter : rpc_context) =>
      let '{| rpc_context.context := context_value |} := function_parameter in
      f context_value).

Definition opt_register1_fullctxt {A B C D : Set}
  (chunked : bool)
  (s : RPC_service.t Updater.rpc_context (Updater.rpc_context * A) B C D)
  (f : rpc_context -> A -> B -> C -> M=? (option D)) : unit :=
  Pervasives.op_coloneq rpc_services
    (RPC_directory.opt_register chunked (Pervasives.op_exclamation rpc_services)
      s
      (fun (function_parameter : Updater.rpc_context * A) =>
        let '(ctxt, arg) := function_parameter in
        fun (q : B) =>
          fun (i : C) =>
            let=? ctxt := rpc_init ctxt in
            f ctxt arg q i)).

Definition opt_register1 {A B C D : Set}
  (chunked : bool)
  (s : RPC_service.t Updater.rpc_context (Updater.rpc_context * A) B C D)
  (f : Alpha_context.t -> A -> B -> C -> M=? (option D)) : unit :=
  opt_register1_fullctxt chunked s
    (fun (function_parameter : rpc_context) =>
      let '{| rpc_context.context := context_value |} := function_parameter in
      fun (x : A) => f context_value x).

Definition opt_register2_fullctxt {A B C D E : Set}
  (chunked : bool)
  (s : RPC_service.t Updater.rpc_context ((Updater.rpc_context * A) * B) C D E)
  (f : rpc_context -> A -> B -> C -> D -> M=? (option E)) : unit :=
  Pervasives.op_coloneq rpc_services
    (RPC_directory.opt_register chunked (Pervasives.op_exclamation rpc_services)
      s
      (fun (function_parameter : (Updater.rpc_context * A) * B) =>
        let '((ctxt, arg1), arg2) := function_parameter in
        fun (q : C) =>
          fun (i : D) =>
            let=? ctxt := rpc_init ctxt in
            f ctxt arg1 arg2 q i)).

Definition opt_register2 {A B C D E : Set}
  (chunked : bool)
  (s : RPC_service.t Updater.rpc_context ((Updater.rpc_context * A) * B) C D E)
  (f : Alpha_context.t -> A -> B -> C -> D -> M=? (option E)) : unit :=
  opt_register2_fullctxt chunked s
    (fun (function_parameter : rpc_context) =>
      let '{| rpc_context.context := context_value |} := function_parameter in
      fun (a1 : A) =>
        fun (a2 : B) => fun (q : C) => fun (i : D) => f context_value a1 a2 q i).

Definition get_rpc_services (function_parameter : unit)
  : RPC_directory.directory Updater.rpc_context :=
  let '_ := function_parameter in
  let p_value :=
    RPC_directory.map
      (fun (c : Updater.rpc_context) =>
        let= function_parameter := rpc_init c in
        match function_parameter with
        | Pervasives.Error _ =>
          return=
            (* ❌ Assert instruction is not handled. *)
            (assert Alpha_context.t false)
        | Pervasives.Ok c => return= c.(rpc_context.context)
        end) (Storage_description.build_directory Alpha_context.description) in
  RPC_directory.register_dynamic_directory None
    (Pervasives.op_exclamation rpc_services)
    (RPC_path.op_div
      (RPC_path.op_div (RPC_path.op_div RPC_path.open_root "context") "raw")
      "json")
    (fun (function_parameter : Updater.rpc_context) =>
      let '_ := function_parameter in
      return= p_value).
