Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_alpha.Environment.Format.
Require Proto_alpha.Environment.Z.

Inductive json : Set :=
| Bool : bool -> json
| Null : json
| O : list (string * json) -> json
| Float : float -> json
| String : string -> json
| A : list json -> json.

Parameter json_schema : Set.

Parameter t : forall (a : Set), Set.

Definition encoding (a : Set) : Set := t a.

Parameter classify : forall {a : Set},
  encoding a -> (* `Variable *) unit + (* `Fixed *) int + (* `Dynamic *) unit.

Parameter splitted : forall {a : Set}, encoding a -> encoding a -> encoding a.

Parameter null : encoding unit.

Parameter empty : encoding unit.

Parameter unit_value : encoding unit.

Parameter constant : string -> encoding unit.

Parameter int8 : encoding int.

Parameter uint8 : encoding int.

Parameter int16 : encoding int.

Parameter uint16 : encoding int.

Parameter int31 : encoding int.

Parameter int32_value : encoding int32.

Parameter int64_value : encoding int64.

Parameter n : encoding Z.t.

Parameter z : encoding Z.t.

Parameter bool_value : encoding bool.

Parameter string_value : encoding string.

Parameter bytes_value : encoding bytes.

Parameter option_value : forall {a : Set},
  encoding a -> encoding (option a).

Parameter string_enum : forall {a : Set}, list (string * a) -> encoding a.

Module Fixed.
  Parameter string_value : int -> encoding string.
  
  Parameter bytes_value : int -> encoding bytes.
  
  Parameter add_padding : forall {a : Set}, encoding a -> int -> encoding a.
End Fixed.

Module _Variable.
  Parameter string_value : encoding string.
  
  Parameter bytes_value : encoding bytes.
  
  Parameter array : forall {a : Set},
    option int -> encoding a -> encoding (array a).
  
  Parameter list_value : forall {a : Set},
    option int -> encoding a -> encoding (list a).
End _Variable.

Module Bounded.
  Parameter string_value : int -> encoding string.
  
  Parameter bytes_value : int -> encoding bytes.
End Bounded.

Parameter dynamic_size : forall {a : Set},
  option ((* `Uint16 *) unit + (* `Uint8 *) unit + (* `Uint30 *) unit) ->
  encoding a -> encoding a.

Parameter json_value : encoding json.

Parameter json_schema_value : encoding json_schema.

Parameter field : forall (a : Set), Set.

Parameter req : forall {t : Set},
  option string -> option string -> string -> encoding t -> field t.

Parameter opt : forall {t : Set},
  option string -> option string -> string -> encoding t -> field (option t).

Parameter varopt : forall {t : Set},
  option string -> option string -> string -> encoding t -> field (option t).

Parameter dft : forall {t : Set},
  option string -> option string -> string -> encoding t -> t -> field t.

Parameter obj1 : forall {f1 : Set}, field f1 -> encoding f1.

Parameter obj2 : forall {f1 f2 : Set},
  field f1 -> field f2 -> encoding (f1 * f2).

Parameter obj3 : forall {f1 f2 f3 : Set},
  field f1 -> field f2 -> field f3 -> encoding (f1 * f2 * f3).

Parameter obj4 : forall {f1 f2 f3 f4 : Set},
  field f1 -> field f2 -> field f3 -> field f4 -> encoding (f1 * f2 * f3 * f4).

Parameter obj5 : forall {f1 f2 f3 f4 f5 : Set},
  field f1 -> field f2 -> field f3 -> field f4 -> field f5 ->
  encoding (f1 * f2 * f3 * f4 * f5).

Parameter obj6 : forall {f1 f2 f3 f4 f5 f6 : Set},
  field f1 -> field f2 -> field f3 -> field f4 -> field f5 -> field f6 ->
  encoding (f1 * f2 * f3 * f4 * f5 * f6).

Parameter obj7 : forall {f1 f2 f3 f4 f5 f6 f7 : Set},
  field f1 -> field f2 -> field f3 -> field f4 -> field f5 -> field f6 ->
  field f7 -> encoding (f1 * f2 * f3 * f4 * f5 * f6 * f7).

Parameter obj8 : forall {f1 f2 f3 f4 f5 f6 f7 f8 : Set},
  field f1 -> field f2 -> field f3 -> field f4 -> field f5 -> field f6 ->
  field f7 -> field f8 -> encoding (f1 * f2 * f3 * f4 * f5 * f6 * f7 * f8).

Parameter obj9 : forall {f1 f2 f3 f4 f5 f6 f7 f8 f9 : Set},
  field f1 -> field f2 -> field f3 -> field f4 -> field f5 -> field f6 ->
  field f7 -> field f8 -> field f9 ->
  encoding (f1 * f2 * f3 * f4 * f5 * f6 * f7 * f8 * f9).

Parameter obj10 : forall {f1 f10 f2 f3 f4 f5 f6 f7 f8 f9 : Set},
  field f1 -> field f2 -> field f3 -> field f4 -> field f5 -> field f6 ->
  field f7 -> field f8 -> field f9 -> field f10 ->
  encoding (f1 * f2 * f3 * f4 * f5 * f6 * f7 * f8 * f9 * f10).

Parameter tup1 : forall {f1 : Set}, encoding f1 -> encoding f1.

Parameter tup2 : forall {f1 f2 : Set},
  encoding f1 -> encoding f2 -> encoding (f1 * f2).

Parameter tup3 : forall {f1 f2 f3 : Set},
  encoding f1 -> encoding f2 -> encoding f3 -> encoding (f1 * f2 * f3).

Parameter tup4 : forall {f1 f2 f3 f4 : Set},
  encoding f1 -> encoding f2 -> encoding f3 -> encoding f4 ->
  encoding (f1 * f2 * f3 * f4).

Parameter tup5 : forall {f1 f2 f3 f4 f5 : Set},
  encoding f1 -> encoding f2 -> encoding f3 -> encoding f4 -> encoding f5 ->
  encoding (f1 * f2 * f3 * f4 * f5).

Parameter tup6 : forall {f1 f2 f3 f4 f5 f6 : Set},
  encoding f1 -> encoding f2 -> encoding f3 -> encoding f4 -> encoding f5 ->
  encoding f6 -> encoding (f1 * f2 * f3 * f4 * f5 * f6).

Parameter tup7 : forall {f1 f2 f3 f4 f5 f6 f7 : Set},
  encoding f1 -> encoding f2 -> encoding f3 -> encoding f4 -> encoding f5 ->
  encoding f6 -> encoding f7 -> encoding (f1 * f2 * f3 * f4 * f5 * f6 * f7).

Parameter tup8 : forall {f1 f2 f3 f4 f5 f6 f7 f8 : Set},
  encoding f1 -> encoding f2 -> encoding f3 -> encoding f4 -> encoding f5 ->
  encoding f6 -> encoding f7 -> encoding f8 ->
  encoding (f1 * f2 * f3 * f4 * f5 * f6 * f7 * f8).

Parameter tup9 : forall {f1 f2 f3 f4 f5 f6 f7 f8 f9 : Set},
  encoding f1 -> encoding f2 -> encoding f3 -> encoding f4 -> encoding f5 ->
  encoding f6 -> encoding f7 -> encoding f8 -> encoding f9 ->
  encoding (f1 * f2 * f3 * f4 * f5 * f6 * f7 * f8 * f9).

Parameter tup10 : forall {f1 f10 f2 f3 f4 f5 f6 f7 f8 f9 : Set},
  encoding f1 -> encoding f2 -> encoding f3 -> encoding f4 -> encoding f5 ->
  encoding f6 -> encoding f7 -> encoding f8 -> encoding f9 -> encoding f10 ->
  encoding (f1 * f2 * f3 * f4 * f5 * f6 * f7 * f8 * f9 * f10).

Parameter merge_objs : forall {o1 o2 : Set},
  encoding o1 -> encoding o2 -> encoding (o1 * o2).

Parameter merge_tups : forall {a1 a2 : Set},
  encoding a1 -> encoding a2 -> encoding (a1 * a2).

Parameter array : forall {a : Set},
  option int -> encoding a -> encoding (array a).

Parameter list_value : forall {a : Set},
  option int -> encoding a -> encoding (list a).

Parameter assoc : forall {a : Set},
  encoding a -> encoding (list (string * a)).

Inductive case_tag : Set :=
| Tag : int -> case_tag
| Json_only : case_tag.

Parameter case : forall (t : Set), Set.

Parameter case_value : forall {a t : Set},
  string -> option string -> case_tag -> encoding a -> (t -> option a) ->
  (a -> t) -> case t.

Parameter match_result : Set.
  
Definition matching_function (a : Set) : Set := a -> match_result.

Inductive tag_size : Set :=
| Uint16 : tag_size
| Uint8 : tag_size.

Parameter matched : forall {a : Set},
  option tag_size -> int -> encoding a -> a -> match_result.

Parameter matching : forall {t : Set},
  option tag_size -> matching_function t -> list (case t) -> encoding t.

Parameter union : forall {t : Set},
  option tag_size -> list (case t) -> encoding t.

Parameter def : forall {t : Set},
  string -> option string -> option string -> encoding t -> encoding t.

Parameter conv : forall {a b : Set},
  (a -> b) -> (b -> a) -> option json_schema -> encoding b -> encoding a.

Parameter conv_with_guard : forall {a b : Set},
  (a -> b) -> (b -> Pervasives.result a string) -> option json_schema ->
  encoding b -> encoding a.

Parameter with_decoding_guard : forall {a : Set},
  (a -> Pervasives.result unit string) -> encoding a -> encoding a.

Parameter mu : forall {a : Set},
  string -> option string -> option string -> (encoding a -> encoding a) ->
  encoding a.

Module Lazy.
  Inductive t (a : Set) : Set :=
  | Both : bytes -> a -> t a
  | Bytes : bytes -> t a
  | Value : a -> t a.

  Arguments Both {_}.
  Arguments Bytes {_}.
  Arguments Value {_}.
End Lazy.

Definition lazy_t : forall (a : Set), Set := Lazy.t.

Parameter lazy_encoding : forall {a : Set}, encoding a -> encoding (lazy_t a).

Parameter force_decode : forall {a : Set}, lazy_t a -> option a.

Parameter force_bytes : forall {a : Set}, lazy_t a -> bytes.

Parameter make_lazy : forall {a : Set}, encoding a -> a -> lazy_t a.

Definition apply_lazy {a b : Set}
  (fun_value : a -> b) (fun_bytes : bytes -> b) (fun_combine : b -> b -> b)
  (le : lazy_t a) : b :=
  match le with
  | Lazy.Both bytes value => fun_combine (fun_value value) (fun_bytes bytes)
  | Lazy.Bytes bytes => fun_bytes bytes
  | Lazy.Value value => fun_value value
  end.

Module Json.
  Parameter schema : forall {a : Set},
    option string -> encoding a -> json_schema.
  
  Parameter construct : forall {t : Set}, encoding t -> t -> json.
  
  Parameter destruct : forall {t : Set}, encoding t -> json -> t.
  
  Reserved Notation "'path".
  
  Inductive path_item : Set :=
  | Index : int -> path_item
  | Field : string -> path_item
  | Next : path_item
  | Star : path_item
  
  where "'path" := (list path_item).
  
  Definition path := 'path.
  
  Parameter print_error :
    option (Format.formatter -> extensible_type -> unit) ->
    Format.formatter -> extensible_type -> unit.
  
  Parameter cannot_destruct : forall {a b : Set},
    Pervasives.format4 a Format.formatter unit b -> a.
  
  Parameter wrap_error : forall {a b : Set}, (a -> b) -> a -> b.
  
  Parameter pp : Format.formatter -> json -> unit.
End Json.

Module Binary.
  Parameter fixed_length : forall {a : Set}, encoding a -> option int.
    
  Parameter maximum_length : forall {a : Set}, encoding a -> option int.
  
  Parameter length : forall {a : Set}, encoding a -> a -> int.
  
  Parameter to_bytes_opt : forall {a : Set},
    option int -> encoding a -> a -> option bytes.
  
  Parameter to_bytes_exn : forall {a : Set},
    option int -> encoding a -> a -> bytes.
  
  Parameter of_bytes_opt : forall {a : Set}, encoding a -> bytes -> option a.
  
  Parameter to_string_opt : forall {a : Set},
    option int -> encoding a -> a -> option string.
  
  Parameter to_string_exn : forall {a : Set},
    option int -> encoding a -> a -> string.
  
  Parameter of_string_opt : forall {a : Set},
    encoding a -> string -> option a.
End Binary.

Parameter check_size : forall {a : Set}, int -> encoding a -> encoding a.
