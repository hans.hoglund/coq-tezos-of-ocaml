Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_alpha.Environment.Compare.
Require Proto_alpha.Environment.Error_monad.
Require Proto_alpha.Environment.Map.
Import Error_monad.Notations.

Module S.
  Record signature {elt t : Set} : Set := {
    elt := elt;
    t := t;
    empty : t;
    is_empty : t -> bool;
    mem : elt -> t -> bool;
    add : elt -> t -> t;
    singleton : elt -> t;
    remove : elt -> t -> t;
    union : t -> t -> t;
    inter : t -> t -> t;
    disjoint : t -> t -> bool;
    diff_value : t -> t -> t;
    compare : t -> t -> int;
    equal : t -> t -> bool;
    subset : t -> t -> bool;
    iter : (elt -> unit) -> t -> unit;
    iter_e :
      forall {trace : Set},
      (elt -> Pervasives.result unit trace) -> t ->
      Pervasives.result unit trace;
    iter_s : (elt -> Lwt.t unit) -> t -> Lwt.t unit;
    iter_p : (elt -> Lwt.t unit) -> t -> Lwt.t unit;
    iter_es :
      forall {trace : Set},
      (elt -> Lwt.t (Pervasives.result unit trace)) -> t ->
      Lwt.t (Pervasives.result unit trace);
    map : (elt -> elt) -> t -> t;
    fold : forall {a : Set}, (elt -> a -> a) -> t -> a -> a;
    fold_e :
      forall {a trace : Set},
      (elt -> a -> Pervasives.result a trace) -> t -> a ->
      Pervasives.result a trace;
    fold_s : forall {a : Set}, (elt -> a -> Lwt.t a) -> t -> a -> Lwt.t a;
    fold_es :
      forall {a trace : Set},
      (elt -> a -> Lwt.t (Pervasives.result a trace)) -> t -> a ->
      Lwt.t (Pervasives.result a trace);
    for_all : (elt -> bool) -> t -> bool;
    _exists : (elt -> bool) -> t -> bool;
    filter : (elt -> bool) -> t -> t;
    partition : (elt -> bool) -> t -> t * t;
    cardinal : t -> int;
    elements : t -> list elt;
    min_elt : t -> option elt;
    max_elt : t -> option elt;
    choose : t -> option elt;
    split : elt -> t -> t * bool * t;
    find : elt -> t -> option elt;
    find_first : (elt -> bool) -> t -> option elt;
    find_last : (elt -> bool) -> t -> option elt;
    of_list : list elt -> t;
    to_seq_from : elt -> t -> CoqOfOCaml.Seq.t elt;
    to_seq : t -> CoqOfOCaml.Seq.t elt;
    add_seq : CoqOfOCaml.Seq.t elt -> t -> t;
    of_seq : CoqOfOCaml.Seq.t elt -> t;
    iter_ep : (elt -> Lwt.t (M? unit)) -> t -> Lwt.t (M? unit);
  }.
End S.
Definition S := @S.signature.
Arguments S {_ _}.

(** We define sets as maps to the unit type. *)
Module Make.
  Class FArgs {t : Set} := {
    Ord : Compare.COMPARABLE (t := t);
  }.
  Arguments Build_FArgs {_}.

  Definition elt `{FArgs} : Set := Ord.(Compare.COMPARABLE.t).

  Definition Map `{FArgs} : Map.S (key := elt) (t := _) :=
    Map.Make Ord.

  Definition t `{FArgs} : Set := Map.(Map.S.t) unit.

  Definition empty `{FArgs} : t :=
    Map.(Map.S.empty).

  Definition is_empty `{FArgs} (s : t) : bool :=
    Map.(Map.S.is_empty) s.

  Definition mem `{FArgs} (e : elt) (s : t) : bool :=
    Map.(Map.S.mem) e s.

  Definition add `{FArgs} (e : elt) (s : t) : t :=
    Map.(Map.S.add) e tt s.

  Definition singleton `{FArgs} (e : elt) : t :=
    Map.(Map.S.singleton) e tt.

  Definition remove `{FArgs} (e : elt) (m : t) : t :=
    Map.(Map.S.remove) e m.

  Definition fold `{FArgs} {a : Set} (f : elt -> a -> a) (s : t) (acc : a)
    : a :=
    Map.(Map.S.fold) (fun e _ acc => f e acc) s acc.

  Definition union `{FArgs} (s1 s2 : t) : t :=
    Map.(Map.S.union) (fun e _ _ => Some tt) s1 s2.

  Definition filter `{FArgs} (p : elt -> bool) (s : t) : t :=
    Map.(Map.S.filter) (fun e _ => p e) s.

  Definition inter `{FArgs} (s1 s2 : t) : t :=
    Map.(Map.S.merge)
      (fun e v1 v2 =>
        match v1, v2 with
        | Some _, Some _ => Some tt
        | _, _ => None
        end
      )
      s1 s2.

  Parameter disjoint : forall `{FArgs}, t -> t -> bool.

  Definition diff_value `{FArgs} (s1 s2 : t) : t :=
    Map.(Map.S.merge)
      (fun e v1 v2 =>
        match v1, v2 with
        | Some _, None => Some tt
        | _, _ => None
        end
      )
      s1 s2.

  Definition compare `{FArgs} (s1 s2 : t) : int :=
    Map.(Map.S.compare) (fun _ _ => 0) s1 s2.

  Definition equal `{FArgs} (s1 s2 : t) : bool :=
    Map.(Map.S.equal) (fun _ _ => true) s1 s2.

  Definition iter `{FArgs} (f : elt -> unit) (s : t) : unit :=
    Map.(Map.S.iter) (fun e _ => f e) s.

  Parameter iter_e :
    forall `{FArgs} {trace : Set},
    (elt -> Pervasives.result unit trace) -> t ->
    Pervasives.result unit trace.

  Parameter iter_s : forall `{FArgs},
    (elt -> Lwt.t unit) -> t -> Lwt.t unit.

  Parameter iter_p : forall `{FArgs},
    (elt -> Lwt.t unit) -> t -> Lwt.t unit.

  Parameter iter_es :
    forall `{FArgs} {trace : Set},
    (elt -> Lwt.t (Pervasives.result unit trace)) -> t ->
    Lwt.t (Pervasives.result unit trace).

  Definition map `{FArgs} (f : elt -> elt) (s : t) : t :=
    fold (fun e acc => add (f e) acc) s empty.

  Parameter fold_e :
    forall `{FArgs} {a trace : Set},
    (elt -> a -> Pervasives.result a trace) -> t -> a ->
    Pervasives.result a trace.

  Parameter fold_s : forall `{FArgs} {a : Set},
    (elt -> a -> Lwt.t a) -> t -> a -> Lwt.t a.

  Parameter fold_es :
    forall `{FArgs} {a trace : Set},
    (elt -> a -> Lwt.t (Pervasives.result a trace)) -> t -> a ->
    Lwt.t (Pervasives.result a trace).

  Definition for_all `{FArgs} (p : elt -> bool) (s : t) : bool :=
    Map.(Map.S.for_all) (fun e _ => p e) s.

  Definition _exists `{FArgs} (p : elt -> bool) (s : t) : bool :=
    Map.(Map.S._exists) (fun e _ => p e) s.

  Definition subset `{FArgs} (s1 s2 : t) : bool :=
    for_all (fun value1 => mem value1 s2) s1.

  Definition partition `{FArgs} (p : elt -> bool) (s : t) : t * t :=
    Map.(Map.S.partition) (fun e _ => p e) s.

  Definition elements `{FArgs} (s : t) : list elt :=
    List.map fst (Map.(Map.S.bindings) s).

  Definition cardinal `{FArgs} (s : t) : int :=
    Map.(Map.S.cardinal) s.

  Definition option_fst {a b : Set} (x : option (a * b)) : option a :=
    match x with
    | Some (v, _) => Some v
    | None => None
    end.

  Definition min_elt `{FArgs} (s : t) : option elt :=
    option_fst (Map.(Map.S.min_binding) s).

  Definition max_elt `{FArgs} (s : t) : option elt :=
    option_fst (Map.(Map.S.max_binding) s).

  Definition choose `{FArgs} (s : t) : option elt :=
    option_fst (Map.(Map.S.choose) s).

  Definition split `{FArgs} (e : elt) (s : t) : t * bool * t :=
    let '(s_lt, v, s_gt) := Map.(Map.S.split) e s in
    let found := match v with Some _ => true | None => false end in
    (s_lt, found, s_gt).

  (** The [find] operation on the maps would not work as it does not return
      the key. *)
  Definition find `{FArgs} (e : elt) (s : t) : option elt :=
    option_fst (
      Map.(Map.S.find_first)
        (fun e' => Z.eqb (Ord.(Compare.COMPARABLE.compare) e e') 0)
        s
      ).

  Definition find_first `{FArgs} (p : elt -> bool) (s : t) : option elt :=
    option_fst (Map.(Map.S.find_first) p s).

  Definition find_last `{FArgs} (p : elt -> bool) (s : t) : option elt :=
    option_fst (Map.(Map.S.find_last) p s).

  Definition of_list `{FArgs} (l : list elt) : t :=
    List.fold_left (fun s e => add e s) empty l.

  Parameter to_seq_from : forall `{FArgs},
    elt -> t -> CoqOfOCaml.Seq.t elt.

  Parameter to_seq : forall `{FArgs},
    t -> CoqOfOCaml.Seq.t elt.

  Parameter add_seq : forall `{FArgs},
    CoqOfOCaml.Seq.t elt -> t -> t.

  Parameter of_seq : forall `{FArgs},
    CoqOfOCaml.Seq.t elt -> t.

  Parameter iter_ep : forall `{FArgs},
    (elt -> Lwt.t (M? unit)) -> t -> Lwt.t (M? unit).

  Definition functor `(FArgs) :=
    {|
      S.empty := empty;
      S.is_empty := is_empty;
      S.mem := mem;
      S.add := add;
      S.singleton := singleton;
      S.remove := remove;
      S.union := union;
      S.inter := inter;
      S.disjoint := disjoint;
      S.diff_value := diff_value;
      S.compare := compare;
      S.equal := equal;
      S.subset := subset;
      S.iter := iter;
      S.iter_e _ := iter_e;
      S.iter_s := iter_s;
      S.iter_p := iter_p;
      S.iter_es _ := iter_es;
      S.map := map;
      S.fold _ := fold;
      S.fold_e _ _ := fold_e;
      S.fold_s _ := fold_s;
      S.fold_es _ _ := fold_es;
      S.for_all := for_all;
      S._exists := _exists;
      S.filter := filter;
      S.partition := partition;
      S.cardinal := cardinal;
      S.elements := elements;
      S.min_elt := min_elt;
      S.max_elt := max_elt;
      S.choose := choose;
      S.split := split;
      S.find := find;
      S.find_first := find_first;
      S.find_last := find_last;
      S.of_list := of_list;
      S.to_seq_from := to_seq_from;
      S.to_seq := to_seq;
      S.add_seq := add_seq;
      S.of_seq := of_seq;
      S.iter_ep := iter_ep;
    |}.
End Make.

Definition Make_t {Ord_t : Set}
  (Ord : Compare.COMPARABLE (t := Ord_t)) :=
  let '_ := Make.Build_FArgs Ord in
  Make.t.

Definition Make {Ord_t : Set}
  (Ord : Compare.COMPARABLE (t := Ord_t))
  : S (elt := Ord.(Compare.COMPARABLE.t)) (t := Make_t Ord) :=
  Make.functor (Make.Build_FArgs Ord).
