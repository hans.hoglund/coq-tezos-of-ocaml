Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_alpha.Environment.Error_monad.
Require Proto_alpha.Environment.Lwt.
Require Proto_alpha.Environment.Pervasives.
Require Proto_alpha.Environment.Seq.
Import Error_monad.Notations.
Import Lwt.Notations.

Definition t (a : Set) : Set := list a.

Definition nil : forall {a : Set}, list a :=
  fun _ =>
  [].

Definition nil_e : forall {a trace : Set}, Pervasives.result (list a) trace :=
  fun _ _ =>
  return? [].

Definition nil_s : forall {a : Set}, Lwt.t (list a) :=
  fun _ =>
  return= [].

Definition nil_es : forall {a trace : Set},
  Lwt.t (Pervasives.result (list a) trace) :=
  fun _ _ =>
  return=? [].

Definition hd : forall {a : Set}, list a -> option a :=
  fun _ l =>
    match l with
    | [] => None
    | x :: _ => Some x
    end.

Definition tl : forall {a : Set}, list a -> option (list a) :=
  fun _ l =>
    match l with
    | [] => None
    | _x :: m => Some m
    end. 

Fixpoint nth_nat {a : Set} (l : list a) (n : nat) : option a :=
  match l, n with
  | [], _ => None
  | x :: _, O => Some x
  | _ :: l', S n' => nth_nat l' n'
  end.

Definition nth : forall {a : Set}, list a -> int -> option a :=
  fun _ l n =>
  nth_nat l (Z.to_nat n).

Definition nth_opt : forall {a : Set}, list a -> int -> option a :=
  fun _ l n =>
  nth l n.

Parameter last : forall {a : Set}, a -> list a -> a.

Parameter last_opt : forall {a : Set}, list a -> option a.

Definition find : forall {a : Set}, (a -> bool) -> list a -> option a :=
  fun a =>
  List.find (A := a).

Definition find_opt : forall {a : Set}, (a -> bool) -> list a -> option a :=
  fun _ =>
  find.

Parameter mem : forall {a : Set}, (a -> a -> bool) -> a -> list a -> bool.

Parameter assoc : forall {a b : Set},
  (a -> a -> bool) -> a -> list (a * b) -> option b.

Parameter assoc_opt : forall {a b : Set},
  (a -> a -> bool) -> a -> list (a * b) -> option b.

Parameter assq : forall {a b : Set}, a -> list (a * b) -> option b.

Parameter assq_opt : forall {a b : Set}, a -> list (a * b) -> option b.

Parameter mem_assoc : forall {a b : Set},
  (a -> a -> bool) -> a -> list (a * b) -> bool.

Parameter mem_assq : forall {a b : Set}, a -> list (a * b) -> bool.

Parameter remove_assoc : forall {a b : Set},
  (a -> a -> bool) -> a -> list (a * b) -> list (a * b).

Parameter remove_assq : forall {a b : Set}, a -> list (a * b) -> list (a * b).

Parameter init_value : forall {a trace : Set},
  trace -> int -> (int -> a) -> Pervasives.result (list a) trace.

Definition length : forall {a : Set}, list a -> int :=
  fun _ =>
  List.length.

Definition rev : forall {a : Set}, list a -> list a :=
  fun _ =>
  List.rev.

Definition concat : forall {a : Set}, list (list a) -> list a :=
  fun _ =>
  List.concat.

Definition append : forall {a : Set}, list a -> list a -> list a :=
  fun _ =>
  List.append.

Definition rev_append : forall {a : Set}, list a -> list a -> list a :=
  fun _ =>
  List.rev_append.

Definition flatten : forall {a : Set}, list (list a) -> list a :=
  fun _ =>
  List.concat.

Parameter combine : forall {a b trace : Set},
  trace -> list a -> list b -> Pervasives.result (list (a * b)) trace.

Parameter rev_combine : forall {a b trace : Set},
  trace -> list a -> list b -> Pervasives.result (list (a * b)) trace.

Definition split : forall {a b : Set}, list (a * b) -> list a * list b :=
  fun _ _ =>
  List.split.

Parameter iter2 : forall {a b trace : Set},
  trace -> (a -> b -> unit) -> list a -> list b ->
  Pervasives.result unit trace.

Parameter map2 : forall {a b c trace : Set},
  trace -> (a -> b -> c) -> list a -> list b ->
  Pervasives.result (list c) trace.

Parameter rev_map2 : forall {a b c trace : Set},
  trace -> (a -> b -> c) -> list a -> list b ->
  Pervasives.result (list c) trace.

Parameter fold_left2 : forall {a b c trace : Set},
  trace -> (a -> b -> c -> a) -> a -> list b -> list c ->
  Pervasives.result a trace.

Parameter fold_right2 : forall {a b c trace : Set},
  trace -> (a -> b -> c -> c) -> list a -> list b -> c ->
  Pervasives.result c trace.

Parameter for_all2 : forall {a b trace : Set},
  trace -> (a -> b -> bool) -> list a -> list b ->
  Pervasives.result bool trace.

Parameter _exists2 : forall {a b trace : Set},
  trace -> (a -> b -> bool) -> list a -> list b ->
  Pervasives.result bool trace.

Parameter init_e : forall {a trace : Set},
  trace -> int -> (int -> Pervasives.result a trace) ->
  Pervasives.result (list a) trace.

Parameter init_s : forall {a trace : Set},
  trace -> int -> (int -> Lwt.t a) -> Lwt.t (Pervasives.result (list a) trace).

Parameter init_es : forall {a trace : Set},
  trace -> int -> (int -> Lwt.t (Pervasives.result a trace)) ->
  Lwt.t (Pervasives.result (list a) trace).

Parameter init_p : forall {a trace : Set},
  trace -> int -> (int -> Lwt.t a) -> Lwt.t (Pervasives.result (list a) trace).

Parameter find_e : forall {a trace : Set},
  (a -> Pervasives.result bool trace) -> list a ->
  Pervasives.result (option a) trace.

Parameter find_s : forall {a : Set},
  (a -> Lwt.t bool) -> list a -> Lwt.t (option a).

Parameter find_es : forall {a trace : Set},
  (a -> Lwt.t (Pervasives.result bool trace)) -> list a ->
  Lwt.t (Pervasives.result (option a) trace).

Definition filter : forall {a : Set}, (a -> bool) -> list a -> list a :=
  fun _ =>
  List.filter.

Parameter rev_filter : forall {a : Set}, (a -> bool) -> list a -> list a.

Parameter rev_filter_some : forall {a : Set}, list (option a) -> list a.

Parameter filter_some : forall {a : Set}, list (option a) -> list a.

Parameter rev_filter_ok : forall {a b : Set},
  list (Pervasives.result a b) -> list a.

Parameter filter_ok : forall {a b : Set},
  list (Pervasives.result a b) -> list a.

Parameter rev_filter_error : forall {a b : Set},
  list (Pervasives.result a b) -> list b.

Parameter filter_error : forall {a b : Set},
  list (Pervasives.result a b) -> list b.

Parameter rev_filter_e : forall {a trace : Set},
  (a -> Pervasives.result bool trace) -> list a ->
  Pervasives.result (list a) trace.

Fixpoint filter_e {a trace : Set}
  (f : a -> Pervasives.result bool trace) (l : list a)
  : Pervasives.result (list a) trace :=
  match l with
  | [] => return? []
  | x :: l =>
    let? b := f x in
    let? l := filter_e f l in
    if b then
      return? (x :: l)
    else
      return? l
  end.

Parameter rev_filter_s : forall {a : Set},
  (a -> Lwt.t bool) -> list a -> Lwt.t (list a).

Fixpoint filter_s {a : Set}
  (f : a -> Lwt.t bool) (l : list a) : Lwt.t (list a) :=
  match l with
  | [] => return= []
  | x :: l =>
    let= b := f x in
    let= l := filter_s f l in
    if b then
      return= (x :: l)
    else
      return= l
  end.

Parameter rev_filter_es : forall {a trace : Set},
  (a -> Lwt.t (Pervasives.result bool trace)) -> list a ->
  Lwt.t (Pervasives.result (list a) trace).

Fixpoint filter_es {a trace : Set}
  (f : a -> Lwt.t (Pervasives.result bool trace)) (l : list a)
  : Lwt.t (Pervasives.result (list a) trace) :=
  match l with
  | [] => return=? []
  | x :: l =>
    let=? b := f x in
    let=? l := filter_es f l in
    if b then
      return=? (x :: l)
    else
      return=? l
  end.

Parameter filter_p : forall {a : Set},
  (a -> Lwt.t bool) -> list a -> Lwt.t (list a).

Parameter rev_partition : forall {a : Set},
  (a -> bool) -> list a -> list a * list a.

Definition partition : forall {a : Set},
  (a -> bool) -> list a -> list a * list a :=
  fun _ =>
  List.partition.

Parameter rev_partition_result : forall {a b : Set},
  list (Pervasives.result a b) -> list a * list b.

Parameter partition_result : forall {a b : Set},
  list (Pervasives.result a b) -> list a * list b.

Parameter rev_partition_e : forall {a trace : Set},
  (a -> Pervasives.result bool trace) -> list a ->
  Pervasives.result (list a * list a) trace.

Parameter partition_e : forall {a trace : Set},
  (a -> Pervasives.result bool trace) -> list a ->
  Pervasives.result (list a * list a) trace.

Parameter rev_partition_s : forall {a : Set},
  (a -> Lwt.t bool) -> list a -> Lwt.t (list a * list a).

Fixpoint partition_s {a : Set}
  (f : a -> Lwt.t bool) (l : list a) : Lwt.t (list a * list a) :=
  match l with
  | [] => return= ([], [])
  | x :: l =>
    let= b := f x in
    let= '(yes, no) := partition_s f l in
    if b then
      return= (x :: yes, no)
    else
    return= (yes, x :: no)
  end.

Parameter rev_partition_es : forall {a trace : Set},
  (a -> Lwt.t (Pervasives.result bool trace)) -> list a ->
  Lwt.t (Pervasives.result (list a * list a) trace).

Parameter partition_es : forall {a trace : Set},
  (a -> Lwt.t (Pervasives.result bool trace)) -> list a ->
  Lwt.t (Pervasives.result (list a * list a) trace).

Parameter partition_p : forall {a : Set},
  (a -> Lwt.t bool) -> list a -> Lwt.t (list a * list a).

Parameter iter : forall {a : Set}, (a -> unit) -> list a -> unit.

Fixpoint iter_e {a trace : Set}
  (f : a -> Pervasives.result unit trace) (l : list a)
  : Pervasives.result unit trace :=
  match l with
  | [] => return? tt
  | x :: l =>
    let? _ := f x in
    iter_e f l
  end.

Fixpoint iter_s {a : Set}
  (f : a -> Lwt.t unit) (l : list a) : Lwt.t unit :=
  match l with
  | [] => return= tt
  | x :: l =>
    let= _ := f x in
    iter_s f l
  end.

Fixpoint iter_es {a trace : Set}
  (f : a -> Lwt.t (Pervasives.result unit trace)) (l : list a)
  : Lwt.t (Pervasives.result unit trace) :=
  match l with
  | [] => return=? tt
  | x :: l =>
    let=? _ := f x in
    iter_es f l
  end.

Parameter iter_p : forall {a : Set}, (a -> Lwt.t unit) -> list a -> Lwt.t unit.

Parameter iteri : forall {a : Set}, (int -> a -> unit) -> list a -> unit.

Parameter iteri_e : forall {a trace : Set},
  (int -> a -> Pervasives.result unit trace) -> list a ->
  Pervasives.result unit trace.

Parameter iteri_s : forall {a : Set},
  (int -> a -> Lwt.t unit) -> list a -> Lwt.t unit.

Parameter iteri_es : forall {a trace : Set},
  (int -> a -> Lwt.t (Pervasives.result unit trace)) -> list a ->
  Lwt.t (Pervasives.result unit trace).

Parameter iteri_p : forall {a : Set},
  (int -> a -> Lwt.t unit) -> list a -> Lwt.t unit.

Definition map : forall {a b : Set}, (a -> b) -> list a -> list b :=
  fun _ _ =>
  List.map.

Fixpoint map_e {a b trace : Set}
  (f : a -> Pervasives.result b trace) (l : list a)
  : Pervasives.result (list b) trace :=
  match l with
  | [] => return? []
  | x :: l =>
    let? y := f x in
    let? l' := map_e f l in
    return? (y :: l')
  end.

Fixpoint map_s {a b : Set}
  (f : a -> Lwt.t b) (l : list a) : Lwt.t (list b) :=
  match l with
  | [] => return= []
  | x :: l =>
    let= x := f x in
    let= l := map_s f l in
    return= (x :: l)
  end.

Fixpoint map_es {a b trace : Set}
  (f : a -> Lwt.t (Pervasives.result b trace)) (l : list a)
  : Lwt.t (Pervasives.result (list b) trace) :=
  match l with
  | [] => return=? []
  | x :: l =>
    let=? y := f x in
    let=? l' := map_es f l in
    return=? (y :: l')
  end.

Parameter map_p : forall {a b : Set},
  (a -> Lwt.t b) -> list a -> Lwt.t (list b).

Definition mapi : forall {a b : Set}, (int -> a -> b) -> list a -> list b :=
  fun _ _ =>
  List.mapi.

Parameter mapi_e : forall {a b trace : Set},
  (int -> a -> Pervasives.result b trace) -> list a ->
  Pervasives.result (list b) trace.

Parameter mapi_s : forall {a b : Set},
  (int -> a -> Lwt.t b) -> list a -> Lwt.t (list b).

Parameter mapi_es : forall {a b trace : Set},
  (int -> a -> Lwt.t (Pervasives.result b trace)) -> list a ->
  Lwt.t (Pervasives.result (list b) trace).

Parameter mapi_p : forall {a b : Set},
  (int -> a -> Lwt.t b) -> list a -> Lwt.t (list b).

Definition rev_map : forall {a b : Set}, (a -> b) -> list a -> list b :=
  fun _ _ =>
  List.rev_map.

Parameter rev_mapi : forall {a b : Set}, (int -> a -> b) -> list a -> list b.

Parameter rev_map_e : forall {a b trace : Set},
  (a -> Pervasives.result b trace) -> list a ->
  Pervasives.result (list b) trace.

Parameter rev_map_s : forall {a b : Set},
  (a -> Lwt.t b) -> list a -> Lwt.t (list b).

Parameter rev_map_es : forall {a b trace : Set},
  (a -> Lwt.t (Pervasives.result b trace)) -> list a ->
  Lwt.t (Pervasives.result (list b) trace).

Parameter rev_map_p : forall {a b : Set},
  (a -> Lwt.t b) -> list a -> Lwt.t (list b).

Parameter rev_mapi_e : forall {a b trace : Set},
  (int -> a -> Pervasives.result b trace) -> list a ->
  Pervasives.result (list b) trace.

Parameter rev_mapi_s : forall {a b : Set},
  (int -> a -> Lwt.t b) -> list a -> Lwt.t (list b).

Parameter rev_mapi_es : forall {a b trace : Set},
  (int -> a -> Lwt.t (Pervasives.result b trace)) -> list a ->
  Lwt.t (Pervasives.result (list b) trace).

Parameter rev_mapi_p : forall {a b : Set},
  (int -> a -> Lwt.t b) -> list a -> Lwt.t (list b).

Parameter rev_filter_map : forall {a b : Set},
  (a -> option b) -> list a -> list b.

Parameter rev_filter_map_e : forall {a b trace : Set},
  (a -> Pervasives.result (option b) trace) -> list a ->
  Pervasives.result (list b) trace.

Parameter filter_map_e : forall {a b trace : Set},
  (a -> Pervasives.result (option b) trace) -> list a ->
  Pervasives.result (list b) trace.

Parameter rev_filter_map_s : forall {a b : Set},
  (a -> Lwt.t (option b)) -> list a -> Lwt.t (list b).

Fixpoint filter_map {a b : Set} (f : a -> option b) (l : list a) : list b :=
  match l with
  | [] => []
  | x :: l' =>
    match f x with
    | None => filter_map f l'
    | Some y => y :: filter_map f l'
    end
  end.

Fixpoint filter_map_s {a b : Set}
  (f : a -> Lwt.t (option b)) (l : list a) : Lwt.t (list b) :=
  match l with
  | [] => return= []
  | x :: l =>
    let= y := f x in
    let= l' := filter_map_s f l in
    match y with
    | Some y => return= (y :: l')
    | None => return= l'
    end
  end.

Parameter rev_filter_map_es : forall {a b trace : Set},
  (a -> Lwt.t (Pervasives.result (option b) trace)) -> list a ->
  Lwt.t (Pervasives.result (list b) trace).

Fixpoint filter_map_es {a b trace : Set}
  (f : a -> Lwt.t (Pervasives.result (option b) trace)) (l : list a)
  : Lwt.t (Pervasives.result (list b) trace) :=
  match l with
  | [] => return=? []
  | x :: l =>
    let=? y := f x in
    let=? l' := filter_map_es f l in
    match y with
    | Some y => return=? (y :: l')
    | None => return=? l'
    end
  end.

Parameter filter_map_p : forall {a b : Set},
  (a -> Lwt.t (option b)) -> list a -> Lwt.t (list b).

Definition fold_left : forall {a b : Set}, (a -> b -> a) -> a -> list b -> a :=
  fun _ _ =>
  List.fold_left.

Parameter fold_left_e : forall {a b trace : Set},
  (a -> b -> Pervasives.result a trace) -> a -> list b ->
  Pervasives.result a trace.

Fixpoint fold_left_s {a b : Set}
  (f : a -> b -> Lwt.t a) (accumulator : a) (l : list b) : Lwt.t a :=
  match l with
  | [] => return= accumulator
  | x :: l =>
    let= accumulator := f accumulator x in
    fold_left_s f accumulator l
  end.

Fixpoint fold_left_es {a b trace : Set}
  (f : a -> b -> Lwt.t (Pervasives.result a trace)) (accumulator : a)
  (l : list b) : Lwt.t (Pervasives.result a trace) :=
  match l with
  | [] => return=? accumulator
  | x :: l =>
    let=? accumulator := f accumulator x in
    fold_left_es f accumulator l
  end.

Definition fold_right : forall {a b : Set}, (a -> b -> b) -> list a -> b -> b :=
  fun _ _ =>
  List.fold_right.

Parameter fold_right_e : forall {a b trace : Set},
  (a -> b -> Pervasives.result b trace) -> list a -> b ->
  Pervasives.result b trace.

Fixpoint fold_right_s {a b : Set}
  (f : a -> b -> Lwt.t b) (l : list a) (accumulator : b) : Lwt.t b :=
  match l with
  | [] => return= accumulator
  | x :: l =>
    let= accumulator := fold_right_s f l accumulator in
    f x accumulator
  end.

Fixpoint fold_right_es {a b trace : Set}
  (f : a -> b -> Lwt.t (Pervasives.result b trace)) (l : list a)
  (accumulator : b) : Lwt.t (Pervasives.result b trace) :=
  match l with
  | [] => return=? accumulator
  | x :: l =>
    let=? accumulator := fold_right_es f l accumulator in
    f x accumulator
  end.

Parameter iter2_e : forall {a b trace : Set},
  trace -> (a -> b -> Pervasives.result unit trace) -> list a -> list b ->
  Pervasives.result unit trace.

Parameter iter2_s : forall {a b trace : Set},
  trace -> (a -> b -> Lwt.t unit) -> list a -> list b ->
  Lwt.t (Pervasives.result unit trace).

Parameter iter2_es : forall {a b trace : Set},
  trace -> (a -> b -> Lwt.t (Pervasives.result unit trace)) -> list a ->
  list b -> Lwt.t (Pervasives.result unit trace).

Parameter map2_e : forall {a b c trace : Set},
  trace -> (a -> b -> Pervasives.result c trace) -> list a -> list b ->
  Pervasives.result (list c) trace.

Parameter map2_s : forall {a b c trace : Set},
  trace -> (a -> b -> Lwt.t c) -> list a -> list b ->
  Lwt.t (Pervasives.result (list c) trace).

Parameter map2_es : forall {a b c trace : Set},
  trace -> (a -> b -> Lwt.t (Pervasives.result c trace)) -> list a ->
  list b -> Lwt.t (Pervasives.result (list c) trace).

Parameter rev_map2_e : forall {a b c trace : Set},
  trace -> (a -> b -> Pervasives.result c trace) -> list a -> list b ->
  Pervasives.result (list c) trace.

Parameter rev_map2_s : forall {a b c trace : Set},
  trace -> (a -> b -> Lwt.t c) -> list a -> list b ->
  Lwt.t (Pervasives.result (list c) trace).

Parameter rev_map2_es : forall {a b c trace : Set},
  trace -> (a -> b -> Lwt.t (Pervasives.result c trace)) -> list a ->
  list b -> Lwt.t (Pervasives.result (list c) trace).

Parameter fold_left2_e : forall {a b c trace : Set},
  trace -> (a -> b -> c -> Pervasives.result a trace) -> a -> list b ->
  list c -> Pervasives.result a trace.

Parameter fold_left2_s : forall {a b c trace : Set},
  trace -> (a -> b -> c -> Lwt.t a) -> a -> list b -> list c ->
  Lwt.t (Pervasives.result a trace).

Parameter fold_left2_es : forall {a b c trace : Set},
  trace -> (a -> b -> c -> Lwt.t (Pervasives.result a trace)) -> a ->
  list b -> list c -> Lwt.t (Pervasives.result a trace).

Parameter fold_right2_e : forall {a b c trace : Set},
  trace -> (a -> b -> c -> Pervasives.result c trace) -> list a -> list b ->
  c -> Pervasives.result c trace.

Parameter fold_right2_s : forall {a b c trace : Set},
  trace -> (a -> b -> c -> Lwt.t c) -> list a -> list b -> c ->
  Lwt.t (Pervasives.result c trace).

Parameter fold_right2_es : forall {a b c trace : Set},
  trace -> (a -> b -> c -> Lwt.t (Pervasives.result c trace)) -> list a ->
  list b -> c -> Lwt.t (Pervasives.result c trace).

Definition for_all : forall {a : Set}, (a -> bool) -> list a -> bool :=
  fun a =>
  List.forallb (A := a).

Parameter for_all_e : forall {a trace : Set},
  (a -> Pervasives.result bool trace) -> list a ->
  Pervasives.result bool trace.

Fixpoint for_all_s {a : Set}
  (f : a -> Lwt.t bool) (l : list a) : Lwt.t bool :=
  match l with
  | [] => return= true
  | x :: l =>
    let= b := f x in
    if b then
      for_all_s f l
    else
      return= false
  end.

Parameter for_all_es : forall {a trace : Set},
  (a -> Lwt.t (Pervasives.result bool trace)) -> list a ->
  Lwt.t (Pervasives.result bool trace).

Parameter for_all_p : forall {a : Set},
  (a -> Lwt.t bool) -> list a -> Lwt.t bool.

Definition _exists : forall {a : Set}, (a -> bool) -> list a -> bool :=
  fun a =>
  List.existsb (A := a).

Parameter exists_e : forall {a trace : Set},
  (a -> Pervasives.result bool trace) -> list a ->
  Pervasives.result bool trace.

Fixpoint exists_s {a : Set}
  (f : a -> Lwt.t bool) (l : list a) : Lwt.t bool :=
  match l with
  | [] => return= false
  | x :: l =>
    let= b := f x in
    if b then
      return= true
    else
      exists_s f l
  end.

Parameter exists_es : forall {a trace : Set},
  (a -> Lwt.t (Pervasives.result bool trace)) -> list a ->
  Lwt.t (Pervasives.result bool trace).

Parameter exists_p : forall {a : Set},
  (a -> Lwt.t bool) -> list a -> Lwt.t bool.

Parameter for_all2_e : forall {a b trace : Set},
  trace -> (a -> b -> Pervasives.result bool trace) -> list a -> list b ->
  Pervasives.result bool trace.

Parameter for_all2_s : forall {a b trace : Set},
  trace -> (a -> b -> Lwt.t bool) -> list a -> list b ->
  Lwt.t (Pervasives.result bool trace).

Parameter for_all2_es : forall {a b trace : Set},
  trace -> (a -> b -> Lwt.t (Pervasives.result bool trace)) -> list a ->
  list b -> Lwt.t (Pervasives.result bool trace).

Parameter exists2_e : forall {a b trace : Set},
  trace -> (a -> b -> Pervasives.result bool trace) -> list a -> list b ->
  Pervasives.result bool trace.

Parameter exists2_s : forall {a b trace : Set},
  trace -> (a -> b -> Lwt.t bool) -> list a -> list b ->
  Lwt.t (Pervasives.result bool trace).

Parameter exists2_es : forall {a b trace : Set},
  trace -> (a -> b -> Lwt.t (Pervasives.result bool trace)) -> list a ->
  list b -> Lwt.t (Pervasives.result bool trace).

Parameter combine_drop : forall {a b : Set}, list a -> list b -> list (a * b).

Inductive left_or_right_list (a b : Set) : Set :=
| Right : list b -> left_or_right_list a b
| Left : list a -> left_or_right_list a b.

Arguments Right {_ _}.
Arguments Left {_ _}.

Parameter combine_with_leftovers : forall {a b : Set},
  list a -> list b -> list (a * b) * option (left_or_right_list a b).

Parameter compare : forall {a : Set},
  (a -> a -> int) -> list a -> list a -> int.

Definition compare_lengths : forall {a : Set}, list a -> list a -> int :=
  fun _ l1 l2 =>
  Z.sub (length l1) (length l2).

Definition compare_length_with : forall {a : Set}, list a -> int -> int :=
  fun _ l n =>
  Z.sub (length l) n.

Parameter equal : forall {a : Set},
  (a -> a -> bool) -> list a -> list a -> bool.

Parameter sort : forall {a : Set}, (a -> a -> int) -> list a -> list a.

Parameter stable_sort : forall {a : Set}, (a -> a -> int) -> list a -> list a.

Parameter fast_sort : forall {a : Set}, (a -> a -> int) -> list a -> list a.

Parameter sort_uniq : forall {a : Set}, (a -> a -> int) -> list a -> list a.

Parameter to_seq : forall {a : Set}, t a -> Seq.t a.

Parameter of_seq : forall {a : Set}, Seq.t a -> list a.

Parameter init_ep : forall {_error a : Set},
  _error -> int -> (int -> Lwt.t (M? a)) -> Lwt.t (M? (list a)).

Parameter filter_ep : forall {a : Set},
  (a -> Lwt.t (M? bool)) -> list a -> Lwt.t (M? (list a)).

Parameter partition_ep : forall {a : Set},
  (a -> Lwt.t (M? bool)) -> list a -> Lwt.t (M? (list a * list a)).

Parameter iter_ep : forall {a : Set},
  (a -> Lwt.t (M? unit)) -> list a -> Lwt.t (M? unit).

Parameter iteri_ep : forall {a : Set},
  (int -> a -> Lwt.t (M? unit)) -> list a -> Lwt.t (M? unit).

Parameter map_ep : forall {a b : Set},
  (a -> Lwt.t (M? b)) -> list a -> Lwt.t (M? (list b)).

Parameter mapi_ep : forall {a b : Set},
  (int -> a -> Lwt.t (M? b)) -> list a -> Lwt.t (M? (list b)).

Parameter rev_map_ep : forall {a b : Set},
  (a -> Lwt.t (M? b)) -> list a -> Lwt.t (M? (list b)).

Parameter rev_mapi_ep : forall {a b : Set},
  (int -> a -> Lwt.t (M? b)) -> list a -> Lwt.t (M? (list b)).

Parameter filter_map_ep : forall {a b : Set},
  (a -> Lwt.t (M? (option b))) -> list a -> Lwt.t (M? (list b)).

Parameter for_all_ep : forall {a : Set},
  (a -> Lwt.t (M? bool)) -> list a -> Lwt.t (M? bool).

Parameter exists_ep : forall {a : Set},
  (a -> Lwt.t (M? bool)) -> list a -> Lwt.t (M? bool).
