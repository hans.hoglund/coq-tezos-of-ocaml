Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Parameter t : forall (a : Set), Set.
  
Parameter make : forall {a : Set}, int -> a -> t a.

Parameter fallback : forall {a : Set}, t a -> a.

Parameter length : forall {a : Set}, t a -> int.

Parameter get : forall {a : Set}, t a -> int -> a.

Parameter set : forall {a : Set}, t a -> int -> a -> unit.

Parameter iter : forall {a : Set}, (a -> unit) -> t a -> unit.

Parameter map : forall {a b : Set}, (a -> b) -> t a -> t b.

Parameter fold : forall {a b : Set}, (b -> a -> b) -> t a -> b -> b.
