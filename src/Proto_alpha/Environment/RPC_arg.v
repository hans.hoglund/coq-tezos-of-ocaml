Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_alpha.Environment.Pervasives.

Parameter t : forall (a : Set), Set.

Definition arg (a : Set) : Set := t a.

Parameter make : forall {a : Set},
  option string -> string -> (string -> Pervasives.result a string) ->
  (a -> string) -> unit -> arg a.

Module descr.
  Record record : Set := Build {
    name : string;
    descr : option string }.
  Definition with_name name (r : record) :=
    Build name r.(descr).
  Definition with_descr descr (r : record) :=
    Build r.(name) descr.
End descr.
Definition descr := descr.record.

Parameter descr_value : forall {a : Set}, arg a -> descr.

Parameter bool_value : arg bool.

Parameter int_value : arg int.

Parameter int32_value : arg int32.

Parameter int64_value : arg int64.

Parameter string_value : arg string.

Parameter like : forall {a : Set}, arg a -> option string -> string -> arg a.

Inductive eq (a : Set) : Set :=
| Eq : eq a.

Arguments Eq {_}.

Parameter eq_value : forall {a b : Set}, arg a -> arg b -> option (eq a).
