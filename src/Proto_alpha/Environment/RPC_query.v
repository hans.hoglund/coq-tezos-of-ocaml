Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_alpha.Environment.RPC_arg.

Parameter t : forall (a : Set), Set.

Definition query (a : Set) : Set := t a.

Parameter empty : query unit.

Parameter field : forall (a b : Set), Set.

Parameter field_value : forall {a b : Set},
  option string -> string -> RPC_arg.t a -> a -> (b -> a) -> field b a.

Parameter opt_field : forall {a b : Set},
  option string -> string -> RPC_arg.t a -> (b -> option a) ->
  field b (option a).

Parameter flag : forall {b : Set},
  option string -> string -> (b -> bool) -> field b bool.

Parameter multi_field : forall {a b : Set},
  option string -> string -> RPC_arg.t a -> (b -> list a) -> field b (list a).

Parameter open_query : forall (a b c : Set), Set.

Parameter query_value : forall {a b : Set}, b -> open_query a b b.

Parameter op_pipeplus : forall {a b c d : Set},
  open_query a b (c -> d) -> field a c -> open_query a b d.

Parameter seal : forall {a b : Set}, open_query a b a -> t a.

Definition untyped : Set := list (string * string).

Parameter parse : forall {a : Set}, query a -> untyped -> a.
