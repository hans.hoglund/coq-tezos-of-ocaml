Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_alpha.Environment.Pervasives.

Parameter formatter : Set.

Parameter pp_open_box : formatter -> int -> unit.

Parameter pp_close_box : formatter -> unit -> unit.

Parameter pp_open_hbox : formatter -> unit -> unit.

Parameter pp_open_vbox : formatter -> int -> unit.

Parameter pp_open_hvbox : formatter -> int -> unit.

Parameter pp_open_hovbox : formatter -> int -> unit.

Parameter pp_print_string : formatter -> string -> unit.

Parameter pp_print_as : formatter -> int -> string -> unit.

Parameter pp_print_int : formatter -> int -> unit.

Parameter pp_print_char : formatter -> ascii -> unit.

Parameter pp_print_bool : formatter -> bool -> unit.

Parameter pp_print_space : formatter -> unit -> unit.

Parameter pp_print_cut : formatter -> unit -> unit.

Parameter pp_print_break : formatter -> int -> int -> unit.

Parameter pp_print_custom_break :
  formatter -> string * int * string -> string * int * string -> unit.

Parameter pp_force_newline : formatter -> unit -> unit.

Parameter pp_print_if_newline : formatter -> unit -> unit.

Parameter pp_print_flush : formatter -> unit -> unit.

Parameter pp_print_newline : formatter -> unit -> unit.

Parameter pp_set_margin : formatter -> int -> unit.

Parameter pp_get_margin : formatter -> unit -> int.

Parameter pp_set_max_indent : formatter -> int -> unit.

Parameter pp_get_max_indent : formatter -> unit -> int.

Parameter pp_set_max_boxes : formatter -> int -> unit.

Parameter pp_get_max_boxes : formatter -> unit -> int.

Parameter pp_over_max_boxes : formatter -> unit -> bool.

Parameter pp_open_tbox : formatter -> unit -> unit.

Parameter pp_close_tbox : formatter -> unit -> unit.

Parameter pp_set_tab : formatter -> unit -> unit.

Parameter pp_print_tab : formatter -> unit -> unit.

Parameter pp_print_tbreak : formatter -> int -> int -> unit.

Parameter pp_set_ellipsis_text : formatter -> string -> unit.

Parameter pp_get_ellipsis_text : formatter -> unit -> string.

Parameter pp_print_list : forall {a : Set},
  option (formatter -> unit -> unit) -> (formatter -> a -> unit) ->
  formatter -> list a -> unit.

Parameter pp_print_text : formatter -> string -> unit.

Parameter pp_print_option : forall {a : Set},
  option (formatter -> unit -> unit) -> (formatter -> a -> unit) ->
  formatter -> option a -> unit.

Parameter pp_print_result : forall {a e : Set},
  (formatter -> a -> unit) -> (formatter -> e -> unit) -> formatter ->
  Pervasives.result a e -> unit.

Parameter fprintf : forall {a : Set},
  formatter -> Pervasives.format a formatter unit -> a.

Parameter sprintf : forall {a : Set}, Pervasives.format a unit string -> a.

Parameter asprintf : forall {a : Set},
  Pervasives.format4 a formatter unit string -> a.

Parameter dprintf : forall {a : Set},
  Pervasives.format4 a formatter unit (formatter -> unit) -> a.

Parameter ifprintf : forall {a : Set},
  formatter -> Pervasives.format a formatter unit -> a.

Parameter kfprintf : forall {a b : Set},
  (formatter -> a) -> formatter -> Pervasives.format4 b formatter unit a -> b.

Parameter kdprintf : forall {a b : Set},
  ((formatter -> unit) -> a) -> Pervasives.format4 b formatter unit a -> b.

Parameter ikfprintf : forall {a b : Set},
  (formatter -> a) -> formatter -> Pervasives.format4 b formatter unit a -> b.

Parameter ksprintf : forall {a b : Set},
  (string -> a) -> Pervasives.format4 b unit string a -> b.

Parameter kasprintf : forall {a b : Set},
  (string -> a) -> Pervasives.format4 b formatter unit a -> b.
