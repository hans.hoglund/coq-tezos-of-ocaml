Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Inductive eq : Set :=
| Refl : eq.

Parameter t : forall (a : Set), Set.

Parameter make : forall {a : Set}, unit -> t a.

Parameter eq_value : forall {a b : Set}, t a -> t b -> option eq.

Parameter hash_value : forall {a : Set}, t a -> int.
