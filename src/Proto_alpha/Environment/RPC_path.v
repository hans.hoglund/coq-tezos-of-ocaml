Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_alpha.Environment.RPC_arg.

Parameter t : forall (prefix params : Set), Set.

Definition path (prefix params : Set) : Set := t prefix params.

Definition context (prefix : Set) : Set := path prefix prefix.

Parameter root : context unit.

Parameter open_root : forall {a : Set}, context a.

Parameter add_suffix : forall {params prefix : Set},
  path prefix params -> string -> path prefix params.

Parameter op_div : forall {params prefix : Set},
  path prefix params -> string -> path prefix params.

Parameter add_arg : forall {a params prefix : Set},
  path prefix params -> RPC_arg.t a -> path prefix (params * a).

Parameter op_divcolon : forall {a params prefix : Set},
  path prefix params -> RPC_arg.t a -> path prefix (params * a).

Parameter add_final_args : forall {a params prefix : Set},
  path prefix params -> RPC_arg.t a -> path prefix (params * list a).

Parameter op_divcolonstar : forall {a params prefix : Set},
  path prefix params -> RPC_arg.t a -> path prefix (params * list a).
