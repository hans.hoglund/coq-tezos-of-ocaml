Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_alpha.Environment.Data_encoding.
Require Proto_alpha.Environment.RPC_path.
Require Proto_alpha.Environment.RPC_query.

Inductive meth : Set :=
| PUT : meth
| GET : meth
| DELETE : meth
| POST : meth
| PATCH : meth.

Parameter t : forall (prefix params query input output : Set), Set.

Definition service (prefix params query input output : Set) : Set :=
  t prefix params query input output.

Parameter get_service : forall {output params prefix query : Set},
  option string -> RPC_query.t query -> Data_encoding.t output ->
  RPC_path.t prefix params -> service prefix params query unit output.

Parameter post_service : forall {input output params prefix query : Set},
  option string -> RPC_query.t query -> Data_encoding.t input ->
  Data_encoding.t output -> RPC_path.t prefix params ->
  service prefix params query input output.

Parameter delete_service : forall {output params prefix query : Set},
  option string -> RPC_query.t query -> Data_encoding.t output ->
  RPC_path.t prefix params -> service prefix params query unit output.

Parameter patch_service : forall {input output params prefix query : Set},
  option string -> RPC_query.t query -> Data_encoding.t input ->
  Data_encoding.t output -> RPC_path.t prefix params ->
  service prefix params query input output.

Parameter put_service : forall {input output params prefix query : Set},
  option string -> RPC_query.t query -> Data_encoding.t input ->
  Data_encoding.t output -> RPC_path.t prefix params ->
  service prefix params query input output.
