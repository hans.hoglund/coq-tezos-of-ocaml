Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_alpha.Environment.Format.

Inductive level : Set :=
| Debug : level
| Info : level
| Notice : level
| Warning : level
| Error : level
| Fatal : level.

Parameter log : forall {a : Set},
  level -> Pervasives.format4 a Format.formatter unit unit -> a.

Parameter log_string : level -> string -> unit.
