Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_alpha.Environment.Lwt.
Require Proto_alpha.Environment.Pervasives.

Definition t (a e : Set) : Set := Pervasives.result a e.

Parameter ok : forall {a e : Set}, a -> Pervasives.result a e.

Parameter ok_s : forall {a e : Set}, a -> Lwt.t (Pervasives.result a e).

Parameter error_value : forall {a e : Set}, e -> Pervasives.result a e.

Parameter error_s : forall {a e : Set}, e -> Lwt.t (Pervasives.result a e).

Parameter value : forall {a e : Set}, Pervasives.result a e -> a -> a.

Parameter value_f : forall {a e : Set},
  Pervasives.result a e -> (unit -> a) -> a.

Parameter bind : forall {a b e : Set},
  Pervasives.result a e -> (a -> Pervasives.result b e) ->
  Pervasives.result b e.

Parameter bind_s : forall {a b e : Set},
  Pervasives.result a e -> (a -> Lwt.t (Pervasives.result b e)) ->
  Lwt.t (Pervasives.result b e).

Parameter bind_error : forall {a e f : Set},
  Pervasives.result a e -> (e -> Pervasives.result a f) ->
  Pervasives.result a f.

Parameter bind_error_s : forall {a e f : Set},
  Pervasives.result a e -> (e -> Lwt.t (Pervasives.result a f)) ->
  Lwt.t (Pervasives.result a f).

Parameter join : forall {a e : Set},
  Pervasives.result (Pervasives.result a e) e -> Pervasives.result a e.

Parameter map : forall {a b e : Set},
  (a -> b) -> Pervasives.result a e -> Pervasives.result b e.

Parameter map_e : forall {a b e : Set},
  (a -> Pervasives.result b e) -> Pervasives.result a e ->
  Pervasives.result b e.

Parameter map_s : forall {a b e : Set},
  (a -> Lwt.t b) -> Pervasives.result a e -> Lwt.t (Pervasives.result b e).

Parameter map_es : forall {a b e : Set},
  (a -> Lwt.t (Pervasives.result b e)) -> Pervasives.result a e ->
  Lwt.t (Pervasives.result b e).

Parameter map_error : forall {a e f : Set},
  (e -> f) -> Pervasives.result a e -> Pervasives.result a f.

Parameter map_error_e : forall {a e f : Set},
  (e -> Pervasives.result a f) -> Pervasives.result a e ->
  Pervasives.result a f.

Parameter map_error_s : forall {a e f : Set},
  (e -> Lwt.t f) -> Pervasives.result a e -> Lwt.t (Pervasives.result a f).

Parameter map_error_es : forall {a e f : Set},
  (e -> Lwt.t (Pervasives.result a f)) -> Pervasives.result a e ->
  Lwt.t (Pervasives.result a f).

Parameter fold : forall {a c e : Set},
  (a -> c) -> (e -> c) -> Pervasives.result a e -> c.

Parameter iter : forall {a e : Set},
  (a -> unit) -> Pervasives.result a e -> unit.

Parameter iter_s : forall {a e : Set},
  (a -> Lwt.t unit) -> Pervasives.result a e -> Lwt.t unit.

Parameter iter_error : forall {a e : Set},
  (e -> unit) -> Pervasives.result a e -> unit.

Parameter iter_error_s : forall {a e : Set},
  (e -> Lwt.t unit) -> Pervasives.result a e -> Lwt.t unit.

Parameter is_ok : forall {a e : Set}, Pervasives.result a e -> bool.

Parameter is_error : forall {a e : Set}, Pervasives.result a e -> bool.

Parameter equal : forall {a e : Set},
  (a -> a -> bool) -> (e -> e -> bool) -> Pervasives.result a e ->
  Pervasives.result a e -> bool.

Parameter compare : forall {a e : Set},
  (a -> a -> int) -> (e -> e -> int) -> Pervasives.result a e ->
  Pervasives.result a e -> int.

Parameter to_option : forall {a e : Set}, Pervasives.result a e -> option a.

Parameter of_option : forall {a e : Set},
  e -> option a -> Pervasives.result a e.

Parameter to_list : forall {a e : Set}, Pervasives.result a e -> list a.

Parameter to_seq : forall {a e : Set},
  Pervasives.result a e -> CoqOfOCaml.Seq.t a.

Parameter catch : forall {a : Set},
  option (extensible_type -> bool) -> (unit -> a) ->
  Pervasives.result a extensible_type.

Parameter catch_f : forall {_error a : Set},
  option (extensible_type -> bool) -> (unit -> a) ->
  (extensible_type -> _error) -> Pervasives.result a _error.

Parameter catch_s : forall {a : Set},
  option (extensible_type -> bool) -> (unit -> Lwt.t a) ->
  Lwt.t (Pervasives.result a extensible_type).
