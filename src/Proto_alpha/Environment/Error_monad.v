Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_alpha.Environment.Data_encoding.
Require Proto_alpha.Environment.Lwt.
Import Lwt.Notations.

Inductive error_category : Set :=
| Permanent : error_category
| Temporary : error_category
| Branch : error_category.

Definition _error : Set := extensible_type.

Parameter error_encoding : Data_encoding.t _error.

Parameter pp : Format.formatter -> _error -> unit.

Parameter register_error_kind : forall {err : Set},
  error_category -> string -> string -> string ->
  option (Format.formatter -> err -> unit) -> Data_encoding.t err ->
  (_error -> option err) -> (err -> _error) -> unit.

Parameter classify_error : _error -> error_category.

Parameter json_of_error : _error -> Data_encoding.json.

Parameter error_of_json : Data_encoding.json -> _error.

Module error_info.
  Record record : Set := Build {
    category : error_category;
    id : string;
    title : string;
    description : string;
    schema : Data_encoding.json_schema }.
  Definition with_category category (r : record) :=
    Build category r.(id) r.(title) r.(description) r.(schema).
  Definition with_id id (r : record) :=
    Build r.(category) id r.(title) r.(description) r.(schema).
  Definition with_title title (r : record) :=
    Build r.(category) r.(id) title r.(description) r.(schema).
  Definition with_description description (r : record) :=
    Build r.(category) r.(id) r.(title) description r.(schema).
  Definition with_schema schema (r : record) :=
    Build r.(category) r.(id) r.(title) r.(description) schema.
End error_info.
Definition error_info := error_info.record.

Parameter pp_info : Format.formatter -> error_info -> unit.

Parameter get_registered_errors : unit -> list error_info.

Definition trace (err : Set) : Set := list err.

Definition tzresult (a : Set) : Set := Pervasives.result a (trace _error).

Parameter make_trace_encoding : forall {_error : Set},
  Data_encoding.t _error -> Data_encoding.t (trace _error).

Parameter trace_encoding : Data_encoding.t (trace _error).

Parameter pp_trace : Format.formatter -> trace _error -> unit.

Parameter result_encoding : forall {a : Set},
  Data_encoding.t a -> Data_encoding.t (tzresult a).

Definition ok {a trace : Set} (x : a) : Pervasives.result a trace :=
  Pervasives.Ok x.

Definition op_gtgtquestion {a b trace : Set}
  (x : Pervasives.result a trace) (f : a -> Pervasives.result b trace) :
  Pervasives.result b trace :=
  match x with
  | Pervasives.Ok x => f x
  | Pervasives.Error trace => Pervasives.Error trace
  end.

Definition _return {a trace : Set} (x : a) :
  Lwt.t (Pervasives.result a trace) :=
  Lwt._return (ok x).

Definition op_gtgteqquestion {a b trace : Set}
  (x : Lwt.t (Pervasives.result a trace))
  (f : a -> Lwt.t (Pervasives.result b trace)) :
  Lwt.t (Pervasives.result b trace) :=
  Lwt.bind x (fun x =>
  match x with
  | Pervasives.Ok x => f x
  | Pervasives.Error trace => Lwt._return (Pervasives.Error trace)
  end).

Module Notations.
  Notation "M=? X" := (Lwt.t (tzresult X)) (at level 20).

  Notation "M? X" := (tzresult X) (at level 20).

  Notation "return? X" := (ok X) (at level 20).

  Notation "'let?' x ':=' X 'in' Y" :=
    (op_gtgtquestion X (fun x => Y))
    (at level 200, x name, X at level 100, Y at level 200).

  Notation "'let?' ' x ':=' X 'in' Y" :=
    (op_gtgtquestion X (fun x => Y))
    (at level 200, x pattern, X at level 100, Y at level 200).

  Notation "return=? X" := (_return X) (at level 20).

  Notation "'let=?' x ':=' X 'in' Y" :=
    (op_gtgteqquestion X (fun x => Y))
    (at level 200, x name, X at level 100, Y at level 200).

  Notation "'let=?' ' x ':=' X 'in' Y" :=
    (op_gtgteqquestion X (fun x => Y))
    (at level 200, x pattern, X at level 100, Y at level 200).
End Notations.
Import Notations.

Definition ok_unit {trace : Set} : Pervasives.result unit trace :=
  return? tt.

Definition ok_none {a trace : Set} : Pervasives.result (option a) trace :=
  return? None.

Definition ok_some {a trace : Set} (x : a) :
  Pervasives.result (option a) trace :=
  return? (Some x).

Definition ok_nil {a trace : Set} : Pervasives.result (list a) trace :=
  return? [].

Definition ok_true {trace : Set} : Pervasives.result bool trace :=
  return? true.

Definition ok_false {trace : Set} : Pervasives.result bool trace :=
  return? false.

Definition return_unit {trace : Set} : Lwt.t (Pervasives.result unit trace) :=
  return=? tt.

Definition return_none {a trace : Set} :
  Lwt.t (Pervasives.result (option a) trace) :=
  return=? None.

Definition return_some {a trace : Set} (x : a) :
  Lwt.t (Pervasives.result (option a) trace) :=
  return=? (Some x).

Definition return_nil {a trace : Set} :
  Lwt.t (Pervasives.result (list a) trace) :=
  return=? [].

Definition return_true {trace : Set} :
  Lwt.t (Pervasives.result bool trace) :=
  return=? true.

Definition return_false {trace : Set} :
  Lwt.t (Pervasives.result bool trace) :=
  return=? false.

Definition error_value {a err : Set} (e : err) :
  Pervasives.result a (trace err) :=
  Pervasives.Error [e].

Parameter trace_of_error : forall {err : Set}, err -> trace err.

Definition fail {a err : Set} (e : err) :
  Lwt.t (Pervasives.result a (trace err)) :=
  return= (error_value e).

Definition op_gtgteq {a b : Set} :
  Lwt.t a -> (a -> Lwt.t b) -> Lwt.t b :=
  Lwt.bind.

Definition op_gtpipeeq {a b : Set} (x : Lwt.t a) (f : a -> b) : Lwt.t b :=
  Lwt.map f x.

Definition op_gtpipequestion {a b trace : Set}
  (x : Pervasives.result a trace) (f : a -> b) : Pervasives.result b trace :=
  let? x := x in
  return? (f x).

Definition op_gtpipeeqquestion {a b trace : Set}
  (x : Lwt.t (Pervasives.result a trace)) (f : a -> b) :
  Lwt.t (Pervasives.result b trace) :=
  let=? x := x in
  return=? (f x).

Definition op_gtgtquestioneq {a b trace : Set}
  (x : Pervasives.result a trace) (f : a -> Lwt.t (Pervasives.result b trace)) :
  Lwt.t (Pervasives.result b trace) :=
  let=? x := return= x in
  f x.

Definition op_gtpipequestioneq {a b trace : Set}
  (x : Pervasives.result a trace) (f : a -> Lwt.t b) :
  Lwt.t (Pervasives.result b trace) :=
  let=? x := return= x in
  let= y := f x in
  return=? y.

Definition record_trace {a err : Set}
  (e : err) (x : Pervasives.result a (trace err)) :
  Pervasives.result a (trace err) :=
  match x with
  | Pervasives.Ok _ => x
  | Pervasives.Error trace => Pervasives.Error (e :: trace)
  end.

Definition trace_value {b err : Set}
  (e : err) (x : Lwt.t (Pervasives.result b (trace err))) :
  Lwt.t (Pervasives.result b (trace err)) :=
  let= x := x in
  match x with
  | Pervasives.Ok _ => return= x
  | Pervasives.Error trace => return= (Pervasives.Error (e :: trace))
  end.

Definition record_trace_eval {a err : Set}
  (mk_err : unit -> Pervasives.result err (trace err))
  (x : Pervasives.result a (trace err)) :
  Pervasives.result a (trace err) :=
  match x with
  | Pervasives.Ok _ => x
  | Pervasives.Error trace =>
    let? e := mk_err tt in
    Pervasives.Error (e :: trace)
  end.

Definition trace_eval {b err : Set}
  (mk_err : unit -> Lwt.t (Pervasives.result err (trace err)))
  (x : Lwt.t (Pervasives.result b (trace err))) :
  Lwt.t (Pervasives.result b (trace err)) :=
  let= x := x in
  match x with
  | Pervasives.Ok _ => return= x
  | Pervasives.Error trace =>
    let=? e := mk_err tt in
    return= (Pervasives.Error (e :: trace))
  end.

Definition error_unless {err : Set}
  (cond : bool) (e : err) : Pervasives.result unit (trace err) :=
  if cond then
    return? tt
  else
    error_value e.

Definition error_when {err : Set}
  (cond : bool) (e : err) : Pervasives.result unit (trace err) :=
  if cond then
    error_value e
  else
    return? tt.

Definition fail_unless {err : Set}
  (cond : bool) (e : err) : Lwt.t (Pervasives.result unit (trace err)) :=
  if cond then
    return=? tt
  else
    fail e.

Definition fail_when {err : Set}
  (cond : bool) (e : err) : Lwt.t (Pervasives.result unit (trace err)) :=
  if cond then
    fail e
  else
    return=? tt.

Definition unless {trace : Set}
  (cond : bool) (f : unit -> Lwt.t (Pervasives.result unit trace)) :
  Lwt.t (Pervasives.result unit trace) :=
  if cond then
    return=? tt
  else
    f tt.

Definition when_ {trace : Set}
  (cond : bool) (f : unit -> Lwt.t (Pervasives.result unit trace)) :
  Lwt.t (Pervasives.result unit trace) :=
  if cond then
    f tt
  else
    return=? tt.

Parameter dont_wait : forall {trace : Set},
  (extensible_type -> unit) -> (trace -> unit) ->
  (unit -> Lwt.t (Pervasives.result unit trace)) -> unit.

Parameter catch : forall {a : Set},
  option (extensible_type -> bool) -> (unit -> a) -> tzresult a.

Parameter catch_f : forall {a : Set},
  option (extensible_type -> bool) -> (unit -> a) ->
  (extensible_type -> _error) -> tzresult a.

Parameter catch_s : forall {a : Set},
  option (extensible_type -> bool) -> (unit -> Lwt.t a) -> Lwt.t (tzresult a).

Parameter join_e : forall {err : Set},
  list (Pervasives.result unit (trace err)) ->
  Pervasives.result unit (trace err).

Parameter all_e : forall {a err : Set},
  list (Pervasives.result a (trace err)) ->
  Pervasives.result (list a) (trace err).

Parameter both_e : forall {a b err : Set},
  Pervasives.result a (trace err) -> Pervasives.result b (trace err) ->
  Pervasives.result (a * b) (trace err).

Parameter shell_tztrace : Set.

Definition shell_tzresult (a : Set) : Set :=
  Pervasives.result a shell_tztrace.
