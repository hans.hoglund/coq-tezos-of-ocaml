Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Lwt.

Definition post_when_success {a : Set} (x : M? a) (P : a -> Prop) : Prop :=
  match x with
  | Pervasives.Ok v => P v
  | Pervasives.Error _ => True
  end.

Lemma post_when_success_bind {a b : Set} (e1 : M? a) (e2 : a -> M? b)
  (P1 : a -> Prop) (H1 : post_when_success e1 P1) (P2 : b -> Prop)
  : (forall (v1 : a), P1 v1 -> post_when_success (e2 v1) P2) ->
    post_when_success (let? v1 := e1 in e2 v1) P2.
  intro H.
  destruct e1; simpl; trivial.
  now apply H.
Qed.

Definition terminates {A : Set} (x : M? A) : Prop :=
  match x with
  | Pervasives.Error _ => False
  | Pervasives.Ok _ => True
  end.

Definition terminates_lwt {A : Set} (x : M=? A) : Prop :=
  match x with
  | Lwt.Return (Pervasives.Error _) => False
  | Lwt.Return (Pervasives.Ok _) => True
  end.

Lemma bind_return_eq {A : Set} (e : M? A) :
  (let? x := e in return? x) =
  e.
  now destruct e.
Qed.

Lemma bind_return_lwt_eq {A : Set} (e : M=? A) :
  (let=? x := e in return=? x) =
  e.
  destruct e as [e].
  now destruct e.
Qed.

Lemma rewrite_bind {A B : Set} (e1 : M? A) (e2 e2' : A -> M? B)
  (H : forall v, e2 v = e2' v) :
  Error_monad.op_gtgtquestion e1 e2 =
  Error_monad.op_gtgtquestion e1 e2'.
  destruct e1; now simpl.
Qed.

Lemma rewrite_bind_lwt {A B : Set} (e1 : M=? A) (e2 e2' : A -> M=? B)
  (H : forall v, e2 v = e2' v) :
  Error_monad.op_gtgteqquestion e1 e2 =
  Error_monad.op_gtgteqquestion e1 e2'.
  destruct e1 as [e1]; destruct e1; now simpl.
Qed.

Lemma elim_record_trace_eval {A : Set} (mk_error : unit -> M? _error)
  (x : M? A) (v : A)
  : x = return? v ->
    record_trace_eval mk_error x = return? v.
  intro H; now rewrite H.
Qed.

Lemma elim_trace_eval {A : Set} (mk_error : unit -> M=? _error)
  (x : M=? A) (v : A)
  : x = return=? v ->
    trace_eval mk_error x = return=? v.
  intro H; now rewrite H.
Qed.
