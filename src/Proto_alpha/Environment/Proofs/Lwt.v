Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Lemma return_bind_eq {a b: Set} (v : a) (e : a -> M= b)
  : (let= x := return= v in e x) = e v.
  reflexivity.
Qed.

Lemma bind_return_eq {a : Set} (e : M= a)
  : (let= x := e in return= x) = e.
  now destruct e.
Qed.
