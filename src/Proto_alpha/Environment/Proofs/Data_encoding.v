Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Option.

Module Valid_on.
  (** We should be more precise by also talking about the JSON encoding, but
      this is enough for now. *)
  Definition t {a : Set} (domain : a -> Prop) (encoding : Data_encoding.t a)
    : Prop :=
    forall x, domain x ->
    match Data_encoding.Binary.to_bytes_opt None encoding x with
    | Some bytes =>
      Data_encoding.Binary.of_bytes_opt encoding bytes = Some x
    | None => False
    end.

  Lemma implies {a : Set} {domain domain' : a -> Prop}
    {encoding : Data_encoding.t a}
    : t domain encoding ->
      (forall x, domain' x -> domain x) ->
      t domain' encoding.
    unfold t; intros H_encoding H_implies; intros.
    now apply H_encoding; apply H_implies.
  Qed.

  (** Since we do not specify the JSON encoding, we require both of the
      arguments of [splitted] to be valid. We believe this is a good enough
      approximation for now. *)
  Axiom splitted : forall {a : Set} {domain} {json binary : Data_encoding.t a},
    t domain json -> t domain binary ->
    t domain (Data_encoding.splitted json binary).

  Axiom option_value : forall {a : Set} {domain : a -> Prop}
    {encoding : Data_encoding.t a},
    t domain encoding ->
    let option_domain x :=
      match x with
      | None => True
      | Some x => domain x
      end in
    t option_domain (Data_encoding.option_value encoding).

  (** We use an excluding list to define the domain of
      [Data_encoding.string_enum] because a string tag can only be used once. *)
  Fixpoint string_enum_domain {a : Set} (enum : list (string * a))
    (excluding_list : list string) (x : a) : Prop :=
    match enum with
    | [] => False
    | (tag, value) :: enum =>
      (~ (List.In tag excluding_list) /\ x = value) \/
      string_enum_domain enum (tag :: excluding_list) x
    end.

  Axiom string_enum : forall {a : Set} (enum : list (string * a)),
    t (string_enum_domain enum []) (Data_encoding.string_enum enum).

  Module Fixed.
    Axiom string_value : forall size,
      let domain s := String.length s = size in
      t domain (Data_encoding.Fixed.string_value size).

    Axiom bytes_value : forall size,
      let domain s := Bytes.length s = size in
      t domain (Data_encoding.Fixed.bytes_value size).
  End Fixed.

  Axiom obj1 : forall {f1 : Set}
    {name1} {domain1} {encoding1 : Data_encoding.t f1},
    t domain1 encoding1 ->
    t domain1 (Data_encoding.obj1 (Data_encoding.req None None name1 encoding1)).

  Axiom tup1 : forall {f1 : Set} {domain1} {encoding1 : Data_encoding.t f1},
    t domain1 encoding1 ->
    t domain1 (Data_encoding.tup1 encoding1).

  Axiom tup2 : forall {f1 f2 : Set} {domain1 domain2}
    {encoding1 : Data_encoding.t f1}
    {encoding2 : Data_encoding.t f2},
    t domain1 encoding1 ->
    t domain2 encoding2 ->
    let domain '(x1, x2) := domain1 x1 /\ domain2 x2 in
    t domain (Data_encoding.tup2 encoding1 encoding2).

  Axiom tup3 : forall {f1 f2 f3 : Set} {domain1 domain2 domain3}
    {encoding1 : Data_encoding.t f1}
    {encoding2 : Data_encoding.t f2}
    {encoding3 : Data_encoding.t f3},
    t domain1 encoding1 ->
    t domain2 encoding2 ->
    t domain3 encoding3 ->
    let domain '(x1, x2, x3) := domain1 x1 /\ domain2 x2 /\ domain3 x3 in
    t domain (Data_encoding.tup3 encoding1 encoding2 encoding3).

  (** We define the domain and validity on cases as an inductive rather than a
      function. This is because a case contains an existential set, and we
      cannot match on existential sets in [Prop] with impredicative sets. *)
  Module Cases.
    Inductive t {u : Set}
      (excluding_titles : list string) (excluding_tags : list int)
      : (u -> Prop) -> list (Data_encoding.case u) -> Prop :=
    | Nil : t _ _ (fun '_ => False) []
    | Cons :
      forall {a : Set} {domain_a : a -> Prop},
      forall (title : string) (description : option string) (tag : int)
        (encoding : Data_encoding.t a) (proj : u -> option a) (inj : a -> u)
        domain cases,
      ~ List.In title excluding_titles ->
      ~ List.In tag excluding_tags ->
      (forall (x : u),
        match proj x with
        | None => True
        | Some y => domain_a y -> inj y = x
        end) ->
      t (title :: excluding_titles) (tag :: excluding_tags) domain cases ->
      Valid_on.t domain_a encoding ->
      let domain x :=
        match proj x with
        | None => False
        | Some y => domain_a y
        end \/
        domain x in
      let case :=
        Data_encoding.case_value
          title description (Data_encoding.Tag tag) encoding proj inj in
      t _ _ domain (case :: cases).
  End Cases.

  Axiom union : forall {u : Set} {domain} {cases : list (case u)},
    Cases.t [] [] domain cases ->
    t domain (Data_encoding.union None cases).

  Axiom conv : forall {a b : Set} {domain_b : b -> Prop}
    {a_to_b : a -> b} {b_to_a} {encoding_b : Data_encoding.t b},
    (forall v_a, b_to_a (a_to_b v_a) = v_a) ->
    t domain_b encoding_b ->
    t
      (fun v_a => domain_b (a_to_b v_a))
      (Data_encoding.conv a_to_b b_to_a None encoding_b).

  Axiom conv_with_guard : forall {a b : Set} {domain_b : b -> Prop}
    {a_to_b : a -> b} {b_to_a} {encoding_b : Data_encoding.t b},
    (forall v_a, b_to_a (a_to_b v_a) = Pervasives.Ok v_a) ->
    t domain_b encoding_b ->
    t
      (fun v_a => domain_b (a_to_b v_a))
      (Data_encoding.conv_with_guard a_to_b b_to_a None encoding_b).
End Valid_on.

Module Valid.
  Definition t {a : Set} (encoding : Data_encoding.t a) : Prop :=
    Valid_on.t (fun _ => True) encoding.

  Axiom null : t Data_encoding.null.
  Axiom empty : t Data_encoding.empty.
  Axiom unit_value : t Data_encoding.unit_value.
  Axiom int8 : t Data_encoding.int8.
  Axiom uint8 : t Data_encoding.uint8.
  Axiom int16 : t Data_encoding.int16.
  Axiom uint16 : t Data_encoding.uint16.
  Axiom int31 : t Data_encoding.int31.
  Axiom int32_value : t Data_encoding.int32_value.
  Axiom int64_value : t Data_encoding.int64_value.
  Axiom n : t Data_encoding.n.
  Axiom z : t Data_encoding.z.
  Axiom bool_value : t Data_encoding.bool_value.
  Axiom string_value : t Data_encoding.string_value.
  Axiom bytes_value : t Data_encoding.bytes_value.
End Valid.

Module Binary.
  (** We suppose that all the encodings that we consider always succeed. We
      should remove this axiom at some point. *)
  Axiom of_bytes_to_bytes_opt :
    forall {a : Set} (encoding : Data_encoding.t a), Valid.t encoding.
End Binary.
