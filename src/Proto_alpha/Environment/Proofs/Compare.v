Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Require Import Lia.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Definition wrap_compare {a : Set} (compare : a -> a -> int) (x y : a) : int :=
    let res := compare x y in
    match res with
    | 0 => 0
    | _ =>
      if res >i 0 then
        1
      else
        -1
    end.

Lemma wrap_lexicographic_compare {a b : Set} {compare_a : a -> a -> int}
  {compare_b : b -> b -> int} {x y : a * b}
  : wrap_compare (Compare.lexicographic_compare compare_a compare_b) x y =
    lexicographic_compare (wrap_compare compare_a) (wrap_compare compare_b) x y.
  unfold wrap_compare, lexicographic_compare.
  destruct x, y.
  now destruct (compare_a _ _); destruct (compare_b _ _).
Qed.

Lemma lexicographic_one {a b : Set}
  {compare_a : a -> a -> _} {compare_b : b -> b -> _}
  {x_a y_a : a} {x_b y_b : b}
  : lexicographic_compare compare_a compare_b (x_a, x_b) (y_a, y_b) = 1 ->
    compare_a x_a y_a = 1 \/ (compare_a x_a y_a = 0 /\ compare_b x_b y_b = 1).
  simpl.
  destruct (compare_a _ _); tauto.
Qed.

(** Definition of the comparison as a pre-order. *)
Module Pre_valid.
  (** We consider two elements to be equivalent if their images using [f] are
      the same. *)
  Record t {a a' : Set} {f : a -> a'} {compare : a -> a -> int} : Prop := {
    congruence_left x1 x2 y :
      f x1 = f x2 -> compare x1 y = compare x2 y;
    domain x y :
      match compare x y with
      | -1 | 0 | 1 => True
      | _ => False
      end;
    zero x y : compare x y = 0 -> f x = f y;
    sym x y : compare x y = - compare y x;
    trans x y z :
      compare x y = 1 ->
      compare y z = 1 ->
      compare x z = 1;
  }.
  Arguments t {_ _}.

  Lemma refl {a a' : Set} {f : a -> a'} {compare : a -> a -> int}
    (H : t f compare) (x : a)
    : compare x x = 0.
    set (c := compare x x).
    assert (c = -c) by apply H.(sym).
    lia.
  Qed.

  Lemma congruence_right {a a' : Set} {f : a -> a'} {compare : a -> a -> int}
    {x y1 y2 : a}
    : t f compare -> f y1 = f y2 -> compare x y1 = compare x y2.
    intros H_t H_eq.
    rewrite H_t.(sym).
    rewrite (H_t.(congruence_left) _ _ _ H_eq).
    now rewrite (H_t.(sym) x).
  Qed.

  Lemma option {a a' : Set} {f : a -> a'} {compare : a -> a -> int}
    : t f compare ->
      t (Option.map f) (Compare.Option.compare compare).
    intro H.
    constructor; intros; destruct_all (option a); simpl in *; try easy;
      try apply H; try congruence;
      try (f_equal; now apply H.(zero)).
    match goal with
    | [v : a |- _] => now apply H with (y := v)
    end.
  Qed.

  Lemma lexicographic_compare {a b a' b' : Set}
    {f_a : a -> a'} {f_b : b -> b'}
    {compare_a : a -> a -> int} {compare_b : b -> b -> int}
    : t f_a compare_a -> t f_b compare_b ->
      let f '(x_a, x_b) := (f_a x_a, f_b x_b) in
      t f (lexicographic_compare compare_a compare_b).
    intros H_a H_b.
    constructor; unfold lexicographic_compare; intros; destruct_all (a * b).
    erewrite H_a.(congruence_left);
      try erewrite H_b.(congruence_left);
      try reflexivity;
      try congruence.
    match goal with
    | [|- context[compare_a ?v1 ?v2]] =>
      let H := fresh "H" in
      assert (H := H_a.(domain) v1 v2);
      destruct (compare_a v1 v2); trivial;
      apply H_b
    end.
    destruct (compare_a _ _) eqn:H_ca; try congruence.
    f_equal.
    now erewrite H_a.(zero); try reflexivity.
    now erewrite H_b.(zero); try reflexivity.
    rewrite H_a.(sym); rewrite H_b.(sym).
    now destruct (compare_a _ _).

    repeat match goal with
    | [H : match _ with _ => _ end = 1 |- _] =>
      let H_zero := fresh "H_zero" in
      let H_one := fresh "H_one" in
      destruct (lexicographic_one H) as [H_one|[H_zero H_one]];
      clear H
    end;
    repeat match goal with
    | [H : compare_a ?v1 ?v2 = 0 |- _] =>
      let H_eq := fresh "H_eq" in
      assert (H_eq := H_a.(zero) _ _ H);
      clear H;
      try (rewrite (H_a.(congruence_left) _ _ _ H_eq); clear H_eq);
      try (rewrite <- (congruence_right H_a H_eq); clear H_eq)
    end.
    match goal with
    | [v : a |- _] => now rewrite H_a.(trans) with (y := v)
    end.
    match goal with
    | [|- context[compare_a ?v1 ?v2]] =>
      now replace (compare_a v1 v2) with 1
    end.
    match goal with
    | [|- context[compare_a ?v1 ?v2]] =>
      now replace (compare_a v1 v2) with 1
    end.
    match goal with
    | [v3 : b |- context[compare_b ?v1 ?v2]] =>
      rewrite (refl H_a);
      now apply H_b.(trans) with (y := v3)
    end.
  Qed.

  Lemma equality {a a' : Set} {f : a -> a'} {compare1 compare2 : a -> a -> int}
    : (forall x y, compare1 x y = compare2 x y) -> t f compare1 -> t f compare2.
    intros H_eq H_compare1.
    constructor; intros; repeat rewrite <- H_eq in *; try now apply H_compare1.
    now apply H_compare1 with (y := y).
  Qed.

  Lemma equality_f {a a' : Set} {f1 f2 : a -> a'} {compare : a -> a -> int}
    : (forall x, f1 x = f2 x) -> t f1 compare -> t f2 compare.
    intros H_eq H_compare1.
    constructor; intros; repeat rewrite <- H_eq in *; try now apply H_compare1.
    now apply H_compare1 with (y := y).
  Qed.
End Pre_valid.

(** Definition of the comparison as an order. *)
Module Valid.
  (** We define an order as a pre-order for the identity function. *)
  Definition t {a : Set} (compare : a -> a -> int) : Prop :=
    Pre_valid.t id compare.

  Lemma unit : t (wrap_compare Compare.Unit.(Compare.S.compare)).
    constructor; intros; try easy.
    now destruct x, y.
  Qed.

  Lemma bool : t (wrap_compare Compare.Bool.(Compare.S.compare)).
    constructor; intros;
      now repeat match goal with
      | [b : bool |- _] => destruct b
      end.
  Qed.

  Lemma int : t (wrap_compare Compare.Int.(Compare.S.compare)).
    constructor; unfold wrap_compare, id;
      repeat match goal with
      | [|- forall (_ : int), _] => intro
      end;
      repeat match goal with
      | [|- context[Compare.Int.(Compare.S.compare) ?x ?y]] =>
        let H := fresh "H" in
        destruct (Compare.Int.(Compare.S.compare) x y) eqn:H
      end;
      simpl in *; unfold Z.compare, "-Z" in *;
      lia.
  Qed.

  Axiom int64 : t (wrap_compare Compare.Int64.(Compare.S.compare)).

  Axiom string : t (wrap_compare Compare.String.(Compare.S.compare)).

  Lemma option {a : Set} {compare : a -> a -> _}
    : t compare ->
      t (Compare.Option.compare compare).
    intro H.
    eapply Pre_valid.equality_f;
      [| eapply Pre_valid.option; now apply H].
    intro; now destruct x.
  Qed.

  Lemma wrap_compare_eq {a : Set} {compare : a -> a -> _}
    : t compare -> forall x y, (wrap_compare compare) x y = compare x y.
    unfold t, id, wrap_compare.
    intros H x y.
    assert (H_domain := H.(Pre_valid.domain) x y).
    destruct (compare x y);
      now try match goal with
      | [p : positive |- _] => destruct p
      end.
  Qed.

  Lemma wrap_compare {a : Set} {compare : a -> a -> _}
    : t compare -> t (wrap_compare compare).
    intros; refine (Pre_valid.equality _ H).
    intros; now rewrite <- wrap_compare_eq.
  Qed.

  Lemma lexicographic_compare {a b : Set}
    {compare_a : a -> a -> _} {compare_b : b -> b -> _}
    : t compare_a -> t compare_b ->
      t (lexicographic_compare compare_a compare_b).
    intros H_a H_b.
    eapply Pre_valid.equality_f;
      [| eapply Pre_valid.lexicographic_compare].
    intro x; now destruct x.
    all: trivial.
  Qed.

  Lemma equality {a : Set} {compare1 compare2 : a -> a -> _}
    : (forall x y, compare1 x y = compare2 x y) -> t compare1 -> t compare2.
    intros H_eq H_compare1.
    constructor; intros; repeat rewrite <- H_eq in *; try now apply H_compare1.
    now apply H_compare1 with (y := y).
  Qed.
End Valid.
