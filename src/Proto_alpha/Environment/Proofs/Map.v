Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Import Map.

Axiom find_add : forall `{Make.FArgs} {a : Set} k v (m : Make.t a),
  Make.find k (Make.add k v m) = Some v.

Axiom find_update : forall `{Make.FArgs} {a : Set} k f (m : Make.t a),
  Make.find k (Make.update k f m) = f (Make.find k m).

Axiom find_singleton : forall `{Make.FArgs} {a : Set} k (v : a),
  Make.find k (Make.singleton k v) = Some v.
