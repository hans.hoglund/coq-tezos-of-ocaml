Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Map.

Module Tree.
  Module Raw.
    Lemma mem_find (tree : Tree.t) (path : key)
      : Tree.Raw.mem tree path =
        let value := Tree.Raw.find tree path in
        match value with
        | Some _ => true
        | None => false
        end.
      unfold Tree.Raw.mem, Tree.Raw.find.
      destruct (Tree.Aux.get _ _) as [tree'|]; simpl; trivial.
      now destruct tree'.
    Qed.
  End Raw.
End Tree.

Module Raw.
  Lemma mem_find (view : View.t) (path : key)
    : Raw.mem view path =
      let value := Raw.find view path in
      match value with
      | Some _ => true
      | None => false
      end.
    unfold Raw.mem, Raw.find.
    now rewrite Tree.Raw.mem_find.
    Qed.
End Raw.

Module Key.
  Parameter compare : key -> key -> int.
End Key.
