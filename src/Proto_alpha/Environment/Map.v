Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_alpha.Environment.Compare.
Require Proto_alpha.Environment.Error_monad.
Require Proto_alpha.Environment.List.
Import Error_monad.Notations.

Module S.
  Record signature {key : Set} {t : Set -> Set} : Set := {
    key := key;
    t := t;
    empty : forall {a : Set}, t a;
    is_empty : forall {a : Set}, t a -> bool;
    mem : forall {a : Set}, key -> t a -> bool;
    add : forall {a : Set}, key -> a -> t a -> t a;
    update : forall {a : Set}, key -> (option a -> option a) -> t a -> t a;
    singleton : forall {a : Set}, key -> a -> t a;
    remove : forall {a : Set}, key -> t a -> t a;
    merge :
      forall {a b c : Set},
      (key -> option a -> option b -> option c) -> t a -> t b -> t c;
    union :
      forall {a : Set}, (key -> a -> a -> option a) -> t a -> t a -> t a;
    compare : forall {a : Set}, (a -> a -> int) -> t a -> t a -> int;
    equal : forall {a : Set}, (a -> a -> bool) -> t a -> t a -> bool;
    iter : forall {a : Set}, (key -> a -> unit) -> t a -> unit;
    iter_e :
      forall {a trace : Set},
      (key -> a -> Pervasives.result unit trace) -> t a ->
      Pervasives.result unit trace;
    iter_s : forall {a : Set}, (key -> a -> Lwt.t unit) -> t a -> Lwt.t unit;
    iter_p : forall {a : Set}, (key -> a -> Lwt.t unit) -> t a -> Lwt.t unit;
    iter_es :
      forall {a trace : Set},
      (key -> a -> Lwt.t (Pervasives.result unit trace)) -> t a ->
      Lwt.t (Pervasives.result unit trace);
    fold : forall {a b : Set}, (key -> a -> b -> b) -> t a -> b -> b;
    fold_e :
      forall {a b trace : Set},
      (key -> a -> b -> Pervasives.result b trace) -> t a -> b ->
      Pervasives.result b trace;
    fold_s :
      forall {a b : Set}, (key -> a -> b -> Lwt.t b) -> t a -> b -> Lwt.t b;
    fold_es :
      forall {a b trace : Set},
      (key -> a -> b -> Lwt.t (Pervasives.result b trace)) -> t a -> b ->
      Lwt.t (Pervasives.result b trace);
    for_all : forall {a : Set}, (key -> a -> bool) -> t a -> bool;
    _exists : forall {a : Set}, (key -> a -> bool) -> t a -> bool;
    filter : forall {a : Set}, (key -> a -> bool) -> t a -> t a;
    partition : forall {a : Set}, (key -> a -> bool) -> t a -> t a * t a;
    cardinal : forall {a : Set}, t a -> int;
    bindings : forall {a : Set}, t a -> list (key * a);
    min_binding : forall {a : Set}, t a -> option (key * a);
    max_binding : forall {a : Set}, t a -> option (key * a);
    choose : forall {a : Set}, t a -> option (key * a);
    split : forall {a : Set}, key -> t a -> t a * option a * t a;
    find : forall {a : Set}, key -> t a -> option a;
    find_first : forall {a : Set}, (key -> bool) -> t a -> option (key * a);
    find_last : forall {a : Set}, (key -> bool) -> t a -> option (key * a);
    map : forall {a b : Set}, (a -> b) -> t a -> t b;
    mapi : forall {a b : Set}, (key -> a -> b) -> t a -> t b;
    to_seq : forall {a : Set}, t a -> Seq.t (key * a);
    to_seq_from : forall {a : Set}, key -> t a -> Seq.t (key * a);
    add_seq : forall {a : Set}, Seq.t (key * a) -> t a -> t a;
    of_seq : forall {a : Set}, Seq.t (key * a) -> t a;
    iter_ep :
      forall {a : Set},
      (key -> a -> Lwt.t (M? unit)) -> t a -> Lwt.t (M? unit);
  }.
End S.
Definition S := @S.signature.
Arguments S {_ _}.

(** We define a simple version of maps, using sorted lists. *)
Module Make.
  Class FArgs {t : Set} := {
    Ord : Compare.COMPARABLE (t := t);
  }.
  Arguments Build_FArgs {_}.

  Definition key `{FArgs} : Set := Ord.(Compare.COMPARABLE.t).

  Definition t `{FArgs} (a : Set) : Set := list (key * a).

  Definition compare_keys `{FArgs} (k1 k2 : key) : comparison :=
    let z := Ord.(Compare.COMPARABLE.compare) k1 k2 in
    if Z.eqb z 0 then
      Eq
    else if Z.leb z 0 then
      Lt
    else
      Gt.

  Definition empty `{FArgs} {a : Set} : t a :=
    [].

  Definition is_empty `{FArgs} {a : Set} (m : t a) : bool :=
    match m with
    | [] => true
    | _ :: _ => false
    end.

  Fixpoint mem `{FArgs} {a : Set} (k : key) (m : t a) : bool :=
    match m with
    | [] => false
    | (k', _) :: m =>
      match compare_keys k k' with
      | Lt => false
      | Eq => true
      | Gt => mem k m
      end
    end.

  Fixpoint find `{FArgs} {a : Set} (k : key) (m : t a) : option a :=
    match m with
    | [] => None
    | (k', v') :: m' =>
      match compare_keys k k' with
      | Lt => None
      | Eq => Some v'
      | Gt => find k m'
      end
    end.

  Fixpoint add `{FArgs} {a : Set} (k : key) (v : a) (m : t a) : t a :=
    match m with
    | [] => [(k, v)]
    | (k', v') :: m' =>
      match compare_keys k k' with
      | Lt => (k', v') :: add k v m'
      | Eq => (k, v) :: m'
      | Gt => (k, v) :: m
      end
    end.

  Fixpoint remove `{FArgs} {a : Set} (k : key) (m : t a) : t a :=
    match m with
    | [] => m
    | (k', v') :: m' =>
      match compare_keys k k' with
      | Lt => m
      | Eq => m'
      | Gt => (k', v') :: remove k m'
      end
    end.

  Definition update `{FArgs} {a : Set} (k : key) (f : option a -> option a)
    (m : t a) : t a :=
    match f (find k m) with
    | None => remove k m
    | Some v' => add k v' m
    end.

  Definition singleton `{FArgs} {a : Set} (k : key) (v : a) : t a :=
    [(k, v)].

  Fixpoint pick_opt `{FArgs} {a : Set} (k : key) (m : t a) : option a * t a :=
    match m with
    | [] => (None, m)
    | (k', v') :: m' =>
      match compare_keys k k' with
      | Lt => (None, m)
      | Eq => (Some v', m')
      | Gt =>
        let '(v, m') := pick_opt k m' in
        (v, (k', v') :: m')
      end
    end.

  Fixpoint merge `{FArgs} {a b c : Set}
    (f : key -> option a -> option b -> option c) (m1 : t a) (m2 : t b) : t c :=
    match m1 with
    | [] =>
      List.filter_map
        (fun '(k2, v2) =>
          match f k2 None (Some v2) with
          | None => None
          | Some v => Some (k2, v)
          end
        )
        m2
    | (k1, v1) :: m1 =>
      let '(v2, m2) := pick_opt k1 m2 in
      let m := merge f m1 m2 in
      match f k1 (Some v1) v2 with
      | None => m
      | Some v => add k1 v m
      end
    end.

  Definition union `{FArgs} {a : Set} (f : key -> a -> a -> option a)
    (m1 m2 : t a) : t a :=
    merge
      (fun k v1 v2 =>
        match v1, v2 with
        | None, None => None
        | Some v1, None => Some v1
        | None, Some v2 => Some v2
        | Some v1, Some v2 => f k v1 v2
        end
      )
      m1 m2.

  Fixpoint compare `{FArgs} {a : Set} (f : a -> a -> int) (m1 m2 : t a) : int :=
    match m1, m2 with
    | [], [] => 0
    | _ :: _, [] => 1
    | [], _ :: _ => -1
    | (k1, v1) :: m1, (k2, v2) :: m2 =>
      match compare_keys k1 k2 with
      | Lt => -1
      | Eq =>
        let compare_values := f v1 v2 in
        if Z.eqb compare_values 0 then
          compare f m1 m2
        else
          compare_values
      | Gt => 1
      end
    end.

  Fixpoint equal `{FArgs} {a : Set} (f : a -> a -> bool) (m1 m2 : t a) : bool :=
    match m1, m2 with
    | [], [] => true
    | _ :: _, [] | [], _ :: _ => false
    | (k1, v1) :: m1, (k2, v2) :: m2 =>
      match compare_keys k1 k2 with
      | Eq =>
        if f v1 v2 then
          equal f m1 m2
        else
          false
      | _ => false
      end
    end.

  Fixpoint iter `{FArgs} {a : Set} (f : key -> a -> unit) (m : t a) : unit :=
    match m with
    | [] => tt
    | (k, v) :: m =>
      let _ : unit := f k v in
      iter f m
    end.

  Parameter iter_e :
    forall `{FArgs} {a trace : Set},
    (key -> a -> Pervasives.result unit trace) -> t a ->
    Pervasives.result unit trace.

  Parameter iter_s : forall `{FArgs} {a : Set},
    (key -> a -> Lwt.t unit) -> t a -> Lwt.t unit.

  Parameter iter_p : forall `{FArgs} {a : Set},
    (key -> a -> Lwt.t unit) -> t a -> Lwt.t unit.

  Parameter iter_es : forall `{FArgs} {a trace : Set},
    (key -> a -> Lwt.t (Pervasives.result unit trace)) -> t a ->
    Lwt.t (Pervasives.result unit trace).

  Fixpoint fold `{FArgs} {a b : Set} (f : key -> a -> b -> b) (m : t a)
    (acc : b) : b :=
    match m with
    | [] => acc
    | (k, v) :: m =>
      let acc := f k v acc in
      fold f m acc
    end.

  Parameter fold_e :
    forall `{FArgs} {a b trace : Set},
    (key -> a -> b -> Pervasives.result b trace) -> t a -> b ->
    Pervasives.result b trace.

  Parameter fold_s :
    forall `{FArgs} {a b : Set},
    (key -> a -> b -> Lwt.t b) -> t a -> b -> Lwt.t b.

  Parameter fold_es :
    forall `{FArgs} {a b trace : Set},
    (key -> a -> b -> Lwt.t (Pervasives.result b trace)) ->
    t a -> b ->
    Lwt.t (Pervasives.result b trace).

  Fixpoint for_all `{FArgs} {a : Set} (p : key -> a -> bool) (m : t a) : bool :=
    match m with
    | [] => true
    | (k, v) :: m => p k v && for_all p m
    end.

  Fixpoint _exists `{FArgs} {a : Set} (p : key -> a -> bool) (m : t a) : bool :=
    match m with
    | [] => false
    | (k, v) :: m => p k v || _exists p m
    end.

  Fixpoint filter `{FArgs} {a : Set} (p : key -> a -> bool) (m : t a) : t a :=
    match m with
    | [] => m
    | (k, v) :: m =>
      let m := filter p m in
      if p k v then
        (k, v) :: m
      else
        m
    end.

  Fixpoint partition `{FArgs} {a : Set} (p : key -> a -> bool) (m : t a)
    : t a * t a :=
    match m with
    | [] => ([], [])
    | (k, v) :: m =>
      let '(m_true, m_false) := partition p m in
      if p k v then
        ((k, v) :: m_true, m_false)
      else
        (m_true, (k, v) :: m_false)
    end.

  Definition cardinal `{FArgs} {a : Set} (m : t a) : int :=
    List.length m.

  Definition bindings `{FArgs} {a : Set} (m : t a) : list (key * a) :=
    m.

  Definition min_binding `{FArgs} {a : Set} (m : t a) : option (key * a) :=
    match m with
    | [] => None
    | binding :: _ => Some binding
    end.

  Fixpoint max_binding `{FArgs} {a : Set} (m : t a) : option (key * a) :=
    match m with
    | [] => None
    | [binding] => Some binding
    | _ :: (_ :: _) as m => max_binding m
    end.

  Definition choose : forall `{FArgs} {a : Set}, t a -> option (key * a) :=
    @min_binding.

  Fixpoint split `{FArgs} {a : Set} (k : key) (m : t a)
    : t a * option a * t a :=
    match m with
    | [] => ([], None, [])
    | (k', v') :: m' =>
      match compare_keys k k' with
      | Lt => ([], None, m)
      | Eq => ([], Some v', m')
      | Gt =>
        let '(m_lt, v, m_gt) := split k m' in
        ((k', v') :: m_lt, v, m_gt)
      end
    end.

  Fixpoint find_first `{FArgs} {a : Set} (p : key -> bool) (m : t a)
    : option (key * a) :=
    match m with
    | [] => None
    | (k, v) :: m =>
      if p k then
        Some (k, v)
      else
        find_first p m
    end.

  Fixpoint find_last `{FArgs} {a : Set} (p : key -> bool) (m : t a)
    : option (key * a) :=
    match m with
    | [] => None
    | (k, v) :: m =>
      match find_last p m with
      | None =>
        if p k then
          Some (k, v)
        else
          None
      | Some _ as result => result
      end
    end.

  Fixpoint map `{FArgs} {a b : Set} (f : a -> b) (m : t a) : t b :=
    match m with
    | [] => []
    | (k, v) :: m => (k, f v) :: map f m
    end.

  Fixpoint mapi `{FArgs} {a b : Set} (f : key -> a -> b) (m : t a) : t b :=
    match m with
    | [] => []
    | (k, v) :: m => (k, f k v) :: mapi f m
    end.

  Parameter to_seq : forall `{FArgs} {a : Set},
    t a -> Seq.t (key * a).

  Parameter to_seq_from : forall `{FArgs} {a : Set},
    key -> t a -> Seq.t (key * a).

  Parameter add_seq : forall `{FArgs} {a : Set},
    Seq.t (key * a) -> t a -> t a.

  Parameter of_seq : forall `{FArgs} {a : Set},
    Seq.t (key * a) -> t a.

  Parameter iter_ep :
    forall `{FArgs} {a : Set},
    (key -> a -> Lwt.t (M? unit)) -> t a -> Lwt.t (M? unit).

  Definition functor `(FArgs) :=
    {|
      S.empty _ := empty;
      S.is_empty _ := is_empty;
      S.mem _ := mem;
      S.add _ := add;
      S.update _ := update;
      S.singleton _ := singleton;
      S.remove _ := remove;
      S.merge _ _ _:= merge;
      S.union _ := union;
      S.compare _ := compare;
      S.equal _ := equal;
      S.iter _ := iter;
      S.iter_e _ _ := iter_e;
      S.iter_s _ := iter_s;
      S.iter_p _ := iter_p;
      S.iter_es _ _ := iter_es;
      S.fold _ _ := fold;
      S.fold_e _ _ _ := fold_e;
      S.fold_s _ _ := fold_s;
      S.fold_es _ _ _ := fold_es;
      S.for_all _ := for_all;
      S._exists _ := _exists;
      S.filter _ := filter;
      S.partition _ := partition;
      S.cardinal _ := cardinal;
      S.bindings _ := bindings;
      S.min_binding _ := min_binding;
      S.max_binding _ := max_binding;
      S.choose _ := choose;
      S.split _ := split;
      S.find _ := find;
      S.find_first _ := find_first;
      S.find_last _ := find_last;
      S.map _ _ := map;
      S.mapi _ _ := mapi;
      S.to_seq _ := to_seq;
      S.to_seq_from _ := to_seq_from;
      S.add_seq _ := add_seq;
      S.of_seq _ := of_seq;
      S.iter_ep _ := iter_ep;
    |}.
End Make.

Definition Make_t {Ord_t : Set}
  (Ord : Compare.COMPARABLE (t := Ord_t)) :=
  let '_ := Make.Build_FArgs Ord in
  Make.t.

Definition Make {Ord_t : Set}
  (Ord : Compare.COMPARABLE (t := Ord_t))
  : S (key := Ord.(Compare.COMPARABLE.t)) (t := Make_t Ord) :=
  Make.functor (Make.Build_FArgs Ord).
