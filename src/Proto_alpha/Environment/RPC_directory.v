Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_alpha.Environment.Error_monad.
Require Proto_alpha.Environment.Lwt.
Require Proto_alpha.Environment.RPC_answer.
Require Proto_alpha.Environment.RPC_path.
Require Proto_alpha.Environment.RPC_service.

Parameter t : forall (prefix : Set), Set.

Definition directory (prefix : Set) : Set := t prefix.

Parameter empty : forall {prefix : Set}, directory prefix.

Parameter map : forall {a b : Set},
  (a -> Lwt.t b) -> directory b -> directory a.

Parameter prefix : forall {p pr : Set},
  RPC_path.path pr p -> directory p -> directory pr.

Parameter merge : forall {a : Set}, directory a -> directory a -> directory a.

Inductive step : Set :=
| Static : string -> step
| Dynamic : RPC_arg.descr -> step
| DynamicTail : RPC_arg.descr -> step.

Inductive conflict : Set :=
| CService : RPC_service.meth -> conflict
| CDir : conflict
| CBuilder : conflict
| CTail : conflict
| CTypes : RPC_arg.descr -> RPC_arg.descr -> conflict
| CType : RPC_arg.descr -> list string -> conflict.

Parameter register : forall {input output params prefix query : Set},
  bool -> directory prefix ->
  RPC_service.t prefix params query input output ->
  (params -> query -> input -> Lwt.t (Error_monad.tzresult output)) ->
  directory prefix.

Parameter opt_register : forall {input output params prefix query : Set},
  bool -> directory prefix ->
  RPC_service.t prefix params query input output ->
  (params -> query -> input -> Lwt.t (Error_monad.tzresult (option output)))
  -> directory prefix.

Parameter gen_register : forall {input output params prefix query : Set},
  directory prefix -> RPC_service.t prefix params query input output ->
  (params -> query -> input ->
  Lwt.t
    ((* `OkStream *) RPC_answer.stream output +
      (* `Unauthorized *) option (list Error_monad._error) +
      (* `OkChunk *) output + (* `Error *) option (list Error_monad._error) +
      (* `Ok *) output + (* `Not_found *) option (list Error_monad._error) +
      (* `Forbidden *) option (list Error_monad._error) +
      (* `Created *) option string +
      (* `Conflict *) option (list Error_monad._error) +
      (* `No_content *) unit)) -> directory prefix.

Parameter lwt_register : forall {input output params prefix query : Set},
  bool -> directory prefix ->
  RPC_service.t prefix params query input output ->
  (params -> query -> input -> Lwt.t output) -> directory prefix.

Parameter register0 : forall {i o q : Set},
  bool -> directory unit -> RPC_service.t unit unit q i o ->
  (q -> i -> Lwt.t (Error_monad.tzresult o)) -> directory unit.

Parameter register1 : forall {a i o prefix q : Set},
  bool -> directory prefix -> RPC_service.t prefix (unit * a) q i o ->
  (a -> q -> i -> Lwt.t (Error_monad.tzresult o)) -> directory prefix.

Parameter register2 : forall {a b i o prefix q : Set},
  bool -> directory prefix -> RPC_service.t prefix ((unit * a) * b) q i o ->
  (a -> b -> q -> i -> Lwt.t (Error_monad.tzresult o)) -> directory prefix.

Parameter register3 : forall {a b c i o prefix q : Set},
  bool -> directory prefix ->
  RPC_service.t prefix (((unit * a) * b) * c) q i o ->
  (a -> b -> c -> q -> i -> Lwt.t (Error_monad.tzresult o)) ->
  directory prefix.

Parameter register4 : forall {a b c d i o prefix q : Set},
  bool -> directory prefix ->
  RPC_service.t prefix ((((unit * a) * b) * c) * d) q i o ->
  (a -> b -> c -> d -> q -> i -> Lwt.t (Error_monad.tzresult o)) ->
  directory prefix.

Parameter register5 : forall {a b c d e i o prefix q : Set},
  bool -> directory prefix ->
  RPC_service.t prefix (((((unit * a) * b) * c) * d) * e) q i o ->
  (a -> b -> c -> d -> e -> q -> i -> Lwt.t (Error_monad.tzresult o)) ->
  directory prefix.

Parameter opt_register0 : forall {i o q : Set},
  bool -> directory unit -> RPC_service.t unit unit q i o ->
  (q -> i -> Lwt.t (Error_monad.tzresult (option o))) -> directory unit.

Parameter opt_register1 : forall {a i o prefix q : Set},
  bool -> directory prefix -> RPC_service.t prefix (unit * a) q i o ->
  (a -> q -> i -> Lwt.t (Error_monad.tzresult (option o))) -> directory prefix.

Parameter opt_register2 : forall {a b i o prefix q : Set},
  bool -> directory prefix -> RPC_service.t prefix ((unit * a) * b) q i o ->
  (a -> b -> q -> i -> Lwt.t (Error_monad.tzresult (option o))) ->
  directory prefix.

Parameter opt_register3 : forall {a b c i o prefix q : Set},
  bool -> directory prefix ->
  RPC_service.t prefix (((unit * a) * b) * c) q i o ->
  (a -> b -> c -> q -> i -> Lwt.t (Error_monad.tzresult (option o))) ->
  directory prefix.

Parameter opt_register4 : forall {a b c d i o prefix q : Set},
  bool -> directory prefix ->
  RPC_service.t prefix ((((unit * a) * b) * c) * d) q i o ->
  (a -> b -> c -> d -> q -> i -> Lwt.t (Error_monad.tzresult (option o))) ->
  directory prefix.

Parameter opt_register5 : forall {a b c d e i o prefix q : Set},
  bool -> directory prefix ->
  RPC_service.t prefix (((((unit * a) * b) * c) * d) * e) q i o ->
  (a -> b -> c -> d -> e -> q -> i -> Lwt.t (Error_monad.tzresult (option o)))
  -> directory prefix.

Parameter gen_register0 : forall {i o q : Set},
  directory unit -> RPC_service.t unit unit q i o ->
  (q -> i ->
  Lwt.t
    ((* `OkStream *) RPC_answer.stream o +
      (* `Unauthorized *) option (list Error_monad._error) + (* `OkChunk *) o
      + (* `Error *) option (list Error_monad._error) + (* `Ok *) o +
      (* `Not_found *) option (list Error_monad._error) +
      (* `Forbidden *) option (list Error_monad._error) +
      (* `Created *) option string +
      (* `Conflict *) option (list Error_monad._error) +
      (* `No_content *) unit)) -> directory unit.

Parameter gen_register1 : forall {a i o prefix q : Set},
  directory prefix -> RPC_service.t prefix (unit * a) q i o ->
  (a -> q -> i ->
  Lwt.t
    ((* `OkStream *) RPC_answer.stream o +
      (* `Unauthorized *) option (list Error_monad._error) + (* `OkChunk *) o
      + (* `Error *) option (list Error_monad._error) + (* `Ok *) o +
      (* `Not_found *) option (list Error_monad._error) +
      (* `Forbidden *) option (list Error_monad._error) +
      (* `Created *) option string +
      (* `Conflict *) option (list Error_monad._error) +
      (* `No_content *) unit)) -> directory prefix.

Parameter gen_register2 : forall {a b i o prefix q : Set},
  directory prefix -> RPC_service.t prefix ((unit * a) * b) q i o ->
  (a -> b -> q -> i ->
  Lwt.t
    ((* `OkStream *) RPC_answer.stream o +
      (* `Unauthorized *) option (list Error_monad._error) + (* `OkChunk *) o
      + (* `Error *) option (list Error_monad._error) + (* `Ok *) o +
      (* `Not_found *) option (list Error_monad._error) +
      (* `Forbidden *) option (list Error_monad._error) +
      (* `Created *) option string +
      (* `Conflict *) option (list Error_monad._error) +
      (* `No_content *) unit)) -> directory prefix.

Parameter gen_register3 : forall {a b c i o prefix q : Set},
  directory prefix -> RPC_service.t prefix (((unit * a) * b) * c) q i o ->
  (a -> b -> c -> q -> i ->
  Lwt.t
    ((* `OkStream *) RPC_answer.stream o +
      (* `Unauthorized *) option (list Error_monad._error) + (* `OkChunk *) o
      + (* `Error *) option (list Error_monad._error) + (* `Ok *) o +
      (* `Not_found *) option (list Error_monad._error) +
      (* `Forbidden *) option (list Error_monad._error) +
      (* `Created *) option string +
      (* `Conflict *) option (list Error_monad._error) +
      (* `No_content *) unit)) -> directory prefix.

Parameter gen_register4 : forall {a b c d i o prefix q : Set},
  directory prefix ->
  RPC_service.t prefix ((((unit * a) * b) * c) * d) q i o ->
  (a -> b -> c -> d -> q -> i ->
  Lwt.t
    ((* `OkStream *) RPC_answer.stream o +
      (* `Unauthorized *) option (list Error_monad._error) + (* `OkChunk *) o
      + (* `Error *) option (list Error_monad._error) + (* `Ok *) o +
      (* `Not_found *) option (list Error_monad._error) +
      (* `Forbidden *) option (list Error_monad._error) +
      (* `Created *) option string +
      (* `Conflict *) option (list Error_monad._error) +
      (* `No_content *) unit)) -> directory prefix.

Parameter gen_register5 : forall {a b c d e i o prefix q : Set},
  directory prefix ->
  RPC_service.t prefix (((((unit * a) * b) * c) * d) * e) q i o ->
  (a -> b -> c -> d -> e -> q -> i ->
  Lwt.t
    ((* `OkStream *) RPC_answer.stream o +
      (* `Unauthorized *) option (list Error_monad._error) + (* `OkChunk *) o
      + (* `Error *) option (list Error_monad._error) + (* `Ok *) o +
      (* `Not_found *) option (list Error_monad._error) +
      (* `Forbidden *) option (list Error_monad._error) +
      (* `Created *) option string +
      (* `Conflict *) option (list Error_monad._error) +
      (* `No_content *) unit)) -> directory prefix.

Parameter lwt_register0 : forall {i o q : Set},
  bool -> directory unit -> RPC_service.t unit unit q i o ->
  (q -> i -> Lwt.t o) -> directory unit.

Parameter lwt_register1 : forall {a i o prefix q : Set},
  bool -> directory prefix -> RPC_service.t prefix (unit * a) q i o ->
  (a -> q -> i -> Lwt.t o) -> directory prefix.

Parameter lwt_register2 : forall {a b i o prefix q : Set},
  bool -> directory prefix -> RPC_service.t prefix ((unit * a) * b) q i o ->
  (a -> b -> q -> i -> Lwt.t o) -> directory prefix.

Parameter lwt_register3 : forall {a b c i o prefix q : Set},
  bool -> directory prefix ->
  RPC_service.t prefix (((unit * a) * b) * c) q i o ->
  (a -> b -> c -> q -> i -> Lwt.t o) -> directory prefix.

Parameter lwt_register4 : forall {a b c d i o prefix q : Set},
  bool -> directory prefix ->
  RPC_service.t prefix ((((unit * a) * b) * c) * d) q i o ->
  (a -> b -> c -> d -> q -> i -> Lwt.t o) -> directory prefix.

Parameter lwt_register5 : forall {a b c d e i o prefix q : Set},
  bool -> directory prefix ->
  RPC_service.t prefix (((((unit * a) * b) * c) * d) * e) q i o ->
  (a -> b -> c -> d -> e -> q -> i -> Lwt.t o) -> directory prefix.

Parameter register_dynamic_directory : forall {a prefix : Set},
  option string -> directory prefix -> RPC_path.t prefix a ->
  (a -> Lwt.t (directory a)) -> directory prefix.
