Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Script_typed_ir.

Definition default_now_annot : option Script_typed_ir.var_annot :=
  Some (Script_typed_ir.Var_annot "now").

Definition default_amount_annot : option Script_typed_ir.var_annot :=
  Some (Script_typed_ir.Var_annot "amount").

Definition default_balance_annot : option Script_typed_ir.var_annot :=
  Some (Script_typed_ir.Var_annot "balance").

Definition default_level_annot : option Script_typed_ir.var_annot :=
  Some (Script_typed_ir.Var_annot "level").

Definition default_steps_annot : option Script_typed_ir.var_annot :=
  Some (Script_typed_ir.Var_annot "steps").

Definition default_source_annot : option Script_typed_ir.var_annot :=
  Some (Script_typed_ir.Var_annot "source").

Definition default_sender_annot : option Script_typed_ir.var_annot :=
  Some (Script_typed_ir.Var_annot "sender").

Definition default_self_annot : option Script_typed_ir.var_annot :=
  Some (Script_typed_ir.Var_annot "self").

Definition default_arg_annot : option Script_typed_ir.var_annot :=
  Some (Script_typed_ir.Var_annot "arg").

Definition default_param_annot : option Script_typed_ir.var_annot :=
  Some (Script_typed_ir.Var_annot "parameter").

Definition default_storage_annot : option Script_typed_ir.var_annot :=
  Some (Script_typed_ir.Var_annot "storage").

Definition default_car_annot : option Script_typed_ir.field_annot :=
  Some (Script_typed_ir.Field_annot "car").

Definition default_cdr_annot : option Script_typed_ir.field_annot :=
  Some (Script_typed_ir.Field_annot "cdr").

Definition default_contract_annot : option Script_typed_ir.field_annot :=
  Some (Script_typed_ir.Field_annot "contract").

Definition default_addr_annot : option Script_typed_ir.field_annot :=
  Some (Script_typed_ir.Field_annot "address").

Definition default_manager_annot : option Script_typed_ir.field_annot :=
  Some (Script_typed_ir.Field_annot "manager").

Definition default_pack_annot : option Script_typed_ir.field_annot :=
  Some (Script_typed_ir.Field_annot "packed").

Definition default_unpack_annot : option Script_typed_ir.field_annot :=
  Some (Script_typed_ir.Field_annot "unpacked").

Definition default_slice_annot : option Script_typed_ir.field_annot :=
  Some (Script_typed_ir.Field_annot "slice").

Definition default_elt_annot : option Script_typed_ir.field_annot :=
  Some (Script_typed_ir.Field_annot "elt").

Definition default_key_annot : option Script_typed_ir.field_annot :=
  Some (Script_typed_ir.Field_annot "key").

Definition default_hd_annot : option Script_typed_ir.field_annot :=
  Some (Script_typed_ir.Field_annot "hd").

Definition default_tl_annot : option Script_typed_ir.field_annot :=
  Some (Script_typed_ir.Field_annot "tl").

Definition default_some_annot : option Script_typed_ir.field_annot :=
  Some (Script_typed_ir.Field_annot "some").

Definition default_left_annot : option Script_typed_ir.field_annot :=
  Some (Script_typed_ir.Field_annot "left").

Definition default_right_annot : option Script_typed_ir.field_annot :=
  Some (Script_typed_ir.Field_annot "right").

Definition default_binding_annot : option Script_typed_ir.field_annot :=
  Some (Script_typed_ir.Field_annot "bnd").

Definition default_sapling_state_annot : option Script_typed_ir.var_annot :=
  Some (Script_typed_ir.Var_annot "sapling").

Definition default_sapling_balance_annot : option Script_typed_ir.var_annot :=
  Some (Script_typed_ir.Var_annot "sapling_balance").

Definition unparse_type_annot
  (function_parameter : option Script_typed_ir.type_annot) : list string :=
  match function_parameter with
  | None => nil
  | Some (Script_typed_ir.Type_annot a_value) =>
    [ Pervasives.op_caret ":" a_value ]
  end.

Definition unparse_var_annot
  (function_parameter : option Script_typed_ir.var_annot) : list string :=
  match function_parameter with
  | None => nil
  | Some (Script_typed_ir.Var_annot a_value) =>
    [ Pervasives.op_caret "@" a_value ]
  end.

Definition unparse_field_annot
  (function_parameter : option Script_typed_ir.field_annot) : list string :=
  match function_parameter with
  | None => nil
  | Some (Script_typed_ir.Field_annot a_value) =>
    [ Pervasives.op_caret "%" a_value ]
  end.

Definition field_to_var_annot
  (function_parameter : option Script_typed_ir.field_annot)
  : option Script_typed_ir.var_annot :=
  match function_parameter with
  | None => None
  | Some (Script_typed_ir.Field_annot s) => Some (Script_typed_ir.Var_annot s)
  end.

Definition type_to_var_annot
  (function_parameter : option Script_typed_ir.type_annot)
  : option Script_typed_ir.var_annot :=
  match function_parameter with
  | None => None
  | Some (Script_typed_ir.Type_annot s) => Some (Script_typed_ir.Var_annot s)
  end.

Definition var_to_field_annot
  (function_parameter : option Script_typed_ir.var_annot)
  : option Script_typed_ir.field_annot :=
  match function_parameter with
  | None => None
  | Some (Script_typed_ir.Var_annot s) => Some (Script_typed_ir.Field_annot s)
  end.

Definition default_annot {A : Set}
  (default : option A) (function_parameter : option A) : option A :=
  match function_parameter with
  | None => default
  | annot => annot
  end.

Definition gen_access_annot
  (value_annot : option Script_typed_ir.var_annot)
  (op_staroptstar : option (option Script_typed_ir.field_annot))
  : option Script_typed_ir.field_annot -> option Script_typed_ir.var_annot :=
  let default :=
    match op_staroptstar with
    | Some op_starsthstar => op_starsthstar
    | None => None
    end in
  fun (field_annot : option Script_typed_ir.field_annot) =>
    match (value_annot, field_annot, default) with
    |
      ((None, None, _) | (Some _, None, None) |
      (None, Some (Script_typed_ir.Field_annot ""), _)) => None
    | (None, Some (Script_typed_ir.Field_annot f), _) =>
      Some (Script_typed_ir.Var_annot f)
    |
      (Some (Script_typed_ir.Var_annot v),
        (None | Some (Script_typed_ir.Field_annot "")),
        Some (Script_typed_ir.Field_annot f)) =>
      Some (Script_typed_ir.Var_annot (String.concat "." [ v; f ]))
    |
      (Some (Script_typed_ir.Var_annot v), Some (Script_typed_ir.Field_annot f),
        _) => Some (Script_typed_ir.Var_annot (String.concat "." [ v; f ]))
    end.

Definition merge_type_annot
  (legacy : bool) (annot1 : option Script_typed_ir.type_annot)
  (annot2 : option Script_typed_ir.type_annot)
  : M? (option Script_typed_ir.type_annot) :=
  match (annot1, annot2) with
  | ((None, None) | (Some _, None) | (None, Some _)) => Error_monad.ok_none
  | (Some (Script_typed_ir.Type_annot a1), Some (Script_typed_ir.Type_annot a2))
    =>
    if legacy || (String.equal a1 a2) then
      return? annot1
    else
      Error_monad.error_value
        (Build_extensible "Inconsistent_annotations" (string * string)
          ((Pervasives.op_caret ":" a1), (Pervasives.op_caret ":" a2)))
  end.

Definition merge_field_annot
  (legacy : bool) (annot1 : option Script_typed_ir.field_annot)
  (annot2 : option Script_typed_ir.field_annot)
  : M? (option Script_typed_ir.field_annot) :=
  match (annot1, annot2) with
  | ((None, None) | (Some _, None) | (None, Some _)) => Error_monad.ok_none
  |
    (Some (Script_typed_ir.Field_annot a1),
      Some (Script_typed_ir.Field_annot a2)) =>
    if legacy || (String.equal a1 a2) then
      return? annot1
    else
      Error_monad.error_value
        (Build_extensible "Inconsistent_annotations" (string * string)
          ((Pervasives.op_caret "%" a1), (Pervasives.op_caret "%" a2)))
  end.

Definition merge_var_annot
  (annot1 : option Script_typed_ir.var_annot)
  (annot2 : option Script_typed_ir.var_annot)
  : option Script_typed_ir.var_annot :=
  match (annot1, annot2) with
  | ((None, None) | (Some _, None) | (None, Some _)) => None
  | (Some (Script_typed_ir.Var_annot a1), Some (Script_typed_ir.Var_annot a2))
    =>
    if String.equal a1 a2 then
      annot1
    else
      None
  end.

Definition error_unexpected_annot {A : Set}
  (loc : Alpha_context.Script.location) (annot : list A) : M? unit :=
  match annot with
  | [] => Error_monad.ok_unit
  | cons _ _ =>
    Error_monad.error_value
      (Build_extensible "Unexpected_annotation" Alpha_context.Script.location
        loc)
  end.

Definition string_iter {A : Set}
  (p_value : ascii -> Pervasives.result unit A) (s : string) (i : int)
  : Pervasives.result unit A :=
  let len := String.length s in
  let fix aux (i : int) : Pervasives.result unit A :=
    if i >=i len then
      Error_monad.ok_unit
    else
      let? '_ := p_value (String.get s i) in
      aux (i +i 1) in
  aux i.

Definition is_allowed_char (function_parameter : ascii) : bool :=
  match function_parameter with
  |
    ("a" % char | "b" % char | "c" % char | "d" % char | "e" % char | "f" % char
    | "g" % char | "h" % char | "i" % char | "j" % char | "k" % char |
    "l" % char | "m" % char | "n" % char | "o" % char | "p" % char | "q" % char
    | "r" % char | "s" % char | "t" % char | "u" % char | "v" % char |
    "w" % char | "x" % char | "y" % char | "z" % char | "A" % char | "B" % char
    | "C" % char | "D" % char | "E" % char | "F" % char | "G" % char |
    "H" % char | "I" % char | "J" % char | "K" % char | "L" % char | "M" % char
    | "N" % char | "O" % char | "P" % char | "Q" % char | "R" % char |
    "S" % char | "T" % char | "U" % char | "V" % char | "W" % char | "X" % char
    | "Y" % char | "Z" % char | "_" % char | "." % char | "%" % char |
    "@" % char | "0" % char | "1" % char | "2" % char | "3" % char | "4" % char
    | "5" % char | "6" % char | "7" % char | "8" % char | "9" % char) => true
  | _ => false
  end.

Definition check_char (loc : Alpha_context.Script.location) (c : ascii)
  : M? unit :=
  if is_allowed_char c then
    Error_monad.ok_unit
  else
    Error_monad.error_value
      (Build_extensible "Unexpected_annotation" Alpha_context.Script.location
        loc).

Definition max_annot_length : int := 255.

Inductive annot_opt : Set :=
| Field_annot_opt : option string -> annot_opt
| Type_annot_opt : option string -> annot_opt
| Var_annot_opt : option string -> annot_opt.

Definition parse_annots
  (loc : Alpha_context.Script.location) (op_staroptstar : option bool)
  : option bool -> list string -> M? (list annot_opt) :=
  let allow_special_var :=
    match op_staroptstar with
    | Some op_starsthstar => op_starsthstar
    | None => false
    end in
  fun (op_staroptstar : option bool) =>
    let allow_special_field :=
      match op_staroptstar with
      | Some op_starsthstar => op_starsthstar
      | None => false
      end in
    fun (l_value : list string) =>
      let sub_or_wildcard {A : Set}
        (specials : list Char.t) (wrap : option string -> A) (s : string)
        (acc_value : list A) : M? (list A) :=
        let mem_char (c : Char.t) (cs : list Char.t) : bool :=
          List._exists (Char.equal c) cs in
        let len := String.length s in
        let? '_ :=
          if len >i max_annot_length then
            Error_monad.error_value
              (Build_extensible "Unexpected_annotation"
                Alpha_context.Script.location loc)
          else
            Error_monad.ok_unit in
        if len =i 1 then
          return? (cons (wrap None) acc_value)
        else
          match
            ((String.get s 1),
              match String.get s 1 with
              | "@" % char => (len =i 2) && (mem_char "@" % char specials)
              | _ => false
              end,
              match String.get s 1 with
              | "%" % char => mem_char "%" % char specials
              | _ => false
              end) with
          |
            (("a" % char | "b" % char | "c" % char | "d" % char | "e" % char |
            "f" % char | "g" % char | "h" % char | "i" % char | "j" % char |
            "k" % char | "l" % char | "m" % char | "n" % char | "o" % char |
            "p" % char | "q" % char | "r" % char | "s" % char | "t" % char |
            "u" % char | "v" % char | "w" % char | "x" % char | "y" % char |
            "z" % char | "A" % char | "B" % char | "C" % char | "D" % char |
            "E" % char | "F" % char | "G" % char | "H" % char | "I" % char |
            "J" % char | "K" % char | "L" % char | "M" % char | "N" % char |
            "O" % char | "P" % char | "Q" % char | "R" % char | "S" % char |
            "T" % char | "U" % char | "V" % char | "W" % char | "X" % char |
            "Y" % char | "Z" % char | "_" % char | "0" % char | "1" % char |
            "2" % char | "3" % char | "4" % char | "5" % char | "6" % char |
            "7" % char | "8" % char | "9" % char), _, _) =>
            let? '_ := string_iter (check_char loc) s 2 in
            return? (cons (wrap (Some (String.sub s 1 (len -i 1)))) acc_value)
          | ("@" % char, true, _) => return? (cons (wrap (Some "@")) acc_value)
          | ("%" % char, _, true) =>
            if len =i 2 then
              return? (cons (wrap (Some "%")) acc_value)
            else
              if
                (len =i 3) &&
                (Compare.Char.(Compare.S.op_eq) (String.get s 2) "%" % char)
              then
                return? (cons (wrap (Some "%%")) acc_value)
              else
                Error_monad.error_value
                  (Build_extensible "Unexpected_annotation"
                    Alpha_context.Script.location loc)
          | (_, _, _) =>
            Error_monad.error_value
              (Build_extensible "Unexpected_annotation"
                Alpha_context.Script.location loc)
          end in
      Error_monad.op_gtpipequestion
        (List.fold_left_e
          (fun (acc_value : list annot_opt) =>
            fun (s : string) =>
              if (String.length s) =i 0 then
                Error_monad.error_value
                  (Build_extensible "Unexpected_annotation"
                    Alpha_context.Script.location loc)
              else
                match String.get s 0 with
                | ":" % char =>
                  sub_or_wildcard nil
                    (fun (a_value : option string) => Type_annot_opt a_value) s
                    acc_value
                | "@" % char =>
                  sub_or_wildcard
                    (if allow_special_var then
                      [ "%" % char ]
                    else
                      nil)
                    (fun (a_value : option string) => Var_annot_opt a_value) s
                    acc_value
                | "%" % char =>
                  sub_or_wildcard
                    (if allow_special_field then
                      [ "@" % char ]
                    else
                      nil)
                    (fun (a_value : option string) => Field_annot_opt a_value) s
                    acc_value
                | _ =>
                  Error_monad.error_value
                    (Build_extensible "Unexpected_annotation"
                      Alpha_context.Script.location loc)
                end) nil l_value) List.rev.

Definition opt_var_of_var_opt (function_parameter : option string)
  : option Script_typed_ir.var_annot :=
  match function_parameter with
  | None => None
  | Some a_value => Some (Script_typed_ir.Var_annot a_value)
  end.

Definition opt_field_of_field_opt (function_parameter : option string)
  : option Script_typed_ir.field_annot :=
  match function_parameter with
  | None => None
  | Some a_value => Some (Script_typed_ir.Field_annot a_value)
  end.

Definition opt_type_of_type_opt (function_parameter : option string)
  : option Script_typed_ir.type_annot :=
  match function_parameter with
  | None => None
  | Some a_value => Some (Script_typed_ir.Type_annot a_value)
  end.

Definition classify_annot
  (loc : Alpha_context.Script.location) (l_value : list annot_opt)
  : M?
    (list (option Script_typed_ir.var_annot) *
      list (option Script_typed_ir.type_annot) *
      list (option Script_typed_ir.field_annot)) :=
  (* ❌ Use a trivial matching for the `with` clause, like: *)
  typ_with_with_non_trivial_matching.

Definition get_one_annot {A : Set}
  (loc : Alpha_context.Script.location) (function_parameter : list (option A))
  : M? (option A) :=
  match function_parameter with
  | [] => Error_monad.ok_none
  | cons a_value [] => return? a_value
  | _ =>
    Error_monad.error_value
      (Build_extensible "Unexpected_annotation" Alpha_context.Script.location
        loc)
  end.

Definition get_two_annot {A : Set}
  (loc : Alpha_context.Script.location) (function_parameter : list (option A))
  : M? (option A * option A) :=
  match function_parameter with
  | [] => return? (None, None)
  | cons a_value [] => return? (a_value, None)
  | cons a_value (cons b_value []) => return? (a_value, b_value)
  | _ =>
    Error_monad.error_value
      (Build_extensible "Unexpected_annotation" Alpha_context.Script.location
        loc)
  end.

Definition parse_type_annot (loc : int) (annot : list string)
  : M? (option Script_typed_ir.type_annot) :=
  let? '(vars, types, fields) :=
    Error_monad.op_gtgtquestion (parse_annots loc None None annot)
      (classify_annot loc) in
  let? '_ := error_unexpected_annot loc vars in
  let? '_ := error_unexpected_annot loc fields in
  get_one_annot loc types.

Definition parse_type_field_annot (loc : int) (annot : list string)
  : M? (option Script_typed_ir.type_annot * option Script_typed_ir.field_annot) :=
  let? '(vars, types, fields) :=
    Error_monad.op_gtgtquestion (parse_annots loc None None annot)
      (classify_annot loc) in
  let? '_ := error_unexpected_annot loc vars in
  let? t_value := get_one_annot loc types in
  let? f := get_one_annot loc fields in
  return? (t_value, f).

Definition parse_composed_type_annot (loc : int) (annot : list string)
  : M?
    (option Script_typed_ir.type_annot * option Script_typed_ir.field_annot *
      option Script_typed_ir.field_annot) :=
  let? '(vars, types, fields) :=
    Error_monad.op_gtgtquestion (parse_annots loc None None annot)
      (classify_annot loc) in
  let? '_ := error_unexpected_annot loc vars in
  let? t_value := get_one_annot loc types in
  let? '(f1, f2) := get_two_annot loc fields in
  return? (t_value, f1, f2).

Definition parse_field_annot (loc : int) (annot : list string)
  : M? (option Script_typed_ir.field_annot) :=
  let? '(vars, types, fields) :=
    Error_monad.op_gtgtquestion (parse_annots loc None None annot)
      (classify_annot loc) in
  let? '_ := error_unexpected_annot loc vars in
  let? '_ := error_unexpected_annot loc types in
  get_one_annot loc fields.

Definition extract_field_annot (function_parameter : Alpha_context.Script.node)
  : M? (Alpha_context.Script.node * option Script_typed_ir.field_annot) :=
  match function_parameter with
  | Micheline.Prim loc prim args annot =>
    let fix extract_first
      (acc_value : list string) (function_parameter : list string)
      : option string * Micheline.annot :=
      match function_parameter with
      | [] => (None, annot)
      | cons s rest =>
        if
          ((String.length s) >i 0) &&
          (Compare.Char.(Compare.S.op_eq) (String.get s 0) "%" % char)
        then
          ((Some s), (List.rev_append acc_value rest))
        else
          extract_first (cons s acc_value) rest
      end in
    let '(field_annot, annot) := extract_first nil annot in
    let? field_annot :=
      match field_annot with
      | None => Error_monad.ok_none
      | Some field_annot => parse_field_annot loc [ field_annot ]
      end in
    return? ((Micheline.Prim loc prim args annot), field_annot)
  | expr => return? (expr, None)
  end.

Definition check_correct_field
  (f1 : option Script_typed_ir.field_annot)
  (f2 : option Script_typed_ir.field_annot) : M? unit :=
  match (f1, f2) with
  | ((None, _) | (_, None)) => Error_monad.ok_unit
  |
    (Some (Script_typed_ir.Field_annot s1),
      Some (Script_typed_ir.Field_annot s2)) =>
    if String.equal s1 s2 then
      Error_monad.ok_unit
    else
      Error_monad.error_value
        (Build_extensible "Inconsistent_field_annotations" (string * string)
          ((Pervasives.op_caret "%" s1), (Pervasives.op_caret "%" s2)))
  end.

Definition parse_var_annot
  (loc : int) (default : option (option Script_typed_ir.var_annot))
  (annot : list string) : M? (option Script_typed_ir.var_annot) :=
  let? '(vars, types, fields) :=
    Error_monad.op_gtgtquestion (parse_annots loc None None annot)
      (classify_annot loc) in
  let? '_ := error_unexpected_annot loc types in
  let? '_ := error_unexpected_annot loc fields in
  let? function_parameter := get_one_annot loc vars in
  match function_parameter with
  | (Some _) as a_value => return? a_value
  | None =>
    match default with
    | Some a_value => return? a_value
    | None => return? None
    end
  end.

Definition split_last_dot
  (function_parameter : option Script_typed_ir.field_annot)
  : option Script_typed_ir.var_annot * option Script_typed_ir.field_annot :=
  match function_parameter with
  | None => (None, None)
  | Some (Script_typed_ir.Field_annot s) =>
    match String.rindex_opt s "." % char with
    | None => (None, (Some (Script_typed_ir.Field_annot s)))
    | Some i =>
      let s1 := String.sub s 0 i in
      let s2 := String.sub s (i +i 1) (((String.length s) -i i) -i 1) in
      let f :=
        if
          (Compare.String.(Compare.S.equal) s2 "car") ||
          (Compare.String.(Compare.S.equal) s2 "cdr")
        then
          None
        else
          Some (Script_typed_ir.Field_annot s2) in
      ((Some (Script_typed_ir.Var_annot s1)), f)
    end
  end.

Definition common_prefix
  (v1 : option Script_typed_ir.var_annot)
  (v2 : option Script_typed_ir.var_annot) : option Script_typed_ir.var_annot :=
  match
    ((v1, v2),
      match (v1, v2) with
      |
        (Some (Script_typed_ir.Var_annot s1),
          Some (Script_typed_ir.Var_annot s2)) =>
        Compare.String.(Compare.S.equal) s1 s2
      | _ => false
      end) with
  |
    ((Some (Script_typed_ir.Var_annot s1), Some (Script_typed_ir.Var_annot s2)),
      true) => v1
  | ((Some _, None), _) => v1
  | ((None, Some _), _) => v2
  | ((_, _), _) => None
  end.

Definition parse_constr_annot
  (loc : int) (if_special_first : option (option Script_typed_ir.field_annot))
  (if_special_second : option (option Script_typed_ir.field_annot))
  (annot : list string)
  : M?
    (option Script_typed_ir.var_annot * option Script_typed_ir.type_annot *
      option Script_typed_ir.field_annot * option Script_typed_ir.field_annot) :=
  let? '(vars, types, fields) :=
    Error_monad.op_gtgtquestion (parse_annots loc None (Some true) annot)
      (classify_annot loc) in
  let? v := get_one_annot loc vars in
  let? t_value := get_one_annot loc types in
  let? '(f1, f2) := get_two_annot loc fields in
  let? '(v1, f1) :=
    match (if_special_first, f1) with
    | (Some special_var, Some (Script_typed_ir.Field_annot "@")) =>
      return? (split_last_dot special_var)
    | (None, Some (Script_typed_ir.Field_annot "@")) =>
      Error_monad.error_value
        (Build_extensible "Unexpected_annotation" Alpha_context.Script.location
          loc)
    | (_, _) => return? (v, f1)
    end in
  let? '(v2, f2) :=
    match (if_special_second, f2) with
    | (Some special_var, Some (Script_typed_ir.Field_annot "@")) =>
      return? (split_last_dot special_var)
    | (None, Some (Script_typed_ir.Field_annot "@")) =>
      Error_monad.error_value
        (Build_extensible "Unexpected_annotation" Alpha_context.Script.location
          loc)
    | (_, _) => return? (v, f2)
    end in
  let v :=
    match v with
    | None => common_prefix v1 v2
    | Some _ => v
    end in
  return? (v, t_value, f1, f2).

Definition parse_two_var_annot (loc : int) (annot : list string)
  : M? (option Script_typed_ir.var_annot * option Script_typed_ir.var_annot) :=
  let? '(vars, types, fields) :=
    Error_monad.op_gtgtquestion (parse_annots loc None None annot)
      (classify_annot loc) in
  let? '_ := error_unexpected_annot loc types in
  let? '_ := error_unexpected_annot loc fields in
  get_two_annot loc vars.

Definition var_annot_from_special
  (field_name : option Script_typed_ir.field_annot)
  (default : option Script_typed_ir.var_annot)
  (value_annot : option Script_typed_ir.var_annot)
  (v : option Script_typed_ir.var_annot) : option Script_typed_ir.var_annot :=
  match v with
  | Some (Script_typed_ir.Var_annot "%") => field_to_var_annot field_name
  | Some (Script_typed_ir.Var_annot "%%") => default
  | Some _ => v
  | None => value_annot
  end.

Definition parse_destr_annot
  (loc : int) (annot : list string)
  (default_accessor : option Script_typed_ir.field_annot)
  (field_name : option Script_typed_ir.field_annot)
  (pair_annot : option Script_typed_ir.var_annot)
  (value_annot : option Script_typed_ir.var_annot)
  : M? (option Script_typed_ir.var_annot * option Script_typed_ir.field_annot) :=
  let? '(vars, types, fields) :=
    Error_monad.op_gtgtquestion (parse_annots loc (Some true) None annot)
      (classify_annot loc) in
  let? '_ := error_unexpected_annot loc types in
  let? v := get_one_annot loc vars in
  let? f := get_one_annot loc fields in
  let default := gen_access_annot pair_annot (Some default_accessor) field_name
    in
  let v := var_annot_from_special field_name default value_annot v in
  return? (v, f).

Definition parse_unpair_annot
  (loc : int) (annot : list string)
  (field_name_car : option Script_typed_ir.field_annot)
  (field_name_cdr : option Script_typed_ir.field_annot)
  (pair_annot : option Script_typed_ir.var_annot)
  (value_annot_car : option Script_typed_ir.var_annot)
  (value_annot_cdr : option Script_typed_ir.var_annot)
  : M?
    (option Script_typed_ir.var_annot * option Script_typed_ir.var_annot *
      option Script_typed_ir.field_annot * option Script_typed_ir.field_annot) :=
  let? '(vars, types, fields) :=
    Error_monad.op_gtgtquestion (parse_annots loc (Some true) None annot)
      (classify_annot loc) in
  let? '_ := error_unexpected_annot loc types in
  let? '(vcar, vcdr) := get_two_annot loc vars in
  let? '(fcar, fcdr) := get_two_annot loc fields in
  let default_car :=
    gen_access_annot pair_annot (Some default_car_annot) field_name_car in
  let default_cdr :=
    gen_access_annot pair_annot (Some default_cdr_annot) field_name_cdr in
  let vcar :=
    var_annot_from_special field_name_car default_car value_annot_car vcar in
  let vcdr :=
    var_annot_from_special field_name_cdr default_cdr value_annot_cdr vcdr in
  return? (vcar, vcdr, fcar, fcdr).

Definition parse_entrypoint_annot
  (loc : int) (default : option (option Script_typed_ir.var_annot))
  (annot : list string)
  : M? (option Script_typed_ir.var_annot * option Script_typed_ir.field_annot) :=
  let? '(vars, types, fields) :=
    Error_monad.op_gtgtquestion (parse_annots loc None None annot)
      (classify_annot loc) in
  let? '_ := error_unexpected_annot loc types in
  let? f := get_one_annot loc fields in
  let? function_parameter := get_one_annot loc vars in
  match function_parameter with
  | (Some _) as a_value => return? (a_value, f)
  | None =>
    match default with
    | Some a_value => return? (a_value, f)
    | None => return? (None, f)
    end
  end.

Definition parse_var_type_annot (loc : int) (annot : list string)
  : M? (option Script_typed_ir.var_annot * option Script_typed_ir.type_annot) :=
  let? '(vars, types, fields) :=
    Error_monad.op_gtgtquestion (parse_annots loc None None annot)
      (classify_annot loc) in
  let? '_ := error_unexpected_annot loc fields in
  let? v := get_one_annot loc vars in
  let? t_value := get_one_annot loc types in
  return? (v, t_value).
