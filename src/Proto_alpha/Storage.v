Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Blinded_public_key_hash.
Require TezosOfOCaml.Proto_alpha.Contract_repr.
Require TezosOfOCaml.Proto_alpha.Cycle_repr.
Require TezosOfOCaml.Proto_alpha.Gas_limit_repr.
Require TezosOfOCaml.Proto_alpha.Lazy_storage_kind.
Require TezosOfOCaml.Proto_alpha.Level_repr.
Require TezosOfOCaml.Proto_alpha.Manager_repr.
Require TezosOfOCaml.Proto_alpha.Migration_repr.
Require TezosOfOCaml.Proto_alpha.Misc.
Require TezosOfOCaml.Proto_alpha.Nonce_hash.
Require TezosOfOCaml.Proto_alpha.Path_encoding.
Require TezosOfOCaml.Proto_alpha.Raw_context.
Require TezosOfOCaml.Proto_alpha.Raw_context_intf.
Require TezosOfOCaml.Proto_alpha.Raw_level_repr.
Require TezosOfOCaml.Proto_alpha.Receipt_repr.
Require TezosOfOCaml.Proto_alpha.Roll_repr.
Require TezosOfOCaml.Proto_alpha.Sapling_repr.
Require TezosOfOCaml.Proto_alpha.Script_expr_hash.
Require TezosOfOCaml.Proto_alpha.Script_repr.
Require TezosOfOCaml.Proto_alpha.Seed_repr.
Require TezosOfOCaml.Proto_alpha.Storage_description.
Require TezosOfOCaml.Proto_alpha.Storage_functors.
Require TezosOfOCaml.Proto_alpha.Storage_sigs.
Require TezosOfOCaml.Proto_alpha.Tez_repr.
Require TezosOfOCaml.Proto_alpha.Vote_repr.
Require TezosOfOCaml.Proto_alpha.Voting_period_repr.

Module Encoding.
  Module UInt16.
    Definition t : Set := int.
    
    Definition encoding : Data_encoding.encoding int := Data_encoding.uint16.
    
    Definition module :=
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}.
  End UInt16.
  Definition UInt16 := UInt16.module.
  
  Module Int32.
    Definition t : Set := Int32.t.
    
    Definition encoding : Data_encoding.encoding int32 :=
      Data_encoding.int32_value.
    
    Definition module :=
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}.
  End Int32.
  Definition Int32 := Int32.module.
  
  Module Int64.
    Definition t : Set := Int64.t.
    
    Definition encoding : Data_encoding.encoding int64 :=
      Data_encoding.int64_value.
    
    Definition module :=
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}.
  End Int64.
  Definition Int64 := Int64.module.
  
  Module Z.
    Definition t : Set := Z.t.
    
    Definition encoding : Data_encoding.encoding Z.t := Data_encoding.z.
    
    Definition module :=
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}.
  End Z.
  Definition Z := Z.module.
End Encoding.

Module Int31_index.
  Definition t : Set := int.
  
  Definition path_length : int := 1.
  
  Definition to_path (c : int) (l_value : list string) : list string :=
    cons (Pervasives.string_of_int c) l_value.
  
  Definition of_path (function_parameter : list string) : option int :=
    match function_parameter with
    | ([] | cons _ (cons _ _)) => None
    | cons c [] => Pervasives.int_of_string_opt c
    end.
  
  Definition ipath (a : Set) : Set := a * t.
  
  Definition args : Storage_description.args :=
    Storage_description.One
      {| Storage_description.args.One.rpc_arg := RPC_arg.int_value;
        Storage_description.args.One.encoding := Data_encoding.int31;
        Storage_description.args.One.compare := Compare.Int.(Compare.S.compare)
        |}.
  
  Definition module : Storage_functors.INDEX.signature (ipath := ipath) := {|
      Storage_functors.INDEX.to_path := to_path;
      Storage_functors.INDEX.of_path := of_path;
      Storage_functors.INDEX.path_length := path_length;
      Storage_functors.INDEX.args := args
    |}.
End Int31_index.
Definition Int31_index : Storage_functors.INDEX (t := int) (ipath := _) :=
  Int31_index.module.

Module Make_index.
  Class FArgs {H_t : Set} := {
    H : Storage_description.INDEX (t := H_t);
  }.
  Arguments Build_FArgs {_}.
  
  (** Inclusion of the module [H] *)
  Definition t `{FArgs} := H.(Storage_description.INDEX.t).
  
  Definition to_path `{FArgs} := H.(Storage_description.INDEX.to_path).
  
  Definition of_path `{FArgs} := H.(Storage_description.INDEX.of_path).
  
  Definition path_length `{FArgs} := H.(Storage_description.INDEX.path_length).
  
  Definition rpc_arg `{FArgs} := H.(Storage_description.INDEX.rpc_arg).
  
  Definition encoding `{FArgs} := H.(Storage_description.INDEX.encoding).
  
  Definition compare `{FArgs} := H.(Storage_description.INDEX.compare).
  
  Definition ipath `{FArgs} (a : Set) : Set := a * t.
  
  Definition args `{FArgs} : Storage_description.args :=
    Storage_description.One
      {| Storage_description.args.One.rpc_arg := rpc_arg;
        Storage_description.args.One.encoding := encoding;
        Storage_description.args.One.compare := compare |}.
  
  Definition functor `{FArgs} : Storage_functors.INDEX.signature (ipath := ipath) := {|
      Storage_functors.INDEX.to_path := to_path;
      Storage_functors.INDEX.of_path := of_path;
      Storage_functors.INDEX.path_length := path_length;
      Storage_functors.INDEX.args := args
    |}.
End Make_index.
Definition Make_index {H_t : Set} (H : Storage_description.INDEX (t := H_t))
  : Storage_functors.INDEX (t := H.(Storage_description.INDEX.t))
    (ipath := fun (a : Set) => a * H.(Storage_description.INDEX.t)) :=
  let '_ := Make_index.Build_FArgs H in
  Make_index.functor.

Module Simple_single_data_storage.
  Record signature {value : Set} : Set := {
    value := value;
    get : Raw_context.t -> M=? value;
    update : Raw_context.t -> value -> M=? Raw_context.t;
    init_value : Raw_context.t -> value -> M=? Raw_context.t;
  }.
End Simple_single_data_storage.
Definition Simple_single_data_storage := @Simple_single_data_storage.signature.
Arguments Simple_single_data_storage {_}.

Definition Block_priority : Simple_single_data_storage (value := int) :=
  let functor_result :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      {|
        Raw_context_intf.T.mem := Raw_context.mem;
        Raw_context_intf.T.mem_tree := Raw_context.mem_tree;
        Raw_context_intf.T.get := Raw_context.get;
        Raw_context_intf.T.get_tree := Raw_context.get_tree;
        Raw_context_intf.T.find := Raw_context.find;
        Raw_context_intf.T.find_tree := Raw_context.find_tree;
        Raw_context_intf.T.list_value := Raw_context.list_value;
        Raw_context_intf.T.init_value := Raw_context.init_value;
        Raw_context_intf.T.init_tree := Raw_context.init_tree;
        Raw_context_intf.T.update := Raw_context.update;
        Raw_context_intf.T.update_tree := Raw_context.update_tree;
        Raw_context_intf.T.add := Raw_context.add;
        Raw_context_intf.T.add_tree := Raw_context.add_tree;
        Raw_context_intf.T.remove := Raw_context.remove;
        Raw_context_intf.T.remove_existing := Raw_context.remove_existing;
        Raw_context_intf.T.remove_existing_tree :=
          Raw_context.remove_existing_tree;
        Raw_context_intf.T.add_or_remove := Raw_context.add_or_remove;
        Raw_context_intf.T.add_or_remove_tree := Raw_context.add_or_remove_tree;
        Raw_context_intf.T.fold _ := Raw_context.fold;
        Raw_context_intf.T.Tree := Raw_context.Tree;
        Raw_context_intf.T.project := Raw_context.project;
        Raw_context_intf.T.absolute_key := Raw_context.absolute_key;
        Raw_context_intf.T.consume_gas := Raw_context.consume_gas;
        Raw_context_intf.T.check_enough_gas := Raw_context.check_enough_gas;
        Raw_context_intf.T.description := Raw_context.description
      |}
      (let name := [ "block_priority" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.UInt16 in
  {|
    Simple_single_data_storage.get :=
      functor_result.(Storage_sigs.Single_data_storage.get);
    Simple_single_data_storage.update :=
      functor_result.(Storage_sigs.Single_data_storage.update);
    Simple_single_data_storage.init_value :=
      functor_result.(Storage_sigs.Single_data_storage.init_value)
  |}.

Module Contract.
  Definition Raw_context :=
    Storage_functors.Make_subcontext Storage_functors.Registered
      {|
        Raw_context_intf.T.mem := Raw_context.mem;
        Raw_context_intf.T.mem_tree := Raw_context.mem_tree;
        Raw_context_intf.T.get := Raw_context.get;
        Raw_context_intf.T.get_tree := Raw_context.get_tree;
        Raw_context_intf.T.find := Raw_context.find;
        Raw_context_intf.T.find_tree := Raw_context.find_tree;
        Raw_context_intf.T.list_value := Raw_context.list_value;
        Raw_context_intf.T.init_value := Raw_context.init_value;
        Raw_context_intf.T.init_tree := Raw_context.init_tree;
        Raw_context_intf.T.update := Raw_context.update;
        Raw_context_intf.T.update_tree := Raw_context.update_tree;
        Raw_context_intf.T.add := Raw_context.add;
        Raw_context_intf.T.add_tree := Raw_context.add_tree;
        Raw_context_intf.T.remove := Raw_context.remove;
        Raw_context_intf.T.remove_existing := Raw_context.remove_existing;
        Raw_context_intf.T.remove_existing_tree :=
          Raw_context.remove_existing_tree;
        Raw_context_intf.T.add_or_remove := Raw_context.add_or_remove;
        Raw_context_intf.T.add_or_remove_tree := Raw_context.add_or_remove_tree;
        Raw_context_intf.T.fold _ := Raw_context.fold;
        Raw_context_intf.T.Tree := Raw_context.Tree;
        Raw_context_intf.T.project := Raw_context.project;
        Raw_context_intf.T.absolute_key := Raw_context.absolute_key;
        Raw_context_intf.T.consume_gas := Raw_context.consume_gas;
        Raw_context_intf.T.check_enough_gas := Raw_context.check_enough_gas;
        Raw_context_intf.T.description := Raw_context.description
      |}
      (let name := [ "contracts" ] in
      {|
        Storage_sigs.NAME.name := name
      |}).
  
  Definition Global_counter : Simple_single_data_storage (value := Z.t) :=
    let functor_result :=
      Storage_functors.Make_single_data_storage Storage_functors.Registered
        Raw_context
        (let name := [ "global_counter" ] in
        {|
          Storage_sigs.NAME.name := name
        |}) Encoding.Z in
    {|
      Simple_single_data_storage.get :=
        functor_result.(Storage_sigs.Single_data_storage.get);
      Simple_single_data_storage.update :=
        functor_result.(Storage_sigs.Single_data_storage.update);
      Simple_single_data_storage.init_value :=
        functor_result.(Storage_sigs.Single_data_storage.init_value)
    |}.
  
  Definition Indexed_context :=
    Storage_functors.Make_indexed_subcontext
      (Storage_functors.Make_subcontext Storage_functors.Registered Raw_context
        (let name := [ "index" ] in
        {|
          Storage_sigs.NAME.name := name
        |})) (Make_index Contract_repr.Index).
  
  Definition fold {A : Set}
    : Raw_context.t -> A -> (Contract_repr.t -> A -> M= A) -> M= A :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.fold_keys).
  
  Definition list_value : Raw_context.t -> M= (list Contract_repr.t) :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.keys).
  
  Definition Balance :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "balance" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Tez_repr.encoding
      |}.
  
  Definition Frozen_balance_index :=
    Storage_functors.Make_indexed_subcontext
      (Storage_functors.Make_subcontext Storage_functors.Registered
        Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
        (let name := [ "frozen_balance" ] in
        {|
          Storage_sigs.NAME.name := name
        |})) (Make_index Cycle_repr.Index).
  
  Definition Frozen_deposits :=
    Frozen_balance_index.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "deposits" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Tez_repr.encoding
      |}.
  
  Definition Frozen_fees :=
    Frozen_balance_index.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "fees" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Tez_repr.encoding
      |}.
  
  Definition Frozen_rewards :=
    Frozen_balance_index.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "rewards" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Tez_repr.encoding
      |}.
  
  Definition Manager :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "manager" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Manager_repr.encoding
      |}.
  
  Definition Delegate :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "delegate" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding :=
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)
      |}.
  
  Definition Inactive_delegate :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_set)
      Storage_functors.Registered
      (let name := [ "inactive_delegate" ] in
      {|
        Storage_sigs.NAME.name := name
      |}).
  
  Definition Delegate_desactivation :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "delegate_desactivation" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Cycle_repr.encoding
      |}.
  
  Definition Delegated :=
    Storage_functors.Make_data_set_storage
      (Storage_functors.Make_subcontext Storage_functors.Registered
        Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
        (let name := [ "delegated" ] in
        {|
          Storage_sigs.NAME.name := name
        |})) (Make_index Contract_repr.Index).
  
  Definition Counter :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "counter" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.Z.
  
  Module Make_carbonated_map_expr.
    Class FArgs := {
      N : Storage_sigs.NAME;
    }.
    
    Definition I `{FArgs} :=
      Indexed_context.(Storage_sigs.Indexed_raw_context.Make_carbonated_map) N
        (let t : Set := Script_repr.lazy_expr in
        let encoding := Script_repr.lazy_expr_encoding in
        {|
          Storage_sigs.VALUE.encoding := encoding
        |}).
    
    Definition context `{FArgs} : Set := Raw_context.t.
    
    Definition key `{FArgs} : Set := Contract_repr.t.
    
    Definition value `{FArgs} : Set := Script_repr.lazy_expr.
    
    Definition mem `{FArgs}
      : Raw_context.t -> Contract_repr.t -> M=? (Raw_context.t * bool) :=
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.mem).
    
    Definition remove_existing `{FArgs}
      : Raw_context.t -> Contract_repr.t -> M=? (Raw_context.t * int) :=
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.remove_existing).
    
    Definition remove `{FArgs}
      : Raw_context.t -> Contract_repr.t -> M=? (Raw_context.t * int * bool) :=
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.remove).
    
    Definition consume_deserialize_gas `{FArgs}
      (ctxt : Raw_context.t) (value : Script_repr.lazy_expr)
      : M? Raw_context.t :=
      Raw_context.(Raw_context_intf.T.consume_gas) ctxt
        (Script_repr.force_decode_cost value).
    
    Definition consume_serialize_gas `{FArgs}
      (ctxt : Raw_context.t) (value : Script_repr.lazy_expr)
      : M? Raw_context.t :=
      Raw_context.(Raw_context_intf.T.consume_gas) ctxt
        (Script_repr.force_bytes_cost value).
    
    Definition get `{FArgs} (ctxt : Raw_context.t) (contract : Contract_repr.t)
      : M=? (Raw_context.t * Script_repr.lazy_expr) :=
      let=? '(ctxt, value) :=
        I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.get) ctxt
          contract in
      return=
        (let? ctxt := consume_deserialize_gas ctxt value in
        return? (ctxt, value)).
    
    Definition find `{FArgs} (ctxt : Raw_context.t) (contract : Contract_repr.t)
      : M=? (Raw_context.t * option Script_repr.lazy_expr) :=
      let=? '(ctxt, value_opt) :=
        I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.find) ctxt
          contract in
      match value_opt with
      | None => return=? (ctxt, None)
      | Some value =>
        return=
          (let? ctxt := consume_deserialize_gas ctxt value in
          return? (ctxt, value_opt))
      end.
    
    Definition update `{FArgs}
      (ctxt : Raw_context.t) (contract : Contract_repr.t)
      (value : Script_repr.lazy_expr) : M=? (Raw_context.t * int) :=
      let=? ctxt := return= (consume_serialize_gas ctxt value) in
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.update) ctxt
        contract value.
    
    Definition add_or_remove `{FArgs}
      (ctxt : Raw_context.t) (contract : Contract_repr.t)
      (value_opt : option Script_repr.lazy_expr)
      : M=? (Raw_context.t * int * bool) :=
      match value_opt with
      | None =>
        I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.add_or_remove)
          ctxt contract None
      | Some value =>
        let=? ctxt := return= (consume_serialize_gas ctxt value) in
        I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.add_or_remove)
          ctxt contract value_opt
      end.
    
    Definition init_value `{FArgs}
      (ctxt : Raw_context.t) (contract : Contract_repr.t)
      (value : Script_repr.lazy_expr) : M=? (Raw_context.t * int) :=
      let=? ctxt := return= (consume_serialize_gas ctxt value) in
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.init_value)
        ctxt contract value.
    
    Definition add `{FArgs}
      (ctxt : Raw_context.t) (contract : Contract_repr.t)
      (value : Script_repr.lazy_expr) : M=? (Raw_context.t * int * bool) :=
      let=? ctxt := return= (consume_serialize_gas ctxt value) in
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.add) ctxt
        contract value.
    
    Definition functor `{FArgs} :=
      {|
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.mem := mem;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.get := get;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.find := find;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.update :=
          update;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.init_value :=
          init_value;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.add := add;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.add_or_remove :=
          add_or_remove;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.remove_existing :=
          remove_existing;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.remove :=
          remove
      |}.
  End Make_carbonated_map_expr.
  Definition Make_carbonated_map_expr (N : Storage_sigs.NAME)
    : Storage_sigs.Non_iterable_indexed_carbonated_data_storage
      (t := Raw_context.t) (key := Contract_repr.t)
      (value := Script_repr.lazy_expr) :=
    let '_ := Make_carbonated_map_expr.Build_FArgs N in
    Make_carbonated_map_expr.functor.
  
  Definition Code :=
    Make_carbonated_map_expr
      (let name := [ "code" ] in
      {|
        Storage_sigs.NAME.name := name
      |}).
  
  Definition Storage :=
    Make_carbonated_map_expr
      (let name := [ "storage" ] in
      {|
        Storage_sigs.NAME.name := name
      |}).
  
  Definition Paid_storage_space :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "paid_bytes" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.Z.
  
  Definition Used_storage_space :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "used_bytes" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.Z.
  
  Definition Roll_list :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "roll_list" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Roll_repr.encoding
      |}.
  
  Definition Change :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "change" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Tez_repr.encoding
      |}.
End Contract.

Module NEXT.
  Record signature {id : Set} : Set := {
    id := id;
    init_value : Raw_context.t -> M=? Raw_context.t;
    incr : Raw_context.t -> M=? (Raw_context.t * id);
  }.
End NEXT.
Definition NEXT := @NEXT.signature.
Arguments NEXT {_}.

Module Global_constants.
  Definition Map :
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage
      (t := Raw_context.t) (key := Script_expr_hash.t)
      (value := Script_repr.expr) :=
    let functor_result :=
      Storage_functors.Make_indexed_carbonated_data_storage
        (Storage_functors.Make_subcontext Storage_functors.Registered
          {|
            Raw_context_intf.T.mem := Raw_context.mem;
            Raw_context_intf.T.mem_tree := Raw_context.mem_tree;
            Raw_context_intf.T.get := Raw_context.get;
            Raw_context_intf.T.get_tree := Raw_context.get_tree;
            Raw_context_intf.T.find := Raw_context.find;
            Raw_context_intf.T.find_tree := Raw_context.find_tree;
            Raw_context_intf.T.list_value := Raw_context.list_value;
            Raw_context_intf.T.init_value := Raw_context.init_value;
            Raw_context_intf.T.init_tree := Raw_context.init_tree;
            Raw_context_intf.T.update := Raw_context.update;
            Raw_context_intf.T.update_tree := Raw_context.update_tree;
            Raw_context_intf.T.add := Raw_context.add;
            Raw_context_intf.T.add_tree := Raw_context.add_tree;
            Raw_context_intf.T.remove := Raw_context.remove;
            Raw_context_intf.T.remove_existing := Raw_context.remove_existing;
            Raw_context_intf.T.remove_existing_tree :=
              Raw_context.remove_existing_tree;
            Raw_context_intf.T.add_or_remove := Raw_context.add_or_remove;
            Raw_context_intf.T.add_or_remove_tree :=
              Raw_context.add_or_remove_tree;
            Raw_context_intf.T.fold _ := Raw_context.fold;
            Raw_context_intf.T.Tree := Raw_context.Tree;
            Raw_context_intf.T.project := Raw_context.project;
            Raw_context_intf.T.absolute_key := Raw_context.absolute_key;
            Raw_context_intf.T.consume_gas := Raw_context.consume_gas;
            Raw_context_intf.T.check_enough_gas := Raw_context.check_enough_gas;
            Raw_context_intf.T.description := Raw_context.description
          |}
          (let name := [ "global_constant" ] in
          {|
            Storage_sigs.NAME.name := name
          |}))
        (Make_index
          {|
            Storage_description.INDEX.to_path := Script_expr_hash.to_path;
            Storage_description.INDEX.of_path := Script_expr_hash.of_path;
            Storage_description.INDEX.path_length :=
              Script_expr_hash.path_length;
            Storage_description.INDEX.rpc_arg := Script_expr_hash.rpc_arg;
            Storage_description.INDEX.encoding := Script_expr_hash.encoding;
            Storage_description.INDEX.compare := Script_expr_hash.compare
          |})
        (let t : Set := Script_repr.expr in
        let encoding := Script_repr.expr_encoding in
        {|
          Storage_sigs.VALUE.encoding := encoding
        |}) in
    {|
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.mem :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.mem);
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.get :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.get);
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.find :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.find);
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.update :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.update);
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.init_value :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.init_value);
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.add :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.add);
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.add_or_remove :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.add_or_remove);
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.remove_existing :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.remove_existing);
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.remove :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.remove)
    |}.
End Global_constants.

Module Big_map.
  Definition id : Set := Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.ID.t).
  
  Definition Raw_context :=
    Storage_functors.Make_subcontext Storage_functors.Registered
      {|
        Raw_context_intf.T.mem := Raw_context.mem;
        Raw_context_intf.T.mem_tree := Raw_context.mem_tree;
        Raw_context_intf.T.get := Raw_context.get;
        Raw_context_intf.T.get_tree := Raw_context.get_tree;
        Raw_context_intf.T.find := Raw_context.find;
        Raw_context_intf.T.find_tree := Raw_context.find_tree;
        Raw_context_intf.T.list_value := Raw_context.list_value;
        Raw_context_intf.T.init_value := Raw_context.init_value;
        Raw_context_intf.T.init_tree := Raw_context.init_tree;
        Raw_context_intf.T.update := Raw_context.update;
        Raw_context_intf.T.update_tree := Raw_context.update_tree;
        Raw_context_intf.T.add := Raw_context.add;
        Raw_context_intf.T.add_tree := Raw_context.add_tree;
        Raw_context_intf.T.remove := Raw_context.remove;
        Raw_context_intf.T.remove_existing := Raw_context.remove_existing;
        Raw_context_intf.T.remove_existing_tree :=
          Raw_context.remove_existing_tree;
        Raw_context_intf.T.add_or_remove := Raw_context.add_or_remove;
        Raw_context_intf.T.add_or_remove_tree := Raw_context.add_or_remove_tree;
        Raw_context_intf.T.fold _ := Raw_context.fold;
        Raw_context_intf.T.Tree := Raw_context.Tree;
        Raw_context_intf.T.project := Raw_context.project;
        Raw_context_intf.T.absolute_key := Raw_context.absolute_key;
        Raw_context_intf.T.consume_gas := Raw_context.consume_gas;
        Raw_context_intf.T.check_enough_gas := Raw_context.check_enough_gas;
        Raw_context_intf.T.description := Raw_context.description
      |}
      (let name := [ "big_maps" ] in
      {|
        Storage_sigs.NAME.name := name
      |}).
  
  Module Next.
    Definition Storage :=
      Storage_functors.Make_single_data_storage Storage_functors.Registered
        Raw_context
        (let name := [ "next" ] in
        {|
          Storage_sigs.NAME.name := name
        |})
        {|
          Storage_sigs.VALUE.encoding :=
            Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.ID.encoding)
        |}.
    
    Definition incr (ctxt : Raw_context.t)
      : M=?
        (Raw_context.t * Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.ID.t)) :=
      let=? i := Storage.(Storage_sigs.Single_data_storage.get) ctxt in
      let=? ctxt :=
        Storage.(Storage_sigs.Single_data_storage.update) ctxt
          (Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.ID.next) i) in
      return=? (ctxt, i).
    
    Definition init_value (ctxt : Raw_context.t) : M=? Raw_context.t :=
      Storage.(Storage_sigs.Single_data_storage.init_value) ctxt
        Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.ID.init_value).
    
    Definition module :=
      {|
        NEXT.init_value := init_value;
        NEXT.incr := incr
      |}.
  End Next.
  Definition Next : NEXT (id := id) := Next.module.
  
  Definition Index := Lazy_storage_kind.Big_map.Id.
  
  Definition Indexed_context :=
    Storage_functors.Make_indexed_subcontext
      (Storage_functors.Make_subcontext Storage_functors.Registered Raw_context
        (let name := [ "index" ] in
        {|
          Storage_sigs.NAME.name := name
        |}))
      (Make_index
        {|
          Storage_description.INDEX.to_path :=
            Index.(Lazy_storage_kind.ID.to_path);
          Storage_description.INDEX.of_path :=
            Index.(Lazy_storage_kind.ID.of_path);
          Storage_description.INDEX.path_length :=
            Index.(Lazy_storage_kind.ID.path_length);
          Storage_description.INDEX.rpc_arg :=
            Index.(Lazy_storage_kind.ID.rpc_arg);
          Storage_description.INDEX.encoding :=
            Index.(Lazy_storage_kind.ID.encoding);
          Storage_description.INDEX.compare :=
            Index.(Lazy_storage_kind.ID.compare)
        |}).
  
  Definition rpc_arg
    : RPC_arg.arg Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.ID.t) :=
    Index.(Lazy_storage_kind.ID.rpc_arg).
  
  Definition fold {A : Set}
    : Raw_context.t -> A ->
    (Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.ID.t) -> A -> M= A) -> M= A :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.fold_keys).
  
  Definition list_value
    : Raw_context.t ->
    M= (list Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.ID.t)) :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.keys).
  
  Definition remove
    (ctxt : Raw_context.t)
    (n : Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.ID.t))
    : M= Raw_context.t :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.remove) ctxt n.
  
  Definition copy
    (ctxt : Raw_context.t)
    (from : Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.ID.t))
    (to_ : Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.ID.t))
    : M=? Raw_context.t :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.copy) ctxt from to_.
  
  Definition key : Set :=
    Raw_context.t * Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.ID.t).
  
  Definition Total_bytes :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "total_bytes" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.Z.
  
  Definition Key_type :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "key_type" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      (let t : Set := Script_repr.expr in
      let encoding := Script_repr.expr_encoding in
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}).
  
  Definition Value_type :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "value_type" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      (let t : Set := Script_repr.expr in
      let encoding := Script_repr.expr_encoding in
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}).
  
  Module Contents.
    Definition I :=
      Storage_functors.Make_indexed_carbonated_data_storage
        (Storage_functors.Make_subcontext Storage_functors.Registered
          Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
          (let name := [ "contents" ] in
          {|
            Storage_sigs.NAME.name := name
          |}))
        (Make_index
          {|
            Storage_description.INDEX.to_path := Script_expr_hash.to_path;
            Storage_description.INDEX.of_path := Script_expr_hash.of_path;
            Storage_description.INDEX.path_length :=
              Script_expr_hash.path_length;
            Storage_description.INDEX.rpc_arg := Script_expr_hash.rpc_arg;
            Storage_description.INDEX.encoding := Script_expr_hash.encoding;
            Storage_description.INDEX.compare := Script_expr_hash.compare
          |})
        (let t : Set := Script_repr.expr in
        let encoding := Script_repr.expr_encoding in
        {|
          Storage_sigs.VALUE.encoding := encoding
        |}).
    
    Definition context : Set :=
      Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context_intf.T.t).
    
    Definition key : Set := Script_expr_hash.t.
    
    Definition value : Set := Script_repr.expr.
    
    Definition mem
      : Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context_intf.T.t)
      -> Script_expr_hash.t -> M=? (Raw_context.t * bool) :=
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.mem).
    
    Definition remove_existing
      : Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context_intf.T.t)
      -> Script_expr_hash.t -> M=? (Raw_context.t * int) :=
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.remove_existing).
    
    Definition remove
      : Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context_intf.T.t)
      -> Script_expr_hash.t -> M=? (Raw_context.t * int * bool) :=
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.remove).
    
    Definition update
      : Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context_intf.T.t)
      -> Script_expr_hash.t -> Script_repr.expr -> M=? (Raw_context.t * int) :=
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.update).
    
    Definition add_or_remove
      : Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context_intf.T.t)
      -> Script_expr_hash.t -> option Script_repr.expr ->
      M=? (Raw_context.t * int * bool) :=
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.add_or_remove).
    
    Definition init_value
      : Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context_intf.T.t)
      -> Script_expr_hash.t -> Script_repr.expr -> M=? (Raw_context.t * int) :=
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.init_value).
    
    Definition add
      : Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context_intf.T.t)
      -> Script_expr_hash.t -> Script_repr.expr ->
      M=? (Raw_context.t * int * bool) :=
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.add).
    
    Definition list_values
      : option int -> option int ->
      Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context_intf.T.t)
      -> M=? (Raw_context.t * list Script_repr.expr) :=
      I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.list_values).
    
    Definition consume_deserialize_gas
      (ctxt : Raw_context.t) (value : Script_repr.expr) : M? Raw_context.t :=
      Raw_context.(Raw_context_intf.T.consume_gas) ctxt
        (Script_repr.deserialized_cost value).
    
    Definition get
      (ctxt :
        Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context_intf.T.t))
      (contract : Script_expr_hash.t)
      : M=? (Raw_context.t * Script_repr.expr) :=
      let=? '(ctxt, value) :=
        I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.get)
          ctxt contract in
      return=
        (let? ctxt := consume_deserialize_gas ctxt value in
        return? (ctxt, value)).
    
    Definition find
      (ctxt :
        Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context_intf.T.t))
      (contract : Script_expr_hash.t)
      : M=? (Raw_context.t * option Script_repr.expr) :=
      let=? '(ctxt, value_opt) :=
        I.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.find)
          ctxt contract in
      match value_opt with
      | None => return=? (ctxt, None)
      | Some value =>
        return=
          (let? ctxt := consume_deserialize_gas ctxt value in
          return? (ctxt, value_opt))
      end.
    
    Definition module :=
      {|
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.mem :=
          mem;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.get :=
          get;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.find :=
          find;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.update :=
          update;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.init_value :=
          init_value;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.add :=
          add;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.add_or_remove :=
          add_or_remove;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.remove_existing :=
          remove_existing;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.remove :=
          remove;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.list_values :=
          list_values
      |}.
  End Contents.
  Definition Contents
    : Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values
      (t := key) (key := Script_expr_hash.t) (value := Script_repr.expr) :=
    Contents.module.
End Big_map.

Module Sapling.
  Definition id : Set :=
    Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.ID.t).
  
  Definition Raw_context :=
    Storage_functors.Make_subcontext Storage_functors.Registered
      {|
        Raw_context_intf.T.mem := Raw_context.mem;
        Raw_context_intf.T.mem_tree := Raw_context.mem_tree;
        Raw_context_intf.T.get := Raw_context.get;
        Raw_context_intf.T.get_tree := Raw_context.get_tree;
        Raw_context_intf.T.find := Raw_context.find;
        Raw_context_intf.T.find_tree := Raw_context.find_tree;
        Raw_context_intf.T.list_value := Raw_context.list_value;
        Raw_context_intf.T.init_value := Raw_context.init_value;
        Raw_context_intf.T.init_tree := Raw_context.init_tree;
        Raw_context_intf.T.update := Raw_context.update;
        Raw_context_intf.T.update_tree := Raw_context.update_tree;
        Raw_context_intf.T.add := Raw_context.add;
        Raw_context_intf.T.add_tree := Raw_context.add_tree;
        Raw_context_intf.T.remove := Raw_context.remove;
        Raw_context_intf.T.remove_existing := Raw_context.remove_existing;
        Raw_context_intf.T.remove_existing_tree :=
          Raw_context.remove_existing_tree;
        Raw_context_intf.T.add_or_remove := Raw_context.add_or_remove;
        Raw_context_intf.T.add_or_remove_tree := Raw_context.add_or_remove_tree;
        Raw_context_intf.T.fold _ := Raw_context.fold;
        Raw_context_intf.T.Tree := Raw_context.Tree;
        Raw_context_intf.T.project := Raw_context.project;
        Raw_context_intf.T.absolute_key := Raw_context.absolute_key;
        Raw_context_intf.T.consume_gas := Raw_context.consume_gas;
        Raw_context_intf.T.check_enough_gas := Raw_context.check_enough_gas;
        Raw_context_intf.T.description := Raw_context.description
      |}
      (let name := [ "sapling" ] in
      {|
        Storage_sigs.NAME.name := name
      |}).
  
  Module Next.
    Definition Storage :=
      Storage_functors.Make_single_data_storage Storage_functors.Registered
        Raw_context
        (let name := [ "next" ] in
        {|
          Storage_sigs.NAME.name := name
        |})
        {|
          Storage_sigs.VALUE.encoding :=
            Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.ID.encoding)
        |}.
    
    Definition incr (ctxt : Raw_context.t)
      : M=?
        (Raw_context.t *
          Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.ID.t)) :=
      let=? i := Storage.(Storage_sigs.Single_data_storage.get) ctxt in
      let=? ctxt :=
        Storage.(Storage_sigs.Single_data_storage.update) ctxt
          (Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.ID.next) i) in
      return=? (ctxt, i).
    
    Definition init_value (ctxt : Raw_context.t) : M=? Raw_context.t :=
      Storage.(Storage_sigs.Single_data_storage.init_value) ctxt
        Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.ID.init_value).
  End Next.
  
  Definition Index := Lazy_storage_kind.Sapling_state.Id.
  
  Definition rpc_arg
    : RPC_arg.arg Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.ID.t) :=
    Index.(Lazy_storage_kind.ID.rpc_arg).
  
  Definition Indexed_context :=
    Storage_functors.Make_indexed_subcontext
      (Storage_functors.Make_subcontext Storage_functors.Registered Raw_context
        (let name := [ "index" ] in
        {|
          Storage_sigs.NAME.name := name
        |}))
      (Make_index
        {|
          Storage_description.INDEX.to_path :=
            Index.(Lazy_storage_kind.ID.to_path);
          Storage_description.INDEX.of_path :=
            Index.(Lazy_storage_kind.ID.of_path);
          Storage_description.INDEX.path_length :=
            Index.(Lazy_storage_kind.ID.path_length);
          Storage_description.INDEX.rpc_arg :=
            Index.(Lazy_storage_kind.ID.rpc_arg);
          Storage_description.INDEX.encoding :=
            Index.(Lazy_storage_kind.ID.encoding);
          Storage_description.INDEX.compare :=
            Index.(Lazy_storage_kind.ID.compare)
        |}).
  
  Definition remove
    (ctxt : Raw_context.t)
    (n : Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.ID.t))
    : M= Raw_context.t :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.remove) ctxt n.
  
  Definition copy
    (ctxt : Raw_context.t)
    (from : Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.ID.t))
    (to_ : Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.ID.t))
    : M=? Raw_context.t :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.copy) ctxt from to_.
  
  Definition Total_bytes :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "total_bytes" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.Z.
  
  Definition Commitments_size :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
      (let name := [ "commitments_size" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.Int64.
  
  Definition Memo_size :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
      (let name := [ "memo_size" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Sapling_repr.Memo_size.encoding
      |}.
  
  Definition Commitments :
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage
      (t := Raw_context.t * id) (key := int64) (value := Sapling.Hash.t) :=
    let functor_result :=
      Storage_functors.Make_indexed_carbonated_data_storage
        (Storage_functors.Make_subcontext Storage_functors.Registered
          Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
          (let name := [ "commitments" ] in
          {|
            Storage_sigs.NAME.name := name
          |}))
        (Make_index
          (let t : Set := int64 in
          let rpc_arg :=
            let construct := Int64.to_string in
            let destruct (hash_value : string)
              : Pervasives.result int64 string :=
              Result.of_option "Cannot parse node position"
                (Int64.of_string_opt hash_value) in
            RPC_arg.make
              (Some "The position of a node in a sapling commitment tree")
              "sapling_node_position" destruct construct tt in
          let encoding :=
            Data_encoding.def "sapling_node_position"
              (Some "Sapling node position")
              (Some "The position of a node in a sapling commitment tree")
              Data_encoding.int64_value in
          let compare := Compare.Int64.(Compare.S.compare) in
          let path_length := 1 in
          let to_path (c : int64) (l_value : list string) : list string :=
            cons (Int64.to_string c) l_value in
          let of_path (function_parameter : list string) : option int64 :=
            match function_parameter with
            | cons c [] => Int64.of_string_opt c
            | _ => None
            end in
          {|
            Storage_description.INDEX.to_path := to_path;
            Storage_description.INDEX.of_path := of_path;
            Storage_description.INDEX.path_length := path_length;
            Storage_description.INDEX.rpc_arg := rpc_arg;
            Storage_description.INDEX.encoding := encoding;
            Storage_description.INDEX.compare := compare
          |}))
        {|
          Storage_sigs.VALUE.encoding := Sapling.Hash.encoding
        |} in
    {|
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.mem :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.mem);
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.get :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.get);
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.find :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.find);
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.update :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.update);
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.init_value :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.init_value);
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.add :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.add);
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.add_or_remove :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.add_or_remove);
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.remove_existing :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.remove_existing);
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.remove :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.remove)
    |}.
  
  Definition commitments_init
    (ctx : Raw_context.t)
    (id : Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.ID.t))
    : M= Raw_context.t :=
    let= '(ctx, _id) :=
      Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context_intf.T.remove)
        (ctx, id) [ "commitments" ] in
    return= ctx.
  
  Definition Ciphertexts :
    Storage_sigs.Non_iterable_indexed_carbonated_data_storage
      (t := Raw_context.t * id) (key := int64) (value := Sapling.Ciphertext.t)
    :=
    let functor_result :=
      Storage_functors.Make_indexed_carbonated_data_storage
        (Storage_functors.Make_subcontext Storage_functors.Registered
          Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
          (let name := [ "ciphertexts" ] in
          {|
            Storage_sigs.NAME.name := name
          |}))
        (Make_index
          (let t : Set := int64 in
          let rpc_arg :=
            let construct := Int64.to_string in
            let destruct (hash_value : string)
              : Pervasives.result int64 string :=
              Result.of_option "Cannot parse ciphertext position"
                (Int64.of_string_opt hash_value) in
            RPC_arg.make (Some "The position of a sapling ciphertext")
              "sapling_ciphertext_position" destruct construct tt in
          let encoding :=
            Data_encoding.def "sapling_ciphertext_position"
              (Some "Sapling ciphertext position")
              (Some "The position of a sapling ciphertext")
              Data_encoding.int64_value in
          let compare := Compare.Int64.(Compare.S.compare) in
          let path_length := 1 in
          let to_path (c : int64) (l_value : list string) : list string :=
            cons (Int64.to_string c) l_value in
          let of_path (function_parameter : list string) : option int64 :=
            match function_parameter with
            | cons c [] => Int64.of_string_opt c
            | _ => None
            end in
          {|
            Storage_description.INDEX.to_path := to_path;
            Storage_description.INDEX.of_path := of_path;
            Storage_description.INDEX.path_length := path_length;
            Storage_description.INDEX.rpc_arg := rpc_arg;
            Storage_description.INDEX.encoding := encoding;
            Storage_description.INDEX.compare := compare
          |}))
        {|
          Storage_sigs.VALUE.encoding := Sapling.Ciphertext.encoding
        |} in
    {|
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.mem :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.mem);
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.get :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.get);
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.find :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.find);
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.update :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.update);
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.init_value :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.init_value);
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.add :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.add);
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.add_or_remove :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.add_or_remove);
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.remove_existing :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.remove_existing);
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.remove :=
        functor_result.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.remove)
    |}.
  
  Definition ciphertexts_init
    (ctx : Raw_context.t)
    (id : Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.ID.t))
    : M= Raw_context.t :=
    let= '(ctx, _id) :=
      Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context_intf.T.remove)
        (ctx, id) [ "commitments" ] in
    return= ctx.
  
  Definition Nullifiers_size :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
      (let name := [ "nullifiers_size" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.Int64.
  
  Definition Nullifiers_ordered :
    Storage_sigs.Non_iterable_indexed_data_storage (t := Raw_context.t * id)
      (key := int64) (value := Sapling.Nullifier.t) :=
    let functor_result :=
      Storage_functors.Make_indexed_data_storage
        (Storage_functors.Make_subcontext Storage_functors.Registered
          Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
          (let name := [ "nullifiers_ordered" ] in
          {|
            Storage_sigs.NAME.name := name
          |}))
        (Make_index
          (let t : Set := int64 in
          let rpc_arg :=
            let construct := Int64.to_string in
            let destruct (hash_value : string)
              : Pervasives.result int64 string :=
              Result.of_option "Cannot parse nullifier position"
                (Int64.of_string_opt hash_value) in
            RPC_arg.make (Some "A sapling nullifier position")
              "sapling_nullifier_position" destruct construct tt in
          let encoding :=
            Data_encoding.def "sapling_nullifier_position"
              (Some "Sapling nullifier position")
              (Some "Sapling nullifier position") Data_encoding.int64_value in
          let compare := Compare.Int64.(Compare.S.compare) in
          let path_length := 1 in
          let to_path (c : int64) (l_value : list string) : list string :=
            cons (Int64.to_string c) l_value in
          let of_path (function_parameter : list string) : option int64 :=
            match function_parameter with
            | cons c [] => Int64.of_string_opt c
            | _ => None
            end in
          {|
            Storage_description.INDEX.to_path := to_path;
            Storage_description.INDEX.of_path := of_path;
            Storage_description.INDEX.path_length := path_length;
            Storage_description.INDEX.rpc_arg := rpc_arg;
            Storage_description.INDEX.encoding := encoding;
            Storage_description.INDEX.compare := compare
          |}))
        {|
          Storage_sigs.VALUE.encoding := Sapling.Nullifier.encoding
        |} in
    {|
      Storage_sigs.Non_iterable_indexed_data_storage.mem :=
        functor_result.(Storage_sigs.Indexed_data_storage.mem);
      Storage_sigs.Non_iterable_indexed_data_storage.get :=
        functor_result.(Storage_sigs.Indexed_data_storage.get);
      Storage_sigs.Non_iterable_indexed_data_storage.find :=
        functor_result.(Storage_sigs.Indexed_data_storage.find);
      Storage_sigs.Non_iterable_indexed_data_storage.update :=
        functor_result.(Storage_sigs.Indexed_data_storage.update);
      Storage_sigs.Non_iterable_indexed_data_storage.init_value :=
        functor_result.(Storage_sigs.Indexed_data_storage.init_value);
      Storage_sigs.Non_iterable_indexed_data_storage.add :=
        functor_result.(Storage_sigs.Indexed_data_storage.add);
      Storage_sigs.Non_iterable_indexed_data_storage.add_or_remove :=
        functor_result.(Storage_sigs.Indexed_data_storage.add_or_remove);
      Storage_sigs.Non_iterable_indexed_data_storage.remove_existing :=
        functor_result.(Storage_sigs.Indexed_data_storage.remove_existing);
      Storage_sigs.Non_iterable_indexed_data_storage.remove :=
        functor_result.(Storage_sigs.Indexed_data_storage.remove)
    |}.
  
  Definition Nullifiers_hashed :=
    Storage_functors.Make_carbonated_data_set_storage
      (Storage_functors.Make_subcontext Storage_functors.Registered
        Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
        (let name := [ "nullifiers_hashed" ] in
        {|
          Storage_sigs.NAME.name := name
        |}))
      (Make_index
        (let t : Set := Sapling.Nullifier.t in
        let encoding := Sapling.Nullifier.encoding in
        let of_string (hexstring : string)
          : Pervasives.result Sapling.Nullifier.t string :=
          let b_value := Hex.to_bytes (Hex.Hex hexstring) in
          Result.of_option "Cannot parse sapling nullifier"
            (Data_encoding.Binary.of_bytes_opt encoding b_value) in
        let to_string (nf : Sapling.Nullifier.t) : string :=
          let b_value := Data_encoding.Binary.to_bytes_exn None encoding nf in
          let 'Hex.Hex hexstring := Hex.of_bytes None b_value in
          hexstring in
        let rpc_arg :=
          RPC_arg.make (Some "A sapling nullifier") "sapling_nullifier"
            of_string to_string tt in
        let compare := Sapling.Nullifier.compare in
        let path_length := 1 in
        let to_path (c : Sapling.Nullifier.t) (l_value : list string)
          : list string :=
          cons (to_string c) l_value in
        let of_path (function_parameter : list string)
          : option Sapling.Nullifier.t :=
          match function_parameter with
          | cons c [] => Result.to_option (of_string c)
          | _ => None
          end in
        {|
          Storage_description.INDEX.to_path := to_path;
          Storage_description.INDEX.of_path := of_path;
          Storage_description.INDEX.path_length := path_length;
          Storage_description.INDEX.rpc_arg := rpc_arg;
          Storage_description.INDEX.encoding := encoding;
          Storage_description.INDEX.compare := compare
        |})).
  
  Definition nullifiers_init
    (ctx : Raw_context.t)
    (id : Lazy_storage_kind.Sapling_state.Id.(Lazy_storage_kind.ID.t))
    : M= Raw_context.t :=
    let= ctx :=
      Nullifiers_size.(Storage_sigs.Single_data_storage.add) (ctx, id)
        Int64.zero in
    let= '(ctx, id) :=
      Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context_intf.T.remove)
        (ctx, id) [ "nullifiers_ordered" ] in
    let= '(ctx, _id) :=
      Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context).(Raw_context_intf.T.remove)
        (ctx, id) [ "nullifiers_hashed" ] in
    return= ctx.
  
  Definition Roots :
    Storage_sigs.Non_iterable_indexed_data_storage (t := Raw_context.t * id)
      (key := int32) (value := Sapling.Hash.t) :=
    let functor_result :=
      Storage_functors.Make_indexed_data_storage
        (Storage_functors.Make_subcontext Storage_functors.Registered
          Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
          (let name := [ "roots" ] in
          {|
            Storage_sigs.NAME.name := name
          |}))
        (Make_index
          (let t : Set := int32 in
          let rpc_arg :=
            let construct := Int32.to_string in
            let destruct (hash_value : string)
              : Pervasives.result int32 string :=
              Result.of_option "Cannot parse nullifier position"
                (Int32.of_string_opt hash_value) in
            RPC_arg.make (Some "A sapling root") "sapling_root" destruct
              construct tt in
          let encoding :=
            Data_encoding.def "sapling_root" (Some "Sapling root")
              (Some "Sapling root") Data_encoding.int32_value in
          let compare := Compare.Int32.(Compare.S.compare) in
          let path_length := 1 in
          let to_path (c : int32) (l_value : list string) : list string :=
            cons (Int32.to_string c) l_value in
          let of_path (function_parameter : list string) : option int32 :=
            match function_parameter with
            | cons c [] => Int32.of_string_opt c
            | _ => None
            end in
          {|
            Storage_description.INDEX.to_path := to_path;
            Storage_description.INDEX.of_path := of_path;
            Storage_description.INDEX.path_length := path_length;
            Storage_description.INDEX.rpc_arg := rpc_arg;
            Storage_description.INDEX.encoding := encoding;
            Storage_description.INDEX.compare := compare
          |}))
        {|
          Storage_sigs.VALUE.encoding := Sapling.Hash.encoding
        |} in
    {|
      Storage_sigs.Non_iterable_indexed_data_storage.mem :=
        functor_result.(Storage_sigs.Indexed_data_storage.mem);
      Storage_sigs.Non_iterable_indexed_data_storage.get :=
        functor_result.(Storage_sigs.Indexed_data_storage.get);
      Storage_sigs.Non_iterable_indexed_data_storage.find :=
        functor_result.(Storage_sigs.Indexed_data_storage.find);
      Storage_sigs.Non_iterable_indexed_data_storage.update :=
        functor_result.(Storage_sigs.Indexed_data_storage.update);
      Storage_sigs.Non_iterable_indexed_data_storage.init_value :=
        functor_result.(Storage_sigs.Indexed_data_storage.init_value);
      Storage_sigs.Non_iterable_indexed_data_storage.add :=
        functor_result.(Storage_sigs.Indexed_data_storage.add);
      Storage_sigs.Non_iterable_indexed_data_storage.add_or_remove :=
        functor_result.(Storage_sigs.Indexed_data_storage.add_or_remove);
      Storage_sigs.Non_iterable_indexed_data_storage.remove_existing :=
        functor_result.(Storage_sigs.Indexed_data_storage.remove_existing);
      Storage_sigs.Non_iterable_indexed_data_storage.remove :=
        functor_result.(Storage_sigs.Indexed_data_storage.remove)
    |}.
  
  Definition Roots_pos :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
      (let name := [ "roots_pos" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.Int32.
  
  Definition Roots_level :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
      (let name := [ "roots_level" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Raw_level_repr.encoding
      |}.
End Sapling.

Module Public_key_hash.
  (** Inclusion of the module [Signature.Public_key_hash] *)
  Definition t := Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.t).
  
  Definition pp := Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp).
  
  Definition pp_short :=
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp_short).
  
  Definition op_eq :=
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.op_eq).
  
  Definition op_ltgt :=
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.op_ltgt).
  
  Definition op_lt :=
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.op_lt).
  
  Definition op_lteq :=
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.op_lteq).
  
  Definition op_gteq :=
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.op_gteq).
  
  Definition op_gt :=
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.op_gt).
  
  Definition compare :=
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.compare).
  
  Definition equal :=
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.equal).
  
  Definition max := Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.max).
  
  Definition min := Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.min).
  
  Definition size_value :=
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.size_value).
  
  Definition to_bytes :=
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.to_bytes).
  
  Definition of_bytes_opt :=
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.of_bytes_opt).
  
  Definition of_bytes_exn :=
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.of_bytes_exn).
  
  Definition to_b58check :=
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.to_b58check).
  
  Definition to_short_b58check :=
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.to_short_b58check).
  
  Definition of_b58check_exn :=
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.of_b58check_exn).
  
  Definition of_b58check_opt :=
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.of_b58check_opt).
  
  Definition b58check_encoding :=
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.b58check_encoding).
  
  Definition encoding :=
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding).
  
  Definition rpc_arg :=
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.rpc_arg).
  
  Definition zero :=
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.zero).
  
  Definition Path_Ed25519 :=
    Path_encoding.Make_hex
      {|
        Path_encoding.ENCODING.to_bytes :=
          Ed25519.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.to_bytes);
        Path_encoding.ENCODING.of_bytes_opt :=
          Ed25519.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.of_bytes_opt)
      |}.
  
  Definition Path_Secp256k1 :=
    Path_encoding.Make_hex
      {|
        Path_encoding.ENCODING.to_bytes :=
          Secp256k1.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.to_bytes);
        Path_encoding.ENCODING.of_bytes_opt :=
          Secp256k1.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.of_bytes_opt)
      |}.
  
  Definition Path_P256 :=
    Path_encoding.Make_hex
      {|
        Path_encoding.ENCODING.to_bytes :=
          P256.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.to_bytes);
        Path_encoding.ENCODING.of_bytes_opt :=
          P256.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.of_bytes_opt)
      |}.
  
  Definition to_path
    (key_value : Signature.public_key_hash) (l_value : list string)
    : list string :=
    match key_value with
    | Signature.Ed25519Hash h =>
      match Path_Ed25519.(Path_encoding.S.to_path) h l_value with
      | cons s [] => [ "ed25519"; s ]
      | _ =>
        (* ❌ Assert instruction is not handled. *)
        assert (list string) false
      end
    | Signature.Secp256k1Hash h =>
      match Path_Secp256k1.(Path_encoding.S.to_path) h l_value with
      | cons s [] => [ "secp256k1"; s ]
      | _ =>
        (* ❌ Assert instruction is not handled. *)
        assert (list string) false
      end
    | Signature.P256Hash h =>
      match Path_P256.(Path_encoding.S.to_path) h l_value with
      | cons s [] => [ "p256"; s ]
      | _ =>
        (* ❌ Assert instruction is not handled. *)
        assert (list string) false
      end
    end.
  
  Definition of_path (function_parameter : list string)
    : option Signature.public_key_hash :=
    match function_parameter with
    | cons "ed25519" rest =>
      match Path_Ed25519.(Path_encoding.S.of_path) rest with
      | Some pkh => Some (Signature.Ed25519Hash pkh)
      | None => None
      end
    | cons "secp256k1" rest =>
      match Path_Secp256k1.(Path_encoding.S.of_path) rest with
      | Some pkh => Some (Signature.Secp256k1Hash pkh)
      | None => None
      end
    | cons "p256" rest =>
      match Path_P256.(Path_encoding.S.of_path) rest with
      | Some pkh => Some (Signature.P256Hash pkh)
      | None => None
      end
    | _ => None
    end.
  
  Definition path_length : int :=
    let l1 : int :=
      Path_Ed25519.(Path_encoding.S.path_length)
    in let l2 : int :=
      Path_Secp256k1.(Path_encoding.S.path_length)
    in let l3 : int :=
      Path_P256.(Path_encoding.S.path_length) in
    let '_ :=
      (* ❌ Assert instruction is not handled. *)
      assert unit
        match (l1, l2, l3) with
        | (1, 1, 1) => true
        | _ => false
        end in
    2.
End Public_key_hash.

Definition Public_key_hash_index :=
  Make_index
    {|
      Storage_description.INDEX.to_path := Public_key_hash.to_path;
      Storage_description.INDEX.of_path := Public_key_hash.of_path;
      Storage_description.INDEX.path_length := Public_key_hash.path_length;
      Storage_description.INDEX.rpc_arg := Public_key_hash.rpc_arg;
      Storage_description.INDEX.encoding := Public_key_hash.encoding;
      Storage_description.INDEX.compare := Public_key_hash.compare
    |}.

Module Protocol_hash_with_path_encoding.
  Include Protocol_hash.
  
  Definition Path_encoding_Make_hex_include :=
    Path_encoding.Make_hex
      {|
        Path_encoding.ENCODING.to_bytes := Protocol_hash.to_bytes;
        Path_encoding.ENCODING.of_bytes_opt := Protocol_hash.of_bytes_opt
      |}.
  
  (** Inclusion of the module [Path_encoding_Make_hex_include] *)
  Definition to_path :=
    Path_encoding_Make_hex_include.(Path_encoding.S.to_path).
  
  Definition of_path :=
    Path_encoding_Make_hex_include.(Path_encoding.S.of_path).
  
  Definition path_length :=
    Path_encoding_Make_hex_include.(Path_encoding.S.path_length).
End Protocol_hash_with_path_encoding.

Definition Delegates :=
  Storage_functors.Make_data_set_storage
    (Storage_functors.Make_subcontext Storage_functors.Registered
      {|
        Raw_context_intf.T.mem := Raw_context.mem;
        Raw_context_intf.T.mem_tree := Raw_context.mem_tree;
        Raw_context_intf.T.get := Raw_context.get;
        Raw_context_intf.T.get_tree := Raw_context.get_tree;
        Raw_context_intf.T.find := Raw_context.find;
        Raw_context_intf.T.find_tree := Raw_context.find_tree;
        Raw_context_intf.T.list_value := Raw_context.list_value;
        Raw_context_intf.T.init_value := Raw_context.init_value;
        Raw_context_intf.T.init_tree := Raw_context.init_tree;
        Raw_context_intf.T.update := Raw_context.update;
        Raw_context_intf.T.update_tree := Raw_context.update_tree;
        Raw_context_intf.T.add := Raw_context.add;
        Raw_context_intf.T.add_tree := Raw_context.add_tree;
        Raw_context_intf.T.remove := Raw_context.remove;
        Raw_context_intf.T.remove_existing := Raw_context.remove_existing;
        Raw_context_intf.T.remove_existing_tree :=
          Raw_context.remove_existing_tree;
        Raw_context_intf.T.add_or_remove := Raw_context.add_or_remove;
        Raw_context_intf.T.add_or_remove_tree := Raw_context.add_or_remove_tree;
        Raw_context_intf.T.fold _ := Raw_context.fold;
        Raw_context_intf.T.Tree := Raw_context.Tree;
        Raw_context_intf.T.project := Raw_context.project;
        Raw_context_intf.T.absolute_key := Raw_context.absolute_key;
        Raw_context_intf.T.consume_gas := Raw_context.consume_gas;
        Raw_context_intf.T.check_enough_gas := Raw_context.check_enough_gas;
        Raw_context_intf.T.description := Raw_context.description
      |}
      (let name := [ "delegates" ] in
      {|
        Storage_sigs.NAME.name := name
      |})) Public_key_hash_index.

Definition Active_delegates_with_rolls :=
  Storage_functors.Make_data_set_storage
    (Storage_functors.Make_subcontext Storage_functors.Registered
      {|
        Raw_context_intf.T.mem := Raw_context.mem;
        Raw_context_intf.T.mem_tree := Raw_context.mem_tree;
        Raw_context_intf.T.get := Raw_context.get;
        Raw_context_intf.T.get_tree := Raw_context.get_tree;
        Raw_context_intf.T.find := Raw_context.find;
        Raw_context_intf.T.find_tree := Raw_context.find_tree;
        Raw_context_intf.T.list_value := Raw_context.list_value;
        Raw_context_intf.T.init_value := Raw_context.init_value;
        Raw_context_intf.T.init_tree := Raw_context.init_tree;
        Raw_context_intf.T.update := Raw_context.update;
        Raw_context_intf.T.update_tree := Raw_context.update_tree;
        Raw_context_intf.T.add := Raw_context.add;
        Raw_context_intf.T.add_tree := Raw_context.add_tree;
        Raw_context_intf.T.remove := Raw_context.remove;
        Raw_context_intf.T.remove_existing := Raw_context.remove_existing;
        Raw_context_intf.T.remove_existing_tree :=
          Raw_context.remove_existing_tree;
        Raw_context_intf.T.add_or_remove := Raw_context.add_or_remove;
        Raw_context_intf.T.add_or_remove_tree := Raw_context.add_or_remove_tree;
        Raw_context_intf.T.fold _ := Raw_context.fold;
        Raw_context_intf.T.Tree := Raw_context.Tree;
        Raw_context_intf.T.project := Raw_context.project;
        Raw_context_intf.T.absolute_key := Raw_context.absolute_key;
        Raw_context_intf.T.consume_gas := Raw_context.consume_gas;
        Raw_context_intf.T.check_enough_gas := Raw_context.check_enough_gas;
        Raw_context_intf.T.description := Raw_context.description
      |}
      (let name := [ "active_delegates_with_rolls" ] in
      {|
        Storage_sigs.NAME.name := name
      |})) Public_key_hash_index.

Definition Delegates_with_frozen_balance_index :=
  Storage_functors.Make_indexed_subcontext
    (Storage_functors.Make_subcontext Storage_functors.Registered
      {|
        Raw_context_intf.T.mem := Raw_context.mem;
        Raw_context_intf.T.mem_tree := Raw_context.mem_tree;
        Raw_context_intf.T.get := Raw_context.get;
        Raw_context_intf.T.get_tree := Raw_context.get_tree;
        Raw_context_intf.T.find := Raw_context.find;
        Raw_context_intf.T.find_tree := Raw_context.find_tree;
        Raw_context_intf.T.list_value := Raw_context.list_value;
        Raw_context_intf.T.init_value := Raw_context.init_value;
        Raw_context_intf.T.init_tree := Raw_context.init_tree;
        Raw_context_intf.T.update := Raw_context.update;
        Raw_context_intf.T.update_tree := Raw_context.update_tree;
        Raw_context_intf.T.add := Raw_context.add;
        Raw_context_intf.T.add_tree := Raw_context.add_tree;
        Raw_context_intf.T.remove := Raw_context.remove;
        Raw_context_intf.T.remove_existing := Raw_context.remove_existing;
        Raw_context_intf.T.remove_existing_tree :=
          Raw_context.remove_existing_tree;
        Raw_context_intf.T.add_or_remove := Raw_context.add_or_remove;
        Raw_context_intf.T.add_or_remove_tree := Raw_context.add_or_remove_tree;
        Raw_context_intf.T.fold _ := Raw_context.fold;
        Raw_context_intf.T.Tree := Raw_context.Tree;
        Raw_context_intf.T.project := Raw_context.project;
        Raw_context_intf.T.absolute_key := Raw_context.absolute_key;
        Raw_context_intf.T.consume_gas := Raw_context.consume_gas;
        Raw_context_intf.T.check_enough_gas := Raw_context.check_enough_gas;
        Raw_context_intf.T.description := Raw_context.description
      |}
      (let name := [ "delegates_with_frozen_balance" ] in
      {|
        Storage_sigs.NAME.name := name
      |})) (Make_index Cycle_repr.Index).

Definition Delegates_with_frozen_balance :=
  Storage_functors.Make_data_set_storage
    Delegates_with_frozen_balance_index.(Storage_sigs.Indexed_raw_context.Raw_context)
    Public_key_hash_index.

Module Cycle.
  Definition Indexed_context :=
    Storage_functors.Make_indexed_subcontext
      (Storage_functors.Make_subcontext Storage_functors.Registered
        {|
          Raw_context_intf.T.mem := Raw_context.mem;
          Raw_context_intf.T.mem_tree := Raw_context.mem_tree;
          Raw_context_intf.T.get := Raw_context.get;
          Raw_context_intf.T.get_tree := Raw_context.get_tree;
          Raw_context_intf.T.find := Raw_context.find;
          Raw_context_intf.T.find_tree := Raw_context.find_tree;
          Raw_context_intf.T.list_value := Raw_context.list_value;
          Raw_context_intf.T.init_value := Raw_context.init_value;
          Raw_context_intf.T.init_tree := Raw_context.init_tree;
          Raw_context_intf.T.update := Raw_context.update;
          Raw_context_intf.T.update_tree := Raw_context.update_tree;
          Raw_context_intf.T.add := Raw_context.add;
          Raw_context_intf.T.add_tree := Raw_context.add_tree;
          Raw_context_intf.T.remove := Raw_context.remove;
          Raw_context_intf.T.remove_existing := Raw_context.remove_existing;
          Raw_context_intf.T.remove_existing_tree :=
            Raw_context.remove_existing_tree;
          Raw_context_intf.T.add_or_remove := Raw_context.add_or_remove;
          Raw_context_intf.T.add_or_remove_tree :=
            Raw_context.add_or_remove_tree;
          Raw_context_intf.T.fold _ := Raw_context.fold;
          Raw_context_intf.T.Tree := Raw_context.Tree;
          Raw_context_intf.T.project := Raw_context.project;
          Raw_context_intf.T.absolute_key := Raw_context.absolute_key;
          Raw_context_intf.T.consume_gas := Raw_context.consume_gas;
          Raw_context_intf.T.check_enough_gas := Raw_context.check_enough_gas;
          Raw_context_intf.T.description := Raw_context.description
        |}
        (let name := [ "cycle" ] in
        {|
          Storage_sigs.NAME.name := name
        |})) (Make_index Cycle_repr.Index).
  
  Definition Last_roll :=
    Storage_functors.Make_indexed_data_storage
      (Storage_functors.Make_subcontext Storage_functors.Registered
        Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
        (let name := [ "last_roll" ] in
        {|
          Storage_sigs.NAME.name := name
        |})) Int31_index
      {|
        Storage_sigs.VALUE.encoding := Roll_repr.encoding
      |}.
  
  Definition Roll_snapshot :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "roll_snapshot" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.UInt16.
  
  Module unrevealed_nonce.
    Record record : Set := Build {
      nonce_hash : Nonce_hash.t;
      delegate : Signature.public_key_hash;
      rewards : Tez_repr.t;
      fees : Tez_repr.t }.
    Definition with_nonce_hash nonce_hash (r : record) :=
      Build nonce_hash r.(delegate) r.(rewards) r.(fees).
    Definition with_delegate delegate (r : record) :=
      Build r.(nonce_hash) delegate r.(rewards) r.(fees).
    Definition with_rewards rewards (r : record) :=
      Build r.(nonce_hash) r.(delegate) rewards r.(fees).
    Definition with_fees fees (r : record) :=
      Build r.(nonce_hash) r.(delegate) r.(rewards) fees.
  End unrevealed_nonce.
  Definition unrevealed_nonce := unrevealed_nonce.record.
  
  Inductive nonce_status : Set :=
  | Unrevealed : unrevealed_nonce -> nonce_status
  | Revealed : Seed_repr.nonce -> nonce_status.
  
  Definition nonce_status_encoding : Data_encoding.encoding nonce_status :=
    Data_encoding.union None
      [
        Data_encoding.case_value "Unrevealed" None (Data_encoding.Tag 0)
          (Data_encoding.tup4 Nonce_hash.encoding
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)
            Tez_repr.encoding Tez_repr.encoding)
          (fun (function_parameter : nonce_status) =>
            match function_parameter with
            |
              Unrevealed {|
                unrevealed_nonce.nonce_hash := nonce_hash;
                  unrevealed_nonce.delegate := delegate;
                  unrevealed_nonce.rewards := rewards;
                  unrevealed_nonce.fees := fees
                  |} =>
              Some (nonce_hash, delegate, rewards, fees)
            | _ => None
            end)
          (fun (function_parameter :
            Nonce_hash.t * Signature.public_key_hash * Tez_repr.t *
              Tez_repr.t) =>
            let '(nonce_hash, delegate, rewards, fees) :=
              function_parameter in
            Unrevealed
              {| unrevealed_nonce.nonce_hash := nonce_hash;
                unrevealed_nonce.delegate := delegate;
                unrevealed_nonce.rewards := rewards;
                unrevealed_nonce.fees := fees |});
        Data_encoding.case_value "Revealed" None (Data_encoding.Tag 1)
          Seed_repr.nonce_encoding
          (fun (function_parameter : nonce_status) =>
            match function_parameter with
            | Revealed nonce_value => Some nonce_value
            | _ => None
            end)
          (fun (nonce_value : Seed_repr.nonce) => Revealed nonce_value)
      ].
  
  Definition Nonce :=
    Storage_functors.Make_indexed_data_storage
      (Storage_functors.Make_subcontext Storage_functors.Registered
        Indexed_context.(Storage_sigs.Indexed_raw_context.Raw_context)
        (let name := [ "nonces" ] in
        {|
          Storage_sigs.NAME.name := name
        |})) (Make_index Raw_level_repr.Index)
      (let t : Set := nonce_status in
      let encoding := nonce_status_encoding in
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}).
  
  Definition Seed :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "random_seed" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      (let t : Set := Seed_repr.seed in
      let encoding := Seed_repr.seed_encoding in
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}).
End Cycle.

Module Roll.
  Definition Raw_context :=
    Storage_functors.Make_subcontext Storage_functors.Registered
      {|
        Raw_context_intf.T.mem := Raw_context.mem;
        Raw_context_intf.T.mem_tree := Raw_context.mem_tree;
        Raw_context_intf.T.get := Raw_context.get;
        Raw_context_intf.T.get_tree := Raw_context.get_tree;
        Raw_context_intf.T.find := Raw_context.find;
        Raw_context_intf.T.find_tree := Raw_context.find_tree;
        Raw_context_intf.T.list_value := Raw_context.list_value;
        Raw_context_intf.T.init_value := Raw_context.init_value;
        Raw_context_intf.T.init_tree := Raw_context.init_tree;
        Raw_context_intf.T.update := Raw_context.update;
        Raw_context_intf.T.update_tree := Raw_context.update_tree;
        Raw_context_intf.T.add := Raw_context.add;
        Raw_context_intf.T.add_tree := Raw_context.add_tree;
        Raw_context_intf.T.remove := Raw_context.remove;
        Raw_context_intf.T.remove_existing := Raw_context.remove_existing;
        Raw_context_intf.T.remove_existing_tree :=
          Raw_context.remove_existing_tree;
        Raw_context_intf.T.add_or_remove := Raw_context.add_or_remove;
        Raw_context_intf.T.add_or_remove_tree := Raw_context.add_or_remove_tree;
        Raw_context_intf.T.fold _ := Raw_context.fold;
        Raw_context_intf.T.Tree := Raw_context.Tree;
        Raw_context_intf.T.project := Raw_context.project;
        Raw_context_intf.T.absolute_key := Raw_context.absolute_key;
        Raw_context_intf.T.consume_gas := Raw_context.consume_gas;
        Raw_context_intf.T.check_enough_gas := Raw_context.check_enough_gas;
        Raw_context_intf.T.description := Raw_context.description
      |}
      (let name := [ "rolls" ] in
      {|
        Storage_sigs.NAME.name := name
      |}).
  
  Definition Indexed_context :=
    Storage_functors.Make_indexed_subcontext
      (Storage_functors.Make_subcontext Storage_functors.Registered Raw_context
        (let name := [ "index" ] in
        {|
          Storage_sigs.NAME.name := name
        |})) (Make_index Roll_repr.Index).
  
  Definition Next :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      Raw_context
      (let name := [ "next" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Roll_repr.encoding
      |}.
  
  Definition Limbo :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      Raw_context
      (let name := [ "limbo" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Roll_repr.encoding
      |}.
  
  Definition Delegate_roll_list :=
    Storage_functors.Wrap_indexed_data_storage Contract.Roll_list
      (let t : Set := Signature.public_key_hash in
      let wrap := Contract_repr.implicit_contract in
      let unwrap := Contract_repr.is_implicit in
      {|
        Storage_functors.WRAPPER.wrap := wrap;
        Storage_functors.WRAPPER.unwrap := unwrap
      |}).
  
  Definition Successor :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.Make_map)
      (let name := [ "successor" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Roll_repr.encoding
      |}.
  
  Definition Delegate_change :=
    Storage_functors.Wrap_indexed_data_storage Contract.Change
      (let t : Set := Signature.public_key_hash in
      let wrap := Contract_repr.implicit_contract in
      let unwrap := Contract_repr.is_implicit in
      {|
        Storage_functors.WRAPPER.wrap := wrap;
        Storage_functors.WRAPPER.unwrap := unwrap
      |}).
  
  Module Snapshoted_owner_index.
    Definition t : Set := Cycle_repr.t * int.
    
    Definition path_length : int :=
      Cycle_repr.Index.(Storage_description.INDEX.path_length) +i 1.
    
    Definition to_path (function_parameter : Cycle_repr.t * int)
      : list string -> list string :=
      let '(c, n) := function_parameter in
      fun (s : list string) =>
        Cycle_repr.Index.(Storage_description.INDEX.to_path) c
          (cons (Pervasives.string_of_int n) s).
    
    Definition of_path (l_value : list string) : option (Cycle_repr.t * int) :=
      match
        Misc.take Cycle_repr.Index.(Storage_description.INDEX.path_length)
          l_value with
      | (None | Some (_, ([] | cons _ (cons _ _)))) => None
      | Some (l1, cons l2 []) =>
        match
          ((Cycle_repr.Index.(Storage_description.INDEX.of_path) l1),
            (Pervasives.int_of_string_opt l2)) with
        | ((None, _) | (_, None)) => None
        | (Some c, Some i) => Some (c, i)
        end
      end.
    
    Definition ipath (a : Set) : Set := (a * Cycle_repr.t) * int.
    
    Definition left_args : Storage_description.args :=
      Storage_description.One
        {| Storage_description.args.One.rpc_arg := Cycle_repr.rpc_arg;
          Storage_description.args.One.encoding := Cycle_repr.encoding;
          Storage_description.args.One.compare := Cycle_repr.compare |}.
    
    Definition right_args : Storage_description.args :=
      Storage_description.One
        {| Storage_description.args.One.rpc_arg := RPC_arg.int_value;
          Storage_description.args.One.encoding := Data_encoding.int31;
          Storage_description.args.One.compare :=
            Compare.Int.(Compare.S.compare) |}.
    
    Definition args : Storage_description.args :=
      Storage_description.Pair left_args right_args.
    
    Definition module : Storage_functors.INDEX.signature (ipath := ipath) := {|
        Storage_functors.INDEX.to_path := to_path;
        Storage_functors.INDEX.of_path := of_path;
        Storage_functors.INDEX.path_length := path_length;
        Storage_functors.INDEX.args := args
      |}.
  End Snapshoted_owner_index.
  Definition Snapshoted_owner_index
    : Storage_functors.INDEX (t := Cycle_repr.t * int) (ipath := _) :=
    Snapshoted_owner_index.module.
  
  Definition Owner :=
    Storage_functors.Make_indexed_data_snapshotable_storage
      (Storage_functors.Make_subcontext Storage_functors.Registered Raw_context
        (let name := [ "owner" ] in
        {|
          Storage_sigs.NAME.name := name
        |})) Snapshoted_owner_index (Make_index Roll_repr.Index)
      {|
        Storage_sigs.VALUE.encoding :=
          Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.encoding)
      |}.
  
  Definition Snapshot_for_cycle := Cycle.Roll_snapshot.
  
  Definition Last_for_snapshot := Cycle.Last_roll.
  
  Definition clear : Raw_context.t -> M= Raw_context.t :=
    Indexed_context.(Storage_sigs.Indexed_raw_context.clear).
End Roll.

Module Vote.
  Definition Raw_context :=
    Storage_functors.Make_subcontext Storage_functors.Registered
      {|
        Raw_context_intf.T.mem := Raw_context.mem;
        Raw_context_intf.T.mem_tree := Raw_context.mem_tree;
        Raw_context_intf.T.get := Raw_context.get;
        Raw_context_intf.T.get_tree := Raw_context.get_tree;
        Raw_context_intf.T.find := Raw_context.find;
        Raw_context_intf.T.find_tree := Raw_context.find_tree;
        Raw_context_intf.T.list_value := Raw_context.list_value;
        Raw_context_intf.T.init_value := Raw_context.init_value;
        Raw_context_intf.T.init_tree := Raw_context.init_tree;
        Raw_context_intf.T.update := Raw_context.update;
        Raw_context_intf.T.update_tree := Raw_context.update_tree;
        Raw_context_intf.T.add := Raw_context.add;
        Raw_context_intf.T.add_tree := Raw_context.add_tree;
        Raw_context_intf.T.remove := Raw_context.remove;
        Raw_context_intf.T.remove_existing := Raw_context.remove_existing;
        Raw_context_intf.T.remove_existing_tree :=
          Raw_context.remove_existing_tree;
        Raw_context_intf.T.add_or_remove := Raw_context.add_or_remove;
        Raw_context_intf.T.add_or_remove_tree := Raw_context.add_or_remove_tree;
        Raw_context_intf.T.fold _ := Raw_context.fold;
        Raw_context_intf.T.Tree := Raw_context.Tree;
        Raw_context_intf.T.project := Raw_context.project;
        Raw_context_intf.T.absolute_key := Raw_context.absolute_key;
        Raw_context_intf.T.consume_gas := Raw_context.consume_gas;
        Raw_context_intf.T.check_enough_gas := Raw_context.check_enough_gas;
        Raw_context_intf.T.description := Raw_context.description
      |}
      (let name := [ "votes" ] in
      {|
        Storage_sigs.NAME.name := name
      |}).
  
  Definition Pred_period_kind :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      Raw_context
      (let name := [ "pred_period_kind" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      (let t : Set := Voting_period_repr.kind in
      let encoding := Voting_period_repr.kind_encoding in
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}).
  
  Definition Current_period :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      Raw_context
      (let name := [ "current_period" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      (let t : Set := Voting_period_repr.t in
      let encoding := Voting_period_repr.encoding in
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}).
  
  Definition Participation_ema :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      Raw_context
      (let name := [ "participation_ema" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.Int32.
  
  Definition Current_proposal :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      Raw_context
      (let name := [ "current_proposal" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Protocol_hash.encoding
      |}.
  
  Definition Listings_size :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      Raw_context
      (let name := [ "listings_size" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.Int32.
  
  Definition Listings :=
    Storage_functors.Make_indexed_data_storage
      (Storage_functors.Make_subcontext Storage_functors.Registered Raw_context
        (let name := [ "listings" ] in
        {|
          Storage_sigs.NAME.name := name
        |})) Public_key_hash_index Encoding.Int32.
  
  Definition Proposals :=
    Storage_functors.Make_data_set_storage
      (Storage_functors.Make_subcontext Storage_functors.Registered Raw_context
        (let name := [ "proposals" ] in
        {|
          Storage_sigs.NAME.name := name
        |}))
      (Storage_functors.Pair
        (Make_index
          {|
            Storage_description.INDEX.to_path :=
              Protocol_hash_with_path_encoding.to_path;
            Storage_description.INDEX.of_path :=
              Protocol_hash_with_path_encoding.of_path;
            Storage_description.INDEX.path_length :=
              Protocol_hash_with_path_encoding.path_length;
            Storage_description.INDEX.rpc_arg :=
              Protocol_hash_with_path_encoding.rpc_arg;
            Storage_description.INDEX.encoding :=
              Protocol_hash_with_path_encoding.encoding;
            Storage_description.INDEX.compare :=
              Protocol_hash_with_path_encoding.compare
          |}) Public_key_hash_index).
  
  Definition Proposals_count :=
    Storage_functors.Make_indexed_data_storage
      (Storage_functors.Make_subcontext Storage_functors.Registered Raw_context
        (let name := [ "proposals_count" ] in
        {|
          Storage_sigs.NAME.name := name
        |})) Public_key_hash_index Encoding.UInt16.
  
  Definition Ballots :=
    Storage_functors.Make_indexed_data_storage
      (Storage_functors.Make_subcontext Storage_functors.Registered Raw_context
        (let name := [ "ballots" ] in
        {|
          Storage_sigs.NAME.name := name
        |})) Public_key_hash_index
      (let t : Set := Vote_repr.ballot in
      let encoding := Vote_repr.ballot_encoding in
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}).
End Vote.

Module FOR_CYCLE.
  Record signature : Set := {
    init_value :
      Raw_context.t -> Cycle_repr.t -> Seed_repr.seed -> M=? Raw_context.t;
    get : Raw_context.t -> Cycle_repr.t -> M=? Seed_repr.seed;
    remove_existing : Raw_context.t -> Cycle_repr.t -> M=? Raw_context.t;
  }.
End FOR_CYCLE.
Definition FOR_CYCLE := FOR_CYCLE.signature.

Module Seed.
  Definition unrevealed_nonce : Set := Cycle.unrevealed_nonce.
  
  Definition nonce_status : Set := Cycle.nonce_status.
  
  Module Nonce.
    Definition context : Set := Raw_context.t.
    
    Definition mem (ctxt : Raw_context.t) (l_value : Level_repr.t) : M= bool :=
      Cycle.Nonce.(Storage_sigs.Indexed_data_storage.mem)
        (ctxt, l_value.(Level_repr.t.cycle)) l_value.(Level_repr.t.level).
    
    Definition get (ctxt : Raw_context.t) (l_value : Level_repr.t)
      : M=? Cycle.nonce_status :=
      Cycle.Nonce.(Storage_sigs.Indexed_data_storage.get)
        (ctxt, l_value.(Level_repr.t.cycle)) l_value.(Level_repr.t.level).
    
    Definition find (ctxt : Raw_context.t) (l_value : Level_repr.t)
      : M=? (option Cycle.nonce_status) :=
      Cycle.Nonce.(Storage_sigs.Indexed_data_storage.find)
        (ctxt, l_value.(Level_repr.t.cycle)) l_value.(Level_repr.t.level).
    
    Definition update
      (ctxt : Raw_context.t) (l_value : Level_repr.t) (v : Cycle.nonce_status)
      : M=? Raw_context.t :=
      Cycle.Nonce.(Storage_sigs.Indexed_data_storage.update)
        (ctxt, l_value.(Level_repr.t.cycle)) l_value.(Level_repr.t.level) v.
    
    Definition init_value
      (ctxt : Raw_context.t) (l_value : Level_repr.t) (v : Cycle.nonce_status)
      : M=? Raw_context.t :=
      Cycle.Nonce.(Storage_sigs.Indexed_data_storage.init_value)
        (ctxt, l_value.(Level_repr.t.cycle)) l_value.(Level_repr.t.level) v.
    
    Definition add
      (ctxt : Raw_context.t) (l_value : Level_repr.t) (v : Cycle.nonce_status)
      : M= Raw_context.t :=
      Cycle.Nonce.(Storage_sigs.Indexed_data_storage.add)
        (ctxt, l_value.(Level_repr.t.cycle)) l_value.(Level_repr.t.level) v.
    
    Definition add_or_remove
      (ctxt : Raw_context.t) (l_value : Level_repr.t)
      (v : option Cycle.nonce_status) : M= Raw_context.t :=
      Cycle.Nonce.(Storage_sigs.Indexed_data_storage.add_or_remove)
        (ctxt, l_value.(Level_repr.t.cycle)) l_value.(Level_repr.t.level) v.
    
    Definition remove_existing (ctxt : Raw_context.t) (l_value : Level_repr.t)
      : M=? Raw_context.t :=
      Cycle.Nonce.(Storage_sigs.Indexed_data_storage.remove_existing)
        (ctxt, l_value.(Level_repr.t.cycle)) l_value.(Level_repr.t.level).
    
    Definition remove (ctxt : Raw_context.t) (l_value : Level_repr.t)
      : M= Raw_context.t :=
      Cycle.Nonce.(Storage_sigs.Indexed_data_storage.remove)
        (ctxt, l_value.(Level_repr.t.cycle)) l_value.(Level_repr.t.level).
    
    Definition module :=
      {|
        Storage_sigs.Non_iterable_indexed_data_storage.mem := mem;
        Storage_sigs.Non_iterable_indexed_data_storage.get := get;
        Storage_sigs.Non_iterable_indexed_data_storage.find := find;
        Storage_sigs.Non_iterable_indexed_data_storage.update := update;
        Storage_sigs.Non_iterable_indexed_data_storage.init_value := init_value;
        Storage_sigs.Non_iterable_indexed_data_storage.add := add;
        Storage_sigs.Non_iterable_indexed_data_storage.add_or_remove :=
          add_or_remove;
        Storage_sigs.Non_iterable_indexed_data_storage.remove_existing :=
          remove_existing;
        Storage_sigs.Non_iterable_indexed_data_storage.remove := remove
      |}.
  End Nonce.
  Definition Nonce
    : Storage_sigs.Non_iterable_indexed_data_storage (t := Raw_context.t)
      (key := Level_repr.t) (value := nonce_status) := Nonce.module.
  
  Definition For_cycle : FOR_CYCLE :=
    {|
      FOR_CYCLE.init_value :=
        Cycle.Seed.(Storage_sigs.Indexed_data_storage.init_value);
      FOR_CYCLE.get := Cycle.Seed.(Storage_sigs.Indexed_data_storage.get);
      FOR_CYCLE.remove_existing :=
        Cycle.Seed.(Storage_sigs.Indexed_data_storage.remove_existing)
    |}.
End Seed.

Definition Commitments :=
  Storage_functors.Make_indexed_data_storage
    (Storage_functors.Make_subcontext Storage_functors.Registered
      {|
        Raw_context_intf.T.mem := Raw_context.mem;
        Raw_context_intf.T.mem_tree := Raw_context.mem_tree;
        Raw_context_intf.T.get := Raw_context.get;
        Raw_context_intf.T.get_tree := Raw_context.get_tree;
        Raw_context_intf.T.find := Raw_context.find;
        Raw_context_intf.T.find_tree := Raw_context.find_tree;
        Raw_context_intf.T.list_value := Raw_context.list_value;
        Raw_context_intf.T.init_value := Raw_context.init_value;
        Raw_context_intf.T.init_tree := Raw_context.init_tree;
        Raw_context_intf.T.update := Raw_context.update;
        Raw_context_intf.T.update_tree := Raw_context.update_tree;
        Raw_context_intf.T.add := Raw_context.add;
        Raw_context_intf.T.add_tree := Raw_context.add_tree;
        Raw_context_intf.T.remove := Raw_context.remove;
        Raw_context_intf.T.remove_existing := Raw_context.remove_existing;
        Raw_context_intf.T.remove_existing_tree :=
          Raw_context.remove_existing_tree;
        Raw_context_intf.T.add_or_remove := Raw_context.add_or_remove;
        Raw_context_intf.T.add_or_remove_tree := Raw_context.add_or_remove_tree;
        Raw_context_intf.T.fold _ := Raw_context.fold;
        Raw_context_intf.T.Tree := Raw_context.Tree;
        Raw_context_intf.T.project := Raw_context.project;
        Raw_context_intf.T.absolute_key := Raw_context.absolute_key;
        Raw_context_intf.T.consume_gas := Raw_context.consume_gas;
        Raw_context_intf.T.check_enough_gas := Raw_context.check_enough_gas;
        Raw_context_intf.T.description := Raw_context.description
      |}
      (let name := [ "commitments" ] in
      {|
        Storage_sigs.NAME.name := name
      |})) (Make_index Blinded_public_key_hash.Index)
    {|
      Storage_sigs.VALUE.encoding := Tez_repr.encoding
    |}.

Module Ramp_up.
  Definition Rewards :=
    Storage_functors.Make_indexed_data_storage
      (Storage_functors.Make_subcontext Storage_functors.Registered
        {|
          Raw_context_intf.T.mem := Raw_context.mem;
          Raw_context_intf.T.mem_tree := Raw_context.mem_tree;
          Raw_context_intf.T.get := Raw_context.get;
          Raw_context_intf.T.get_tree := Raw_context.get_tree;
          Raw_context_intf.T.find := Raw_context.find;
          Raw_context_intf.T.find_tree := Raw_context.find_tree;
          Raw_context_intf.T.list_value := Raw_context.list_value;
          Raw_context_intf.T.init_value := Raw_context.init_value;
          Raw_context_intf.T.init_tree := Raw_context.init_tree;
          Raw_context_intf.T.update := Raw_context.update;
          Raw_context_intf.T.update_tree := Raw_context.update_tree;
          Raw_context_intf.T.add := Raw_context.add;
          Raw_context_intf.T.add_tree := Raw_context.add_tree;
          Raw_context_intf.T.remove := Raw_context.remove;
          Raw_context_intf.T.remove_existing := Raw_context.remove_existing;
          Raw_context_intf.T.remove_existing_tree :=
            Raw_context.remove_existing_tree;
          Raw_context_intf.T.add_or_remove := Raw_context.add_or_remove;
          Raw_context_intf.T.add_or_remove_tree :=
            Raw_context.add_or_remove_tree;
          Raw_context_intf.T.fold _ := Raw_context.fold;
          Raw_context_intf.T.Tree := Raw_context.Tree;
          Raw_context_intf.T.project := Raw_context.project;
          Raw_context_intf.T.absolute_key := Raw_context.absolute_key;
          Raw_context_intf.T.consume_gas := Raw_context.consume_gas;
          Raw_context_intf.T.check_enough_gas := Raw_context.check_enough_gas;
          Raw_context_intf.T.description := Raw_context.description
        |}
        (let name := [ "ramp_up"; "rewards" ] in
        {|
          Storage_sigs.NAME.name := name
        |})) (Make_index Cycle_repr.Index)
      (let t : Set := list Tez_repr.t * list Tez_repr.t in
      let encoding :=
        Data_encoding.obj2
          (Data_encoding.req None None "baking_reward_per_endorsement"
            (Data_encoding.list_value None Tez_repr.encoding))
          (Data_encoding.req None None "endorsement_reward"
            (Data_encoding.list_value None Tez_repr.encoding)) in
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}).
  
  Definition Security_deposits :=
    Storage_functors.Make_indexed_data_storage
      (Storage_functors.Make_subcontext Storage_functors.Registered
        {|
          Raw_context_intf.T.mem := Raw_context.mem;
          Raw_context_intf.T.mem_tree := Raw_context.mem_tree;
          Raw_context_intf.T.get := Raw_context.get;
          Raw_context_intf.T.get_tree := Raw_context.get_tree;
          Raw_context_intf.T.find := Raw_context.find;
          Raw_context_intf.T.find_tree := Raw_context.find_tree;
          Raw_context_intf.T.list_value := Raw_context.list_value;
          Raw_context_intf.T.init_value := Raw_context.init_value;
          Raw_context_intf.T.init_tree := Raw_context.init_tree;
          Raw_context_intf.T.update := Raw_context.update;
          Raw_context_intf.T.update_tree := Raw_context.update_tree;
          Raw_context_intf.T.add := Raw_context.add;
          Raw_context_intf.T.add_tree := Raw_context.add_tree;
          Raw_context_intf.T.remove := Raw_context.remove;
          Raw_context_intf.T.remove_existing := Raw_context.remove_existing;
          Raw_context_intf.T.remove_existing_tree :=
            Raw_context.remove_existing_tree;
          Raw_context_intf.T.add_or_remove := Raw_context.add_or_remove;
          Raw_context_intf.T.add_or_remove_tree :=
            Raw_context.add_or_remove_tree;
          Raw_context_intf.T.fold _ := Raw_context.fold;
          Raw_context_intf.T.Tree := Raw_context.Tree;
          Raw_context_intf.T.project := Raw_context.project;
          Raw_context_intf.T.absolute_key := Raw_context.absolute_key;
          Raw_context_intf.T.consume_gas := Raw_context.consume_gas;
          Raw_context_intf.T.check_enough_gas := Raw_context.check_enough_gas;
          Raw_context_intf.T.description := Raw_context.description
        |}
        (let name := [ "ramp_up"; "deposits" ] in
        {|
          Storage_sigs.NAME.name := name
        |})) (Make_index Cycle_repr.Index)
      (let t : Set := Tez_repr.t * Tez_repr.t in
      let encoding := Data_encoding.tup2 Tez_repr.encoding Tez_repr.encoding in
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}).
End Ramp_up.

Module Pending_migration.
  Definition Balance_updates :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      {|
        Raw_context_intf.T.mem := Raw_context.mem;
        Raw_context_intf.T.mem_tree := Raw_context.mem_tree;
        Raw_context_intf.T.get := Raw_context.get;
        Raw_context_intf.T.get_tree := Raw_context.get_tree;
        Raw_context_intf.T.find := Raw_context.find;
        Raw_context_intf.T.find_tree := Raw_context.find_tree;
        Raw_context_intf.T.list_value := Raw_context.list_value;
        Raw_context_intf.T.init_value := Raw_context.init_value;
        Raw_context_intf.T.init_tree := Raw_context.init_tree;
        Raw_context_intf.T.update := Raw_context.update;
        Raw_context_intf.T.update_tree := Raw_context.update_tree;
        Raw_context_intf.T.add := Raw_context.add;
        Raw_context_intf.T.add_tree := Raw_context.add_tree;
        Raw_context_intf.T.remove := Raw_context.remove;
        Raw_context_intf.T.remove_existing := Raw_context.remove_existing;
        Raw_context_intf.T.remove_existing_tree :=
          Raw_context.remove_existing_tree;
        Raw_context_intf.T.add_or_remove := Raw_context.add_or_remove;
        Raw_context_intf.T.add_or_remove_tree := Raw_context.add_or_remove_tree;
        Raw_context_intf.T.fold _ := Raw_context.fold;
        Raw_context_intf.T.Tree := Raw_context.Tree;
        Raw_context_intf.T.project := Raw_context.project;
        Raw_context_intf.T.absolute_key := Raw_context.absolute_key;
        Raw_context_intf.T.consume_gas := Raw_context.consume_gas;
        Raw_context_intf.T.check_enough_gas := Raw_context.check_enough_gas;
        Raw_context_intf.T.description := Raw_context.description
      |}
      (let name := [ "pending_migration_balance_updates" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      (let t : Set := Receipt_repr.balance_updates in
      let encoding := Receipt_repr.balance_updates_encoding in
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}).
  
  Definition Operation_results :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      {|
        Raw_context_intf.T.mem := Raw_context.mem;
        Raw_context_intf.T.mem_tree := Raw_context.mem_tree;
        Raw_context_intf.T.get := Raw_context.get;
        Raw_context_intf.T.get_tree := Raw_context.get_tree;
        Raw_context_intf.T.find := Raw_context.find;
        Raw_context_intf.T.find_tree := Raw_context.find_tree;
        Raw_context_intf.T.list_value := Raw_context.list_value;
        Raw_context_intf.T.init_value := Raw_context.init_value;
        Raw_context_intf.T.init_tree := Raw_context.init_tree;
        Raw_context_intf.T.update := Raw_context.update;
        Raw_context_intf.T.update_tree := Raw_context.update_tree;
        Raw_context_intf.T.add := Raw_context.add;
        Raw_context_intf.T.add_tree := Raw_context.add_tree;
        Raw_context_intf.T.remove := Raw_context.remove;
        Raw_context_intf.T.remove_existing := Raw_context.remove_existing;
        Raw_context_intf.T.remove_existing_tree :=
          Raw_context.remove_existing_tree;
        Raw_context_intf.T.add_or_remove := Raw_context.add_or_remove;
        Raw_context_intf.T.add_or_remove_tree := Raw_context.add_or_remove_tree;
        Raw_context_intf.T.fold _ := Raw_context.fold;
        Raw_context_intf.T.Tree := Raw_context.Tree;
        Raw_context_intf.T.project := Raw_context.project;
        Raw_context_intf.T.absolute_key := Raw_context.absolute_key;
        Raw_context_intf.T.consume_gas := Raw_context.consume_gas;
        Raw_context_intf.T.check_enough_gas := Raw_context.check_enough_gas;
        Raw_context_intf.T.description := Raw_context.description
      |}
      (let name := [ "pending_migration_operation_results" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      (let t : Set := list Migration_repr.origination_result in
      let encoding := Migration_repr.origination_result_list_encoding in
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}).
  
  Definition remove (ctxt : Raw_context.t)
    : M=?
      (Raw_context.t * Receipt_repr.balance_updates *
        Operation_results.(Storage_sigs.Single_data_storage.value)) :=
    let balance_updates (ctxt : Raw_context.t)
      : M=? (Raw_context.t * Receipt_repr.balance_updates) :=
      let=? function_parameter :=
        Balance_updates.(Storage_sigs.Single_data_storage.find) ctxt in
      match function_parameter with
      | Some balance_updates =>
        let= ctxt :=
          Balance_updates.(Storage_sigs.Single_data_storage.remove) ctxt in
        return=? (ctxt, balance_updates)
      | None => return=? (ctxt, nil)
      end in
    let operation_results (ctxt : Raw_context.t)
      : M=?
        (Raw_context.t *
          Operation_results.(Storage_sigs.Single_data_storage.value)) :=
      let=? function_parameter :=
        Operation_results.(Storage_sigs.Single_data_storage.find) ctxt in
      match function_parameter with
      | Some operation_results =>
        let= ctxt :=
          Operation_results.(Storage_sigs.Single_data_storage.remove) ctxt in
        return=? (ctxt, operation_results)
      | None => return=? (ctxt, nil)
      end in
    let=? '(ctxt, balance_updates) := balance_updates ctxt in
    let=? '(ctxt, operation_results) := operation_results ctxt in
    return=? (ctxt, balance_updates, operation_results).
End Pending_migration.

Module Liquidity_baking.
  Definition Escape_ema :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      {|
        Raw_context_intf.T.mem := Raw_context.mem;
        Raw_context_intf.T.mem_tree := Raw_context.mem_tree;
        Raw_context_intf.T.get := Raw_context.get;
        Raw_context_intf.T.get_tree := Raw_context.get_tree;
        Raw_context_intf.T.find := Raw_context.find;
        Raw_context_intf.T.find_tree := Raw_context.find_tree;
        Raw_context_intf.T.list_value := Raw_context.list_value;
        Raw_context_intf.T.init_value := Raw_context.init_value;
        Raw_context_intf.T.init_tree := Raw_context.init_tree;
        Raw_context_intf.T.update := Raw_context.update;
        Raw_context_intf.T.update_tree := Raw_context.update_tree;
        Raw_context_intf.T.add := Raw_context.add;
        Raw_context_intf.T.add_tree := Raw_context.add_tree;
        Raw_context_intf.T.remove := Raw_context.remove;
        Raw_context_intf.T.remove_existing := Raw_context.remove_existing;
        Raw_context_intf.T.remove_existing_tree :=
          Raw_context.remove_existing_tree;
        Raw_context_intf.T.add_or_remove := Raw_context.add_or_remove;
        Raw_context_intf.T.add_or_remove_tree := Raw_context.add_or_remove_tree;
        Raw_context_intf.T.fold _ := Raw_context.fold;
        Raw_context_intf.T.Tree := Raw_context.Tree;
        Raw_context_intf.T.project := Raw_context.project;
        Raw_context_intf.T.absolute_key := Raw_context.absolute_key;
        Raw_context_intf.T.consume_gas := Raw_context.consume_gas;
        Raw_context_intf.T.check_enough_gas := Raw_context.check_enough_gas;
        Raw_context_intf.T.description := Raw_context.description
      |}
      (let name := [ "liquidity_baking_escape_ema" ] in
      {|
        Storage_sigs.NAME.name := name
      |}) Encoding.Int32.
  
  Definition Cpmm_address :=
    Storage_functors.Make_single_data_storage Storage_functors.Registered
      {|
        Raw_context_intf.T.mem := Raw_context.mem;
        Raw_context_intf.T.mem_tree := Raw_context.mem_tree;
        Raw_context_intf.T.get := Raw_context.get;
        Raw_context_intf.T.get_tree := Raw_context.get_tree;
        Raw_context_intf.T.find := Raw_context.find;
        Raw_context_intf.T.find_tree := Raw_context.find_tree;
        Raw_context_intf.T.list_value := Raw_context.list_value;
        Raw_context_intf.T.init_value := Raw_context.init_value;
        Raw_context_intf.T.init_tree := Raw_context.init_tree;
        Raw_context_intf.T.update := Raw_context.update;
        Raw_context_intf.T.update_tree := Raw_context.update_tree;
        Raw_context_intf.T.add := Raw_context.add;
        Raw_context_intf.T.add_tree := Raw_context.add_tree;
        Raw_context_intf.T.remove := Raw_context.remove;
        Raw_context_intf.T.remove_existing := Raw_context.remove_existing;
        Raw_context_intf.T.remove_existing_tree :=
          Raw_context.remove_existing_tree;
        Raw_context_intf.T.add_or_remove := Raw_context.add_or_remove;
        Raw_context_intf.T.add_or_remove_tree := Raw_context.add_or_remove_tree;
        Raw_context_intf.T.fold _ := Raw_context.fold;
        Raw_context_intf.T.Tree := Raw_context.Tree;
        Raw_context_intf.T.project := Raw_context.project;
        Raw_context_intf.T.absolute_key := Raw_context.absolute_key;
        Raw_context_intf.T.consume_gas := Raw_context.consume_gas;
        Raw_context_intf.T.check_enough_gas := Raw_context.check_enough_gas;
        Raw_context_intf.T.description := Raw_context.description
      |}
      (let name := [ "liquidity_baking_cpmm_address" ] in
      {|
        Storage_sigs.NAME.name := name
      |})
      {|
        Storage_sigs.VALUE.encoding := Contract_repr.encoding
      |}.
End Liquidity_baking.
