Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Constants_repr.
Require TezosOfOCaml.Proto_2021_01.Contract_repr.
Require TezosOfOCaml.Proto_2021_01.Contract_storage.
Require TezosOfOCaml.Proto_2021_01.Cycle_repr.
Require TezosOfOCaml.Proto_2021_01.Delegate_storage.
Require TezosOfOCaml.Proto_2021_01.Lazy_storage_diff.
Require TezosOfOCaml.Proto_2021_01.Misc.
Require TezosOfOCaml.Proto_2021_01.Parameters_repr.
Require TezosOfOCaml.Proto_2021_01.Raw_context.
Require TezosOfOCaml.Proto_2021_01.Script_repr.
Require TezosOfOCaml.Proto_2021_01.Storage.
Require TezosOfOCaml.Proto_2021_01.Storage_sigs.
Require TezosOfOCaml.Proto_2021_01.Tez_repr.

Definition init_account
  (ctxt : Raw_context.t)
  (function_parameter : Parameters_repr.bootstrap_account)
  : M=? Raw_context.t :=
  let '{|
    Parameters_repr.bootstrap_account.public_key_hash := public_key_hash;
      Parameters_repr.bootstrap_account.public_key := public_key;
      Parameters_repr.bootstrap_account.amount := amount
      |} := function_parameter in
  let contract := Contract_repr.implicit_contract public_key_hash in
  let=? ctxt := Contract_storage.credit ctxt contract amount in
  match public_key with
  | Some public_key =>
    let=? ctxt :=
      Contract_storage.reveal_manager_key ctxt public_key_hash public_key in
    Delegate_storage.set ctxt contract (Some public_key_hash)
  | None => Error_monad._return ctxt
  end.

Definition init_contract
  (typecheck :
    Raw_context.t -> Script_repr.t ->
    M=? ((Script_repr.t * option Lazy_storage_diff.diffs) * Raw_context.t))
  (ctxt : Raw_context.t)
  (function_parameter : Parameters_repr.bootstrap_contract)
  : M=? Raw_context.t :=
  let '{|
    Parameters_repr.bootstrap_contract.delegate := delegate;
      Parameters_repr.bootstrap_contract.amount := amount;
      Parameters_repr.bootstrap_contract.script := script
      |} := function_parameter in
  let=? '(ctxt, contract) :=
    return= (Contract_storage.fresh_contract_from_current_nonce ctxt) in
  let=? '(script, ctxt) := typecheck ctxt script in
  Contract_storage.raw_originate ctxt (Some true) contract amount script
    (Some delegate).

Definition init_value
  (ctxt : Raw_context.t)
  (typecheck :
    Raw_context.t -> Script_repr.t ->
    M=? ((Script_repr.t * option Lazy_storage_diff.diffs) * Raw_context.t))
  (ramp_up_cycles : option int) (no_reward_cycles : option int)
  (accounts : list Parameters_repr.bootstrap_account)
  (contracts : list Parameters_repr.bootstrap_contract) : M=? Raw_context.t :=
  let nonce_value :=
    Operation_hash.hash_bytes None [ Bytes.of_string "Un festival de GADT." ] in
  let ctxt := Raw_context.init_origination_nonce ctxt nonce_value in
  let=? ctxt := Error_monad.fold_left_s init_account ctxt accounts in
  let=? ctxt := Error_monad.fold_left_s (init_contract typecheck) ctxt contracts
    in
  let=? ctxt :=
    match no_reward_cycles with
    | None => Error_monad._return ctxt
    | Some cycles =>
      let constants := Raw_context.constants ctxt in
      let= ctxt :=
        Raw_context.patch_constants ctxt
          (fun (c : Constants_repr.parametric) =>
            Constants_repr.parametric.with_endorsement_reward [ Tez_repr.zero ]
              (Constants_repr.parametric.with_baking_reward_per_endorsement
                [ Tez_repr.zero ] c)) in
      Storage.Ramp_up.Rewards.(Storage_sigs.Indexed_data_storage.init_value)
        ctxt (Cycle_repr.of_int32_exn (Int32.of_int cycles))
        (constants.(Constants_repr.parametric.baking_reward_per_endorsement),
          constants.(Constants_repr.parametric.endorsement_reward))
    end in
  match ramp_up_cycles with
  | None => Error_monad._return ctxt
  | Some cycles =>
    let constants := Raw_context.constants ctxt in
    let=? block_step :=
      return=
        (Tez_repr.op_divquestion
          constants.(Constants_repr.parametric.block_security_deposit)
          (Int64.of_int cycles)) in
    let=? endorsement_step :=
      return=
        (Tez_repr.op_divquestion
          constants.(Constants_repr.parametric.endorsement_security_deposit)
          (Int64.of_int cycles)) in
    let= ctxt :=
      Raw_context.patch_constants ctxt
        (fun (c : Constants_repr.parametric) =>
          Constants_repr.parametric.with_endorsement_security_deposit
            Tez_repr.zero
            (Constants_repr.parametric.with_block_security_deposit Tez_repr.zero
              c)) in
    let=? ctxt :=
      Error_monad.fold_left_s
        (fun (ctxt : Raw_context.t) =>
          fun (cycle : int) =>
            let=? block_security_deposit :=
              return= (Tez_repr.op_starquestion block_step (Int64.of_int cycle))
              in
            let=? endorsement_security_deposit :=
              return=
                (Tez_repr.op_starquestion endorsement_step (Int64.of_int cycle))
              in
            let cycle := Cycle_repr.of_int32_exn (Int32.of_int cycle) in
            Storage.Ramp_up.Security_deposits.(Storage_sigs.Indexed_data_storage.init_value)
              ctxt cycle (block_security_deposit, endorsement_security_deposit))
        ctxt (Misc.op_minusminusgt 1 (cycles -i 1)) in
    Storage.Ramp_up.Security_deposits.(Storage_sigs.Indexed_data_storage.init_value)
      ctxt (Cycle_repr.of_int32_exn (Int32.of_int cycles))
      (constants.(Constants_repr.parametric.block_security_deposit),
        constants.(Constants_repr.parametric.endorsement_security_deposit))
  end.

Definition cycle_end (ctxt : Raw_context.t) (last_cycle : Cycle_repr.cycle)
  : M=? Raw_context.t :=
  let next_cycle := Cycle_repr.succ last_cycle in
  let=? ctxt :=
    let=? function_parameter :=
      Storage.Ramp_up.Rewards.(Storage_sigs.Indexed_data_storage.get_option)
        ctxt next_cycle in
    match function_parameter with
    | None => Error_monad._return ctxt
    | Some (baking_reward_per_endorsement, endorsement_reward) =>
      let=? ctxt :=
        Storage.Ramp_up.Rewards.(Storage_sigs.Indexed_data_storage.delete) ctxt
          next_cycle in
      Error_monad.op_gtpipeeq
        (Raw_context.patch_constants ctxt
          (fun (c : Constants_repr.parametric) =>
            Constants_repr.parametric.with_endorsement_reward endorsement_reward
              (Constants_repr.parametric.with_baking_reward_per_endorsement
                baking_reward_per_endorsement c))) Error_monad.ok
    end in
  let=? function_parameter :=
    Storage.Ramp_up.Security_deposits.(Storage_sigs.Indexed_data_storage.get_option)
      ctxt next_cycle in
  match function_parameter with
  | None => Error_monad._return ctxt
  | Some (block_security_deposit, endorsement_security_deposit) =>
    let=? ctxt :=
      Storage.Ramp_up.Security_deposits.(Storage_sigs.Indexed_data_storage.delete)
        ctxt next_cycle in
    Error_monad.op_gtpipeeq
      (Raw_context.patch_constants ctxt
        (fun (c : Constants_repr.parametric) =>
          Constants_repr.parametric.with_endorsement_security_deposit
            endorsement_security_deposit
            (Constants_repr.parametric.with_block_security_deposit
              block_security_deposit c))) Error_monad.ok
  end.
