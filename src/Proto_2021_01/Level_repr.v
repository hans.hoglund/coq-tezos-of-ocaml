Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Cycle_repr.
Require TezosOfOCaml.Proto_2021_01.Raw_level_repr.

Module t.
  Record record : Set := Build {
    level : Raw_level_repr.t;
    level_position : int32;
    cycle : Cycle_repr.t;
    cycle_position : int32;
    expected_commitment : bool }.
  Definition with_level level (r : record) :=
    Build level r.(level_position) r.(cycle) r.(cycle_position)
      r.(expected_commitment).
  Definition with_level_position level_position (r : record) :=
    Build r.(level) level_position r.(cycle) r.(cycle_position)
      r.(expected_commitment).
  Definition with_cycle cycle (r : record) :=
    Build r.(level) r.(level_position) cycle r.(cycle_position)
      r.(expected_commitment).
  Definition with_cycle_position cycle_position (r : record) :=
    Build r.(level) r.(level_position) r.(cycle) cycle_position
      r.(expected_commitment).
  Definition with_expected_commitment expected_commitment (r : record) :=
    Build r.(level) r.(level_position) r.(cycle) r.(cycle_position)
      expected_commitment.
End t.
Definition t := t.record.

Definition Compare_Make_include :=
  Compare.Make
    (let t : Set := t in
    let compare (function_parameter : t) : t -> int :=
      let '{| t.level := l1 |} := function_parameter in
      fun (function_parameter : t) =>
        let '{| t.level := l2 |} := function_parameter in
        Raw_level_repr.compare l1 l2 in
    {|
      Compare.COMPARABLE.compare := compare
    |}).

(** Inclusion of the module [Compare_Make_include] *)
Definition op_eq := Compare_Make_include.(Compare.S.op_eq).

Definition op_ltgt := Compare_Make_include.(Compare.S.op_ltgt).

Definition op_lt := Compare_Make_include.(Compare.S.op_lt).

Definition op_lteq := Compare_Make_include.(Compare.S.op_lteq).

Definition op_gteq := Compare_Make_include.(Compare.S.op_gteq).

Definition op_gt := Compare_Make_include.(Compare.S.op_gt).

Definition compare := Compare_Make_include.(Compare.S.compare).

Definition equal := Compare_Make_include.(Compare.S.equal).

Definition max := Compare_Make_include.(Compare.S.max).

Definition min := Compare_Make_include.(Compare.S.min).

Definition level : Set := t.

Definition pp (ppf : Format.formatter) (function_parameter : t) : unit :=
  let '{| t.level := level |} := function_parameter in
  Raw_level_repr.pp ppf level.

Definition pp_full (ppf : Format.formatter) (l_value : t) : unit :=
  Format.fprintf ppf
    (CamlinternalFormatBasics.Format
      (CamlinternalFormatBasics.Alpha
        (CamlinternalFormatBasics.Char_literal "." % char
          (CamlinternalFormatBasics.Int32 CamlinternalFormatBasics.Int_d
            CamlinternalFormatBasics.No_padding
            CamlinternalFormatBasics.No_precision
            (CamlinternalFormatBasics.String_literal " (cycle "
              (CamlinternalFormatBasics.Alpha
                (CamlinternalFormatBasics.Char_literal "." % char
                  (CamlinternalFormatBasics.Int32 CamlinternalFormatBasics.Int_d
                    CamlinternalFormatBasics.No_padding
                    CamlinternalFormatBasics.No_precision
                    (CamlinternalFormatBasics.Char_literal ")" % char
                      CamlinternalFormatBasics.End_of_format))))))))
      "%a.%ld (cycle %a.%ld)") Raw_level_repr.pp l_value.(t.level)
    l_value.(t.level_position) Cycle_repr.pp l_value.(t.cycle)
    l_value.(t.cycle_position).

Definition encoding : Data_encoding.encoding t :=
  Data_encoding.conv
    (fun (function_parameter : t) =>
      let '{|
        t.level := level;
          t.level_position := level_position;
          t.cycle := cycle;
          t.cycle_position := cycle_position;
          t.expected_commitment := expected_commitment
          |} := function_parameter in
      (level, level_position, cycle, cycle_position, expected_commitment))
    (fun (function_parameter :
      Raw_level_repr.t * int32 * Cycle_repr.t * int32 * bool) =>
      let
        '(level, level_position, cycle, cycle_position, expected_commitment) :=
        function_parameter in
      {| t.level := level; t.level_position := level_position; t.cycle := cycle;
        t.cycle_position := cycle_position;
        t.expected_commitment := expected_commitment |}) None
    (Data_encoding.obj5
      (Data_encoding.req None
        (Some
          "The level of the block relative to genesis. This is also the Shell's notion of level")
        "level" Raw_level_repr.encoding)
      (Data_encoding.req None
        (Some
          "The level of the block relative to the block that starts protocol alpha. This is specific to the protocol alpha. Other protocols might or might not include a similar notion.")
        "level_position" Data_encoding.int32_value)
      (Data_encoding.req None
        (Some
          "The current cycle's number. Note that cycles are a protocol-specific notion. As a result, the cycle number starts at 0 with the first block of protocol alpha.")
        "cycle" Cycle_repr.encoding)
      (Data_encoding.req None
        (Some
          "The current level of the block relative to the first block of the current cycle.")
        "cycle_position" Data_encoding.int32_value)
      (Data_encoding.req None
        (Some
          "Tells wether the baker of this block has to commit a seed nonce hash.")
        "expected_commitment" Data_encoding.bool_value)).

Definition root_level (first_level : Raw_level_repr.t) : t :=
  {| t.level := first_level; t.level_position := 0; t.cycle := Cycle_repr.root;
    t.cycle_position := 0; t.expected_commitment := false |}.

Definition level_from_raw
  (first_level : Raw_level_repr.raw_level) (blocks_per_cycle : int32)
  (blocks_per_commitment : int32) (level : Raw_level_repr.raw_level) : t :=
  let raw_level := Raw_level_repr.to_int32 level in
  let first_level := Raw_level_repr.to_int32 first_level in
  let level_position :=
    Compare.Int32.(Compare.S.max) 0 (raw_level -i32 first_level) in
  let cycle := Cycle_repr.of_int32_exn (level_position /i32 blocks_per_cycle) in
  let cycle_position := Int32.rem level_position blocks_per_cycle in
  let expected_commitment :=
    (Int32.rem cycle_position blocks_per_commitment) =i32
    (Int32.pred blocks_per_commitment) in
  {| t.level := level; t.level_position := level_position; t.cycle := cycle;
    t.cycle_position := cycle_position;
    t.expected_commitment := expected_commitment |}.

Definition diff_value (function_parameter : t) : t -> int32 :=
  let '{| t.level := l1 |} := function_parameter in
  fun (function_parameter : t) =>
    let '{| t.level := l2 |} := function_parameter in
    (Raw_level_repr.to_int32 l1) -i32 (Raw_level_repr.to_int32 l2).

Module compat_t.
  Record record : Set := Build {
    level : Raw_level_repr.t;
    level_position : int32;
    cycle : Cycle_repr.t;
    cycle_position : int32;
    voting_period : int32;
    voting_period_position : int32;
    expected_commitment : bool }.
  Definition with_level level (r : record) :=
    Build level r.(level_position) r.(cycle) r.(cycle_position)
      r.(voting_period) r.(voting_period_position) r.(expected_commitment).
  Definition with_level_position level_position (r : record) :=
    Build r.(level) level_position r.(cycle) r.(cycle_position)
      r.(voting_period) r.(voting_period_position) r.(expected_commitment).
  Definition with_cycle cycle (r : record) :=
    Build r.(level) r.(level_position) cycle r.(cycle_position)
      r.(voting_period) r.(voting_period_position) r.(expected_commitment).
  Definition with_cycle_position cycle_position (r : record) :=
    Build r.(level) r.(level_position) r.(cycle) cycle_position
      r.(voting_period) r.(voting_period_position) r.(expected_commitment).
  Definition with_voting_period voting_period (r : record) :=
    Build r.(level) r.(level_position) r.(cycle) r.(cycle_position)
      voting_period r.(voting_period_position) r.(expected_commitment).
  Definition with_voting_period_position voting_period_position (r : record) :=
    Build r.(level) r.(level_position) r.(cycle) r.(cycle_position)
      r.(voting_period) voting_period_position r.(expected_commitment).
  Definition with_expected_commitment expected_commitment (r : record) :=
    Build r.(level) r.(level_position) r.(cycle) r.(cycle_position)
      r.(voting_period) r.(voting_period_position) expected_commitment.
End compat_t.
Definition compat_t := compat_t.record.

Definition compat_encoding : Data_encoding.encoding compat_t :=
  Data_encoding.conv
    (fun (function_parameter : compat_t) =>
      let '{|
        compat_t.level := level;
          compat_t.level_position := level_position;
          compat_t.cycle := cycle;
          compat_t.cycle_position := cycle_position;
          compat_t.voting_period := voting_period;
          compat_t.voting_period_position := voting_period_position;
          compat_t.expected_commitment := expected_commitment
          |} := function_parameter in
      (level, level_position, cycle, cycle_position, voting_period,
        voting_period_position, expected_commitment))
    (fun (function_parameter :
      Raw_level_repr.t * int32 * Cycle_repr.t * int32 * int32 * int32 * bool) =>
      let
        '(level, level_position, cycle, cycle_position, voting_period,
          voting_period_position, expected_commitment) := function_parameter in
      {| compat_t.level := level; compat_t.level_position := level_position;
        compat_t.cycle := cycle; compat_t.cycle_position := cycle_position;
        compat_t.voting_period := voting_period;
        compat_t.voting_period_position := voting_period_position;
        compat_t.expected_commitment := expected_commitment |}) None
    (Data_encoding.obj7
      (Data_encoding.req None
        (Some
          "The level of the block relative to genesis. This is also the Shell's notion of level")
        "level" Raw_level_repr.encoding)
      (Data_encoding.req None
        (Some
          "The level of the block relative to the block that starts protocol alpha. This is specific to the protocol alpha. Other protocols might or might not include a similar notion.")
        "level_position" Data_encoding.int32_value)
      (Data_encoding.req None
        (Some
          "The current cycle's number. Note that cycles are a protocol-specific notion. As a result, the cycle number starts at 0 with the first block of protocol alpha.")
        "cycle" Cycle_repr.encoding)
      (Data_encoding.req None
        (Some
          "The current level of the block relative to the first block of the current cycle.")
        "cycle_position" Data_encoding.int32_value)
      (Data_encoding.req None
        (Some
          "The current voting period's index. Note that cycles are a protocol-specific notion. As a result, the voting period index starts at 0 with the first block of protocol alpha. This field is DEPRECATED: use `..<block_id>/votes/voting_period` RPC instead.")
        "voting_period" Data_encoding.int32_value)
      (Data_encoding.req None
        (Some
          "The current level of the block relative to the first block of the current voting period. This field is DEPRECATED: use `..<block_id>/votes/voting_period` RPC instead.")
        "voting_period_position" Data_encoding.int32_value)
      (Data_encoding.req None
        (Some
          "Tells wether the baker of this block has to commit a seed nonce hash.")
        "expected_commitment" Data_encoding.bool_value)).

Definition to_deprecated_type (function_parameter : t)
  : int32 -> int32 -> compat_t :=
  let '{|
    t.level := level;
      t.level_position := level_position;
      t.cycle := cycle;
      t.cycle_position := cycle_position;
      t.expected_commitment := expected_commitment
      |} := function_parameter in
  fun (voting_period_index : int32) =>
    fun (voting_period_position : int32) =>
      {| compat_t.level := level; compat_t.level_position := level_position;
        compat_t.cycle := cycle; compat_t.cycle_position := cycle_position;
        compat_t.voting_period := voting_period_index;
        compat_t.voting_period_position := voting_period_position;
        compat_t.expected_commitment := expected_commitment |}.
