Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Misc.

Definition t : Set := int.

Parameter mul_safe : Set.

Parameter may_saturate : Set.

Definition may_saturate_value (x : int) : int := x.

Definition to_int {A : Set} (x : A) : A := x.

Definition op_lt : int -> int -> bool := Compare.Int.(Compare.S.op_lt).

Definition op_lteq : int -> int -> bool := Compare.Int.(Compare.S.op_lteq).

Definition op_gt : int -> int -> bool := Compare.Int.(Compare.S.op_gt).

Definition op_gteq : int -> int -> bool := Compare.Int.(Compare.S.op_gteq).

Definition op_eq : int -> int -> bool := Compare.Int.(Compare.S.op_eq).

Definition equal : int -> int -> bool := op_eq.

Definition op_ltgt : int -> int -> bool := Compare.Int.(Compare.S.op_ltgt).

Definition max (x : int) (y : int) : int :=
  if op_gteq x y then
    x
  else
    y.

Definition min (x : int) (y : int) : int :=
  if op_gteq x y then
    y
  else
    x.

Definition compare : int -> int -> int := Compare.Int.(Compare.S.compare).

Definition saturated : int := Pervasives.max_int.

Definition of_int_opt (t_value : int) : option int :=
  if (op_gteq t_value 0) && (op_lt t_value saturated) then
    Some t_value
  else
    None.

Definition of_z_opt (z : Z.t) : option int :=
  match
    Misc.result_of_exception
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Z.to_int z) with
  | Pervasives.Ok int_value => of_int_opt int_value
  | Pervasives.Error exn_value =>
    match exn_value with
    | Build_extensible tag _ payload =>
      if String.eqb tag "Overflow" then
        None
      else Pervasives.raise exn_value
    end
  end.

Definition to_z (x : int) : Z.t := Z.of_int x.

Definition zero : int := 0.

Definition small_enough (z : int) : bool :=
  op_eq (Pervasives.land z (-2147483648)) 0.

Definition mul_safe_value (x : int) : option int :=
  if small_enough x then
    Some x
  else
    None.

Definition mul (x : int) (y : int) : int :=
  match x with
  | 0 => 0
  | x =>
    if (small_enough x) && (small_enough y) then
      x *i y
    else
      if y >i (saturated /i x) then
        saturated
      else
        x *i y
  end.

Definition mul_fast (x : int) (y : int) : int := x *i y.

Definition scale_fast (x : int) (y : int) : int :=
  if op_eq x 0 then
    0
  else
    if small_enough y then
      x *i y
    else
      if y >i (saturated /i x) then
        saturated
      else
        x *i y.

Definition add (x : int) (y : int) : int :=
  let z := x +i y in
  if op_gteq z 0 then
    z
  else
    saturated.

Definition sub (x : int) (y : int) : int :=
  Compare.Int.(Compare.S.max) (x -i y) 0.

Definition sub_opt (x : int) (y : int) : option int :=
  let s := x -i y in
  if s >=i 0 then
    Some s
  else
    None.

Definition erem (x : int) (y : int) : int := Pervasives._mod x y.

Definition ediv (x : int) (y : int) : int := x /i y.

Definition t_to_z_exn (z : Z.t) : int :=
  match of_z_opt z with
  | None =>
    (* ❌ Assert instruction is not handled. *)
    assert int false
  | Some x => x
  end.

Definition z_encoding : Data_encoding.encoding int :=
  Data_encoding.check_size 9
    (Data_encoding.conv to_z t_to_z_exn None Data_encoding.z).

Definition n_encoding : Data_encoding.encoding int :=
  Data_encoding.check_size 9
    (Data_encoding.conv to_z t_to_z_exn None Data_encoding.n).

Definition pp (fmt : Format.formatter) (x : int) : unit :=
  Format.pp_print_int fmt x.
