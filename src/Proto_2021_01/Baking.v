Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Alpha_context.
Require TezosOfOCaml.Proto_2021_01.Misc.

(** Init function; without side-effects in Coq *)
Definition init_module1 : unit :=
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "baking.timestamp_too_early" "Block forged too early"
      "The block timestamp is before the first slot for this baker at this level"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : Time.t * Time.t) =>
            let '(r_value, p_value) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Block forged too early ("
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal " is before "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.Char_literal ")" % char
                          CamlinternalFormatBasics.End_of_format)))))
                "Block forged too early (%a is before %a)") Time.pp_hum p_value
              Time.pp_hum r_value))
      (Data_encoding.obj2 (Data_encoding.req None None "minimum" Time.encoding)
        (Data_encoding.req None None "provided" Time.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Timestamp_too_early" then
            let '(r_value, p_value) :=
              cast (Alpha_context.Timestamp.t * Alpha_context.Timestamp.t)
                payload in
            Some (r_value, p_value)
          else None
        end)
      (fun (function_parameter : Time.t * Time.t) =>
        let '(r_value, p_value) := function_parameter in
        Build_extensible "Timestamp_too_early" (Time.t * Time.t)
          (r_value, p_value)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "baking.invalid_fitness_gap" "Invalid fitness gap"
      "The gap of fitness is out of bounds"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : int64 * int64) =>
            let '(m, g) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "The gap of fitness "
                  (CamlinternalFormatBasics.Int64 CamlinternalFormatBasics.Int_d
                    CamlinternalFormatBasics.No_padding
                    CamlinternalFormatBasics.No_precision
                    (CamlinternalFormatBasics.String_literal
                      " is not between 0 and "
                      (CamlinternalFormatBasics.Int64
                        CamlinternalFormatBasics.Int_d
                        CamlinternalFormatBasics.No_padding
                        CamlinternalFormatBasics.No_precision
                        CamlinternalFormatBasics.End_of_format))))
                "The gap of fitness %Ld is not between 0 and %Ld") g m))
      (Data_encoding.obj2
        (Data_encoding.req None None "maximum" Data_encoding.int64_value)
        (Data_encoding.req None None "provided" Data_encoding.int64_value))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_fitness_gap" then
            let '(m, g) := cast (int64 * int64) payload in
            Some (m, g)
          else None
        end)
      (fun (function_parameter : int64 * int64) =>
        let '(m, g) := function_parameter in
        Build_extensible "Invalid_fitness_gap" (int64 * int64) (m, g)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "baking.invalid_block_signature" "Invalid block signature"
      "A block was not signed with the expected private key."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : Block_hash.t * Signature.public_key_hash) =>
            let '(block, pkh) := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Invalid signature for block "
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.String_literal ". Expected: "
                      (CamlinternalFormatBasics.Alpha
                        (CamlinternalFormatBasics.Char_literal "." % char
                          CamlinternalFormatBasics.End_of_format)))))
                "Invalid signature for block %a. Expected: %a.")
              Block_hash.pp_short block
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp_short)
              pkh))
      (Data_encoding.obj2
        (Data_encoding.req None None "block" Block_hash.encoding)
        (Data_encoding.req None None "expected"
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_block_signature" then
            let '(block, pkh) :=
              cast (Block_hash.t * Signature.public_key_hash) payload in
            Some (block, pkh)
          else None
        end)
      (fun (function_parameter : Block_hash.t * Signature.public_key_hash) =>
        let '(block, pkh) := function_parameter in
        Build_extensible "Invalid_block_signature"
          (Block_hash.t * Signature.public_key_hash) (block, pkh)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "baking.invalid_signature" "Invalid block signature"
      "The block's signature is invalid"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Invalid block signature"
                  CamlinternalFormatBasics.End_of_format)
                "Invalid block signature"))) Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_signature" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Invalid_signature" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "baking.insufficient_proof_of_work"
      "Insufficient block proof-of-work stamp"
      "The block's proof-of-work stamp is insufficient"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Insufficient proof-of-work stamp"
                  CamlinternalFormatBasics.End_of_format)
                "Insufficient proof-of-work stamp"))) Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Invalid_stamp" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Invalid_stamp" unit tt) in
  Error_monad.register_error_kind Error_monad.Permanent
    "baking.unexpected_endorsement" "Endorsement from unexpected delegate"
    "The operation is signed by a delegate without endorsement rights."
    (Some
      (fun (ppf : Format.formatter) =>
        fun (function_parameter : unit) =>
          let '_ := function_parameter in
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal
                "The endorsement is signed by a delegate without endorsement rights."
                CamlinternalFormatBasics.End_of_format)
              "The endorsement is signed by a delegate without endorsement rights.")))
    Data_encoding.unit_value
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Unexpected_endorsement" then
          Some tt
        else None
      end)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Build_extensible "Unexpected_endorsement" unit tt).

Definition minimal_time
  (c : Alpha_context.context) (priority : int)
  (pred_timestamp : Alpha_context.Timestamp.time)
  : M? Alpha_context.Timestamp.time :=
  let priority := Int32.of_int priority in
  let fix cumsum_time_between_blocks
    (acc_value : Alpha_context.Timestamp.time)
    (durations : list Alpha_context.Period.period) (p_value : int32)
    {struct durations} : M? Alpha_context.Timestamp.time :=
    if p_value <=i32 0 then
      return? acc_value
    else
      match durations with
      | [] =>
        cumsum_time_between_blocks acc_value [ Alpha_context.Period.one_minute ]
          p_value
      | cons last [] =>
        let? period := Alpha_context.Period.mult p_value last in
        Alpha_context.Timestamp.op_plusquestion acc_value period
      | cons first durations =>
        let? acc_value :=
          Alpha_context.Timestamp.op_plusquestion acc_value first in
        let p_value := Int32.pred p_value in
        cumsum_time_between_blocks acc_value durations p_value
      end in
  cumsum_time_between_blocks pred_timestamp
    (Alpha_context.Constants.time_between_blocks c) (Int32.succ priority).

Definition earlier_predecessor_timestamp
  (ctxt : Alpha_context.context) (level : Alpha_context.Level.level)
  : M? Alpha_context.Timestamp.time :=
  let current := Alpha_context.Level.current ctxt in
  let current_timestamp := Alpha_context.Timestamp.current ctxt in
  let gap := Alpha_context.Level.diff_value level current in
  let step := List.hd (Alpha_context.Constants.time_between_blocks ctxt) in
  if gap <i32 1 then
    Pervasives.failwith "Baking.earlier_block_timestamp: past block."
  else
    let? delay := Alpha_context.Period.mult (Int32.pred gap) step in
    Alpha_context.Timestamp.op_plusquestion current_timestamp delay.

Definition check_timestamp
  (c : Alpha_context.context) (priority : int)
  (pred_timestamp : Alpha_context.Timestamp.time) : M? Alpha_context.Period.t :=
  let? minimal_time := minimal_time c priority pred_timestamp in
  let timestamp := Alpha_context.Timestamp.current c in
  Error_monad.record_trace
    (Build_extensible "Timestamp_too_early"
      (Alpha_context.Timestamp.time * Alpha_context.Timestamp.time)
      (minimal_time, timestamp))
    (Alpha_context.Timestamp.op_minusquestion timestamp minimal_time).

Definition check_baking_rights
  (c : Alpha_context.context)
  (function_parameter : Alpha_context.Block_header.contents)
  : Alpha_context.Timestamp.time ->
  M=? (Alpha_context.public_key * Alpha_context.Period.t) :=
  let '{| Alpha_context.Block_header.contents.priority := priority |} :=
    function_parameter in
  fun (pred_timestamp : Alpha_context.Timestamp.time) =>
    let level := Alpha_context.Level.current c in
    let=? delegate := Alpha_context.Roll.baking_rights_owner c level priority in
    Lwt._return
      (let? block_delay := check_timestamp c priority pred_timestamp in
      return? (delegate, block_delay)).

(** Init function; without side-effects in Coq *)
Definition init_module2 : unit :=
  Error_monad.register_error_kind Error_monad.Permanent "incorrect_priority"
    "Incorrect priority" "Block priority must be non-negative."
    (Some
      (fun (ppf : Format.formatter) =>
        fun (function_parameter : unit) =>
          let '_ := function_parameter in
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal
                "The block priority must be non-negative."
                CamlinternalFormatBasics.End_of_format)
              "The block priority must be non-negative.")))
    Data_encoding.unit_value
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Incorrect_priority" then
          Some tt
        else None
      end)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Build_extensible "Incorrect_priority" unit tt).

(** Init function; without side-effects in Coq *)
Definition init_module3 : unit :=
  let description :=
    "The number of endorsements must be non-negative and at most the endorsers_per_block constant."
    in
  Error_monad.register_error_kind Error_monad.Permanent
    "incorrect_number_of_endorsements" "Incorrect number of endorsements"
    description
    (Some
      (fun (ppf : Format.formatter) =>
        fun (function_parameter : unit) =>
          let '_ := function_parameter in
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String
                CamlinternalFormatBasics.No_padding
                CamlinternalFormatBasics.End_of_format) "%s") description))
    Data_encoding.unit_value
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Incorrect_number_of_endorsements" then
          Some tt
        else None
      end)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Build_extensible "Incorrect_number_of_endorsements" unit tt).

Fixpoint reward_for_priority
  (reward_per_prio : list Alpha_context.Tez.tez) (prio : int)
  : Alpha_context.Tez.tez :=
  match reward_per_prio with
  | [] => Alpha_context.Tez.zero
  | cons last [] => last
  | cons first rest =>
    if prio <=i 0 then
      first
    else
      reward_for_priority rest (Pervasives.pred prio)
  end.

Definition baking_reward
  (ctxt : Alpha_context.context) (block_priority : int)
  (included_endorsements : int) : M? Alpha_context.Tez.tez :=
  let? '_ :=
    Error_monad.error_unless (block_priority >=i 0)
      (Build_extensible "Incorrect_priority" unit tt) in
  let? '_ :=
    Error_monad.error_unless
      ((included_endorsements >=i 0) &&
      (included_endorsements <=i
      (Alpha_context.Constants.endorsers_per_block ctxt)))
      (Build_extensible "Incorrect_number_of_endorsements" unit tt) in
  let reward_per_endorsement :=
    reward_for_priority
      (Alpha_context.Constants.baking_reward_per_endorsement ctxt)
      block_priority in
  Alpha_context.Tez.op_starquestion reward_per_endorsement
    (Int64.of_int included_endorsements).

Definition endorsing_reward
  (ctxt : Alpha_context.context) (block_priority : int) (num_slots : int)
  : M? Alpha_context.Tez.tez :=
  let? '_ :=
    Error_monad.error_unless (block_priority >=i 0)
      (Build_extensible "Incorrect_priority" unit tt) in
  let reward_per_endorsement :=
    reward_for_priority (Alpha_context.Constants.endorsement_reward ctxt)
      block_priority in
  Alpha_context.Tez.op_starquestion reward_per_endorsement
    (Int64.of_int num_slots).

Definition baking_priorities
  (c : Alpha_context.context) (level : Alpha_context.Level.t)
  : M=? (Misc.lazy_list_t Alpha_context.public_key) :=
  let fix f (priority : int)
    : M=? (Misc.lazy_list_t Alpha_context.public_key) :=
    let=? delegate := Alpha_context.Roll.baking_rights_owner c level priority in
    return=?
      (Misc.LCons delegate
        (fun (function_parameter : unit) =>
          let '_ := function_parameter in
          f (Pervasives.succ priority))) in
  f 0.

Definition endorsement_rights
  (ctxt : Alpha_context.context) (level : Alpha_context.Level.t)
  : M=?
    (Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.t)
      (Alpha_context.public_key * list int * bool)) :=
  Error_monad.fold_left_s
    (fun (acc_value :
      Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.t)
        (Alpha_context.public_key * list int * bool)) =>
      fun (slot : int) =>
        let=? pk := Alpha_context.Roll.endorsement_rights_owner ctxt level slot
          in
        let pkh := Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.hash_value) pk
          in
        let _right :=
          match
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.find_opt)
              pkh acc_value with
          | None => (pk, [ slot ], false)
          | Some (pk, slots, used) => (pk, (cons slot slots), used)
          end in
        return=?
          (Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.add)
            pkh _right acc_value))
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.empty)
    (Misc.op_minusminusgt 0
      ((Alpha_context.Constants.endorsers_per_block ctxt) -i 1)).

Definition check_endorsement_rights
  (ctxt : Alpha_context.context) (chain_id : Chain_id.t)
  (op : Alpha_context.Operation.t)
  : M=? (Signature.public_key_hash * list int * bool) :=
  let current_level := Alpha_context.Level.current ctxt in
  match
    op.(Alpha_context.operation.protocol_data).(Alpha_context.protocol_data.contents)
    with
  |
    Alpha_context.Single
      (Alpha_context.Endorsement {|
        Alpha_context.contents.Endorsement.level := level |}) =>
    let=? endorsements :=
      if
        Alpha_context.Raw_level.op_eq (Alpha_context.Raw_level.succ level)
          current_level.(Alpha_context.Level.t.level)
      then
        Error_monad._return (Alpha_context.allowed_endorsements ctxt)
      else
        endorsement_rights ctxt (Alpha_context.Level.from_raw ctxt None level)
      in
    match
      Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.fold)
        (fun (pkh : Signature.public_key_hash) =>
          fun (function_parameter : Alpha_context.public_key * list int * bool)
            =>
            let '(pk, slots, used) := function_parameter in
            fun (acc_value :
              option (Signature.public_key_hash * list int * bool)) =>
              match Alpha_context.Operation.check_signature pk chain_id op with
              | Pervasives.Error _ => acc_value
              | Pervasives.Ok _ => Some (pkh, slots, used)
              end) endorsements None with
    | None =>
      Error_monad.fail (Build_extensible "Unexpected_endorsement" unit tt)
    | Some v => Error_monad._return v
    end
  | _ => unreachable_gadt_branch
  end.

Definition select_delegate
  (delegate : Signature.public_key_hash)
  (delegate_list : Misc.lazy_list_t Signature.public_key) (max_priority : int)
  : M=? (list int) :=
  let fix loop
    (acc_value : list int) (l_value : Misc.lazy_list_t Signature.public_key)
    (n : int) {struct n} : M=? (list int) :=
    if n >=i max_priority then
      Error_monad._return (List.rev acc_value)
    else
      let 'Misc.LCons pk t_value := l_value in
      let acc_value :=
        if
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.equal) delegate
            (Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.hash_value) pk)
        then
          cons n acc_value
        else
          acc_value in
      let=? t_value := t_value tt in
      loop acc_value t_value (Pervasives.succ n) in
  loop nil delegate_list 0.

Definition first_baking_priorities
  (ctxt : Alpha_context.context) (op_staroptstar : option int)
  : Signature.public_key_hash -> Alpha_context.Level.t -> M=? (list int) :=
  let max_priority :=
    match op_staroptstar with
    | Some op_starsthstar => op_starsthstar
    | None => 32
    end in
  fun (delegate : Signature.public_key_hash) =>
    fun (level : Alpha_context.Level.t) =>
      let=? delegate_list := baking_priorities ctxt level in
      select_delegate delegate delegate_list max_priority.

Definition check_hash
  (hash_value : Block_hash.t) (stamp_threshold : Compare.Uint64.(Compare.S.t))
  : bool :=
  let bytes_value := Block_hash.to_bytes hash_value in
  let word := TzEndian.get_int64 bytes_value 0 in
  Compare.Uint64.(Compare.S.op_lteq) word stamp_threshold.

Definition check_header_proof_of_work_stamp
  (shell : Block_header.shell_header)
  (contents : Alpha_context.Block_header.contents)
  (stamp_threshold : Compare.Uint64.(Compare.S.t)) : bool :=
  let hash_value :=
    Alpha_context.Block_header.hash_value
      {| Alpha_context.Block_header.t.shell := shell;
        Alpha_context.Block_header.t.protocol_data :=
          {| Alpha_context.Block_header.protocol_data.contents := contents;
            Alpha_context.Block_header.protocol_data.signature := Signature.zero
            |} |} in
  check_hash hash_value stamp_threshold.

Definition check_proof_of_work_stamp
  (ctxt : Alpha_context.context) (block : Alpha_context.Block_header.t)
  : M? unit :=
  let proof_of_work_threshold :=
    Alpha_context.Constants.proof_of_work_threshold ctxt in
  if
    check_header_proof_of_work_stamp block.(Alpha_context.Block_header.t.shell)
      block.(Alpha_context.Block_header.t.protocol_data).(Alpha_context.Block_header.protocol_data.contents)
      proof_of_work_threshold
  then
    Error_monad.ok_unit
  else
    Error_monad.error_value (Build_extensible "Invalid_stamp" unit tt).

Definition check_signature
  (block : Alpha_context.Block_header.t) (chain_id : Chain_id.t)
  (key_value : Signature.public_key) : M=? unit :=
  let check_signature
    (key_value : Signature.public_key)
    (function_parameter : Alpha_context.Block_header.t) : bool :=
    let '{|
      Alpha_context.Block_header.t.shell := shell;
        Alpha_context.Block_header.t.protocol_data := {|
          Alpha_context.Block_header.protocol_data.contents := contents;
            Alpha_context.Block_header.protocol_data.signature :=
              signature
            |}
        |} := function_parameter in
    let unsigned_header :=
      Data_encoding.Binary.to_bytes_exn
        Alpha_context.Block_header.unsigned_encoding (shell, contents) in
    Signature.check (Some (Signature.Block_header chain_id)) key_value signature
      unsigned_header in
  if check_signature key_value block then
    Error_monad.return_unit
  else
    Error_monad.fail
      (Build_extensible "Invalid_block_signature"
        (Block_hash.t * Signature.public_key_hash)
        ((Alpha_context.Block_header.hash_value block),
          (Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.hash_value) key_value))).

Definition max_fitness_gap {A : Set} (_ctxt : A) : int64 := 1.

Definition check_fitness_gap
  (ctxt : Alpha_context.context) (block : Alpha_context.Block_header.t)
  : M? unit :=
  let current_fitness := Alpha_context.Fitness.current ctxt in
  let? announced_fitness :=
    Alpha_context.Fitness.to_int64
      block.(Alpha_context.Block_header.t.shell).(Block_header.shell_header.fitness)
    in
  let gap := announced_fitness -i64 current_fitness in
  if (gap <=i64 0) || ((max_fitness_gap ctxt) <i64 gap) then
    Error_monad.error_value
      (Build_extensible "Invalid_fitness_gap" (int64 * int64)
        ((max_fitness_gap ctxt), gap))
  else
    Error_monad.ok_unit.

Definition last_of_a_cycle
  (ctxt : Alpha_context.context) (l_value : Alpha_context.Level.t) : bool :=
  (Int32.succ l_value.(Alpha_context.Level.t.cycle_position)) =i32
  (Alpha_context.Constants.blocks_per_cycle ctxt).

Definition dawn_of_a_new_cycle (ctxt : Alpha_context.context)
  : option Alpha_context.Cycle.t :=
  let level := Alpha_context.Level.current ctxt in
  if last_of_a_cycle ctxt level then
    Some level.(Alpha_context.Level.t.cycle)
  else
    None.

Definition minimum_allowed_endorsements
  (ctxt : Alpha_context.context) (block_delay : Alpha_context.Period.period)
  : int :=
  let minimum := Alpha_context.Constants.initial_endorsers ctxt in
  let delay_per_missing_endorsement :=
    Alpha_context.Period.to_seconds
      (Alpha_context.Constants.delay_per_missing_endorsement ctxt) in
  let reduced_time_constraint :=
    let delay := Alpha_context.Period.to_seconds block_delay in
    if delay_per_missing_endorsement =i64 0 then
      delay
    else
      delay /i64 delay_per_missing_endorsement in
  if (Int64.of_int minimum) <i64 reduced_time_constraint then
    0
  else
    minimum -i (Int64.to_int reduced_time_constraint).

Definition minimal_valid_time
  (ctxt : Alpha_context.context) (priority : int) (endorsing_power : int)
  : M? Time.t :=
  let predecessor_timestamp := Alpha_context.Timestamp.current ctxt in
  let? minimal_time := minimal_time ctxt priority predecessor_timestamp in
  let minimal_required_endorsements :=
    Alpha_context.Constants.initial_endorsers ctxt in
  let delay_per_missing_endorsement :=
    Alpha_context.Constants.delay_per_missing_endorsement ctxt in
  let missing_endorsements :=
    Compare.Int.(Compare.S.max) 0
      (minimal_required_endorsements -i endorsing_power) in
  let? delay :=
    Alpha_context.Period.mult (Int32.of_int missing_endorsements)
      delay_per_missing_endorsement in
  return? (Time.add minimal_time (Alpha_context.Period.to_seconds delay)).
