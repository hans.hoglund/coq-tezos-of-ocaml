Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Constants_storage.
Require TezosOfOCaml.Proto_2021_01.Contract_repr.
Require TezosOfOCaml.Proto_2021_01.Cycle_repr.
Require TezosOfOCaml.Proto_2021_01.Level_repr.
Require TezosOfOCaml.Proto_2021_01.Level_storage.
Require TezosOfOCaml.Proto_2021_01.Manager_repr.
Require TezosOfOCaml.Proto_2021_01.Nonce_storage.
Require TezosOfOCaml.Proto_2021_01.Raw_context.
Require TezosOfOCaml.Proto_2021_01.Receipt_repr.
Require TezosOfOCaml.Proto_2021_01.Roll_storage.
Require TezosOfOCaml.Proto_2021_01.Storage.
Require TezosOfOCaml.Proto_2021_01.Storage_sigs.
Require TezosOfOCaml.Proto_2021_01.Tez_repr.

Module frozen_balance.
  Record record : Set := Build {
    deposit : Tez_repr.t;
    fees : Tez_repr.t;
    rewards : Tez_repr.t }.
  Definition with_deposit deposit (r : record) :=
    Build deposit r.(fees) r.(rewards).
  Definition with_fees fees (r : record) :=
    Build r.(deposit) fees r.(rewards).
  Definition with_rewards rewards (r : record) :=
    Build r.(deposit) r.(fees) rewards.
End frozen_balance.
Definition frozen_balance := frozen_balance.record.

Definition frozen_balance_encoding : Data_encoding.encoding frozen_balance :=
  Data_encoding.conv
    (fun (function_parameter : frozen_balance) =>
      let '{|
        frozen_balance.deposit := deposit;
          frozen_balance.fees := fees;
          frozen_balance.rewards := rewards
          |} := function_parameter in
      (deposit, fees, rewards))
    (fun (function_parameter : Tez_repr.t * Tez_repr.t * Tez_repr.t) =>
      let '(deposit, fees, rewards) := function_parameter in
      {| frozen_balance.deposit := deposit; frozen_balance.fees := fees;
        frozen_balance.rewards := rewards |}) None
    (Data_encoding.obj3
      (Data_encoding.req None None "deposit" Tez_repr.encoding)
      (Data_encoding.req None None "fees" Tez_repr.encoding)
      (Data_encoding.req None None "rewards" Tez_repr.encoding)).

Module Balance_too_low_for_deposit.
  Record record : Set := Build {
    delegate : Signature.public_key_hash;
    deposit : Tez_repr.t;
    balance : Tez_repr.t }.
  Definition with_delegate delegate (r : record) :=
    Build delegate r.(deposit) r.(balance).
  Definition with_deposit deposit (r : record) :=
    Build r.(delegate) deposit r.(balance).
  Definition with_balance balance (r : record) :=
    Build r.(delegate) r.(deposit) balance.
End Balance_too_low_for_deposit.
Definition Balance_too_low_for_deposit := Balance_too_low_for_deposit.record.

(** Init function; without side-effects in Coq *)
Definition init_module : unit :=
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent "delegate.no_deletion"
      "Forbidden delegate deletion" "Tried to unregister a delegate"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (delegate : Signature.public_key_hash) =>
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Delegate deletion is forbidden ("
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.Char_literal ")" % char
                      CamlinternalFormatBasics.End_of_format)))
                "Delegate deletion is forbidden (%a)")
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp)
              delegate))
      (Data_encoding.obj1
        (Data_encoding.req None None "delegate"
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "No_deletion" then
            let 'c := cast Signature.public_key_hash payload in
            Some c
          else None
        end)
      (fun (c : Signature.public_key_hash) =>
        Build_extensible "No_deletion" Signature.public_key_hash c) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "delegate.already_active" "Delegate already active"
      "Useless delegate reactivation"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "The delegate is still active, no need to refresh it"
                  CamlinternalFormatBasics.End_of_format)
                "The delegate is still active, no need to refresh it")))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Active_delegate" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Active_delegate" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary "delegate.unchanged"
      "Unchanged delegated" "Contract already delegated to the given delegate"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "The contract is already delegated to the same delegate"
                  CamlinternalFormatBasics.End_of_format)
                "The contract is already delegated to the same delegate")))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Current_delegate" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Current_delegate" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "delegate.empty_delegate_account" "Empty delegate account"
      "Cannot register a delegate when its implicit account is empty"
      (Some
        (fun (ppf : Format.formatter) =>
          fun (delegate : Signature.public_key_hash) =>
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal
                  "Delegate registration is forbidden when the delegate\n           implicit account is empty ("
                  (CamlinternalFormatBasics.Alpha
                    (CamlinternalFormatBasics.Char_literal ")" % char
                      CamlinternalFormatBasics.End_of_format)))
                "Delegate registration is forbidden when the delegate\n           implicit account is empty (%a)")
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp)
              delegate))
      (Data_encoding.obj1
        (Data_encoding.req None None "delegate"
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding)))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Empty_delegate_account" then
            let 'c := cast Signature.public_key_hash payload in
            Some c
          else None
        end)
      (fun (c : Signature.public_key_hash) =>
        Build_extensible "Empty_delegate_account" Signature.public_key_hash c)
    in
  Error_monad.register_error_kind Error_monad.Temporary
    "delegate.balance_too_low_for_deposit" "Balance too low for deposit"
    "Cannot freeze deposit when the balance is too low"
    (Some
      (fun (ppf : Format.formatter) =>
        fun (function_parameter :
          Signature.public_key_hash * Tez_repr.t * Tez_repr.t) =>
          let '(delegate, balance, deposit) := function_parameter in
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal "Delegate "
                (CamlinternalFormatBasics.Alpha
                  (CamlinternalFormatBasics.String_literal
                    " has a too low balance ("
                    (CamlinternalFormatBasics.Alpha
                      (CamlinternalFormatBasics.String_literal ") to deposit "
                        (CamlinternalFormatBasics.Alpha
                          CamlinternalFormatBasics.End_of_format))))))
              "Delegate %a has a too low balance (%a) to deposit %a")
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.pp) delegate
            Tez_repr.pp balance Tez_repr.pp deposit))
    (Data_encoding.obj3
      (Data_encoding.req None None "delegate"
        Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding))
      (Data_encoding.req None None "balance" Tez_repr.encoding)
      (Data_encoding.req None None "deposit" Tez_repr.encoding))
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Balance_too_low_for_deposit" then
          let '{|
            Balance_too_low_for_deposit.delegate := delegate;
              Balance_too_low_for_deposit.deposit := deposit;
              Balance_too_low_for_deposit.balance := balance
              |} := cast Balance_too_low_for_deposit payload in
          Some (delegate, balance, deposit)
        else None
      end)
    (fun (function_parameter :
      Signature.public_key_hash * Tez_repr.t * Tez_repr.t) =>
      let '(delegate, balance, deposit) := function_parameter in
      Build_extensible "Balance_too_low_for_deposit" Balance_too_low_for_deposit
        {| Balance_too_low_for_deposit.delegate := delegate;
          Balance_too_low_for_deposit.deposit := deposit;
          Balance_too_low_for_deposit.balance := balance |}).

Definition link
  (c : Raw_context.t) (contract : Contract_repr.t)
  (delegate : Signature.public_key_hash) : M=? Raw_context.t :=
  let=? balance :=
    Storage.Contract.Balance.(Storage_sigs.Indexed_data_storage.get) c contract
    in
  let=? c := Roll_storage.Delegate.add_amount c delegate balance in
  Error_monad.op_gtpipeeq
    (Storage.Contract.Delegated.(Storage_sigs.Data_set_storage.add)
      (c, (Contract_repr.implicit_contract delegate)) contract) Error_monad.ok.

Definition unlink (c : Raw_context.t) (contract : Contract_repr.t)
  : M=? Raw_context.t :=
  let=? balance :=
    Storage.Contract.Balance.(Storage_sigs.Indexed_data_storage.get) c contract
    in
  let=? function_parameter :=
    Storage.Contract.Delegate.(Storage_sigs.Indexed_data_storage.get_option) c
      contract in
  match function_parameter with
  | None => Error_monad._return c
  | Some delegate =>
    let=? c := Roll_storage.Delegate.remove_amount c delegate balance in
    Error_monad.op_gtpipeeq
      (Storage.Contract.Delegated.(Storage_sigs.Data_set_storage.del)
        (c, (Contract_repr.implicit_contract delegate)) contract) Error_monad.ok
  end.

Definition known (c : Raw_context.t) (delegate : Signature.public_key_hash)
  : M=? bool :=
  let=? function_parameter :=
    Storage.Contract.Manager.(Storage_sigs.Indexed_data_storage.get_option) c
      (Contract_repr.implicit_contract delegate) in
  match function_parameter with
  | (None | Some (Manager_repr.Hash _)) => Error_monad.return_false
  | Some (Manager_repr.Public_key _) => Error_monad.return_true
  end.

Definition registered (c : Raw_context.t) (delegate : Signature.public_key_hash)
  : M=? bool :=
  let=? function_parameter :=
    Storage.Contract.Delegate.(Storage_sigs.Indexed_data_storage.get_option) c
      (Contract_repr.implicit_contract delegate) in
  match function_parameter with
  | Some current_delegate =>
    return=?
      (Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.equal) delegate
        current_delegate)
  | None => return=? false
  end.

Definition init_value
  (ctxt : Raw_context.t) (contract : Contract_repr.t)
  (delegate : Signature.public_key_hash) : M=? Raw_context.t :=
  let=? known_delegate := known ctxt delegate in
  let=? '_ :=
    return=
      (Error_monad.error_unless known_delegate
        (Build_extensible "Unregistered_delegate" Signature.public_key_hash
          delegate)) in
  let=? is_registered := registered ctxt delegate in
  let=? '_ :=
    return=
      (Error_monad.error_unless is_registered
        (Build_extensible "Unregistered_delegate" Signature.public_key_hash
          delegate)) in
  let=? ctxt :=
    Storage.Contract.Delegate.(Storage_sigs.Indexed_data_storage.init_value)
      ctxt contract delegate in
  link ctxt contract delegate.

Definition get
  : Raw_context.t -> Contract_repr.t -> M=? (option Signature.public_key_hash) :=
  Roll_storage.get_contract_delegate.

Definition set
  (c : Raw_context.t) (contract : Contract_repr.t)
  (delegate : option Signature.public_key_hash) : M=? Raw_context.t :=
  match delegate with
  | None =>
    let delete (function_parameter : unit) : M=? Raw_context.t :=
      let '_ := function_parameter in
      let=? c := unlink c contract in
      Error_monad.op_gtpipeeq
        (Storage.Contract.Delegate.(Storage_sigs.Indexed_data_storage.remove) c
          contract) Error_monad.ok in
    match Contract_repr.is_implicit contract with
    | Some pkh =>
      let=? is_registered := registered c pkh in
      if is_registered then
        Error_monad.fail
          (Build_extensible "No_deletion" Signature.public_key_hash pkh)
      else
        delete tt
    | None => delete tt
    end
  | Some delegate =>
    let=? known_delegate := known c delegate in
    let=? registered_delegate := registered c delegate in
    let self_delegation :=
      match Contract_repr.is_implicit contract with
      | Some pkh =>
        Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.equal) pkh
          delegate
      | None => false
      end in
    if
      (Pervasives.not known_delegate) ||
      (Pervasives.not (registered_delegate || self_delegation))
    then
      Error_monad.fail
        (Build_extensible "Unregistered_delegate" Signature.public_key_hash
          delegate)
    else
      let=? '_ :=
        let=? function_parameter :=
          Storage.Contract.Delegate.(Storage_sigs.Indexed_data_storage.get_option)
            c contract in
        match
          (function_parameter,
            match function_parameter with
            | Some current_delegate =>
              Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.equal)
                delegate current_delegate
            | _ => false
            end) with
        | (Some current_delegate, true) =>
          if self_delegation then
            let=? function_parameter :=
              Roll_storage.Delegate.is_inactive c delegate in
            match function_parameter with
            | true => Error_monad.return_unit
            | false =>
              Error_monad.fail (Build_extensible "Active_delegate" unit tt)
            end
          else
            Error_monad.fail (Build_extensible "Current_delegate" unit tt)
        | ((None | Some _), _) => Error_monad.return_unit
        end in
      let=? '_ :=
        match Contract_repr.is_implicit contract with
        | Some pkh =>
          let=? is_registered := registered c pkh in
          if (Pervasives.not self_delegation) && is_registered then
            Error_monad.fail
              (Build_extensible "No_deletion" Signature.public_key_hash pkh)
          else
            Error_monad.return_unit
        | None => Error_monad.return_unit
        end in
      let= _exists :=
        Storage.Contract.Balance.(Storage_sigs.Indexed_data_storage.mem) c
          contract in
      let=? '_ :=
        return=
          (Error_monad.error_when (self_delegation && (Pervasives.not _exists))
            (Build_extensible "Empty_delegate_account" Signature.public_key_hash
              delegate)) in
      let=? c := unlink c contract in
      let= c :=
        Storage.Contract.Delegate.(Storage_sigs.Indexed_data_storage.init_set) c
          contract delegate in
      let=? c := link c contract delegate in
      if self_delegation then
        let= c :=
          Storage.Delegates.(Storage_sigs.Data_set_storage.add) c delegate in
        Roll_storage.Delegate.set_active c delegate
      else
        Error_monad._return c
  end.

Definition remove (ctxt : Raw_context.t) (contract : Contract_repr.t)
  : M=? Raw_context.t := unlink ctxt contract.

Definition delegated_contracts
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  : M= (list Contract_repr.t) :=
  let contract := Contract_repr.implicit_contract delegate in
  Storage.Contract.Delegated.(Storage_sigs.Data_set_storage.elements)
    (ctxt, contract).

Definition get_frozen_deposit
  (ctxt : Raw_context.t) (contract : Contract_repr.t) (cycle : Cycle_repr.t)
  : M=? Tez_repr.t :=
  Error_monad.op_gtpipeeqquestion
    (Storage.Contract.Frozen_deposits.(Storage_sigs.Indexed_data_storage.get_option)
      (ctxt, contract) cycle) (fun x_1 => Option.value x_1 Tez_repr.zero).

Definition credit_frozen_deposit
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  (cycle : Cycle_repr.t) (amount : Tez_repr.t) : M=? Raw_context.t :=
  let contract := Contract_repr.implicit_contract delegate in
  let=? old_amount := get_frozen_deposit ctxt contract cycle in
  let=? new_amount := return= (Tez_repr.op_plusquestion old_amount amount) in
  let= ctxt :=
    Storage.Contract.Frozen_deposits.(Storage_sigs.Indexed_data_storage.init_set)
      (ctxt, contract) cycle new_amount in
  Error_monad.op_gtpipeeq
    (Storage.Delegates_with_frozen_balance.(Storage_sigs.Data_set_storage.add)
      (ctxt, cycle) delegate) Error_monad.ok.

Definition freeze_deposit
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  (amount : Tez_repr.t) : M=? Raw_context.t :=
  let '{| Level_repr.t.cycle := cycle |} := Level_storage.current ctxt in
  let=? ctxt := Roll_storage.Delegate.set_active ctxt delegate in
  let contract := Contract_repr.implicit_contract delegate in
  let=? balance :=
    Storage.Contract.Balance.(Storage_sigs.Indexed_data_storage.get) ctxt
      contract in
  let=? new_balance :=
    return=
      (Error_monad.record_trace
        (Build_extensible "Balance_too_low_for_deposit"
          Balance_too_low_for_deposit
          {| Balance_too_low_for_deposit.delegate := delegate;
            Balance_too_low_for_deposit.deposit := amount;
            Balance_too_low_for_deposit.balance := balance |})
        (Tez_repr.op_minusquestion balance amount)) in
  let=? ctxt :=
    Storage.Contract.Balance.(Storage_sigs.Indexed_data_storage.set) ctxt
      contract new_balance in
  credit_frozen_deposit ctxt delegate cycle amount.

Definition get_frozen_fees
  (ctxt : Raw_context.t) (contract : Contract_repr.t) (cycle : Cycle_repr.t)
  : M=? Tez_repr.t :=
  Error_monad.op_gtpipeeqquestion
    (Storage.Contract.Frozen_fees.(Storage_sigs.Indexed_data_storage.get_option)
      (ctxt, contract) cycle) (fun x_1 => Option.value x_1 Tez_repr.zero).

Definition credit_frozen_fees
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  (cycle : Cycle_repr.t) (amount : Tez_repr.t) : M=? Raw_context.t :=
  let contract := Contract_repr.implicit_contract delegate in
  let=? old_amount := get_frozen_fees ctxt contract cycle in
  let=? new_amount := return= (Tez_repr.op_plusquestion old_amount amount) in
  let= ctxt :=
    Storage.Contract.Frozen_fees.(Storage_sigs.Indexed_data_storage.init_set)
      (ctxt, contract) cycle new_amount in
  Error_monad.op_gtpipeeq
    (Storage.Delegates_with_frozen_balance.(Storage_sigs.Data_set_storage.add)
      (ctxt, cycle) delegate) Error_monad.ok.

Definition freeze_fees
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  (amount : Tez_repr.t) : M=? Raw_context.t :=
  let '{| Level_repr.t.cycle := cycle |} := Level_storage.current ctxt in
  let=? ctxt := Roll_storage.Delegate.add_amount ctxt delegate amount in
  credit_frozen_fees ctxt delegate cycle amount.

Definition burn_fees
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  (cycle : Cycle_repr.t) (amount : Tez_repr.t) : M=? Raw_context.t :=
  let contract := Contract_repr.implicit_contract delegate in
  let=? old_amount := get_frozen_fees ctxt contract cycle in
  let=? '(new_amount, ctxt) :=
    match Tez_repr.op_minusquestion old_amount amount with
    | Pervasives.Ok new_amount =>
      let=? ctxt := Roll_storage.Delegate.remove_amount ctxt delegate amount in
      return=? (new_amount, ctxt)
    | Pervasives.Error _ =>
      let=? ctxt := Roll_storage.Delegate.remove_amount ctxt delegate old_amount
        in
      return=? (Tez_repr.zero, ctxt)
    end in
  Error_monad.op_gtpipeeq
    (Storage.Contract.Frozen_fees.(Storage_sigs.Indexed_data_storage.init_set)
      (ctxt, contract) cycle new_amount) Error_monad.ok.

Definition get_frozen_rewards
  (ctxt : Raw_context.t) (contract : Contract_repr.t) (cycle : Cycle_repr.t)
  : M=? Tez_repr.t :=
  Error_monad.op_gtpipeeqquestion
    (Storage.Contract.Frozen_rewards.(Storage_sigs.Indexed_data_storage.get_option)
      (ctxt, contract) cycle) (fun x_1 => Option.value x_1 Tez_repr.zero).

Definition credit_frozen_rewards
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  (cycle : Cycle_repr.t) (amount : Tez_repr.t) : M=? Raw_context.t :=
  let contract := Contract_repr.implicit_contract delegate in
  let=? old_amount := get_frozen_rewards ctxt contract cycle in
  let=? new_amount := return= (Tez_repr.op_plusquestion old_amount amount) in
  let= ctxt :=
    Storage.Contract.Frozen_rewards.(Storage_sigs.Indexed_data_storage.init_set)
      (ctxt, contract) cycle new_amount in
  Error_monad.op_gtpipeeq
    (Storage.Delegates_with_frozen_balance.(Storage_sigs.Data_set_storage.add)
      (ctxt, cycle) delegate) Error_monad.ok.

Definition freeze_rewards
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  (amount : Tez_repr.t) : M=? Raw_context.t :=
  let '{| Level_repr.t.cycle := cycle |} := Level_storage.current ctxt in
  credit_frozen_rewards ctxt delegate cycle amount.

Definition burn_rewards
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  (cycle : Cycle_repr.t) (amount : Tez_repr.t) : M=? Raw_context.t :=
  let contract := Contract_repr.implicit_contract delegate in
  let=? old_amount := get_frozen_rewards ctxt contract cycle in
  let new_amount :=
    match Tez_repr.op_minusquestion old_amount amount with
    | Pervasives.Error _ => Tez_repr.zero
    | Pervasives.Ok new_amount => new_amount
    end in
  Error_monad.op_gtpipeeq
    (Storage.Contract.Frozen_rewards.(Storage_sigs.Indexed_data_storage.init_set)
      (ctxt, contract) cycle new_amount) Error_monad.ok.

Definition unfreeze
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  (cycle : Cycle_repr.t) : M=? (Raw_context.t * Receipt_repr.balance_updates) :=
  let contract := Contract_repr.implicit_contract delegate in
  let=? deposit := get_frozen_deposit ctxt contract cycle in
  let=? fees := get_frozen_fees ctxt contract cycle in
  let=? rewards := get_frozen_rewards ctxt contract cycle in
  let=? balance :=
    Storage.Contract.Balance.(Storage_sigs.Indexed_data_storage.get) ctxt
      contract in
  let=? unfrozen_amount := return= (Tez_repr.op_plusquestion deposit fees) in
  let=? unfrozen_amount :=
    return= (Tez_repr.op_plusquestion unfrozen_amount rewards) in
  let=? balance := return= (Tez_repr.op_plusquestion balance unfrozen_amount) in
  let=? ctxt :=
    Storage.Contract.Balance.(Storage_sigs.Indexed_data_storage.set) ctxt
      contract balance in
  let=? ctxt := Roll_storage.Delegate.add_amount ctxt delegate rewards in
  let= ctxt :=
    Storage.Contract.Frozen_deposits.(Storage_sigs.Indexed_data_storage.remove)
      (ctxt, contract) cycle in
  let= ctxt :=
    Storage.Contract.Frozen_fees.(Storage_sigs.Indexed_data_storage.remove)
      (ctxt, contract) cycle in
  let= ctxt :=
    Storage.Contract.Frozen_rewards.(Storage_sigs.Indexed_data_storage.remove)
      (ctxt, contract) cycle in
  return=?
    (ctxt,
      (Receipt_repr.cleanup_balance_updates
        [
          ((Receipt_repr.Deposits delegate cycle),
            (Receipt_repr.Debited deposit), Receipt_repr.Block_application);
          ((Receipt_repr.Fees delegate cycle), (Receipt_repr.Debited fees),
            Receipt_repr.Block_application);
          ((Receipt_repr.Rewards delegate cycle),
            (Receipt_repr.Debited rewards), Receipt_repr.Block_application);
          ((Receipt_repr.Contract (Contract_repr.implicit_contract delegate)),
            (Receipt_repr.Credited unfrozen_amount),
            Receipt_repr.Block_application)
        ])).

Definition cycle_end
  (ctxt : Raw_context.context) (last_cycle : Cycle_repr.cycle)
  (unrevealed : list Nonce_storage.unrevealed)
  : M=?
    (Raw_context.context *
      list
        (Receipt_repr.balance * Receipt_repr.balance_update *
          Receipt_repr.update_origin) * list Signature.public_key_hash) :=
  let preserved := Constants_storage.preserved_cycles ctxt in
  let=? '(ctxt, balance_updates) :=
    match Cycle_repr.pred last_cycle with
    | None => Error_monad._return (ctxt, nil)
    | Some revealed_cycle =>
      Error_monad.fold_left_s
        (fun (function_parameter :
          Raw_context.t *
            list
              (Receipt_repr.balance * Receipt_repr.balance_update *
                Receipt_repr.update_origin)) =>
          let '(ctxt, balance_updates) := function_parameter in
          fun (u : Nonce_storage.unrevealed) =>
            let=? ctxt :=
              burn_fees ctxt u.(Storage.Cycle.unrevealed_nonce.delegate)
                revealed_cycle u.(Storage.Cycle.unrevealed_nonce.fees) in
            let=? ctxt :=
              burn_rewards ctxt u.(Storage.Cycle.unrevealed_nonce.delegate)
                revealed_cycle u.(Storage.Cycle.unrevealed_nonce.rewards) in
            let bus :=
              [
                ((Receipt_repr.Fees u.(Storage.Cycle.unrevealed_nonce.delegate)
                  revealed_cycle),
                  (Receipt_repr.Debited
                    u.(Storage.Cycle.unrevealed_nonce.fees)),
                  Receipt_repr.Block_application);
                ((Receipt_repr.Rewards
                  u.(Storage.Cycle.unrevealed_nonce.delegate)
                  revealed_cycle),
                  (Receipt_repr.Debited
                    u.(Storage.Cycle.unrevealed_nonce.rewards)),
                  Receipt_repr.Block_application)
              ] in
            return=? (ctxt, (Pervasives.op_at bus balance_updates))) (ctxt, nil)
        unrevealed
    end in
  match Cycle_repr.sub last_cycle preserved with
  | None => Error_monad._return (ctxt, balance_updates, nil)
  | Some unfrozen_cycle =>
    let=? '(ctxt, balance_updates) :=
      Storage.Delegates_with_frozen_balance.(Storage_sigs.Data_set_storage.fold)
        (ctxt, unfrozen_cycle) (Pervasives.Ok (ctxt, balance_updates))
        (fun (delegate : Signature.public_key_hash) =>
          fun (acc_value :
            M?
              (Raw_context.context *
                list
                  (Receipt_repr.balance * Receipt_repr.balance_update *
                    Receipt_repr.update_origin))) =>
            let=? '(ctxt, bus) := return= acc_value in
            let=? '(ctxt, balance_updates) :=
              unfreeze ctxt delegate unfrozen_cycle in
            return=? (ctxt, (Pervasives.op_at balance_updates bus))) in
    let= ctxt :=
      Storage.Delegates_with_frozen_balance.(Storage_sigs.Data_set_storage.clear)
        (ctxt, unfrozen_cycle) in
    let=? '(ctxt, deactivated) :=
      Storage.Active_delegates_with_rolls.(Storage_sigs.Data_set_storage.fold)
        ctxt (Pervasives.Ok (ctxt, nil))
        (fun (delegate : Signature.public_key_hash) =>
          fun (acc_value : M? (Raw_context.t * list Signature.public_key_hash))
            =>
            let=? '(ctxt, deactivated) := return= acc_value in
            let=? cycle :=
              Storage.Contract.Delegate_desactivation.(Storage_sigs.Indexed_data_storage.get)
                ctxt (Contract_repr.implicit_contract delegate) in
            if Cycle_repr.op_lteq cycle last_cycle then
              let=? ctxt := Roll_storage.Delegate.set_inactive ctxt delegate in
              return=? (ctxt, (cons delegate deactivated))
            else
              Error_monad._return (ctxt, deactivated)) in
    return=? (ctxt, balance_updates, deactivated)
  end.

Definition punish
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  (cycle : Cycle_repr.t) : M=? (Raw_context.t * frozen_balance) :=
  let contract := Contract_repr.implicit_contract delegate in
  let=? deposit := get_frozen_deposit ctxt contract cycle in
  let=? fees := get_frozen_fees ctxt contract cycle in
  let=? rewards := get_frozen_rewards ctxt contract cycle in
  let=? ctxt := Roll_storage.Delegate.remove_amount ctxt delegate deposit in
  let=? ctxt := Roll_storage.Delegate.remove_amount ctxt delegate fees in
  let= ctxt :=
    Storage.Contract.Frozen_deposits.(Storage_sigs.Indexed_data_storage.remove)
      (ctxt, contract) cycle in
  let= ctxt :=
    Storage.Contract.Frozen_fees.(Storage_sigs.Indexed_data_storage.remove)
      (ctxt, contract) cycle in
  let= ctxt :=
    Storage.Contract.Frozen_rewards.(Storage_sigs.Indexed_data_storage.remove)
      (ctxt, contract) cycle in
  return=?
    (ctxt,
      {| frozen_balance.deposit := deposit; frozen_balance.fees := fees;
        frozen_balance.rewards := rewards |}).

Definition has_frozen_balance
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  (cycle : Cycle_repr.t) : M=? bool :=
  let contract := Contract_repr.implicit_contract delegate in
  let=? deposit := get_frozen_deposit ctxt contract cycle in
  if Tez_repr.op_ltgt deposit Tez_repr.zero then
    Error_monad.return_true
  else
    let=? fees := get_frozen_fees ctxt contract cycle in
    if Tez_repr.op_ltgt fees Tez_repr.zero then
      Error_monad.return_true
    else
      let=? rewards := get_frozen_rewards ctxt contract cycle in
      return=? (Tez_repr.op_ltgt rewards Tez_repr.zero).

Definition frozen_balance_by_cycle_encoding
  : Data_encoding.encoding (Cycle_repr.Map.(S.MAP.t) frozen_balance) :=
  Data_encoding.conv Cycle_repr.Map.(S.MAP.bindings)
    (List.fold_left
      (fun (m : Cycle_repr.Map.(S.MAP.t) frozen_balance) =>
        fun (function_parameter : Cycle_repr.t * frozen_balance) =>
          let '(c, b_value) := function_parameter in
          Cycle_repr.Map.(S.MAP.add) c b_value m) Cycle_repr.Map.(S.MAP.empty))
    None
    (Data_encoding.list_value None
      (Data_encoding.merge_objs
        (Data_encoding.obj1
          (Data_encoding.req None None "cycle" Cycle_repr.encoding))
        frozen_balance_encoding)).

Definition empty_frozen_balance : frozen_balance :=
  {| frozen_balance.deposit := Tez_repr.zero;
    frozen_balance.fees := Tez_repr.zero;
    frozen_balance.rewards := Tez_repr.zero |}.

Definition frozen_balance_by_cycle
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  : M= (Cycle_repr.Map.(S.MAP.t) frozen_balance) :=
  let contract := Contract_repr.implicit_contract delegate in
  let map {A : Set} : Cycle_repr.Map.(S.MAP.t) A :=
    Cycle_repr.Map.(S.MAP.empty) in
  let= map :=
    Storage.Contract.Frozen_deposits.(Storage_sigs.Indexed_data_storage.fold)
      (ctxt, contract) map
      (fun (cycle : Cycle_repr.t) =>
        fun (amount : Tez_repr.t) =>
          fun (map : Cycle_repr.Map.(S.MAP.t) frozen_balance) =>
            Lwt._return
              (Cycle_repr.Map.(S.MAP.add) cycle
                (frozen_balance.with_deposit amount empty_frozen_balance) map))
    in
  let= map :=
    Storage.Contract.Frozen_fees.(Storage_sigs.Indexed_data_storage.fold)
      (ctxt, contract) map
      (fun (cycle : Cycle_repr.t) =>
        fun (amount : Tez_repr.t) =>
          fun (map : Cycle_repr.Map.(S.MAP.t) frozen_balance) =>
            let balance :=
              match Cycle_repr.Map.(S.MAP.find_opt) cycle map with
              | None => empty_frozen_balance
              | Some balance => balance
              end in
            Lwt._return
              (Cycle_repr.Map.(S.MAP.add) cycle
                (frozen_balance.with_fees amount balance) map)) in
  Storage.Contract.Frozen_rewards.(Storage_sigs.Indexed_data_storage.fold)
    (ctxt, contract) map
    (fun (cycle : Cycle_repr.t) =>
      fun (amount : Tez_repr.t) =>
        fun (map : Cycle_repr.Map.(S.MAP.t) frozen_balance) =>
          let balance :=
            match Cycle_repr.Map.(S.MAP.find_opt) cycle map with
            | None => empty_frozen_balance
            | Some balance => balance
            end in
          Lwt._return
            (Cycle_repr.Map.(S.MAP.add) cycle
              (frozen_balance.with_rewards amount balance) map)).

Definition frozen_balance_value
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  : M=? Tez_repr.t :=
  let contract := Contract_repr.implicit_contract delegate in
  let balance {A : Set} : Pervasives.result Tez_repr.t A :=
    Pervasives.Ok Tez_repr.zero in
  let= balance :=
    Storage.Contract.Frozen_deposits.(Storage_sigs.Indexed_data_storage.fold)
      (ctxt, contract) balance
      (fun (_cycle : Cycle_repr.t) =>
        fun (amount : Tez_repr.t) =>
          fun (acc_value : M? Tez_repr.t) =>
            Lwt._return
              (let? acc_value := acc_value in
              Tez_repr.op_plusquestion acc_value amount)) in
  let= balance :=
    Storage.Contract.Frozen_fees.(Storage_sigs.Indexed_data_storage.fold)
      (ctxt, contract) balance
      (fun (_cycle : Cycle_repr.t) =>
        fun (amount : Tez_repr.t) =>
          fun (acc_value : M? Tez_repr.t) =>
            Lwt._return
              (let? acc_value := acc_value in
              Tez_repr.op_plusquestion acc_value amount)) in
  Storage.Contract.Frozen_rewards.(Storage_sigs.Indexed_data_storage.fold)
    (ctxt, contract) balance
    (fun (_cycle : Cycle_repr.t) =>
      fun (amount : Tez_repr.t) =>
        fun (acc_value : M? Tez_repr.t) =>
          Lwt._return
            (let? acc_value := acc_value in
            Tez_repr.op_plusquestion acc_value amount)).

Definition full_balance
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  : M=? Tez_repr.t :=
  let contract := Contract_repr.implicit_contract delegate in
  let=? frozen_balance_value := frozen_balance_value ctxt delegate in
  let=? balance :=
    Storage.Contract.Balance.(Storage_sigs.Indexed_data_storage.get) ctxt
      contract in
  Lwt._return (Tez_repr.op_plusquestion frozen_balance_value balance).

Definition deactivated
  : Raw_context.t -> Signature.public_key_hash -> M=? bool :=
  Roll_storage.Delegate.is_inactive.

Definition grace_period
  (ctxt : Raw_context.t) (delegate : Signature.public_key_hash)
  : M=? Cycle_repr.t :=
  let contract := Contract_repr.implicit_contract delegate in
  Storage.Contract.Delegate_desactivation.(Storage_sigs.Indexed_data_storage.get)
    ctxt contract.

Definition staking_balance
  (ctxt : Raw_context.context) (delegate : Signature.public_key_hash)
  : M=? Tez_repr.t :=
  let token_per_rolls := Constants_storage.tokens_per_roll ctxt in
  let=? rolls := Roll_storage.count_rolls ctxt delegate in
  let=? change := Roll_storage.get_change ctxt delegate in
  Lwt._return
    (let? balance :=
      Tez_repr.op_starquestion token_per_rolls (Int64.of_int rolls) in
    Tez_repr.op_plusquestion balance change).

Definition delegated_balance
  (ctxt : Raw_context.context) (delegate : Signature.public_key_hash)
  : M=? Tez_repr.t :=
  let contract := Contract_repr.implicit_contract delegate in
  let=? staking_balance := staking_balance ctxt delegate in
  let= self_staking_balance :=
    Storage.Contract.Balance.(Storage_sigs.Indexed_data_storage.get) ctxt
      contract in
  let= self_staking_balance :=
    Storage.Contract.Frozen_deposits.(Storage_sigs.Indexed_data_storage.fold)
      (ctxt, contract) self_staking_balance
      (fun (_cycle : Cycle_repr.t) =>
        fun (amount : Tez_repr.t) =>
          fun (acc_value : M? Tez_repr.t) =>
            Lwt._return
              (let? acc_value := acc_value in
              Tez_repr.op_plusquestion acc_value amount)) in
  let=? self_staking_balance :=
    Storage.Contract.Frozen_fees.(Storage_sigs.Indexed_data_storage.fold)
      (ctxt, contract) self_staking_balance
      (fun (_cycle : Cycle_repr.t) =>
        fun (amount : Tez_repr.t) =>
          fun (acc_value : M? Tez_repr.t) =>
            Lwt._return
              (let? acc_value := acc_value in
              Tez_repr.op_plusquestion acc_value amount)) in
  Lwt._return (Tez_repr.op_minusquestion staking_balance self_staking_balance).

Definition fold {A : Set}
  : Raw_context.t -> A -> (Signature.public_key_hash -> A -> M= A) -> M= A :=
  Storage.Delegates.(Storage_sigs.Data_set_storage.fold).

Definition list_value : Raw_context.t -> M= (list Signature.public_key_hash) :=
  Storage.Delegates.(Storage_sigs.Data_set_storage.elements).
