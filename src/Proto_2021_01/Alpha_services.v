Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Alpha_context.
Require TezosOfOCaml.Proto_2021_01.Constants_services.
Require TezosOfOCaml.Proto_2021_01.Contract_services.
Require TezosOfOCaml.Proto_2021_01.Delegate_services.
Require TezosOfOCaml.Proto_2021_01.Helpers_services.
Require TezosOfOCaml.Proto_2021_01.Nonce_hash.
Require TezosOfOCaml.Proto_2021_01.Sapling_services.
Require TezosOfOCaml.Proto_2021_01.Services_registration.
Require TezosOfOCaml.Proto_2021_01.Storage.
Require TezosOfOCaml.Proto_2021_01.Voting_services.

Definition custom_root {A : Set} : RPC_path.context A := RPC_path.open_root.

Module Seed.
  Module S.
    Definition seed_value
      : RPC_service.service Updater.rpc_context Updater.rpc_context unit unit
        Alpha_context.Seed.seed :=
      RPC_service.post_service
        (Some "Seed of the cycle to which the block belongs.") RPC_query.empty
        Data_encoding.empty Alpha_context.Seed.seed_encoding
        (RPC_path.op_div (RPC_path.op_div custom_root "context") "seed").
  End S.
  
  (** Init function; without side-effects in Coq *)
  Definition init_module : unit :=
    Services_registration.register0 S.seed_value
      (fun (ctxt : Alpha_context.t) =>
        fun (function_parameter : unit) =>
          let '_ := function_parameter in
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            let l_value := Alpha_context.Level.current ctxt in
            Alpha_context.Seed.for_cycle ctxt
              l_value.(Alpha_context.Level.t.cycle)).
  
  Definition get {A : Set} (ctxt : RPC_context.simple A) (block : A)
    : M= (Error_monad.shell_tzresult Alpha_context.Seed.seed) :=
    RPC_context.make_call0 S.seed_value ctxt block tt tt.
End Seed.

Module Nonce.
  Inductive info : Set :=
  | Revealed : Alpha_context.Nonce.t -> info
  | Missing : Nonce_hash.t -> info
  | Forgotten : info.
  
  Definition info_encoding : Data_encoding.encoding info :=
    Data_encoding.union None
      [
        Data_encoding.case_value "Revealed" None (Data_encoding.Tag 0)
          (Data_encoding.obj1
            (Data_encoding.req None None "nonce"
              Alpha_context.Nonce.encoding))
          (fun (function_parameter : info) =>
            match function_parameter with
            | Revealed nonce_value => Some nonce_value
            | _ => None
            end)
          (fun (nonce_value : Alpha_context.Nonce.nonce) =>
            Revealed nonce_value);
        Data_encoding.case_value "Missing" None (Data_encoding.Tag 1)
          (Data_encoding.obj1
            (Data_encoding.req None None "hash" Nonce_hash.encoding))
          (fun (function_parameter : info) =>
            match function_parameter with
            | Missing nonce_value => Some nonce_value
            | _ => None
            end)
          (fun (nonce_value : Nonce_hash.t) => Missing nonce_value);
        Data_encoding.case_value "Forgotten" None (Data_encoding.Tag 2)
          Data_encoding.empty
          (fun (function_parameter : info) =>
            match function_parameter with
            | Forgotten => Some tt
            | _ => None
            end)
          (fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Forgotten)
      ].
  
  Module S.
    Definition get
      : RPC_service.service Updater.rpc_context
        (Updater.rpc_context * Alpha_context.Raw_level.raw_level) unit unit info :=
      RPC_service.get_service (Some "Info about the nonce of a previous block.")
        RPC_query.empty info_encoding
        (RPC_path.op_divcolon
          (RPC_path.op_div (RPC_path.op_div custom_root "context") "nonces")
          Alpha_context.Raw_level.rpc_arg).
  End S.
  
  Definition register (function_parameter : unit) : unit :=
    let '_ := function_parameter in
    Services_registration.register1 S.get
      (fun (ctxt : Alpha_context.t) =>
        fun (raw_level : Alpha_context.Raw_level.raw_level) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            fun (function_parameter : unit) =>
              let '_ := function_parameter in
              let level := Alpha_context.Level.from_raw ctxt None raw_level in
              let= function_parameter := Alpha_context.Nonce.get ctxt level in
              match function_parameter with
              | Pervasives.Ok (Storage.Cycle.Revealed nonce_value) =>
                return=? (Revealed nonce_value)
              |
                Pervasives.Ok
                  (Storage.Cycle.Unrevealed {|
                    Storage.Cycle.unrevealed_nonce.nonce_hash := nonce_hash
                      |}) => return=? (Missing nonce_hash)
              | Pervasives.Error _ => return=? Forgotten
              end).
  
  Definition get {A : Set}
    (ctxt : RPC_context.simple A) (block : A)
    (level : Alpha_context.Raw_level.raw_level)
    : M= (Error_monad.shell_tzresult info) :=
    RPC_context.make_call1 S.get ctxt block level tt tt.
End Nonce.

Module Contract := Contract_services.

Module Constants := Constants_services.

Module Delegate := Delegate_services.

Module Helpers := Helpers_services.

Module Forge := Helpers_services.Forge.

Module Parse := Helpers_services.Parse.

Module Voting := Voting_services.

Module Sapling := Sapling_services.

Definition register (function_parameter : unit) : unit :=
  let '_ := function_parameter in
  let '_ := Contract.register tt in
  let '_ := Constants.register tt in
  let '_ := Delegate.register tt in
  let '_ := Helpers.register tt in
  let '_ := Nonce.register tt in
  let '_ := Voting.register tt in
  Sapling.register tt.
