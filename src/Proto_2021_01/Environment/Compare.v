Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_2021_01.Environment.Char.
Require Proto_2021_01.Environment.Int32.
Require Proto_2021_01.Environment.Int64.
Require Proto_2021_01.Environment.String.
Require Proto_2021_01.Environment.Z.

Module COMPARABLE.
  Record signature {t : Set} : Set := {
    t := t;
    compare : t -> t -> int;
  }.
End COMPARABLE.
Definition COMPARABLE := @COMPARABLE.signature.
Arguments COMPARABLE {_}.

Module S.
  Record signature {t : Set} : Set := {
    t := t;
    op_eq : t -> t -> bool;
    op_ltgt : t -> t -> bool;
    op_lt : t -> t -> bool;
    op_lteq : t -> t -> bool;
    op_gteq : t -> t -> bool;
    op_gt : t -> t -> bool;
    compare : t -> t -> int;
    equal : t -> t -> bool;
    max : t -> t -> t;
    min : t -> t -> t;
  }.
End S.
Definition S := @S.signature.
Arguments S {_}.

Definition Make {P_t : Set} (P : COMPARABLE (t := P_t))
  : S (t := P.(COMPARABLE.t)) :=
  {|
    S.op_eq x y := Z.eqb (P.(COMPARABLE.compare) x y) 0;
    S.op_ltgt x y := negb (Z.eqb (P.(COMPARABLE.compare) x y) 0);
    S.op_lt x y := Z.ltb (P.(COMPARABLE.compare) x y) 0;
    S.op_lteq x y := Z.leb (P.(COMPARABLE.compare) x y) 0;
    S.op_gteq x y := Z.geb (P.(COMPARABLE.compare) x y) 0;
    S.op_gt x y := Z.gtb (P.(COMPARABLE.compare) x y) 0;
    S.compare x y := P.(COMPARABLE.compare) x y;
    S.equal x y := Z.eqb (P.(COMPARABLE.compare) x y) 0;
    S.max x y :=
      if Z.leb (P.(COMPARABLE.compare) x y) 0 then
        y
      else
        x;
    S.min x y :=
      if Z.leb (P.(COMPARABLE.compare) x y) 0 then
        x
      else
        y;
  |}.

(** A utility function built on top of [Make] to make it simpler to use. *)
Definition Make_with_compare {A : Set} (compare : A -> A -> int) : S (t := A) :=
  Make {| COMPARABLE.compare := compare |}.

Definition Char : S (t := ascii) :=
  Make_with_compare Char.compare.

Definition int_of_bool (b : bool) : int :=
  if b then 1 else 0.

Definition Bool : S (t := bool) :=
  Make_with_compare (fun x y => Z.sub (int_of_bool x) (int_of_bool y)).

Definition Int : S (t := int) :=
  Make_with_compare Z.compare.

Lemma Int_eq_refl x : Int.(S.op_eq) x x = true.
  simpl.
  unfold Z.compare.
  now rewrite Z.sub_diag.
Qed.

Definition Int32 : S (t := int32) :=
  Make_with_compare Int32.compare.

Definition Uint32 : S (t := int32) :=
  Make_with_compare Int32.compare.

Definition Int64 : S (t := int64) :=
  Make_with_compare Int64.compare.

Definition Uint64 : S (t := int64) :=
  Make_with_compare Int64.compare.

Definition String : S (t := string) :=
  Make_with_compare String.compare.

Definition Bytes : S (t := bytes) :=
  Make_with_compare String.compare.

Definition Z : S (t := Z.t) :=
  Make_with_compare Z.compare.

Parameter List :
  forall {P_t : Set},
  forall (P : COMPARABLE (t := P_t)),
  S (t := list P.(COMPARABLE.t)).

Parameter Option :
  forall {P_t : Set},
  forall (P : COMPARABLE (t := P_t)),
  S (t := option P.(COMPARABLE.t)).

Module Notations.
  Infix "=i" := Int.(S.op_eq)
    (at level 70, no associativity).
  Infix "<>i" := Int.(S.op_ltgt)
    (at level 70, no associativity).
  Infix "<=i" := Int.(S.op_lteq)
    (at level 70, no associativity).
  Infix "<i" := Int.(S.op_lt)
    (at level 70, no associativity).
  Infix ">=i" := Int.(S.op_gteq)
    (at level 70, no associativity).
  Infix ">i" := Int.(S.op_gt)
    (at level 70, no associativity).

  Infix "=i32" := Int32.(S.op_eq)
    (at level 70, no associativity).
  Infix "<>i32" := Int32.(S.op_ltgt)
    (at level 70, no associativity).
  Infix "<=i32" := Int32.(S.op_lteq)
    (at level 70, no associativity).
  Infix "<i32" := Int32.(S.op_lt)
    (at level 70, no associativity).
  Infix ">=i32" := Int32.(S.op_gteq)
    (at level 70, no associativity).
  Infix ">i32" := Int32.(S.op_gt)
    (at level 70, no associativity).

  Infix "=i64" := Int64.(S.op_eq)
    (at level 70, no associativity).
  Infix "<>i64" := Int64.(S.op_ltgt)
    (at level 70, no associativity).
  Infix "<=i64" := Int64.(S.op_lteq)
    (at level 70, no associativity).
  Infix "<i64" := Int64.(S.op_lt)
    (at level 70, no associativity).
  Infix ">=i64" := Int64.(S.op_gteq)
    (at level 70, no associativity).
  Infix ">i64" := Int64.(S.op_gt)
    (at level 70, no associativity).

  Infix "=Z" := Z.(S.op_eq)
    (at level 70, no associativity).
  Infix "<>Z" := Z.(S.op_ltgt)
    (at level 70, no associativity).
  Infix "<=Z" := Z.(S.op_lteq)
    (at level 70, no associativity).
  Infix "<Z" := Z.(S.op_lt)
    (at level 70, no associativity).
  Infix ">=Z" := Z.(S.op_gteq)
    (at level 70, no associativity).
  Infix ">Z" := Z.(S.op_gt)
    (at level 70, no associativity).
End Notations.
