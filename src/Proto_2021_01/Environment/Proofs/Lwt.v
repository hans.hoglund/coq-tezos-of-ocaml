Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.

Definition eval {A : Set} (x : M= A) : A :=
  match x with
  | Lwt.Return v => v
  end.

Lemma bind_return {A : Set} (e : M= A) :
  (let= x := e in return= x) =
  e.
  now destruct e.
Qed.
