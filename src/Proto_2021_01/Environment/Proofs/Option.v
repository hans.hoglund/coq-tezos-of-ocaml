Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.

Definition terminates {A : Set} (x : M* A) : Prop :=
  match x with
  | None => False
  | Some _ => True
  end.

Lemma bind_return {A : Set} (e : M* A) :
  (let* x := e in return* x) =
  e.
  now destruct e.
Qed.

Lemma rewrite_bind_left {A A' B : Set}
  (e1 : M* A) (e1' : M* A') (f : A -> A') (e2 : A' -> M* B)
  (H_e1 : (let* v := e1 in return* f v) = e1') :
  Option.bind e1 (fun v => e2 (f v)) =
  Option.bind e1' e2.
  destruct e1; simpl in *; now rewrite <- H_e1.
Qed.

Lemma rewrite_bind_right {A B : Set} (e1 : M* A) (e2 e2' : A -> M* B)
  (H : forall v, e2 v = e2' v) :
  Option.bind e1 e2 =
  Option.bind e1 e2'.
  destruct e1; now simpl.
Qed.
