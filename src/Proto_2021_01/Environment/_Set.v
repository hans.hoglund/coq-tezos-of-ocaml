Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_2021_01.Environment.Compare.
Require Proto_2021_01.Environment.Map.
Require Proto_2021_01.Environment.S.

(** We define sets as maps to the unit type. *)
Module Make.
  Class FArgs {t : Set} := {
    Ord : Compare.COMPARABLE (t := t);
  }.
  Arguments Build_FArgs {_}.

  Definition elt `{FArgs} : Set := Ord.(Compare.COMPARABLE.t).

  Definition Map `{FArgs} : S.MAP (key := elt) (t := _) :=
    Map.Make Ord.

  Definition t `{FArgs} : Set := Map.(S.MAP.t) unit.

  Definition empty `{FArgs} : t :=
    Map.(S.MAP.empty).

  Definition is_empty `{FArgs} (s : t) : bool :=
    Map.(S.MAP.is_empty) s.

  Definition mem `{FArgs} (e : elt) (s : t) : bool :=
    Map.(S.MAP.mem) e s.

  Definition add `{FArgs} (e : elt) (s : t) : t :=
    Map.(S.MAP.add) e tt s.

  Definition singleton `{FArgs} (e : elt) : t :=
    Map.(S.MAP.singleton) e tt.

  Definition remove `{FArgs} (e : elt) (m : t) : t :=
    Map.(S.MAP.remove) e m.

  Definition fold `{FArgs} {a : Set} (f : elt -> a -> a) (s : t) (acc : a)
    : a :=
    Map.(S.MAP.fold) (fun e _ acc => f e acc) s acc.

  Definition union `{FArgs} (s1 s2 : t) : t :=
    Map.(S.MAP.union) (fun e _ _ => Some tt) s1 s2.

  Definition filter `{FArgs} (p : elt -> bool) (s : t) : t :=
    Map.(S.MAP.filter) (fun e _ => p e) s.

  Definition inter `{FArgs} (s1 s2 : t) : t :=
    Map.(S.MAP.merge)
      (fun e v1 v2 =>
        match v1, v2 with
        | Some _, Some _ => Some tt
        | _, _ => None
        end
      )
      s1 s2.

  Definition diff_value `{FArgs} (s1 s2 : t) : t :=
    Map.(S.MAP.merge)
      (fun e v1 v2 =>
        match v1, v2 with
        | Some _, None => Some tt
        | _, _ => None
        end
      )
      s1 s2.

  Definition compare `{FArgs} (s1 s2 : t) : int :=
    Map.(S.MAP.compare) (fun _ _ => 0) s1 s2.

  Definition equal `{FArgs} (s1 s2 : t) : bool :=
    Map.(S.MAP.equal) (fun _ _ => true) s1 s2.

  Definition iter `{FArgs} (f : elt -> unit) (s : t) : unit :=
    Map.(S.MAP.iter) (fun e _ => f e) s.

  Definition map `{FArgs} (f : elt -> elt) (s : t) : t :=
    fold (fun e acc => add (f e) acc) s empty.

  Definition for_all `{FArgs} (p : elt -> bool) (s : t) : bool :=
    Map.(S.MAP.for_all) (fun e _ => p e) s.

  Definition _exists `{FArgs} (p : elt -> bool) (s : t) : bool :=
    Map.(S.MAP._exists) (fun e _ => p e) s.

  Definition subset `{FArgs} (s1 s2 : t) : bool :=
    for_all (fun value1 => mem value1 s2) s1.

  Definition partition `{FArgs} (p : elt -> bool) (s : t) : t * t :=
    Map.(S.MAP.partition) (fun e _ => p e) s.

  Definition elements `{FArgs} (s : t) : list elt :=
    List.map fst (Map.(S.MAP.bindings) s).

  Definition cardinal `{FArgs} (s : t) : int :=
    Map.(S.MAP.cardinal) s.

  Definition option_fst {a b : Set} (x : option (a * b)) : option a :=
    match x with
    | Some (v, _) => Some v
    | None => None
    end.

  Definition min_elt_opt `{FArgs} (s : t) : option elt :=
    option_fst (Map.(S.MAP.min_binding_opt) s).

  Definition max_elt_opt `{FArgs} (s : t) : option elt :=
    option_fst (Map.(S.MAP.max_binding_opt) s).

  Definition choose_opt `{FArgs} (s : t) : option elt :=
    option_fst (Map.(S.MAP.choose_opt) s).

  Definition split `{FArgs} (e : elt) (s : t) : t * bool * t :=
    let '(s_lt, v, s_gt) := Map.(S.MAP.split) e s in
    let found := match v with Some _ => true | None => false end in
    (s_lt, found, s_gt).

  (** The [find_opt] operation on the maps would not work as it does not return
      the key. *)
  Definition find_opt `{FArgs} (e : elt) (s : t) : option elt :=
    option_fst (
      Map.(S.MAP.find_first_opt)
        (fun e' => Z.eqb (Ord.(Compare.COMPARABLE.compare) e e') 0)
        s
      ).

  Definition find_first_opt `{FArgs} (p : elt -> bool) (s : t) : option elt :=
    option_fst (Map.(S.MAP.find_first_opt) p s).

  Definition find_last_opt `{FArgs} (p : elt -> bool) (s : t) : option elt :=
    option_fst (Map.(S.MAP.find_last_opt) p s).

  Definition of_list `{FArgs} (l : list elt) : t :=
    List.fold_left (fun s e => add e s) empty l.

  Definition functor `(FArgs) :=
    {|
      S.SET.empty := empty;
      S.SET.is_empty := is_empty;
      S.SET.mem := mem;
      S.SET.add := add;
      S.SET.singleton := singleton;
      S.SET.remove := remove;
      S.SET.union := union;
      S.SET.inter := inter;
      S.SET.diff_value := diff_value;
      S.SET.compare := compare;
      S.SET.equal := equal;
      S.SET.subset := subset;
      S.SET.iter := iter;
      S.SET.map := map;
      S.SET.fold _ := fold;
      S.SET.for_all := for_all;
      S.SET._exists := _exists;
      S.SET.filter := filter;
      S.SET.partition := partition;
      S.SET.cardinal := cardinal;
      S.SET.elements := elements;
      S.SET.min_elt_opt := min_elt_opt;
      S.SET.max_elt_opt := max_elt_opt;
      S.SET.choose_opt := choose_opt;
      S.SET.split := split;
      S.SET.find_opt := find_opt;
      S.SET.find_first_opt := find_first_opt;
      S.SET.find_last_opt := find_last_opt;
      S.SET.of_list := of_list;
    |}.
End Make.

Definition Make_t {Ord_t : Set}
  (Ord : Compare.COMPARABLE (t := Ord_t)) :=
  let '_ := Make.Build_FArgs Ord in
  Make.t.

Definition Make {Ord_t : Set}
  (Ord : Compare.COMPARABLE (t := Ord_t))
  : S.SET (elt := Ord.(Compare.COMPARABLE.t)) (t := Make_t Ord) :=
  Make.functor (Make.Build_FArgs Ord).
