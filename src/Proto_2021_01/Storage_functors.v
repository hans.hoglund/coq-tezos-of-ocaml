Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Gas_limit_repr.
Require TezosOfOCaml.Proto_2021_01.Misc.
Require TezosOfOCaml.Proto_2021_01.Raw_context.
Require TezosOfOCaml.Proto_2021_01.Storage_costs.
Require TezosOfOCaml.Proto_2021_01.Storage_description.
Require TezosOfOCaml.Proto_2021_01.Storage_sigs.

Module Registered.
  Definition ghost : bool := false.
  
  Definition module :=
    {|
      Storage_sigs.REGISTER.ghost := ghost
    |}.
End Registered.
Definition Registered := Registered.module.

Module Ghost.
  Definition ghost : bool := true.
  
  Definition module :=
    {|
      Storage_sigs.REGISTER.ghost := ghost
    |}.
End Ghost.
Definition Ghost := Ghost.module.

Module ENCODER.
  Record signature {t : Set} : Set := {
    t := t;
    of_bytes : list string -> bytes -> M? t;
    to_bytes : t -> bytes;
  }.
End ENCODER.
Definition ENCODER := @ENCODER.signature.
Arguments ENCODER {_}.

Module Make_encoder.
  Class FArgs {V_t : Set} := {
    V : Storage_sigs.VALUE (t := V_t);
  }.
  Arguments Build_FArgs {_}.
  
  Definition of_bytes `{FArgs} (key_value : list string) (b_value : bytes)
    : M? V.(Storage_sigs.VALUE.t) :=
    match Data_encoding.Binary.of_bytes V.(Storage_sigs.VALUE.encoding) b_value
      with
    | None =>
      Error_monad.error_value
        (Build_extensible "Storage_error" Raw_context.storage_error
          (Raw_context.Corrupted_data key_value))
    | Some v => Pervasives.Ok v
    end.
  
  Definition to_bytes `{FArgs} (v : V.(Storage_sigs.VALUE.t)) : bytes :=
    match Data_encoding.Binary.to_bytes V.(Storage_sigs.VALUE.encoding) v with
    | Some b_value => b_value
    | None => Bytes.empty
    end.
  
  Definition functor `{FArgs} :=
    {|
      ENCODER.of_bytes := of_bytes;
      ENCODER.to_bytes := to_bytes
    |}.
End Make_encoder.
Definition Make_encoder {V_t : Set} (V : Storage_sigs.VALUE (t := V_t))
  : ENCODER (t := V.(Storage_sigs.VALUE.t)) :=
  let '_ := Make_encoder.Build_FArgs V in
  Make_encoder.functor.

Definition len_name : string := "len".

Definition data_name : string := "data".

Definition encode_len_value (bytes_value : bytes) : bytes :=
  let length := Bytes.length bytes_value in
  Data_encoding.Binary.to_bytes_exn Data_encoding.int31 length.

Definition decode_len_value (key_value : list string) (len : bytes) : M? int :=
  match Data_encoding.Binary.of_bytes Data_encoding.int31 len with
  | None =>
    Error_monad.error_value
      (Build_extensible "Storage_error" Raw_context.storage_error
        (Raw_context.Corrupted_data key_value))
  | Some len => return? len
  end.

Definition map_key
  (f : Context.key -> Context.key) (function_parameter : Context.key_or_dir)
  : Context.key_or_dir :=
  match function_parameter with
  | Context.Key k => Context.Key (f k)
  | Context.Dir k => Context.Dir (f k)
  end.

Module Make_subcontext.
  Class FArgs {C_t : Set} := {
    R : Storage_sigs.REGISTER;
    C : Raw_context.T (t := C_t);
    N : Storage_sigs.NAME;
  }.
  Arguments Build_FArgs {_}.
  
  Definition t `{FArgs} : Set := C.(Raw_context.T.t).
  
  Definition context `{FArgs} : Set := t.
  
  Definition name_length `{FArgs} : int :=
    List.length N.(Storage_sigs.NAME.name).
  
  Definition to_key `{FArgs} (k : list string) : list string :=
    Pervasives.op_at N.(Storage_sigs.NAME.name) k.
  
  Definition of_key `{FArgs} {A : Set} (k : list A) : list A :=
    Misc.remove_elem_from_list name_length k.
  
  Definition mem `{FArgs} (t_value : C.(Raw_context.T.t)) (k : list string)
    : M= bool := C.(Raw_context.T.mem) t_value (to_key k).
  
  Definition dir_mem `{FArgs} (t_value : C.(Raw_context.T.t)) (k : list string)
    : M= bool := C.(Raw_context.T.dir_mem) t_value (to_key k).
  
  Definition get `{FArgs} (t_value : C.(Raw_context.T.t)) (k : list string)
    : M=? Raw_context.value := C.(Raw_context.T.get) t_value (to_key k).
  
  Definition get_option `{FArgs}
    (t_value : C.(Raw_context.T.t)) (k : list string)
    : M= (option Raw_context.value) :=
    C.(Raw_context.T.get_option) t_value (to_key k).
  
  Definition init_value `{FArgs}
    (t_value : C.(Raw_context.T.t)) (k : list string) (v : Raw_context.value)
    : M=? C.(Raw_context.T.t) :=
    C.(Raw_context.T.init_value) t_value (to_key k) v.
  
  Definition set `{FArgs}
    (t_value : C.(Raw_context.T.t)) (k : list string) (v : Raw_context.value)
    : M=? C.(Raw_context.T.t) := C.(Raw_context.T.set) t_value (to_key k) v.
  
  Definition init_set `{FArgs}
    (t_value : C.(Raw_context.T.t)) (k : list string) (v : Raw_context.value)
    : M= C.(Raw_context.T.t) := C.(Raw_context.T.init_set) t_value (to_key k) v.
  
  Definition set_option `{FArgs}
    (t_value : C.(Raw_context.T.t)) (k : list string)
    (v : option Raw_context.value) : M= C.(Raw_context.T.t) :=
    C.(Raw_context.T.set_option) t_value (to_key k) v.
  
  Definition delete `{FArgs} (t_value : C.(Raw_context.T.t)) (k : list string)
    : M=? C.(Raw_context.T.t) := C.(Raw_context.T.delete) t_value (to_key k).
  
  Definition remove `{FArgs} (t_value : C.(Raw_context.T.t)) (k : list string)
    : M= C.(Raw_context.T.t) := C.(Raw_context.T.remove) t_value (to_key k).
  
  Definition remove_rec `{FArgs}
    (t_value : C.(Raw_context.T.t)) (k : list string)
    : M= C.(Raw_context.T.t) := C.(Raw_context.T.remove_rec) t_value (to_key k).
  
  Definition copy `{FArgs}
    (t_value : C.(Raw_context.T.t)) (from : list string) (to_ : list string)
    : M=? C.(Raw_context.T.t) :=
    C.(Raw_context.T.copy) t_value (to_key from) (to_key to_).
  
  Definition fold `{FArgs} {A : Set}
    (t_value : C.(Raw_context.T.t)) (k : list string) (init_value : A)
    (f : Context.key_or_dir -> A -> M= A) : M= A :=
    C.(Raw_context.T.fold) t_value (to_key k) init_value
      (fun (k : Context.key_or_dir) =>
        fun (acc_value : A) => f (map_key of_key k) acc_value).
  
  Definition keys `{FArgs} (t_value : C.(Raw_context.T.t)) (k : list string)
    : M= (list (list string)) :=
    let= keys := C.(Raw_context.T.keys) t_value (to_key k) in
    return= (List.map of_key keys).
  
  Definition fold_keys `{FArgs} {A : Set}
    (t_value : C.(Raw_context.T.t)) (k : list string) (init_value : A)
    (f : list string -> A -> M= A) : M= A :=
    C.(Raw_context.T.fold_keys) t_value (to_key k) init_value
      (fun (k : Raw_context.key) =>
        fun (acc_value : A) => f (of_key k) acc_value).
  
  Definition project `{FArgs}
    : C.(Raw_context.T.t) -> Raw_context.root_context :=
    C.(Raw_context.T.project).
  
  Definition absolute_key `{FArgs} (c : C.(Raw_context.T.t)) (k : list string)
    : Raw_context.key := C.(Raw_context.T.absolute_key) c (to_key k).
  
  Definition consume_gas `{FArgs}
    : C.(Raw_context.T.t) -> Gas_limit_repr.cost -> M? C.(Raw_context.T.t) :=
    C.(Raw_context.T.consume_gas).
  
  Definition check_enough_gas `{FArgs}
    : C.(Raw_context.T.t) -> Gas_limit_repr.cost -> M? unit :=
    C.(Raw_context.T.check_enough_gas).
  
  Definition description `{FArgs} : Storage_description.t C.(Raw_context.T.t) :=
    let description :=
      if R.(Storage_sigs.REGISTER.ghost) then
        Storage_description.create tt
      else
        C.(Raw_context.T.description) in
    Storage_description.register_named_subcontext description
      N.(Storage_sigs.NAME.name).
  
  Definition functor `{FArgs} :=
    {|
      Raw_context.T.mem := mem;
      Raw_context.T.dir_mem := dir_mem;
      Raw_context.T.get := get;
      Raw_context.T.get_option := get_option;
      Raw_context.T.init_value := init_value;
      Raw_context.T.set := set;
      Raw_context.T.init_set := init_set;
      Raw_context.T.set_option := set_option;
      Raw_context.T.delete := delete;
      Raw_context.T.remove := remove;
      Raw_context.T.remove_rec := remove_rec;
      Raw_context.T.copy := copy;
      Raw_context.T.fold _ := fold;
      Raw_context.T.keys := keys;
      Raw_context.T.fold_keys _ := fold_keys;
      Raw_context.T.project := project;
      Raw_context.T.absolute_key := absolute_key;
      Raw_context.T.consume_gas := consume_gas;
      Raw_context.T.check_enough_gas := check_enough_gas;
      Raw_context.T.description := description
    |}.
End Make_subcontext.
Definition Make_subcontext {C_t : Set}
  (R : Storage_sigs.REGISTER) (C : Raw_context.T (t := C_t))
  (N : Storage_sigs.NAME) : Raw_context.T (t := C.(Raw_context.T.t)) :=
  let '_ := Make_subcontext.Build_FArgs R C N in
  Make_subcontext.functor.

Module Make_single_data_storage.
  Class FArgs {C_t V_t : Set} := {
    R : Storage_sigs.REGISTER;
    C : Raw_context.T (t := C_t);
    N : Storage_sigs.NAME;
    V : Storage_sigs.VALUE (t := V_t);
  }.
  Arguments Build_FArgs {_ _}.
  
  Definition t `{FArgs} : Set := C.(Raw_context.T.t).
  
  Definition context `{FArgs} : Set := t.
  
  Definition value `{FArgs} : Set := V.(Storage_sigs.VALUE.t).
  
  Definition mem `{FArgs} (t_value : C.(Raw_context.T.t)) : M= bool :=
    C.(Raw_context.T.mem) t_value N.(Storage_sigs.NAME.name).
  
  Definition Make_encoder_include `{FArgs} := Make_encoder V.
  
  (** Inclusion of the module [Make_encoder_include] *)
  Definition of_bytes `{FArgs} := Make_encoder_include.(ENCODER.of_bytes).
  
  Definition to_bytes `{FArgs} := Make_encoder_include.(ENCODER.to_bytes).
  
  Definition get `{FArgs} (t_value : C.(Raw_context.T.t))
    : M=? V.(Storage_sigs.VALUE.t) :=
    let=? b_value := C.(Raw_context.T.get) t_value N.(Storage_sigs.NAME.name) in
    let key_value :=
      C.(Raw_context.T.absolute_key) t_value N.(Storage_sigs.NAME.name) in
    Lwt._return (of_bytes key_value b_value).
  
  Definition get_option `{FArgs} (t_value : C.(Raw_context.T.t))
    : M=? (option V.(Storage_sigs.VALUE.t)) :=
    let= function_parameter :=
      C.(Raw_context.T.get_option) t_value N.(Storage_sigs.NAME.name) in
    match function_parameter with
    | None => return= Error_monad.ok_none
    | Some b_value =>
      let key_value :=
        C.(Raw_context.T.absolute_key) t_value N.(Storage_sigs.NAME.name) in
      return=
        (let? v := of_bytes key_value b_value in
        return? (Some v))
    end.
  
  Definition init_value `{FArgs}
    (t_value : C.(Raw_context.T.t)) (v : V.(Storage_sigs.VALUE.t))
    : M=? Raw_context.root_context :=
    let=? t_value :=
      C.(Raw_context.T.init_value) t_value N.(Storage_sigs.NAME.name)
        (to_bytes v) in
    return=? (C.(Raw_context.T.project) t_value).
  
  Definition set `{FArgs}
    (t_value : C.(Raw_context.T.t)) (v : V.(Storage_sigs.VALUE.t))
    : M=? Raw_context.root_context :=
    let=? t_value :=
      C.(Raw_context.T.set) t_value N.(Storage_sigs.NAME.name) (to_bytes v) in
    return=? (C.(Raw_context.T.project) t_value).
  
  Definition init_set `{FArgs}
    (t_value : C.(Raw_context.T.t)) (v : V.(Storage_sigs.VALUE.t))
    : M= Raw_context.root_context :=
    let= t_value :=
      C.(Raw_context.T.init_set) t_value N.(Storage_sigs.NAME.name) (to_bytes v)
      in
    return= (C.(Raw_context.T.project) t_value).
  
  Definition set_option `{FArgs}
    (t_value : C.(Raw_context.T.t)) (v : option V.(Storage_sigs.VALUE.t))
    : M= Raw_context.root_context :=
    let= t_value :=
      C.(Raw_context.T.set_option) t_value N.(Storage_sigs.NAME.name)
        (Option.map to_bytes v) in
    return= (C.(Raw_context.T.project) t_value).
  
  Definition remove `{FArgs} (t_value : C.(Raw_context.T.t))
    : M= Raw_context.root_context :=
    let= t_value := C.(Raw_context.T.remove) t_value N.(Storage_sigs.NAME.name)
      in
    return= (C.(Raw_context.T.project) t_value).
  
  Definition delete `{FArgs} (t_value : C.(Raw_context.T.t))
    : M=? Raw_context.root_context :=
    let=? t_value := C.(Raw_context.T.delete) t_value N.(Storage_sigs.NAME.name)
      in
    return=? (C.(Raw_context.T.project) t_value).
  
  Definition description `{FArgs} : Storage_description.t C.(Raw_context.T.t) :=
    if R.(Storage_sigs.REGISTER.ghost) then
      Storage_description.create tt
    else
      C.(Raw_context.T.description).
  
  (** Init function; without side-effects in Coq *)
  Definition init_module `{FArgs} : unit :=
    Storage_description.register_value
      (Storage_description.register_named_subcontext description
        N.(Storage_sigs.NAME.name)) get_option V.(Storage_sigs.VALUE.encoding).
  
  Definition functor `{FArgs} :=
    {|
      Storage_sigs.Single_data_storage.mem := mem;
      Storage_sigs.Single_data_storage.get := get;
      Storage_sigs.Single_data_storage.get_option := get_option;
      Storage_sigs.Single_data_storage.init_value := init_value;
      Storage_sigs.Single_data_storage.set := set;
      Storage_sigs.Single_data_storage.init_set := init_set;
      Storage_sigs.Single_data_storage.set_option := set_option;
      Storage_sigs.Single_data_storage.delete := delete;
      Storage_sigs.Single_data_storage.remove := remove
    |}.
End Make_single_data_storage.
Definition Make_single_data_storage {C_t V_t : Set}
  (R : Storage_sigs.REGISTER) (C : Raw_context.T (t := C_t))
  (N : Storage_sigs.NAME) (V : Storage_sigs.VALUE (t := V_t))
  : Storage_sigs.Single_data_storage (t := C.(Raw_context.T.t))
    (value := V.(Storage_sigs.VALUE.t)) :=
  let '_ := Make_single_data_storage.Build_FArgs R C N V in
  Make_single_data_storage.functor.

Module INDEX.
  Record signature {t : Set} {ipath : Set -> Set} : Set := {
    t := t;
    path_length : int;
    to_path : t -> list string -> list string;
    of_path : list string -> option t;
    ipath := ipath;
    args : Storage_description.args;
  }.
End INDEX.
Definition INDEX := @INDEX.signature.
Arguments INDEX {_ _}.

Module Pair.
  Class FArgs {I1_t : Set} {I1_ipath : Set -> Set} {I2_t : Set}
    {I2_ipath : Set -> Set} := {
    I1 : INDEX (t := I1_t) (ipath := I1_ipath);
    I2 : INDEX (t := I2_t) (ipath := I2_ipath);
  }.
  Arguments Build_FArgs {_ _ _ _}.
  
  Definition t `{FArgs} : Set := I1.(INDEX.t) * I2.(INDEX.t).
  
  Definition path_length `{FArgs} : int :=
    I1.(INDEX.path_length) +i I2.(INDEX.path_length).
  
  Definition to_path `{FArgs} (function_parameter : I1.(INDEX.t) * I2.(INDEX.t))
    : list string -> list string :=
    let '(x, y) := function_parameter in
    fun (l_value : list string) =>
      I1.(INDEX.to_path) x (I2.(INDEX.to_path) y l_value).
  
  Definition of_path `{FArgs} (l_value : list string)
    : option (I1.(INDEX.t) * I2.(INDEX.t)) :=
    match Misc.take I1.(INDEX.path_length) l_value with
    | None => None
    | Some (l1, l2) =>
      match ((I1.(INDEX.of_path) l1), (I2.(INDEX.of_path) l2)) with
      | (Some x, Some y) => Some (x, y)
      | _ => None
      end
    end.
  
  Definition ipath `{FArgs} (a : Set) : Set :=
    I2.(INDEX.ipath) (I1.(INDEX.ipath) a).
  
  Definition args `{FArgs} : Storage_description.args :=
    Storage_description.Pair I1.(INDEX.args) I2.(INDEX.args).
  
  Definition functor `{FArgs} : INDEX.signature (ipath := ipath) :=
    {|
      INDEX.path_length := path_length;
      INDEX.to_path := to_path;
      INDEX.of_path := of_path;
      INDEX.args := args
    |}.
End Pair.
Definition Pair
  {I1_t : Set} {I1_ipath : Set -> Set} {I2_t : Set} {I2_ipath : Set -> Set}
  (I1 : INDEX (t := I1_t) (ipath := I1_ipath))
  (I2 : INDEX (t := I2_t) (ipath := I2_ipath))
  : INDEX (t := I1.(INDEX.t) * I2.(INDEX.t)) (ipath := _) :=
  let '_ := Pair.Build_FArgs I1 I2 in
  Pair.functor.

Module Make_data_set_storage.
  Class FArgs {C_t I_t : Set} {I_ipath : Set -> Set} := {
    C : Raw_context.T (t := C_t);
    I : INDEX (t := I_t) (ipath := I_ipath);
  }.
  Arguments Build_FArgs {_ _ _}.
  
  Definition t `{FArgs} : Set := C.(Raw_context.T.t).
  
  Definition context `{FArgs} : Set := t.
  
  Definition elt `{FArgs} : Set := I.(INDEX.t).
  
  Definition inited `{FArgs} : bytes := Bytes.of_string "inited".
  
  Definition mem `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
    : M= bool := C.(Raw_context.T.mem) s (I.(INDEX.to_path) i nil).
  
  Definition add `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
    : M= Raw_context.root_context :=
    let= t_value :=
      C.(Raw_context.T.init_set) s (I.(INDEX.to_path) i nil) inited in
    return= (C.(Raw_context.T.project) t_value).
  
  Definition del `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
    : M= Raw_context.root_context :=
    let= t_value := C.(Raw_context.T.remove) s (I.(INDEX.to_path) i nil) in
    return= (C.(Raw_context.T.project) t_value).
  
  Definition set `{FArgs}
    (s : C.(Raw_context.T.t)) (i : I.(INDEX.t)) (function_parameter : bool)
    : M= Raw_context.root_context :=
    match function_parameter with
    | true => add s i
    | false => del s i
    end.
  
  Definition clear `{FArgs} (s : C.(Raw_context.T.t))
    : M= Raw_context.root_context :=
    let= t_value := C.(Raw_context.T.remove_rec) s nil in
    return= (C.(Raw_context.T.project) t_value).
  
  Definition fold `{FArgs} {A : Set}
    (s : C.(Raw_context.T.t)) (init_value : A) (f : I.(INDEX.t) -> A -> M= A)
    : M= A :=
    let fix dig (i : int) (path : Raw_context.key) (acc_value : A) {struct i}
      : M= A :=
      if i <=i 1 then
        C.(Raw_context.T.fold) s path acc_value
          (fun (k : Context.key_or_dir) =>
            fun (acc_value : A) =>
              match k with
              | Context.Dir _ => Lwt._return acc_value
              | Context.Key file =>
                match I.(INDEX.of_path) file with
                | None =>
                  (* ❌ Assert instruction is not handled. *)
                  assert (M= _) false
                | Some p_value => f p_value acc_value
                end
              end)
      else
        C.(Raw_context.T.fold) s path acc_value
          (fun (k : Context.key_or_dir) =>
            fun (acc_value : A) =>
              match k with
              | Context.Dir k => dig (i -i 1) k acc_value
              | Context.Key _ => Lwt._return acc_value
              end) in
    dig I.(INDEX.path_length) nil init_value.
  
  Definition elements `{FArgs} (s : C.(Raw_context.T.t))
    : M= (list I.(INDEX.t)) :=
    fold s (nil (A := elt))
      (fun (p_value : I.(INDEX.t)) =>
        fun (acc_value : list I.(INDEX.t)) =>
          Lwt._return (cons p_value acc_value)).
  
  (** Init function; without side-effects in Coq *)
  Definition init_module `{FArgs} : unit :=
    let unpack {A : Set} (c : I.(INDEX.ipath) A) : A * I.(INDEX.t) :=
      Storage_description.unpack I.(INDEX.args) c in
    Storage_description.register_value
      (Storage_description.register_indexed_subcontext
        C.(Raw_context.T.description)
        (fun (c : C.(Raw_context.T.t)) =>
          Error_monad.op_gtpipeeq (elements c) Error_monad.ok) I.(INDEX.args))
      (fun (c : I.(INDEX.ipath) C.(Raw_context.T.t)) =>
        let '(c, k) := unpack c in
        let= function_parameter := mem c k in
        match function_parameter with
        | true => Error_monad.return_some true
        | false => Error_monad.return_none
        end) Data_encoding.bool_value.
  
  Definition functor `{FArgs} :=
    {|
      Storage_sigs.Data_set_storage.mem := mem;
      Storage_sigs.Data_set_storage.add := add;
      Storage_sigs.Data_set_storage.del := del;
      Storage_sigs.Data_set_storage.set := set;
      Storage_sigs.Data_set_storage.elements := elements;
      Storage_sigs.Data_set_storage.fold _ := fold;
      Storage_sigs.Data_set_storage.clear := clear
    |}.
End Make_data_set_storage.
Definition Make_data_set_storage {C_t I_t : Set} {I_ipath : Set -> Set}
  (C : Raw_context.T (t := C_t)) (I : INDEX (t := I_t) (ipath := I_ipath))
  : Storage_sigs.Data_set_storage (t := C.(Raw_context.T.t))
    (elt := I.(INDEX.t)) :=
  let '_ := Make_data_set_storage.Build_FArgs C I in
  Make_data_set_storage.functor.

Module Make_indexed_data_storage.
  Class FArgs {C_t I_t : Set} {I_ipath : Set -> Set} {V_t : Set} := {
    C : Raw_context.T (t := C_t);
    I : INDEX (t := I_t) (ipath := I_ipath);
    V : Storage_sigs.VALUE (t := V_t);
  }.
  Arguments Build_FArgs {_ _ _ _}.
  
  Definition t `{FArgs} : Set := C.(Raw_context.T.t).
  
  Definition context `{FArgs} : Set := t.
  
  Definition key `{FArgs} : Set := I.(INDEX.t).
  
  Definition value `{FArgs} : Set := V.(Storage_sigs.VALUE.t).
  
  Definition Make_encoder_include `{FArgs} := Make_encoder V.
  
  (** Inclusion of the module [Make_encoder_include] *)
  Definition of_bytes `{FArgs} := Make_encoder_include.(ENCODER.of_bytes).
  
  Definition to_bytes `{FArgs} := Make_encoder_include.(ENCODER.to_bytes).
  
  Definition mem `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
    : M= bool := C.(Raw_context.T.mem) s (I.(INDEX.to_path) i nil).
  
  Definition get `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
    : M=? V.(Storage_sigs.VALUE.t) :=
    let=? b_value := C.(Raw_context.T.get) s (I.(INDEX.to_path) i nil) in
    let key_value := C.(Raw_context.T.absolute_key) s (I.(INDEX.to_path) i nil)
      in
    Lwt._return (of_bytes key_value b_value).
  
  Definition get_option `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
    : M=? (option V.(Storage_sigs.VALUE.t)) :=
    let= function_parameter :=
      C.(Raw_context.T.get_option) s (I.(INDEX.to_path) i nil) in
    match function_parameter with
    | None => return= Error_monad.ok_none
    | Some b_value =>
      let key_value :=
        C.(Raw_context.T.absolute_key) s (I.(INDEX.to_path) i nil) in
      return=
        (let? v := of_bytes key_value b_value in
        return? (Some v))
    end.
  
  Definition set `{FArgs}
    (s : C.(Raw_context.T.t)) (i : I.(INDEX.t)) (v : V.(Storage_sigs.VALUE.t))
    : M=? Raw_context.root_context :=
    let=? t_value :=
      C.(Raw_context.T.set) s (I.(INDEX.to_path) i nil) (to_bytes v) in
    return=? (C.(Raw_context.T.project) t_value).
  
  Definition init_value `{FArgs}
    (s : C.(Raw_context.T.t)) (i : I.(INDEX.t)) (v : V.(Storage_sigs.VALUE.t))
    : M=? Raw_context.root_context :=
    let=? t_value :=
      C.(Raw_context.T.init_value) s (I.(INDEX.to_path) i nil) (to_bytes v) in
    return=? (C.(Raw_context.T.project) t_value).
  
  Definition init_set `{FArgs}
    (s : C.(Raw_context.T.t)) (i : I.(INDEX.t)) (v : V.(Storage_sigs.VALUE.t))
    : M= Raw_context.root_context :=
    let= t_value :=
      C.(Raw_context.T.init_set) s (I.(INDEX.to_path) i nil) (to_bytes v) in
    return= (C.(Raw_context.T.project) t_value).
  
  Definition set_option `{FArgs}
    (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
    (v : option V.(Storage_sigs.VALUE.t)) : M= Raw_context.root_context :=
    let= t_value :=
      C.(Raw_context.T.set_option) s (I.(INDEX.to_path) i nil)
        (Option.map to_bytes v) in
    return= (C.(Raw_context.T.project) t_value).
  
  Definition remove `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
    : M= Raw_context.root_context :=
    let= t_value := C.(Raw_context.T.remove) s (I.(INDEX.to_path) i nil) in
    return= (C.(Raw_context.T.project) t_value).
  
  Definition delete `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
    : M=? Raw_context.root_context :=
    let=? t_value := C.(Raw_context.T.delete) s (I.(INDEX.to_path) i nil) in
    return=? (C.(Raw_context.T.project) t_value).
  
  Definition clear `{FArgs} (s : C.(Raw_context.T.t))
    : M= Raw_context.root_context :=
    let= t_value := C.(Raw_context.T.remove_rec) s nil in
    return= (C.(Raw_context.T.project) t_value).
  
  Definition fold_keys `{FArgs} {A : Set}
    (s : C.(Raw_context.T.t)) (init_value : A) (f : I.(INDEX.t) -> A -> M= A)
    : M= A :=
    let fix dig (i : int) (path : Raw_context.key) (acc_value : A) {struct i}
      : M= A :=
      if i <=i 1 then
        C.(Raw_context.T.fold) s path acc_value
          (fun (k : Context.key_or_dir) =>
            fun (acc_value : A) =>
              match k with
              | Context.Dir _ => Lwt._return acc_value
              | Context.Key file =>
                match I.(INDEX.of_path) file with
                | None =>
                  (* ❌ Assert instruction is not handled. *)
                  assert (M= _) false
                | Some path => f path acc_value
                end
              end)
      else
        C.(Raw_context.T.fold) s path acc_value
          (fun (k : Context.key_or_dir) =>
            fun (acc_value : A) =>
              match k with
              | Context.Dir k => dig (i -i 1) k acc_value
              | Context.Key _ => Lwt._return acc_value
              end) in
    dig I.(INDEX.path_length) nil init_value.
  
  Definition fold `{FArgs} {A : Set}
    (s : C.(Raw_context.T.t)) (init_value : A)
    (f : I.(INDEX.t) -> V.(Storage_sigs.VALUE.t) -> A -> M= A) : M= A :=
    let f (path : I.(INDEX.t)) (acc_value : A) : M= A :=
      let= function_parameter := get s path in
      match function_parameter with
      | Pervasives.Error _ => Lwt._return acc_value
      | Pervasives.Ok v => f path v acc_value
      end in
    fold_keys s init_value f.
  
  Definition bindings `{FArgs} (s : C.(Raw_context.T.t))
    : M= (list (I.(INDEX.t) * V.(Storage_sigs.VALUE.t))) :=
    fold s (nil (A := key * value))
      (fun (p_value : I.(INDEX.t)) =>
        fun (v : V.(Storage_sigs.VALUE.t)) =>
          fun (acc_value : list (I.(INDEX.t) * V.(Storage_sigs.VALUE.t))) =>
            Lwt._return (cons (p_value, v) acc_value)).
  
  Definition keys `{FArgs} (s : C.(Raw_context.T.t)) : M= (list I.(INDEX.t)) :=
    fold_keys s (nil (A := key))
      (fun (p_value : I.(INDEX.t)) =>
        fun (acc_value : list I.(INDEX.t)) =>
          Lwt._return (cons p_value acc_value)).
  
  (** Init function; without side-effects in Coq *)
  Definition init_module `{FArgs} : unit :=
    let unpack {A : Set} (c : I.(INDEX.ipath) A) : A * I.(INDEX.t) :=
      Storage_description.unpack I.(INDEX.args) c in
    Storage_description.register_value
      (Storage_description.register_indexed_subcontext
        C.(Raw_context.T.description)
        (fun (c : C.(Raw_context.T.t)) =>
          Error_monad.op_gtpipeeq (keys c) Error_monad.ok) I.(INDEX.args))
      (fun (c : I.(INDEX.ipath) C.(Raw_context.T.t)) =>
        let '(c, k) := unpack c in
        get_option c k) V.(Storage_sigs.VALUE.encoding).
  
  Definition functor `{FArgs} :=
    {|
      Storage_sigs.Indexed_data_storage.mem := mem;
      Storage_sigs.Indexed_data_storage.get := get;
      Storage_sigs.Indexed_data_storage.get_option := get_option;
      Storage_sigs.Indexed_data_storage.set := set;
      Storage_sigs.Indexed_data_storage.init_value := init_value;
      Storage_sigs.Indexed_data_storage.init_set := init_set;
      Storage_sigs.Indexed_data_storage.set_option := set_option;
      Storage_sigs.Indexed_data_storage.delete := delete;
      Storage_sigs.Indexed_data_storage.remove := remove;
      Storage_sigs.Indexed_data_storage.clear := clear;
      Storage_sigs.Indexed_data_storage.keys := keys;
      Storage_sigs.Indexed_data_storage.bindings := bindings;
      Storage_sigs.Indexed_data_storage.fold _ := fold;
      Storage_sigs.Indexed_data_storage.fold_keys _ := fold_keys
    |}.
End Make_indexed_data_storage.
Definition Make_indexed_data_storage
  {C_t I_t : Set} {I_ipath : Set -> Set} {V_t : Set}
  (C : Raw_context.T (t := C_t)) (I : INDEX (t := I_t) (ipath := I_ipath))
  (V : Storage_sigs.VALUE (t := V_t))
  : Storage_sigs.Indexed_data_storage (t := C.(Raw_context.T.t))
    (key := I.(INDEX.t)) (value := V.(Storage_sigs.VALUE.t)) :=
  let '_ := Make_indexed_data_storage.Build_FArgs C I V in
  Make_indexed_data_storage.functor.

Module Make_indexed_carbonated_data_storage_INTERNAL.
  Class FArgs {C_t I_t : Set} {I_ipath : Set -> Set} {V_t : Set} := {
    C : Raw_context.T (t := C_t);
    I : INDEX (t := I_t) (ipath := I_ipath);
    V : Storage_sigs.VALUE (t := V_t);
  }.
  Arguments Build_FArgs {_ _ _ _}.
  
  Definition t `{FArgs} : Set := C.(Raw_context.T.t).
  
  Definition context `{FArgs} : Set := t.
  
  Definition key `{FArgs} : Set := I.(INDEX.t).
  
  Definition value `{FArgs} : Set := V.(Storage_sigs.VALUE.t).
  
  Definition Make_encoder_include `{FArgs} := Make_encoder V.
  
  (** Inclusion of the module [Make_encoder_include] *)
  Definition of_bytes `{FArgs} := Make_encoder_include.(ENCODER.of_bytes).
  
  Definition to_bytes `{FArgs} := Make_encoder_include.(ENCODER.to_bytes).
  
  Definition data_key `{FArgs} (i : I.(INDEX.t)) : list string :=
    I.(INDEX.to_path) i [ data_name ].
  
  Definition len_key `{FArgs} (i : I.(INDEX.t)) : list string :=
    I.(INDEX.to_path) i [ len_name ].
  
  Definition consume_mem_gas `{FArgs} {A : Set}
    (c : C.(Raw_context.T.t)) (key_value : list A) : M? C.(Raw_context.T.t) :=
    C.(Raw_context.T.consume_gas) c
      (Storage_costs.read_access (List.length key_value) 0).
  
  Definition existing_size `{FArgs} (c : C.(Raw_context.T.t)) (i : I.(INDEX.t))
    : M=? (int * bool) :=
    let= function_parameter := C.(Raw_context.T.get_option) c (len_key i) in
    match function_parameter with
    | None => return=? (0, false)
    | Some len =>
      return=
        (let? len := decode_len_value (len_key i) len in
        return? (len, true))
    end.
  
  Definition consume_read_gas `{FArgs}
    (get : C.(Raw_context.T.t) -> list string -> M=? bytes)
    (c : C.(Raw_context.T.t)) (i : I.(INDEX.t)) : M=? C.(Raw_context.T.t) :=
    let len_key := len_key i in
    let=? len := get c len_key in
    Lwt._return
      (let? read_bytes := decode_len_value len_key len in
      let cost := Storage_costs.read_access (List.length len_key) read_bytes in
      C.(Raw_context.T.consume_gas) c cost).
  
  Definition consume_serialize_write_gas `{FArgs} {A : Set}
    (set : C.(Raw_context.T.t) -> list string -> bytes -> M=? A)
    (c : C.(Raw_context.T.t)) (i : I.(INDEX.t)) (v : V.(Storage_sigs.VALUE.t))
    : M=? (A * bytes) :=
    let bytes_value := to_bytes v in
    let len := Bytes.length bytes_value in
    let=? c :=
      return=
        (C.(Raw_context.T.consume_gas) c (Gas_limit_repr.alloc_mbytes_cost len))
      in
    let cost := Storage_costs.write_access len in
    let=? c := return= (C.(Raw_context.T.consume_gas) c cost) in
    let=? c := set c (len_key i) (encode_len_value bytes_value) in
    return=? (c, bytes_value).
  
  Definition consume_remove_gas `{FArgs} {A : Set}
    (del : C.(Raw_context.T.t) -> list string -> M=? A)
    (c : C.(Raw_context.T.t)) (i : I.(INDEX.t)) : M=? A :=
    let=? c :=
      return= (C.(Raw_context.T.consume_gas) c (Storage_costs.write_access 0))
      in
    del c (len_key i).
  
  Definition mem `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
    : M=? (Raw_context.root_context * bool) :=
    let key_value := data_key i in
    let=? s := return= (consume_mem_gas s key_value) in
    let= _exists := C.(Raw_context.T.mem) s key_value in
    return=? ((C.(Raw_context.T.project) s), _exists).
  
  Definition get `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
    : M=? (Raw_context.root_context * V.(Storage_sigs.VALUE.t)) :=
    let=? s := consume_read_gas C.(Raw_context.T.get) s i in
    let=? b_value := C.(Raw_context.T.get) s (data_key i) in
    let key_value := C.(Raw_context.T.absolute_key) s (data_key i) in
    Lwt._return
      (let? v := of_bytes key_value b_value in
      return? ((C.(Raw_context.T.project) s), v)).
  
  Definition get_option `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
    : M=? (Raw_context.root_context * option V.(Storage_sigs.VALUE.t)) :=
    let key_value := data_key i in
    let=? s := return= (consume_mem_gas s key_value) in
    let= _exists := C.(Raw_context.T.mem) s key_value in
    if _exists then
      let=? '(s, v) := get s i in
      return=? (s, (Some v))
    else
      Error_monad._return ((C.(Raw_context.T.project) s), None).
  
  Definition set `{FArgs}
    (s : C.(Raw_context.T.t)) (i : I.(INDEX.t)) (v : V.(Storage_sigs.VALUE.t))
    : M=? (Raw_context.root_context * int) :=
    let=? '(prev_size, _) := existing_size s i in
    let=? '(s, bytes_value) :=
      consume_serialize_write_gas C.(Raw_context.T.set) s i v in
    let=? t_value := C.(Raw_context.T.set) s (data_key i) bytes_value in
    let size_diff := (Bytes.length bytes_value) -i prev_size in
    return=? ((C.(Raw_context.T.project) t_value), size_diff).
  
  Definition init_value `{FArgs}
    (s : C.(Raw_context.T.t)) (i : I.(INDEX.t)) (v : V.(Storage_sigs.VALUE.t))
    : M=? (Raw_context.root_context * int) :=
    let=? '(s, bytes_value) :=
      consume_serialize_write_gas C.(Raw_context.T.init_value) s i v in
    let=? t_value := C.(Raw_context.T.init_value) s (data_key i) bytes_value in
    let size := Bytes.length bytes_value in
    return=? ((C.(Raw_context.T.project) t_value), size).
  
  Definition init_set `{FArgs}
    (s : C.(Raw_context.T.t)) (i : I.(INDEX.t)) (v : V.(Storage_sigs.VALUE.t))
    : M=? (Raw_context.root_context * int * bool) :=
    let init_set {A : Set}
      (s : C.(Raw_context.T.t)) (i : Raw_context.key) (v : Raw_context.value)
      : M= (Pervasives.result C.(Raw_context.T.t) A) :=
      Error_monad.op_gtpipeeq (C.(Raw_context.T.init_set) s i v) Error_monad.ok
      in
    let=? '(prev_size, existed) := existing_size s i in
    let=? '(s, bytes_value) := consume_serialize_write_gas init_set s i v in
    let=? t_value := init_set s (data_key i) bytes_value in
    let size_diff := (Bytes.length bytes_value) -i prev_size in
    return=? ((C.(Raw_context.T.project) t_value), size_diff, existed).
  
  Definition remove `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
    : M=? (Raw_context.root_context * int * bool) :=
    let remove {A : Set} (s : C.(Raw_context.T.t)) (i : Raw_context.key)
      : M= (Pervasives.result C.(Raw_context.T.t) A) :=
      Error_monad.op_gtpipeeq (C.(Raw_context.T.remove) s i) Error_monad.ok in
    let=? '(prev_size, existed) := existing_size s i in
    let=? s := consume_remove_gas remove s i in
    let=? t_value := remove s (data_key i) in
    return=? ((C.(Raw_context.T.project) t_value), prev_size, existed).
  
  Definition delete `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
    : M=? (Raw_context.root_context * int) :=
    let=? '(prev_size, _) := existing_size s i in
    let=? s := consume_remove_gas C.(Raw_context.T.delete) s i in
    let=? t_value := C.(Raw_context.T.delete) s (data_key i) in
    return=? ((C.(Raw_context.T.project) t_value), prev_size).
  
  Definition set_option `{FArgs}
    (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
    (v : option V.(Storage_sigs.VALUE.t))
    : M=? (Raw_context.root_context * int * bool) :=
    match v with
    | None => remove s i
    | Some v => init_set s i v
    end.
  
  Definition fold_keys_unaccounted `{FArgs} {A : Set}
    (s : C.(Raw_context.T.t)) (init_value : A) (f : I.(INDEX.t) -> A -> M= A)
    : M= A :=
    let fix dig (i : int) (path : Raw_context.key) (acc_value : A) {struct i}
      : M= A :=
      if i <=i 0 then
        C.(Raw_context.T.fold) s path acc_value
          (fun (k : Context.key_or_dir) =>
            fun (acc_value : A) =>
              match k with
              | Context.Dir _ => Lwt._return acc_value
              | Context.Key file =>
                match
                  ((List.rev file),
                    match List.rev file with
                    | cons last _ =>
                      Compare.String.(Compare.S.op_eq) last len_name
                    | _ => false
                    end,
                    match List.rev file with
                    | cons last rest =>
                      Compare.String.(Compare.S.op_eq) last data_name
                    | _ => false
                    end) with
                | (cons last _, true, _) => Lwt._return acc_value
                | (cons last rest, _, true) =>
                  let file := List.rev rest in
                  match I.(INDEX.of_path) file with
                  | None =>
                    (* ❌ Assert instruction is not handled. *)
                    assert (M= _) false
                  | Some path => f path acc_value
                  end
                | (_, _, _) =>
                  (* ❌ Assert instruction is not handled. *)
                  assert (M= _) false
                end
              end)
      else
        C.(Raw_context.T.fold) s path acc_value
          (fun (k : Context.key_or_dir) =>
            fun (acc_value : A) =>
              match k with
              | Context.Dir k => dig (i -i 1) k acc_value
              | Context.Key _ => Lwt._return acc_value
              end) in
    dig I.(INDEX.path_length) nil init_value.
  
  Definition keys_unaccounted `{FArgs} (s : C.(Raw_context.T.t))
    : M= (list I.(INDEX.t)) :=
    fold_keys_unaccounted s (nil (A := key))
      (fun (p_value : I.(INDEX.t)) =>
        fun (acc_value : list I.(INDEX.t)) =>
          Lwt._return (cons p_value acc_value)).
  
  (** Init function; without side-effects in Coq *)
  Definition init_module `{FArgs} : unit :=
    let unpack {A : Set} (c : I.(INDEX.ipath) A) : A * I.(INDEX.t) :=
      Storage_description.unpack I.(INDEX.args) c in
    Storage_description.register_value
      (Storage_description.register_indexed_subcontext
        C.(Raw_context.T.description)
        (fun (c : C.(Raw_context.T.t)) =>
          Error_monad.op_gtpipeeq (keys_unaccounted c) Error_monad.ok)
        I.(INDEX.args))
      (fun (c : I.(INDEX.ipath) C.(Raw_context.T.t)) =>
        let '(c, k) := unpack c in
        let=? '(_, v) := get_option c k in
        return=? v) V.(Storage_sigs.VALUE.encoding).
  
  Definition functor `{FArgs} :=
    {|
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.mem :=
        mem;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.get :=
        get;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.get_option :=
        get_option;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.set :=
        set;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.init_value :=
        init_value;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.init_set :=
        init_set;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.set_option :=
        set_option;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.delete :=
        delete;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.remove :=
        remove;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.fold_keys_unaccounted
        _ := fold_keys_unaccounted
    |}.
End Make_indexed_carbonated_data_storage_INTERNAL.
Definition Make_indexed_carbonated_data_storage_INTERNAL
  {C_t I_t : Set} {I_ipath : Set -> Set} {V_t : Set}
  (C : Raw_context.T (t := C_t)) (I : INDEX (t := I_t) (ipath := I_ipath))
  (V : Storage_sigs.VALUE (t := V_t))
  : Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL
    (t := C.(Raw_context.T.t)) (key := I.(INDEX.t))
    (value := V.(Storage_sigs.VALUE.t)) :=
  let '_ := Make_indexed_carbonated_data_storage_INTERNAL.Build_FArgs C I V in
  Make_indexed_carbonated_data_storage_INTERNAL.functor.

Module Make_indexed_carbonated_data_storage.
  Class FArgs {C_t I_t : Set} {I_ipath : Set -> Set} {V_t : Set} := {
    C : Raw_context.T (t := C_t);
    I : INDEX (t := I_t) (ipath := I_ipath);
    V : Storage_sigs.VALUE (t := V_t);
  }.
  Arguments Build_FArgs {_ _ _ _}.
  
  Definition Make_indexed_carbonated_data_storage_INTERNAL_include `{FArgs} :=
    Make_indexed_carbonated_data_storage_INTERNAL C I V.
  
  (** Inclusion of the module [Make_indexed_carbonated_data_storage_INTERNAL_include] *)
  Definition t `{FArgs} :=
    Make_indexed_carbonated_data_storage_INTERNAL_include.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.t).
  
  Definition context `{FArgs} :=
    Make_indexed_carbonated_data_storage_INTERNAL_include.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.context).
  
  Definition key `{FArgs} :=
    Make_indexed_carbonated_data_storage_INTERNAL_include.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.key).
  
  Definition value `{FArgs} :=
    Make_indexed_carbonated_data_storage_INTERNAL_include.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.value).
  
  Definition mem `{FArgs} :=
    Make_indexed_carbonated_data_storage_INTERNAL_include.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.mem).
  
  Definition get `{FArgs} :=
    Make_indexed_carbonated_data_storage_INTERNAL_include.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.get).
  
  Definition get_option `{FArgs} :=
    Make_indexed_carbonated_data_storage_INTERNAL_include.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.get_option).
  
  Definition set `{FArgs} :=
    Make_indexed_carbonated_data_storage_INTERNAL_include.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.set).
  
  Definition init_value `{FArgs} :=
    Make_indexed_carbonated_data_storage_INTERNAL_include.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.init_value).
  
  Definition init_set `{FArgs} :=
    Make_indexed_carbonated_data_storage_INTERNAL_include.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.init_set).
  
  Definition set_option `{FArgs} :=
    Make_indexed_carbonated_data_storage_INTERNAL_include.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.set_option).
  
  Definition delete `{FArgs} :=
    Make_indexed_carbonated_data_storage_INTERNAL_include.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.delete).
  
  Definition remove `{FArgs} :=
    Make_indexed_carbonated_data_storage_INTERNAL_include.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.remove).
  
  Definition fold_keys_unaccounted `{FArgs} {a : Set} :=
    Make_indexed_carbonated_data_storage_INTERNAL_include.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.fold_keys_unaccounted)
      (a := a).
  
  Definition functor `{FArgs} :=
    {|
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.mem := mem;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.get := get;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.get_option :=
        get_option;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.set := set;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.init_value :=
        init_value;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.init_set :=
        init_set;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.set_option :=
        set_option;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.delete := delete;
      Storage_sigs.Non_iterable_indexed_carbonated_data_storage.remove := remove
    |}.
End Make_indexed_carbonated_data_storage.
Definition Make_indexed_carbonated_data_storage
  {C_t I_t : Set} {I_ipath : Set -> Set} {V_t : Set}
  (C : Raw_context.T (t := C_t)) (I : INDEX (t := I_t) (ipath := I_ipath))
  (V : Storage_sigs.VALUE (t := V_t))
  : Storage_sigs.Non_iterable_indexed_carbonated_data_storage
    (t := C.(Raw_context.T.t)) (key := I.(INDEX.t))
    (value := V.(Storage_sigs.VALUE.t)) :=
  let '_ := Make_indexed_carbonated_data_storage.Build_FArgs C I V in
  Make_indexed_carbonated_data_storage.functor.

Module Make_carbonated_data_set_storage.
  Class FArgs {C_t I_t : Set} {I_ipath : Set -> Set} := {
    C : Raw_context.T (t := C_t);
    I : INDEX (t := I_t) (ipath := I_ipath);
  }.
  Arguments Build_FArgs {_ _ _}.
  
  Module V.
    Definition t `{FArgs} : Set := unit.
    
    Definition encoding `{FArgs} : Data_encoding.encoding unit :=
      Data_encoding.unit_value.
    
    Definition module `{FArgs} :=
      {|
        Storage_sigs.VALUE.encoding := encoding
      |}.
  End V.
  Definition V `{FArgs} := V.module.
  
  Definition M `{FArgs} := Make_indexed_carbonated_data_storage_INTERNAL C I V.
  
  Definition t `{FArgs} : Set := C.(Raw_context.T.t).
  
  Definition context `{FArgs} : Set := t.
  
  Definition elt `{FArgs} : Set := I.(INDEX.t).
  
  Definition mem `{FArgs}
    : C.(Raw_context.T.t) -> I.(INDEX.t) -> M=? (Raw_context.t * bool) :=
    M.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.mem).
  
  Definition init_value `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
    : M=? (Raw_context.t * int) :=
    M.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.init_value)
      s i tt.
  
  Definition del `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
    : M=? (Raw_context.t * int * bool) :=
    M.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.remove)
      s i.
  
  Definition fold_keys_unaccounted `{FArgs} {A : Set}
    : C.(Raw_context.T.t) -> A -> (I.(INDEX.t) -> A -> M= A) -> M= A :=
    M.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_INTERNAL.fold_keys_unaccounted).
  
  Definition functor `{FArgs} :=
    {|
      Storage_sigs.Carbonated_data_set_storage.mem := mem;
      Storage_sigs.Carbonated_data_set_storage.init_value := init_value;
      Storage_sigs.Carbonated_data_set_storage.del := del;
      Storage_sigs.Carbonated_data_set_storage.fold_keys_unaccounted _ :=
        fold_keys_unaccounted
    |}.
End Make_carbonated_data_set_storage.
Definition Make_carbonated_data_set_storage
  {C_t I_t : Set} {I_ipath : Set -> Set}
  (C : Raw_context.T (t := C_t)) (I : INDEX (t := I_t) (ipath := I_ipath))
  : Storage_sigs.Carbonated_data_set_storage (t := C.(Raw_context.T.t))
    (elt := I.(INDEX.t)) :=
  let '_ := Make_carbonated_data_set_storage.Build_FArgs C I in
  Make_carbonated_data_set_storage.functor.

Module Make_indexed_data_snapshotable_storage.
  Class FArgs {C_t Snapshot_index_t : Set} {Snapshot_index_ipath : Set -> Set}
    {I_t : Set} {I_ipath : Set -> Set} {V_t : Set} := {
    C : Raw_context.T (t := C_t);
    Snapshot_index :
      INDEX (t := Snapshot_index_t) (ipath := Snapshot_index_ipath);
    I : INDEX (t := I_t) (ipath := I_ipath);
    V : Storage_sigs.VALUE (t := V_t);
  }.
  Arguments Build_FArgs {_ _ _ _ _ _}.
  
  Definition snapshot `{FArgs} : Set := Snapshot_index.(INDEX.t).
  
  Definition data_name `{FArgs} : list string := [ "current" ].
  
  Definition snapshot_name `{FArgs} : list string := [ "snapshot" ].
  
  Definition C_data `{FArgs} :=
    Make_subcontext Registered C
      (let name := data_name in
      {|
        Storage_sigs.NAME.name := name
      |}).
  
  Definition C_snapshot `{FArgs} :=
    Make_subcontext Registered C
      (let name := snapshot_name in
      {|
        Storage_sigs.NAME.name := name
      |}).
  
  Definition Make_indexed_data_storage_include `{FArgs} :=
    Make_indexed_data_storage C_data I V.
  
  (** Inclusion of the module [Make_indexed_data_storage_include] *)
  Definition t `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.t).
  
  Definition context `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.context).
  
  Definition key `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.key).
  
  Definition value `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.value).
  
  Definition mem `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.mem).
  
  Definition get `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.get).
  
  Definition get_option `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.get_option).
  
  Definition set `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.set).
  
  Definition init_value `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.init_value).
  
  Definition init_set `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.init_set).
  
  Definition set_option `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.set_option).
  
  Definition delete `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.delete).
  
  Definition remove `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.remove).
  
  Definition clear `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.clear).
  
  Definition keys `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.keys).
  
  Definition bindings `{FArgs} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.bindings).
  
  Definition fold `{FArgs} {a : Set} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.fold)
      (a := a).
  
  Definition fold_keys `{FArgs} {a : Set} :=
    Make_indexed_data_storage_include.(Storage_sigs.Indexed_data_storage.fold_keys)
      (a := a).
  
  Definition Snapshot `{FArgs} :=
    Make_indexed_data_storage C_snapshot (Pair Snapshot_index I) V.
  
  Definition snapshot_path `{FArgs} (id : Snapshot_index.(INDEX.t))
    : list string :=
    Pervasives.op_at snapshot_name (Snapshot_index.(INDEX.to_path) id nil).
  
  Definition snapshot_exists `{FArgs}
    (s : C.(Raw_context.T.t)) (id : Snapshot_index.(INDEX.t)) : M= bool :=
    C.(Raw_context.T.dir_mem) s (snapshot_path id).
  
  Definition snapshot_value `{FArgs}
    (s : C.(Raw_context.T.t)) (id : Snapshot_index.(INDEX.t))
    : M=? Raw_context.root_context :=
    let=? t_value := C.(Raw_context.T.copy) s data_name (snapshot_path id) in
    return=? (C.(Raw_context.T.project) t_value).
  
  Definition delete_snapshot `{FArgs}
    (s : C.(Raw_context.T.t)) (id : Snapshot_index.(INDEX.t))
    : M= Raw_context.root_context :=
    let= t_value := C.(Raw_context.T.remove_rec) s (snapshot_path id) in
    return= (C.(Raw_context.T.project) t_value).
  
  Definition functor `{FArgs} :=
    {|
      Storage_sigs.Indexed_data_snapshotable_storage.mem := mem;
      Storage_sigs.Indexed_data_snapshotable_storage.get := get;
      Storage_sigs.Indexed_data_snapshotable_storage.get_option := get_option;
      Storage_sigs.Indexed_data_snapshotable_storage.set := set;
      Storage_sigs.Indexed_data_snapshotable_storage.init_value := init_value;
      Storage_sigs.Indexed_data_snapshotable_storage.init_set := init_set;
      Storage_sigs.Indexed_data_snapshotable_storage.set_option := set_option;
      Storage_sigs.Indexed_data_snapshotable_storage.delete := delete;
      Storage_sigs.Indexed_data_snapshotable_storage.remove := remove;
      Storage_sigs.Indexed_data_snapshotable_storage.clear := clear;
      Storage_sigs.Indexed_data_snapshotable_storage.keys := keys;
      Storage_sigs.Indexed_data_snapshotable_storage.bindings := bindings;
      Storage_sigs.Indexed_data_snapshotable_storage.fold _ := fold;
      Storage_sigs.Indexed_data_snapshotable_storage.fold_keys _ := fold_keys;
      Storage_sigs.Indexed_data_snapshotable_storage.Snapshot := Snapshot;
      Storage_sigs.Indexed_data_snapshotable_storage.snapshot_exists :=
        snapshot_exists;
      Storage_sigs.Indexed_data_snapshotable_storage.snapshot_value :=
        snapshot_value;
      Storage_sigs.Indexed_data_snapshotable_storage.delete_snapshot :=
        delete_snapshot
    |}.
End Make_indexed_data_snapshotable_storage.
Definition Make_indexed_data_snapshotable_storage
  {C_t Snapshot_index_t : Set} {Snapshot_index_ipath : Set -> Set} {I_t : Set}
  {I_ipath : Set -> Set} {V_t : Set}
  (C : Raw_context.T (t := C_t))
  (Snapshot_index :
    INDEX (t := Snapshot_index_t) (ipath := Snapshot_index_ipath))
  (I : INDEX (t := I_t) (ipath := I_ipath)) (V : Storage_sigs.VALUE (t := V_t))
  : Storage_sigs.Indexed_data_snapshotable_storage
    (snapshot := Snapshot_index.(INDEX.t)) (key := I.(INDEX.t))
    (t := C.(Raw_context.T.t)) (value := V.(Storage_sigs.VALUE.t)) :=
  let '_ :=
    Make_indexed_data_snapshotable_storage.Build_FArgs C Snapshot_index I V in
  Make_indexed_data_snapshotable_storage.functor.

Module Make_indexed_subcontext.
  Class FArgs {C_t I_t : Set} {I_ipath : Set -> Set} := {
    C : Raw_context.T (t := C_t);
    I : INDEX (t := I_t) (ipath := I_ipath);
  }.
  Arguments Build_FArgs {_ _ _}.
  
  Definition t `{FArgs} : Set := C.(Raw_context.T.t).
  
  Definition context `{FArgs} : Set := t.
  
  Definition key `{FArgs} : Set := I.(INDEX.t).
  
  Definition ipath `{FArgs} (a : Set) : Set := I.(INDEX.ipath) a.
  
  Definition clear `{FArgs} (t_value : C.(Raw_context.T.t))
    : M= Raw_context.root_context :=
    let= t_value := C.(Raw_context.T.remove_rec) t_value nil in
    return= (C.(Raw_context.T.project) t_value).
  
  Definition fold_keys `{FArgs} {A : Set}
    (t_value : C.(Raw_context.T.t)) (init_value : A)
    (f : I.(INDEX.t) -> A -> M= A) : M= A :=
    let fix dig (i : int) (path : Raw_context.key) (acc_value : A) {struct i}
      : M= A :=
      if i <=i 0 then
        match I.(INDEX.of_path) path with
        | None =>
          (* ❌ Assert instruction is not handled. *)
          assert (M= _) false
        | Some path => f path acc_value
        end
      else
        C.(Raw_context.T.fold) t_value path acc_value
          (fun (k : Context.key_or_dir) =>
            fun (acc_value : A) =>
              match k with
              | Context.Dir k => dig (i -i 1) k acc_value
              | Context.Key _ => Lwt._return acc_value
              end) in
    dig I.(INDEX.path_length) nil init_value.
  
  Definition keys `{FArgs} (t_value : C.(Raw_context.T.t))
    : M= (list I.(INDEX.t)) :=
    fold_keys t_value (nil (A := key))
      (fun (i : I.(INDEX.t)) =>
        fun (acc_value : list I.(INDEX.t)) => Lwt._return (cons i acc_value)).
  
  Definition list_value `{FArgs}
    (t_value : C.(Raw_context.T.t)) (k : Raw_context.key)
    : M= (list Context.key_or_dir) :=
    C.(Raw_context.T.fold) t_value k nil
      (fun (k : Context.key_or_dir) =>
        fun (acc_value : list Context.key_or_dir) =>
          Lwt._return (cons k acc_value)).
  
  Definition remove_rec `{FArgs}
    (t_value : C.(Raw_context.T.t)) (k : I.(INDEX.t))
    : M= C.(Raw_context.T.t) :=
    C.(Raw_context.T.remove_rec) t_value (I.(INDEX.to_path) k nil).
  
  Definition copy `{FArgs}
    (t_value : C.(Raw_context.T.t)) (from : I.(INDEX.t)) (to_ : I.(INDEX.t))
    : M=? C.(Raw_context.T.t) :=
    C.(Raw_context.T.copy) t_value (I.(INDEX.to_path) from nil)
      (I.(INDEX.to_path) to_ nil).
  
  Definition description `{FArgs}
    : Storage_description.t (I.(INDEX.ipath) C.(Raw_context.T.t)) :=
    Storage_description.register_indexed_subcontext
      C.(Raw_context.T.description)
      (fun (c : C.(Raw_context.T.t)) =>
        Error_monad.op_gtpipeeq (keys c) Error_monad.ok) I.(INDEX.args).
  
  Definition unpack `{FArgs}
    : I.(INDEX.ipath) C.(Raw_context.T.t) -> C.(Raw_context.T.t) * I.(INDEX.t) :=
    Storage_description.unpack I.(INDEX.args).
  
  Definition _pack `{FArgs}
    : C.(Raw_context.T.t) -> I.(INDEX.t) -> I.(INDEX.ipath) C.(Raw_context.T.t) :=
    Storage_description._pack I.(INDEX.args).
  
  Module Raw_context.
    Definition t `{FArgs} : Set := I.(INDEX.ipath) C.(Raw_context.T.t).
    
    Definition context `{FArgs} : Set := t.
    
    Definition to_key `{FArgs} (i : I.(INDEX.t)) (k : list string)
      : list string := I.(INDEX.to_path) i k.
    
    Definition of_key `{FArgs} {A : Set} (k : list A) : list A :=
      Misc.remove_elem_from_list I.(INDEX.path_length) k.
    
    Definition mem `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context.T.t)) (k : list string) : M= bool :=
      let '(t_value, i) := unpack c in
      C.(Raw_context.T.mem) t_value (to_key i k).
    
    Definition dir_mem `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context.T.t)) (k : list string) : M= bool :=
      let '(t_value, i) := unpack c in
      C.(Raw_context.T.dir_mem) t_value (to_key i k).
    
    Definition get `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context.T.t)) (k : list string)
      : M=? Raw_context.value :=
      let '(t_value, i) := unpack c in
      C.(Raw_context.T.get) t_value (to_key i k).
    
    Definition get_option `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context.T.t)) (k : list string)
      : M= (option Raw_context.value) :=
      let '(t_value, i) := unpack c in
      C.(Raw_context.T.get_option) t_value (to_key i k).
    
    Definition init_value `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context.T.t)) (k : list string)
      (v : Raw_context.value) : M=? (I.(INDEX.ipath) C.(Raw_context.T.t)) :=
      let '(t_value, i) := unpack c in
      let=? t_value := C.(Raw_context.T.init_value) t_value (to_key i k) v in
      return=? (_pack t_value i).
    
    Definition set `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context.T.t)) (k : list string)
      (v : Raw_context.value) : M=? (I.(INDEX.ipath) C.(Raw_context.T.t)) :=
      let '(t_value, i) := unpack c in
      let=? t_value := C.(Raw_context.T.set) t_value (to_key i k) v in
      return=? (_pack t_value i).
    
    Definition init_set `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context.T.t)) (k : list string)
      (v : Raw_context.value) : M= (I.(INDEX.ipath) C.(Raw_context.T.t)) :=
      let '(t_value, i) := unpack c in
      let= t_value := C.(Raw_context.T.init_set) t_value (to_key i k) v in
      return= (_pack t_value i).
    
    Definition set_option `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context.T.t)) (k : list string)
      (v : option Raw_context.value)
      : M= (I.(INDEX.ipath) C.(Raw_context.T.t)) :=
      let '(t_value, i) := unpack c in
      let= t_value := C.(Raw_context.T.set_option) t_value (to_key i k) v in
      return= (_pack t_value i).
    
    Definition delete `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context.T.t)) (k : list string)
      : M=? (I.(INDEX.ipath) C.(Raw_context.T.t)) :=
      let '(t_value, i) := unpack c in
      let=? t_value := C.(Raw_context.T.delete) t_value (to_key i k) in
      return=? (_pack t_value i).
    
    Definition remove `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context.T.t)) (k : list string)
      : M= (I.(INDEX.ipath) C.(Raw_context.T.t)) :=
      let '(t_value, i) := unpack c in
      let= t_value := C.(Raw_context.T.remove) t_value (to_key i k) in
      return= (_pack t_value i).
    
    Definition remove_rec `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context.T.t)) (k : list string)
      : M= (I.(INDEX.ipath) C.(Raw_context.T.t)) :=
      let '(t_value, i) := unpack c in
      let= t_value := C.(Raw_context.T.remove_rec) t_value (to_key i k) in
      return= (_pack t_value i).
    
    Definition copy `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context.T.t)) (from : list string)
      (to_ : list string) : M=? (I.(INDEX.ipath) C.(Raw_context.T.t)) :=
      let '(t_value, i) := unpack c in
      let=? t_value :=
        C.(Raw_context.T.copy) t_value (to_key i from) (to_key i to_) in
      return=? (_pack t_value i).
    
    Definition fold `{FArgs} {A : Set}
      (c : I.(INDEX.ipath) C.(Raw_context.T.t)) (k : list string)
      (init_value : A) (f : Context.key_or_dir -> A -> M= A) : M= A :=
      let '(t_value, i) := unpack c in
      C.(Raw_context.T.fold) t_value (to_key i k) init_value
        (fun (k : Context.key_or_dir) =>
          fun (acc_value : A) => f (map_key of_key k) acc_value).
    
    Definition keys `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context.T.t)) (k : list string)
      : M= (list (list string)) :=
      let '(t_value, i) := unpack c in
      let= keys := C.(Raw_context.T.keys) t_value (to_key i k) in
      return= (List.map of_key keys).
    
    Definition fold_keys `{FArgs} {A : Set}
      (c : I.(INDEX.ipath) C.(Raw_context.T.t)) (k : list string)
      (init_value : A) (f : list string -> A -> M= A) : M= A :=
      let '(t_value, i) := unpack c in
      C.(Raw_context.T.fold_keys) t_value (to_key i k) init_value
        (fun (k : Raw_context.key) =>
          fun (acc_value : A) => f (of_key k) acc_value).
    
    Definition project `{FArgs} (c : I.(INDEX.ipath) C.(Raw_context.T.t))
      : Raw_context.root_context :=
      let '(t_value, _) := unpack c in
      C.(Raw_context.T.project) t_value.
    
    Definition absolute_key `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context.T.t)) (k : list string)
      : Raw_context.key :=
      let '(t_value, i) := unpack c in
      C.(Raw_context.T.absolute_key) t_value (to_key i k).
    
    Definition consume_gas `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context.T.t)) (g : Gas_limit_repr.cost)
      : M? (I.(INDEX.ipath) C.(Raw_context.T.t)) :=
      let '(t_value, i) := unpack c in
      let? t_value := C.(Raw_context.T.consume_gas) t_value g in
      return? (_pack t_value i).
    
    Definition check_enough_gas `{FArgs}
      (c : I.(INDEX.ipath) C.(Raw_context.T.t)) (g : Gas_limit_repr.cost)
      : M? unit :=
      let '(t_value, _i) := unpack c in
      C.(Raw_context.T.check_enough_gas) t_value g.
    
    Definition description `{FArgs}
      : Storage_description.t (I.(INDEX.ipath) C.(Raw_context.T.t)) :=
      description.
    
    Definition module `{FArgs} :=
      {|
        Raw_context.T.mem := mem;
        Raw_context.T.dir_mem := dir_mem;
        Raw_context.T.get := get;
        Raw_context.T.get_option := get_option;
        Raw_context.T.init_value := init_value;
        Raw_context.T.set := set;
        Raw_context.T.init_set := init_set;
        Raw_context.T.set_option := set_option;
        Raw_context.T.delete := delete;
        Raw_context.T.remove := remove;
        Raw_context.T.remove_rec := remove_rec;
        Raw_context.T.copy := copy;
        Raw_context.T.fold _ := fold;
        Raw_context.T.keys := keys;
        Raw_context.T.fold_keys _ := fold_keys;
        Raw_context.T.project := project;
        Raw_context.T.absolute_key := absolute_key;
        Raw_context.T.consume_gas := consume_gas;
        Raw_context.T.check_enough_gas := check_enough_gas;
        Raw_context.T.description := description
      |}.
  End Raw_context.
  Definition Raw_context `{FArgs} : Raw_context.T (t := ipath t) :=
    Raw_context.module.
  
  Definition resolve `{FArgs}
    (t_value : C.(Raw_context.T.t)) (prefix : list string)
    : M= (list I.(INDEX.t)) :=
    let fix loop
      (i : int) (prefix : Raw_context.key) (function_parameter : list string)
      {struct i} : M= (list I.(INDEX.t)) :=
      match
        (function_parameter,
          match function_parameter with
          | [] => i =i I.(INDEX.path_length)
          | _ => false
          end,
          match function_parameter with
          | cons d [] => i =i (I.(INDEX.path_length) -i 1)
          | _ => false
          end) with
      | ([], true, _) =>
        match I.(INDEX.of_path) prefix with
        | None =>
          (* ❌ Assert instruction is not handled. *)
          assert (M= (list I.(INDEX.t))) false
        | Some path => Lwt._return [ path ]
        end
      | ([], _, _) =>
        let= prefixes := list_value t_value prefix in
        Error_monad.op_gtpipeeq
          (Lwt_list.map_s
            (fun (function_parameter : Context.key_or_dir) =>
              match function_parameter with
              | (Context.Key prefix | Context.Dir prefix) =>
                loop (i +i 1) prefix nil
              end) prefixes) List.flatten
      | (cons d [], _, true) =>
        let '_ :=
          if i >=i I.(INDEX.path_length) then
            Pervasives.invalid_arg "IO.resolve"
          else
            tt in
        let= prefixes := list_value t_value prefix in
        Error_monad.op_gtpipeeq
          (Lwt_list.map_s
            (fun (function_parameter : Context.key_or_dir) =>
              match function_parameter with
              | (Context.Key prefix | Context.Dir prefix) =>
                match Misc.remove_prefix d (List.hd (List.rev prefix)) with
                | None => Lwt.return_nil
                | Some _ => loop (i +i 1) prefix nil
                end
              end) prefixes) List.flatten
      | (cons "" ds, _, _) =>
        let= prefixes := list_value t_value prefix in
        Error_monad.op_gtpipeeq
          (Lwt_list.map_s
            (fun (function_parameter : Context.key_or_dir) =>
              match function_parameter with
              | (Context.Key prefix | Context.Dir prefix) =>
                loop (i +i 1) prefix ds
              end) prefixes) List.flatten
      | (cons d ds, _, _) =>
        let '_ :=
          if i >=i I.(INDEX.path_length) then
            Pervasives.invalid_arg "IO.resolve"
          else
            tt in
        let= function_parameter :=
          C.(Raw_context.T.dir_mem) t_value (Pervasives.op_at prefix [ d ]) in
        match function_parameter with
        | true => loop (i +i 1) (Pervasives.op_at prefix [ d ]) ds
        | false => Lwt.return_nil
        end
      end in
    loop 0 nil prefix.
  
  Module Make_set.
    Class FArgs `{FArgs} := {
      R : Storage_sigs.REGISTER;
      N : Storage_sigs.NAME;
    }.
    Arguments Build_FArgs {_ _ _ _}.
    
    Definition t `{FArgs} : Set := C.(Raw_context.T.t).
    
    Definition context `{FArgs} : Set := t.
    
    Definition elt `{FArgs} : Set := I.(INDEX.t).
    
    Definition inited `{FArgs} : bytes := Bytes.of_string "inited".
    
    Definition mem `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
      : M= bool :=
      Raw_context.(Raw_context.T.mem) (_pack s i) N.(Storage_sigs.NAME.name).
    
    Definition add `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
      : M= Raw_context.root_context :=
      let= c :=
        Raw_context.(Raw_context.T.init_set) (_pack s i)
          N.(Storage_sigs.NAME.name) inited in
      let '(s, _) := unpack c in
      return= (C.(Raw_context.T.project) s).
    
    Definition del `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
      : M= Raw_context.root_context :=
      let= c :=
        Raw_context.(Raw_context.T.remove) (_pack s i)
          N.(Storage_sigs.NAME.name) in
      let '(s, _) := unpack c in
      return= (C.(Raw_context.T.project) s).
    
    Definition set `{FArgs}
      (s : C.(Raw_context.T.t)) (i : I.(INDEX.t)) (function_parameter : bool)
      : M= Raw_context.root_context :=
      match function_parameter with
      | true => add s i
      | false => del s i
      end.
    
    Definition clear `{FArgs} (s : C.(Raw_context.T.t))
      : M= Raw_context.root_context :=
      let= t_value :=
        fold_keys s s
          (fun (i : I.(INDEX.t)) =>
            fun (s : C.(Raw_context.T.t)) =>
              let= c :=
                Raw_context.(Raw_context.T.remove) (_pack s i)
                  N.(Storage_sigs.NAME.name) in
              let '(s, _) := unpack c in
              return= s) in
      return= (C.(Raw_context.T.project) t_value).
    
    Definition fold `{FArgs} {A : Set}
      (s : C.(Raw_context.T.t)) (init_value : A) (f : I.(INDEX.t) -> A -> M= A)
      : M= A :=
      fold_keys s init_value
        (fun (i : I.(INDEX.t)) =>
          fun (acc_value : A) =>
            let= function_parameter := mem s i in
            match function_parameter with
            | true => f i acc_value
            | false => Lwt._return acc_value
            end).
    
    Definition elements `{FArgs} (s : C.(Raw_context.T.t))
      : M= (list I.(INDEX.t)) :=
      fold s (nil (A := elt))
        (fun (p_value : I.(INDEX.t)) =>
          fun (acc_value : list I.(INDEX.t)) =>
            Lwt._return (cons p_value acc_value)).
    
    Definition description `{FArgs}
      : Storage_description.t Raw_context.(Raw_context.T.t) :=
      if R.(Storage_sigs.REGISTER.ghost) then
        Storage_description.create tt
      else
        Raw_context.(Raw_context.T.description).
    
    (** Init function; without side-effects in Coq *)
    Definition init_module `{FArgs} : unit :=
      Storage_description.register_value
        (Storage_description.register_named_subcontext description
          N.(Storage_sigs.NAME.name))
        (fun (c : Raw_context.(Raw_context.T.t)) =>
          let '(c, k) := Storage_description.unpack I.(INDEX.args) c in
          let= function_parameter := mem c k in
          match function_parameter with
          | true => Error_monad.return_some true
          | false => Error_monad.return_none
          end) Data_encoding.bool_value.
    
    Definition functor `{FArgs} :=
      {|
        Storage_sigs.Data_set_storage.mem := mem;
        Storage_sigs.Data_set_storage.add := add;
        Storage_sigs.Data_set_storage.del := del;
        Storage_sigs.Data_set_storage.set := set;
        Storage_sigs.Data_set_storage.elements := elements;
        Storage_sigs.Data_set_storage.fold _ := fold;
        Storage_sigs.Data_set_storage.clear := clear
      |}.
  End Make_set.
  Definition Make_set `{FArgs}
    (R : Storage_sigs.REGISTER) (N : Storage_sigs.NAME)
    : Storage_sigs.Data_set_storage (t := t) (elt := key) :=
    let '_ := Make_set.Build_FArgs R N in
    Make_set.functor.
  
  Module Make_map.
    Class FArgs `{FArgs} {V_t : Set} := {
      N : Storage_sigs.NAME;
      V : Storage_sigs.VALUE (t := V_t);
    }.
    Arguments Build_FArgs {_ _ _ _ _}.
    
    Definition t `{FArgs} : Set := C.(Raw_context.T.t).
    
    Definition context `{FArgs} : Set := t.
    
    Definition key `{FArgs} : Set := I.(INDEX.t).
    
    Definition value `{FArgs} : Set := V.(Storage_sigs.VALUE.t).
    
    Definition Make_encoder_include `{FArgs} := Make_encoder V.
    
    (** Inclusion of the module [Make_encoder_include] *)
    Definition of_bytes `{FArgs} := Make_encoder_include.(ENCODER.of_bytes).
    
    Definition to_bytes `{FArgs} := Make_encoder_include.(ENCODER.to_bytes).
    
    Definition mem `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
      : M= bool :=
      Raw_context.(Raw_context.T.mem) (_pack s i) N.(Storage_sigs.NAME.name).
    
    Definition get `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
      : M=? V.(Storage_sigs.VALUE.t) :=
      let=? b_value :=
        Raw_context.(Raw_context.T.get) (_pack s i) N.(Storage_sigs.NAME.name)
        in
      let key_value :=
        Raw_context.(Raw_context.T.absolute_key) (_pack s i)
          N.(Storage_sigs.NAME.name) in
      Lwt._return (of_bytes key_value b_value).
    
    Definition get_option `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
      : M=? (option V.(Storage_sigs.VALUE.t)) :=
      let= function_parameter :=
        Raw_context.(Raw_context.T.get_option) (_pack s i)
          N.(Storage_sigs.NAME.name) in
      match function_parameter with
      | None => return= Error_monad.ok_none
      | Some b_value =>
        let key_value :=
          Raw_context.(Raw_context.T.absolute_key) (_pack s i)
            N.(Storage_sigs.NAME.name) in
        return=
          (let? v := of_bytes key_value b_value in
          return? (Some v))
      end.
    
    Definition set `{FArgs}
      (s : C.(Raw_context.T.t)) (i : I.(INDEX.t)) (v : V.(Storage_sigs.VALUE.t))
      : M=? Raw_context.root_context :=
      let=? c :=
        Raw_context.(Raw_context.T.set) (_pack s i) N.(Storage_sigs.NAME.name)
          (to_bytes v) in
      let '(s, _) := unpack c in
      return=? (C.(Raw_context.T.project) s).
    
    Definition init_value `{FArgs}
      (s : C.(Raw_context.T.t)) (i : I.(INDEX.t)) (v : V.(Storage_sigs.VALUE.t))
      : M=? Raw_context.root_context :=
      let=? c :=
        Raw_context.(Raw_context.T.init_value) (_pack s i)
          N.(Storage_sigs.NAME.name) (to_bytes v) in
      let '(s, _) := unpack c in
      return=? (C.(Raw_context.T.project) s).
    
    Definition init_set `{FArgs}
      (s : C.(Raw_context.T.t)) (i : I.(INDEX.t)) (v : V.(Storage_sigs.VALUE.t))
      : M= Raw_context.root_context :=
      let= c :=
        Raw_context.(Raw_context.T.init_set) (_pack s i)
          N.(Storage_sigs.NAME.name) (to_bytes v) in
      let '(s, _) := unpack c in
      return= (C.(Raw_context.T.project) s).
    
    Definition set_option `{FArgs}
      (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
      (v : option V.(Storage_sigs.VALUE.t)) : M= Raw_context.root_context :=
      let= c :=
        Raw_context.(Raw_context.T.set_option) (_pack s i)
          N.(Storage_sigs.NAME.name) (Option.map to_bytes v) in
      let '(s, _) := unpack c in
      return= (C.(Raw_context.T.project) s).
    
    Definition remove `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
      : M= Raw_context.root_context :=
      let= c :=
        Raw_context.(Raw_context.T.remove) (_pack s i)
          N.(Storage_sigs.NAME.name) in
      let '(s, _) := unpack c in
      return= (C.(Raw_context.T.project) s).
    
    Definition delete `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
      : M=? Raw_context.root_context :=
      let=? c :=
        Raw_context.(Raw_context.T.delete) (_pack s i)
          N.(Storage_sigs.NAME.name) in
      let '(s, _) := unpack c in
      return=? (C.(Raw_context.T.project) s).
    
    Definition clear `{FArgs} (s : C.(Raw_context.T.t))
      : M= Raw_context.root_context :=
      let= t_value :=
        fold_keys s s
          (fun (i : I.(INDEX.t)) =>
            fun (s : C.(Raw_context.T.t)) =>
              let= c :=
                Raw_context.(Raw_context.T.remove) (_pack s i)
                  N.(Storage_sigs.NAME.name) in
              let '(s, _) := unpack c in
              return= s) in
      return= (C.(Raw_context.T.project) t_value).
    
    Definition fold `{FArgs} {A : Set}
      (s : C.(Raw_context.T.t)) (init_value : A)
      (f : I.(INDEX.t) -> V.(Storage_sigs.VALUE.t) -> A -> M= A) : M= A :=
      fold_keys s init_value
        (fun (i : I.(INDEX.t)) =>
          fun (acc_value : A) =>
            let= function_parameter := get s i in
            match function_parameter with
            | Pervasives.Error _ => Lwt._return acc_value
            | Pervasives.Ok v => f i v acc_value
            end).
    
    Definition bindings `{FArgs} (s : C.(Raw_context.T.t))
      : M= (list (I.(INDEX.t) * V.(Storage_sigs.VALUE.t))) :=
      fold s (nil (A := key * value))
        (fun (p_value : I.(INDEX.t)) =>
          fun (v : V.(Storage_sigs.VALUE.t)) =>
            fun (acc_value : list (I.(INDEX.t) * V.(Storage_sigs.VALUE.t))) =>
              Lwt._return (cons (p_value, v) acc_value)).
    
    Definition fold_keys `{FArgs} {A : Set}
      (s : C.(Raw_context.T.t)) (init_value : A) (f : I.(INDEX.t) -> A -> M= A)
      : M= A :=
      fold_keys s init_value
        (fun (i : I.(INDEX.t)) =>
          fun (acc_value : A) =>
            let= function_parameter := mem s i in
            match function_parameter with
            | false => Lwt._return acc_value
            | true => f i acc_value
            end).
    
    Definition keys `{FArgs} (s : C.(Raw_context.T.t))
      : M= (list I.(INDEX.t)) :=
      fold_keys s (nil (A := key))
        (fun (p_value : I.(INDEX.t)) =>
          fun (acc_value : list I.(INDEX.t)) =>
            Lwt._return (cons p_value acc_value)).
    
    (** Init function; without side-effects in Coq *)
    Definition init_module `{FArgs} : unit :=
      (Storage_description.register_value :
        Storage_description.t Raw_context.(Raw_context.T.t) ->
        (Raw_context.(Raw_context.T.t) -> M=? (option V.(Storage_sigs.VALUE.t)))
        -> Data_encoding.t V.(Storage_sigs.VALUE.t) -> unit)
        (Storage_description.register_named_subcontext
          Raw_context.(Raw_context.T.description) N.(Storage_sigs.NAME.name))
        (fun (c : Raw_context.(Raw_context.T.t)) =>
          let '(c, k) := Storage_description.unpack I.(INDEX.args) c in
          get_option c k) V.(Storage_sigs.VALUE.encoding).
    
    Definition functor `{FArgs} :=
      {|
        Storage_sigs.Indexed_data_storage.mem := mem;
        Storage_sigs.Indexed_data_storage.get := get;
        Storage_sigs.Indexed_data_storage.get_option := get_option;
        Storage_sigs.Indexed_data_storage.set := set;
        Storage_sigs.Indexed_data_storage.init_value := init_value;
        Storage_sigs.Indexed_data_storage.init_set := init_set;
        Storage_sigs.Indexed_data_storage.set_option := set_option;
        Storage_sigs.Indexed_data_storage.delete := delete;
        Storage_sigs.Indexed_data_storage.remove := remove;
        Storage_sigs.Indexed_data_storage.clear := clear;
        Storage_sigs.Indexed_data_storage.keys := keys;
        Storage_sigs.Indexed_data_storage.bindings := bindings;
        Storage_sigs.Indexed_data_storage.fold _ := fold;
        Storage_sigs.Indexed_data_storage.fold_keys _ := fold_keys
      |}.
  End Make_map.
  Definition Make_map `{FArgs} {V_t : Set}
    (N : Storage_sigs.NAME) (V : Storage_sigs.VALUE (t := V_t))
    : Storage_sigs.Indexed_data_storage (t := t) (key := key)
      (value := V.(Storage_sigs.VALUE.t)) :=
    let '_ := Make_map.Build_FArgs N V in
    Make_map.functor.
  
  Module Make_carbonated_map.
    Class FArgs `{FArgs} {V_t : Set} := {
      N : Storage_sigs.NAME;
      V : Storage_sigs.VALUE (t := V_t);
    }.
    Arguments Build_FArgs {_ _ _ _ _}.
    
    Definition t `{FArgs} : Set := C.(Raw_context.T.t).
    
    Definition context `{FArgs} : Set := t.
    
    Definition key `{FArgs} : Set := I.(INDEX.t).
    
    Definition value `{FArgs} : Set := V.(Storage_sigs.VALUE.t).
    
    Definition Make_encoder_include `{FArgs} := Make_encoder V.
    
    (** Inclusion of the module [Make_encoder_include] *)
    Definition of_bytes `{FArgs} := Make_encoder_include.(ENCODER.of_bytes).
    
    Definition to_bytes `{FArgs} := Make_encoder_include.(ENCODER.to_bytes).
    
    Definition len_name `{FArgs} : list string :=
      cons len_name N.(Storage_sigs.NAME.name).
    
    Definition data_name `{FArgs} : list string :=
      cons data_name N.(Storage_sigs.NAME.name).
    
    Definition path_length `{FArgs} : int :=
      (List.length N.(Storage_sigs.NAME.name)) +i 1.
    
    Definition consume_mem_gas `{FArgs} (c : Raw_context.(Raw_context.T.t))
      : M? Raw_context.(Raw_context.T.t) :=
      Raw_context.(Raw_context.T.consume_gas) c
        (Storage_costs.read_access path_length 0).
    
    Definition existing_size `{FArgs} (c : Raw_context.(Raw_context.T.t))
      : M=? (int * bool) :=
      let= function_parameter :=
        Raw_context.(Raw_context.T.get_option) c len_name in
      match function_parameter with
      | None => return=? (0, false)
      | Some len =>
        return=
          (let? len := decode_len_value len_name len in
          return? (len, true))
      end.
    
    Definition consume_read_gas `{FArgs}
      (get : Raw_context.(Raw_context.T.t) -> list string -> M=? bytes)
      (c : Raw_context.(Raw_context.T.t)) : M=? Raw_context.(Raw_context.T.t) :=
      let=? len := get c len_name in
      Lwt._return
        (let? read_bytes := decode_len_value len_name len in
        Raw_context.(Raw_context.T.consume_gas) c
          (Storage_costs.read_access path_length read_bytes)).
    
    Definition consume_write_gas `{FArgs} {A : Set}
      (set : Raw_context.(Raw_context.T.t) -> list string -> bytes -> M=? A)
      (c : Raw_context.(Raw_context.T.t)) (v : V.(Storage_sigs.VALUE.t))
      : M=? (A * bytes) :=
      let bytes_value := to_bytes v in
      let len := Bytes.length bytes_value in
      let=? c :=
        return=
          (Raw_context.(Raw_context.T.consume_gas) c
            (Storage_costs.write_access len)) in
      let=? c := set c len_name (encode_len_value bytes_value) in
      return=? (c, bytes_value).
    
    Definition consume_remove_gas `{FArgs} {A : Set}
      (del : Raw_context.(Raw_context.T.t) -> list string -> M=? A)
      (c : Raw_context.(Raw_context.T.t)) : M=? A :=
      let=? c :=
        return=
          (Raw_context.(Raw_context.T.consume_gas) c
            (Storage_costs.write_access 0)) in
      del c len_name.
    
    Definition mem `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
      : M=? (Raw_context.root_context * bool) :=
      let=? c := return= (consume_mem_gas (_pack s i)) in
      let= res := Raw_context.(Raw_context.T.mem) c data_name in
      return=? ((Raw_context.(Raw_context.T.project) c), res).
    
    Definition get `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
      : M=? (Raw_context.root_context * V.(Storage_sigs.VALUE.t)) :=
      let=? c := consume_read_gas Raw_context.(Raw_context.T.get) (_pack s i) in
      let=? b_value := Raw_context.(Raw_context.T.get) c data_name in
      let key_value := Raw_context.(Raw_context.T.absolute_key) c data_name in
      Lwt._return
        (let? v := of_bytes key_value b_value in
        return? ((Raw_context.(Raw_context.T.project) c), v)).
    
    Definition get_option `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
      : M=? (Raw_context.root_context * option V.(Storage_sigs.VALUE.t)) :=
      let=? c := return= (consume_mem_gas (_pack s i)) in
      let '(s, _) := unpack c in
      let= _exists := Raw_context.(Raw_context.T.mem) (_pack s i) data_name in
      if _exists then
        let=? '(s, v) := get s i in
        return=? (s, (Some v))
      else
        Error_monad._return ((C.(Raw_context.T.project) s), None).
    
    Definition set `{FArgs}
      (s : C.(Raw_context.T.t)) (i : I.(INDEX.t)) (v : V.(Storage_sigs.VALUE.t))
      : M=? (Raw_context.root_context * int) :=
      let=? '(prev_size, _) := existing_size (_pack s i) in
      let=? '(c, bytes_value) :=
        consume_write_gas Raw_context.(Raw_context.T.set) (_pack s i) v in
      let=? c := Raw_context.(Raw_context.T.set) c data_name bytes_value in
      let size_diff := (Bytes.length bytes_value) -i prev_size in
      return=? ((Raw_context.(Raw_context.T.project) c), size_diff).
    
    Definition init_value `{FArgs}
      (s : C.(Raw_context.T.t)) (i : I.(INDEX.t)) (v : V.(Storage_sigs.VALUE.t))
      : M=? (Raw_context.root_context * int) :=
      let=? '(c, bytes_value) :=
        consume_write_gas Raw_context.(Raw_context.T.init_value) (_pack s i) v
        in
      let=? c := Raw_context.(Raw_context.T.init_value) c data_name bytes_value
        in
      let size := Bytes.length bytes_value in
      return=? ((Raw_context.(Raw_context.T.project) c), size).
    
    Definition init_set `{FArgs}
      (s : C.(Raw_context.T.t)) (i : I.(INDEX.t)) (v : V.(Storage_sigs.VALUE.t))
      : M=? (Raw_context.root_context * int * bool) :=
      let init_set {A : Set}
        (c : Raw_context.(Raw_context.T.t)) (k : Raw_context.key)
        (v : Raw_context.value)
        : M= (Pervasives.result Raw_context.(Raw_context.T.t) A) :=
        Error_monad.op_gtpipeeq (Raw_context.(Raw_context.T.init_set) c k v)
          Error_monad.ok in
      let=? '(prev_size, existed) := existing_size (_pack s i) in
      let=? '(c, bytes_value) := consume_write_gas init_set (_pack s i) v in
      let=? c := init_set c data_name bytes_value in
      let size_diff := (Bytes.length bytes_value) -i prev_size in
      return=? ((Raw_context.(Raw_context.T.project) c), size_diff, existed).
    
    Definition remove `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
      : M=? (Raw_context.root_context * int * bool) :=
      let remove {A : Set}
        (c : Raw_context.(Raw_context.T.t)) (k : Raw_context.key)
        : M= (Pervasives.result Raw_context.(Raw_context.T.t) A) :=
        Error_monad.op_gtpipeeq (Raw_context.(Raw_context.T.remove) c k)
          Error_monad.ok in
      let=? '(prev_size, existed) := existing_size (_pack s i) in
      let=? c := consume_remove_gas remove (_pack s i) in
      let=? c := remove c data_name in
      return=? ((Raw_context.(Raw_context.T.project) c), prev_size, existed).
    
    Definition delete `{FArgs} (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
      : M=? (Raw_context.root_context * int) :=
      let=? '(prev_size, _) := existing_size (_pack s i) in
      let=? c :=
        consume_remove_gas Raw_context.(Raw_context.T.delete) (_pack s i) in
      let=? c := Raw_context.(Raw_context.T.delete) c data_name in
      return=? ((Raw_context.(Raw_context.T.project) c), prev_size).
    
    Definition set_option `{FArgs}
      (s : C.(Raw_context.T.t)) (i : I.(INDEX.t))
      (v : option V.(Storage_sigs.VALUE.t))
      : M=? (Raw_context.root_context * int * bool) :=
      match v with
      | None => remove s i
      | Some v => init_set s i v
      end.
    
    (** Init function; without side-effects in Coq *)
    Definition init_module `{FArgs} : unit :=
      (Storage_description.register_value :
        Storage_description.t Raw_context.(Raw_context.T.t) ->
        (Raw_context.(Raw_context.T.t) -> M=? (option V.(Storage_sigs.VALUE.t)))
        -> Data_encoding.t V.(Storage_sigs.VALUE.t) -> unit)
        (Storage_description.register_named_subcontext
          Raw_context.(Raw_context.T.description) N.(Storage_sigs.NAME.name))
        (fun (c : Raw_context.(Raw_context.T.t)) =>
          let '(c, k) := Storage_description.unpack I.(INDEX.args) c in
          let=? '(_, v) := get_option c k in
          return=? v) V.(Storage_sigs.VALUE.encoding).
    
    Definition functor `{FArgs} :=
      {|
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.mem := mem;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.get := get;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.get_option :=
          get_option;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.set := set;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.init_value :=
          init_value;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.init_set :=
          init_set;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.set_option :=
          set_option;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.delete :=
          delete;
        Storage_sigs.Non_iterable_indexed_carbonated_data_storage.remove :=
          remove
      |}.
  End Make_carbonated_map.
  Definition Make_carbonated_map `{FArgs} {V_t : Set}
    (N : Storage_sigs.NAME) (V : Storage_sigs.VALUE (t := V_t))
    : Storage_sigs.Non_iterable_indexed_carbonated_data_storage (t := t)
      (key := key) (value := V.(Storage_sigs.VALUE.t)) :=
    let '_ := Make_carbonated_map.Build_FArgs N V in
    Make_carbonated_map.functor.
  
  Definition functor `{FArgs} :=
    {|
      Storage_sigs.Indexed_raw_context.clear := clear;
      Storage_sigs.Indexed_raw_context.fold_keys _ := fold_keys;
      Storage_sigs.Indexed_raw_context.keys := keys;
      Storage_sigs.Indexed_raw_context.resolve := resolve;
      Storage_sigs.Indexed_raw_context.remove_rec := remove_rec;
      Storage_sigs.Indexed_raw_context.copy := copy;
      Storage_sigs.Indexed_raw_context.Make_set := Make_set;
      Storage_sigs.Indexed_raw_context.Make_map _ := Make_map;
      Storage_sigs.Indexed_raw_context.Make_carbonated_map _ :=
        Make_carbonated_map;
      Storage_sigs.Indexed_raw_context.Raw_context := Raw_context
    |}.
End Make_indexed_subcontext.
Definition Make_indexed_subcontext {C_t I_t : Set} {I_ipath : Set -> Set}
  (C : Raw_context.T (t := C_t)) (I : INDEX (t := I_t) (ipath := I_ipath))
  : Storage_sigs.Indexed_raw_context (t := C.(Raw_context.T.t))
    (key := I.(INDEX.t)) (ipath := fun (a : Set) => I.(INDEX.ipath) a) :=
  let '_ := Make_indexed_subcontext.Build_FArgs C I in
  Make_indexed_subcontext.functor.

Module WRAPPER.
  Record signature {t key : Set} : Set := {
    t := t;
    key := key;
    wrap : t -> key;
    unwrap : key -> option t;
  }.
End WRAPPER.
Definition WRAPPER := @WRAPPER.signature.
Arguments WRAPPER {_ _}.

Module Wrap_indexed_data_storage.
  Class FArgs {C_t C_key C_value K_t : Set} := {
    C :
      Storage_sigs.Indexed_data_storage (t := C_t) (key := C_key)
        (value := C_value);
    K : WRAPPER (t := K_t) (key := C.(Storage_sigs.Indexed_data_storage.key));
  }.
  Arguments Build_FArgs {_ _ _ _}.
  
  Definition t `{FArgs} : Set := C.(Storage_sigs.Indexed_data_storage.t).
  
  Definition context `{FArgs} : Set := C.(Storage_sigs.Indexed_data_storage.t).
  
  Definition key `{FArgs} : Set := K.(WRAPPER.t).
  
  Definition value `{FArgs} : Set :=
    C.(Storage_sigs.Indexed_data_storage.value).
  
  Definition mem `{FArgs}
    (ctxt : C.(Storage_sigs.Indexed_data_storage.t)) (k : K.(WRAPPER.t))
    : M= bool :=
    C.(Storage_sigs.Indexed_data_storage.mem) ctxt (K.(WRAPPER.wrap) k).
  
  Definition get `{FArgs}
    (ctxt : C.(Storage_sigs.Indexed_data_storage.t)) (k : K.(WRAPPER.t))
    : M=? C.(Storage_sigs.Indexed_data_storage.value) :=
    C.(Storage_sigs.Indexed_data_storage.get) ctxt (K.(WRAPPER.wrap) k).
  
  Definition get_option `{FArgs}
    (ctxt : C.(Storage_sigs.Indexed_data_storage.t)) (k : K.(WRAPPER.t))
    : M=? (option C.(Storage_sigs.Indexed_data_storage.value)) :=
    C.(Storage_sigs.Indexed_data_storage.get_option) ctxt (K.(WRAPPER.wrap) k).
  
  Definition set `{FArgs}
    (ctxt : C.(Storage_sigs.Indexed_data_storage.t)) (k : K.(WRAPPER.t))
    (v : C.(Storage_sigs.Indexed_data_storage.value)) : M=? Raw_context.t :=
    C.(Storage_sigs.Indexed_data_storage.set) ctxt (K.(WRAPPER.wrap) k) v.
  
  Definition init_value `{FArgs}
    (ctxt : C.(Storage_sigs.Indexed_data_storage.t)) (k : K.(WRAPPER.t))
    (v : C.(Storage_sigs.Indexed_data_storage.value)) : M=? Raw_context.t :=
    C.(Storage_sigs.Indexed_data_storage.init_value) ctxt (K.(WRAPPER.wrap) k) v.
  
  Definition init_set `{FArgs}
    (ctxt : C.(Storage_sigs.Indexed_data_storage.t)) (k : K.(WRAPPER.t))
    (v : C.(Storage_sigs.Indexed_data_storage.value)) : M= Raw_context.t :=
    C.(Storage_sigs.Indexed_data_storage.init_set) ctxt (K.(WRAPPER.wrap) k) v.
  
  Definition set_option `{FArgs}
    (ctxt : C.(Storage_sigs.Indexed_data_storage.t)) (k : K.(WRAPPER.t))
    (v : option C.(Storage_sigs.Indexed_data_storage.value))
    : M= Raw_context.t :=
    C.(Storage_sigs.Indexed_data_storage.set_option) ctxt (K.(WRAPPER.wrap) k) v.
  
  Definition delete `{FArgs}
    (ctxt : C.(Storage_sigs.Indexed_data_storage.t)) (k : K.(WRAPPER.t))
    : M=? Raw_context.t :=
    C.(Storage_sigs.Indexed_data_storage.delete) ctxt (K.(WRAPPER.wrap) k).
  
  Definition remove `{FArgs}
    (ctxt : C.(Storage_sigs.Indexed_data_storage.t)) (k : K.(WRAPPER.t))
    : M= Raw_context.t :=
    C.(Storage_sigs.Indexed_data_storage.remove) ctxt (K.(WRAPPER.wrap) k).
  
  Definition clear `{FArgs} (ctxt : C.(Storage_sigs.Indexed_data_storage.t))
    : M= Raw_context.t := C.(Storage_sigs.Indexed_data_storage.clear) ctxt.
  
  Definition fold `{FArgs} {A : Set}
    (ctxt : C.(Storage_sigs.Indexed_data_storage.t)) (init_value : A)
    (f :
      K.(WRAPPER.t) -> C.(Storage_sigs.Indexed_data_storage.value) -> A -> M= A)
    : M= A :=
    C.(Storage_sigs.Indexed_data_storage.fold) ctxt init_value
      (fun (k : C.(Storage_sigs.Indexed_data_storage.key)) =>
        fun (v : C.(Storage_sigs.Indexed_data_storage.value)) =>
          fun (acc_value : A) =>
            match K.(WRAPPER.unwrap) k with
            | None => Lwt._return acc_value
            | Some k => f k v acc_value
            end).
  
  Definition bindings `{FArgs} (s : C.(Storage_sigs.Indexed_data_storage.t))
    : M= (list (K.(WRAPPER.t) * C.(Storage_sigs.Indexed_data_storage.value))) :=
    fold s (nil (A := key * value))
      (fun (p_value : K.(WRAPPER.t)) =>
        fun (v : C.(Storage_sigs.Indexed_data_storage.value)) =>
          fun (acc_value :
            list (K.(WRAPPER.t) * C.(Storage_sigs.Indexed_data_storage.value)))
            => Lwt._return (cons (p_value, v) acc_value)).
  
  Definition fold_keys `{FArgs} {A : Set}
    (s : C.(Storage_sigs.Indexed_data_storage.t)) (init_value : A)
    (f : K.(WRAPPER.t) -> A -> M= A) : M= A :=
    C.(Storage_sigs.Indexed_data_storage.fold_keys) s init_value
      (fun (k : C.(Storage_sigs.Indexed_data_storage.key)) =>
        fun (acc_value : A) =>
          match K.(WRAPPER.unwrap) k with
          | None => Lwt._return acc_value
          | Some k => f k acc_value
          end).
  
  Definition keys `{FArgs} (s : C.(Storage_sigs.Indexed_data_storage.t))
    : M= (list K.(WRAPPER.t)) :=
    fold_keys s (nil (A := key))
      (fun (p_value : K.(WRAPPER.t)) =>
        fun (acc_value : list K.(WRAPPER.t)) =>
          Lwt._return (cons p_value acc_value)).
  
  Definition functor `{FArgs} :=
    {|
      Storage_sigs.Indexed_data_storage.mem := mem;
      Storage_sigs.Indexed_data_storage.get := get;
      Storage_sigs.Indexed_data_storage.get_option := get_option;
      Storage_sigs.Indexed_data_storage.set := set;
      Storage_sigs.Indexed_data_storage.init_value := init_value;
      Storage_sigs.Indexed_data_storage.init_set := init_set;
      Storage_sigs.Indexed_data_storage.set_option := set_option;
      Storage_sigs.Indexed_data_storage.delete := delete;
      Storage_sigs.Indexed_data_storage.remove := remove;
      Storage_sigs.Indexed_data_storage.clear := clear;
      Storage_sigs.Indexed_data_storage.keys := keys;
      Storage_sigs.Indexed_data_storage.bindings := bindings;
      Storage_sigs.Indexed_data_storage.fold _ := fold;
      Storage_sigs.Indexed_data_storage.fold_keys _ := fold_keys
    |}.
End Wrap_indexed_data_storage.
Definition Wrap_indexed_data_storage {C_t C_key C_value K_t : Set}
  (C :
    Storage_sigs.Indexed_data_storage (t := C_t) (key := C_key)
      (value := C_value))
  (K : WRAPPER (t := K_t) (key := C.(Storage_sigs.Indexed_data_storage.key)))
  : Storage_sigs.Indexed_data_storage
    (t := C.(Storage_sigs.Indexed_data_storage.t)) (key := K.(WRAPPER.t))
    (value := C.(Storage_sigs.Indexed_data_storage.value)) :=
  let '_ := Wrap_indexed_data_storage.Build_FArgs C K in
  Wrap_indexed_data_storage.functor.
