Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Alpha_context.
Require TezosOfOCaml.Proto_2021_01.Script_typed_ir.
Require TezosOfOCaml.Proto_2021_01.Storage_costs.

Module Cost_of.
  Module Z_syntax.
    Definition log2 (x : Z.t) : Z.t := Z.of_int (1 +i (Z.numbits x)).
    
    Definition op_plus : Z.t -> Z.t -> Z.t := Z.add.
    
    Definition op_star : Z.t -> Z.t -> Z.t := Z.mul.
    
    Definition lsr : Z.t -> int -> Z.t := Z.shift_right.
  End Z_syntax.
  
  Definition z_bytes (z : Z.t) : int :=
    let bits := Z.numbits z in
    (7 +i bits) /i 8.
  
  Definition int_bytes (z : Alpha_context.Script_int.num) : int :=
    z_bytes (Alpha_context.Script_int.to_zint z).
  
  Definition timestamp_bytes (t_value : Alpha_context.Script_timestamp.t)
    : int :=
    let z := Alpha_context.Script_timestamp.to_zint t_value in
    z_bytes z.
  
  Fixpoint size_of_comparable {a : Set}
    (wit : Script_typed_ir.comparable_ty) (v : a) {struct wit} : Z.t :=
    match (wit, v) with
    | (Script_typed_ir.Unit_key, _) => Z.of_int 1
    
    | (Script_typed_ir.Int_key, v) =>
      let v := cast Alpha_context.Script_int.num v in
      Z.of_int (int_bytes v)
    
    | (Script_typed_ir.Nat_key, v) =>
      let v := cast Alpha_context.Script_int.num v in
      Z.of_int (int_bytes v)
    
    | (Script_typed_ir.Signature_key, _) => Z.of_int Signature.size
    
    | (Script_typed_ir.String_key, v) =>
      let v := cast string v in
      Z.of_int (String.length v)
    
    | (Script_typed_ir.Bytes_key, v) =>
      let v := cast bytes v in
      Z.of_int (Bytes.length v)
    
    | (Script_typed_ir.Bool_key, _) => Z.of_int 8
    
    | (Script_typed_ir.Key_hash_key, _) =>
      Z.of_int Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.size)
    
    | (Script_typed_ir.Key_key, k) =>
      let k := cast Alpha_context.public_key k in
      Z.of_int (Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.size) k)
    
    | (Script_typed_ir.Timestamp_key, v) =>
      let v := cast Alpha_context.Script_timestamp.t v in
      Z.of_int (timestamp_bytes v)
    
    | (Script_typed_ir.Address_key, _) =>
      Z.of_int Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.size)
    
    | (Script_typed_ir.Mutez_key, _) => Z.of_int 8
    
    | (Script_typed_ir.Chain_id_key, _) => Z.of_int Chain_id.size
    
    | (Script_typed_ir.Pair_key l_value r_value, x) =>
      let 'existT _ [__0, __1] [x, r_value, l_value] :=
        cast_exists (Es := [Set ** Set])
          (fun '[__0, __1] =>
            [__0 * __1 ** Script_typed_ir.comparable_ty **
              Script_typed_ir.comparable_ty]) [x, r_value, l_value] in
      let '(lval, rval) := x in
      (size_of_comparable l_value lval) +Z (size_of_comparable r_value rval)
    
    | (Script_typed_ir.Union_key t_l t_r, x) =>
      let 'existT _ [__2, __3] [x, t_r, t_l] :=
        cast_exists (Es := [Set ** Set])
          (fun '[__2, __3] =>
            [Script_typed_ir.union __2 __3 ** Script_typed_ir.comparable_ty **
              Script_typed_ir.comparable_ty]) [x, t_r, t_l] in
      match x with
      | Script_typed_ir.L x => (Z.of_int 1) +Z (size_of_comparable t_l x)
      | Script_typed_ir.R x => (Z.of_int 1) +Z (size_of_comparable t_r x)
      end
    
    | (Script_typed_ir.Option_key t_value, x) =>
      let 'existT _ __4 [x, t_value] :=
        cast_exists (Es := Set)
          (fun __4 => [option __4 ** Script_typed_ir.comparable_ty])
          [x, t_value] in
      match x with
      | None => Z.of_int 1
      | Some x => (Z.of_int 1) +Z (size_of_comparable t_value x)
      end
    | _ => unreachable_gadt_branch
    end.
  
  Definition manager_operation : Alpha_context.Gas.cost :=
    Alpha_context.Gas.step_cost (Z.of_int 1000).
  
  Definition public_key_size : int := 64.
  
  Module Generated_costs_007.
    Definition cost_N_Abs_int (size : int) : Z.t :=
      Z.of_int (80 +i (Pervasives.lsr size 4)).
    
    Definition cost_N_Add_bls12_381_fr : Z.t := Z.of_int 230.
    
    Definition cost_N_Add_bls12_381_g1 : Z.t := Z.of_int 9300.
    
    Definition cost_N_Add_bls12_381_g2 : Z.t := Z.of_int 13000.
    
    Definition cost_N_Add_intint (size1 : int) (size2 : int) : Z.t :=
      let v0 := Compare.Int.(Compare.S.max) size1 size2 in
      Z.of_int (80 +i ((Pervasives.lsr v0 4) +i (Pervasives.lsr v0 6))).
    
    Definition cost_N_Add_tez : Z.t := Z.of_int 100.
    
    Definition cost_N_And : Z.t := Z.of_int 100.
    
    Definition cost_N_And_nat (size1 : int) (size2 : int) : Z.t :=
      let v0 := Compare.Int.(Compare.S.min) size1 size2 in
      Z.of_int (80 +i ((Pervasives.lsr v0 4) +i (Pervasives.lsr v0 6))).
    
    Definition cost_N_Blake2b (size : int) : Z.t :=
      let size := Z.of_int size in
      (Z.of_int 500) +Z (size +Z (Z_syntax.lsr size 2)).
    
    Definition cost_N_Car : Z.t := Z.of_int 80.
    
    Definition cost_N_Cdr : Z.t := Z.of_int 80.
    
    Definition cost_N_Check_signature_ed25519 (size : int) : Z.t :=
      let size := Z.of_int size in
      (Z.of_int 270000) +Z (size +Z (Z_syntax.lsr size 2)).
    
    Definition cost_N_Check_signature_p256 (size : int) : Z.t :=
      let size := Z.of_int size in
      (Z.of_int 600000) +Z
      ((size +Z (Z_syntax.lsr size 2)) +Z (Z_syntax.lsr size 3)).
    
    Definition cost_N_Check_signature_secp256k1 (size : int) : Z.t :=
      let size := Z.of_int size in
      (Z.of_int 60000) +Z (size +Z (Z_syntax.lsr size 2)).
    
    Definition cost_N_Comb (size : int) : Z.t :=
      Z.of_int (80 +i ((3 *i size) +i (Pervasives.lsr size 2))).
    
    Definition cost_N_Comb_get (size : int) : Z.t :=
      Z.of_int (80 +i ((Pervasives.lsr size 1) +i (Pervasives.lsr size 4))).
    
    Definition cost_N_Comb_set (size : int) : Z.t :=
      Z.of_int (80 +i (size +i (Pervasives.lsr size 2))).
    
    Definition cost_N_Compare_address (size1 : int) (size2 : int) : Z.t :=
      Z.of_int (80 +i (2 *i (Compare.Int.(Compare.S.min) size1 size2))).
    
    Definition cost_N_Compare_bool (size1 : int) (size2 : int) : Z.t :=
      Z.of_int (80 +i (128 *i (Compare.Int.(Compare.S.min) size1 size2))).
    
    Definition cost_N_Compare_int (size1 : int) (size2 : int) : Z.t :=
      let v0 := Compare.Int.(Compare.S.min) size1 size2 in
      Z.of_int (150 +i ((Pervasives.lsr v0 4) +i (Pervasives.lsr v0 7))).
    
    Definition cost_N_Compare_key_hash (size1 : int) (size2 : int) : Z.t :=
      Z.of_int (80 +i (2 *i (Compare.Int.(Compare.S.min) size1 size2))).
    
    Definition cost_N_Compare_mutez (size1 : int) (size2 : int) : Z.t :=
      Z.of_int (13 *i (Compare.Int.(Compare.S.min) size1 size2)).
    
    Definition cost_N_Compare_string (size1 : int) (size2 : int) : Z.t :=
      let v0 := Compare.Int.(Compare.S.min) size1 size2 in
      Z.of_int (120 +i ((Pervasives.lsr v0 5) +i (Pervasives.lsr v0 7))).
    
    Definition cost_N_Compare_timestamp (size1 : int) (size2 : int) : Z.t :=
      let v0 := Compare.Int.(Compare.S.min) size1 size2 in
      Z.of_int (140 +i ((Pervasives.lsr v0 4) +i (Pervasives.lsr v0 7))).
    
    Definition cost_N_Concat_string_pair (size1 : int) (size2 : int) : Z.t :=
      let v0 := (Z.of_int size1) +Z (Z.of_int size2) in
      (Z.of_int 80) +Z (Z_syntax.lsr v0 4).
    
    Definition cost_N_Cons_list : Z.t := Z.of_int 80.
    
    Definition cost_N_Cons_none : Z.t := Z.of_int 80.
    
    Definition cost_N_Cons_pair : Z.t := Z.of_int 80.
    
    Definition cost_N_Cons_some : Z.t := Z.of_int 80.
    
    Definition cost_N_Const : Z.t := Z.of_int 80.
    
    Definition cost_N_Dig (size : int) : Z.t := Z.of_int (100 +i (4 *i size)).
    
    Definition cost_N_Dip : Z.t := Z.of_int 100.
    
    Definition cost_N_DipN (size : int) : Z.t := Z.of_int (100 +i (4 *i size)).
    
    Definition cost_N_Drop : Z.t := Z.of_int 80.
    
    Definition cost_N_DropN (size : int) : Z.t := Z.of_int (100 +i (4 *i size)).
    
    Definition cost_N_Dug (size : int) : Z.t := Z.of_int (100 +i (4 *i size)).
    
    Definition cost_N_Dup : Z.t := Z.of_int 80.
    
    Definition cost_N_DupN (size : int) : Z.t :=
      Z.of_int ((60 +i size) +i (Pervasives.lsr size 2)).
    
    Definition cost_N_Ediv_natnat (size1 : int) (size2 : int) : Z.t :=
      let q := size1 -i size2 in
      if q <i 0 then
        Z.of_int 300
      else
        let v0 := (Z.of_int q) *Z (Z.of_int size2) in
        (((Z.of_int 300) +Z (Z_syntax.lsr v0 10)) +Z (Z_syntax.lsr v0 11)) +Z
        (Z_syntax.lsr v0 13).
    
    Definition cost_N_Ediv_tez : Z.t := Z.of_int 200.
    
    Definition cost_N_Ediv_teznat : Z.t := Z.of_int 300.
    
    Definition cost_N_Empty_map : Z.t := Z.of_int 240.
    
    Definition cost_N_Empty_set : Z.t := Z.of_int 240.
    
    Definition cost_N_Eq : Z.t := Z.of_int 80.
    
    Definition cost_N_If : Z.t := Z.of_int 60.
    
    Definition cost_N_If_cons : Z.t := Z.of_int 110.
    
    Definition cost_N_If_left : Z.t := Z.of_int 90.
    
    Definition cost_N_If_none : Z.t := Z.of_int 80.
    
    Definition cost_N_Int_nat : Z.t := Z.of_int 80.
    
    Definition cost_N_Is_nat : Z.t := Z.of_int 80.
    
    Definition cost_N_Keccak (size : int) : Z.t :=
      (Z.of_int 1400) +Z ((Z.of_int 30) *Z (Z.of_int size)).
    
    Definition cost_N_Left : Z.t := Z.of_int 80.
    
    Definition cost_N_List_iter (size : int) : Z.t :=
      (Z.of_int 500) +Z ((Z.of_int 7) *Z (Z.of_int size)).
    
    Definition cost_N_List_map (size : int) : Z.t :=
      (Z.of_int 500) +Z ((Z.of_int 12) *Z (Z.of_int size)).
    
    Definition cost_N_List_size : Z.t := Z.of_int 80.
    
    Definition cost_N_Loop : Z.t := Z.of_int 70.
    
    Definition cost_N_Loop_left : Z.t := Z.of_int 80.
    
    Definition cost_N_Lsl_nat (size : int) : Z.t :=
      Z.of_int (150 +i (Pervasives.lsr size 3)).
    
    Definition cost_N_Lsr_nat (size : int) : Z.t :=
      Z.of_int (150 +i (Pervasives.lsr size 3)).
    
    Definition cost_N_Map_get (size1 : Z.t) (size2 : int) : Z.t :=
      let v0 := size1 *Z (Z_syntax.log2 (Z.of_int size2)) in
      (((Z.of_int 80) +Z (Z_syntax.lsr v0 5)) +Z (Z_syntax.lsr v0 6)) +Z
      (Z_syntax.lsr v0 7).
    
    Definition cost_N_Map_iter (size : int) : Z.t :=
      (Z.of_int 80) +Z ((Z.of_int 40) *Z (Z.of_int size)).
    
    Definition cost_N_Map_map (size : int) : Z.t :=
      (Z.of_int 80) +Z ((Z.of_int 761) *Z (Z.of_int size)).
    
    Definition cost_N_Map_mem (size1 : Z.t) (size2 : int) : Z.t :=
      let v0 := size1 *Z (Z_syntax.log2 (Z.of_int size2)) in
      (((Z.of_int 80) +Z (Z_syntax.lsr v0 5)) +Z (Z_syntax.lsr v0 6)) +Z
      (Z_syntax.lsr v0 7).
    
    Definition cost_N_Map_size : Z.t := Z.of_int 90.
    
    Definition cost_N_Map_update (size1 : Z.t) (size2 : int) : Z.t :=
      let v0 := size1 *Z (Z_syntax.log2 (Z.of_int size2)) in
      ((((Z.of_int 80) +Z (Z_syntax.lsr v0 4)) +Z (Z_syntax.lsr v0 5)) +Z
      (Z_syntax.lsr v0 6)) +Z (Z_syntax.lsr v0 7).
    
    Definition cost_N_Mul_bls12_381_fr : Z.t := Z.of_int 260.
    
    Definition cost_N_Mul_bls12_381_g1 : Z.t := Z.of_int 265000.
    
    Definition cost_N_Mul_bls12_381_g2 : Z.t := Z.of_int 850000.
    
    Definition cost_bls12_381_fr_of_z : Z.t := Z.of_int 130.
    
    Definition cost_bls12_381_fr_to_z : Z.t := Z.of_int 30.
    
    Definition cost_N_Mul_bls12_381_fr_z : Z.t :=
      cost_bls12_381_fr_of_z +Z cost_N_Mul_bls12_381_fr.
    
    Definition cost_N_Int_bls12_381_fr : Z.t := cost_bls12_381_fr_to_z.
    
    Definition cost_N_Mul_intint (size1 : int) (size2 : int) : Z.t :=
      let a_value := (Z.of_int size1) +Z (Z.of_int size2) in
      (Z.of_int 80) +Z (a_value *Z (Z_syntax.log2 a_value)).
    
    Definition cost_N_Mul_teznat (size : int) : Z.t :=
      (Z.of_int 200) +Z ((Z.of_int 133) *Z (Z.of_int size)).
    
    Definition cost_N_Neg_bls12_381_fr : Z.t := Z.of_int 180.
    
    Definition cost_N_Neg_bls12_381_g1 : Z.t := Z.of_int 410.
    
    Definition cost_N_Neg_bls12_381_g2 : Z.t := Z.of_int 715.
    
    Definition cost_N_Neg_int (size : int) : Z.t :=
      Z.of_int (80 +i (Pervasives.lsr size 4)).
    
    Definition cost_N_Neq : Z.t := Z.of_int 80.
    
    Definition cost_N_Nil : Z.t := Z.of_int 80.
    
    Definition cost_N_Nop : Z.t := Z.of_int 70.
    
    Definition cost_N_Not : Z.t := Z.of_int 90.
    
    Definition cost_N_Not_int (size : int) : Z.t :=
      Z.of_int (55 +i ((Pervasives.lsr size 4) +i (Pervasives.lsr size 7))).
    
    Definition cost_N_Or : Z.t := Z.of_int 90.
    
    Definition cost_N_Or_nat (size1 : int) (size2 : int) : Z.t :=
      let v0 := Compare.Int.(Compare.S.max) size1 size2 in
      Z.of_int (80 +i ((Pervasives.lsr v0 4) +i (Pervasives.lsr v0 6))).
    
    Definition cost_N_Pairing_check_bls12_381 (size : int) : Z.t :=
      (Z.of_int 1550000) +Z ((Z.of_int 510000) *Z (Z.of_int size)).
    
    Definition cost_N_Right : Z.t := Z.of_int 80.
    
    Definition cost_N_Seq : Z.t := Z.of_int 60.
    
    Definition cost_N_Set_iter (size : int) : Z.t :=
      (Z.of_int 80) +Z ((Z.of_int 36) *Z (Z.of_int size)).
    
    Definition cost_N_Set_mem (size1 : Z.t) (size2 : int) : Z.t :=
      let v0 := size1 *Z (Z_syntax.log2 (Z.of_int size2)) in
      ((((Z.of_int 80) +Z (Z_syntax.lsr v0 5)) +Z (Z_syntax.lsr v0 6)) +Z
      (Z_syntax.lsr v0 7)) +Z (Z_syntax.lsr v0 8).
    
    Definition cost_N_Set_size : Z.t := Z.of_int 80.
    
    Definition cost_N_Set_update (size1 : Z.t) (size2 : int) : Z.t :=
      let v0 := size1 *Z (Z_syntax.log2 (Z.of_int size2)) in
      (Z.of_int 80) +Z (Z_syntax.lsr v0 3).
    
    Definition cost_N_Sha256 (size : int) : Z.t :=
      (Z.of_int 500) +Z ((Z.of_int 5) *Z (Z.of_int size)).
    
    Definition cost_N_Sha3 (size : int) : Z.t :=
      (Z.of_int 1400) +Z ((Z.of_int 32) *Z (Z.of_int size)).
    
    Definition cost_N_Sha512 (size : int) : Z.t :=
      (Z.of_int 500) +Z ((Z.of_int 3) *Z (Z.of_int size)).
    
    Definition cost_N_Slice_string (size : int) : Z.t :=
      Z.of_int (80 +i (Pervasives.lsr size 4)).
    
    Definition cost_N_String_size : Z.t := Z.of_int 80.
    
    Definition cost_N_Sub_int (size1 : int) (size2 : int) : Z.t :=
      let v0 := Compare.Int.(Compare.S.max) size1 size2 in
      Z.of_int (80 +i ((Pervasives.lsr v0 4) +i (Pervasives.lsr v0 6))).
    
    Definition cost_N_Sub_tez : Z.t := Z.of_int 80.
    
    Definition cost_N_Swap : Z.t := Z.of_int 70.
    
    Definition cost_N_Total_voting_power : Z.t := Z.of_int 400.
    
    Definition cost_N_Uncomb (size : int) : Z.t :=
      Z.of_int
        (80 +i
        (((3 *i size) +i (Pervasives.lsr size 1)) +i (Pervasives.lsr size 3))).
    
    Definition cost_N_Unpair : Z.t := Z.of_int 80.
    
    Definition cost_N_Voting_power : Z.t := Z.of_int 400.
    
    Definition cost_N_Xor : Z.t := Z.of_int 100.
    
    Definition cost_N_Xor_nat (size1 : int) (size2 : int) : Z.t :=
      let v0 := Compare.Int.(Compare.S.max) size1 size2 in
      Z.of_int (80 +i ((Pervasives.lsr v0 4) +i (Pervasives.lsr v0 6))).
    
    Definition cost_DECODING_BLS_FR : Z.t := Z.of_int 50.
    
    Definition cost_DECODING_BLS_G1 : Z.t := Z.of_int 230000.
    
    Definition cost_DECODING_BLS_G2 : Z.t := Z.of_int 740000.
    
    Definition cost_B58CHECK_DECODING_CHAIN_ID : Z.t := Z.of_int 1500.
    
    Definition cost_B58CHECK_DECODING_PUBLIC_KEY_HASH_ed25519 : Z.t :=
      Z.of_int 3300.
    
    Definition cost_B58CHECK_DECODING_PUBLIC_KEY_HASH_p256 : Z.t :=
      Z.of_int 3300.
    
    Definition cost_B58CHECK_DECODING_PUBLIC_KEY_HASH_secp256k1 : Z.t :=
      Z.of_int 3300.
    
    Definition cost_B58CHECK_DECODING_PUBLIC_KEY_ed25519 : Z.t := Z.of_int 4300.
    
    Definition cost_B58CHECK_DECODING_PUBLIC_KEY_p256 : Z.t := Z.of_int 29000.
    
    Definition cost_B58CHECK_DECODING_PUBLIC_KEY_secp256k1 : Z.t :=
      Z.of_int 9400.
    
    Definition cost_B58CHECK_DECODING_SIGNATURE_ed25519 : Z.t := Z.of_int 6600.
    
    Definition cost_B58CHECK_DECODING_SIGNATURE_p256 : Z.t := Z.of_int 6600.
    
    Definition cost_B58CHECK_DECODING_SIGNATURE_secp256k1 : Z.t :=
      Z.of_int 6600.
    
    Definition cost_ENCODING_BLS_FR : Z.t := Z.of_int 30.
    
    Definition cost_ENCODING_BLS_G1 : Z.t := Z.of_int 30.
    
    Definition cost_ENCODING_BLS_G2 : Z.t := Z.of_int 30.
    
    Definition cost_B58CHECK_ENCODING_CHAIN_ID : Z.t := Z.of_int 1600.
    
    Definition cost_B58CHECK_ENCODING_PUBLIC_KEY_HASH_ed25519 : Z.t :=
      Z.of_int 3300.
    
    Definition cost_B58CHECK_ENCODING_PUBLIC_KEY_HASH_p256 : Z.t :=
      Z.of_int 3750.
    
    Definition cost_B58CHECK_ENCODING_PUBLIC_KEY_HASH_secp256k1 : Z.t :=
      Z.of_int 3300.
    
    Definition cost_B58CHECK_ENCODING_PUBLIC_KEY_ed25519 : Z.t := Z.of_int 4500.
    
    Definition cost_B58CHECK_ENCODING_PUBLIC_KEY_p256 : Z.t := Z.of_int 5300.
    
    Definition cost_B58CHECK_ENCODING_PUBLIC_KEY_secp256k1 : Z.t :=
      Z.of_int 5000.
    
    Definition cost_B58CHECK_ENCODING_SIGNATURE_ed25519 : Z.t := Z.of_int 8700.
    
    Definition cost_B58CHECK_ENCODING_SIGNATURE_p256 : Z.t := Z.of_int 8700.
    
    Definition cost_B58CHECK_ENCODING_SIGNATURE_secp256k1 : Z.t :=
      Z.of_int 8700.
    
    Definition cost_DECODING_CHAIN_ID : Z.t := Z.of_int 50.
    
    Definition cost_DECODING_PUBLIC_KEY_HASH_ed25519 : Z.t := Z.of_int 50.
    
    Definition cost_DECODING_PUBLIC_KEY_HASH_p256 : Z.t := Z.of_int 60.
    
    Definition cost_DECODING_PUBLIC_KEY_HASH_secp256k1 : Z.t := Z.of_int 60.
    
    Definition cost_DECODING_PUBLIC_KEY_ed25519 : Z.t := Z.of_int 60.
    
    Definition cost_DECODING_PUBLIC_KEY_p256 : Z.t := Z.of_int 25000.
    
    Definition cost_DECODING_PUBLIC_KEY_secp256k1 : Z.t := Z.of_int 5300.
    
    Definition cost_DECODING_SIGNATURE_ed25519 : Z.t := Z.of_int 30.
    
    Definition cost_DECODING_SIGNATURE_p256 : Z.t := Z.of_int 30.
    
    Definition cost_DECODING_SIGNATURE_secp256k1 : Z.t := Z.of_int 30.
    
    Definition cost_ENCODING_CHAIN_ID : Z.t := Z.of_int 50.
    
    Definition cost_ENCODING_PUBLIC_KEY_HASH_ed25519 : Z.t := Z.of_int 70.
    
    Definition cost_ENCODING_PUBLIC_KEY_HASH_p256 : Z.t := Z.of_int 80.
    
    Definition cost_ENCODING_PUBLIC_KEY_HASH_secp256k1 : Z.t := Z.of_int 70.
    
    Definition cost_ENCODING_PUBLIC_KEY_ed25519 : Z.t := Z.of_int 80.
    
    Definition cost_ENCODING_PUBLIC_KEY_p256 : Z.t := Z.of_int 450.
    
    Definition cost_ENCODING_PUBLIC_KEY_secp256k1 : Z.t := Z.of_int 490.
    
    Definition cost_ENCODING_SIGNATURE_ed25519 : Z.t := Z.of_int 40.
    
    Definition cost_ENCODING_SIGNATURE_p256 : Z.t := Z.of_int 40.
    
    Definition cost_ENCODING_SIGNATURE_secp256k1 : Z.t := Z.of_int 40.
    
    Definition cost_TIMESTAMP_READABLE_DECODING : Z.t := Z.of_int 130.
    
    Definition cost_TIMESTAMP_READABLE_ENCODING : Z.t := Z.of_int 900.
    
    Definition cost_CHECK_PRINTABLE (size : int) : Z.t :=
      (Z.of_int 14) +Z ((Z.of_int 10) *Z (Z.of_int size)).
    
    Definition cost_MERGE_TYPES : Z.t := Z.of_int 130.
    
    Definition cost_TYPECHECKING_CODE : Z.t := Z.of_int 375.
    
    Definition cost_UNPARSING_CODE : Z.t := Z.of_int 200.
    
    Definition cost_TYPECHECKING_DATA : Z.t := Z.of_int 240.
    
    Definition cost_UNPARSING_DATA : Z.t := Z.of_int 140.
    
    Definition cost_PARSE_TYPE : Z.t := Z.of_int 170.
    
    Definition cost_UNPARSE_TYPE : Z.t := Z.of_int 185.
    
    Definition cost_COMPARABLE_TY_OF_TY : Z.t := Z.of_int 120.
  End Generated_costs_007.
  
  Module Interpreter.
    Definition drop : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Drop.
    
    Definition dup : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Dup.
    
    Definition swap : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Swap.
    
    Definition push : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Const.
    
    Definition cons_some : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Cons_some.
    
    Definition cons_none : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Cons_none.
    
    Definition if_none : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_If_none.
    
    Definition cons_pair : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Cons_pair.
    
    Definition unpair : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Unpair.
    
    Definition car : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Car.
    
    Definition cdr : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Cdr.
    
    Definition cons_left : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Left.
    
    Definition cons_right : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Right.
    
    Definition if_left : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_If_left.
    
    Definition cons_list : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Cons_list.
    
    Definition nil : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Nil.
    
    Definition if_cons : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_If_cons.
    
    Definition list_map {a : Set}
      (function_parameter : Script_typed_ir.boxed_list a)
      : Alpha_context.Gas.cost :=
      let '{| Script_typed_ir.boxed_list.length := length |} :=
        function_parameter in
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_List_map length).
    
    Definition list_size : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_List_size.
    
    Definition list_iter {a : Set}
      (function_parameter : Script_typed_ir.boxed_list a)
      : Alpha_context.Gas.cost :=
      let '{| Script_typed_ir.boxed_list.length := length |} :=
        function_parameter in
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_List_iter length).
    
    Definition empty_set : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Empty_set.
    
    Definition set_iter {a : Set} (Box : Script_typed_ir.set a)
      : Alpha_context.Gas.cost :=
      let 'existS _ _ Box := Box in
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Set_iter
          Box.(Script_typed_ir.Boxed_set.size)).
    
    Definition set_mem {a : Set} (elt_value : a) (Box : Script_typed_ir.set a)
      : Alpha_context.Gas.cost :=
      let 'existS _ _ Box := Box in
      let elt_size :=
        size_of_comparable Box.(Script_typed_ir.Boxed_set.elt_ty) elt_value in
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Set_mem elt_size
          Box.(Script_typed_ir.Boxed_set.size)).
    
    Definition set_update {a : Set}
      (elt_value : a) (Box : Script_typed_ir.set a) : Alpha_context.Gas.cost :=
      let 'existS _ _ Box := Box in
      let elt_size :=
        size_of_comparable Box.(Script_typed_ir.Boxed_set.elt_ty) elt_value in
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Set_update elt_size
          Box.(Script_typed_ir.Boxed_set.size)).
    
    Definition set_size : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Set_size.
    
    Definition empty_map : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Empty_map.
    
    Definition map_map {k v : Set} (Box : Script_typed_ir.map k v)
      : Alpha_context.Gas.cost :=
      let 'existS _ _ Box := Box in
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Map_map
          (Pervasives.snd Box.(Script_typed_ir.Boxed_map.boxed))).
    
    Definition map_iter {k v : Set} (Box : Script_typed_ir.map k v)
      : Alpha_context.Gas.cost :=
      let 'existS _ _ Box := Box in
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Map_iter
          (Pervasives.snd Box.(Script_typed_ir.Boxed_map.boxed))).
    
    Definition map_mem {k v : Set}
      (elt_value : k) (Box : Script_typed_ir.map k v)
      : Alpha_context.Gas.cost :=
      let 'existS _ _ Box := Box in
      let elt_size :=
        size_of_comparable Box.(Script_typed_ir.Boxed_map.key_ty) elt_value in
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Map_mem elt_size
          (Pervasives.snd Box.(Script_typed_ir.Boxed_map.boxed))).
    
    Definition map_get {k v : Set}
      (elt_value : k) (Box : Script_typed_ir.map k v)
      : Alpha_context.Gas.cost :=
      let 'existS _ _ Box := Box in
      let elt_size :=
        size_of_comparable Box.(Script_typed_ir.Boxed_map.key_ty) elt_value in
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Map_get elt_size
          (Pervasives.snd Box.(Script_typed_ir.Boxed_map.boxed))).
    
    Definition map_update {k v : Set}
      (elt_value : k) (Box : Script_typed_ir.map k v)
      : Alpha_context.Gas.cost :=
      let 'existS _ _ Box := Box in
      let elt_size :=
        size_of_comparable Box.(Script_typed_ir.Boxed_map.key_ty) elt_value in
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Map_update elt_size
          (Pervasives.snd Box.(Script_typed_ir.Boxed_map.boxed))).
    
    Definition map_get_and_update {k v : Set}
      (elt_value : k) (m : Script_typed_ir.map k v) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.op_plusat (map_get elt_value m) (map_update elt_value m).
    
    Definition map_size : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Map_size.
    
    Definition add_seconds_timestamp
      (seconds : Alpha_context.Script_int.num)
      (timestamp : Alpha_context.Script_timestamp.t) : Alpha_context.Gas.cost :=
      let seconds_bytes := int_bytes seconds in
      let timestamp_bytes :=
        z_bytes (Alpha_context.Script_timestamp.to_zint timestamp) in
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Add_intint seconds_bytes timestamp_bytes).
    
    Definition sub_seconds_timestamp
      (seconds : Alpha_context.Script_int.num)
      (timestamp : Alpha_context.Script_timestamp.t) : Alpha_context.Gas.cost :=
      let seconds_bytes := int_bytes seconds in
      let timestamp_bytes :=
        z_bytes (Alpha_context.Script_timestamp.to_zint timestamp) in
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Sub_int seconds_bytes timestamp_bytes).
    
    Definition diff_timestamps
      (t1 : Alpha_context.Script_timestamp.t)
      (t2 : Alpha_context.Script_timestamp.t) : Alpha_context.Gas.cost :=
      let t1_bytes := z_bytes (Alpha_context.Script_timestamp.to_zint t1) in
      let t2_bytes := z_bytes (Alpha_context.Script_timestamp.to_zint t2) in
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Sub_int t1_bytes t2_bytes).
    
    Definition concat_string_pair (s1 : string) (s2 : string)
      : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Concat_string_pair (String.length s1)
          (String.length s2)).
    
    Definition slice_string (s : string) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Slice_string (String.length s)).
    
    Definition string_size : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_String_size.
    
    Definition concat_bytes_pair (b1 : bytes) (b2 : bytes)
      : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Concat_string_pair (Bytes.length b1)
          (Bytes.length b2)).
    
    Definition slice_bytes (b_value : bytes) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Slice_string (Bytes.length b_value)).
    
    Definition bytes_size : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_String_size.
    
    Definition add_tez : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Add_tez.
    
    Definition sub_tez : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Sub_tez.
    
    Definition mul_teznat (n : Alpha_context.Script_int.num)
      : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Mul_teznat (int_bytes n)).
    
    Definition bool_or : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Or.
    
    Definition bool_and : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_And.
    
    Definition bool_xor : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Xor.
    
    Definition bool_not : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Not.
    
    Definition is_nat : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Is_nat.
    
    Definition abs_int (i : Alpha_context.Script_int.num)
      : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Abs_int (int_bytes i)).
    
    Definition int_nat : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Int_nat.
    
    Definition neg_int (i : Alpha_context.Script_int.num)
      : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Neg_int (int_bytes i)).
    
    Definition neg_nat (n : Alpha_context.Script_int.num)
      : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Neg_int (int_bytes n)).
    
    Definition add_bigint
      (i1 : Alpha_context.Script_int.num) (i2 : Alpha_context.Script_int.num)
      : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Add_intint (int_bytes i1) (int_bytes i2)).
    
    Definition sub_bigint
      (i1 : Alpha_context.Script_int.num) (i2 : Alpha_context.Script_int.num)
      : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Sub_int (int_bytes i1) (int_bytes i2)).
    
    Definition mul_bigint
      (i1 : Alpha_context.Script_int.num) (i2 : Alpha_context.Script_int.num)
      : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Mul_intint (int_bytes i1) (int_bytes i2)).
    
    Definition ediv_teznat {A B : Set} (_tez : A) (_n : B)
      : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Ediv_teznat.
    
    Definition ediv_tez : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Ediv_tez.
    
    Definition ediv_bigint
      (i1 : Alpha_context.Script_int.num) (i2 : Alpha_context.Script_int.num)
      : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Ediv_natnat (int_bytes i1) (int_bytes i2)).
    
    Definition eq_value : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Eq.
    
    Definition lsl_nat (shifted : Alpha_context.Script_int.num)
      : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Lsl_nat (int_bytes shifted)).
    
    Definition lsr_nat (shifted : Alpha_context.Script_int.num)
      : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Lsr_nat (int_bytes shifted)).
    
    Definition or_nat
      (n1 : Alpha_context.Script_int.num) (n2 : Alpha_context.Script_int.num)
      : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Or_nat (int_bytes n1) (int_bytes n2)).
    
    Definition and_nat
      (n1 : Alpha_context.Script_int.num) (n2 : Alpha_context.Script_int.num)
      : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_And_nat (int_bytes n1) (int_bytes n2)).
    
    Definition xor_nat
      (n1 : Alpha_context.Script_int.num) (n2 : Alpha_context.Script_int.num)
      : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Xor_nat (int_bytes n1) (int_bytes n2)).
    
    Definition not_int (i : Alpha_context.Script_int.num)
      : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Not_int (int_bytes i)).
    
    Definition not_nat
      : Alpha_context.Script_int.num -> Alpha_context.Gas.cost := not_int.
    
    Definition seq : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Seq.
    
    Definition if_ : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_If.
    
    Definition loop : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Loop.
    
    Definition loop_left : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Loop_left.
    
    Definition dip : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Dip.
    
    Definition check_signature (pkey : Signature.public_key) (b_value : bytes)
      : Alpha_context.Gas.cost :=
      let cost :=
        match pkey with
        | Signature.Ed25519 _ =>
          Generated_costs_007.cost_N_Check_signature_ed25519
            (Bytes.length b_value)
        | Signature.Secp256k1 _ =>
          Generated_costs_007.cost_N_Check_signature_secp256k1
            (Bytes.length b_value)
        | Signature.P256 _ =>
          Generated_costs_007.cost_N_Check_signature_p256 (Bytes.length b_value)
        end in
      Alpha_context.Gas.atomic_step_cost cost.
    
    Definition blake2b (b_value : bytes) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Blake2b (Bytes.length b_value)).
    
    Definition sha256 (b_value : bytes) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Sha256 (Bytes.length b_value)).
    
    Definition sha512 (b_value : bytes) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Sha512 (Bytes.length b_value)).
    
    Definition dign (n : int) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost (Generated_costs_007.cost_N_Dig n).
    
    Definition dugn (n : int) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost (Generated_costs_007.cost_N_Dug n).
    
    Definition dipn (n : int) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost (Generated_costs_007.cost_N_DipN n).
    
    Definition dropn (n : int) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost (Generated_costs_007.cost_N_DropN n).
    
    Definition voting_power : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Voting_power.
    
    Definition total_voting_power : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_N_Total_voting_power.
    
    Definition keccak (b_value : bytes) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Keccak (Bytes.length b_value)).
    
    Definition sha3 (b_value : bytes) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Sha3 (Bytes.length b_value)).
    
    Definition add_bls12_381_g1 : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_N_Add_bls12_381_g1.
    
    Definition add_bls12_381_g2 : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_N_Add_bls12_381_g2.
    
    Definition add_bls12_381_fr : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_N_Add_bls12_381_fr.
    
    Definition mul_bls12_381_g1 : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_N_Mul_bls12_381_g1.
    
    Definition mul_bls12_381_g2 : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_N_Mul_bls12_381_g2.
    
    Definition mul_bls12_381_fr : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_N_Mul_bls12_381_fr.
    
    Definition mul_bls12_381_fr_z : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_N_Mul_bls12_381_fr_z.
    
    Definition int_bls12_381_fr : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_N_Int_bls12_381_fr.
    
    Definition neg_bls12_381_g1 : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_N_Neg_bls12_381_g1.
    
    Definition neg_bls12_381_g2 : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_N_Neg_bls12_381_g2.
    
    Definition neg_bls12_381_fr : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_N_Neg_bls12_381_fr.
    
    Definition neq : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Neq.
    
    Definition nop : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_N_Nop.
    
    Definition pairing_check_bls12_381 {a : Set}
      (l_value : Script_typed_ir.boxed_list a) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Pairing_check_bls12_381
          l_value.(Script_typed_ir.boxed_list.length)).
    
    Definition comb (n : int) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost (Generated_costs_007.cost_N_Comb n).
    
    Definition uncomb (n : int) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost (Generated_costs_007.cost_N_Uncomb n).
    
    Definition comb_get (n : int) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost (Generated_costs_007.cost_N_Comb_get n).
    
    Definition comb_set (n : int) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost (Generated_costs_007.cost_N_Comb_set n).
    
    Definition dupn (n : int) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost (Generated_costs_007.cost_N_DupN n).
    
    Definition sapling_verify_update (inputs : int) (outputs : int)
      : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (((Z.of_int 85000) +Z ((Z.of_int inputs) *Z (Z.of_int 4))) +Z
        ((Z.of_int outputs) *Z (Z.of_int 30))).
    
    Definition compare_unit : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost (Z.of_int 10).
    
    Definition compare_union_tag : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost (Z.of_int 10).
    
    Definition compare_option_tag : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost (Z.of_int 10).
    
    Definition compare_bool : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Compare_bool 1 1).
    
    Definition compare_signature : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost (Z.of_int 92).
    
    Definition compare_string (s1 : string) (s2 : string)
      : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Compare_string (String.length s1)
          (String.length s2)).
    
    Definition compare_bytes (b1 : bytes) (b2 : bytes)
      : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Compare_string (Bytes.length b1)
          (Bytes.length b2)).
    
    Definition compare_mutez : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Compare_mutez 8 8).
    
    Definition compare_int
      (i1 : Alpha_context.Script_int.num) (i2 : Alpha_context.Script_int.num)
      : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Compare_int (int_bytes i1) (int_bytes i2)).
    
    Definition compare_nat
      (n1 : Alpha_context.Script_int.num) (n2 : Alpha_context.Script_int.num)
      : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Compare_int (int_bytes n1) (int_bytes n2)).
    
    Definition compare_key_hash : Alpha_context.Gas.cost :=
      let sz := Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.size) in
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Compare_key_hash sz sz).
    
    Definition compare_key : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost (Z.of_int 92).
    
    Definition compare_timestamp
      (t1 : Alpha_context.Script_timestamp.t)
      (t2 : Alpha_context.Script_timestamp.t) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Compare_timestamp
          (z_bytes (Alpha_context.Script_timestamp.to_zint t1))
          (z_bytes (Alpha_context.Script_timestamp.to_zint t2))).
    
    Definition compare_address : Alpha_context.Gas.cost :=
      let sz :=
        Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.size) +i
        Chain_id.size in
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Compare_address sz sz).
    
    Definition compare_chain_id : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost (Z.of_int 30).
    
    Fixpoint compare {a : Set}
      (ty : Script_typed_ir.comparable_ty) (x : a) (y : a) {struct ty}
      : Alpha_context.Gas.cost :=
      match (ty, x, y) with
      | (Script_typed_ir.Unit_key, _, _) => compare_unit
      
      | (Script_typed_ir.Bool_key, _, _) => compare_bool
      
      | (Script_typed_ir.String_key, x, y) =>
        let '[y, x] := cast [string ** string] [y, x] in
        compare_string x y
      
      | (Script_typed_ir.Signature_key, _, _) => compare_signature
      
      | (Script_typed_ir.Bytes_key, x, y) =>
        let '[y, x] := cast [bytes ** bytes] [y, x] in
        compare_bytes x y
      
      | (Script_typed_ir.Mutez_key, _, _) => compare_mutez
      
      | (Script_typed_ir.Int_key, x, y) =>
        let '[y, x] :=
          cast [Alpha_context.Script_int.num ** Alpha_context.Script_int.num]
            [y, x] in
        compare_int x y
      
      | (Script_typed_ir.Nat_key, x, y) =>
        let '[y, x] :=
          cast [Alpha_context.Script_int.num ** Alpha_context.Script_int.num]
            [y, x] in
        compare_nat x y
      
      | (Script_typed_ir.Key_hash_key, _, _) => compare_key_hash
      
      | (Script_typed_ir.Key_key, _, _) => compare_key
      
      | (Script_typed_ir.Timestamp_key, x, y) =>
        let '[y, x] :=
          cast
            [Alpha_context.Script_timestamp.t **
              Alpha_context.Script_timestamp.t] [y, x] in
        compare_timestamp x y
      
      | (Script_typed_ir.Address_key, _, _) => compare_address
      
      | (Script_typed_ir.Chain_id_key, _, _) => compare_chain_id
      
      | (Script_typed_ir.Pair_key tl tr, x, y) =>
        let 'existT _ [__0, __1] [y, x, tr, tl] :=
          cast_exists (Es := [Set ** Set])
            (fun '[__0, __1] =>
              [__0 * __1 ** __0 * __1 ** Script_typed_ir.comparable_ty **
                Script_typed_ir.comparable_ty]) [y, x, tr, tl] in
        let '(xl, xr) := x in
        let '(yl, yr) := y in
        Alpha_context.Gas.op_plusat (compare tl xl yl) (compare tr xr yr)
      
      | (Script_typed_ir.Union_key tl tr, x, y) =>
        let 'existT _ [__2, __3] [y, x, tr, tl] :=
          cast_exists (Es := [Set ** Set])
            (fun '[__2, __3] =>
              [Script_typed_ir.union __2 __3 ** Script_typed_ir.union __2 __3 **
                Script_typed_ir.comparable_ty ** Script_typed_ir.comparable_ty])
            [y, x, tr, tl] in
        Alpha_context.Gas.op_plusat compare_union_tag
          match (x, y) with
          | (Script_typed_ir.L x, Script_typed_ir.L y) => compare tl x y
          | (Script_typed_ir.L _, Script_typed_ir.R _) => Alpha_context.Gas.free
          | (Script_typed_ir.R _, Script_typed_ir.L _) => Alpha_context.Gas.free
          | (Script_typed_ir.R x, Script_typed_ir.R y) => compare tr x y
          end
      
      | (Script_typed_ir.Option_key t_value, x, y) =>
        let 'existT _ __4 [y, x, t_value] :=
          cast_exists (Es := Set)
            (fun __4 =>
              [option __4 ** option __4 ** Script_typed_ir.comparable_ty])
            [y, x, t_value] in
        Alpha_context.Gas.op_plusat compare_option_tag
          match (x, y) with
          | (None, None) => Alpha_context.Gas.free
          | (None, Some _) => Alpha_context.Gas.free
          | (Some _, None) => Alpha_context.Gas.free
          | (Some x, Some y) => compare t_value x y
          end
      | _ => unreachable_gadt_branch
      end.
    
    Definition sapling_empty_state : Alpha_context.Gas.cost := empty_map.
    
    Definition concat_string_precheck {a : Set}
      (l_value : Script_typed_ir.boxed_list a) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        ((Z.of_int l_value.(Script_typed_ir.boxed_list.length)) *Z (Z.of_int 10)).
    
    Definition concat_string (total_bytes : Z.t) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        ((Z.of_int 100) +Z
        (Pervasives.fst (Z.ediv_rem total_bytes (Z.of_int 10)))).
    
    Definition concat_bytes (total_bytes : Z.t) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        ((Z.of_int 100) +Z
        (Pervasives.fst (Z.ediv_rem total_bytes (Z.of_int 10)))).
    
    Definition exec : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost (Z.of_int 100).
    
    Definition apply : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost (Z.of_int 1000).
    
    Definition lambda : Alpha_context.Gas.cost := push.
    
    Definition address : Alpha_context.Gas.cost := push.
    
    Definition contract : Alpha_context.Gas.cost := push.
    
    Definition transfer_tokens : Alpha_context.Gas.cost :=
      Alpha_context.Gas.op_plusat push push.
    
    Definition implicit_account : Alpha_context.Gas.cost := push.
    
    Definition create_contract : Alpha_context.Gas.cost :=
      Alpha_context.Gas.op_plusat push push.
    
    Definition set_delegate : Alpha_context.Gas.cost :=
      Alpha_context.Gas.op_plusat push push.
    
    Definition balance : Alpha_context.Gas.cost := Alpha_context.Gas.free.
    
    Definition level : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        ((Z.of_int 2) *Z Generated_costs_007.cost_N_Const).
    
    Definition now : Alpha_context.Gas.cost := level.
    
    Definition hash_key {A : Set} (_pk : A) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_N_Blake2b public_key_size).
    
    Definition source : Alpha_context.Gas.cost := push.
    
    Definition sender : Alpha_context.Gas.cost := source.
    
    Definition self : Alpha_context.Gas.cost := source.
    
    Definition self_address : Alpha_context.Gas.cost := source.
    
    Definition amount : Alpha_context.Gas.cost := source.
    
    Definition chain_id : Alpha_context.Gas.cost := source.
    
    Definition unpack_failed (bytes_value : bytes) : Alpha_context.Gas.cost :=
      let len := Z.of_int (Bytes.length bytes_value) in
      Alpha_context.Gas.op_plusat
        (Alpha_context.Gas.op_starat len (Alpha_context.Gas.alloc_mbytes_cost 1))
        (Alpha_context.Gas.op_starat len
          (Alpha_context.Gas.op_starat (Z.of_int (Z.numbits len))
            (Alpha_context.Gas.op_plusat
              (Alpha_context.Gas.alloc_cost (Z.of_int 3))
              (Alpha_context.Gas.step_cost Z.one)))).
    
    Definition ticket : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost (Z.of_int 80).
    
    Definition read_ticket : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost (Z.of_int 80).
    
    Definition split_ticket
      (ticket_amount : Alpha_context.Script_int.num)
      (amount_a : Alpha_context.Script_int.num)
      (amount_b : Alpha_context.Script_int.num) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.op_plusat
        (Alpha_context.Gas.op_plusat ticket (add_bigint amount_a amount_b))
        (compare_nat ticket_amount ticket_amount).
    
    Definition join_tickets {a : Set}
      (ty : Script_typed_ir.comparable_ty) (ticket_a : Script_typed_ir.ticket a)
      (ticket_b : Script_typed_ir.ticket a) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.op_plusat
        (Alpha_context.Gas.op_plusat
          (Alpha_context.Gas.op_plusat ticket compare_address)
          (add_bigint ticket_a.(Script_typed_ir.ticket.amount)
            ticket_b.(Script_typed_ir.ticket.amount)))
        (compare ty ticket_a.(Script_typed_ir.ticket.contents)
          ticket_b.(Script_typed_ir.ticket.contents)).
  End Interpreter.
  
  Module Typechecking.
    Definition public_key_optimized : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Compare.Z.(Compare.S.max)
          Generated_costs_007.cost_DECODING_PUBLIC_KEY_ed25519
          (Compare.Z.(Compare.S.max)
            Generated_costs_007.cost_DECODING_PUBLIC_KEY_secp256k1
            Generated_costs_007.cost_DECODING_PUBLIC_KEY_p256)).
    
    Definition public_key_readable : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Compare.Z.(Compare.S.max)
          Generated_costs_007.cost_B58CHECK_DECODING_PUBLIC_KEY_ed25519
          (Compare.Z.(Compare.S.max)
            Generated_costs_007.cost_B58CHECK_DECODING_PUBLIC_KEY_secp256k1
            Generated_costs_007.cost_B58CHECK_DECODING_PUBLIC_KEY_p256)).
    
    Definition key_hash_optimized : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Compare.Z.(Compare.S.max)
          Generated_costs_007.cost_DECODING_PUBLIC_KEY_HASH_ed25519
          (Compare.Z.(Compare.S.max)
            Generated_costs_007.cost_DECODING_PUBLIC_KEY_HASH_secp256k1
            Generated_costs_007.cost_DECODING_PUBLIC_KEY_HASH_p256)).
    
    Definition key_hash_readable : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Compare.Z.(Compare.S.max)
          Generated_costs_007.cost_B58CHECK_DECODING_PUBLIC_KEY_HASH_ed25519
          (Compare.Z.(Compare.S.max)
            Generated_costs_007.cost_B58CHECK_DECODING_PUBLIC_KEY_HASH_secp256k1
            Generated_costs_007.cost_B58CHECK_DECODING_PUBLIC_KEY_HASH_p256)).
    
    Definition signature_optimized : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Compare.Z.(Compare.S.max)
          Generated_costs_007.cost_DECODING_SIGNATURE_ed25519
          (Compare.Z.(Compare.S.max)
            Generated_costs_007.cost_DECODING_SIGNATURE_secp256k1
            Generated_costs_007.cost_DECODING_SIGNATURE_p256)).
    
    Definition signature_readable : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Compare.Z.(Compare.S.max)
          Generated_costs_007.cost_B58CHECK_DECODING_SIGNATURE_ed25519
          (Compare.Z.(Compare.S.max)
            Generated_costs_007.cost_B58CHECK_DECODING_SIGNATURE_secp256k1
            Generated_costs_007.cost_B58CHECK_DECODING_SIGNATURE_p256)).
    
    Definition chain_id_optimized : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_DECODING_CHAIN_ID.
    
    Definition chain_id_readable : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_B58CHECK_DECODING_CHAIN_ID.
    
    Definition address_optimized : Alpha_context.Gas.cost := key_hash_optimized.
    
    Definition contract_optimized : Alpha_context.Gas.cost :=
      key_hash_optimized.
    
    Definition contract_readable : Alpha_context.Gas.cost := key_hash_readable.
    
    Definition bls12_381_g1 : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_DECODING_BLS_G1.
    
    Definition bls12_381_g2 : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_DECODING_BLS_G2.
    
    Definition bls12_381_fr : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_DECODING_BLS_FR.
    
    Definition check_printable (s : string) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Generated_costs_007.cost_CHECK_PRINTABLE (String.length s)).
    
    Definition merge_cycle : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_MERGE_TYPES.
    
    Definition parse_type_cycle : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_PARSE_TYPE.
    
    Definition parse_instr_cycle : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_TYPECHECKING_CODE.
    
    Definition parse_data_cycle : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_TYPECHECKING_DATA.
    
    Definition comparable_ty_of_ty_cycle : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_COMPARABLE_TY_OF_TY.
    
    Definition check_dupable_cycle : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_TYPECHECKING_DATA.
    
    Definition bool_value : Alpha_context.Gas.cost := Alpha_context.Gas.free.
    
    Definition unit_value : Alpha_context.Gas.cost := Alpha_context.Gas.free.
    
    Definition timestamp_readable : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_TIMESTAMP_READABLE_DECODING.
    
    Definition contract : Alpha_context.Gas.cost :=
      Alpha_context.Gas.op_starat (Z.of_int 2) public_key_readable.
    
    Definition contract_exists : Alpha_context.Gas.cost :=
      Alpha_context.Gas.cost_of_repr (Storage_costs.read_access 9 8).
    
    Definition proof_argument (n : int) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost ((Z.of_int n) *Z (Z.of_int 50)).
  End Typechecking.
  
  Module Unparsing.
    Definition public_key_optimized : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Compare.Z.(Compare.S.max)
          Generated_costs_007.cost_ENCODING_PUBLIC_KEY_ed25519
          (Compare.Z.(Compare.S.max)
            Generated_costs_007.cost_ENCODING_PUBLIC_KEY_secp256k1
            Generated_costs_007.cost_ENCODING_PUBLIC_KEY_p256)).
    
    Definition public_key_readable : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Compare.Z.(Compare.S.max)
          Generated_costs_007.cost_B58CHECK_ENCODING_PUBLIC_KEY_ed25519
          (Compare.Z.(Compare.S.max)
            Generated_costs_007.cost_B58CHECK_ENCODING_PUBLIC_KEY_secp256k1
            Generated_costs_007.cost_B58CHECK_ENCODING_PUBLIC_KEY_p256)).
    
    Definition key_hash_optimized : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Compare.Z.(Compare.S.max)
          Generated_costs_007.cost_ENCODING_PUBLIC_KEY_HASH_ed25519
          (Compare.Z.(Compare.S.max)
            Generated_costs_007.cost_ENCODING_PUBLIC_KEY_HASH_secp256k1
            Generated_costs_007.cost_ENCODING_PUBLIC_KEY_HASH_p256)).
    
    Definition key_hash_readable : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Compare.Z.(Compare.S.max)
          Generated_costs_007.cost_B58CHECK_ENCODING_PUBLIC_KEY_HASH_ed25519
          (Compare.Z.(Compare.S.max)
            Generated_costs_007.cost_B58CHECK_ENCODING_PUBLIC_KEY_HASH_secp256k1
            Generated_costs_007.cost_B58CHECK_ENCODING_PUBLIC_KEY_HASH_p256)).
    
    Definition signature_optimized : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Compare.Z.(Compare.S.max)
          Generated_costs_007.cost_ENCODING_SIGNATURE_ed25519
          (Compare.Z.(Compare.S.max)
            Generated_costs_007.cost_ENCODING_SIGNATURE_secp256k1
            Generated_costs_007.cost_ENCODING_SIGNATURE_p256)).
    
    Definition signature_readable : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        (Compare.Z.(Compare.S.max)
          Generated_costs_007.cost_B58CHECK_ENCODING_SIGNATURE_ed25519
          (Compare.Z.(Compare.S.max)
            Generated_costs_007.cost_B58CHECK_ENCODING_SIGNATURE_secp256k1
            Generated_costs_007.cost_B58CHECK_ENCODING_SIGNATURE_p256)).
    
    Definition chain_id_optimized : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_ENCODING_CHAIN_ID.
    
    Definition chain_id_readable : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_B58CHECK_ENCODING_CHAIN_ID.
    
    Definition timestamp_readable : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_TIMESTAMP_READABLE_ENCODING.
    
    Definition address_optimized : Alpha_context.Gas.cost := key_hash_optimized.
    
    Definition contract_optimized : Alpha_context.Gas.cost :=
      key_hash_optimized.
    
    Definition contract_readable : Alpha_context.Gas.cost := key_hash_readable.
    
    Definition bls12_381_g1 : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_ENCODING_BLS_G1.
    
    Definition bls12_381_g2 : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_ENCODING_BLS_G2.
    
    Definition bls12_381_fr : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost
        Generated_costs_007.cost_ENCODING_BLS_FR.
    
    Definition unparse_type_cycle : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_UNPARSE_TYPE.
    
    Definition unparse_instr_cycle : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_UNPARSING_CODE.
    
    Definition unparse_data_cycle : Alpha_context.Gas.cost :=
      Alpha_context.Gas.atomic_step_cost Generated_costs_007.cost_UNPARSING_DATA.
    
    Definition unit_value : Alpha_context.Gas.cost := Alpha_context.Gas.free.
    
    Definition contract : Alpha_context.Gas.cost :=
      Alpha_context.Gas.op_starat (Z.of_int 2) public_key_readable.
    
    Definition operation (bytes_value : bytes) : Alpha_context.Gas.cost :=
      Alpha_context.Script.bytes_node_cost bytes_value.
    
    Definition sapling_transaction {A : Set} (_t : A)
      : Alpha_context.Gas.cost := Alpha_context.Gas.free.
    
    Definition sapling_diff {A : Set} (_d : A) : Alpha_context.Gas.cost :=
      Alpha_context.Gas.free.
  End Unparsing.
End Cost_of.
