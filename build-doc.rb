# Run this script after coqdoc to populate the Docusaurus documentation in doc/.
require 'fileutils'
require 'json'
require 'pathname'

$base_url = "/coq-tezos-of-ocaml"
$coqdoc_path = "html"
$output_directory = File.join("doc", "docs")
$source_directory = File.join("src", "Proto_alpha")

$nb_documentation_files = 0

def get_relative_path(path)
  source = Pathname.new($source_directory)
  target = Pathname.new(path)
  target.relative_path_from(source).to_s
end

# Fix the links in the generated coqdoc files.
def fix_links(content)
  content
    .gsub(/href="TezosOfOCaml.Proto_alpha.((\w|\.)+).html/) {
      "href=\"#{$base_url}/docs/#{$1.downcase.gsub(".", "/")}/"
    }
    .gsub("#\"", "\"")
end

# We treat the coqdoc as raw HTML, as it would slow-down React otherwise (too
# many links).
def to_jsx(content)
  "<div id=\"main\" dangerouslySetInnerHTML={#{JSON.generate({"__html" => content})}} />"
end

def write_if_different(path, content)
  if !File.exist?(path) || File.read(path, :encoding => 'utf-8') != content then
    FileUtils.mkdir_p(File.dirname(path))
    File.open(path, "w") do |file|
      file << content
    end
  end
end

def list_directories(path)
  Dir.glob(File.join(path, "*")).select {|path| File.directory?(path)}
end

def list_md_files(path)
  Dir.glob(File.join(path, "*.md"))
end

$v_files_black_list = File.read("blacklist.txt").split("\n")

def list_v_files(path)
  Dir.glob(File.join(path, "*.v")) - $v_files_black_list
end

def get_target_path_for_sidebar(relative_path, suffix)
  Pathname.new(File.join(File.dirname(relative_path), File.basename(relative_path, suffix))).cleanpath.to_s
end

def md_of_md(path)
  base_name = File.basename(path, ".md")
  content = <<-END
---
id: #{base_name.downcase}
title: #{base_name.gsub("_", " ").strip.capitalize}
---

_Source: [`#{path}`](https://gitlab.com/nomadic-labs/coq-tezos-of-ocaml/-/blob/master/#{path})_

#{File.read(path, :encoding => 'utf-8')}
  END
  relative_path = get_relative_path(path).downcase
  target_path = File.join($output_directory, relative_path)
  write_if_different(target_path, content)
  $nb_documentation_files += 1
  get_target_path_for_sidebar(relative_path, ".md")
end

def mdx_of_v(path)
  doc_path = File.join(
    $coqdoc_path,
    File.basename("TezosOfOCaml.Proto_alpha." + get_relative_path(path).gsub("/", "."), ".v") + ".html"
  )
  content = <<-END
---
id: #{File.basename(path, ".v").downcase}
title: #{File.basename(path)}
hide_table_of_contents: true
---

_Source: [`#{path}`](https://gitlab.com/nomadic-labs/coq-tezos-of-ocaml/-/blob/master/#{path})_

#{to_jsx(fix_links(File.read(doc_path, :encoding => 'utf-8')))}
  END
  relative_path = get_relative_path(path).downcase
  target_path_v = File.join($output_directory, relative_path)
  target_path_mdx = File.join(File.dirname(target_path_v), File.basename(target_path_v, ".v") + ".mdx")
  write_if_different(target_path_mdx, content)
  $nb_documentation_files += 1
  get_target_path_for_sidebar(relative_path, ".v")
end

def documentation_of_path(path)
  list_directories(path).sort_by {|name| name.downcase}.map do |path|
    {
      "type" => "category",
      "label" => File.basename(path).gsub("_", " ").strip.capitalize,
      "items" => documentation_of_path(path)
    }
  end +
  list_md_files(path).sort_by {|name| name.downcase}.map do |path|
    md_of_md(path)
  end +
  list_v_files(path).sort_by {|name| name.downcase}.map do |path|
    mdx_of_v(path)
  end
end

sidebars = { "docs" => documentation_of_path($source_directory) }
File.open("doc/sidebars.js", "w") do |file|
  file << "module.exports = " + JSON.pretty_generate(sidebars) + ";"
end

def nb_lines(&predicate)
  (Dir.glob(File.join($source_directory, "**", "*.v")) - $v_files_black_list)
    .select(&predicate)
    .map {|path| File.read(path, :encoding => 'utf-8').split("\n").size}
    .reduce(:+)
end

def is_proof(path)
  Pathname(path).each_filename.include?("Proofs")
end

def is_environment(path)
  !is_proof(path) &&
  Pathname(path).each_filename.include?("Environment")
end

def is_code(path)
  !is_proof(path) && !is_environment(path) &&
  File.basename(path) != "Environment.v"
end

File.open("doc/nbLines.js", "w") do |file|
  file << <<-END
export default {
  code: #{nb_lines {|path| is_code(path)}},
  environment: #{nb_lines {|path| is_environment(path)}},
  proofs: #{nb_lines {|path| is_proof(path)}},
}
  END
end

puts "#{$nb_documentation_files} generated documentation files in #{$output_directory}"
